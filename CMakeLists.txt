#CMAKE_TOOLCHAIN_FILE

project(nemirtingas_steam_emu)
cmake_minimum_required(VERSION 3.0)

# Workaround because cross-compiling with CMake + vcpkg fucks up this variable
# making the 'find_*' functions/macros somehow fail to find the right arch.
# There are conditions like
#  CMAKE_SIZEOF_VOID_P = "4" then look for i386 libraries...
# or
#  CMAKE_SIZEOF_VOID_P = "8" then look for x86_64 libraries...
if(X86 AND NOT X64)
  set(CMAKE_SIZEOF_VOID_P "4")
elseif(X64 AND NOT X86)
  set(CMAKE_SIZEOF_VOID_P "8")
else()
  message(FATAL_ERROR "Please define either -DX86=ON or -DX64=ON")
endif()

if(WIN32) # Setup some variables for Windows build
  if(MSVC) # If building with MSVC
    add_definitions(-D_CRT_SECURE_NO_WARNINGS) # Disable warning about strncpy_s and his friends
    #set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /NODEFAULTLIB:\"msvcrtd.lib\"")       # Disable this linkage
    #set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} /NODEFAULTLIB:\"msvcrtd.lib\"") # Disable this linkage
    set(CMAKE_CONFIGURATION_TYPES "Debug;Release" CACHE STRING "" FORCE) # Force to only build Debug & Release projects

    set(CompilerFlags
        CMAKE_CXX_FLAGS
        CMAKE_CXX_FLAGS_DEBUG
        CMAKE_CXX_FLAGS_RELEASE
        CMAKE_C_FLAGS
        CMAKE_C_FLAGS_DEBUG
        CMAKE_C_FLAGS_RELEASE
        )
    foreach(CompilerFlag ${CompilerFlags})
      string(REPLACE "/MD" "/MT" ${CompilerFlag} "${${CompilerFlag}}")
    endforeach()
    
  endif()

  if(X64)
    set(STEAM_CLIENT steamclient64)
    set(OUT_DIR win64)
  elseif(X86)
    set(STEAM_CLIENT steamclient)
    set(OUT_DIR win32)
  else()
    message(FATAL_ERROR "Arch unknown")
  endif()

  file(
    GLOB
    overlay_sources
	overlay/*.cpp
    overlay/windows/*.cpp
	extra/ImGui/*.cpp
    extra/ImGui/impls/*.cpp
    extra/ImGui/impls/windows/*.cpp
  )
  
  set(overlay_includes
    overlay/
    overlay/windows/
  )

elseif(APPLE)
  if(X64)
    # Global flags for building steamapi (64bits)
    set(CMAKE_C_FLAGS             "${CMAKE_C_FLAGS} -m64")
    set(CMAKE_CXX_FLAGS           "${CMAKE_CXX_FLAGS} -m64")
    set(CMAKE_EXE_LINKER_FLAGS    "${CMAKE_EXE_LINKER_FLAGS} -m64")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -m64")
    #set(CMAKE_STATIC_LINKER_FLAGS "${CMAKE_STATIC_LINKER_FLAGS} -m64")
    set(OUT_DIR macosx64)
  elseif(X86)
    # Global flags for building steamapi (32bits)
    set(CMAKE_C_FLAGS             "${CMAKE_C_FLAGS} -m32")
    set(CMAKE_CXX_FLAGS           "${CMAKE_CXX_FLAGS} -m32")
    set(CMAKE_EXE_LINKER_FLAGS    "${CMAKE_EXE_LINKER_FLAGS} -m32")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -m32")
    #set(CMAKE_STATIC_LINKER_FLAGS "${CMAKE_STATIC_LINKER_FLAGS} -m32")
    set(OUT_DIR macosx32)
  else()
    message(FATAL_ERROR "Arch unknown")
  endif()
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -stdlib=libc++")

  set(STEAM_CLIENT steamclient)

  file(
    GLOB
    overlay_sources
    overlay/macosx/*.cpp
  )
  
  set(overlay_includes
    overlay/
    overlay/macosx/
  )

elseif(UNIX)
  if(X64)
    # Global flags for building steamapi (64bits)
    set(CMAKE_C_FLAGS             "${CMAKE_C_FLAGS} -m64")
    set(CMAKE_CXX_FLAGS           "${CMAKE_CXX_FLAGS} -m64")
    set(CMAKE_EXE_LINKER_FLAGS    "${CMAKE_EXE_LINKER_FLAGS} -m64")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -m64")
    #set(CMAKE_STATIC_LINKER_FLAGS "${CMAKE_STATIC_LINKER_FLAGS} -m64")
    set(OUT_DIR linux64)
  elseif(X86)
    # Global flags for building steamapi (32bits)
    set(CMAKE_C_FLAGS             "${CMAKE_C_FLAGS} -m32")
    set(CMAKE_CXX_FLAGS           "${CMAKE_CXX_FLAGS} -m32")
    set(CMAKE_EXE_LINKER_FLAGS    "${CMAKE_EXE_LINKER_FLAGS} -m32")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -m32")
    #set(CMAKE_STATIC_LINKER_FLAGS "${CMAKE_STATIC_LINKER_FLAGS} -m32")
    set(OUT_DIR linux32)
  else()
    message(FATAL_ERROR "Arch unknown")
  endif()
  set(STEAM_CLIENT steamclient)

  file(
    GLOB
    overlay_sources
	overlay/*.cpp
    overlay/linux/*.cpp
	extra/ImGui/*.cpp
    extra/ImGui/impls/*.cpp
    extra/ImGui/impls/linux/*.cpp
  )

  set(overlay_includes
    overlay/
    overlay/linux/
  )

else()
  message(FATAL_ERROR "No CMake for other platforms")

endif()

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Debug or Release")

option(DISABLE_LOG "Disable all logging. Will reduce emu size and will speed it up (a bit)" OFF)

option(USE_ZSTD_COMPRESS "Use zstd to compress network messages." ON)

set(Protobuf_USE_STATIC_LIBS ON)
include(FindProtobuf)
find_package(Threads REQUIRED)
find_package(Protobuf CONFIG REQUIRED)
find_package(CURL CONFIG REQUIRED)
find_package(ZLIB REQUIRED)
find_package(portaudio CONFIG REQUIRED)
find_package(nlohmann_json CONFIG REQUIRED)
find_package(nlohmann-fifo-map CONFIG REQUIRED)

if(NOT WIN32)
  find_package(OpenSSL REQUIRED)
endif()

if(NOT APPLE)
  find_package(GLEW REQUIRED)
endif()


if(USE_ZSTD_COMPRESS)
  find_package(zstd CONFIG REQUIRED)
endif()

########################################
## net.h net.cc
protobuf_generate_cpp(net_PROTO_SRCS net_PROTO_HDRS proto/network_proto.proto)

########################################
## steamclient[64].dll
file(
  GLOB
  emu_sources
  steam_dll/*.cpp
  managers/*.cpp
)

file(
  GLOB
  socket_sources
  extra/Socket/src/common/*.cpp
  extra/Socket/src/ipv4/*.cpp
  extra/Socket/src/ipv6/*.cpp
)

file(
  GLOB
  utils_sources
  extra/utils/src/*.cpp
)

file(
  GLOB
  gamepad_sources
  extra/gamepad/gamepad.cpp
)

file(
  GLOB
  mini_detour_sources
  extra/mini_detour/*.cpp
)

########################################
## steamclient[64].dll
add_library(
  ${STEAM_CLIENT}
  SHARED
  ${emu_sources}
  ${net_PROTO_SRCS}
  
  ${mini_detour_sources}
  ${socket_sources}
  ${utils_sources}
  ${gamepad_sources}
  
  ${overlay_sources}
)

if(UNIX)
  SET_TARGET_PROPERTIES(${STEAM_CLIENT} PROPERTIES PREFIX "")
endif()

target_link_libraries(
  ${STEAM_CLIENT}
  PRIVATE
  Threads::Threads
  ZLIB::ZLIB
  protobuf::libprotobuf-lite
  CURL::libcurl
  #nlohmann_json::nlohmann_json doesn't work
  nlohmann-fifo-map::nlohmann-fifo-map
  
  $<$<NOT:$<BOOL:${WIN32}>>:OpenSSL::SSL>
  $<$<NOT:$<BOOL:${WIN32}>>:OpenSSL::Crypto>
  $<$<NOT:$<BOOL:${APPLE}>>:GLEW::GLEW>
  $<$<BOOL:${USE_ZSTD_COMPRESS}>:libzstd>

  # For gamepad open-source xinput
  $<$<BOOL:${WIN32}>:setupapi>
  # For overlay opengl
  $<$<BOOL:${WIN32}>:opengl32>
  # For library network
  $<$<BOOL:${WIN32}>:ws2_32>
  $<$<BOOL:${WIN32}>:iphlpapi>
  
  $<$<BOOL:${WIN32}>:shell32>
  $<$<BOOL:${WIN32}>:user32>

  $<$<BOOL:${WIN32}>:advapi32>
  $<$<BOOL:${WIN32}>:crypt32>

  # For library UNIX loading
  $<$<NOT:$<BOOL:${WIN32}>>:dl>
  # For overlay opengl
  $<$<AND:$<BOOL:${UNIX}>,$<NOT:$<BOOL:${APPLE}>>>:GL>
)

target_include_directories(
  ${STEAM_CLIENT}
  PRIVATE
  ${CMAKE_CURRENT_BINARY_DIR}
  
  steam_dll/
  managers/
  
  extra/
  extra/steam_sdk/
  extra/mini_detour/
  extra/ImGui/
  extra/gamepad/
  extra/stb/
  extra/utils/include
  extra/Socket/include
  
  resources/
  
  ${overlay_includes}
)

target_compile_options(
  ${STEAM_CLIENT}
  PUBLIC
  
  $<$<BOOL:${UNIX}>:-fPIC -fpermissive -fvisibility=hidden -Wl,--exclude-libs,ALL>
  $<$<AND:$<BOOL:${UNIX}>,$<BOOL:${X86}>>:-m32>
  $<$<AND:$<BOOL:${UNIX}>,$<BOOL:${X64}>>:-m64>
  
  $<$<AND:$<CONFIG:>,$<BOOL:${MSVC}>>:/MP>
  $<$<AND:$<CONFIG:Debug>,$<BOOL:${MSVC}>>:/MP>
  $<$<AND:$<CONFIG:Release>,$<BOOL:${MSVC}>>:/MP>
)

target_compile_definitions(
  ${STEAM_CLIENT}
  PUBLIC
  __EXPORT_SYMBOLS__
  STEAMCLIENT_SHARED_LIBRARY
  STEAMCLIENT_EXPORT
  GLEW_NO_GLU
  STBI_NO_SIMD

  $<$<BOOL:${USE_ZSTD_COMPRESS}>:NETWORK_COMPRESS>

  $<$<BOOL:${UNIX}>:GNUC>

  $<$<BOOL:${DISABLE_LOG}>:DISABLE_LOG>
  $<$<STREQUAL:${CMAKE_BUILD_TYPE},Release>:EMU_RELEASE_BUILD NDEBUG>
)

####################################"
## Targets link & compiler options
## For linux based builds, make install rules

#install(FILES Readme_release.txt DESTINATION release/)
#install(FILES files_example/steam_appid.EDIT_AND_RENAME.txt files_example/steam_interfaces.EXAMPLE.txt DESTINATION release/)
#install(DIRECTORY files_example/steam_settings.EXAMPLE DESTINATION release/)

##################
## Install rules
set(CMAKE_INSTALL_PREFIX ${CMAKE_SOURCE_DIR})

if(${CMAKE_BUILD_TYPE} STREQUAL "Debug")
	set(OUT_DIR debug/${OUT_DIR})
else()
	set(OUT_DIR release/${OUT_DIR})
endif()

get_target_property(PORTAUDIO_PATH portaudio IMPORTED_LOCATION_RELEASE)
install(
	TARGETS ${STEAM_CLIENT}
	RUNTIME DESTINATION ${OUT_DIR}
	LIBRARY DESTINATION ${OUT_DIR}
)
install(
	FILES ${PORTAUDIO_PATH}
	DESTINATION ${OUT_DIR}/
)