﻿/* Copyright (C) 2019-2020 Nemirtingas
   This file is part of the NemirtingasEmuLauncher Launcher

   The NemirtingasEmuLauncher Launcher is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   The NemirtingasEmuLauncher Launcher is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the NemirtingasEmuLauncher; if not, see
   <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;

namespace NemirtingasEmuLauncher
{
    public class Dlc : EqualityComparer<Dlc>, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaiseAndSetIfChanged<TProp>(ref TProp property, TProp value, [CallerMemberName] string prop_name = "")
        {
            if (property == null || !property.Equals(value))
            {
                property = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop_name));
            }
        }


        private ulong _DlcId;
        public ulong DlcId
        {
            get => _DlcId;
            set
            {
                RaiseAndSetIfChanged(ref _DlcId, value);
                Stringified = string.Join("=", DlcId, Name);
            }
        }
        private string _Name;
        public string Name
        {
            get => _Name;
            set
            {
                RaiseAndSetIfChanged(ref _Name, value);
                Stringified = string.Join("=", DlcId, Name);
            }
        }

        private bool _Enabled;
        public bool Enabled
        {
            get => _Enabled;
            set => RaiseAndSetIfChanged(ref _Enabled, value);
        }

        private string _Stringified = string.Empty;
        public string Stringified
        {
            get => _Stringified;
            set => RaiseAndSetIfChanged(ref _Stringified, value);
        }

        public string GrayIcon { get; set; }
        public string Icon { get; set; }

        public override bool Equals([AllowNull] Dlc x, [AllowNull] Dlc y)
        {
            return x?.DlcId == y?.DlcId;
        }

        public override int GetHashCode([DisallowNull] Dlc obj)
        {
            return obj.DlcId.GetHashCode();
        }
    }
}
