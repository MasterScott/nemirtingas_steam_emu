/* Copyright (C) 2019-2020 Nemirtingas
   This file is part of the NemirtingasEmuLauncher Launcher

   The NemirtingasEmuLauncher Launcher is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   The NemirtingasEmuLauncher Launcher is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the NemirtingasEmuLauncher; if not, see
   <http://www.gnu.org/licenses/>.
 */
using System;
using System.Threading;
using System.ComponentModel;

namespace NemirtingasEmuLauncher
{
    public class IpcServer
    {
        protected CancellationTokenSource cts;
        public event EventHandler IpcServerStopped;
        public event EventHandler IpcServerStarted;
        public bool HasIpc { get; protected set; } = false;
        public bool IsStarted { get; protected set; } = false;

        public IpcServer()
        {
        }

        virtual public void StartIpcServer()
        {
        }

        virtual public void StopIpcServer()
        {
        }

        virtual public void SendKillIpcServer()
        {
        }

        protected void RaiseEvent(Delegate[] invocationList)
        {
            if (invocationList == null)
            {
                return;
            }
            //Pour chacun des délégués enregistrés,
            foreach (var del in invocationList)
            {
                //On récupère le contexte de synchronisation
                ISynchronizeInvoke syncer = del.Target as ISynchronizeInvoke;
                if (syncer != null)
                {
                    //si il existe, on invoke le délégué sur ce contexte. Ce qui permet de déclencher l'évènement
                    // sur le thread UI si l'évènement est enregistré dans le thread UI
                    syncer.BeginInvoke(del, new object[] { this, EventArgs.Empty });
                }
                else
                {
                    //s'il n'y a pas de contexte, on invoke simplement le délégué
                    del.DynamicInvoke(this, EventArgs.Empty);
                }
            }
        }

        protected void OnIpcServerStarted()
        {
            RaiseEvent(IpcServerStarted?.GetInvocationList());
        }

        protected void OnIpcServerStopped()
        {
            RaiseEvent(IpcServerStopped?.GetInvocationList());
        }
    }
}
