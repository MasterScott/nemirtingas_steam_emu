/* Copyright (C) 2019-2020 Nemirtingas
   This file is part of the NemirtingasEmuLauncher Launcher

   The NemirtingasEmuLauncher Launcher is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   The NemirtingasEmuLauncher Launcher is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the NemirtingasEmuLauncher; if not, see
   <http://www.gnu.org/licenses/>.
 */
using System;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;

using System.Threading.Tasks;

using OSUtility;

namespace NemirtingasEmuLauncher
{
    public class MacOsIpcServer : IpcServer
    {
        [DllImport("/usr/lib/system/libxpc.dylib")]
        static extern kern_return_t bootstrap_check_in(uint bp, string service_name, out uint sp);

        [DllImport("/usr/lib/system/libxpc.dylib")]
        static extern kern_return_t bootstrap_look_up(uint bp, string service_name, out uint sp);

        [DllImport("/usr/lib/system/libxpc.dylib")]
        static extern kern_return_t bootstrap_register(uint bp, string service_name, uint sp);

        [DllImport("/usr/lib/system/libxpc.dylib")]
        static extern kern_return_t mach_msg(IntPtr msg, MACH_MSG_OPTION option, uint send_size, uint rcv_size, uint rcv_name, uint timeout, uint notify);

        [DllImport("/usr/lib/system/libxpc.dylib")]
        static extern kern_return_t mach_port_allocate(uint task, MACH_PORT_RIGHT right, out uint name);

        [DllImport("/usr/lib/system/libxpc.dylib")]
        static extern kern_return_t mach_port_deallocate(uint task, uint name);

        [DllImport("/usr/lib/system/liblaunch.dylib")]
        static extern IntPtr launch_data_alloc(LAUNCH_DATA_TYPE data_type);
        [DllImport("/usr/lib/system/liblaunch.dylib")]
        static extern IntPtr launch_data_new_string(string str);
        [DllImport("/usr/lib/system/liblaunch.dylib")]
        static extern void launch_data_dict_insert(IntPtr dict, IntPtr val, string key);
        [DllImport("/usr/lib/system/liblaunch.dylib")]
        static extern IntPtr launch_msg(IntPtr msg);
        [DllImport("/usr/lib/system/liblaunch.dylib")]
        static extern void launch_data_free(IntPtr data);

        [DllImport("libdl")]
        static extern IntPtr dlopen(string name, RTLD_FLAGS mode);

        [DllImport("libdl")]
        static extern IntPtr dlsym(IntPtr lib, string name);

        [DllImport("libdl")]
        static extern void dlclose(IntPtr lib);

        /// <summary>
        /// The MODE argument to `dlopen' contains one of the following:
        /// </summary>
        [Flags]
        public enum RTLD_FLAGS : int
        {
            /// <summary>
            /// Unix98 demands the following flag which is the inverse to RTLD_GLOBAL.
            /// The implementation does this by default and so we can define the
            ///   */
            /// </summary>
            RTLD_LOCAL        = 0x0,
            /// <summary>
            /// Lazy function call binding.
            /// </summary>
            RTLD_LAZY         = 0x00001,
            /// <summary>
            /// Immediate function call binding.
            /// </summary>
            RTLD_NOW          = 0x00002,
            /// <summary>
            /// Mask of binding time value.
            /// </summary>
            RTLD_BINDING_MASK = 0x00003,
            /// <summary>
            /// Do not load the object.
            /// </summary>
            RTLD_NOLOAD       = 0x00004,
            /// <summary>
            /// Use deep binding.
            /// </summary>
            RTLD_DEEPBIND     = 0x00008,
            /// <summary>
            /// If the following bit is set in the MODE argument to `dlopen',
            /// the symbols of the loaded object and its dependencies are made
            /// visible as if the object were linked directly into the program.
            /// </summary>
            RTLD_GLOBAL       = 0x00100,
            /// <summary>
            /// Do not delete object when closed.
            /// </summary>
            RTLD_NODELETE     = 0x01000,
        }

        public enum kern_return_t : uint
        {
            KERN_SUCCESS = 0,
        }

        public enum LAUNCH_DATA_TYPE : uint
        {
            LAUNCH_DATA_DICTIONARY = 1,
            LAUNCH_DATA_ARRAY,
            LAUNCH_DATA_FD,
            LAUNCH_DATA_INTEGER,
            LAUNCH_DATA_REAL,
            LAUNCH_DATA_BOOL,
            LAUNCH_DATA_STRING,
            LAUNCH_DATA_OPAQUE,
            LAUNCH_DATA_ERRNO,
            LAUNCH_DATA_MACHPORT,
        }

        public enum MACH_PORT_RIGHT : uint
        {
            MACH_PORT_RIGHT_SEND = 0,
            MACH_PORT_RIGHT_RECEIVE = 1,
            MACH_PORT_RIGHT_SEND_ONCE = 2,
            MACH_PORT_RIGHT_PORT_SET = 3,
            MACH_PORT_RIGHT_DEAD_NAME = 4,
            MACH_PORT_RIGHT_LABELH = 5,
            MACH_PORT_RIGHT_NUMBER = 6,
        }

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct mach_msg_header_t
        {
            public uint msgh_bits;
            public uint msgh_size;
            public uint msgh_remote_port;
            public uint msgh_local_port;
            public uint msgh_reserved;
            public uint msgh_id;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct mach_msg_trailer_t
        {
            public uint msgh_trailer_type;
            public uint msgh_trailer_size;
        }

        [Flags]
        public enum MACH_MSG_OPTION : uint
        {
            MACH_MSG_OPTION_NONE    = 0x00000000,
            MACH_SEND_MSG           = 0x00000001,
            MACH_RCV_MSG            = 0x00000002,
            MACH_RCV_LARGE          = 0x00000004, /* report large message sizes */
            MACH_RCV_LARGE_IDENTITY = 0x00000008, /* identify source of large messages */
            MACH_SEND_TIMEOUT       = 0x00000010, /* timeout value applies to send */
            MACH_SEND_OVERRIDE      = 0x00000020, /* priority override for send */
            MACH_SEND_INTERRUPT     = 0x00000040, /* don't restart interrupted sends */
            MACH_SEND_NOTIFY        = 0x00000080, /* arm send-possible notify */
            MACH_RCV_TIMEOUT        = 0x00000100, /* timeout value applies to receive */
            MACH_RCV_NOTIFY         = 0x00000200, /* reserved - legacy */
            MACH_RCV_INTERRUPT      = 0x00000400, /* don't restart interrupted receive */
            MACH_RCV_VOUCHER        = 0x00000800, /* willing to receive voucher port */
            MACH_RCV_OVERWRITE      = 0x00001000, /* scatter receive (deprecated) */
            MACH_RCV_SYNC_WAIT      = 0x00004000, /* sync waiter waiting for rcv */
            MACH_SEND_ALWAYS        = 0x00010000, /* ignore qlimits - kernel only */
            MACH_SEND_TRAILER       = 0x00020000, /* sender-provided trailer */
            MACH_SEND_NOIMPORTANCE  = 0x00040000, /* msg won't carry importance */
            MACH_SEND_NODENAP       = MACH_SEND_NOIMPORTANCE,
            MACH_SEND_IMPORTANCE    = 0x00080000, /* msg carries importance - kernel only */
            MACH_SEND_SYNC_OVERRIDE = 0x00100000,
        }

        public enum STEAM_IPC : uint
        {
            STEAM_IPC5         = 5,
            GETSTEAMPID        = 11,
            SETSTEAMPID        = 12,
            GETSTEAMPATH       = 13,
            GETSTEAMCLIENTPATH = 14,
            KILLCMD            = 100000,
            GETVERSION         = 100001,
        }

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public unsafe struct steam_ipc_cmd
        {
            public mach_msg_header_t msg;
            public STEAM_IPC msg_type;
            public int arg1;
            //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
            public fixed byte buff[512];
            public mach_msg_trailer_t trailer;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct SteamMachMsgKillSend
        {
            public mach_msg_header_t msg;
            public STEAM_IPC msg_type;
            public int arg1;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct SteamMachMsg5Send
        {
            public mach_msg_header_t msg;
            public int unknown1;
            public int unknown2;
            public int unknown3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct SteamMachMsgVersionSend
        {
            public mach_msg_header_t msg;
            public int unused;
            public int version;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public unsafe struct SteamMachMsgPathSend
        {
            public mach_msg_header_t msg;
            public int unused;
            public int pid;
            //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
            public fixed byte buff[512];
        }

        private string steam_ipc_server = "com.valvesoftware.steam.ipctool";
        private uint steam_ipc_version = 104;

        bool is_initialized = false;

        IntPtr bootstrap_port_addr = IntPtr.Zero;
        uint bootstrap_port = 0;
        IntPtr mach_task_self_addr = IntPtr.Zero;
        uint mach_task_self = 0;

        uint service_rcv_port = 0;

        IntPtr libsystem_kernel = IntPtr.Zero;

        string steam_path = "";
        int steam_pid = Process.GetCurrentProcess().Id;

        public MacOsIpcServer()
        {
            Init();
        }

        private void Init()
        {
            if (OSDetector.IsMacOS() && !is_initialized)
            {
                try
                {
                    libsystem_kernel = dlopen("/usr/lib/system/libsystem_kernel.dylib", RTLD_FLAGS.RTLD_NOW);
                    if (libsystem_kernel != IntPtr.Zero)
                    {
                        bootstrap_port_addr = dlsym(libsystem_kernel, "bootstrap_port");
                        int[] variable = new int[1];
                        Marshal.Copy(bootstrap_port_addr, variable, 0, 1);

                        bootstrap_port = (uint)variable[0];

                        mach_task_self_addr = dlsym(libsystem_kernel, "mach_task_self_");
                        Marshal.Copy(mach_task_self_addr, variable, 0, 1);

                        mach_task_self = (uint)variable[0];

                        dlclose(libsystem_kernel);
                        is_initialized = true;
                        HasIpc = true;
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        override public void StartIpcServer()
        {
            if (OSDetector.IsMacOS())
            {
                if (service_rcv_port == 0)
                {
                    kern_return_t kr = bootstrap_check_in(bootstrap_port, steam_ipc_server, out service_rcv_port);
                    if (kr != kern_return_t.KERN_SUCCESS)
                        return;
                }

                IsStarted = true;
                cts = new CancellationTokenSource();
                Task.Factory.StartNew(IpcServerThread, cts.Token)
                    .ContinueWith(t => { OnIpcServerStopped(); });
            }
        }

        override public void StopIpcServer()
        {
            if (OSDetector.IsMacOS())
            {
                IsStarted = false;
                cts?.Cancel();
            }
        }

        override public unsafe void SendKillIpcServer()
        {
            if (OSDetector.IsMacOS())
            {
                uint service_port;
                uint port;

                kern_return_t kr = bootstrap_look_up(bootstrap_port, steam_ipc_server, out service_port);
                if (kr != kern_return_t.KERN_SUCCESS)
                    return;

                kr = mach_port_allocate(mach_task_self, MACH_PORT_RIGHT.MACH_PORT_RIGHT_RECEIVE, out port);
                if (kr != kern_return_t.KERN_SUCCESS)
                    return;

                SteamMachMsgKillSend msg = new SteamMachMsgKillSend();
                msg.msg.msgh_size = (uint)Marshal.SizeOf(msg);
                msg.msg.msgh_local_port = port;
                msg.msg.msgh_remote_port = service_port;
                msg.msg.msgh_bits = 0x1413;
                msg.msg.msgh_id = steam_ipc_version;
                msg.msg_type = STEAM_IPC.KILLCMD;
                msg.arg1 = Process.GetCurrentProcess().Id;

                GCHandle handle = GCHandle.Alloc(msg, GCHandleType.Pinned);
                kr = mach_msg(handle.AddrOfPinnedObject(), MACH_MSG_OPTION.MACH_SEND_MSG | MACH_MSG_OPTION.MACH_SEND_TIMEOUT, msg.msg.msgh_size, 0, 0, 2000, 0);
                handle.Free();

                if(kr == kern_return_t.KERN_SUCCESS)
                {
                    IntPtr launch_dict;
                    IntPtr launch_str;
                    IntPtr res;
                    
                    launch_dict = launch_data_alloc(LAUNCH_DATA_TYPE.LAUNCH_DATA_DICTIONARY);
                    launch_str = launch_data_new_string(steam_ipc_server);
                    launch_data_dict_insert(launch_dict, launch_str, "StopJob");
                    
                    res = launch_msg(launch_dict);
                    
                    launch_data_free(launch_dict);
                    if (res != IntPtr.Zero)
                        launch_data_free(res);
                }

                mach_port_deallocate(mach_task_self, port);

                Thread.Sleep(1000);
            }
        }

        private unsafe void IpcServerThread(object tkn)
        {
            CancellationToken token = (CancellationToken)tkn;
            OnIpcServerStarted();
            while (!token.IsCancellationRequested)
            {
                steam_ipc_cmd ipc_cmd = new steam_ipc_cmd();
                ipc_cmd.msg.msgh_size = (uint)Marshal.SizeOf(ipc_cmd);
                ipc_cmd.msg.msgh_local_port = service_rcv_port;

                GCHandle ipc_handle = GCHandle.Alloc(ipc_cmd, GCHandleType.Pinned);

                if (mach_msg(ipc_handle.AddrOfPinnedObject(), MACH_MSG_OPTION.MACH_RCV_MSG | MACH_MSG_OPTION.MACH_RCV_TIMEOUT, 0, ipc_cmd.msg.msgh_size, ipc_cmd.msg.msgh_local_port, 2000, 0) == kern_return_t.KERN_SUCCESS)
                {
                    ipc_cmd = (steam_ipc_cmd)Marshal.PtrToStructure(ipc_handle.AddrOfPinnedObject(), typeof(steam_ipc_cmd));

                    switch (ipc_cmd.msg_type)
                    {
                        case STEAM_IPC.STEAM_IPC5:
                            {
                                SteamMachMsg5Send buff = new SteamMachMsg5Send();

                                buff.msg.msgh_size = (uint)Marshal.SizeOf(buff);
                                buff.msg.msgh_local_port = 0;
                                buff.msg.msgh_remote_port = ipc_cmd.msg.msgh_remote_port;
                                buff.msg.msgh_bits = (ipc_cmd.msg.msgh_bits >> 8) & 0x1F;
                                buff.msg.msgh_id = steam_ipc_version;
                                buff.unknown1 = 0;
                                buff.unknown2 = 0;

                                GCHandle handle = GCHandle.Alloc(buff, GCHandleType.Pinned);
                                mach_msg(handle.AddrOfPinnedObject(), MACH_MSG_OPTION.MACH_SEND_MSG | MACH_MSG_OPTION.MACH_SEND_TIMEOUT, buff.msg.msgh_size, 0, 0, 2000, 0);
                                handle.Free();
                            }
                            break;

                        case STEAM_IPC.GETVERSION:
                            {
                                SteamMachMsgVersionSend buff = new SteamMachMsgVersionSend();

                                buff.msg.msgh_size = (uint)Marshal.SizeOf(buff);
                                buff.msg.msgh_local_port = 0;
                                buff.msg.msgh_remote_port = ipc_cmd.msg.msgh_remote_port;
                                buff.msg.msgh_bits = (ipc_cmd.msg.msgh_bits >> 8) & 0x1F;
                                buff.msg.msgh_id = steam_ipc_version;
                                buff.version = (int)steam_ipc_version;

                                GCHandle handle = GCHandle.Alloc(buff, GCHandleType.Pinned);
                                mach_msg(handle.AddrOfPinnedObject(), MACH_MSG_OPTION.MACH_SEND_MSG | MACH_MSG_OPTION.MACH_SEND_TIMEOUT, buff.msg.msgh_size, 0, 0, 2000, 0);
                                handle.Free();
                            }
                            break;

                        case STEAM_IPC.KILLCMD:
                            break;

                        case STEAM_IPC.GETSTEAMPID:
                            break;

                        case STEAM_IPC.SETSTEAMPID:
                            {
                                byte[] arr = new byte[512];
                                Marshal.Copy((IntPtr)ipc_cmd.buff, arr, 0, 512);
                                steam_path = Encoding.UTF8.GetString(arr);
                                steam_pid = ipc_cmd.arg1;
                            }
                            break;

                        case STEAM_IPC.GETSTEAMPATH:
                            {
                                SteamMachMsgPathSend buff = new SteamMachMsgPathSend();

                                buff.msg.msgh_size = (uint)Marshal.SizeOf(buff);
                                buff.msg.msgh_local_port = 0;
                                buff.msg.msgh_remote_port = ipc_cmd.msg.msgh_remote_port;
                                buff.msg.msgh_bits = (ipc_cmd.msg.msgh_bits >> 8) & 0x1F;
                                buff.msg.msgh_id = steam_ipc_version;
                                buff.pid = steam_pid;
                                //buff.buff = steam_path;

                                GCHandle handle = GCHandle.Alloc(buff, GCHandleType.Pinned);
                                mach_msg(handle.AddrOfPinnedObject(), MACH_MSG_OPTION.MACH_SEND_MSG | MACH_MSG_OPTION.MACH_SEND_TIMEOUT, buff.msg.msgh_size, 0, 0, 2000, 0);
                                handle.Free();
                            }
                            break;

                        case STEAM_IPC.GETSTEAMCLIENTPATH:
                            {
                                SteamMachMsgPathSend buff = new SteamMachMsgPathSend();

                                buff.msg.msgh_size = (uint)Marshal.SizeOf(buff);
                                buff.msg.msgh_local_port = 0;
                                buff.msg.msgh_remote_port = ipc_cmd.msg.msgh_remote_port;
                                buff.msg.msgh_bits = (ipc_cmd.msg.msgh_bits >> 8) & 0x1F;
                                buff.msg.msgh_id = steam_ipc_version;
                                buff.pid = steam_pid;

                                string apifolder = OSFuncs.GetMacOSEmuApiFolder(false);
                                byte[] apistr = Encoding.UTF8.GetBytes(apifolder);

                                for (int i = 0; i < (apistr.Length > 511 ? 511 : apistr.Length); ++i)
                                {
                                    buff.buff[i] = apistr[i];
                                }
                                buff.buff[apistr.Length] = (byte)0;

                                GCHandle handle = GCHandle.Alloc(buff, GCHandleType.Pinned);
                                mach_msg(handle.AddrOfPinnedObject(), MACH_MSG_OPTION.MACH_SEND_MSG | MACH_MSG_OPTION.MACH_SEND_TIMEOUT, buff.msg.msgh_size, 0, 0, 2000, 0);
                                handle.Free();
                            }
                            break;
                    }
                }
                ipc_handle.Free();
            }
            token.ThrowIfCancellationRequested();
        }
    }
}
