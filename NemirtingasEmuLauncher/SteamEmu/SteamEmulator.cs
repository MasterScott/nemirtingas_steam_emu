/* Copyright (C) 2019-2020 Nemirtingas
   This file is part of the NemirtingasEmuLauncher Launcher

   The NemirtingasEmuLauncher Launcher is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   The NemirtingasEmuLauncher Launcher is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the NemirtingasEmuLauncher; if not, see
   <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Net;

using Microsoft.Win32;

using OSUtility;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Configuration;
using SkiaSharp;
using DynamicData;

namespace NemirtingasEmuLauncher
{
    static class SteamEmulator
    {
        private static int? steamPid = 0;
        private static string steamClientDll = "";
        private static string steamClientDll64 = "";

        private static string _launcherFolder;
        public static string LauncherFolder => _launcherFolder ??= Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);

        private static string _launcherAppsCacheFolder;
        public static string LauncherAppsCacheFolder => _launcherAppsCacheFolder ??= Path.Combine(LauncherFolder, "apps_cache_infos");

        private static string _gameEmuFolder;
        public static string GameEmuFolder => _gameEmuFolder ??= Path.Combine(LauncherFolder, "games");

        private static List<Process> emuGamesProcess = new List<Process>();

        public static IpcServer IpcServer { get; }

        public static readonly string EmuJsonName = "NemirtingasSteamEmu.json";

        public static EmuStringsConfig EmuConfigs { get; private set; } = new EmuStringsConfig();

        static SteamEmulator()
        {
            var configBuilder = new ConfigurationBuilder()
                .AddJsonFile(Path.Combine(SteamEmulator.LauncherFolder, "appsettings.json"), true, true).Build();

            configBuilder.Bind(EmuConfigs);

            switch (OSDetector.GetOS())
            {
                case OsId.MacOSX: IpcServer = new MacOsIpcServer(); break;
                default: IpcServer = new IpcServer(); break;
            }
        }

        private static ApiResult getGameSavePath(GameConfig app)
        {
            SteamID steam_id = getAppUserId(app);
            if (steam_id == null || !steam_id.IsDesktopUser())
            {
                return new ApiResult { Success = false, Message = "Invalid SteamID: not a Desktop user" };
            }

            string save_path;
            if (string.IsNullOrWhiteSpace(app.SavePath))
            {
                save_path = GameEmuFolder;
            }
            else if (app.SavePath == "appdata")
            {
                save_path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            }
            else
            {
                save_path = Path.Combine(app.StartFolder, app.SavePath);
            }

            string game_folder = Path.Combine(save_path, "NemirtingasSteamEmu", steam_id.ToString(), app.AppId.ToString());
            Directory.CreateDirectory(game_folder);
            return new ApiResult { Success = true, Message = game_folder };
        }

        public static void LoadSave(ICollection<GameConfig> app_list)
        {
            app_list.Clear();
            try
            {
                SavedConf save = new SavedConf();
                var xmlserializer = new XmlSerializer(save.GetType());

                using (FileStream file = File.Open(Path.Combine(LauncherFolder, "NemirtingasEmuLauncher.cfg"), FileMode.Open))
                {
                    save = (SavedConf)xmlserializer.Deserialize(file);

                    foreach (var app in save.apps)
                    {
                        if (app != null)
                        {
                            app_list.Add(app);
                        }
                    }
                    EmuConfig.DefaultEmuConfig = save.config;
                }
            }
            catch (Exception)
            {
            }
        }

        public static void Save(IEnumerable<GameConfig> app_list)
        {
            SavedConf save = new SavedConf();
            var xmlserializer = new XmlSerializer(save.GetType());
            var stringWriter = new StringWriter();

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = ("  ");
            settings.OmitXmlDeclaration = true;

            save.apps.AddRange(app_list);
            save.config = EmuConfig.DefaultEmuConfig;

            using (var writer = XmlWriter.Create(stringWriter, settings))
            {
                xmlserializer.Serialize(writer, save);
                using (FileStream file = File.Open(Path.Combine(LauncherFolder, "NemirtingasEmuLauncher.cfg"), FileMode.Create))
                {
                    byte[] datas = new UTF8Encoding(false).GetBytes(stringWriter.ToString());
                    file.Write(datas, 0, datas.Length);
                }
            }
        }

        private static string getAppUsername(GameConfig app)
        {
            if (string.IsNullOrWhiteSpace(app.EmuConfig.UserName))
            {
                if (string.IsNullOrWhiteSpace(EmuConfig.DefaultEmuConfig.UserName))
                {
                    return null;
                }
                return EmuConfig.DefaultEmuConfig.UserName;
            }
            else
            {
                return app.EmuConfig.UserName;
            }
        }

        private static SteamID getAppUserId(GameConfig app)
        {
            if (app.EmuConfig.SteamId == null || !app.EmuConfig.SteamId.IsDesktopUser())
            {
                if (EmuConfig.DefaultEmuConfig.SteamId == null || !EmuConfig.DefaultEmuConfig.SteamId.IsDesktopUser())
                {
                    string username = getAppUsername(app);
                    if (string.IsNullOrWhiteSpace(username))
                        return null;

                    return new SteamID(SteamID.GenerateIdFromName(username));
                }
                else
                {
                    return EmuConfig.DefaultEmuConfig.SteamId;
                }
            }
            else
            {
                return app.EmuConfig.SteamId;
            }
        }

        private static ApiResult BuildEmuJsonCfg(GameConfig game_app, out JObject emu_cfg)
        {
            string os_folder = OSFuncs.GetEmuApiFolder(game_app.UseX64);

            emu_cfg = new JObject();

            if (string.IsNullOrWhiteSpace(os_folder))
            {
                return new ApiResult { Success = false, Message = "Unable to find emulator folder" };
            }

            string emu_path = os_folder + OSFuncs.GetSteamAPIName(game_app.UseX64);
            if (!File.Exists(emu_path))
            {
                return new ApiResult { Success = false, Message = "Cannot find steam emulator: " + emu_path };
            }

            if (string.IsNullOrWhiteSpace(game_app.EmuConfig.Language))
            {
                if(string.IsNullOrWhiteSpace(EmuConfig.DefaultEmuConfig.Language))
                {
                    // This should never happen
                    return new ApiResult { Success = false, Message = "Invalid language: must not be empty" };
                }
                emu_cfg["language"] = EmuConfig.DefaultEmuConfig.Language;
            }
            else
            {
                emu_cfg["language"] = game_app.EmuConfig.Language;
            }

            if (string.IsNullOrWhiteSpace(game_app.EmuConfig.LogLevel))
            {
                if (string.IsNullOrWhiteSpace(EmuConfig.DefaultEmuConfig.LogLevel))
                {
                    // This should never happen
                    return new ApiResult { Success = false, Message = "Invalid LogLevel: must not be empty" };
                }
                emu_cfg["log_level"] = EmuConfig.DefaultEmuConfig.LogLevel;
            }
            else
            {
                emu_cfg["log_level"] = game_app.EmuConfig.LogLevel;
            }

            if (string.IsNullOrWhiteSpace(game_app.EmuConfig.UserName))
            {
                if (string.IsNullOrWhiteSpace(EmuConfig.DefaultEmuConfig.UserName))
                {
                    return new ApiResult { Success = false, Message = "Invalid username: must not be empty" };
                }
                emu_cfg["username"] = EmuConfig.DefaultEmuConfig.UserName;
            }
            else
            {
                emu_cfg["username"] = game_app.EmuConfig.UserName;
            }

            if (game_app.EmuConfig.SteamId == null || !game_app.EmuConfig.SteamId.IsDesktopUser())
            {
                if (EmuConfig.DefaultEmuConfig.SteamId == null)
                {
                    emu_cfg["steamid"] = SteamID.GenerateIdFromName(emu_cfg.Value<string>("username"));
                }
                else if(!EmuConfig.DefaultEmuConfig.SteamId.IsDesktopUser())
                {
                    return new ApiResult { Success = false, Message = "Please enter a desktop userid" };
                }
                else
                {
                    emu_cfg["steamid"] = EmuConfig.DefaultEmuConfig.SteamId.Id;
                }
            }
            else
            {
                emu_cfg["steamid"] = game_app.EmuConfig.SteamId.Id;
            }

            if (string.IsNullOrWhiteSpace(game_app.AppName))
            {
                return new ApiResult { Success = false, Message = "Invalid game name: must not be empty" };
            }
            emu_cfg["gamename"] = game_app.AppName;

            emu_cfg["savepath"] = string.IsNullOrWhiteSpace(game_app.SavePath) ? GameEmuFolder + Path.DirectorySeparatorChar : game_app.SavePath;
            if(game_app.AppId == 0)
            {
                return new ApiResult { Success = false, Message = "Invalid appid: Must not be 0" };
            }
            emu_cfg["appid"] = game_app.AppId;

            emu_cfg["beta"] = game_app.Beta;
            emu_cfg["beta_name"] = game_app.BetaName;
            emu_cfg["languages"] = string.Join(",", game_app.Languages);
            emu_cfg["disable_online_networking"] = game_app.DisableOnlineNetworking;

            emu_cfg["enable_overlay"] = game_app.EmuConfig.EnableOverlay ?? EmuConfig.DefaultEmuConfig.EnableOverlay;
            emu_cfg["unlock_dlcs"] = game_app.EmuConfig.UnlockDlcs ?? EmuConfig.DefaultEmuConfig.UnlockDlcs;
            
            //_emu_cfg[""] = ;
            //_emu_cfg[""] = app.custom_broadcasts.Count;

            return new ApiResult { Success = true };
        }

        private static ApiResult BuildEmuDlcs(GameConfig app, out JObject emu_dlcs)
        {
            emu_dlcs = new JObject();

            foreach (var dlc in app.Dlcs)
            {
                JObject emu_dlc = new JObject();
                emu_dlc.Add("dlc_name", dlc.Name);
                emu_dlc.Add("enabled", dlc.Enabled);
                //emu_dlc.Add("icon_gray", dlc.GrayIcon);
                //emu_dlc.Add("icon", dlc.Icon);
                try
                {
                    emu_dlcs.Add(dlc.DlcId.ToString(), emu_dlc);
                }
                catch(Exception)
                {
                    return new ApiResult { Success = false, Message = "Dlc " + dlc.DlcId.ToString() + " already present" };
                }
            }

            return new ApiResult { Success = true };
        }

        private static RegistryKey CreateOrOpenKey(string keyPath)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(keyPath, true);
            if (key == null)
            {
                string[] split = keyPath.Split('\\');
                key = CreateOrOpenKey(string.Join("\\", split.Take(split.Length - 1)));
                key.CreateSubKey(split.Last());
                key.Close();
            }
            return Registry.CurrentUser.OpenSubKey(keyPath, true);
        }

        private static ApiResult SetupSteamEmu(GameConfig app)
        {
            try
            {
                if (OSDetector.IsWindows())
                {
                    RegistryKey key = CreateOrOpenKey(@"Software\Valve\Steam\ActiveProcess");

                    if (key == null)
                    {
                        return new ApiResult { Success = false, Message = @"Cannot setup registry keys in HKEY_CURRENT_USER\Software\Valve\Steam\ActiveProcess" };
                    }

                    if (steamPid == 0)
                    {
                        steamPid = (int?)key.GetValue("pid") ?? (int?)0;
                        steamClientDll = key.GetValue("SteamClientDll")?.ToString() ?? string.Empty;
                        steamClientDll64 = key.GetValue("SteamClientDll64")?.ToString() ?? string.Empty;
                    }

                    key.SetValue("pid", Process.GetCurrentProcess().Id, RegistryValueKind.DWord);
                    key.SetValue(app.UseX64 ? "SteamClientDll64" : "SteamClientDll", OSFuncs.GetEmuApiFolder(app.UseX64) + OSFuncs.GetSteamAPIName(app.UseX64), RegistryValueKind.String);

                    key.Close();
                }
                else if (OSDetector.IsLinux())
                {
                    string emu_library_folder = OSFuncs.GetEmuApiFolder(app.UseX64);

                    if (!Directory.Exists(Path.Combine(emu_library_folder, ".steam")))
                    {
                        using (var p = Process.Start(new ProcessStartInfo
                        {
                            CreateNoWindow = false,
                            UseShellExecute = false,
                            FileName = "ln",
                            WindowStyle = ProcessWindowStyle.Normal,
                            Arguments = "-s . " + Path.Combine(emu_library_folder, ".steam")
                        }))
                        {
                            p.WaitForExit();
                        }
                    }
                    if (!Directory.Exists(Path.Combine(emu_library_folder, ".steam", app.UseX64 ? "sdk64" : "sdk32")))
                    {
                        using (var p = Process.Start(new ProcessStartInfo
                        {
                            CreateNoWindow = false,
                            UseShellExecute = false,
                            FileName = "ln",
                            WindowStyle = ProcessWindowStyle.Normal,
                            Arguments = "-s . " + Path.Combine(emu_library_folder, ".steam", app.UseX64 ? "sdk64" : "sdk32")
                        }))
                        {
                            p.WaitForExit();
                        }
                    }

                    Environment.SetEnvironmentVariable("HOME", emu_library_folder);
                    using (StreamWriter streamWriter = new StreamWriter(new FileStream(Path.Combine(emu_library_folder, "steam.pid"), FileMode.Create), Encoding.ASCII))
                    {
                        streamWriter.Write(Process.GetCurrentProcess().Id);
                    }
                    // On linux, steam_api.so checks for steamclient.so in $HOME/.steam/sdk(32|64)
                    // So if we set the HOME env variable before starting the game, it should use our steamclient.so
                }
            }
            catch(Exception e)
            {
                return new ApiResult { Success = false, Message = e.Message };
            }

            return new ApiResult { Success = true };
        }

        public static ApiResult ShowGameEmuFolder(GameConfig app)
        {
            ApiResult res = getGameSavePath(app);

            if(!res.Success)
            {
                return res;
            }

            try
            {
                if (OSDetector.IsWindows())
                    Process.Start("explorer.exe", res.Message);
                else if (OSDetector.IsLinux())
                    Process.Start("nautilus", res.Message);
                else if (OSDetector.IsMacOS())
                    Process.Start("open", res.Message);
            }
            catch
            {
                return new ApiResult { Success = false, Message = "Folder: " + res.Message };
            }

            return new ApiResult { Success = true };
        }

        public static ApiResult StartGame(GameConfig app)
        {
            if (OSDetector.IsMacOS())
            {
                if (!IpcServer.IsStarted)
                {
                    return new ApiResult { Success = false, Message = "Please start the IPCServer before trying to start a game." };
                }
            }

            JObject emu_cfg;
            ApiResult res = BuildEmuJsonCfg(app, out emu_cfg);
            if(!res.Success)
            {
                return res;
            }

            JObject emu_dlcs;
            res = BuildEmuDlcs(app, out emu_dlcs);
            if (!res.Success)
            {
                return res;
            }

            res = SetupSteamEmu(app);
            if (!res.Success)
            {
                return res;
            }

            res = getGameSavePath(app);
            if(!res.Success)
            {
                return res;
            }

            string dlcs_config_file = Path.Combine(res.Message, GameConfig.AppDlcFile);
            try
            {
                using (StreamWriter streamWriter = new StreamWriter(new FileStream(dlcs_config_file, FileMode.Create), new UTF8Encoding(false)))
                {
                    string buffer = Newtonsoft.Json.JsonConvert.SerializeObject(emu_dlcs, Newtonsoft.Json.Formatting.Indented);
                    streamWriter.Write(buffer);
                }
            }
            catch(Exception)
            {
                res.Success = false;
                res.Message = "Failed to create dlcs config: " + dlcs_config_file;
                return res;
            }

            string json_cfg = Path.Combine(Path.GetDirectoryName(app.FullPath), EmuJsonName);
            try
            {
                using (StreamWriter streamWriter = new StreamWriter(new FileStream(json_cfg, FileMode.Create), new UTF8Encoding(false)))
                {
                    string buffer = Newtonsoft.Json.JsonConvert.SerializeObject(emu_cfg, Newtonsoft.Json.Formatting.Indented);
                    streamWriter.Write(buffer);
                }
            }
            catch(Exception)
            {
                res.Success = false;
                res.Message = "Failed to create emulator config: " + json_cfg;
                return res;
            }

            ProcessStartInfo psi = new ProcessStartInfo();

            psi.CreateNoWindow = false;
            psi.UseShellExecute = false;
            psi.FileName = app.FullPath;
            psi.WindowStyle = ProcessWindowStyle.Normal;
            psi.Arguments = app.Parameters;
            psi.WorkingDirectory = app.StartFolder;

            psi.EnvironmentVariables.Add("SteamAppId", app.AppId.ToString());
            psi.EnvironmentVariables.Add("SteamGameId", app.AppId.ToString());

            foreach (EnvVar var in app.EnvVars)
            {
                psi.EnvironmentVariables.Add(var.Key, var.Value);
            }

            Process p = new Process();
            p.EnableRaisingEvents = true;
            p.StartInfo = psi;
            p.Exited += OnProcessExited;
            emuGamesProcess.Add(p);

            p.Start();

            return new ApiResult { Success = true };
        }

        private static void OnProcessExited(object sender, EventArgs e)
        {
            Process p = (Process)sender;
            emuGamesProcess.Remove(p);
            if (emuGamesProcess.Count == 0)
            {
                RestoreSteam();
            }
        }

        public static void RestoreSteam()
        {
            if (steamPid != 0)
            {
                if (OSDetector.IsWindows())
                {
                    RegistryKey key = CreateOrOpenKey(@"Software\Valve\Steam\ActiveProcess");

                    if (key == null)
                        return;

                    key.SetValue("pid", steamPid, RegistryValueKind.DWord);
                    key.SetValue("SteamClientDll64", steamClientDll64, RegistryValueKind.String);
                    key.SetValue("SteamClientDll", steamClientDll, RegistryValueKind.String);

                    steamPid = 0;
                    steamClientDll = "";
                    steamClientDll64 = "";
                }
            }
        }

        public static ApiResult CreateShortcut(GameConfig app)
        {
            if (!OSDetector.IsWindows())
            {
                return new ApiResult { Success = false, Message = "This feature is only available on Windows for the moment" };
            }

            string launcherPath = Path.GetFullPath(Environment.GetCommandLineArgs()[0]);
            string desktopDir = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + Path.DirectorySeparatorChar;


            OSUtility.Shortcut.Create(desktopDir + app.AppName, launcherPath, "--game-guid " + app.GameGuid.ToString(),
                Path.GetDirectoryName(launcherPath), "Starts " + app.AppName + " with the steam emulator",
                string.Empty, app.FullPath);

            return new ApiResult { Success = true };
        }

        public static ApiResult GenerateGameAchievements(GameConfig app)
        {
            ApiResult res = CheckWebApiKey(app, out string webapi_key);
            if (!res.Success)
            {
                return res;
            }

            string language = app.EmuConfig.Language;
            if (string.IsNullOrWhiteSpace(language))
                language = EmuConfig.DefaultEmuConfig.Language;

            res = getGameSavePath(app);
            if(!res.Success)
            {
                return res;
            }
            string save_path = res.Message;
            string icon_dir = Path.Combine(save_path, "achievements_cache");
            try
            {
                Directory.CreateDirectory(icon_dir);
            }
            catch(Exception e)
            {
                res.Success = false;
                res.Message = "Failed to create " + icon_dir + ": " + e.Message;
                return res;
            }

            try
            {
                string url = "http://api.steampowered.com/ISteamUserStats/GetSchemaForGame/v2/?l=" + language + "&key=";
                url += webapi_key + "&appid=" + app.AppId.ToString();
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        return new ApiResult { Success = false, Message = "Failed to get achievements definition (wrong webapi key ?)" };
                    }

                    using (Stream sresult = response.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(sresult))
                        {
                            string buffer = streamReader.ReadToEnd();

                            var json = JObject.Parse(buffer);

                            foreach (var achievement in json["game"]["availableGameStats"]["achievements"])
                            {
                                if (!Directory.Exists(icon_dir))
                                {
                                    Directory.CreateDirectory(icon_dir);
                                }
                                using (WebClient wc = new WebClient())
                                {
                                    wc.DownloadFile(achievement.Value<string>("icon"), Path.Combine(icon_dir, achievement.Value<string>("name") + ".jpg"));
                                    wc.DownloadFile(achievement.Value<string>("icongray"), Path.Combine(icon_dir, achievement.Value<string>("name") + "_gray.jpg"));
                                }
                            }

                            using (StreamWriter streamWriter = new StreamWriter(new FileStream(Path.Combine(save_path, "db_achievments.json"), FileMode.Create), new UTF8Encoding(false)))
                            {
                                buffer = Newtonsoft.Json.JsonConvert.SerializeObject(json["game"]["availableGameStats"].Value<Newtonsoft.Json.Linq.JArray>("achievements"), Newtonsoft.Json.Formatting.Indented);
                                streamWriter.Write(buffer);
                            }
                        }//using (StreamReader streamReader = new StreamReader(sresult))
                    }//using (Stream sresult = response.GetResponseStream())
                }//using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            }
            catch(Exception e)
            {
                res.Success = false;
                res.Message = "Failed to get achievements: " + e.Message;
                return res;
            }

            return new ApiResult { Success = true };
        }

        public static ApiResult GenerateGameDLCs(GameConfig app)
        {
            string webapi_key;
            ApiResult res = CheckWebApiKey(app, out webapi_key);

            if (!res.Success)
            {
                return res;
            }

            string language = app.EmuConfig.Language;
            if (string.IsNullOrWhiteSpace(language))
                language = EmuConfig.DefaultEmuConfig.Language;

            res = getGameSavePath(app);
            if (!res.Success)
            {
                return res;
            }

            string save_path = res.Message;

            string url = "https://store.steampowered.com/api/appdetails/?appids=";
            url += app.AppId.ToString() + "&l=" + language;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        return new ApiResult { Success = false, Message = "Failed to get achievements definition (wrong webapi key ?)" };
                    }

                    StreamReader streamReader = new StreamReader(response.GetResponseStream());

                    List<Dlc> app_dlcs = new List<Dlc>();

                    var json = Newtonsoft.Json.Linq.JObject.Parse(streamReader.ReadToEnd());
                    if (json[app.AppId.ToString()].Value<bool>("success") == true)
                    {
                        Newtonsoft.Json.Linq.JArray dlcs = json[app.AppId.ToString()]["data"].Value<Newtonsoft.Json.Linq.JArray>("dlc");
                        if (dlcs != null)
                        {
                            url = "https://store.steampowered.com/api/appdetails/?l=" + language + "&appids=";
                            foreach (ulong dlcid in dlcs)
                            {
                                request = (HttpWebRequest)WebRequest.Create(url + dlcid.ToString());
                                try
                                {
                                    using (HttpWebResponse dlc_response = (HttpWebResponse)request.GetResponse())
                                    {
                                        streamReader = new StreamReader(dlc_response.GetResponseStream());
                                        json = Newtonsoft.Json.Linq.JObject.Parse(streamReader.ReadToEnd());
                                        if (json[dlcid.ToString()].Value<bool>("success") == true)
                                        {
                                            app_dlcs.Add(new Dlc
                                            {
                                                DlcId = dlcid,
                                                Enabled = true,
                                                Name = json[dlcid.ToString()]["data"].Value<string>("name")
                                                // TODO: add GrayIcon and Icon
                                            });
                                        }
                                    }
                                }
                                catch (Exception)
                                {
                                    // Failed to parse this dlc, ignore 
                                }
                            }

                            app.Dlcs.Clear();
                            app.Dlcs.AddRange(app_dlcs);
                        }
                        else
                        {
                            return new ApiResult { Success = false, Message = "No dlcs for this game" };
                        }
                    }
                    else
                    {
                        return new ApiResult { Success = false, Message = "Error, is appid wrong ?" };
                    }
                }
            }
            catch (Exception)
            {
                return new ApiResult { Success = false, Message = "Error, is appid wrong ?" };
            }

            return new ApiResult { Success = true };
        }

        private static ApiResult CheckWebApiKey(GameConfig app, out string webApiKey)
        {
            webApiKey = app.EmuConfig.WebApiKey;
            if (string.IsNullOrWhiteSpace(webApiKey))
            {
                webApiKey = EmuConfig.DefaultEmuConfig.WebApiKey;
                if (string.IsNullOrWhiteSpace(webApiKey))
                {
                    return new ApiResult { Success = false, Message = "Can't generate items file, webapi key is missing.\n\nSee https://steamcommunity.com/dev/apikey to get yours." };
                }
            }
            return new ApiResult { Success = true };
        }

        public static ApiResult GenerateGameItems(GameConfig app)
        {
            ApiResult res = CheckWebApiKey(app, out string webapi_key);
            if (!res.Success)
            {
                return res;
            }
            res = getGameSavePath(app);
            if (!res.Success)
            {
                return res;
            }
            string save_path = res.Message;

            string url = "https://api.steampowered.com/IInventoryService/GetItemDefMeta/v1?key=";
            url += webapi_key + "&appid=" + app.AppId.ToString();

            Newtonsoft.Json.Linq.JObject json;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    return new ApiResult { Success = false, Message = "Bad HTML response, wrong API key or the game doesn't have items" };
                }

                Stream sresult = response.GetResponseStream();
                using (StreamReader streamReader = new StreamReader(sresult))
                {
                    json = Newtonsoft.Json.Linq.JObject.Parse(streamReader.ReadToEnd());
                }
            }

            if (string.IsNullOrWhiteSpace(json["response"].Value<string>("digest")))
            {// No items
                return new ApiResult { Success = false, Message = "The game doesn't have items" };
            }
            url = "https://api.steampowered.com/IGameInventory/GetItemDefArchive/v0001?appid=";
            url += app.AppId.ToString() + "&digest=" + json["response"].Value<string>("digest");

            request = (HttpWebRequest)WebRequest.Create(url);
            request.Headers.Add("Accept-encoding:gzip, deflate, br");
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {// No items
                    return new ApiResult { Success = false, Message = "The game doesn't have items" };
                }

                Stream sresult = response.GetResponseStream();
                StreamReader streamReader = new StreamReader(sresult);

                var items = Newtonsoft.Json.Linq.JArray.Parse(streamReader.ReadToEnd());
                using (StreamWriter streamWriter = new StreamWriter(new FileStream(Path.Combine(save_path, "db_inventory.json"), FileMode.Create), new UTF8Encoding(false)))
                {
                    streamWriter.Write(Newtonsoft.Json.JsonConvert.SerializeObject(items, Newtonsoft.Json.Formatting.Indented));
                }
            }

            return new ApiResult { Success = true };
        }

        public static ApiResult GenerateGameInfos(GameConfig app, bool clear_cache = false)
        {
            JObject json_cache = null;

            string app_cache_directory = Path.Combine(SteamEmulator.LauncherAppsCacheFolder, app.AppId.ToString());
            string app_cache_file = Path.Combine(app_cache_directory, "infos.json");
            string app_cache_image = Path.Combine(app_cache_directory, "background.jpg");

            if (!clear_cache && File.Exists(app_cache_file))
            {// Try to read the cache file
                using (StreamReader streamReader = new StreamReader(File.OpenRead(app_cache_file)))
                {
                    try
                    {
                        json_cache = JObject.Parse(streamReader.ReadToEnd());
                    }
                    catch(Exception)
                    { }
                }
            }

            if (json_cache == null)
            {// Clear cache or can't find the cache file
                string url = "https://store.steampowered.com/api/appdetails/?l=english&appids=" + app.AppId.ToString();

                JObject json;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        return new ApiResult { Success = false, Message = "Failed to connect to " + url };
                    }

                    Stream sresult = response.GetResponseStream();
                    using (StreamReader streamReader = new StreamReader(sresult))
                    {
                        json = JObject.Parse(streamReader.ReadToEnd());
                    }
                }

                if (json[app.AppId.ToString()].Value<bool>("success") == false)
                {
                    return new ApiResult { Success = false, Message = "Invalid response from " + url };
                }

                request = (HttpWebRequest)WebRequest.Create(json[app.AppId.ToString()]["data"].Value<string>("header_image"));
                request.Headers.Add("Accept-encoding:gzip, deflate, br");
                request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        SKBitmap img = SKBitmap.Decode(response.GetResponseStream());
                        if (!Directory.Exists(app_cache_directory))
                        {
                            Directory.CreateDirectory(app_cache_directory);
                        }
                        // Qualite de 0 - 100
                        using (SKWStream write_stream = new SKFileWStream(app_cache_image))
                        {
                            if (!SKPixmap.Encode(write_stream, img, SKEncodedImageFormat.Jpeg, 90))
                            {
                                try
                                {
                                    File.Delete(app_cache_image);
                                }
                                catch (Exception)
                                { }
                            }
                        }
                    }
                }

                string languages = json[app.AppId.ToString()]["data"].Value<string>("supported_languages");
                int end_index = languages.IndexOf("<br>");
                if (end_index != -1)
                {
                    languages = languages.Remove(end_index);
                }

                languages = languages.Replace("<strong>*</strong>", "");
                languages = languages.Replace(", ", ",");
                languages = languages.Replace("Portuguese - Brazil", "Portuguese");
                languages = languages.Replace("Spanish - Spain", "Spanish");
                languages = languages.Replace("Spanish - Latin America", "Latam");
                languages = languages.Replace("Traditional Chinese", "tchinese");
                languages = languages.Replace("Simplified Chinese", "schinese");
                languages = languages.ToLower();
                languages = languages.Trim();

                json_cache = new JObject();
                json_cache["name"] = json[app.AppId.ToString()]["data"].Value<string>("name");
                json_cache["languages"] = JArray.FromObject(languages.Split(','));

                using (StreamWriter streamWriter = new StreamWriter(new FileStream(app_cache_file, FileMode.Create), new UTF8Encoding(false)))
                {
                    string buffer = Newtonsoft.Json.JsonConvert.SerializeObject(json_cache, Newtonsoft.Json.Formatting.Indented);
                    streamWriter.Write(buffer);
                }
            }

            app.AppName = json_cache.Value<string>("name");
            app.Languages.Clear();
            app.Languages.AddRange(json_cache["languages"].ToObject<List<string>>());

            return new ApiResult { Success = true };
        }
    }
}
