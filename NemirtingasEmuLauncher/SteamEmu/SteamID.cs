/* Copyright (C) 2019-2020 Nemirtingas
   This file is part of the NemirtingasEmuLauncher Launcher

   The NemirtingasEmuLauncher Launcher is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   The NemirtingasEmuLauncher Launcher is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the NemirtingasEmuLauncher; if not, see
   <http://www.gnu.org/licenses/>.
 */

using System;
using System.Xml.Serialization;

namespace NemirtingasEmuLauncher
{
    public class SteamID
    {
        public enum EUniverse : byte
        {
            k_EUniverseInvalid = 0,
            k_EUniversePublic = 1,
            k_EUniverseBeta = 2,
            k_EUniverseInternal = 3,
            k_EUniverseDev = 4,
            // k_EUniverseRC = 5,				// no such universe anymore
            k_EUniverseMax
        }

        public enum EAccountType : byte
        {
            k_EAccountTypeInvalid = 0,
            k_EAccountTypeIndividual = 1,       // single user account
            k_EAccountTypeMultiseat = 2,        // multiseat (e.g. cybercafe) account
            k_EAccountTypeGameServer = 3,       // game server account
            k_EAccountTypeAnonGameServer = 4,   // anonymous game server account
            k_EAccountTypePending = 5,          // pending
            k_EAccountTypeContentServer = 6,    // content server
            k_EAccountTypeClan = 7,
            k_EAccountTypeChat = 8,
            k_EAccountTypeConsoleUser = 9,      // Fake SteamID for local PSN account on PS3 or Live account on 360, etc.
            k_EAccountTypeAnonUser = 10,

            // Max of 16 items in this field
            k_EAccountTypeMax
        }

        [Flags]
        public enum EInstanceFlags : uint
        {
            k_SteamAccountInstanceMask = 0x000FFFFF,
            k_DefaultInstance          = 1,
            k_DesktopInstance          = 1,	 
            k_ConsoleInstance          = 2,
            k_WebInstance              = 4,

            k_EChatAccountInstanceMask  = 0x00000FFF, // top 8 bits are flags
            k_EChatInstanceFlagClan     = (k_SteamAccountInstanceMask + 1) >> 1,  // top bit
            k_EChatInstanceFlagLobby    = (k_SteamAccountInstanceMask + 1) >> 2, // next one down, etc
            k_EChatInstanceFlagMMSLobby = (k_SteamAccountInstanceMask + 1) >> 3,  // next one down, etc

            // Max of 8 flags
        };

        /// 64bits steam id
        ///   Universe AccountType InstanceID: 20 bits AccountId: 32 bits
        /// 0x[FF]     [F]         [FFFFF]             [FFFFFFFF]
        public ulong Id { get; set; } = 0;

        [XmlIgnore]
        public EUniverse Universe
        {
            get => (EUniverse)((Id & 0xFF00000000000000) >> 56);
            set => Id = (Id & 0x00FFFFFFFFFFFFFF) | (((ulong)value & 0xFF) << 56);
        }

        [XmlIgnore]
        public EAccountType AccountType
        {
            get => (EAccountType)((Id & 0x00F0000000000000) >> 52);
            set => Id = (Id & 0xFF0FFFFFFFFFFFFF) | (((ulong)value & 0xF) << 52);
        }

        [XmlIgnore]
        public EInstanceFlags InstanceID
        {
            get => (EInstanceFlags)((Id & 0x000FFFFF00000000) >> 32);
            set => Id = (Id & 0xFFF00000FFFFFFFF) | (((ulong)value & 0xFFFFF) << 32);
        }

        [XmlIgnore]
        public uint AccountID
        {
            get => (uint)Id;
            set => Id = (Id & 0xFFFFFFFF00000000) | value;
        }

        public SteamID()
        { }

        public SteamID(ulong steamId)
        {
            Id = steamId;
        }

        public SteamID(string username)
        {
            Id = GenerateIdFromName(username);
        }

        public static ulong GenerateIdFromName(string username)
        {
            uint accountid = 0;
            int i;
            uint base_id = 0;

            while (accountid == 0)
            {
                accountid = base_id;
                base_id += 0x00001337;
                i = 0;
                foreach (var c in username)
                {
                    uint b = (byte)(c + i * 27);
                    int shift = (i++ * 8) % 32;

                    accountid = (uint)(accountid ^ (b << shift));

                    shift = (i * 8) % 32;
                    b = (byte)(c - i * 8);
                    accountid = (uint)(accountid ^ (b << shift));
                }
            }

            ulong id;
            id = (((ulong)EUniverse.k_EUniversePublic & 0xFF) << 56) |
                 (((ulong)EAccountType.k_EAccountTypeIndividual & 0xF) << 52) |
                 (((ulong)EInstanceFlags.k_DesktopInstance & 0xFFFFF) << 32) |
                 accountid;

            return id;
        }

        public static uint RandomAccountId()
        {
            return (uint)(new Random().NextDouble() * uint.MaxValue);
        }

        public static ulong RandomDesktopInstance()
        {
            return (((ulong)EUniverse.k_EUniversePublic & 0xFF) << 56) |
                   (((ulong)EAccountType.k_EAccountTypeIndividual & 0xF) << 52) |
                   (((ulong)EInstanceFlags.k_DesktopInstance & 0xFFFFF) << 32) |
                   RandomAccountId();
        }

        public bool IsIndividual()
        {
            return (AccountType & EAccountType.k_EAccountTypeIndividual) == EAccountType.k_EAccountTypeIndividual;
        }

        public bool IsDesktopUser()
        {
            return (AccountType == EAccountType.k_EAccountTypeIndividual &&
                    Universe == EUniverse.k_EUniversePublic &&
                    (InstanceID & EInstanceFlags.k_DesktopInstance) == EInstanceFlags.k_DesktopInstance &&
                    AccountID != 0);
        }

        public override string ToString()
        {
            return Id.ToString();
        }

        public static SteamID Parse(string value)
        {
            return new SteamID
            {
                Id = ulong.Parse(value)
            };
        }

        public static bool TryParse(string value, out SteamID steam_id)
        {
            try
            {
                if (value != null)
                {
                    steam_id = Parse(value);
                    return true;
                }
            }
            catch { }
            steam_id = null;
            return false;
        }
    }
}
