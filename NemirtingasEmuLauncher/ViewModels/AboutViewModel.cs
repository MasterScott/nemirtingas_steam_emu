using OSUtility;
using System.Diagnostics;
using System.IO;
using MessageBox.Avalonia;
using MessageBox.Avalonia.Enums;
using NemirtingasEmuLauncher.Windows;

namespace NemirtingasEmuLauncher.ViewModels
{
    public class AboutViewModel : ViewModelBase
    {
        private About _parent;
        public AboutViewModel(Windows.About parent)
        {
            _parent = parent;
            if (OSDetector.IsMacOS())
            {
                SteamApi = "Folder to steam emu\'s " + OSFuncs.GetSteamAPIName(false);
            }
            else
            {
                SteamApi = "Folder to steam emu\'s 32bits " + OSFuncs.GetSteamAPIName(false);
                SteamApi64 = "Folder to steam emu\'s 64bits " + OSFuncs.GetSteamAPIName(true);
            }
        }

        public void OpenGitPage()
        {
            string url = "https://gitlab.com/Nemirtingas/nemirtingas_steam_emu";
            Process.Start(new ProcessStartInfo(url) { CreateNoWindow = true, UseShellExecute = true });
        }

        public bool Is32BitsVisible => !OSDetector.IsMacOS();
        public bool Is64BitsVisible => true;

        private string steamApi;

        public string SteamApi
        {
            get { return steamApi; }
            set { RaiseAndSetIfChanged(ref steamApi, value); }
        }

        private string steamApi64;

        public string SteamApi64
        {
            get { return steamApi64; }
            set { RaiseAndSetIfChanged(ref steamApi64, value ); }
        }

        public void SteamApi_Click()
        {
            OpenEmuFolder(false);
        }

        public void SteamApi64_Click()
        {
            OpenEmuFolder(true);
        }

        private void OpenEmuFolder(bool x64)
        {
            string emu_folder = OSFuncs.GetEmuApiFolder(x64);

            if (!Directory.Exists(emu_folder))
            {
                Directory.CreateDirectory(emu_folder);
            }

            try
            {
                if (OSDetector.IsWindows())
                {
                    Process.Start("explorer.exe", emu_folder);
                }
                else if (OSDetector.IsLinux())
                {
                    Process.Start("nautilus", emu_folder);
                }
                else if (OSDetector.IsMacOS())
                {
                    Process.Start("open", emu_folder);
                }
            }
            catch
            {
                MessageBoxManager.GetMessageBoxStandardWindow(
                    "Game emu folder",
                    "Folder: " + emu_folder,
                    ButtonEnum.Ok,
                    Icon.Info).ShowDialog(_parent);
            }
        }

        public void Close_Click()
        {
            _parent.Close();
        }

    }
}
