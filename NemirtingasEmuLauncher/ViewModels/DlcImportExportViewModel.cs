using NemirtingasEmuLauncher.Windows;

namespace NemirtingasEmuLauncher.ViewModels
{
    public class DlcImportExportViewModel : ViewModelBase
    {
        private DlcImportExport _parent;

        private string _DlcsText = string.Empty;
        public string DlcsText
        {
            get => _DlcsText;
            set => RaiseAndSetIfChanged(ref _DlcsText, value);
        }
        private bool _IsImport;
        public bool IsImport
        {
            get => _IsImport;
            set => RaiseAndSetIfChanged(ref _IsImport, value);
        }

        public DlcImportExportViewModel(Windows.DlcImportExport parent)
        {
            _parent = parent;
        }

        public void SetAction(string dlcs, bool import)
        {
            IsImport = import;
            if (!import)
            {
                DlcsText = dlcs;
            }
        }

        public void OnOk()
        {
            _parent.Close(DialogResult.DialogOk);
        }

        public void OnCancel()
        {
            _parent.Close(DialogResult.DialogCancel);
        }
    }
}
