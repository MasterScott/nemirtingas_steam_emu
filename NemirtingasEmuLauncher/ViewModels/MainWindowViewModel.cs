using Avalonia.Controls;
using Avalonia.Input;

using MessageBox.Avalonia;
using MessageBox.Avalonia.Enums;

using DynamicData.Kernel;

using System;
using System.IO;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using OSUtility;
using HeaderReader;

namespace NemirtingasEmuLauncher.ViewModels
{

    public class MainWindowViewModel : ViewModelBase
    {
        private Windows.MainWindow _parent;
        public ObservableCollectionAndItems<GameConfig> AppList { get; set; } = new ObservableCollectionAndItems<GameConfig>();

        public MainWindowViewModel(Windows.MainWindow parent)
        {
            _parent = parent;
            SteamEmulator.LoadSave(AppList);

            string[] args = Environment.GetCommandLineArgs();
            bool started_shortcut = false;
            if (args.Length >= 3)
            {
                for (int i = 1; i < args.Length; ++i)
                {
                    if (args[i] == "--game-guid" && ++i < args.Length)
                    {
                        try
                        {
                            Guid guid = Guid.Parse(args[i]);
                            GameConfig app = AppList.AsList().Find(p => p.GameGuid == guid);
                            ApiResult res = SteamEmulator.StartGame(app);
                            if (!res.Success)
                            {
                                MessageBoxManager.GetMessageBoxStandardWindow(
                                        "Error",
                                        "Failed to start shortcut: " + res.Message,
                                        ButtonEnum.Ok,
                                        Icon.Error).ShowDialog(_parent);
                            }

                            started_shortcut = true;
                        }
                        catch (Exception)
                        { }
                        break;
                    }
                }
            }

            if (started_shortcut)
            {
                _parent.IsVisible = false;
                _parent.Close();
            }
            else
            {
                StartIPCEnabled = SteamEmulator.IpcServer.HasIpc;

                SteamEmulator.IpcServer.IpcServerStarted += (object sender, EventArgs e) =>
                {
                    StartIPCEnabled = false;
                    StopIPCEnabled = true;
                };
                SteamEmulator.IpcServer.IpcServerStopped += (object sender, EventArgs e) =>
                {
                    StartIPCEnabled = true;
                    StopIPCEnabled = false;
                };
            }
        }

        //////////////////////////////////////////////////
        /// UI events/properties
        public bool IPCServerMenuEnabled
        {
            get => SteamEmulator.IpcServer.HasIpc;
        }

        private bool _StartIPCEnabled = OSDetector.IsMacOS();
        public bool StartIPCEnabled
        {
            get => _StartIPCEnabled;
            set => RaiseAndSetIfChanged(ref _StartIPCEnabled, value);
        }

        private bool _StopIPCEnabled = false;
        public bool StopIPCEnabled
        {
            get => _StopIPCEnabled;
            set => RaiseAndSetIfChanged(ref _StopIPCEnabled, value);
        }

        public void OnMenuExit()
        {
            _parent.Close();
        }

        public void OnMenuOpenAbout()
        {
            Windows.About aboutBox = new Windows.About();
            aboutBox.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            aboutBox.ShowDialog(_parent);
        }

        public async void OnMenuAddGame()
        {
            GameConfig app = new GameConfig();
            if (await openGameProperties(app, false) == DialogResult.DialogOk)
            {
                addGame(app);
            }
        }

        public async void OnMenuOpenSettings()
        {
            GameConfig app = new GameConfig();
            if (await openGameProperties(app, true) == DialogResult.DialogOk)
            {
                SteamEmulator.Save(AppList);
            }
        }

        public void OnMenuStartIPC()
        {
            SteamEmulator.IpcServer.StartIpcServer();
        }

        public void OnMenuStopIPC()
        {
            SteamEmulator.IpcServer.StopIpcServer();
        }

        public void OnGameClick(GameConfig app)
        {
            startGame(app);
        }

        public async void OnDropFiles(object sender, DragEventArgs e)
        {
            foreach (string file in e.Data.GetFileNames())
            {
                if (File.Exists(file))
                {
                    GameConfig app = new GameConfig();
                    app.FullPath = file;
                    app.AppName = Path.GetFileNameWithoutExtension(file);
                    app.StartFolder = Path.GetDirectoryName(file);
                    app.UseX64 = isAppX64(app);


                    if (await openGameProperties(app, false) == DialogResult.DialogOk)
                    {
                        addGame(app);
                    }
                }
                else
                {
                    MessageBoxManager.GetMessageBoxStandardWindow(
                        "Error",
                        file + " is not a file",
                        ButtonEnum.Ok,
                        Icon.Error).ShowDialog(_parent);
                }
            }
        }

        //////////////////////////////////////////////////
        /// Context Menu events/properties
        public void OnMenuGameStart(GameConfig app)
        {
            startGame(app);
        }

        public void OnMenuGameOpenEmuFolder(GameConfig app)
        {
            ApiResult res = SteamEmulator.ShowGameEmuFolder(app);
            if (!res.Success)
            {
                MessageBoxManager.GetMessageBoxStandardWindow(
                        "Error",
                        res.Message,
                        ButtonEnum.Ok,
                        Icon.Error).ShowDialog(_parent);
            }
        }

        public void OnMenuGameCreateShortcut(GameConfig app)
        {
            ApiResult res = SteamEmulator.CreateShortcut(app);
            if (!res.Success)
            {
                MessageBoxManager.GetMessageBoxStandardWindow(
                        "Error",
                        res.Message,
                        ButtonEnum.Ok,
                        Icon.Error).ShowDialog(_parent);
            }
        }

        public void OnMenuGameGenInfos(GameConfig app)
        {
            genGameInfos(app, true);
            SteamEmulator.Save(AppList);
        }

        public void OnMenuGameGenAchievements(GameConfig app)
        {
            ApiResult res = SteamEmulator.GenerateGameAchievements(app);
            if(!res.Success)
            { 
                MessageBoxManager.GetMessageBoxStandardWindow(
                        "Error",
                        res.Message,
                        ButtonEnum.Ok,
                        Icon.Error).ShowDialog(_parent);
            }
        }

        public void OnMenuGameGenDlcs(GameConfig app)
        {
            ApiResult res = SteamEmulator.GenerateGameDLCs(app);
            if (res.Success)
            {
                SteamEmulator.Save(AppList);
            }
            else
            {
                MessageBoxManager.GetMessageBoxStandardWindow(
                        "Error",
                        res.Message,
                        ButtonEnum.Ok,
                        Icon.Error).ShowDialog(_parent);
            }
        }

        public void OnMenuGameGenItems(GameConfig app)
        {
            ApiResult res = SteamEmulator.GenerateGameItems(app);
            if (!res.Success)
            {
                MessageBoxManager.GetMessageBoxStandardWindow(
                        "Error",
                        res.Message,
                        ButtonEnum.Ok,
                        Icon.Error).ShowDialog(_parent);
            }
        }

        public void OnMenuGameRemove(GameConfig app)
        {
            AppList.Remove(app);
            SteamEmulator.Save(AppList);
        }

        public async void OnMenuGameProperties(GameConfig app)
        {
            ulong oldAppId = app.AppId;
            if (await openGameProperties(app, false) == DialogResult.DialogOk)
            {
                if (oldAppId != app.AppId)
                {
                    genGameInfos(app);
                }
                SteamEmulator.Save(AppList);
            }
        }

        //////////////////////////////////////////////////
        /// Class functions
        private void startGame(GameConfig app)
        {
            ApiResult res = SteamEmulator.StartGame(app);
            if(!res.Success)
            {
                MessageBoxManager.GetMessageBoxStandardWindow(
                    "Error",
                    res.Message,
                    ButtonEnum.Ok,
                    Icon.Error).ShowDialog(_parent);
            }
        }

        private void addGame(GameConfig app)
        {
            genGameInfos(app);
            AppList.Add(app);
            SteamEmulator.Save(AppList);
        }

        private void genGameInfos(GameConfig app, bool clear_cache = false)
        {
            ApiResult res = SteamEmulator.GenerateGameInfos(app, clear_cache);
            if(!res.Success)
            {
                MessageBoxManager.GetMessageBoxStandardWindow(
                        "Error",
                        "Failed to retrieve game Infos: " + res.Message,
                        ButtonEnum.Ok,
                        Icon.Error).ShowDialog(_parent);
            }
        }

        private bool isAppX64(GameConfig app)
        {
            ExecutableHeaderReader reader;

            try
            {
                if (!(reader = new PeHeaderReader(app.FullPath)).IsValidHeader &&
                   !(reader = new ElfHeaderReader(app.FullPath)).IsValidHeader &&
                   !(reader = new MachOHeaderReader(app.FullPath)).IsValidHeader)
                {
                    reader = null;
                }
            }
            catch (Exception)
            {
                reader = null;
            }

            if (reader != null)
            {
                return !reader.Is32BitHeader;
            }

            return false;
        }

        private async Task<DialogResult> openGameProperties(GameConfig app, bool is_default_app)
        {
            Windows.Settings settingWindow = new Windows.Settings();
            settingWindow.SetGameConfig(app, is_default_app);

            settingWindow.WindowStartupLocation = Avalonia.Controls.WindowStartupLocation.CenterOwner;
            return await settingWindow.ShowDialog<DialogResult>(_parent);
        }
    }
}
