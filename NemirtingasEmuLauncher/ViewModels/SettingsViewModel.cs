using Avalonia.Controls;
using HeaderReader;
using OSUtility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using MessageBox.Avalonia;
using MessageBox.Avalonia.Enums;
using System.IO;
using DynamicData;
using System.Linq;
using System.Reactive.Linq;
using Avalonia;
using System.Reactive.Subjects;
using System.Reflection.Metadata.Ecma335;
using NemirtingasEmuLauncher.Windows;
using System.Threading.Tasks;

namespace NemirtingasEmuLauncher.ViewModels
{
    public class SettingsViewModel : ViewModelBase
    {
        static public readonly string inherit_global = "Use Global Setting...";

        #region Settings Tab
        public ObservableCollection<string> OverrideLanguageCombo { get; set; } = new ObservableCollection<string>();

        private string _SelectedOverrideLanguage = "";
        public string SelectedOverrideLanguage
        {
            get => _SelectedOverrideLanguage;
            set => RaiseAndSetIfChanged(ref _SelectedOverrideLanguage, value);
        }

        private string _WebApiKey = "";
        public string WebApiKey
        {
            get => _WebApiKey;
            set => RaiseAndSetIfChanged(ref _WebApiKey, value);
        }

        private string _UserName = "";
        public string UserName
        {
            get => _UserName;
            set
            {
                RaiseAndSetIfChanged(ref _UserName, value);
                SteamIdWatermark.OnNext(SteamID.GenerateIdFromName(_UserName).ToString());
            }
        }

        private string _SteamId = "";
        public string SteamId
        {
            get => _SteamId;
            set
            {
                //ulong id;
                //if(!ulong.TryParse(value, out id))
                //{
                //    throw new InvalidDataException("SteamID should only contain digits");
                //}
                RaiseAndSetIfChanged(ref _SteamId, value);
                if (string.IsNullOrWhiteSpace(_SteamId))
                {
                    SteamIdWatermark.OnNext(SteamID.GenerateIdFromName(_UserName).ToString());
                }
            }
        }

        public Subject<string> SteamIdWatermark = new Subject<string>();

        public ObservableCollection<string> LogLevels { get; set; } = new ObservableCollection<string>();

        private string _SelectedLogLevel = "";
        public string SelectedLogLevel
        {
            get => _SelectedLogLevel;
            set => RaiseAndSetIfChanged(ref _SelectedLogLevel, value);
        }

        private bool? _EnableOverlay;
        public bool? EnableOverlay
        {
            get => _EnableOverlay;
            set => RaiseAndSetIfChanged(ref _EnableOverlay, value);
        }

        private bool? _UnlockDlcs;
        public bool? UnlockDlcs
        {
            get => _UnlockDlcs;
            set => RaiseAndSetIfChanged(ref _UnlockDlcs, value);
        }
        #endregion
        #region "Game Settings" Tab
        private string _GameName = "";
        public string GameName
        {
            get => _GameName;
            set => RaiseAndSetIfChanged(ref _GameName, value);
        }
        private string _GameExePath = string.Empty;
        public string GameExePath
        {
            get => _GameExePath;
            set => RaiseAndSetIfChanged(ref _GameExePath, value);
        }
        private string _GameParameters = string.Empty;
        public string GameParameters
        {
            get => _GameParameters;
            set => RaiseAndSetIfChanged(ref _GameParameters, value);
        }
        private string _GameStartFolder = string.Empty;
        public string GameStartFolder
        {
            get => _GameStartFolder;
            set => RaiseAndSetIfChanged(ref _GameStartFolder, value);
        }
        private ulong _AppId = 0;
        public ulong AppId
        {
            get => _AppId;
            set => RaiseAndSetIfChanged(ref _AppId, value);
        }

        private bool _GameIsX64 = false;
        public bool GameIsX64
        {
            get => _GameIsX64;
            set => RaiseAndSetIfChanged(ref _GameIsX64, value);
        }

        private string _GameSavePath = string.Empty;
        public string GameSavePath
        {
            get => _GameSavePath;
            set => RaiseAndSetIfChanged(ref _GameSavePath, value);
        }
        #endregion
        #region "Dlcs" tab
        private ulong _DlcId;
        public ulong DlcId
        {
            get => _DlcId;
            set => RaiseAndSetIfChanged(ref _DlcId, value);
        }
        private string _DlcName = string.Empty;
        public string DlcName
        {
            get => _DlcName;
            set => RaiseAndSetIfChanged(ref _DlcName, value);
        }

        public ObservableCollectionAndItems<Dlc> Dlcs { get; set; } = new ObservableCollectionAndItems<Dlc>();

        public ObservableCollection<Dlc> SelectedDlcs { get; set; } = new ObservableCollection<Dlc>();
        #endregion
        #region "Custom" tab
        public ObservableCollection<string> AvailableLanguagesCombo { get; set; } = new ObservableCollection<string>();
        private string _SelectedAvailableLanguage = string.Empty;
        public string SelectedAvailableLanguage
        {
            get => _SelectedAvailableLanguage;
            set => RaiseAndSetIfChanged(ref _SelectedAvailableLanguage, value);
        }
        
        public ObservableCollection<string> SupportedLanguagesList { get; set; } = new ObservableCollection<string>();

        public ObservableCollection<string> SelectedSupportedLanguages { get; set; } = new ObservableCollection<string>();

        private string _CustomEnvVarKey = string.Empty;
        public string CustomEnvVarKey
        {
            get => _CustomEnvVarKey;
            set => RaiseAndSetIfChanged(ref _CustomEnvVarKey, value);
        }

        private string _CustomEnvVarValue = string.Empty;
        public string CustomEnvVarValue
        {
            get => _CustomEnvVarValue;
            set => RaiseAndSetIfChanged(ref _CustomEnvVarValue, value);
        }

        public ObservableCollection<EnvVar> CustomEnvVars { get; set; } = new ObservableCollection<EnvVar>();
        public ObservableCollection<EnvVar> SelectedCustomEnvVars { get; set; } = new ObservableCollection<EnvVar>();
        #endregion
        #region Class members
        private GameConfig game_app { get; set; }

        private bool _IsDefaultApp = false;
        public bool IsDefaultApp
        {
            get => _IsDefaultApp;
            set => RaiseAndSetIfChanged(ref _IsDefaultApp, value);
        }

        private Windows.Settings _parent;
        public SettingsViewModel(Windows.Settings parent)
        {
            _parent = parent;
        }

        public void SetApp(GameConfig app, bool is_default_app)
        {
            IsDefaultApp = is_default_app;
            game_app = app;

            WebApiKey        = game_app.EmuConfig.WebApiKey;
            UserName         = game_app.EmuConfig.UserName;
            EnableOverlay    = game_app.EmuConfig.EnableOverlay;
            UnlockDlcs       = game_app.EmuConfig.UnlockDlcs;
            LogLevels.AddRange(EmuConfig.LogLevels);
            SelectedLogLevel = game_app.EmuConfig.LogLevel;

            if (!is_default_app)
            {
                OverrideLanguageCombo.Insert(0, inherit_global);
                LogLevels.Insert(0, inherit_global);
            }
            
            for (int i = 0; i < SteamEmulator.EmuConfigs.Languages.Length; ++i)
            {
                string lang = SteamEmulator.EmuConfigs.Languages[i];
                if (is_default_app || app.Languages.Contains(lang))
                {
                    OverrideLanguageCombo.Add(lang);
                    SupportedLanguagesList.Add(lang);
                    if (string.IsNullOrEmpty(SelectedOverrideLanguage))
                    {
                        if (lang.Equals(app.EmuConfig.Language))
                        {
                            SelectedOverrideLanguage = lang;
                        }
                    }
                }
                else
                {
                    AvailableLanguagesCombo.Add(lang);
                }
            }
            
            if (string.IsNullOrEmpty(SelectedOverrideLanguage))
            {
                SelectedOverrideLanguage = is_default_app ? "english" : OverrideLanguageCombo[0];
            }
            if (string.IsNullOrEmpty(SelectedLogLevel))
            {
                SelectedLogLevel = is_default_app ? "OFF" : LogLevels[0];
            }

            if (!is_default_app)
            {
                _parent.SettingsTab.SelectedIndex = 1;

                _parent.TbxUserName.Watermark  = "Use Global Setting...";
                _parent.TbxSteamId.Watermark   = "Use Global Setting...";
                _parent.TbxWebApiKey.Watermark = "Use Global Setting...";

                _parent.TbxGameSavePath.Watermark = SteamEmulator.GameEmuFolder;

                switch (OSDetector.GetOS())
                {
                    case OsId.Linux:
                        _parent.TbxEnvVarKey.Watermark = "LD_PRELOAD";
                        _parent.TbxEnvVarValue.Watermark = Path.Combine(OSFuncs.GetEmuApiFolder(app.UseX64), OSFuncs.GetSteamAPIName(app.UseX64));
                        break;
                    case OsId.MacOSX:
                        _parent.TbxEnvVarKey.Watermark = "DYLD_PRELOAD";
                        _parent.TbxEnvVarValue.Watermark = Path.Combine(OSFuncs.GetEmuApiFolder(app.UseX64), OSFuncs.GetSteamAPIName(app.UseX64));
                        break;
                    default:
                        break;
                }

                GameName        = game_app.AppName;
                GameStartFolder = game_app.StartFolder;
                GameExePath     = game_app.FullPath;
                AppId           = game_app.AppId;
                GameParameters  = game_app.Parameters;
                GameIsX64       = game_app.UseX64;
                GameSavePath    = game_app.SavePath;

                CustomEnvVars.AddRange(Clone.CloneObject(game_app.EnvVars));
                Dlcs.AddRange(Clone.CloneObject(game_app.Dlcs.OrderBy(dlc => dlc.DlcId).Distinct().ToList()));
            }// Not default app
            else
            {
                _parent.TbxSteamId.Bind(TextBox.WatermarkProperty, SteamIdWatermark);
            }

            SteamId = game_app.EmuConfig.SteamId != null ? game_app.EmuConfig.SteamId.ToString() : string.Empty;

            //foreach (string ip in app.CustomBroadcasts)
            //{
            //    try
            //    {
            //        broadcast_listBox.Items.Add(IPAddress.Parse(ip));
            //    }
            //    catch { }
            //}
            //foreach (KeyValuePair<string, string> env_var in app.EnvVar)
            //{
            //    listBox_env_var.Items.Add(env_var);
            //}
        }

        private bool? isExeX64(string app_path)
        {
            ExecutableHeaderReader reader;

            try
            {
                if (!(reader = new PeHeaderReader(app_path)).IsValidHeader &&
                   !(reader = new ElfHeaderReader(app_path)).IsValidHeader &&
                   !(reader = new MachOHeaderReader(app_path)).IsValidHeader)
                {
                    reader = null;
                }
            }
            catch (Exception)
            {
                reader = null;
            }

            if (reader != null)
            {
                return !reader.Is32BitHeader;
            }

            return null;
        }

        public void OnButtonGenerateId()
        {
            SteamId = SteamID.RandomDesktopInstance().ToString();
        }
        
        public void OnButtonSave()
        {
            if (is_app_valid())
            {
                _parent.Close(DialogResult.DialogOk);
            }
        }

        public void OnButtonCancel()
        {
            _parent.Close(DialogResult.DialogCancel);
        }
        public async void OnButtonBrowseGameExe()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if(OSDetector.IsWindows())
            {
                FileDialogFilter filter = new FileDialogFilter();
                filter.Name = "Game executables";
                filter.Extensions.Add("exe");
                openFileDialog.Filters.Add(filter);
                filter = new FileDialogFilter();
                filter.Name = "All files";
                filter.Extensions.Add("*");
                openFileDialog.Filters.Add(filter);
            }

            openFileDialog.AllowMultiple = false;
            openFileDialog.Directory = Path.GetDirectoryName(game_app.FullPath);
            var res = await openFileDialog.ShowAsync(_parent);

            if (res != null && res.Length > 0)
            {
                GameExePath = res[0];
                GameStartFolder = Path.GetDirectoryName(GameExePath);
                if (string.IsNullOrEmpty(GameName))
                {
                    GameName = Path.GetFileNameWithoutExtension(GameExePath);
                }

                bool? x64 = isExeX64(GameExePath);
                if(x64 != null)
                {
                    GameIsX64 = x64.Value;
                }
            }
        }
        
        public async void OnButtonBrowseGameStartFolder()
        {
            OpenFolderDialog openFolderDialog = new OpenFolderDialog();
            openFolderDialog.Directory = string.IsNullOrEmpty(GameStartFolder) ? 
                string.IsNullOrEmpty(GameExePath) ? SteamEmulator.LauncherFolder : Path.GetDirectoryName(GameExePath) 
                : GameStartFolder;
            openFolderDialog.Title = "Game start folder";
            var res = await openFolderDialog.ShowAsync(_parent);
            if (res != null && !string.IsNullOrEmpty(res))
            {
                GameStartFolder = res;
            }
        }
        
        private bool is_app_valid()
        {
            //foreach (IPAddress ip in broadcast_listBox.Items)
            //{
            //    modified_app.CustomBroadcasts.Add(ip.ToString());
            //}
            //

            if (!string.IsNullOrWhiteSpace(WebApiKey) && WebApiKey.Length != 32)
            {
                MessageBoxManager.GetMessageBoxStandardWindow(
                    "Webapi Key invalid",
                    "The webapi key consists of 32 alphanum char in upper case.\n\nMore infos at https://steamcommunity.com/dev/apikey",
                    ButtonEnum.Ok,
                    Icon.Error).ShowDialog(_parent);
                return false;
            }
            
            if (!IsDefaultApp)
            {
                if (string.IsNullOrWhiteSpace(GameStartFolder) || !Directory.Exists(GameStartFolder))
                {
                    MessageBoxManager.GetMessageBoxStandardWindow(
                        "Error",
                        "Start folder " + GameStartFolder + " does not exist",
                        ButtonEnum.Ok,
                        Icon.Error).ShowDialog(_parent);
                    return false;
                }
            
                if (string.IsNullOrEmpty(GameExePath) || !File.Exists(GameExePath))
                {
                    MessageBoxManager.GetMessageBoxStandardWindow(
                        "Error",
                        "Game exe " + GameExePath + " does not exist",
                        ButtonEnum.Ok,
                        Icon.Error).ShowDialog(_parent);
                    return false;
                }

                if(AppId == 0)
                {
                    MessageBoxManager.GetMessageBoxStandardWindow(
                        "Error",
                        "The AppId must be > 0",
                        ButtonEnum.Ok,
                        Icon.Error).ShowDialog(_parent);
                    return false;
                }

                SteamID steam_id = null;
                if (!string.IsNullOrWhiteSpace(SteamId) && (!SteamID.TryParse(SteamId, out steam_id) || !steam_id.IsDesktopUser()))
                {
                    MessageBoxManager.GetMessageBoxStandardWindow(
                        "Invalid SteamID",
                        "Please set a valid SteamID (you can click \"Generate Random ID\" and change the low 32 bits) or remove it to use your global settings.",
                        ButtonEnum.Ok,
                        Icon.Error).ShowDialog(_parent);
                    return false;
                }

                game_app.AppId = AppId;
                game_app.UseX64 = GameIsX64;
                game_app.AppName = GameName;
                game_app.FullPath = GameExePath;
                game_app.StartFolder = GameStartFolder;
                game_app.Parameters = GameParameters;
                //game_app.CustomBroadcasts.Clear();
                //game_app.CustomBroadcasts.AddRange(Broadcasts);
                game_app.Languages.Clear();
                game_app.Languages.AddRange(SupportedLanguagesList);
                game_app.EnvVars.Clear();
                game_app.EnvVars.AddRange(CustomEnvVars);
                game_app.Dlcs.Clear();
                game_app.Dlcs.AddRange(Dlcs);
                game_app.SavePath = GameSavePath;
                game_app.EmuConfig.SteamId = steam_id;
            }
            else
            {
                if (string.IsNullOrWhiteSpace(UserName))
                {
                    MessageBoxManager.GetMessageBoxStandardWindow(
                        "Invalid Username",
                        "Please set your username",
                        ButtonEnum.Ok,
                        Icon.Error).ShowDialog(_parent);
                    return false;
                }

                SteamID steam_id = null;
                if (!string.IsNullOrWhiteSpace(SteamId) && (!SteamID.TryParse(SteamId, out steam_id) || !steam_id.IsDesktopUser()))
                {
                    MessageBoxManager.GetMessageBoxStandardWindow(
                        "Invalid SteamID",
                        "Please set a valid SteamID (you can click \"Generate Random ID\" and change the low 32 bits)",
                        ButtonEnum.Ok,
                        Icon.Error).ShowDialog(_parent);
                    return false;
                }

                game_app.EmuConfig.SteamId = steam_id;
            }
            
            game_app.EmuConfig.UserName = UserName;
            game_app.EmuConfig.WebApiKey = WebApiKey;
            game_app.EmuConfig.Language = SelectedOverrideLanguage == inherit_global ? "" : SelectedOverrideLanguage;
            game_app.EmuConfig.UnlockDlcs = UnlockDlcs;
            game_app.EmuConfig.EnableOverlay = EnableOverlay;
            game_app.EmuConfig.LogLevel = SelectedLogLevel == inherit_global ? "" : SelectedLogLevel;

            return true;
        }
        
        public void OnAddLanguage()
        {
            if (!string.IsNullOrWhiteSpace(SelectedAvailableLanguage))
            {
                bool inserted = false;
                for(int i = 1; i < OverrideLanguageCombo.Count; ++i)
                {
                    if(OverrideLanguageCombo[i].CompareTo(SelectedAvailableLanguage) > 0)
                    {
                        OverrideLanguageCombo.Insert(i, SelectedAvailableLanguage);
                        inserted = true;
                        break;
                    }
                }
                if(!inserted)
                {
                    OverrideLanguageCombo.Add(SelectedAvailableLanguage);
                }
                inserted = false;
                for (int i = 0; i < SupportedLanguagesList.Count; ++i)
                {
                    if (SupportedLanguagesList[i].CompareTo(SelectedAvailableLanguage) > 0)
                    {
                        SupportedLanguagesList.Insert(i, SelectedAvailableLanguage);
                        inserted = true;
                        break;
                    }
                }
                if (!inserted)
                {
                    SupportedLanguagesList.Add(SelectedAvailableLanguage);
                }

                AvailableLanguagesCombo.Remove(SelectedAvailableLanguage);
            }
        }

        public void OnRemoveLanguage()
        {
            List<string> tmp = SelectedSupportedLanguages.ToList();
            SupportedLanguagesList.Remove(tmp);
            OverrideLanguageCombo.Remove(tmp);

            using (List<string>.Enumerator lang_enum = tmp.GetEnumerator())
            {
                lang_enum.MoveNext();
                for (int i = 1; i < AvailableLanguagesCombo.Count; ++i)
                {
                    if (AvailableLanguagesCombo[i].CompareTo(lang_enum.Current) > 0)
                    {
                        AvailableLanguagesCombo.Insert(i, lang_enum.Current);
                        lang_enum.MoveNext();
                        if (lang_enum.Current == null)
                            break;
                    }
                }
                while (lang_enum.Current != null)
                {
                    AvailableLanguagesCombo.Add(lang_enum.Current);
                    lang_enum.MoveNext();
                }
            }
        }

        public void OnClearLanguage()
        {
            AvailableLanguagesCombo.Clear();
            AvailableLanguagesCombo.AddRange(SteamEmulator.EmuConfigs.Languages);
            OverrideLanguageCombo.Clear();
            OverrideLanguageCombo.Add(inherit_global);
            SelectedOverrideLanguage = inherit_global;

            SupportedLanguagesList.Clear();
        }

        public void OnEnvVarAdd()
        {
            if(!string.IsNullOrWhiteSpace(CustomEnvVarKey) && !string.IsNullOrWhiteSpace(CustomEnvVarValue))
            {
                CustomEnvVars.Add(new EnvVar(CustomEnvVarKey, CustomEnvVarValue));
                CustomEnvVarKey = string.Empty;
                CustomEnvVarValue = string.Empty;
            }
        }

        public void OnEnvVarRemove()
        {
            var tmp = new List<EnvVar>(SelectedCustomEnvVars);
            CustomEnvVars.Remove(tmp);
        }

        public void OnEnvVarClear()
        {
            CustomEnvVars.Clear();
        }

        public void OnDlcAdd()
        {
            if (DlcId != 0 && !string.IsNullOrWhiteSpace(DlcName))
            {
                
                try
                {
                    bool inserted = false;

                    for (int i = 0; i < Dlcs.Count; ++i)
                    {
                        if (Dlcs[i].DlcId == DlcId)
                        {
                            Dlcs[i].Name = DlcName;
                            inserted = true;
                            break;
                        }
                        if (Dlcs[i].DlcId > DlcId)
                        {
                            Dlcs.Insert(i, new Dlc { DlcId = DlcId, Name = DlcName, Enabled = true });
                            inserted = true;
                            break;
                        }
                    }
                    if (!inserted)
                    {
                        Dlcs.Add(new Dlc { DlcId = DlcId, Name = DlcName, Enabled = true });
                    }

                    DlcId = 0;
                    DlcName = string.Empty;
                }
                catch(Exception e)
                {
                    MessageBoxManager.GetMessageBoxStandardWindow(
                        "Failed to add dlc",
                        e.Message,
                        ButtonEnum.Ok,
                        Icon.Info).ShowDialog(_parent);
                }
            }
        }

        public void OnDlcRemove()
        {
            var tmp = new List<Dlc>(SelectedDlcs);
            Dlcs.Remove(tmp);
        }

        public void OnDlcClear()
        {
            Dlcs.Clear();
        }

        public async void OnDlcImport()
        {
            DlcImportExport exportWindow = new DlcImportExport();

            (exportWindow.DataContext as DlcImportExportViewModel).SetAction(string.Empty, true);
            DialogResult res = await exportWindow.ShowDialog<DialogResult>(_parent);
            if(res == DialogResult.DialogOk)
            {
                string[] lines = (exportWindow.DataContext as DlcImportExportViewModel).DlcsText.Split(new string[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                foreach (var line in lines)
                {
                    try
                    {
                        string[] items = line.Split('=', 2);
                        if (items.Length == 2)
                        {
                            ulong dlcid = ulong.Parse(items[0]);
                            bool inserted = false;

                            for (int i = 0; i < Dlcs.Count; ++i)
                            {
                                if (Dlcs[i].DlcId == dlcid)
                                {
                                    Dlcs[i].Name = items[1];
                                    inserted = true;
                                    break;
                                }
                                if (Dlcs[i].DlcId > dlcid)
                                {
                                    Dlcs.Insert(i, new Dlc { DlcId = dlcid, Name = items[1], Enabled = true });
                                    inserted = true;
                                    break;
                                }
                            }
                            if (!inserted)
                            {
                                Dlcs.Add(new Dlc { DlcId = dlcid, Name = items[1], Enabled = true });
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        await MessageBoxManager.GetMessageBoxStandardWindow(
                            "Failed to add dlc",
                            e.Message,
                            ButtonEnum.Ok,
                            Icon.Info).ShowDialog(_parent);
                    }
                }
            }
        }

        public void OnDlcExport()
        {
            DlcImportExport exportWindow = new DlcImportExport();
            string dlcsExport = string.Empty;
            foreach (var dlc in Dlcs)
            {
                dlcsExport += string.Format("{0}={1}\n", dlc.DlcId, dlc.Name);
            }

            exportWindow.ShowDialog(_parent);
            (exportWindow.DataContext as DlcImportExportViewModel).SetAction(dlcsExport, false);
        }

        #endregion
    }
}
