﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace NemirtingasEmuLauncher.Windows
{
    public class DlcImportExport : Window
    {
        public DlcImportExport()
        {
            this.InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
            this.DataContext = new ViewModels.DlcImportExportViewModel(this);
        }

    }
}
