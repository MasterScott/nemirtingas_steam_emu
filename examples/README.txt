To build a controller config, download the controller.vdf from steamworkshop.

To do so, go to https://steamdb.info/app/<appid>/config/ and look for steamcontrollerconfigdetails -> 1411285346/controller_type: controller_xbox360.

The number 1411285346 is the workshop id to download, you can download it with the website: http://steamworkshop.download/download/view/1411285346.

Now use vdf_controller_to_json.pl to transform the .vdf to a .json and create a structure like the example.

NemirtingasSteamEmu.json is an example file, it will be generated if it cannot be found and will create default values for keys that are missing.

The "webapi_key" can be retrieved from an official steam account with at least 1 paid game here: https://steamcommunity.com/dev/apikey

It will be used to get the achievments and items definitions for your game.