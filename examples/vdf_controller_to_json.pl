#! /bin/perl

use strict;
use warnings;
use Encode qw(encode decode);

use JSON;
use Data::Dumper;

sub ltrim { my $s = shift; $s =~ s/^\s+//;       return $s };
sub rtrim { my $s = shift; $s =~ s/\s+$//;       return $s };
sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };

$Data::Dumper::Indent = 1;

sub parse_vdf_line
{
  my $json = shift;
  my $line = shift;
  my $fh = shift;
  if( $line =~ m/^"([^"]+)"$/ )
  {# Key
    push(@{$$json{$1}}, {});
    read_vdf(\%{$$json{$1}[-1]}, $fh);
  }
  elsif( $line =~ m/^"([^"]+)"\s+"([^"]+)"$/ )
  {# Key Val
    $$json{$1} = $2;
  }
}

sub read_vdf
{
  my $json = shift;
  my $fh = shift;

  my $line = <$fh>;
  $line=trim($line);
  if($line eq "{")
  {
    while($line = <$fh>)
    {
      $line = trim($line);
      if($line eq '}')
      {
        last;
      }
      parse_vdf_line($json, $line, $fh);
    }
  }
}

if(scalar(@ARGV) != 2)
{
  print "Must have 2 parameters: <input vdf file> <output json file>";
  exit(1);
}

my $vdf_file = $ARGV[0];
my $json_file = $ARGV[1];

my %json;

open(my $vdf, "<", $vdf_file) or die("Can't open $vdf_file: $!\n");

my $key = <$vdf>;
$key = trim($key);
$key = substr($key, 1);
$key = substr($key, 0, -1);
$json{$key} = {};
read_vdf(\%{$json{$key}}, $vdf);

close($vdf);

my %controller_json;

my $mappings_actions = $json{"controller_mappings"}{"actions"};
foreach my $actions (@{$mappings_actions})
{
  foreach my $action_key (keys %{$actions})
  {
    my $action = $$actions{$action_key}[0];
    foreach my $control_key (keys %{$action})
    {
      if($control_key eq "StickPadGyro")
      {
        my $stick= $$action{$control_key}[0];
        foreach my $button (keys %{$stick})
        {
          $controller_json{"actions_sets"}{$action_key}{"analog"}{$button} = $$stick{$button}[0]{"title"};
        }
      }
      elsif($control_key eq "Button")
      {
        my $buttons = $$action{$control_key}[0];
        foreach my $button (keys %{$buttons})
        {
          $controller_json{"actions_sets"}{$action_key}{"digital"}{$button} = $$buttons{$button};
        }
      }
      elsif($control_key eq "AnalogTrigger")
      {
        my $buttons = $$action{$control_key}[0];
        foreach my $button (keys %{$buttons})
        {
          $controller_json{"actions_sets"}{$action_key}{"analog"}{$button} = $$buttons{$button};
        }
      }
      else
      {
        $controller_json{"actions_sets"}{$action_key}{$control_key} = $$action{$control_key};
      }
    }
  }
}

my $mappings_localizations = $json{"controller_mappings"}{"localization"}[0];
foreach my $localization (keys %{$mappings_localizations})
{
  $controller_json{"localizations"}{$localization} = $$mappings_localizations{$localization}[0];
}

my $mappings_groups = $json{"controller_mappings"}{"group"};
foreach my $group (@{$mappings_groups})
{
  my $mode = $$group{"mode"};
  if($mode eq "switches" ||
     $mode eq "dpad" ||
     $mode eq "four_buttons")
  {
    foreach my $button (keys %{$$group{"inputs"}[0]})
    {
      #print "$button\n";
      my $input = $$group{"inputs"}[0]{$button}[0];

      foreach my $activator (keys %{$$input{"activators"}[0]})
      {
        #print "$activator\n";
        my $bindings = $$input{"activators"}[0]{$activator}[0]{"bindings"};
        #my $settings = $$input{"activators"}[0]{$activator}[0]{"settings"};

        for my $binding (@{$bindings})
        {
          if( $$binding{"binding"} =~ m/^(game_action) ([^ ]+) ([^,]+), (.*)$/ )
          {
            $controller_json{"actions"}{"digital"}{$3}{"button"} = $button;
            $controller_json{"actions"}{"digital"}{$3}{"mode"} = $mode;
            $controller_json{"actions"}{"digital"}{$3}{"description"} = $4;
            $controller_json{"actions"}{"digital"}{$3}{"activator"} = $activator;
          }
		  elsif( $$binding{"binding"} =~ m/^(xinput_button) (.*)$/ )
          {
            $controller_json{"actions"}{"digital"}{$button}{"button"} = $button;
            $controller_json{"actions"}{"digital"}{$button}{"mode"} = $mode;
            $controller_json{"actions"}{"digital"}{$button}{"description"} = $$binding{"binding"};
            $controller_json{"actions"}{"digital"}{$button}{"activator"} = $activator;
          }
        }
      }
    }
  }
  elsif($mode eq "joystick_move")
  {
    foreach my $action_set_name (keys %{$$group{"gameactions"}[0]})
    {
      my $action_name = $$group{"gameactions"}[0]{$action_set_name};
      if($mode eq "joystick_move")
      {
        $controller_json{"actions"}{"analog"}{$action_name}{"button"} = "joystick";
      }
      elsif($mode eq "joystick_camera")
      {
        $controller_json{"actions"}{"analog"}{$action_name}{"button"} = "right_joystick";
      }
      $controller_json{"actions"}{"analog"}{$action_name}{"mode"} = $mode;
      $controller_json{"actions"}{"analog"}{$action_name}{"description"} = "Action_$action_name";
    }
  }
}

#print Dumper(\%controller_json);

open(my $json_handle, ">", $json_file);

print $json_handle JSON->new->pretty->encode(\%controller_json);

close($json_handle);
