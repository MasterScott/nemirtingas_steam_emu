#! /bin/perl

use strict;
use warnings;
use Encode qw(encode decode);

use JSON;
use Data::Dumper;

sub ltrim { my $s = shift; $s =~ s/^\s+//;       return $s };
sub rtrim { my $s = shift; $s =~ s/\s+$//;       return $s };
sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };

sub parse_vdf_line
{
  my $json = shift;
  my $line = shift;
  my $fh = shift;
  if( $line =~ m/^"([^"]+)"$/ )
  {# Key
    push(@{$$json{$1}}, {});
    read_vdf(\%{$$json{$1}[-1]}, $fh);
  }
  elsif( $line =~ m/^"([^"]+)"\s+"([^"]+)"$/ )
  {# Key Val
    $$json{$1} = $2;
  }
}

sub read_vdf
{
  my $json = shift;
  my $fh = shift;

  my $line = <$fh>;
  $line=trim($line);
  if($line eq "{")
  {
    while($line = <$fh>)
    {
      $line = trim($line);
      if($line eq '}')
      {
        last;
      }
      parse_vdf_line($json, $line, $fh);
    }
  }
}

if(scalar(@ARGV) != 2)
{
  print "Must have 2 parameters: <input vdf file> <output json file>";
  exit(1);
}

my $vdf_file = $ARGV[0];
my $json_file = $ARGV[1];

my %json;

open(my $vdf, "<", $vdf_file) or die("Can't open $vdf_file: $!\n");

my $key = <$vdf>;
$key = trim($key);
$key = substr($key, 1);
$key = substr($key, 0, -1);
$json{$key} = {};
read_vdf(\%{$json{$key}}, $vdf);

close($vdf);

open(my $json_handle, ">", $json_file);

print $json_handle JSON->new->pretty->encode(\%json);

close($json_handle;
