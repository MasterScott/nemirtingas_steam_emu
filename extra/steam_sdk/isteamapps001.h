//====== Copyright © 1996-2008, Valve Corporation, All rights reserved. =======
//
// Purpose: interface to app data in Steam
//
//=============================================================================

#ifndef ISTEAMAPPS001_H
#define ISTEAMAPPS001_H
#ifdef STEAM_WIN32
#pragma once
#endif

//-----------------------------------------------------------------------------
// Purpose: interface to app data
//-----------------------------------------------------------------------------
class ISteamApps001
{
public:
    // returns 0 if the key does not exist
    // this may be true on first call, since the app data may not be cached locally yet
    // If you expect it to exists wait for the AppDataChanged_t after the first failure and ask again
    virtual int GetAppData(AppId_t nAppID, const char* pchKey, char* pchValue, int cchValueMax) = 0;
};

#define STEAMAPPS_INTERFACE_VERSION001 "STEAMAPPS_INTERFACE_VERSION001"

#endif // ISTEAMAPPS001_H
