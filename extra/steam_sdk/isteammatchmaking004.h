//====== Copyright � 1996-2008, Valve Corporation, All rights reserved. =======
//
// Purpose: interface to steam managing game server/client match making
//
//=============================================================================

#ifndef ISTEAMMATCHMAKING004
#define ISTEAMMATCHMAKING004
#ifdef STEAM_WIN32
#pragma once
#endif

//-----------------------------------------------------------------------------
// Purpose: Functions for match making services for clients to get to favorites
//			and to operate on game lobbies.
//-----------------------------------------------------------------------------
class ISteamMatchmaking004
{
public:
    // game server favorites storage
    // saves basic details about a multiplayer game server locally

    // returns the number of favorites servers the user has stored
    virtual int GetFavoriteGameCount() = 0;

    // returns the details of the game server
    // iGame is of range [0,GetFavoriteGameCount())
    // *pnIP, *pnConnPort are filled in the with IP:port of the game server
    // *punFlags specify whether the game server was stored as an explicit favorite or in the history of connections
    // *pRTime32LastPlayedOnServer is filled in the with the Unix time the favorite was added
    virtual bool GetFavoriteGame(int iGame, AppId_t* pnAppID, uint32* pnIP, uint16* pnConnPort, uint16* pnQueryPort, uint32* punFlags, uint32* pRTime32LastPlayedOnServer) = 0;

    // adds the game server to the local list; updates the time played of the server if it already exists in the list
    virtual int AddFavoriteGame(AppId_t nAppID, uint32 nIP, uint16 nConnPort, uint16 nQueryPort, uint32 unFlags, uint32 rTime32LastPlayedOnServer) = 0;

    // removes the game server from the local storage; returns true if one was removed
    virtual bool RemoveFavoriteGame(AppId_t nAppID, uint32 nIP, uint16 nConnPort, uint16 nQueryPort, uint32 unFlags) = 0;

    ///////
    // Game lobby functions

    // Get a list of relevant lobbies
    // this is an asynchronous request
    // results will be returned by LobbyMatchList_t callback, with the number of servers requested
    // if the user is not currently connected to Steam (i.e. SteamUser()->BLoggedOn() returns false) then
    // a LobbyMatchList_t callback will be posted immediately with no servers
    virtual SteamAPICall_t RequestLobbyList() = 0;

    // filters for lobbies
    // this needs to be called before RequestLobbyList() to take effect
    // these are cleared on each call to RequestLobbyList()
    virtual void AddRequestLobbyListFilter(const char* pchKeyToMatch, const char* pchValueToMatch) = 0;
    // numerical comparison - 0 is equal, -1 is the lobby value is less than nValueToMatch, 1 is the lobby value is greater than nValueToMatch
    virtual void AddRequestLobbyListNumericalFilter(const char* pchKeyToMatch, int nValueToMatch, int nComparisonType /* 0 is equal, -1 is less than, 1 is greater than */) = 0;
    // sets RequestLobbyList() to only returns lobbies which aren't yet full - needs SetLobbyMemberLimit() called on the lobby to set an initial limit
    virtual void AddRequestLobbyListSlotsAvailableFilter() = 0;

    // returns the CSteamID of a lobby, as retrieved by a RequestLobbyList call
    // should only be called after a LobbyMatchList_t callback is received
    // iLobby is of the range [0, LobbyMatchList_t::m_nLobbiesMatching)
    // the returned CSteamID::IsValid() will be false if iLobby is out of range
    virtual CSteamID GetLobbyByIndex(int iLobby) = 0;

    // Create a lobby on the Steam servers.
    // If bPrivate is true, then the lobby will not be returned by any RequestLobbyList() call; the CSteamID
    // of the lobby will need to be communicated via game channels or via InviteUserToLobby()
    // this is an asynchronous request
    // results will be returned by LobbyCreated_t callback when the lobby has been created;
    // local user will the join the lobby, resulting in an additional LobbyEnter_t callback being sent
    // operations on the chat room can only proceed once the LobbyEnter_t has been received
    virtual void CreateLobby(bool bPrivate) = 0;

    // Joins an existing lobby
    // this is an asynchronous request
    // results will be returned by LobbyEnter_t callback when the lobby has been joined
    // users already in the lobby will receive LobbyChatUpdate_t callback after this user has successfully joined
    virtual SteamAPICall_t JoinLobby(CSteamID steamIDLobby) = 0;

    // Leave a lobby; this will take effect immediately on the client side
    // other users in the lobby will be notified by a LobbyChatUpdate_t callback
    virtual void LeaveLobby(CSteamID steamIDLobby) = 0;

    // Invite another user to the lobby
    // the target user will receive a LobbyInvite_t callback
    // will return true if the invite is successfully sent, whether or not the target responds
    // returns false if the local user is not connected to the Steam servers
    virtual bool InviteUserToLobby(CSteamID steamIDLobby, CSteamID steamIDInvitee) = 0;

    // Lobby iteration, for viewing details of users in a lobby
    // only accessible if the lobby user is a member of the specified lobby
    // persona information for other lobby members (name, avatar, etc.) will be asynchronously received
    // and accessible via ISteamFriends interface

    // returns the number of users in the specified lobby
    virtual int GetNumLobbyMembers(CSteamID steamIDLobby) = 0;
    // returns the CSteamID of a user in the lobby
    // iMember is of range [0,GetNumLobbyMembers())
    virtual CSteamID GetLobbyMemberByIndex(CSteamID steamIDLobby, int iMember) = 0;

    // Get data associated with this lobby
    // takes a simple key, and returns the string associated with it
    // "" will be returned if no value is set, or if steamIDLobby is invalid
    virtual const char* GetLobbyData(CSteamID steamIDLobby, const char* pchKey) = 0;
    // Sets a key/value pair in the lobby metadata
    // each user in the lobby will be broadcast this new value, and any new users joining will receive any existing data
    // this can be used to set lobby names, map, etc.
    // to reset a key, just set it to ""
    // other users in the lobby will receive notification of the lobby data change via a LobbyDataUpdate_t callback
    virtual bool SetLobbyData(CSteamID steamIDLobby, const char* pchKey, const char* pchValue) = 0;

    // As above, but gets per-user data for someone in this lobby
    virtual const char* GetLobbyMemberData(CSteamID steamIDLobby, CSteamID steamIDUser, const char* pchKey) = 0;
    // Sets per-user metadata (for the local user implicitly)
    virtual void SetLobbyMemberData(CSteamID steamIDLobby, const char* pchKey, const char* pchValue) = 0;

    // Broadcasts a chat message to the all the users in the lobby
    // users in the lobby (including the local user) will receive a LobbyChatMsg_t callback
    // returns true if the message is successfully sent
    // pvMsgBody can be binary or text data, up to 4k
    // if pvMsgBody is text, cubMsgBody should be strlen( text ) + 1, to include the null terminator
    virtual bool SendLobbyChatMsg(CSteamID steamIDLobby, const void* pvMsgBody, int cubMsgBody) = 0;
    // Get a chat message as specified in a LobbyChatMsg_t callback
    // iChatID is the LobbyChatMsg_t::m_iChatID value in the callback
    // *pSteamIDUser is filled in with the CSteamID of the member
    // *pvData is filled in with the message itself
    // return value is the number of bytes written into the buffer
    virtual int GetLobbyChatEntry(CSteamID steamIDLobby, int iChatID, CSteamID* pSteamIDUser, void* pvData, int cubData, EChatEntryType* peChatEntryType) = 0;

    // Fetch metadata for a lobby you're not necessarily in right now
    // this will send down all the metadata associated with a lobby
    // this is an asynchronous call
    // returns false if the local user is not connected to the Steam servers
    virtual bool RequestLobbyData(CSteamID steamIDLobby) = 0;

    // sets the game server associated with the lobby
    // usually at this point, the users will leave the lobby and join the specified game server
    // either the IP/Port or the steamID of the game server has to be valid, depending on how you want the clients to be able to connect
    virtual void SetLobbyGameServer(CSteamID steamIDLobby, uint32 unGameServerIP, uint16 unGameServerPort, CSteamID steamIDGameServer) = 0;
    // returns the details of a game server set in a lobby - returns false if there is no game server set, or that lobby doesn't exist
    virtual bool GetLobbyGameServer(CSteamID steamIDLobby, uint32* punGameServerIP, uint16* punGameServerPort, CSteamID* psteamIDGameServer) = 0;

    // set the limit on the # of users who can join the lobby
    virtual bool SetLobbyMemberLimit(CSteamID steamIDLobby, int cMaxMembers) = 0;
    // returns the current limit on the # of users who can join the lobby; returns 0 if no limit is defined
    virtual int GetLobbyMemberLimit(CSteamID steamIDLobby) = 0;

    // asks the Steam servers for a list of lobbies that friends are in
    // returns results by posting one RequestFriendsLobbiesResponse_t callback per friend/lobby pair
    // if no friends are in lobbies, RequestFriendsLobbiesResponse_t will be posted but with 0 results
    // filters don't apply to lobbies (currently)
    virtual bool RequestFriendsLobbies() = 0;
};

#define STEAMMATCHMAKING_INTERFACE_VERSION004 "SteamMatchMaking004"

enum EMatchMakingType
{
    eInternetServer = 0,
    eLANServer,
    eFriendsServer,
    eFavoritesServer,
    eHistoryServer,
    eSpectatorServer,
    eInvalidServer
};

class ISteamMatchmakingServers001
{
public:
    // Request a new list of servers of a particular type.  These calls each correspond to one of the EMatchMakingType values.
    virtual HServerListRequest RequestInternetServerList(AppId_t iApp, MatchMakingKeyValuePair_t** ppchFilters, uint32 nFilters, ISteamMatchmakingServerListResponse* pRequestServersResponse) = 0;
    virtual HServerListRequest RequestLANServerList(AppId_t iApp, ISteamMatchmakingServerListResponse* pRequestServersResponse) = 0;
    virtual HServerListRequest RequestFriendsServerList(AppId_t iApp, MatchMakingKeyValuePair_t** ppchFilters, uint32 nFilters, ISteamMatchmakingServerListResponse* pRequestServersResponse) = 0;
    virtual HServerListRequest RequestFavoritesServerList(AppId_t iApp, MatchMakingKeyValuePair_t** ppchFilters, uint32 nFilters, ISteamMatchmakingServerListResponse* pRequestServersResponse) = 0;
    virtual HServerListRequest RequestHistoryServerList(AppId_t iApp, MatchMakingKeyValuePair_t** ppchFilters, uint32 nFilters, ISteamMatchmakingServerListResponse* pRequestServersResponse) = 0;
    virtual HServerListRequest RequestSpectatorServerList(AppId_t iApp, MatchMakingKeyValuePair_t** ppchFilters, uint32 nFilters, ISteamMatchmakingServerListResponse* pRequestServersResponse) = 0;

    /* the filters that are available in the ppchFilters params are:

        "map"		- map the server is running, as set in the dedicated server api
        "dedicated" - reports bDedicated from the API
        "secure"	- VAC-enabled
        "full"		- not full
        "empty"		- not empty
        "noplayers" - is empty
        "proxy"		- a relay server

    */

    // Get details on a given server in the list, you can get the valid range of index
    // values by calling GetServerCount().  You will also receive index values in 
    // ISteamMatchmakingServerListResponse::ServerResponded() callbacks
    virtual gameserveritem_t* GetServerDetails(EMatchMakingType eType, int iServer) = 0;

    // Cancel an request which is operation on the given list type.  You should call this to cancel
    // any in-progress requests before destructing a callback object that may have been passed 
    // to one of the above list request calls.  Not doing so may result in a crash when a callback
    // occurs on the destructed object.
    virtual void CancelQuery(EMatchMakingType eType) = 0;

    // Ping every server in your list again but don't update the list of servers
    virtual void RefreshQuery(EMatchMakingType eType) = 0;

    // Returns true if the list is currently refreshing its server list
    virtual bool IsRefreshing(EMatchMakingType eType) = 0;

    // How many servers in the given list, GetServerDetails above takes 0... GetServerCount() - 1
    virtual int GetServerCount(EMatchMakingType eType) = 0;

    // Refresh a single server inside of a query (rather than all the servers )
    virtual void RefreshServer(EMatchMakingType eType, int iServer) = 0;


    //-----------------------------------------------------------------------------
    // Queries to individual servers directly via IP/Port
    //-----------------------------------------------------------------------------

    // Request updated ping time and other details from a single server
    virtual HServerQuery PingServer(uint32 unIP, uint16 usPort, ISteamMatchmakingPingResponse* pRequestServersResponse) = 0;

    // Request the list of players currently playing on a server
    virtual HServerQuery PlayerDetails(uint32 unIP, uint16 usPort, ISteamMatchmakingPlayersResponse* pRequestServersResponse) = 0;

    // Request the list of rules that the server is running (See ISteamMasterServerUpdater->SetKeyValue() to set the rules server side)
    virtual HServerQuery ServerRules(uint32 unIP, uint16 usPort, ISteamMatchmakingRulesResponse* pRequestServersResponse) = 0;

    // Cancel an outstanding Ping/Players/Rules query from above.  You should call this to cancel
    // any in-progress requests before destructing a callback object that may have been passed 
    // to one of the above calls to avoid crashing when callbacks occur.
    virtual void CancelServerQuery(HServerQuery hServerQuery) = 0;
};
#define STEAMMATCHMAKINGSERVERS_INTERFACE_VERSION001 "SteamMatchMakingServers001"

#endif // ISTEAMMATCHMAKING004
