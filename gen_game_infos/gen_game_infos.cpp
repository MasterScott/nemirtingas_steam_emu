
#define NOMINMAX 1

#include <fstream>
#include <iostream>
#include <string>
#include <iomanip>
#include <algorithm>

#include <nlohmann/json.hpp>
#include "easy_curl.h"

template<size_t N>
constexpr size_t static_strlen(const char(&)[N])
{
    return N - 1;
}

std::string to_lower(std::string const& _str)
{
    std::string str(_str);
    std::transform(str.begin(), str.end(), str.begin(), [](unsigned char c)
    {
        return std::tolower(c);
    });

    return str;
}

bool save_json(std::string const& file_path, nlohmann::json const& json)
{
    std::ofstream file(file_path, std::ios::trunc | std::ios::out);
    if (!file)
    {
        std::cout << "Failed to save: " << file_path << std::endl;
        return false;
    }
    file << std::setw(2) << json;
    return true;
}

#if defined(WIN32) || defined(_WIN32)
#include <windows.h>

static bool create_directory(std::string const& strPath)
{
    DWORD dwAttrib = GetFileAttributesA(strPath.c_str());

    if (dwAttrib != INVALID_FILE_ATTRIBUTES && dwAttrib & FILE_ATTRIBUTE_DIRECTORY)
        return true;

    return CreateDirectoryA(strPath.c_str(), NULL);
}
#elif defined(__linux__) || defined(__APPLE__)
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

static bool create_directory(std::string const& strPath)
{
    struct stat sb;

    if (stat(strPath.c_str(), &sb) != 0)
    {
        return mkdir(strPath.c_str(), 0755) == 0;
    }
    if (S_ISDIR(sb.st_mode))
        return true;

    return false;
}

#endif

std::string steam_apikey;
std::string app_id;
std::string language;

void generate_emu_conf()
{
    CurlEasy easy;
    std::string achievments_path = "db_achievments.json";

    if (steam_apikey.empty() || !easy.init())
        return;

    std::string game_url("https://store.steampowered.com/api/appdetails/?l=english&appids=");
    game_url += app_id;

    easy.set_url(game_url);
    easy.skip_verifypeer();
    easy.skip_verifyhost();

    // Try to read game name, languages from steam
    easy.perform();
    nlohmann::json settings;

    std::cout << "Creating settings file" << std::endl;

    settings["appid"] = std::stoul(app_id);
    settings["language"] = "english";
    settings["languages"] = "english";
    settings["gamename"] = "SpaceWar";
    settings["unlock_dlcs"] = true;
    settings["enable_overlay"] = true;
    settings["beta"] = false;
    settings["beta_name"] = "public";
    settings["disable_online_networking"] = false;
    settings["username"] = "DefaultName";
    settings["savepath"] = "appdata";
    settings["log_level"] = "OFF";

    nlohmann::json game_json;
    try
    {
        game_json = nlohmann::json::parse(easy.get_answer());
    }
    catch (...)
    {
        game_json = nlohmann::json::object();
    }

    long html_code;
    easy.get_html_code(html_code);
    if (html_code == 200 && game_json[app_id]["success"] == true)
    {
        // Get the game name
        settings["gamename"] = game_json[app_id]["data"]["name"].get_ref<const std::string&>();
        
        // Get the game's supported languages
        auto& steam_languages = game_json[app_id]["data"]["supported_languages"].get_ref<std::string&>();

        size_t pos;
        while ((pos = steam_languages.find("<strong>*</strong>")) != std::string::npos)
            steam_languages.erase(pos, static_strlen("<strong>*</strong>"));

        pos = steam_languages.find("<br>");
        if (pos != std::string::npos)
            steam_languages.erase(steam_languages.begin() + pos, steam_languages.end());

        while ((pos = steam_languages.find(", ")) != std::string::npos)
            steam_languages.replace(pos, static_strlen(", "), ",");

        if ((pos = steam_languages.find("Portuguese - Brazil")) != std::string::npos)
            steam_languages.replace(pos, static_strlen("Portuguese - Brazil"), "Portuguese");

        if ((pos = steam_languages.find("Spanish - Spain")) != std::string::npos)
            steam_languages.replace(pos, static_strlen("Spanish - Spain"), "Spanish");

        if ((pos = steam_languages.find("Spanish - Latin America")) != std::string::npos)
            steam_languages.replace(pos, static_strlen("Spanish - Latin America"), "Latam");

        if ((pos = steam_languages.find("Traditional Chinese")) != std::string::npos)
            steam_languages.replace(pos, static_strlen("Traditional Chinese"), "tchinese");

        if ((pos = steam_languages.find("Simplified Chinese")) != std::string::npos)
            steam_languages.replace(pos, static_strlen("Simplified Chinese"), "schinese");

        steam_languages = to_lower(steam_languages);

        settings["languages"] = steam_languages;

        if (steam_languages.find(language) != std::string::npos)
        {
            settings["language"] = language;
        }
    }

    save_json("NemirtingasSteamEmu.json", settings);
}

void generate_achievments_db()
{
    CurlEasy easy;
    std::string achievments_path = "db_achievments.json";

    if (steam_apikey.empty() || !easy.init())
        return;

    std::string ach_url = "https://api.steampowered.com/ISteamUserStats/GetSchemaForGame/v2/?key=";
    ach_url += steam_apikey;
    ach_url += "&appid=";
    ach_url += app_id;
    ach_url += "&l=";
    ach_url += language;

    easy.set_url(ach_url);
    easy.skip_verifypeer();
    easy.skip_verifyhost();

    easy.perform();

    long html_code;
    easy.get_html_code(html_code);
    try
    {
        if (html_code == 200)
        {
            create_directory("achievements_cache");

            nlohmann::json ach_json = nlohmann::json::parse(easy.get_answer());

            std::cout << "Downloading achievements images (may take a while...)" << std::endl;
            size_t pos;
            for (auto& achievement : ach_json["game"]["availableGameStats"]["achievements"])
            {
                try
                {
                    std::string& icon = achievement["icon"].get_ref<std::string&>();
                    std::string& icon_gray = achievement["icongray"].get_ref<std::string&>();

                    easy.set_url(icon);
                    easy.perform();
                    icon = "achievements_cache/" + achievement["name"].get_ref<std::string&>() + ".jpg";
                    easy.get_html_code(html_code);
                    if (html_code == 200)
                    {
                        std::string const& img = easy.get_answer();
                        std::ofstream fimg("achievements_cache/" + icon, std::ios::out | std::ios::trunc | std::ios::binary);
                        fimg.write(img.data(), img.size());
                    }

                    easy.set_url(icon_gray);
                    easy.perform();
                    icon_gray = "achievements_cache/" + achievement["name"].get_ref<std::string&>() + "_gray.jpg";
                    easy.get_html_code(html_code);
                    if (html_code == 200)
                    {
                        std::string const& img = easy.get_answer();
                        std::ofstream fimg("achievements_cache/" + icon_gray, std::ios::out | std::ios::trunc | std::ios::binary);
                        fimg.write(img.data(), img.size());
                    }
                }
                catch(...)
                { }
            }

            std::cout << "Creating achievements file" << std::endl;
            save_json(achievments_path, ach_json["game"]["availableGameStats"]["achievements"]);
        }
    }
    catch (...)
    {
        std::cout << "Failed at creating achievements file" << std::endl;
    }
}

void generate_items_db()
{
    CurlEasy easy;
    std::string items_path = "db_inventory.json";

    if (steam_apikey.empty() || !easy.init())
        return;

    std::string metadata_url = "https://api.steampowered.com/IInventoryService/GetItemDefMeta/v1?key=";
    metadata_url += steam_apikey;
    metadata_url += "&appid=";
    metadata_url += app_id;

    easy.set_url(metadata_url);
    easy.skip_verifypeer();
    easy.skip_verifyhost();

    easy.perform();

    long html_code;
    easy.get_html_code(html_code);
    try
    {
        std::cout << "Creating items database file" << std::endl;
        if (html_code == 200)
        {
            nlohmann::json items_json = nlohmann::json::parse(easy.get_answer());

            std::string digest = items_json["response"]["digest"];

            std::string items_url = "https://api.steampowered.com/IGameInventory/GetItemDefArchive/v0001?appid=";
            items_url += app_id;
            items_url += "&digest=";
            items_url += digest;
            items_url += "&l=";
            items_url += language;

            easy.set_url(items_url);
            easy.perform();

            long html_code;
            easy.get_html_code(html_code);
            if (html_code == 200 && items_json.size() > 0)
            {
                items_json = nlohmann::json::parse(easy.get_answer());

                save_json(items_path, items_json);
            }
        }
    }
    catch (...)
    {
    }
}

void generate_dlcs_db()
{
    CurlEasy easy;
    std::string dlcs_path = "dlcs.json";

    if (!easy.init())
        return;

    std::string dlcs_url = "https://store.steampowered.com/api/appdetails/?appids=";
    dlcs_url += app_id;
    dlcs_url += "&l=";
    dlcs_url += language;

    easy.set_url(dlcs_url);
    easy.skip_verifypeer();
    easy.skip_verifyhost();

    easy.perform();

    long html_code;
    easy.get_html_code(html_code);
    try
    {
        std::cout << "Creating dlcs file" << std::endl;
        std::string appid = app_id;
        nlohmann::json game_json = nlohmann::json::parse(easy.get_answer());
        nlohmann::json dlcs_json;

        if (html_code == 200 && game_json[appid]["success"] == true)
        {
            auto& dlcs = game_json[appid]["data"]["dlc"];
            for (auto dlc_it = dlcs.begin(); dlc_it != dlcs.end();)
            {
                uint32_t dlc_id = (*dlc_it).get<uint32_t>();
                std::string str_dlc_id = std::to_string(dlc_id);

                auto settings_dlc_it = dlcs_json.find(str_dlc_id);
                if (settings_dlc_it == dlcs_json.end())
                {
                    dlcs_url = "https://store.steampowered.com/api/appdetails/?appids=";
                    dlcs_url += std::to_string(dlc_id);
                    dlcs_url += "&l=";
                    dlcs_url += language;

                    easy.set_url(dlcs_url);
                    easy.perform();
                    nlohmann::json dlc_json = nlohmann::json::parse(easy.get_answer());
                    if (dlc_json[str_dlc_id]["success"] == true)
                        dlcs_json[str_dlc_id]["dlc_name"] = dlc_json[str_dlc_id]["data"]["name"].get_ref<std::string const&>();

                    dlcs_json[str_dlc_id]["enabled"] = false;

                    // Next dlc only if we succesfuly parsed this dlc
                    ++dlc_it;
                }
                else
                {
                    try
                    {
                        if ((*settings_dlc_it)["dlc_name"].get_ref<std::string const&>().empty())
                        {// If there is no name, try to find it from steam
                            dlcs_url = "https://store.steampowered.com/api/appdetails/?appids=";
                            dlcs_url += std::to_string(dlc_id);
                            dlcs_url += "&l=";
                            dlcs_url += language;

                            easy.set_url(dlcs_url);
                            easy.perform();
                            nlohmann::json dlc_json = nlohmann::json::parse(easy.get_answer());
                            if (dlc_json[str_dlc_id]["success"] == true)
                                (*settings_dlc_it)["dlc_name"] = dlc_json[str_dlc_id]["data"]["name"].get_ref<std::string const&>();
                        }

                        // Next dlc only if we succesfuly parsed this dlc
                        ++dlc_it;
                    }
                    catch (...)
                    {// Remove the invalid dlc from settings
                        dlcs_json.erase(settings_dlc_it);

                        // Reparse the current dlc
                    }
                }
            }
        }

        if (dlcs_json.size() > 0)
        {
            save_json(dlcs_path, dlcs_json);
        }
    }
    catch (...)
    {
        std::cout << "Failed at creating dlcs file" << std::endl;
    }
}


int main(int argc, char* argv[])
{
    CurlEasy easy;

    CurlGlobal& cglobal = CurlGlobal::Inst();
    cglobal.init();

    if (!easy.init())
    {
        return -1;
    }

    easy.skip_verifypeer();

    if (argc > 2) {
        if (argc > 3)
            language = argv[3];
        else
            language = "english";

        app_id = argv[2];
        steam_apikey = argv[1];
    }
    else {
        std::cout << "Usage: " << argv[0] << " steam_api_key app_id [language, default english]" << std::endl;
        std::cout << "Enter the game appid: ";
        std::cin >> app_id;
        std::cout << "Enter your webapi key: ";
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cin >> steam_apikey;
        std::cout << "Enter your desired language: ";
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cin >> language;
    }

    generate_emu_conf();
    generate_achievments_db();
    generate_dlcs_db();
    generate_items_db();

    return 0;
}
