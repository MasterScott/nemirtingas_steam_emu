#include <servers/bootstrap.h>
#include <launch.h>

#include <unistd.h>

#include <iostream>
#include <fstream>
#include <string>

#include <mach-o/dyld_images.h>

#define STEAM_IPC_VERSION            104

#define STEAM_IPC_5                  5
#define STEAM_IPC_GETSTEAMPID        11
#define STEAM_IPC_SETSTEAMPID        12
#define STEAM_IPC_GETSTEAMPATH       13
#define STEAM_IPC_GETSTEAMCLIENTPATH 14
#define STEAM_IPC_KILLCMD            100000
#define STEAM_IPC_GETVERSION         100001

static char steam_ipc_server[] = "com.valvesoftware.steam.ipctool";

static int steam_ipc_version = STEAM_IPC_VERSION;

// Message received has a mach_msg_trailer_t
struct steam_ipc_cmd
{
    mach_msg_header_t msg;
    int msg_type;
    int arg1;
    char buff[512];
    mach_msg_trailer_t trailer;
};

// Message sent doesn't have a mach_msg_trailer_t
struct SteamMachMsgVersionSend
{
    mach_msg_header_t msg;
    int unused;
    int version;
};

struct SteamMachMsg5Send
{
    mach_msg_header_t msg;
    int unknown1;
    int unknown2;
    int unknown3;
};

struct SteamMachMsgPathSend
{
    mach_msg_header_t msg;
    int unused;
    int pid;
    char buff[512];
};

std::string get_executable_path()
{
    std::string exec_path("./");

    task_dyld_info dyld_info;
    task_t t;
    pid_t pid = getpid();
    task_for_pid(mach_task_self(), pid, &t);
    mach_msg_type_number_t count = TASK_DYLD_INFO_COUNT;

    if (task_info(t, TASK_DYLD_INFO, reinterpret_cast<task_info_t>(&dyld_info), &count) == KERN_SUCCESS)
    {
        dyld_all_image_infos *dyld_img_infos = reinterpret_cast<dyld_all_image_infos*>(dyld_info.all_image_info_addr);
        for (int i = 0; i < dyld_img_infos->infoArrayCount; ++i)
        {
            exec_path = dyld_img_infos->infoArray[i].imageFilePath;
            break;
        }
    }

    exec_path = exec_path.substr(0, exec_path.rfind('/') + 1);

    return exec_path;
}

int main(int argc, char *argv[])
{
    mach_port_t server_priv_port;
    mach_port_t service_send_port, service_rcv_port;

    std::string executable_path = get_executable_path();

    std::string steam_path;
    pid_t steam_pid = getpid();

    kern_return_t kr;
    kr = bootstrap_check_in(bootstrap_port, steam_ipc_server, &service_rcv_port);

    if(kr == KERN_SUCCESS)
    {
        std::cout << "Found already started ipctool" << std::endl;
        server_priv_port = bootstrap_port;
    }
    else if(kr == BOOTSTRAP_UNKNOWN_SERVICE)
    {
        std::cout << "Creating new server" << std::endl;

        kr = bootstrap_create_service(bootstrap_port, steam_ipc_server, &server_priv_port);
        if(kr != KERN_SUCCESS)
        {
            std::cout << "Failed bootstrap_create_server" << std::endl;
        }
    }

    if(kr != KERN_SUCCESS)
    {
        std::cout << "Error: " << bootstrap_strerror(kr) << '(' << kr << ')' << std::endl;
    }
    else
    {
        steam_ipc_cmd ipc_cmd;
        while(1)
        {
            memset(&ipc_cmd, 0, sizeof(ipc_cmd));
            ipc_cmd.msg.msgh_size = sizeof(ipc_cmd);
            ipc_cmd.msg.msgh_local_port = service_rcv_port;
            if(mach_msg(&ipc_cmd.msg, MACH_RCV_MSG, 0, ipc_cmd.msg.msgh_size, ipc_cmd.msg.msgh_local_port, MACH_MSG_TIMEOUT_NONE, MACH_PORT_NULL) == KERN_SUCCESS)
            {
                switch(ipc_cmd.msg_type)
                {
                    case STEAM_IPC_5:
                    {
                        SteamMachMsg5Send buff;

                        std::cout << "Get IPC5(" << sizeof(buff) << ')' << std::endl;
                        
                        memset(&buff, 0, sizeof(buff));
                        buff.msg.msgh_size = sizeof(buff);
                        buff.msg.msgh_local_port = 0;
                        buff.msg.msgh_remote_port = ipc_cmd.msg.msgh_remote_port;
                        buff.msg.msgh_bits = (ipc_cmd.msg.msgh_bits >> 8) & 0x1F;
                        buff.msg.msgh_id = steam_ipc_version;
                        buff.unknown1 = 0;
                        buff.unknown2 = 0;
                        
                        mach_msg(&buff.msg, MACH_SEND_MSG|MACH_SEND_TIMEOUT, buff.msg.msgh_size, 0, 0, 2000, MACH_PORT_NULL);
                    };
                    break;

                    case STEAM_IPC_GETVERSION:
                    {
                        // Client sends us his required version
                        // this shoud always allow our ipc server to work on any client version
                        //steam_ipc_version = ipc_cmd.arg1;

                        SteamMachMsgVersionSend buff;

                        std::cout << "Get version(" << sizeof(buff) << ')' << std::endl;

                        memset(&buff, 0, sizeof(buff));
                        buff.msg.msgh_size = sizeof(buff);
                        buff.msg.msgh_local_port = 0;
                        buff.msg.msgh_remote_port = ipc_cmd.msg.msgh_remote_port;
                        buff.msg.msgh_bits = (ipc_cmd.msg.msgh_bits >> 8) & 0x1F;
                        buff.msg.msgh_id = steam_ipc_version;
                        buff.version = steam_ipc_version;
                        
                        mach_msg(&buff.msg, MACH_SEND_MSG|MACH_SEND_TIMEOUT, buff.msg.msgh_size, 0, 0, 2000, MACH_PORT_NULL);
                    }
                    break;

                    case STEAM_IPC_KILLCMD:
                    {
                        std::cout << "Kill process" << std::endl;
                    }
                    break;

                    case STEAM_IPC_GETSTEAMPID:
                    {
                        std::cout << "Get steam PID" << std::endl;
                    }
                    break;

                    case STEAM_IPC_SETSTEAMPID:
                    {
                        std::cout << "Set steam PATH/PID: " << ipc_cmd.buff << ' ' << ipc_cmd.arg1 << std::endl;
                        steam_path = ipc_cmd.buff;
                        steam_pid = ipc_cmd.arg1;
                    }
                    break;

                    case STEAM_IPC_GETSTEAMPATH:
                    {
                        SteamMachMsgPathSend buff;

                        std::cout << "Get steampath(" << sizeof(buff) << ')' << std::endl;

                        memset(&buff, 0, sizeof(buff));
                        buff.msg.msgh_size = sizeof(buff);
                        buff.msg.msgh_local_port = 0;
                        buff.msg.msgh_remote_port = ipc_cmd.msg.msgh_remote_port;
                        buff.msg.msgh_bits = (ipc_cmd.msg.msgh_bits >> 8) & 0x1F;
                        buff.msg.msgh_id = steam_ipc_version;
                        buff.unused = ipc_cmd.msg_type;
                        buff.pid = steam_pid;
                        strncpy(buff.buff, executable_path.c_str(), sizeof(buff.buff)/sizeof(*buff.buff));
                        
                        mach_msg(&buff.msg, MACH_SEND_MSG|MACH_SEND_TIMEOUT, buff.msg.msgh_size, 0, 0, 2000, MACH_PORT_NULL);
                    }
                    break;

                    case STEAM_IPC_GETSTEAMCLIENTPATH:
                    {
                        SteamMachMsgPathSend buff;

                        std::cout << "Get steamclientpath(" << sizeof(buff) << ')' << std::endl;

                        memset(&buff, 0, sizeof(buff));
                        buff.msg.msgh_size = sizeof(buff);
                        buff.msg.msgh_local_port = 0;
                        buff.msg.msgh_remote_port = ipc_cmd.msg.msgh_remote_port;
                        buff.msg.msgh_bits = (ipc_cmd.msg.msgh_bits >> 8) & 0x1F;
                        buff.msg.msgh_id = steam_ipc_version;
                        buff.pid = steam_pid;
                        strncpy(buff.buff, executable_path.c_str(), sizeof(buff.buff)/sizeof(*buff.buff));
                        
                        std::cout << "Answering: " << executable_path << std::endl;
                        mach_msg(&buff.msg, MACH_SEND_MSG|MACH_SEND_TIMEOUT, buff.msg.msgh_size, 0, 0, 2000, MACH_PORT_NULL);
                    }
                    break;

                    default:
                    {
                        std::cout << "Unused:" << ipc_cmd.msg_type << std::endl;
                    }
                }
            }
            else
            {
                std::cout << "Error" << std::endl;
            }
        }
    }

    return 0;
}
