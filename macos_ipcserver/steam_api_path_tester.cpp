#include <iostream>
#include <chrono>
#include <iomanip>

#include <sys/sysctl.h>

#include <mach-o/dyld_images.h>
#include <bootstrap.h>
#include <launch.h>

template<typename Clock, typename Duration>
std::ostream& operator<<(std::ostream &stream, const std::chrono::time_point<Clock, Duration> &time_point) {
    const time_t time = Clock::to_time_t(time_point);

    struct tm tm;
    localtime_r(&time, &tm);

    return stream << std::put_time(&tm, "%c");
}

std::chrono::system_clock::time_point get_boottime()
{
    static bool has_boottime = false;
    static std::chrono::system_clock::time_point boottime(std::chrono::seconds(0));
    if(!has_boottime)
    {
        struct timeval boottime_tv;
        size_t len = sizeof(boottime_tv);
        int mib[2] = { CTL_KERN, KERN_BOOTTIME };
        if (sysctl(mib, sizeof(mib)/sizeof(*mib), &boottime_tv, &len, nullptr, 0) < 0 )
            return boottime;

        boottime = std::chrono::system_clock::time_point(
            std::chrono::seconds(boottime_tv.tv_sec) +
            std::chrono::microseconds(boottime_tv.tv_usec));
        has_boottime = true;
    }

    return boottime;
}

std::chrono::microseconds get_uptime()
{
    return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - get_boottime());
}

#define PATH_SEPARATOR '/'

std::string get_executable_path()
{
    std::string exec_path("./");

    task_dyld_info dyld_info;
    task_t t;
    pid_t pid = getpid();
    task_for_pid(mach_task_self(), pid, &t);
    mach_msg_type_number_t count = TASK_DYLD_INFO_COUNT;

    if (task_info(t, TASK_DYLD_INFO, reinterpret_cast<task_info_t>(&dyld_info), &count) == KERN_SUCCESS)
    {
        dyld_all_image_infos *dyld_img_infos = reinterpret_cast<dyld_all_image_infos*>(dyld_info.all_image_info_addr);
        for(int i = 0; i < dyld_img_infos->infoArrayCount; ++i)
        {
            exec_path = dyld_img_infos->infoArray[i].imageFilePath;
            break;
        }
    }

    exec_path = exec_path.substr(0, exec_path.rfind(PATH_SEPARATOR) + 1);
    return exec_path;
}

#define STEAM_IPC_VERSION      104

#define STEAM_IPC_GETSTEAMPATH       13
#define STEAM_IPC_GETSTEAMCLIENTPATH 14
#define STEAM_IPC_KILLCMD            100000
#define STEAM_IPC_GETVERSION         100001

struct steam_ipc_cmd
{
    mach_msg_header_t msg;
    int msg_type;
    int arg1;
};

struct SteamMachMsgVersionRecv
{
    mach_msg_header_t msg;
    int unused;
    int version;
    mach_msg_trailer_t trailer;
};

struct SteamMachMsgPathRecv
{
    mach_msg_header_t msg;
    int unused;
    int pid;
    char buff[512];
    mach_msg_trailer_t trailer;
};

class CSimpleMachClient
{
protected:
    mach_port_t port;
    mach_port_name_t port_name;
    int version;

public:
    CSimpleMachClient():
        port(0),
        port_name(0)
    {}

    ~CSimpleMachClient()
    {
        if(port)
        {
            mach_port_deallocate(mach_task_self(), port);
            port = 0;
        }
        if(port_name)
        {
            mach_port_deallocate(mach_task_self(), port_name);
            port_name = 0;
        }
    }

    bool Init(const char* service_name, int version)
    {
        this->version = version;
        return (port || !bootstrap_look_up(bootstrap_port, service_name, &port))
            && (port_name || !mach_port_allocate(mach_task_self(), MACH_PORT_RIGHT_RECEIVE, &port_name));
    }

    bool Send(mach_msg_header_t* msg, mach_msg_timeout_t timeout, int* res)
    {
        mach_msg_option_t option = MACH_SEND_MSG;
        mach_msg_return_t result;

        if(timeout != MACH_MSG_TIMEOUT_NONE)
            option |= MACH_SEND_TIMEOUT;
    
        msg->msgh_bits |= 0x1413;
        msg->msgh_remote_port = port;
        msg->msgh_local_port = port_name;
        msg->msgh_reserved = 0;
        msg->msgh_id = version;
        
        result = mach_msg(msg, option, msg->msgh_size, 0, 0, timeout, MACH_PORT_NULL);
        if(res != nullptr)
            *res = result;
        
        return result == 0;
    }

    bool Receive(mach_msg_header_t* msg, mach_msg_timeout_t timeout, int* res)
    {
        mach_msg_option_t option = MACH_RCV_MSG;
        mach_msg_return_t result;

        if(timeout != MACH_MSG_TIMEOUT_NONE)
            option |= MACH_RCV_TIMEOUT;
        
        while(1)
        {
            msg->msgh_remote_port = port;
            msg->msgh_local_port = port_name;
            result = mach_msg(msg, option, 0, msg->msgh_size, port_name, timeout, MACH_PORT_NULL);
            if(res != nullptr)
                *res = result;
            if(result)
                break;

            if(msg->msgh_id == version)
                return true;
        }

        return false;
    }

    bool SendAndReceive(mach_msg_header_t* msg_send, mach_msg_header_t* msg_recv, mach_msg_timeout_t timeout, int* res)
    {
        if( !Send(msg_send, timeout, res) )
            return false;

        return Receive(msg_recv, timeout, res);
    }

    int DoVersionCommand()
    {
        int version;
        steam_ipc_cmd msg_send;
        SteamMachMsgVersionRecv msg_recv;
    
        memset(&msg_recv, 0, sizeof(msg_recv));
        memset(&msg_send, 0, sizeof(msg_send));

        msg_recv.msg.msgh_size = sizeof(msg_recv);

        msg_send.msg.msgh_id = 0;
        msg_send.msg.msgh_reserved = 0;
        msg_send.msg.msgh_size = sizeof(msg_send);
        msg_send.msg_type = STEAM_IPC_GETVERSION;
        msg_send.arg1 = this->version;

        version = 0;
        if( SendAndReceive(&msg_send.msg, &msg_recv.msg, 3000, nullptr) )
        {
            std::cout << "received version:" << msg_recv.version << std::endl;
            version = msg_recv.version;
        }

        return version;
    }

    void DoKillCommand(const char* service_name)
    {
        steam_ipc_cmd msg;
        memset(&msg.msg, 0, sizeof(msg.msg));
        msg.msg.msgh_size = sizeof(msg);
        msg.msg.msgh_bits = 0x1413;
        msg.msg.msgh_remote_port = port;
        msg.msg.msgh_local_port = port_name;
        msg.msg.msgh_reserved = 0;
        msg.msg.msgh_id = version;
        msg.msg_type = STEAM_IPC_KILLCMD;
        msg.arg1 = getpid();
        if( mach_msg(&msg.msg, MACH_SEND_MSG | MACH_SEND_TIMEOUT, sizeof(msg), 0, 0, 2000, 0) )
        {
            launch_data_t launch_dict;
            launch_data_t launch_str;
            launch_data_t res;

            launch_dict = launch_data_alloc(LAUNCH_DATA_DICTIONARY);
            launch_str = launch_data_new_string(service_name);
            launch_data_dict_insert(launch_dict, launch_str, "StopJob");
            
            res = launch_msg(launch_dict);

            launch_data_free(launch_dict);
            if(res)
                launch_data_free(res);
        }
        mach_port_deallocate(mach_task_self(), port);
        port = 0;
        usleep(100000);
    }
};

class CIPCToolMachClient : public CSimpleMachClient
{
public:
    bool Init()
    {
        bool res = CSimpleMachClient::Init("com.valvesoftware.steam.ipctool", STEAM_IPC_VERSION);
        if(res)
        {
            version = DoVersionCommand();
            if( version < STEAM_IPC_VERSION )
            {
                DoKillCommand("com.valvesoftware.steam.ipctool");
                res = CSimpleMachClient::Init("com.valvesoftware.steam.ipctool", STEAM_IPC_VERSION);
                std::cerr << "ipcserver restart: expected: " << STEAM_IPC_VERSION << ", got: " << version << std::endl;
            }
        }
        std::cout << port << std::endl;
        return res;
    }

    bool GetSteammPath(char *path, size_t path_len, pid_t *pid)
    {
        bool res;

        steam_ipc_cmd msg_send;
        SteamMachMsgPathRecv msg_recv;

        memset(&msg_send, 0, sizeof(msg_send));
        memset(&msg_recv, 0, sizeof(msg_recv));
    
        msg_recv.msg.msgh_size = sizeof(msg_recv);

        msg_send.msg.msgh_size = sizeof(msg_send);
        msg_send.msg_type = STEAM_IPC_GETSTEAMPATH;

        res = SendAndReceive(&msg_send.msg, &msg_recv.msg, 5000u, nullptr);

        if(path != nullptr && path_len != 0)
            strncpy(path, msg_recv.buff, path_len);

        if(pid != nullptr)
            *pid = msg_recv.pid;

        return res;
    }

    bool GetSteamPath(char *path, size_t path_len, pid_t *pid)
    {
        bool res;

        steam_ipc_cmd msg_send;
        SteamMachMsgPathRecv msg_recv;

        memset(&msg_send, 0, sizeof(msg_send));
        memset(&msg_recv, 0, sizeof(msg_recv));
    
        msg_recv.msg.msgh_size = sizeof(msg_recv);

        msg_send.msg.msgh_size = sizeof(msg_send);
        msg_send.msg_type = STEAM_IPC_GETSTEAMCLIENTPATH;

        res = SendAndReceive(&msg_send.msg, &msg_recv.msg, 5000u, nullptr);

        if(path != nullptr && path_len != 0)
            strncpy(path, msg_recv.buff, path_len);

        if(pid != nullptr)
            *pid = msg_recv.pid;

        return res;
    }
};

int main(int argc, char *argv[])
{
    mach_port_t sp;
    
    CIPCToolMachClient mach_client;
    if(mach_client.Init())
        mach_client.DoKillCommand("com.valvesoftware.steam.ipctool");

    if(!mach_client.Init())
    {
        std::cout << "Failed to connect to steam IPC" << std::endl;
    }
    else
    {
        {
            char xxx[1024];
            pid_t pid;
            mach_client.GetSteammPath(xxx, 1024, &pid);
            std::cout << xxx << ' ' << pid << std::endl;
        }
        pid_t pid;
        char client_path[1024];
        if(!mach_client.GetSteamPath(client_path, sizeof(client_path)/sizeof(*client_path), &pid) || pid == 0)
        {
            std::cout << "Failed to get steam PATH" << std::endl;
        }
        else
        {
            std::cout << client_path << std::endl;

            if(client_path[0] == 0)
            {
                strncpy(client_path, "./", (sizeof(client_path)/sizeof(*client_path))-1);
                client_path[(sizeof(client_path)/sizeof(*client_path))-1] = 0;
            }

            
            char* index = rindex(client_path, PATH_SEPARATOR);
            strncpy(index+1, "steamclient.dylib", (sizeof(client_path)/sizeof(*client_path))-1);

            std::cout << client_path << std::endl;
        }
    }

    return 0;
}
