/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "audio_manager.h"

#define STB_VORBIS_STATIC
#include <stb_vorbis.h>

bool PortAudio::load()
{
    if (_lib.load_library(FileManager::join(FileManager::dirname(get_module_path()), "plugins", "portaudio")))
    {
        Pa_Initialize = _lib.get_func<decltype(::Pa_Initialize)>("Pa_Initialize");
        Pa_Terminate = _lib.get_func<decltype(::Pa_Terminate)>("Pa_Terminate");
        Pa_OpenDefaultStream = _lib.get_func<decltype(::Pa_OpenDefaultStream)>("Pa_OpenDefaultStream");
        Pa_StartStream = _lib.get_func<decltype(::Pa_StartStream)>("Pa_StartStream");
        Pa_StopStream = _lib.get_func<decltype(::Pa_StopStream)>("Pa_StopStream");
    }
    return is_loaded();
}

bool PortAudio::is_loaded() const
{
    return _lib.is_loaded() && Pa_Initialize != nullptr && Pa_Terminate != nullptr && Pa_OpenDefaultStream != nullptr && Pa_StartStream != nullptr && Pa_StopStream != nullptr;
}

AudioManager::AudioManager():
    _can_play_audio(false)
{
    if (_portaudio.load())
    {
        _can_play_audio = _portaudio.Pa_Initialize() == paNoError;
    }
}

AudioManager::~AudioManager()
{
    if (_can_play_audio)
    {
        _portaudio.Pa_Terminate();
    }
}

inline AudioManager& AudioManager::Inst()
{
    static AudioManager inst;
    return inst;
}

bool AudioManager::audio_exists(std::string const& id)
{
    auto& self = Inst();
    return self._audios.find(id) != self._audios.end();
}

std::shared_ptr<Audio> AudioManager::get_audio(std::string const& id)
{
    auto& self = Inst();
    auto it = self._audios.find(id);
    if (it == self._audios.end())
        return std::shared_ptr<Audio>(nullptr);

    return it->second;
}

std::shared_ptr<Audio> AudioManager::create_audio(void const* data, size_t data_len, int32_t channels, int32_t sample_rate, std::string const& id)
{
    auto& self = Inst();
    std::shared_ptr<Audio> res = std::make_shared<Audio>();

    res->reserve_audio_buffer(data_len / sizeof(uint16_t), channels, sample_rate);
    if (data != nullptr)
        memcpy(res->get_raw_pointer(), data, res->raw_size());

    res->_id = id;
    self._audios.emplace(id, res);

    return res;
}

bool AudioManager::delete_audio(std::string const& id)
{
    auto& self = Inst();
    auto it = self._audios.find(id);
    if (it == self._audios.end())
        return false;

    self._audios.erase(it);
    return true;
}

bool AudioManager::delete_audio(std::shared_ptr<Audio> audio)
{
    auto& self = Inst();
    auto it = std::find_if(self._audios.begin(), self._audios.end(), [&audio](std::pair<const std::string, std::shared_ptr<Audio>>& item)
    {
        return audio == item.second;
    });
    if (it == self._audios.end())
        return false;

    self._audios.erase(it);
    return true;
}

// Load audio from path. If it fails, the pointer will be nullptr
std::shared_ptr<Audio> AudioManager::load_audio(std::string const& audio_path, std::string const& id)
{
    std::shared_ptr<Audio> res;

    std::ifstream f(audio_path, std::ios::in | std::ios::binary);
    if (f.is_open())
    {
        f.seekg(0, std::ios::end);
        size_t file_size = f.tellg();
        f.seekg(0, std::ios::beg);

        char* buff = new char[file_size];
        f.read(buff, file_size);

        res = load_audio(buff, file_size, id);
        delete[]buff;
    }

    return res;
}
// Load audio from buffer. If it fails, the pointer will be nullptr
std::shared_ptr<Audio> AudioManager::load_audio(void const* data, size_t data_len, std::string const& id)
{
    auto& self = Inst();
    std::shared_ptr<Audio> res;
    short* datas = nullptr;
    int channels;
    int sample_rate;
    short* out = nullptr;
    int32_t raw_len = stb_vorbis_decode_memory((const uint8_t*)data, data_len, &channels, &sample_rate, &out);
    
    if (raw_len > 0)
    {
        res = std::make_shared<Audio>();
        res->reserve_audio_buffer(raw_len, channels, sample_rate);
        memcpy(res->get_raw_pointer(), out, raw_len);

        res->_id = id;
        self._audios.emplace(id, res);
    }
    free(datas);

    return res;
}

// Copy an existing audio. If it fails, the pointer will be nullptr
std::shared_ptr<Audio> AudioManager::copy_audio(std::shared_ptr<Audio> const& audio, std::string const& id)
{
    return nullptr;
}
// Copy an existing audio from a buffer. If it fails, the pointer will be nullptr
std::shared_ptr<Audio> AudioManager::copy_audio(void const* data, int32_t channels, int32_t sample_rate, std::string const& id)
{
    return nullptr;
}

bool AudioManager::save_audio_to_file(std::string const& audio_path, std::shared_ptr<Audio> const& img)
{
    return false;
}

bool AudioManager::save_audio_to_file(std::string const& audio_path, void* data, int32_t channels, int32_t sample_rate)
{
    return false;
}

void AudioManager::play_audio(std::string const& id)
{
    play_audio(get_audio(id));
}

int AudioManager::PlayCallback(const void* inputBuffer, void* outputBuffer,
    unsigned long framesPerBuffer,
    const PaStreamCallbackTimeInfo* timeInfo,
    PaStreamCallbackFlags statusFlags,
    void* userData)
{
    auto& self = Inst();

    /* Cast data passed through stream to our structure. */
    StreamPos* data = (StreamPos*)userData;
    uint16_t* out = (uint16_t*)outputBuffer;
    unsigned int i;
    (void)inputBuffer; /* Prevent unused variable warning. */

    if (data->pos >= (data->audio->raw_size() / sizeof(uint16_t)))
    {
        self._portaudio.Pa_StopStream(data->stream);
        delete data;
        return paComplete;
    }

    while (framesPerBuffer--)
    {
        for (int i = 0; i < data->audio->channels(); ++i)
        {
            if (data->pos < (data->audio->raw_size() / sizeof(uint16_t)))
            {
                *out++ = reinterpret_cast<int16_t*>(data->audio->get_raw_pointer())[data->pos++];
            }
            else
            {
                *out++ = 0;
            }
        }
    }
    
    return paContinue;
}

void AudioManager::play_audio(std::shared_ptr<Audio> audio)
{
    auto& self = Inst();
    if (audio != nullptr && self._can_play_audio)
    {
        StreamPos* stream_pos = new StreamPos;
        stream_pos->audio = audio;
        stream_pos->pos = 0;
        
        self._portaudio.Pa_OpenDefaultStream(&stream_pos->stream, 0, audio->channels(), paInt16, audio->sample_rate(), paFramesPerBufferUnspecified, PlayCallback, stream_pos); /*This is a pointer that will be passed to your callback*/
        self._portaudio.Pa_StartStream(stream_pos->stream);
    }
}