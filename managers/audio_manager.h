/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <common_includes.h>
#include <portaudio.h>

class AudioManager;
class Audio
{
    friend class AudioManager;

    std::string _id;
    int32_t _channels;
    int32_t _sample_rate;
    std::vector<uint8_t> _audio;
public:

    inline std::string const& id() const { return _id; }

    // samples is the number of uint16_t, not the raw bytes count. Destructive, use with care
    inline void reserve_audio_buffer(size_t samples, int32_t channels, int32_t sample_rate)
    {
        _channels = channels;
        _sample_rate = sample_rate;

        _audio.resize(samples * sizeof(uint16_t));
    }

    inline void*       get_raw_pointer()       { return _audio.data(); }
    inline void const* get_raw_pointer() const { return _audio.data(); }

    inline uint16_t*       audio_samples()       { return reinterpret_cast<uint16_t*>(_audio.data()); }
    inline uint16_t const* audio_samples() const { return reinterpret_cast<uint16_t const*>(_audio.data()); }

    // The size of the audio as bytes
    inline size_t raw_size() const { return (_audio.size() - 44); }
    // The size of the audio as samples
    inline size_t size() const { return (_audio.size() - 44) / sizeof(uint16_t); }

    inline int32_t channels() const { return _channels; }
    inline void    channels(int32_t channels)
    {
        _channels = channels;
    }

    inline int32_t sample_rate() const { return _sample_rate; }
    inline void    sample_rate(int32_t sample_rate)
    {
        _sample_rate = sample_rate;
    }
};

class PortAudio
{
    Library _lib;

public:
    bool load();

    bool is_loaded() const;

    std::function<decltype(Pa_Initialize)> Pa_Initialize;
    std::function<decltype(Pa_Terminate)> Pa_Terminate;
    std::function<decltype(Pa_OpenDefaultStream)> Pa_OpenDefaultStream;
    std::function<decltype(Pa_StartStream)> Pa_StartStream;
    std::function<decltype(Pa_StopStream)> Pa_StopStream;
};

struct StreamPos
{
    PaStream* stream;
    std::shared_ptr<Audio> audio;
    size_t pos;
};

class AudioManager
{
    std::unordered_map<std::string, std::shared_ptr<Audio>> _audios;

    static inline AudioManager& Inst();
    AudioManager();
    AudioManager(AudioManager const&) = delete;
    AudioManager(AudioManager&&) = delete;
    AudioManager& operator=(AudioManager const&) = delete;
    AudioManager& operator=(AudioManager&&) = delete;

    PortAudio _portaudio;
    bool _can_play_audio;

    static int PlayCallback(const void* inputBuffer, void* outputBuffer,
        unsigned long framesPerBuffer,
        const PaStreamCallbackTimeInfo* timeInfo,
        PaStreamCallbackFlags statusFlags,
        void* userData);

public:
    ~AudioManager();

    static bool audio_exists(std::string const& id);
    static std::shared_ptr<Audio> get_audio(std::string const& id);

    static std::shared_ptr<Audio> create_audio(void const* data, size_t data_len, int32_t channels, int32_t sample_rate, std::string const& id);
    static bool delete_audio(std::string const& id);
    static bool delete_audio(std::shared_ptr<Audio> audio);

    // Load audio from path. If it fails, the pointer will be nullptr
    static std::shared_ptr<Audio> load_audio(std::string const& audio_path, std::string const& id);
    // Load audio from buffer. If it fails, the pointer will be nullptr
    static std::shared_ptr<Audio> load_audio(void const* data, size_t data_len, std::string const& id);

    // Copy an existing audio. If it fails, the pointer will be nullptr
    static std::shared_ptr<Audio> copy_audio(std::shared_ptr<Audio> const& audio, std::string const& id);
    // Copy an existing audio from a buffer. If it fails, the pointer will be nullptr
    static std::shared_ptr<Audio> copy_audio(void const* data, int32_t channels, int32_t sample_rate, std::string const& id);

    static bool save_audio_to_file(std::string const& audio_path, std::shared_ptr<Audio> const& img);
    static bool save_audio_to_file(std::string const& audio_path, void* data, int32_t channels, int32_t sample_rate);

    static void play_audio(std::string const& id);
    static void play_audio(std::shared_ptr<Audio> audio);
};