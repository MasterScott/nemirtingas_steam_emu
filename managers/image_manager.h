/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <common_includes.h>

union image_pixel_t
{
    uint32_t pixel;
    struct pixel_channels_t
    {
        uint8_t r;
        uint8_t g;
        uint8_t b;
        uint8_t a;
    } channels;
};

class ImageManager;
class Image
{
    friend class ImageManager;

    std::string _id;
    int32_t _width;
    int32_t _height;
    std::vector<image_pixel_t> _img; // This should be able to be used in any Renderer. Its a contiguous array of RGBA values

public:
    inline std::string const& id() const { return _id; }

    // Size is the number of image_pixel_t, not the raw bytes count. Destructive, use with care
    inline void reserve_img_buffer(int32_t width, int32_t height) { _img.resize(size_t(width) * size_t(height)); _width = width; _height = height; }

    inline void*       get_raw_pointer()       { return _img.data(); }
    inline void const* get_raw_pointer() const { return _img.data(); }

    inline image_pixel_t*       pixels()       { return _img.data(); }
    inline image_pixel_t const* pixels() const { return _img.data(); }

    inline size_t raw_size() const { return _img.size() * sizeof(image_pixel_t); }
    inline size_t size() const { return _img.size(); }

    inline int32_t width() const { return _width; }
    inline void    width(int32_t width) { _width = width; }

    inline int32_t height() const { return _height; }
    inline void    height(int32_t height) { _height = height; }

    inline image_pixel_t& get_pixel(int32_t x, int32_t y)       { return _img[size_t(_width) * y + x]; }
    inline image_pixel_t  get_pixel(int32_t x, int32_t y) const { return _img[size_t(_width) * y + x]; }
};

class ImageManager
{
    std::unordered_map<std::string, std::shared_ptr<Image>> _images;

    static inline ImageManager& Inst();
    ImageManager();
    ImageManager(ImageManager const&) = delete;
    ImageManager(ImageManager &&) = delete;
    ImageManager& operator=(ImageManager const&) = delete;
    ImageManager& operator=(ImageManager &&) = delete;

public:
    enum class image_type
    {
        png,
        jpeg,
        bmp
    };

    ~ImageManager();

    static bool image_exists(std::string const& id);
    static std::shared_ptr<Image> get_image(std::string const& id);

    static std::shared_ptr<Image> create_image(void const* data, int32_t width, int32_t height, std::string const& id);
    static bool delete_image(std::string const& id);
    static bool delete_image(std::shared_ptr<Image> image);

    // Load image from path, optionnal: resize when loading. If it fails, the pointer will be nullptr
    static std::shared_ptr<Image> load_image(std::string const& image_path, std::string const& id, int32_t resize_width = 0, int32_t resize_height = 0);
    // Load image from buffer, optionnal: resize when loading. If it fails, the pointer will be nullptr
    static std::shared_ptr<Image> load_image(void const* data, size_t data_len, std::string const& id, int32_t resize_width = 0, int32_t resize_height = 0);

    // Copy an existing image, optionnal: resize when copying. If it fails, the pointer will be nullptr
    static std::shared_ptr<Image> copy_image(std::shared_ptr<Image> const& img, std::string const& id, int32_t resize_width = 0, int32_t resize_height = 0);
    // Copy an existing image from a buffer, optionnal: resize when copying. If it fails, the pointer will be nullptr
    static std::shared_ptr<Image> copy_image(void const* data, int32_t width, int32_t height, std::string const& id, int32_t resize_width = 0, int32_t resize_height = 0);

    static bool save_image_to_file(std::string const& image_path, std::shared_ptr<Image> const& img, image_type img_type = image_type::png);
    static bool save_image_to_file(std::string const& image_path, void* data, int32_t width, int32_t height, int32_t channels, image_type img_type = image_type::png);
};