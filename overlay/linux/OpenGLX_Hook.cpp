/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Goldberg Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "../steam_dll/steam_client.h"
#include "OpenGLX_Hook.h"
#include "X11_Hook.h"
#include "Renderer_Detector.h"

#include <imgui.h>
#include <impls/imgui_impl_opengl3.h>

OpenGLX_Hook* OpenGLX_Hook::_inst = nullptr;

bool OpenGLX_Hook::start_hook()
{
    bool res = true;
    if (!hooked)
    {
        if (!X11_Hook::Inst()->start_hook())
            return false;

        GLenum err = glewInit();

        if (err == GLEW_OK)
        {
            APP_LOG(Log::LogLevel::INFO, "Hooked OpenGLX\n");

            hooked = true;
            Renderer_Detector::Inst().renderer_found(this);

            UnhookAll();
            BeginHook();
            HookFuncs(
                std::make_pair<void**, void*>((void**)&_glXSwapBuffers, (void*)&OpenGLX_Hook::MyglXSwapBuffers)
            );
            EndHook();
        }
        else
        {
            APP_LOG(Log::LogLevel::WARN, "Failed to hook OpenGLX\n");
            /* Problem: glewInit failed, something is seriously wrong. */
            APP_LOG(Log::LogLevel::WARN, "Error: %s\n", glewGetErrorString(err));
            res = false;
        }
    }
    return true;
}

void OpenGLX_Hook::resetRenderState()
{
    if (initialized)
    {
        GetSteam_Overlay().HookReady(false);

        ImGui_ImplOpenGL3_Shutdown();
        X11_Hook::Inst()->resetRenderState();
        ImGui::DestroyContext();

        glXDestroyContext(display, context);
        display = nullptr;
        initialized = false;
    }
}

// Try to make this function and overlay's proc as short as possible or it might affect game's fps.
void OpenGLX_Hook::prepareForOverlay(Display* display, GLXDrawable drawable)
{
    APP_LOG(Log::LogLevel::TRACE, "Called SwapBuffer hook");

    if( !initialized )
    {
        ImGui::CreateContext();
        ImGui_ImplOpenGL3_Init();
        GetSteam_Overlay().HookReady(true);

        int attributes[] = { //can't be const b/c X11 doesn't like it.  Not sure if that's intentional or just stupid.
            GLX_RGBA, //apparently nothing comes after this?
            GLX_RED_SIZE,    8,
            GLX_GREEN_SIZE,  8,
            GLX_BLUE_SIZE,   8,
            GLX_ALPHA_SIZE,  8,
            //Ideally, the size would be 32 (or at least 24), but I have actually seen
            //  this size (on a modern OS even).
            GLX_DEPTH_SIZE, 16,
            GLX_DOUBLEBUFFER, True,
            None
        };

        XVisualInfo* visual_info = glXChooseVisual(display, DefaultScreen(display), attributes);

        context = glXCreateContext(display, visual_info, nullptr, True);
        this->display = display;

        initialized = true;
    }

    auto oldContext = glXGetCurrentContext();

    glXMakeCurrent(display, drawable, context);

    if (ImGui_ImplOpenGL3_NewFrame() && X11_Hook::Inst()->prepareForOverlay(display, (Window)drawable))
    {
        ImGui::NewFrame();

        GetSteam_Overlay().OverlayProc();

        ImGui::Render();

        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    }

    glXMakeCurrent(display, drawable, oldContext);
}

void OpenGLX_Hook::MyglXSwapBuffers(Display* display, GLXDrawable drawable)
{
    OpenGLX_Hook::Inst()->prepareForOverlay(display, drawable);
    OpenGLX_Hook::Inst()->_glXSwapBuffers(display, drawable);
}

OpenGLX_Hook::OpenGLX_Hook():
    initialized(false),
    hooked(false),
    _glXSwapBuffers(nullptr)
{
    //_library = dlopen(DLL_NAME);
}

OpenGLX_Hook::~OpenGLX_Hook()
{
    APP_LOG(Log::LogLevel::INFO, "OpenGLX Hook removed\n");

    if (initialized)
    {
        ImGui_ImplOpenGL3_Shutdown();
        ImGui::DestroyContext();
        glXDestroyContext(display, context);
    }

    //dlclose(_library);

    _inst = nullptr;
}

OpenGLX_Hook* OpenGLX_Hook::Inst()
{
    if (_inst == nullptr)
        _inst = new OpenGLX_Hook;

    return _inst;
}

const char* OpenGLX_Hook::get_lib_name() const
{
    return DLL_NAME;
}

void OpenGLX_Hook::loadFunctions(decltype(glXSwapBuffers)* pfnglXSwapBuffers)
{
    _glXSwapBuffers = pfnglXSwapBuffers;
}

void const* OpenGLX_Hook::CreateImageRessource(void const* image_buffer, int32_t width, int32_t height)
{
    return nullptr;
    //GLuint* texture = new GLuint;
    //glGenTextures(1, texture);
    //glBindTexture(GL_TEXTURE_2D, *texture);
    //glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_buffer);
    //
    //return reinterpret_cast<void*>(texture);
}

void        OpenGLX_Hook::FreeImageRessource(void const* handle)
{
    //GLuint const* texture = reinterpret_cast<GLuint const*>(handle);
    //glDeleteTextures(1, texture);
    //delete texture;
}
