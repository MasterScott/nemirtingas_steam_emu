/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Goldberg Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "Renderer_Detector.h"
#include "../Hook_Manager.h"

constexpr int max_hook_retries = 500;

static decltype(glXSwapBuffers)* _glXSwapBuffers = nullptr;
static decltype(XEventsQueued)* _XEventsQueued = nullptr;
static decltype(XPending)* _XPending = nullptr;

void Renderer_Detector::MyglXSwapBuffers(Display* dpy, GLXDrawable drawable)
{
    _glXSwapBuffers(dpy, drawable);
    OpenGLX_Hook::Inst()->start_hook();
}

void Renderer_Detector::HookglXSwapBuffers(decltype(glXSwapBuffers)* glXSwapBuffers)
{
    _glXSwapBuffers = glXSwapBuffers;
    rendererdetect_hook->BeginHook();
    rendererdetect_hook->HookFuncs(
        std::pair<void**, void*>((void**)&_glXSwapBuffers, (void*)&Renderer_Detector::MyglXSwapBuffers)
    );
    rendererdetect_hook->EndHook();
}

void Renderer_Detector::HookX11PendingnQueued(decltype(XEventsQueued)* XEventsQueued, decltype(XPending)* XPending)
{
    _XEventsQueued = XEventsQueued;
    _XPending = XPending;
    rendererdetect_hook->BeginHook();
    //rendererdetect_hook->HookFuncs(
    //    std::pair<void**, void*>((void**)&glXSwapBuffers, (void*)&Renderer_Detector::MyglXSwapBuffers),
    //    std::pair<void**, void*>((void**)&glXSwapBuffers, (void*)&Renderer_Detector::MyglXSwapBuffers)
    //);
    rendererdetect_hook->EndHook();
}

void Renderer_Detector::hook_openglx(const char* libname)
{
    if (!_oglx_hooked && !_renderer_found)
    {
        void* library = dlopen(libname, RTLD_NOW);
        decltype(glXSwapBuffers)* glXSwapBuffers = nullptr;
        if (library != nullptr)
        {
            glXSwapBuffers = (decltype(glXSwapBuffers))dlsym(library, "glXSwapBuffers");
        }
        if (glXSwapBuffers != nullptr)
        {
            APP_LOG(Log::LogLevel::INFO, "Hooked glXSwapBuffers to detect OpenGLX");

            _oglx_hooked = true;
            auto h = OpenGLX_Hook::Inst();
            h->loadFunctions(glXSwapBuffers);
            Hook_Manager::Inst().AddHook(h);
            HookglXSwapBuffers(glXSwapBuffers);
        }
        else
        {
            APP_LOG(Log::LogLevel::WARN, "Failed to Hook glXSwapBuffers to detect OpenGLX");
        }
    }
}

void Renderer_Detector::hook_x11(const char* libname)
{
    if (!_x11_hooked && !_renderer_found)
    {
        void* library = dlopen(libname, RTLD_NOW);
        decltype(XEventsQueued)* XEventsQueued = nullptr;
        decltype(XPending)* XPending = nullptr;

        if (library != nullptr)
        {
            XEventsQueued = (decltype(XEventsQueued))dlsym(library, "XEventsQueued");
            XPending = (decltype(XPending))dlsym(library, "XPending");

            if (XEventsQueued != nullptr && XPending != nullptr)
            {
                APP_LOG(Log::LogLevel::INFO, "Hooked XEventsQueued and XPending to detect X11");

                _x11_hooked = true;
                auto h = X11_Hook::Inst();
                h->loadFunctions(XEventsQueued, XPending);
                Hook_Manager::Inst().AddHook(h);
                HookX11PendingnQueued(XEventsQueued, XPending);
            }
            else
            {
                APP_LOG(Log::LogLevel::INFO, "Failed to hook XEventsQueued and XPending to detect X11");
            }
            dlclose(library);
        }
        else
        {
            APP_LOG(Log::LogLevel::WARN, "Failed to load %s to hook X11", libname);
        }
    }
}

void Renderer_Detector::create_hook(const char* libname)
{
    if (strcmp(libname, X11_Hook::DLL_NAME) == 0)
        hook_x11(libname);
    else if (strcmp(libname, OpenGLX_Hook::DLL_NAME) == 0)
        hook_openglx(libname);
}

void Renderer_Detector::find_renderer_proc(Renderer_Detector* _this)
{
    _this->rendererdetect_hook = new Base_Hook();
    Hook_Manager& hm = Hook_Manager::Inst();
    hm.AddHook(_this->rendererdetect_hook);

    std::vector<std::string> const libraries = {
        OpenGLX_Hook::DLL_NAME,
        X11_Hook::DLL_NAME
    };

    while (!_this->_renderer_found && !_this->stop_retry())
    {
        std::vector<std::string>::const_iterator it = libraries.begin();
        while (it != libraries.end())
        {
            {
                std::lock_guard<std::mutex> lock(_this->_found_mutex);
                if (_this->_renderer_found)
                    break;

                it = std::find_if(it, libraries.end(), [](std::string const& name)
                {
                    auto x = get_module_handle(name.c_str());
                    if (x != NULL)
                        return true;
                    return false;
                });

                if (it == libraries.end())
                    break;

                _this->create_hook(it->c_str());
                ++it;
            }
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
}

Renderer_Detector::Renderer_Detector():
    _hook_thread(nullptr),
    _hook_retries(0),
    _renderer_found(false),
    _oglx_hooked(false),
    rendererdetect_hook(nullptr),
    game_renderer(nullptr)
{}

void Renderer_Detector::renderer_found(Renderer_Hook* hook)
{
    std::lock_guard<std::mutex> lock(_found_mutex);
    Hook_Manager& hm = Hook_Manager::Inst();

    _renderer_found = true;
    game_renderer = hook;

    if (hook == nullptr)
        APP_LOG(Log::LogLevel::WARN, "We found a renderer but couldn't hook it, aborting overlay hook.\n");
    else
        APP_LOG(Log::LogLevel::INFO, "Hooked renderer in %d/%d tries\n", _hook_retries, max_hook_retries);

    hm.RemoveHook(rendererdetect_hook);
}

bool Renderer_Detector::stop_retry()
{
    // Retry or not
    bool stop = ++_hook_retries >= max_hook_retries;

    if (stop)
        renderer_found(nullptr);

    return stop;
}

void Renderer_Detector::find_renderer()
{
    if (_hook_thread == nullptr)
    {
        _hook_thread = new std::thread(&Renderer_Detector::find_renderer_proc, this);
        _hook_thread->detach();
    }
}

Renderer_Detector& Renderer_Detector::Inst()
{
    static Renderer_Detector inst;
    return inst;
}

Renderer_Hook* Renderer_Detector::get_renderer() const
{
    return game_renderer;
}

Renderer_Detector::~Renderer_Detector()
{
    if (_hook_thread != nullptr)
    {
        _hook_retries = max_hook_retries;
    }
}
