/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Goldberg Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "../steam_dll/steam_client.h"
#include "X11_Hook.h"
#include "Renderer_Detector.h"

#include <imgui.h>
#include <impls/linux/imgui_impl_x11.h>

#include <dlfcn.h>
#include <unistd.h>
#include <fstream>

extern int ImGui_ImplX11_EventHandler(XEvent &event);

X11_Hook* X11_Hook::_inst = nullptr;

bool X11_Hook::start_hook()
{
    bool res = false;

    if (!hooked && _XPending != nullptr && _XEventsQueued != nullptr)
    {
        APP_LOG(Log::LogLevel::INFO, "Hooked X11\n");

        hooked = true;
        res = true;

        UnhookAll();
        BeginHook();
        HookFuncs(
            std::make_pair<void**, void*>(&(void*&)_XEventsQueued, (void*)&X11_Hook::MyXEventsQueued),
            std::make_pair<void**, void*>(&(void*&)_XPending, (void*)&X11_Hook::MyXPending)
        );
        EndHook();
    }
    return res;
}

void X11_Hook::resetRenderState()
{
    if (initialized)
    {
        game_wnd = 0;
        initialized = false;
        ImGui_ImplX11_Shutdown();
    }
}

bool X11_Hook::prepareForOverlay(Display *display, Window wnd)
{
    if(!hooked)
        return false;

    if (game_wnd != wnd)
        resetRenderState();

    if (!initialized)
    {
        ImGui_ImplX11_Init(display, (void*)wnd);
        game_wnd = wnd;

        initialized = true;
    }

    ImGui_ImplX11_NewFrame();

    return true;
}

/////////////////////////////////////////////////////////////////////////////////////
// X11 window hooks
bool IgnoreEvent(XEvent &event)
{
    switch(event.type)
    {
        // Keyboard
        case KeyPress: case KeyRelease:
        // MouseButton
        case ButtonPress: case ButtonRelease:
        // Mouse move
        case MotionNotify:
            return true;
    }
    return false;
}

int X11_Hook::check_for_overlay(Display *d, int num_events)
{
    static Time prev_time = {};
    auto& overlay = GetSteam_Overlay();
    if (!overlay.Ready())
        return num_events;

    X11_Hook* inst = Inst();

    if( inst->initialized )
    {
        XEvent event;
        while(num_events)
        {
            //inst->_XPeekEvent(d, &event);
            XPeekEvent(d, &event);
            ImGui_ImplX11_EventHandler(event);

            bool ignore_message = overlay.ShowOverlay();
            // Is the event is a key press
            if (event.type == KeyPress)
            {
                // Tab is pressed and was not pressed before
                //if (event.xkey.keycode == inst->_XKeysymToKeycode(d, XK_Tab) && event.xkey.state & ShiftMask)
                if (event.xkey.keycode == XKeysymToKeycode(d, XK_Tab) && event.xkey.state & ShiftMask)
                {
                    // if key TAB is held, don't make the overlay flicker :p
                    if( event.xkey.time != prev_time)
                    {
                        overlay.ShowOverlay(!overlay.ShowOverlay());
                        ignore_message = true;
                    }
                }
            }
            //else if(event.type == KeyRelease && event.xkey.keycode == inst->_XKeysymToKeycode(d, XK_Tab))
            else if(event.type == KeyRelease && event.xkey.keycode == XKeysymToKeycode(d, XK_Tab))
            {
                prev_time = event.xkey.time;
            }

            if (!ignore_message || !IgnoreEvent(event))
            {
                break;
            }

            //inst->_XNextEvent(d, &event);
            XNextEvent(d, &event);
            --num_events;
        }
    }
    return num_events;
}

int X11_Hook::MyXEventsQueued(Display *display, int mode)
{
    X11_Hook* inst = X11_Hook::Inst();

    int res = inst->_XEventsQueued(display, mode);

    if( res )
    {
        res = inst->check_for_overlay(display, res);
    }

    return res;
}

int X11_Hook::MyXPending(Display* display)
{
    int res = Inst()->_XPending(display);

    if( res )
    {
        res = Inst()->check_for_overlay(display, res);
    }

    return res;
}

/////////////////////////////////////////////////////////////////////////////////////

X11_Hook::X11_Hook() :
    initialized(false),
    hooked(false),
    game_wnd(0),
    _XEventsQueued(nullptr),
    _XPending(nullptr)
{
    //_library = dlopen(DLL_NAME, RTLD_NOW);
}

X11_Hook::~X11_Hook()
{
    APP_LOG(Log::LogLevel::INFO, "X11 Hook removed\n");

    resetRenderState();

    //dlclose(_library);

    _inst = nullptr;
}

X11_Hook* X11_Hook::Inst()
{
    if (_inst == nullptr)
        _inst = new X11_Hook;

    return _inst;
}

const char* X11_Hook::get_lib_name() const
{
    return DLL_NAME;
}

void X11_Hook::loadFunctions(decltype(XEventsQueued)* pfnXEventsQueued, decltype(XPending)* pfnXPending)
{
    _XEventsQueued = pfnXEventsQueued;
    _XPending = pfnXPending;
}
