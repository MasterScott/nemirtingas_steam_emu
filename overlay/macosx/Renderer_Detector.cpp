/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Goldberg Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "Renderer_Detector.h"
#include "../Hook_Manager.h"

constexpr int max_hook_retries = 500;

void Renderer_Detector::find_renderer_proc(Renderer_Detector* _this)
{
}

Renderer_Detector::Renderer_Detector()
{}

void Renderer_Detector::renderer_found(Renderer_Hook* hook)
{}

bool Renderer_Detector::stop_retry()
{
    return true;
}

void Renderer_Detector::find_renderer()
{
}

Renderer_Detector& Renderer_Detector::Inst()
{
    static Renderer_Detector inst;
    return inst;
}

Renderer_Hook* Renderer_Detector::get_renderer() const
{
    return nullptr;
}

Renderer_Detector::~Renderer_Detector()
{
}
