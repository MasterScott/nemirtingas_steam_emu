/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Goldberg Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __INCLUDED_RENDERER_DETECTOR_H__
#define __INCLUDED_RENDERER_DETECTOR_H__

#include "../Renderer_Hook.h"
#include <thread>

class Renderer_Detector
{
private:

    // Functions
    Renderer_Detector();
    ~Renderer_Detector();

    bool stop_retry();

    static void find_renderer_proc(Renderer_Detector* _this);

public:
    void find_renderer();
    void renderer_found(Renderer_Hook* hook);
    Renderer_Hook* get_renderer() const;
    static Renderer_Detector& Inst();
};

#endif//__INCLUDED_RENDERER_DETECTOR_H__
