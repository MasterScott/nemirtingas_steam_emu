/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Goldberg Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "../steam_overlay.h"

#include "../../steam_dll/steam_client.h"

Steam_Overlay::Steam_Overlay()
{
}

Steam_Overlay::~Steam_Overlay()
{
}

void Steam_Overlay::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{

}

void Steam_Overlay::emu_deinit()
{

}

bool Steam_Overlay::Ready() const
{
    return false;
}

bool Steam_Overlay::NeedPresent() const
{
    return true;
}

void Steam_Overlay::SetNotificationPosition(ENotificationPosition eNotificationPosition)
{
}

void Steam_Overlay::SetNotificationInset(int nHorizontalInset, int nVerticalInset)
{
}

void Steam_Overlay::SetupOverlay()
{
}

void Steam_Overlay::HookReady(bool is_ready)
{
}

void Steam_Overlay::OpenOverlayInvite(CSteamID lobbyId)
{
}

void Steam_Overlay::OpenOverlay(const char* pchDialog)
{
}

bool Steam_Overlay::ShowOverlay() const
{
    return false;
}

void Steam_Overlay::ShowOverlay(bool state)
{
}

void Steam_Overlay::SetLobbyInvite(CSteamID friendId, uint64 lobbyId)
{
}

void Steam_Overlay::SetRichInvite(CSteamID friendId, const char* connect_str)
{
}

void Steam_Overlay::FriendConnect(CSteamID _friend, std::string const& name, uint32_t appid)
{
}

void Steam_Overlay::FriendDisconnect(CSteamID _friend)
{
}

void Steam_Overlay::FriendImageLoaded(CSteamID _friend, std::shared_ptr<Image> img)
{
}

void Steam_Overlay::FriendNameChanged(CSteamID _friend, std::string const& name)
{
}

void Steam_Overlay::AddAchievementNotification(nlohmann::json const& ach)
{
}

// Try to make this function as short as possible or it might affect game's fps.
void Steam_Overlay::OverlayProc()
{
}

// Send Network messages
bool Steam_Overlay::send_chat(uint64 remote_id, Overlay_Chat_pb*chat)
{
    return true;
}

// Receive Network messages
bool Steam_Overlay::on_chat_receive(Network_Message_pb const& msg, Overlay_Chat_pb const& chat)
{
    return true;
}

bool Steam_Overlay::CBRunFrame()
{
    return true;
}

bool Steam_Overlay::RunNetwork(Network_Message_pb const& msg)
{
    return true;
}

bool Steam_Overlay::RunCallbacks(pFrameResult_t res)
{
    return res->done;
}
