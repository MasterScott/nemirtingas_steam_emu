/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Goldberg Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_overlay.h"

#include <imgui.h>

#include "../steam_dll/steam_client.h"
#include "../steam_dll/settings.h"
#include <Renderer_Detector.h>

// Put in here some class vars, so we don't need to include ImGui's headers in steam_overlay.h
static ImFont*     font_default = nullptr;
static ImFont*     font_notif = nullptr;
static void const* user_avatar_handle = nullptr;
static void const* default_avatar_handle = nullptr;

static constexpr int max_window_id = 10000;
static constexpr int base_notif_window_id  = 0 * max_window_id;
static constexpr int base_friend_window_id = 1 * max_window_id;
static constexpr int base_friend_item_id   = 2 * max_window_id;

constexpr decltype(friend_window_state::invite_expiration) friend_window_state::invite_expiration;

constexpr decltype(Notification::width)          Notification::width;
constexpr decltype(Notification::height)         Notification::height;
constexpr decltype(Notification::fade_in)        Notification::fade_in;
constexpr decltype(Notification::fade_out)       Notification::fade_out;
constexpr decltype(Notification::show_time)      Notification::show_time;
constexpr decltype(Notification::fade_out_start) Notification::fade_out_start;
constexpr decltype(Notification::r)              Notification::r;
constexpr decltype(Notification::g)              Notification::g;
constexpr decltype(Notification::b)              Notification::b;
constexpr decltype(Notification::max_alpha)      Notification::max_alpha;

int find_free_id(std::vector<int> & ids, int base)
{
    std::sort(ids.begin(), ids.end());

    int id = base;
    for (auto i : ids)
    {
        if (id < i)
            break;
        id = i + 1;
    }

    return id > (base+max_window_id) ? 0 : id;
}

int find_free_friend_id(std::map<CSteamID, friend_window_state> const& friend_windows)
{
    std::vector<int> ids;
    ids.reserve(friend_windows.size());

    std::for_each(friend_windows.begin(), friend_windows.end(), [&ids](std::pair<CSteamID const, friend_window_state> const& i)
    {
        ids.emplace_back(i.second.id);
    });
    
    return find_free_id(ids, base_friend_window_id);
}

int find_free_notification_id(std::vector<Notification> const& notifications)
{
    std::vector<int> ids;
    ids.reserve(notifications.size());

    std::for_each(notifications.begin(), notifications.end(), [&ids](Notification const& i)
    {
        ids.emplace_back(i.id);
    });
    

    return find_free_id(ids, base_friend_window_id);
}

Steam_Overlay::Steam_Overlay() :
    setup_overlay_called(false),
    show_overlay(false),
    is_ready(false),
    notif_position(ENotificationPosition::k_EPositionBottomLeft),
    h_inset(0),
    v_inset(0),
    overlay_state_changed(false)
{
    log_buffer.reserve(500000);
}

Steam_Overlay::~Steam_Overlay()
{
    _network->unregister_listener(this, Network_Message_pb::MessagesCase::kOverlay);
}

void Steam_Overlay::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    APP_LOG(Log::LogLevel::DEBUG, "");

    _network = network;
    _cb_manager = cb_manager;

    _network->register_listener(this, Network_Message_pb::MessagesCase::kOverlay);
}

void Steam_Overlay::emu_deinit()
{
    if (_network != nullptr)
    {
        _network->unregister_listener(this, Network_Message_pb::MessagesCase::kOverlay);
    }

    _network.reset();
    _cb_manager.reset();
}

bool Steam_Overlay::Ready() const
{
    return is_ready && _network != nullptr && _cb_manager != nullptr;
}

bool Steam_Overlay::NeedPresent() const
{
    return true;
}

void Steam_Overlay::SetNotificationPosition(ENotificationPosition eNotificationPosition)
{
    notif_position = eNotificationPosition;
}

void Steam_Overlay::SetNotificationInset(int nHorizontalInset, int nVerticalInset)
{
    h_inset = nHorizontalInset;
    v_inset = nVerticalInset;
}

void Steam_Overlay::SetupOverlay()
{
    std::lock_guard<std::recursive_mutex> lock(overlay_mutex);
    if (!setup_overlay_called)
    {
        setup_overlay_called = true;
        Renderer_Detector::Inst().find_renderer();
    }
}

void Steam_Overlay::log_function(void* user_param, Log::LogLevel lv, const char* message)
{
    Steam_Overlay* _this = (Steam_Overlay*)user_param;
    std::lock_guard<std::recursive_mutex> lock(_this->overlay_mutex);

    _this->log_buffer += message;
    _this->log_lines_offsets.emplace_back(_this->log_buffer.length());
}

void Steam_Overlay::HookReady(bool is_ready)
{
    if (is_ready)
    {
        // TODO: Uncomment this and draw our own cursor (cosmetics)
        ImGuiIO &io = ImGui::GetIO();
        //io.WantSetMousePos = false;
        //io.MouseDrawCursor = false;
        //io.ConfigFlags |= ImGuiConfigFlags_NoMouseCursorChange;
        io.IniFilename = NULL;

        //Log::set_log_func(log_function, this);

        CreateFonts();
        CreateTextures();
    }
    else
    {

    }

    this->is_ready = is_ready;
}

void Steam_Overlay::OpenOverlayInvite(CSteamID lobbyId)
{
    ShowOverlay(true);
}

void Steam_Overlay::OpenOverlay(const char* pchDialog)
{
    // TODO: Show pages depending on pchDialog
    ShowOverlay(true);
}

bool Steam_Overlay::ShowOverlay() const
{
    return show_overlay;
}

void Steam_Overlay::ShowOverlay(bool state)
{
    if (!Ready() || show_overlay == state)
        return;

    ImGuiIO &io = ImGui::GetIO();

    if(state)
    {
        io.MouseDrawCursor = true;
    }
    else
    {
        io.MouseDrawCursor = false;
    }

    pFrameResult_t result(new FrameResult);
    GameOverlayActivated_t& goa = result->CreateCallback<GameOverlayActivated_t>(std::chrono::milliseconds(0));
    goa.m_bActive = state;

    result->done = true;
    _cb_manager->add_callback(nullptr, result);

    show_overlay = state;
    overlay_state_changed = true;
}

void Steam_Overlay::NotifyUser(friend_window_state& friend_state)
{
    if (!(friend_state.state & window_state::show) || !show_overlay)
    {
        friend_state.state |= window_state::need_attention;
        AudioManager::play_audio(AudioManager::get_audio(notification_audio_id));
    }
}

void Steam_Overlay::SetLobbyInvite(CSteamID friendId, uint64 lobbyId)
{
    if (!Ready())
        return;

    std::lock_guard<std::recursive_mutex> lock(overlay_mutex);
    auto i = friends.find(friendId);
    if (i != friends.end())
    {
        auto& frd = i->second;
        frd.lobby_id = lobbyId;
        frd.state |= window_state::lobby_invite;
        // Make sure don't have rich presence invite and a lobby invite (it should not happen but who knows)
        frd.state &= ~window_state::rich_invite;
        frd.invite_time = std::chrono::steady_clock::now();
        AddInviteNotification(*i);
        NotifyUser(i->second);
    }
}

void Steam_Overlay::SetRichInvite(CSteamID friendId, const char* connect_str)
{
    if (!Ready())
        return;

    std::lock_guard<std::recursive_mutex> lock(overlay_mutex);
    auto i = friends.find(friendId);
    if (i != friends.end())
    {
        auto& frd = i->second;
        strncpy(frd.connect, connect_str, k_cchMaxRichPresenceValueLength - 1);
        frd.state |= window_state::rich_invite;
        // Make sure don't have rich presence invite and a lobby invite (it should not happen but who knows)
        frd.state &= ~window_state::lobby_invite;
        frd.invite_time = std::chrono::steady_clock::now();
        AddInviteNotification(*i);
        NotifyUser(i->second);
    }
}

void Steam_Overlay::FriendConnect(CSteamID _friend, std::string const& name, uint32_t appid)
{
    std::lock_guard<std::recursive_mutex> lock(overlay_mutex);
    if (friends.count(_friend) == 0)
    {
        int id = find_free_friend_id(friends);
        if (id != 0)
        {
            auto& item = friends[_friend];

            item.friend_name = name;
            item.appid = appid;
            item.title = std::move(name + " playing " + std::to_string(appid));
            item.state = window_state::none;
            item.id = id;
            memset(item.connect, 0, sizeof(item.connect));
            memset(item.chat_input, 0, max_chat_len);
            item.avatar_handle = nullptr;
        }
        else
        {
            APP_LOG(Log::LogLevel::WARN, "No more free id to create a friend window\n");
        }
    }
}

void Steam_Overlay::FriendDisconnect(CSteamID _friend)
{
    std::lock_guard<std::recursive_mutex> lock(overlay_mutex);
    auto it = friends.find(_friend);
    if (it != friends.end())
    {
        Renderer_Detector::Inst().get_renderer()->FreeImageRessource(it->second.avatar_handle);
        friends.erase(it);
    }
}

void Steam_Overlay::FriendImageLoaded(CSteamID _friend, std::shared_ptr<Image> img)
{
    std::lock_guard<std::recursive_mutex> lock(overlay_mutex);
    auto it = friends.find(_friend);
    if (it != friends.end())
    {
        it->second.avatar_handle = Renderer_Detector::Inst().get_renderer()->CreateImageRessource(img->get_raw_pointer(), img->width(), img->height());
    }
}

void Steam_Overlay::FriendNameChanged(CSteamID _friend, std::string const& name)
{
    std::lock_guard<std::recursive_mutex> lock(overlay_mutex);
    auto it = friends.find(_friend);
    if (it != friends.end())
    {
        it->second.friend_name = name;
    }
}

void Steam_Overlay::AddMessageNotification(std::string const& message)
{
    std::lock_guard<std::recursive_mutex> lock(notifications_mutex);
    int id = find_free_notification_id(notifications);
    if (id != 0)
    {
        Notification notif;
        notif.id = id;
        notif.type = notification_type_message;
        notif.message = message;
        notif.start_time = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch());
        notifications.emplace_back(notif);
    }
    else
        APP_LOG(Log::LogLevel::WARN, "No more free id to create a notification window\n");
}

void Steam_Overlay::AddAchievementNotification(nlohmann::json const& ach)
{
    std::lock_guard<std::recursive_mutex> lock(notifications_mutex);
    int id = find_free_notification_id(notifications);
    if (id != 0)
    {
        Notification notif;
        notif.id = id;
        notif.type = notification_type_achievement;
        // Load achievement image
        notif.message = ach["displayName"].get<std::string>() + "\n" + ach["description"].get<std::string>();
        notif.start_time = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch());
        notifications.emplace_back(notif);
    }
    else
        APP_LOG(Log::LogLevel::WARN, "No more free id to create a notification window\n");
}

void Steam_Overlay::AddInviteNotification(std::pair<const CSteamID, friend_window_state>& wnd_state)
{
    std::lock_guard<std::recursive_mutex> lock(notifications_mutex);
    int id = find_free_notification_id(notifications);
    if (id != 0)
    {
        Notification notif;
        notif.id = id;
        notif.type = notification_type_invite;
        notif.frd = &wnd_state;
        notif.message = wnd_state.second.friend_name + " invited you to join a game";
        notif.start_time = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch());
        notifications.emplace_back(notif);
    }
    else
    {
        APP_LOG(Log::LogLevel::WARN, "No more free id to create a notification window\n");
    }
}

void Steam_Overlay::BuildContextMenu(CSteamID friend_, friend_window_state& state)
{
    if (ImGui::BeginPopupContextItem("Friends_ContextMenu", 1))
    {
        bool close_popup = false;

        if (ImGui::Button("Chat"))
        {
            state.state |= window_state::show;
            close_popup = true;
        }
        if (state.appid == Settings::Inst().gameid.AppID())
        {
            if ((lobby_id != k_steamIDNil || !rich_connect.empty()) && ImGui::Button("Invite###PopupInvite"))
            {
                if (lobby_id != k_steamIDNil)
                    GetSteam_Matchmaking().InviteUserToLobby(lobby_id, friend_);
                else
                    GetSteam_Friends().InviteUserToGame(friend_, rich_connect.c_str());
                close_popup = true;
            }
            if (state.state & window_state::has_invite && (std::chrono::steady_clock::now() - state.invite_time) < friend_window_state::invite_expiration && ImGui::Button("Join###PopupJoin"))
            {
                if (state.state & window_state::rich_invite)
                {
                    pFrameResult_t result(new FrameResult);
                    GameRichPresenceJoinRequested_t& grpjr = result->CreateCallback<GameRichPresenceJoinRequested_t>();
                    grpjr.m_steamIDFriend = friend_;
                    strncpy(grpjr.m_rgchConnect, state.connect, sizeof(grpjr.m_rgchConnect) - 1);
                    grpjr.m_rgchConnect[k_cchMaxRichPresenceValueLength - 1] = 0;

                    result->done = true;
                    _cb_manager->add_callback(nullptr, result);
                }
                else
                {
                    pFrameResult_t result(new FrameResult);
                    GameLobbyJoinRequested_t& gljr = result->CreateCallback<GameLobbyJoinRequested_t>();

                    gljr.m_steamIDLobby = state.lobby_id;
                    gljr.m_steamIDFriend = friend_;

                    result->done = true;
                    _cb_manager->add_callback(nullptr, result);

                    //GetSteam_Matchmaking().JoinLobby(static_cast<uint64>(finfos_it->lobby_id()));
                }

                close_popup = true;
            }
        }
        if (close_popup)
        {
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }
}

void Steam_Overlay::BuildFriendWindow(CSteamID frd, friend_window_state& state)
{
    if (!(state.state & window_state::show))
        return;

    bool show = true;
    bool send_chat_msg = false;

    float width = ImGui::CalcTextSize("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA").x;
    
    if (state.state & window_state::need_attention && ImGui::IsWindowFocused())
    {
        state.state &= ~window_state::need_attention;
    }
    ImGui::SetNextWindowSizeConstraints(ImVec2{ width, ImGui::GetFontSize()*8 + ImGui::GetItemsLineHeightWithSpacing()*4 },
        ImVec2{ std::numeric_limits<float>::max() , std::numeric_limits<float>::max() });

    // Window id is after the ###, the window title is the friend name
    std::string friend_window_id = std::move("###" + std::to_string(state.id));
    if (ImGui::Begin((state.title + friend_window_id).c_str(), &show))
    {
        if (state.state & window_state::need_attention && ImGui::IsWindowFocused())
        {
            state.state &= ~window_state::need_attention;
        }

        // Fill this with the chat box and maybe the invitation
        if (state.appid == Settings::Inst().gameid.AppID() && state.state & (window_state::lobby_invite | window_state::rich_invite))
        {
            if ((std::chrono::steady_clock::now() - state.invite_time) >= friend_window_state::invite_expiration)
            {// When the invite expire, remove the invite flags
                state.state &= ~window_state::has_invite;
            }
            else
            {
                ImGui::LabelText("##label", "%s invited you to join the game.", state.friend_name.c_str());
                ImGui::SameLine();
                if (ImGui::Button("Accept"))
                {
                    if (state.state & window_state::lobby_invite)
                    {// GameLobbyJoinRequested_t on lobby_id
                        pFrameResult_t result(new FrameResult);
                        GameLobbyJoinRequested_t& gljr = result->CreateCallback<GameLobbyJoinRequested_t>();
                        gljr.m_steamIDFriend = frd;
                        gljr.m_steamIDLobby = state.lobby_id;

                        result->done = true;
                        _cb_manager->add_callback(nullptr, result);
                    }
                    else
                    {// GameRichPresenceJoinRequested_t on rich_presence
                        pFrameResult_t result(new FrameResult);
                        GameRichPresenceJoinRequested_t& grpjr = result->CreateCallback<GameRichPresenceJoinRequested_t>();
                        grpjr.m_steamIDFriend = frd;
                        strncpy(grpjr.m_rgchConnect, state.connect, k_cchMaxRichPresenceValueLength);

                        result->done = true;
                        _cb_manager->add_callback(nullptr, result);
                    }
                    state.state &= ~window_state::has_invite;
                }
                ImGui::SameLine();
                if (ImGui::Button("Refuse"))
                {
                    state.state &= ~window_state::has_invite;
                }
            }
        }

        if (state.avatar_handle == nullptr)
        {
            ImGui::Image((ImTextureID)default_avatar_handle, {64, 64});
        }
        else
        {
            ImGui::Image((ImTextureID)state.avatar_handle, { 64, 64 });
        }

        ImGui::ColoredInputTextMultiline("##chat_history", &state.chat_history[0], state.chat_history.length(), { -1.0f, 0 }, ImGuiInputTextFlags_ReadOnly);

        float wnd_width = ImGui::GetWindowContentRegionWidth();
        ImGuiStyle &style = ImGui::GetStyle();
        wnd_width -= ImGui::CalcTextSize("Send").x + style.FramePadding.x * 2 + style.ItemSpacing.x + 1;

        ImGui::PushItemWidth(wnd_width);
        if (ImGui::InputText("##chat_line", state.chat_input, max_chat_len, ImGuiInputTextFlags_EnterReturnsTrue))
        {
            send_chat_msg = true;
        }
        ImGui::PopItemWidth();

        ImGui::SameLine();

        if (ImGui::Button("Send"))
        {
            send_chat_msg = true;
        }

        if (send_chat_msg)
        {
            char* input = state.chat_input;
            char* end_input = input + strlen(input);
            char* printable_char = std::find_if(input, end_input, [](char c) {
                return std::isgraph(c);
                });
            // Check if the message contains something else than blanks
            if (printable_char != end_input)
            {
                // Handle chat send
                Overlay_Chat_pb* chat = new Overlay_Chat_pb;
                chat->set_chat_line(input);
                send_chat(frd.ConvertToUint64(), chat);

                state.chat_history.append("\x1""00FF00FF", 9).append(input).append("\n", 1);
            }
            *input = 0; // Reset the input field
        }
    }
    // User closed the friend window
    if (!show)
        state.state &= ~window_state::show;

    ImGui::End();
}

void Steam_Overlay::BuildNotifications(float width, float height)
{
    auto now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
    int i = 0;

    float font_size = ImGui::GetFontSize();

    std::lock_guard<std::recursive_mutex> lock(notifications_mutex);

    std::unordered_map<ImGuiCol, ImVec4> colors;

    colors[ImGuiCol_WindowBg]      = ImGui::GetStyleColorVec4(ImGuiCol_WindowBg);
    colors[ImGuiCol_Border]        = ImGui::GetStyleColorVec4(ImGuiCol_Border);
    colors[ImGuiCol_Text]          = ImGui::GetStyleColorVec4(ImGuiCol_Text);
    colors[ImGuiCol_Button]        = ImGui::GetStyleColorVec4(ImGuiCol_Button);
    colors[ImGuiCol_ButtonActive]  = ImGui::GetStyleColorVec4(ImGuiCol_ButtonActive);
    colors[ImGuiCol_ButtonHovered] = ImGui::GetStyleColorVec4(ImGuiCol_ButtonHovered);

    float alpha;
    for (auto it = notifications.begin(); it != notifications.end(); ++it, ++i)
    {
        auto elapsed_notif = now - it->start_time;

        if ( elapsed_notif < Notification::fade_in)
        {
            alpha = Notification::max_alpha * (elapsed_notif.count() / static_cast<float>(Notification::fade_in.count()));
        }
        else if ( elapsed_notif > Notification::fade_out_start)
        {
            alpha = Notification::max_alpha * ((Notification::show_time - elapsed_notif).count() / static_cast<float>(Notification::fade_out.count()));
        }
        else
        {
            alpha = Notification::max_alpha;
        }
        
        for (auto& col : colors)
        {
            switch (col.first)
            {
                case ImGuiCol_Text: col.second.w = alpha * 2; break;
                case ImGuiCol_WindowBg: col.second.w = alpha * 2; break;
                default: col.second.w = alpha;
            }
            ImGui::PushStyleColor(col.first, col.second);
        }

        ImGui::SetNextWindowPos(ImVec2((float)width - width * Notification::width, Notification::height * font_size * i ));
        ImGui::SetNextWindowSize(ImVec2( width * Notification::width, Notification::height * font_size ));
        ImGui::Begin(std::to_string(it->id).c_str(), nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoDecoration);

        switch (it->type)
        {
            case notification_type_achievement:
                ImGui::TextWrapped("%s", it->message.c_str());
                break;
            case notification_type_invite:
                {
                    ImGui::TextWrapped("%s", it->message.c_str());
                    friend_window_state& state = it->frd->second;
                    if (state.appid == Settings::Inst().gameid.AppID() && ImGui::Button("Join"))
                    {
                        if (state.state & window_state::lobby_invite)
                        {// GameLobbyJoinRequested_t on lobby_id
                            pFrameResult_t result(new FrameResult);
                            GameLobbyJoinRequested_t& gljr = result->CreateCallback<GameLobbyJoinRequested_t>();
                            gljr.m_steamIDFriend = it->frd->first;
                            gljr.m_steamIDLobby = state.lobby_id;

                            result->done = true;
                            _cb_manager->add_callback(nullptr, result);
                        }
                        else
                        {// GameRichPresenceJoinRequested_t on rich_presence
                            pFrameResult_t result(new FrameResult);
                            GameRichPresenceJoinRequested_t& grpjr = result->CreateCallback<GameRichPresenceJoinRequested_t>();
                            grpjr.m_steamIDFriend = it->frd->first;
                            strncpy(grpjr.m_rgchConnect, state.connect, k_cchMaxRichPresenceValueLength);

                            result->done = true;
                            _cb_manager->add_callback(nullptr, result);
                        }
                        state.state &= ~window_state::has_invite;
                        it->start_time = std::chrono::seconds(0);
                    }
                }
                break;
            case notification_type_message:
                ImGui::TextWrapped("%s", it->message.c_str()); break;
        }

        ImGui::End();

        ImGui::PopStyleColor(colors.size());
    }
    notifications.erase(std::remove_if(notifications.begin(), notifications.end(), [&now](Notification &item) {
        return (now - item.start_time) > Notification::show_time;
    }), notifications.end());
}

void Steam_Overlay::CreateFonts()
{
    ImGuiIO& io = ImGui::GetIO();
    ImFontConfig fontcfg;

    fontcfg.OversampleH = fontcfg.OversampleV = 1;
    fontcfg.PixelSnapH = true;
    fontcfg.GlyphRanges = io.Fonts->GetGlyphRangesDefault();

    //void* font_data;
    //int font_data_size = 0;
    //io.Fonts->AddFontFromMemoryTTF(font_data, font_data_size, io.DisplaySize.y / 68);

    fontcfg.SizePixels = std::round(io.DisplaySize.y / 68);
    font_default = io.Fonts->AddFontDefault(&fontcfg);

    fontcfg.SizePixels = std::round(io.DisplaySize.y / 60);
    font_notif = io.Fonts->AddFontDefault(&fontcfg);

    ImGuiStyle& style = ImGui::GetStyle();
    style.WindowRounding = 0.0; // Disable round window
}

void Steam_Overlay::CreateTextures()
{
    Renderer_Detector::Inst().get_renderer()->FreeImageRessource(user_avatar_handle);
    Renderer_Detector::Inst().get_renderer()->FreeImageRessource(default_avatar_handle);

    auto img = ImageManager::get_image(user_avatar_id);
    user_avatar_handle = Renderer_Detector::Inst().get_renderer()->CreateImageRessource(img->get_raw_pointer(), img->width(), img->height());

    img = ImageManager::get_image(default_avatar_id);
    default_avatar_handle = Renderer_Detector::Inst().get_renderer()->CreateImageRessource(img->get_raw_pointer(), img->width(), img->height());
}

// Try to make this function as short as possible or it might affect game's fps.
void Steam_Overlay::OverlayProc()
{
    if (!Ready())
        return;

    ImGuiIO& io = ImGui::GetIO();

    ImGui::PushFont(font_notif);
    BuildNotifications(io.DisplaySize.x, io.DisplaySize.y);
    ImGui::PopFont();

    if (show_overlay)
    {
        auto& steam_friends = GetSteam_Friends();

        rich_connect = steam_friends.GetFriendRichPresence(Settings::Inst().userid, "connect");
        lobby_id = static_cast<uint64>(steam_friends.myself()->second.infos.lobby_id());

        io.ConfigFlags &= ~ImGuiConfigFlags_NoMouseCursorChange;
        // Set the overlay windows to the size of the game window
        ImGui::SetNextWindowPos({ 0,0 });
        ImGui::SetNextWindowSize({ static_cast<float>(io.DisplaySize.x),
                                   static_cast<float>(io.DisplaySize.y) });

        ImGui::SetNextWindowBgAlpha(0.50);

        ImGui::PushFont(font_default);

        bool show = true;

        if (ImGui::Begin("SteamOverlay", &show, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoBringToFrontOnFocus))
        {
            ImVec2 saved_cursor_pos;
            float max_y = 0.0f;

            if (user_avatar_handle != nullptr)
            {
                ImGui::Image((ImTextureID)user_avatar_handle, { 64, 64 });
                max_y = ImGui::GetCursorPosY();
                ImGui::SameLine();
            }
            saved_cursor_pos = ImGui::GetCursorPos();

            ImGui::LabelText("##label", "Username: %s(%llu) playing %u",
                Settings::Inst().username.c_str(),
                Settings::Inst().userid.ConvertToUint64(),
                Settings::Inst().gameid.AppID());
            
            saved_cursor_pos.y += ImGui::GetFontSize();
            ImGui::SetCursorPos(saved_cursor_pos);
            ImGui::LabelText("##label", "Renderer: %s", io.BackendRendererName);

            saved_cursor_pos.y += ImGui::GetFontSize();
            ImGui::SetCursorPos(saved_cursor_pos);
            ImGui::LabelText("##label", "Platform: %s", io.BackendPlatformName);

            saved_cursor_pos.y = std::max(max_y, saved_cursor_pos.y);
            ImGui::SetCursorPos(saved_cursor_pos);

            ImGui::Spacing();

            ImGui::InputTextMultiline("##log_history", &log_buffer[0], log_buffer.length(), { -1.0f, 0 }, ImGuiInputTextFlags_ReadOnly);

            ImGui::Spacing();

            ImGui::LabelText("##label", "Friends");

            std::lock_guard<std::recursive_mutex> lock(overlay_mutex);
            if (!friends.empty())
            {
                ImGui::ListBoxHeader("##label", friends.size());
                std::for_each(friends.begin(), friends.end(), [&steam_friends, this](std::pair<CSteamID const, friend_window_state> &i)
                {
                    auto friend_it = steam_friends.get_friend(i.first.ConvertToUint64());
                    if (friend_it != steam_friends._friends.end())
                    {
                        auto& rich_presence = friend_it->second.rich_presence.rich_presence();
                        auto rich_it = rich_presence.find("connect");
                        if (rich_it != rich_presence.end())
                        {
                            strncpy(i.second.connect, rich_it->second.c_str(), sizeof(i.second.connect) - 1);
                        }
                        else
                        {
                            i.second.connect[0] = '\0';
                        }

                        i.second.lobby_id = friend_it->second.infos.lobby_id();
                    }
                    else
                    {
                        i.second.connect[0] = '\0';
                        i.second.lobby_id = 0;
                    }

                    ImGui::PushID(i.second.id-base_friend_window_id+base_friend_item_id);

                    ImGui::Selectable(i.second.title.c_str(), false, ImGuiSelectableFlags_AllowDoubleClick);
                    BuildContextMenu(i.first, i.second);
                    if (ImGui::IsItemClicked() && ImGui::IsMouseDoubleClicked(0))
                    {
                        i.second.state |= window_state::show;
                    }
                    ImGui::PopID();

                    BuildFriendWindow(i.first, i.second);
                });
                ImGui::ListBoxFooter();
            }
        }
        ImGui::End();

        ImGui::PopFont();

        if (!show)
            ShowOverlay(false);
    }// if(show_overlay)
    else
    {
        io.ConfigFlags |= ImGuiConfigFlags_NoMouseCursorChange;
    }
}

// Send Network messages
bool Steam_Overlay::send_chat(uint64 remote_id, Overlay_Chat_pb*chat)
{
    Network_Message_pb msg;
    Steam_Overlay_pb* steam_messages = new Steam_Overlay_pb;
    
    steam_messages->set_allocated_chat(chat);

    msg.set_allocated_overlay(steam_messages);
    msg.set_source_id(Settings::Inst().userid.ConvertToUint64());
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

// Receive Network messages
bool Steam_Overlay::on_chat_receive(Network_Message_pb const& msg, Overlay_Chat_pb const& chat)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lock(overlay_mutex);

    CSteamID frd_id = static_cast<uint64>(msg.source_id());
    auto it = friends.find(frd_id);
    if (it != friends.end())
    {
        // Change color to cyan for friend
        it->second.chat_history.append("\x1""00FFFFFF", 9).append(chat.chat_line()).append("\n", 1);
        // If the overlay isn't showing the friend window
        if (!show_overlay || !(it->second.state & window_state::show))
        {
            it->second.state |= window_state::need_attention;
            AddMessageNotification(it->second.friend_name + " says: " + chat.chat_line());

            NotifyUser(it->second);
        }
    }

    return true;
}

bool Steam_Overlay::RunNetwork(Network_Message_pb const& msg)
{
    Steam_Overlay_pb const& so_pb = msg.overlay();

    switch (so_pb.messages_case())
    {
        case Steam_Overlay_pb::MessagesCase::kChat: return on_chat_receive(msg, so_pb.chat());
    }
    
    return false;
}
