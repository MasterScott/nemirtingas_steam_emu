/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Goldberg Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __INCLUDED_STEAM_OVERLAY_H__
#define __INCLUDED_STEAM_OVERLAY_H__

#include "../steam_dll/common_includes.h"
#include "../steam_dll/callback_manager.h"
#include "../steam_dll/network.h"

static constexpr size_t max_chat_len = 768;

enum window_state : uint8_t
{
    none           = 0,
    show           = 1<<0,
    join           = 1<<1,
    lobby_invite   = 1<<2,
    rich_invite    = 1<<3,
    need_attention = 1<<4,

    has_invite = (lobby_invite | rich_invite),
};

UTILS_ENABLE_BITMASK_OPERATORS(window_state);

struct friend_window_state
{
    static constexpr std::chrono::milliseconds invite_expiration = std::chrono::milliseconds(15000); // You have 15 seconds to accept or refuse the invitation

    int id;
    window_state state;
    std::string title;
    std::string friend_name;
    uint32_t appid;
    union // The invitation (if any)
    {
        uint64 lobby_id;
        char connect[k_cchMaxRichPresenceValueLength];
    };
    std::string chat_history;
    char chat_input[max_chat_len];
    std::chrono::steady_clock::time_point invite_time;

    const void* avatar_handle;
};

enum notification_type
{
    notification_type_message = 0,
    notification_type_invite,
    notification_type_achievement,
};

struct Notification
{
    static constexpr float width = 0.25;
    static constexpr float height = 5.0;
    static constexpr std::chrono::milliseconds fade_in   = std::chrono::milliseconds(2000);
    static constexpr std::chrono::milliseconds fade_out  = std::chrono::milliseconds(2000);
    static constexpr std::chrono::milliseconds show_time = std::chrono::milliseconds(6000) + fade_in + fade_out;
    static constexpr std::chrono::milliseconds fade_out_start = show_time - fade_out;
    static constexpr float r = 0.16f;
    static constexpr float g = 0.29f;
    static constexpr float b = 0.48f;
    static constexpr float max_alpha = 0.66f;

    int id;
    uint8 type;
    std::chrono::seconds start_time;
    std::string message;
    std::pair<const CSteamID, friend_window_state>* frd;
};

#ifndef __APPLE__

class Steam_Overlay:
    public IRunNetwork
{
    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;

    // friend id, show client window (to chat and accept invite maybe)
    std::map<CSteamID, friend_window_state> friends;

    std::string log_buffer;
    std::vector<size_t> log_lines_offsets;

    bool setup_overlay_called;
    bool is_ready;
    bool show_overlay;
    ENotificationPosition notif_position;
    int h_inset, v_inset;

    std::vector<Notification> notifications;
    std::recursive_mutex notifications_mutex;
    bool overlay_state_changed;

    std::recursive_mutex overlay_mutex;
    CSteamID lobby_id;
    std::string rich_connect;

    Steam_Overlay(Steam_Overlay const&) = delete;
    Steam_Overlay(Steam_Overlay&&) = delete;
    Steam_Overlay& operator=(Steam_Overlay const&) = delete;
    Steam_Overlay& operator=(Steam_Overlay&&) = delete;

    static void log_function(void* user_param, Log::LogLevel lv, const char* message);

    void NotifyUser(friend_window_state& friend_state);

    // Right click on friend
    void BuildContextMenu(CSteamID frd, friend_window_state &state);
    // Double click on friend
    void BuildFriendWindow(CSteamID frd, friend_window_state &state);
    // Notifications like achievements, chat and invitations
    void BuildNotifications(float width, float height);

    void CreateFonts();
    void CreateTextures();
public:
    Steam_Overlay();
    ~Steam_Overlay();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    virtual bool RunNetwork(Network_Message_pb const& msg);

    // Send Network messages
    bool send_chat(uint64 remote_id, Overlay_Chat_pb* chat);
    // Receive Network messages
    bool on_chat_receive(Network_Message_pb const& msg, Overlay_Chat_pb const& chat);

    bool Ready() const;

    bool NeedPresent() const;

    void SetNotificationPosition(ENotificationPosition eNotificationPosition);

    void SetNotificationInset(int nHorizontalInset, int nVerticalInset);
    void SetupOverlay();

    void HookReady(bool is_ready);

    void OverlayProc();

    void OpenOverlayInvite(CSteamID lobbyId);
    void OpenOverlay(const char* pchDialog);

    bool ShowOverlay() const;
    void ShowOverlay(bool state);

    void SetLobbyInvite(CSteamID friendId, uint64 lobbyId);
    void SetRichInvite(CSteamID friendId, const char* connect_str);

    void FriendConnect(CSteamID _friend, std::string const& name, uint32_t appid);
    void FriendDisconnect(CSteamID _friend);
    void FriendImageLoaded(CSteamID _friend, std::shared_ptr<Image> img);
    void FriendNameChanged(CSteamID _friend, std::string const& name);

    void AddMessageNotification(std::string const& message);
    void AddAchievementNotification(nlohmann::json const& ach);
    void AddInviteNotification(std::pair<const CSteamID, friend_window_state> &wnd_state);
};

#else

class Steam_Overlay:
    public IRunCallback,
    public IRunNetwork
{
public:
    Steam_Overlay();
    ~Steam_Overlay();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    virtual bool CBRunFrame();
    virtual bool RunNetwork(Network_Message_pb const& msg);
    virtual bool RunCallbacks(pFrameResult_t res);

    // Send Network messages
    bool send_chat(uint64 remote_id, Overlay_Chat_pb* chat);
    // Receive Network messages
    bool on_chat_receive(Network_Message_pb const& msg, Overlay_Chat_pb const& chat);

    bool Ready() const;

    bool NeedPresent() const;

    void SetNotificationPosition(ENotificationPosition eNotificationPosition);

    void SetNotificationInset(int nHorizontalInset, int nVerticalInset);
    void SetupOverlay();

    void HookReady(bool is_ready);

    void OverlayProc();

    void OpenOverlayInvite(CSteamID lobbyId);
    void OpenOverlay(const char* pchDialog);

    bool ShowOverlay() const;
    void ShowOverlay(bool state);

    void SetLobbyInvite(CSteamID friendId, uint64 lobbyId);
    void SetRichInvite(CSteamID friendId, const char* connect_str);

    void FriendConnect(CSteamID _friend, std::string const& name, uint32_t appid);
    void FriendDisconnect(CSteamID _friend);
    void FriendImageLoaded(CSteamID _friend, std::shared_ptr<Image> img);
    void FriendNameChanged(CSteamID _friend, std::string const& name);

    void AddAchievementNotification(nlohmann::json const& ach);
};

#endif

#endif//__INCLUDED_STEAM_OVERLAY_H__
