/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Goldberg Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __INCLUDED_DX11_HOOK_H__
#define __INCLUDED_DX11_HOOK_H__

#include "../Renderer_Hook.h"
#ifndef NO_OVERLAY

#include <d3d11.h>
#include "DirectX_VTables.h"

class DX11_Hook : public Renderer_Hook
{
public:
    static constexpr const char *DLL_NAME = "d3d11.dll";

private:
    static DX11_Hook* _inst;

    // Variables
    bool hooked;
    bool initialized;
    ID3D11Device* pDevice;
    ID3D11DeviceContext* pContext;
    ID3D11RenderTargetView* mainRenderTargetView;

    // Functions
    DX11_Hook();

    void resetRenderState();
    void prepareForOverlay(IDXGISwapChain* pSwapChain);

    // Hook to render functions
    static HRESULT STDMETHODCALLTYPE MyPresent(IDXGISwapChain* _this, UINT SyncInterval, UINT Flags);
    static HRESULT STDMETHODCALLTYPE MyResizeTarget(IDXGISwapChain* _this, const DXGI_MODE_DESC* pNewTargetParameters);
    static HRESULT STDMETHODCALLTYPE MyResizeBuffers(IDXGISwapChain* _this, UINT BufferCount, UINT Width, UINT Height, DXGI_FORMAT NewFormat, UINT SwapChainFlags);

    decltype(&IDXGISwapChain::Present)       Present;
    decltype(&IDXGISwapChain::ResizeBuffers) ResizeBuffers;
    decltype(&IDXGISwapChain::ResizeTarget)  ResizeTarget;

public:
    virtual ~DX11_Hook();

    bool start_hook();
    static DX11_Hook* Inst();
    virtual const char* get_lib_name() const;

    void loadFunctions(IDXGISwapChain *pSwapChain);

    virtual void const* CreateImageRessource(void const* image_buffer, int32_t width, int32_t height);
    virtual void        FreeImageRessource(void const* handle);
};

#endif//NO_OVERLAY

#endif//__INCLUDED_DX11_HOOK_H__
