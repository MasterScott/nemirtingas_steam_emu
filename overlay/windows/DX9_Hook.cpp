/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Goldberg Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "DX9_Hook.h"
#include "Windows_Hook.h"
#include "Renderer_Detector.h"
#include "../steam_dll/steam_client.h"

#include <imgui.h>
#include <impls/windows/imgui_impl_dx9.h>

DX9_Hook* DX9_Hook::_inst = nullptr;

bool DX9_Hook::start_hook()
{
    if (!hooked)
    {
        if (!Windows_Hook::Inst()->start_hook())
            return false;

        APP_LOG(Log::LogLevel::INFO, "Hooked DirectX 9\n");
        hooked = true;
        
        Renderer_Detector::Inst().renderer_found(this);

        BeginHook();
        HookFuncs(
            std::make_pair<void**, void*>(&(PVOID&)Reset, &DX9_Hook::MyReset),
            std::make_pair<void**, void*>(&(PVOID&)Present, &DX9_Hook::MyPresent)
        );
        if (PresentEx != nullptr)
        {
            HookFuncs(
                std::make_pair<void**, void*>(&(PVOID&)PresentEx, &DX9_Hook::MyPresentEx)
                //std::make_pair<void**, void*>(&(PVOID&)EndScene, &DX9_Hook::MyEndScene)
            );
        }
        EndHook();
    }
    return true;
}

void DX9_Hook::resetRenderState()
{
    if (initialized)
    {
        GetSteam_Overlay().HookReady(false);

        ImGui_ImplDX9_Shutdown();
        Windows_Hook::Inst()->resetRenderState();
        ImGui::DestroyContext();

        pDevice->Release();
        pDevice = nullptr;
        last_window = nullptr;
        initialized = false;
    }
}

// Try to make this function and overlay's proc as short as possible or it might affect game's fps.
void DX9_Hook::prepareForOverlay(IDirect3DDevice9 *pDevice)
{
    D3DDEVICE_CREATION_PARAMETERS param;
    pDevice->GetCreationParameters(&param);

    // Workaround to detect if we changed window.
    if (param.hFocusWindow == last_window || this->pDevice != pDevice)
        resetRenderState();

    if (!initialized)
    {
        pDevice->AddRef();
        this->pDevice = pDevice;

        ImGui::CreateContext();
        ImGui_ImplDX9_Init(pDevice);
        GetSteam_Overlay().HookReady(true);

        last_window = param.hFocusWindow;
        initialized = true;
    }
    
    if (ImGui_ImplDX9_NewFrame() && Windows_Hook::Inst()->prepareForOverlay(param.hFocusWindow))
    {
        ImGui::NewFrame();

        GetSteam_Overlay().OverlayProc();

        ImGui::Render();

        ImGui_ImplDX9_RenderDrawData(ImGui::GetDrawData());
    }
}

HRESULT STDMETHODCALLTYPE DX9_Hook::MyReset(IDirect3DDevice9* _this, D3DPRESENT_PARAMETERS* pPresentationParameters)
{
    DX9_Hook::Inst()->resetRenderState();
    return (_this->*DX9_Hook::Inst()->Reset)(pPresentationParameters);
}

HRESULT STDMETHODCALLTYPE DX9_Hook::MyEndScene(IDirect3DDevice9* _this)
{   
    if( !DX9_Hook::Inst()->uses_present )
        DX9_Hook::Inst()->prepareForOverlay(_this);
    return (_this->*DX9_Hook::Inst()->EndScene)();
}

HRESULT STDMETHODCALLTYPE DX9_Hook::MyPresent(IDirect3DDevice9* _this, CONST RECT* pSourceRect, CONST RECT* pDestRect, HWND hDestWindowOverride, CONST RGNDATA* pDirtyRegion)
{
    DX9_Hook::Inst()->uses_present = true;
    DX9_Hook::Inst()->prepareForOverlay(_this);
    return (_this->*DX9_Hook::Inst()->Present)(pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion);
}

HRESULT STDMETHODCALLTYPE DX9_Hook::MyPresentEx(IDirect3DDevice9Ex* _this, CONST RECT* pSourceRect, CONST RECT* pDestRect, HWND hDestWindowOverride, CONST RGNDATA* pDirtyRegion, DWORD dwFlags)
{
    DX9_Hook::Inst()->uses_present = true;
    DX9_Hook::Inst()->prepareForOverlay(_this);
    return (_this->*DX9_Hook::Inst()->PresentEx)(pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion, dwFlags);
}

DX9_Hook::DX9_Hook():
    initialized(false),
    hooked(false),
    uses_present(false),
    last_window(nullptr),
    EndScene(nullptr),
    Present(nullptr),
    PresentEx(nullptr),
    Reset(nullptr)
{
    _library = LoadLibrary(DLL_NAME);
}

DX9_Hook::~DX9_Hook()
{
    APP_LOG(Log::LogLevel::INFO, "DX9 Hook removed\n");

    if (initialized)
    {
        ImGui_ImplDX9_InvalidateDeviceObjects();
        ImGui::DestroyContext();
    }

    FreeLibrary(reinterpret_cast<HMODULE>(_library));

    _inst = nullptr;
}

DX9_Hook* DX9_Hook::Inst()
{
    if( _inst == nullptr )
        _inst = new DX9_Hook;

    return _inst;
}

const char* DX9_Hook::get_lib_name() const
{
    return DLL_NAME;
}

void DX9_Hook::loadFunctions(IDirect3DDevice9* pDevice, bool ex)
{
    void** vTable = *reinterpret_cast<void***>(pDevice);

#define LOAD_FUNC(X) (void*&)X = vTable[(int)IDirect3DDevice9VTable::X]
    LOAD_FUNC(Reset);
    LOAD_FUNC(EndScene);
    LOAD_FUNC(Present);
    if (ex)
        LOAD_FUNC(PresentEx);
#undef LOAD_FUNC
}

void const* DX9_Hook::CreateImageRessource(void const* image_buffer, int32_t width, int32_t height)
{
    IDirect3DTexture9* pTexture = nullptr;

    pDevice->CreateTexture(
        width,
        height,
        1,
        0,
        D3DFMT_A8R8G8B8,
        D3DPOOL_DEFAULT,
        &pTexture,
        nullptr
    );

    if (pTexture != nullptr)
    {
        D3DLOCKED_RECT rect;
        if (FAILED(pTexture->LockRect(0, &rect, nullptr, D3DLOCK_DISCARD)))
        {
            pTexture->Release();
            return nullptr;
        }

        uint32_t const* pixels = reinterpret_cast<uint32_t const*>(image_buffer);
        uint32_t* texture_bits = reinterpret_cast<uint32_t*>(rect.pBits);
        for (int32_t i = 0; i < height; ++i)
        {
            for (int32_t j = 0; j < width; ++j)
            {
                // RGBA to ARGB Conversion, DX9 doesn't have a RGBA loader
                *texture_bits = ((*pixels) << 8) | (*pixels & 0xFF000000) >> 24;

                ++texture_bits;
                ++pixels;
            }
        }

        if (FAILED(pTexture->UnlockRect(0)))
        {
            pTexture->Release();
            return nullptr;
        }
    }

    return pTexture;
}

void        DX9_Hook::FreeImageRessource(void const* handle)
{
    if (handle == nullptr) return;

    IDirect3DTexture9* resource = const_cast<IDirect3DTexture9*>(reinterpret_cast<IDirect3DTexture9 const*>(handle));
    resource->Release();
}