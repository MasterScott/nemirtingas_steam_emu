/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Goldberg Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "OpenGL_Hook.h"
#include "Windows_Hook.h"
#include "Renderer_Detector.h"
#include "../steam_dll/steam_client.h"

#include <imgui.h>
#include <impls/imgui_impl_opengl3.h>

#include <GL/glew.h>

OpenGL_Hook* OpenGL_Hook::_inst = nullptr;

bool OpenGL_Hook::start_hook()
{
    bool res = true;
    if (!hooked)
    {
        if (!Windows_Hook::Inst()->start_hook())
            return false;

        GLenum err = glewInit();

        if (err == GLEW_OK)
        {
            APP_LOG(Log::LogLevel::INFO, "Hooked OpenGL\n");

            hooked = true;
            Renderer_Detector::Inst().renderer_found(this);

            UnhookAll();
            BeginHook();
            HookFuncs(
                std::make_pair<void**, void*>(&(PVOID&)wglSwapBuffers, &OpenGL_Hook::MywglSwapBuffers)
            );
            EndHook();
        }
        else
        {
            APP_LOG(Log::LogLevel::WARN, "Failed to hook OpenGL\n");
            /* Problem: glewInit failed, something is seriously wrong. */
            APP_LOG(Log::LogLevel::WARN, "Error: %s\n", glewGetErrorString(err));
            res = false;
        }
    }
    return true;
}

void OpenGL_Hook::resetRenderState()
{
    if (initialized)
    {
        GetSteam_Overlay().HookReady(false);

        ImGui_ImplOpenGL3_Shutdown();
        Windows_Hook::Inst()->resetRenderState();
        ImGui::DestroyContext();

        last_window = nullptr;
        initialized = false;
    }
}

// Try to make this function and overlay's proc as short as possible or it might affect game's fps.
void OpenGL_Hook::prepareForOverlay(HDC hDC)
{
    HWND hWnd = WindowFromDC(hDC);

    if (hWnd != last_window)
        resetRenderState();

    if (!initialized)
    {
        ImGui::CreateContext();
        ImGui_ImplOpenGL3_Init();
        GetSteam_Overlay().HookReady(true);

        last_window = hWnd;
        initialized = true;
    }
    if (ImGui_ImplOpenGL3_NewFrame() && Windows_Hook::Inst()->prepareForOverlay(hWnd))
    {
        ImGui::NewFrame();

        GetSteam_Overlay().OverlayProc();

        ImGui::Render();

        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    }
}

BOOL WINAPI OpenGL_Hook::MywglSwapBuffers(HDC hDC)
{
    OpenGL_Hook::Inst()->prepareForOverlay(hDC);
    return OpenGL_Hook::Inst()->wglSwapBuffers(hDC);
}

OpenGL_Hook::OpenGL_Hook():
    hooked(false),
    initialized(false),
    last_window(nullptr),
    wglSwapBuffers(nullptr)
{
    _library = LoadLibrary(DLL_NAME);
}

OpenGL_Hook::~OpenGL_Hook()
{
    APP_LOG(Log::LogLevel::INFO, "OpenGL Hook removed\n");

    if (initialized)
    {
        ImGui_ImplOpenGL3_Shutdown();
        ImGui::DestroyContext();
    }

    FreeLibrary(reinterpret_cast<HMODULE>(_library));

    _inst = nullptr;
}

OpenGL_Hook* OpenGL_Hook::Inst()
{
    if (_inst == nullptr)
        _inst = new OpenGL_Hook;

    return _inst;
}

const char* OpenGL_Hook::get_lib_name() const
{
    return DLL_NAME;
}

void OpenGL_Hook::loadFunctions(wglSwapBuffers_t pfnwglSwapBuffers)
{
    wglSwapBuffers = pfnwglSwapBuffers;
}

void const* OpenGL_Hook::CreateImageRessource(void const* image_buffer, int32_t width, int32_t height)
{
    GLuint texture;
    glGenTextures(1, &texture);

    // Save old texture id
    GLint oldTex;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &oldTex);

    glBindTexture(GL_TEXTURE_2D, texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Upload pixels into texture
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_buffer);

    glBindTexture(GL_TEXTURE_2D, oldTex);

    return reinterpret_cast<void*>(texture);
}

void        OpenGL_Hook::FreeImageRessource(void const* handle)
{
    if (handle == nullptr) return;

    GLuint const texture = reinterpret_cast<GLuint const>(handle);
    glDeleteTextures(1, &texture);
}