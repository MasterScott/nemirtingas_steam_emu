/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Goldberg Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "Renderer_Detector.h"
#include "../Hook_Manager.h"

constexpr int max_hook_retries = 500;

#include "DX12_Hook.h"
#include "DX11_Hook.h"
#include "DX10_Hook.h"
#include "DX9_Hook.h"
#include "OpenGL_Hook.h"
#include "Windows_Hook.h"

static decltype(&IDXGISwapChain::Present) _IDXGISwapChain_Present = nullptr;
static decltype(&IDirect3DDevice9::Present) _IDirect3DDevice9_Present = nullptr;
static decltype(&IDirect3DDevice9Ex::PresentEx) _IDirect3DDevice9Ex_PresentEx = nullptr;
static decltype(SwapBuffers)* _wglSwapBuffers = nullptr;

static constexpr auto windowClassName = "___overlay_window_class___";

void Renderer_Detector::create_hwnd()
{
    if (dummy_hWnd == nullptr)
    {
        HINSTANCE hInst = GetModuleHandle(nullptr);
        if (atom == 0)
        {
            // Register a window class for creating our render window with.
            WNDCLASSEX windowClass = {};

            windowClass.cbSize = sizeof(WNDCLASSEX);
            windowClass.style = CS_HREDRAW | CS_VREDRAW;
            windowClass.lpfnWndProc = DefWindowProc;
            windowClass.cbClsExtra = 0;
            windowClass.cbWndExtra = 0;
            windowClass.hInstance = hInst;
            windowClass.hIcon = NULL;
            windowClass.hCursor = ::LoadCursor(NULL, IDC_ARROW);
            windowClass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
            windowClass.lpszMenuName = NULL;
            windowClass.lpszClassName = windowClassName;
            windowClass.hIconSm = NULL;

            atom = ::RegisterClassEx(&windowClass);
        }

        if (atom > 0)
        {
            dummy_hWnd = ::CreateWindowEx(
                NULL,
                windowClassName,
                "",
                WS_OVERLAPPEDWINDOW,
                0,
                0,
                1,
                1,
                NULL,
                NULL,
                hInst,
                nullptr
            );

            assert(dummy_hWnd && "Failed to create window");
        }
    }

}

void Renderer_Detector::destroy_hwnd()
{
    if (dummy_hWnd != nullptr)
    {
        DestroyWindow(dummy_hWnd);
        UnregisterClass(windowClassName, GetModuleHandle(nullptr));
    }
}

HRESULT STDMETHODCALLTYPE Renderer_Detector::MyIDXGISwapChain_Present(IDXGISwapChain* _this, UINT SyncInterval, UINT Flags)
{
    Renderer_Detector& inst = Renderer_Detector::Inst();
    Hook_Manager& hm = Hook_Manager::Inst();

    auto res = (_this->*_IDXGISwapChain_Present)(SyncInterval, Flags);
    if (!inst.stop_retry())
    {
        IUnknown* pDevice = nullptr;
        if (inst._dx12_hooked)
        {
            _this->GetDevice(IID_PPV_ARGS(reinterpret_cast<ID3D12Device**>(&pDevice)));
        }
        if (pDevice)
        {
            DX12_Hook::Inst()->start_hook();
        }
        else
        {
            if (inst._dx11_hooked)
            {
                _this->GetDevice(IID_PPV_ARGS(reinterpret_cast<ID3D11Device**>(&pDevice)));
            }
            if (pDevice)
            {
                DX11_Hook::Inst()->start_hook();
            }
            else
            {
                if (inst._dx10_hooked)
                {
                    _this->GetDevice(IID_PPV_ARGS(reinterpret_cast<ID3D10Device**>(&pDevice)));
                }
                if (pDevice)
                {
                    DX10_Hook::Inst()->start_hook();
                }
            }
        }
        if (pDevice) pDevice->Release();
    }

    return res;
}

HRESULT STDMETHODCALLTYPE Renderer_Detector::MyPresent(IDirect3DDevice9* _this, CONST RECT* pSourceRect, CONST RECT* pDestRect, HWND hDestWindowOverride, CONST RGNDATA* pDirtyRegion)
{
    Renderer_Detector& inst = Renderer_Detector::Inst();
    Hook_Manager& hm = Hook_Manager::Inst();
    auto res = (_this->*_IDirect3DDevice9_Present)(pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion);
    if (!inst.stop_retry())
    {
        DX9_Hook::Inst()->start_hook();
    }
    return res;
}

HRESULT STDMETHODCALLTYPE Renderer_Detector::MyPresentEx(IDirect3DDevice9Ex* _this, CONST RECT* pSourceRect, CONST RECT* pDestRect, HWND hDestWindowOverride, CONST RGNDATA* pDirtyRegion, DWORD dwFlags)
{
    Renderer_Detector& inst = Renderer_Detector::Inst();
    Hook_Manager& hm = Hook_Manager::Inst();
    auto res = (_this->*_IDirect3DDevice9Ex_PresentEx)(pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion, dwFlags);
    if (!inst.stop_retry())
    {
        DX9_Hook::Inst()->start_hook();
    }
    return res;
}

BOOL WINAPI Renderer_Detector::MywglSwapBuffers(HDC hDC)
{
    Renderer_Detector& inst = Renderer_Detector::Inst();
    Hook_Manager& hm = Hook_Manager::Inst();
    auto res = _wglSwapBuffers(hDC);
    if (!inst.stop_retry())
    {
        OpenGL_Hook::Inst()->start_hook();
    }
    return res;
}

void Renderer_Detector::HookDXGIPresent(IDXGISwapChain* pSwapChain)
{
    if (!_dxgi_hooked)
    {
        _dxgi_hooked = true;
        (void*&)_IDXGISwapChain_Present = (*reinterpret_cast<void***>(pSwapChain))[(int)IDXGISwapChainVTable::Present];

        rendererdetect_hook->BeginHook();

        rendererdetect_hook->HookFuncs(
            std::pair<void**, void*>((PVOID*)& _IDXGISwapChain_Present, &Renderer_Detector::MyIDXGISwapChain_Present)
        );

        rendererdetect_hook->EndHook();
    }
}

void Renderer_Detector::HookDX9Present(IDirect3DDevice9* pDevice, bool ex)
{
    (void*&)_IDirect3DDevice9_Present = (*reinterpret_cast<void***>(pDevice))[(int)IDirect3DDevice9VTable::Present];
    if (ex)
        (void*&)_IDirect3DDevice9Ex_PresentEx = (*reinterpret_cast<void***>(pDevice))[(int)IDirect3DDevice9VTable::PresentEx];

    rendererdetect_hook->BeginHook();

    rendererdetect_hook->HookFuncs(
        std::pair<void**, void*>((PVOID*)& _IDirect3DDevice9_Present, &Renderer_Detector::MyPresent)
    );
    if (ex)
    {
        rendererdetect_hook->HookFuncs(
            std::pair<void**, void*>((PVOID*)& _IDirect3DDevice9Ex_PresentEx, &Renderer_Detector::MyPresentEx)
        );
    }

    rendererdetect_hook->EndHook();
}

void Renderer_Detector::HookwglSwapBuffers(decltype(SwapBuffers)* wglSwapBuffers)
{
    _wglSwapBuffers = wglSwapBuffers;

    rendererdetect_hook->BeginHook();

    rendererdetect_hook->HookFuncs(
        std::pair<void**, void*>((PVOID*)& _wglSwapBuffers, &Renderer_Detector::MywglSwapBuffers)
    );

    rendererdetect_hook->EndHook();
}

void Renderer_Detector::hook_dx9()
{
    if (!_dx9_hooked && !_renderer_found)
    {
        create_hwnd();
        if (dummy_hWnd == nullptr)
            return;

        IDirect3D9Ex* pD3D = nullptr;
        IUnknown* pDevice = nullptr;
        HMODULE library = GetModuleHandle(DX9_Hook::DLL_NAME);
        decltype(Direct3DCreate9Ex)* Direct3DCreate9Ex = nullptr;
        if (library != nullptr)
        {
            Direct3DCreate9Ex = (decltype(Direct3DCreate9Ex))GetProcAddress(library, "Direct3DCreate9Ex");
            D3DPRESENT_PARAMETERS params = {};
            params.BackBufferWidth = 1;
            params.BackBufferHeight = 1;
            params.hDeviceWindow = dummy_hWnd;
            params.BackBufferCount = 1;
            params.Windowed = TRUE;
            params.SwapEffect = D3DSWAPEFFECT_DISCARD;

            if (Direct3DCreate9Ex != nullptr)
            {
                Direct3DCreate9Ex(D3D_SDK_VERSION, &pD3D);
                pD3D->CreateDeviceEx(D3DADAPTER_DEFAULT, D3DDEVTYPE_NULLREF, NULL, D3DCREATE_HARDWARE_VERTEXPROCESSING, &params, NULL, reinterpret_cast<IDirect3DDevice9Ex * *>(&pDevice));
            }
            else
            {
                decltype(Direct3DCreate9)* Direct3DCreate9 = (decltype(Direct3DCreate9))GetProcAddress(library, "Direct3DCreate9");
                if (Direct3DCreate9 != nullptr)
                {
                    pD3D = reinterpret_cast<IDirect3D9Ex*>(Direct3DCreate9(D3D_SDK_VERSION));
                    pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_NULLREF, NULL, D3DCREATE_HARDWARE_VERTEXPROCESSING, &params, reinterpret_cast<IDirect3DDevice9 * *>(&pDevice));
                }
            }
        }

        if (pDevice != nullptr)
        {
            APP_LOG(Log::LogLevel::INFO, "Hooked D3D9::Present to detect DX Version\n");

            _dx9_hooked = true;
            auto h = DX9_Hook::Inst();
            h->loadFunctions(reinterpret_cast<IDirect3DDevice9*>(pDevice), Direct3DCreate9Ex != nullptr);
            Hook_Manager::Inst().AddHook(h);
            HookDX9Present(reinterpret_cast<IDirect3DDevice9*>(pDevice), Direct3DCreate9Ex != nullptr);
        }
        else
        {
            APP_LOG(Log::LogLevel::WARN, "Failed to hook D3D9::Present to detect DX Version\n");
        }

        if (pDevice) pDevice->Release();
        if (pD3D) pD3D->Release();
    }
}

void Renderer_Detector::hook_dx10()
{
    if (!_dx10_hooked && !_renderer_found)
    {
        create_hwnd();
        if (dummy_hWnd == nullptr)
            return;

        IDXGISwapChain* pSwapChain = nullptr;
        ID3D10Device* pDevice = nullptr;
        HMODULE library = GetModuleHandle(DX10_Hook::DLL_NAME);
        if (library != nullptr)
        {
            decltype(D3D10CreateDeviceAndSwapChain)* D3D10CreateDeviceAndSwapChain =
                (decltype(D3D10CreateDeviceAndSwapChain))GetProcAddress(library, "D3D10CreateDeviceAndSwapChain");
            if (D3D10CreateDeviceAndSwapChain != nullptr)
            {
                DXGI_SWAP_CHAIN_DESC SwapChainDesc = {};

                SwapChainDesc.BufferCount = 1;
                SwapChainDesc.BufferDesc.Width = 1;
                SwapChainDesc.BufferDesc.Height = 1;
                SwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
                SwapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
                SwapChainDesc.BufferDesc.RefreshRate.Denominator = 0;
                SwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
                SwapChainDesc.OutputWindow = dummy_hWnd;
                SwapChainDesc.SampleDesc.Count = 1;
                SwapChainDesc.SampleDesc.Quality = 0;
                SwapChainDesc.Windowed = TRUE;

                D3D10CreateDeviceAndSwapChain(NULL, D3D10_DRIVER_TYPE_NULL, NULL, 0, D3D10_SDK_VERSION, &SwapChainDesc, &pSwapChain, &pDevice);
            }
        }
        if (pSwapChain != nullptr)
        {
            APP_LOG(Log::LogLevel::INFO, "Hooked IDXGISwapChain::Present to detect DX Version\n");

            _dx10_hooked = true;
            auto h = DX10_Hook::Inst();
            h->loadFunctions(pSwapChain);
            Hook_Manager::Inst().AddHook(h);
            HookDXGIPresent(pSwapChain);
        }
        else
        {
            APP_LOG(Log::LogLevel::WARN, "Failed to Hook IDXGISwapChain::Present to detect DX Version\n");
        }
        if (pDevice)pDevice->Release();
        if (pSwapChain)pSwapChain->Release();
    }
}

void Renderer_Detector::hook_dx11()
{
    if (!_dx11_hooked && !_renderer_found)
    {
        create_hwnd();
        if (dummy_hWnd == nullptr)
            return;

        IDXGISwapChain* pSwapChain = nullptr;
        ID3D11Device* pDevice = nullptr;
        HMODULE library = GetModuleHandle(DX11_Hook::DLL_NAME);
        if (library != nullptr)
        {
            decltype(D3D11CreateDeviceAndSwapChain)* D3D11CreateDeviceAndSwapChain =
                (decltype(D3D11CreateDeviceAndSwapChain))GetProcAddress(library, "D3D11CreateDeviceAndSwapChain");
            if (D3D11CreateDeviceAndSwapChain != nullptr)
            {
                DXGI_SWAP_CHAIN_DESC SwapChainDesc = {};

                SwapChainDesc.BufferCount = 1;
                SwapChainDesc.BufferDesc.Width = 1;
                SwapChainDesc.BufferDesc.Height = 1;
                SwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
                SwapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
                SwapChainDesc.BufferDesc.RefreshRate.Denominator = 0;
                SwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
                SwapChainDesc.OutputWindow = dummy_hWnd;
                SwapChainDesc.SampleDesc.Count = 1;
                SwapChainDesc.SampleDesc.Quality = 0;
                SwapChainDesc.Windowed = TRUE;

                D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_NULL, NULL, 0, NULL, NULL, D3D11_SDK_VERSION, &SwapChainDesc, &pSwapChain, &pDevice, NULL, NULL);
            }
        }
        if (pSwapChain != nullptr)
        {
            APP_LOG(Log::LogLevel::INFO, "Hooked IDXGISwapChain::Present to detect DX Version\n");

            _dx11_hooked = true;
            auto h = DX11_Hook::Inst();
            h->loadFunctions(pSwapChain);
            Hook_Manager::Inst().AddHook(h);
            HookDXGIPresent(pSwapChain);
        }
        else
        {
            APP_LOG(Log::LogLevel::WARN, "Failed to Hook IDXGISwapChain::Present to detect DX Version\n");
        }

        if (pDevice) pDevice->Release();
        if (pSwapChain) pSwapChain->Release();
    }
}

void Renderer_Detector::hook_dx12()
{
    if (!_dx12_hooked && !_renderer_found)
    {
        create_hwnd();
        if (dummy_hWnd == nullptr)
            return;

        IDXGIFactory4* pDXGIFactory = nullptr;
        IDXGISwapChain1* pSwapChain = nullptr;
        ID3D12CommandQueue* pCommandQueue = nullptr;
        ID3D12Device* pDevice = nullptr;
        ID3D12CommandAllocator* pCommandAllocator = nullptr;
        ID3D12GraphicsCommandList* pCommandList = nullptr;

        HMODULE library = GetModuleHandle(DX12_Hook::DLL_NAME);
        if (library != nullptr)
        {
            decltype(D3D12CreateDevice)* D3D12CreateDevice =
                (decltype(D3D12CreateDevice))GetProcAddress(library, "D3D12CreateDevice");

            if (D3D12CreateDevice != nullptr)
            {
                D3D12CreateDevice(NULL, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&pDevice));

                if (pDevice != nullptr)
                {
                    DXGI_SWAP_CHAIN_DESC1 SwapChainDesc = {};
                    SwapChainDesc.BufferCount = 2;
                    SwapChainDesc.Width = 1;
                    SwapChainDesc.Height = 1;
                    SwapChainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
                    SwapChainDesc.Stereo = FALSE;
                    SwapChainDesc.SampleDesc = { 1, 0 };
                    SwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
                    SwapChainDesc.Scaling = DXGI_SCALING_NONE;
                    SwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
                    SwapChainDesc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;

                    D3D12_COMMAND_QUEUE_DESC queueDesc = {};
                    queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
                    queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
                    pDevice->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&pCommandQueue));

                    if (pCommandQueue != nullptr)
                    {
                        HMODULE dxgi = GetModuleHandle("dxgi.dll");
                        if (dxgi != nullptr)
                        {
                            decltype(CreateDXGIFactory1)* CreateDXGIFactory1 = (decltype(CreateDXGIFactory1))GetProcAddress(dxgi, "CreateDXGIFactory1");
                            if (CreateDXGIFactory1 != nullptr)
                            {
                                CreateDXGIFactory1(IID_PPV_ARGS(&pDXGIFactory));
                                if (pDXGIFactory != nullptr)
                                {
                                    pDXGIFactory->CreateSwapChainForHwnd(pCommandQueue, dummy_hWnd, &SwapChainDesc, NULL, NULL, &pSwapChain);

                                    pDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&pCommandAllocator));
                                    if (pCommandAllocator != nullptr)
                                    {
                                        pDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, pCommandAllocator, NULL, IID_PPV_ARGS(&pCommandList));
                                    }
                                }
                            }
                        }
                    }
                }//if (pDevice != nullptr)
            }//if (D3D12CreateDevice != nullptr)
        }//if (library != nullptr)
        if (pCommandQueue != nullptr && pSwapChain != nullptr)
        {
            APP_LOG(Log::LogLevel::INFO, "Hooked IDXGISwapChain::Present to detect DX Version\n");

            _dx12_hooked = true;
            auto h = DX12_Hook::Inst();
            h->loadFunctions(pCommandQueue, pSwapChain);
            Hook_Manager::Inst().AddHook(h);
            HookDXGIPresent(pSwapChain);
        }
        else
        {
            APP_LOG(Log::LogLevel::WARN, "Failed to Hook IDXGISwapChain::Present to detect DX Version\n");
        }

        if (pCommandList) pCommandList->Release();
        if (pCommandAllocator) pCommandAllocator->Release();
        if (pSwapChain) pSwapChain->Release();
        if (pDXGIFactory) pDXGIFactory->Release();
        if (pCommandQueue) pCommandQueue->Release();
        if (pDevice) pDevice->Release();
    }
}

void Renderer_Detector::hook_opengl()
{
    if (!_ogl_hooked && !_renderer_found)
    {
        HMODULE library = GetModuleHandle(OpenGL_Hook::DLL_NAME);
        OpenGL_Hook::wglSwapBuffers_t wglSwapBuffers = nullptr;
        if (library != nullptr)
        {
            wglSwapBuffers = (decltype(wglSwapBuffers))GetProcAddress(library, "wglSwapBuffers");
        }
        if (wglSwapBuffers != nullptr)
        {
            APP_LOG(Log::LogLevel::INFO, "Hooked wglSwapBuffers to detect OpenGL\n");

            _ogl_hooked = true;
            auto h = OpenGL_Hook::Inst();
            h->loadFunctions(wglSwapBuffers);
            Hook_Manager::Inst().AddHook(h);
            HookwglSwapBuffers(wglSwapBuffers);
        }
        else
        {
            APP_LOG(Log::LogLevel::WARN, "Failed to Hook wglSwapBuffers to detect OpenGL\n");
        }
    }
}

void Renderer_Detector::create_hook(const char* libname)
{
    if (!_stricmp(libname, DX9_Hook::DLL_NAME))
        hook_dx9();
    else if (!_stricmp(libname, DX10_Hook::DLL_NAME))
        hook_dx10();
    else if (!_stricmp(libname, DX11_Hook::DLL_NAME))
        hook_dx11();
    else if (!_stricmp(libname, DX12_Hook::DLL_NAME))
        hook_dx12();
    else if (!_stricmp(libname, OpenGL_Hook::DLL_NAME))
        hook_opengl();
}

void Renderer_Detector::find_renderer_proc(Renderer_Detector* _this)
{
    _this->rendererdetect_hook = new Base_Hook();
    Hook_Manager& hm = Hook_Manager::Inst();
    hm.AddHook(_this->rendererdetect_hook);

    std::vector<std::string> const libraries = {
        OpenGL_Hook::DLL_NAME,
        DX12_Hook::DLL_NAME,
        DX11_Hook::DLL_NAME,
        DX10_Hook::DLL_NAME,
        DX9_Hook::DLL_NAME,
    };

    while (!_this->_renderer_found && !_this->stop_retry())
    {
        std::vector<std::string>::const_iterator it = libraries.begin();
        while (it != libraries.end())
        {
            {
                std::lock_guard<std::mutex> lock(_this->_found_mutex);
                if (_this->_renderer_found)
                    break;

                it = std::find_if(it, libraries.end(), [](std::string const& name)
                {
                    auto x = get_module_handle(name.c_str());
                    if (x != NULL)
                        return true;
                    return false;
                });

                if (it == libraries.end())
                    break;

                _this->create_hook(it->c_str());
                ++it;
            }
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }

    _this->destroy_hwnd();

    if (_this->game_renderer == nullptr) // Couldn't hook renderer
    {
        hm.RemoveHook(Windows_Hook::Inst());
    }
    else
    {
        hm.AddHook(Windows_Hook::Inst());
    }
    if (_this->_ogl_hooked)
    {
        auto h = OpenGL_Hook::Inst();
        if (h != _this->game_renderer)
        {
            _this->_ogl_hooked = false;
            hm.RemoveHook(h);
        }
    }
    if (_this->_dx9_hooked)
    {
        auto h = DX9_Hook::Inst();
        if (h != _this->game_renderer)
        {
            _this->_dx9_hooked = false;
            hm.RemoveHook(h);
        }
    }
    if (_this->_dx10_hooked)
    {
        auto h = DX10_Hook::Inst();
        if (h != _this->game_renderer)
        {
            _this->_dx10_hooked = false;
            hm.RemoveHook(h);
        }
    }
    if (_this->_dx11_hooked)
    {
        auto h = DX11_Hook::Inst();
        if (h != _this->game_renderer)
        {
            _this->_dx11_hooked = false;
            hm.RemoveHook(h);
        }
    }
    if (_this->_dx12_hooked)
    {
        auto h = DX12_Hook::Inst();
        if (h != _this->game_renderer)
        {
            _this->_dx12_hooked = false;
            hm.RemoveHook(h);
        }
    }

    delete _this->_hook_thread;
    _this->_hook_thread = nullptr;
}

Renderer_Detector::Renderer_Detector():
    _hook_thread(nullptr),
    _hook_retries(0),
    _renderer_found(false),
    _dx9_hooked(false),
    _dx10_hooked(false),
    _dx11_hooked(false),
    _dx12_hooked(false),
    _dxgi_hooked(false),
    _ogl_hooked(false),
    rendererdetect_hook(nullptr),
    game_renderer(nullptr),
    atom(0),
    dummy_hWnd(nullptr)
{}

void Renderer_Detector::renderer_found(Renderer_Hook* hook)
{
    std::lock_guard<std::mutex> lock(_found_mutex);
    Hook_Manager& hm = Hook_Manager::Inst();

    _renderer_found = true;
    game_renderer = hook;

    if (hook == nullptr)
        APP_LOG(Log::LogLevel::WARN, "We found a renderer but couldn't hook it, aborting overlay hook.\n");
    else
        APP_LOG(Log::LogLevel::INFO, "Hooked renderer in %d/%d tries\n", _hook_retries, max_hook_retries);

    hm.RemoveHook(rendererdetect_hook);
}

bool Renderer_Detector::stop_retry()
{
    // Retry or not
    bool stop = ++_hook_retries >= max_hook_retries;

    if (stop)
        renderer_found(nullptr);

    return stop;
}

void Renderer_Detector::find_renderer()
{
    if (_hook_thread == nullptr)
    {
        _hook_thread = new std::thread(&Renderer_Detector::find_renderer_proc, this);
        _hook_thread->detach();
    }
}

Renderer_Detector& Renderer_Detector::Inst()
{
    static Renderer_Detector inst;
    return inst;
}

Renderer_Hook* Renderer_Detector::get_renderer() const
{
    return game_renderer;
}

Renderer_Detector::~Renderer_Detector()
{
    if (_hook_thread != nullptr)
    {
        _hook_retries = max_hook_retries;
    }
}
