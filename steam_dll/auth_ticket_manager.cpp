/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "auth_ticket_manager.h"
#include "steam_client.h"
#include "settings.h"

#pragma pack(push, 1)
struct auth_ticket_v2_header_t
{
    uint32_t size;
    uint32_t pad1;
    uint32_t ticket_id;
    uint64_t steam_id;
};

struct auth_ticket_v2_t
{
    auth_ticket_v2_header_t header;
    uint32_t timestamp1;    //      : (char*)&ticket + header.size = &ticket_timestamp1
    uint8_t pad2[28];       //      : no clue, there is no padding for ticket size != 234, 240, 246, 252
    uint32_t v1;            // v15  : (char*)&ticket + header.size + sizeof(pad2) + 4 = &v15
    uint32_t v2;            // v4[0]: (char*)&ticket + header.size + sizeof(pad2) + 8 = &v4, account type ? >= 42 = steam user
                            // v1-v2 == 128
    uint32_t ticket_version;// v4[1]: v4 + 1 = &ticket_version
    uint64_t steamd_id;     // v4[2]: v4 + 2 = &ticket_steamid
    uint32_t v6;            // v4[4]
    uint32_t v7;            // v4[5]
    uint32_t v8;            // v4[6]
    uint32_t v9;            // v4[7]
    uint32_t v10;           // v4[8]
    uint32_t timestamp2;    // v4[9]: v4 + 9 = &ticket_timestamp2
    uint8_t pad3[8];
    uint16_t vac_banned;    // (char*)&ticket + sizeof(auth_ticket_v2_t) - 130
    uint8_t pad4[128];
};
#pragma pack(pop)


static constexpr auto steam_ticket_min_size = sizeof(auth_ticket_v2_header_t);

Auth_Ticket_Manager::Auth_Ticket_Manager()
{
}

Auth_Ticket_Manager::~Auth_Ticket_Manager()
{
    emu_deinit();
}

void Auth_Ticket_Manager::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    std::lock(_local_mutex, cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(cb_manager->_local_mutex, std::adopt_lock);

    _cb_manager = cb_manager;
    _network = network;

    _network->register_listener(this, Network_Message_pb::MessagesCase::kAuth);
}

void Auth_Ticket_Manager::emu_deinit()
{
    if (_network != nullptr)
    {
        std::lock(_local_mutex, _cb_manager->_local_mutex);
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
        std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

        _network->unregister_listener(this, Network_Message_pb::MessagesCase::kAuth);
    }

    {
        std::lock_guard<std::recursive_mutex> lk(_local_mutex);
        _network.reset();
        _cb_manager.reset();
    }
}

Auth_Ticket_Data Auth_Ticket_Manager::get_ticket_data(void* pTicket, int cbMaxTicket, uint32* pcbTicket)
{
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    Auth_Ticket_Data ticket_data;
    auth_ticket_v2_t* sTicket = reinterpret_cast<auth_ticket_v2_t*>(pTicket);

    ticket_data.id = Settings::Inst().userid;
    ticket_data.number = generate_steam_ticket_id();

    memset(pTicket, ' ', cbMaxTicket);
    *pcbTicket = cbMaxTicket;

    sTicket->header.size = sizeof(auth_ticket_v2_header_t);
    sTicket->header.ticket_id = ticket_data.number;
    sTicket->header.steam_id = Settings::Inst().userid.ConvertToUint64();
    sTicket->steamd_id = sTicket->header.steam_id;
    sTicket->ticket_version = 2;
    sTicket->v1 = 170;
    sTicket->v2 = 42;
    sTicket->timestamp1 = time(nullptr) + 99999;
    sTicket->timestamp2 = sTicket->timestamp1;
    sTicket->vac_banned = 0;

    return ticket_data;
}

uint32 Auth_Ticket_Manager::get_ticket(void* pTicket, int cbMaxTicket, uint32* pcbTicket)
{
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    if (cbMaxTicket < steam_ticket_min_size)
        return 0;

    if (cbMaxTicket > STEAM_AUTH_TICKET_SIZE)
        cbMaxTicket = STEAM_AUTH_TICKET_SIZE;

    Auth_Ticket_Data ticket_data = get_ticket_data(pTicket, cbMaxTicket, pcbTicket);
    uint32 ticket_num = ticket_data.number;

    pFrameResult_t result(new FrameResult);
    GetAuthSessionTicketResponse_t& gastr = result->CreateCallback<GetAuthSessionTicketResponse_t>(std::chrono::milliseconds(5000));

    gastr.m_eResult = k_EResultOK;
    gastr.m_hAuthTicket = ticket_num;

    result->done = true;
    _cb_manager->add_callback(nullptr, result);
    
    outbound.push_back(ticket_data);

    return ticket_num;
}

void Auth_Ticket_Manager::cancel_ticket(uint32 number)
{
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto ticket = std::find_if(outbound.begin(), outbound.end(), [&number](Auth_Ticket_Data const& item) { return item.number == number; });
    if (outbound.end() == ticket)
        return;

    Auth_Cancel_pb* auth = new Auth_Cancel_pb;

    send_auth_ticket_cancel(auth);

    outbound.erase(ticket);
}

EBeginAuthSessionResult Auth_Ticket_Manager::begin_auth(const void* pAuthTicket, int cbAuthTicket, CSteamID steamID)
{
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    if (cbAuthTicket < steam_ticket_min_size)
        return k_EBeginAuthSessionResultInvalidTicket;

    auth_ticket_v2_t const* sTicket = reinterpret_cast<auth_ticket_v2_t const*>(pAuthTicket);

    Auth_Ticket_Data data;
    uint64 id;
    uint32 number;

    id = sTicket->header.steam_id;
    number = sTicket->header.steam_id;

    data.id = CSteamID(id);
    data.number = number;
    data.created = std::chrono::high_resolution_clock::now();

    for (auto& t : inbound)
    {
        if (t.id == data.id)
        // && !check_timedout(t.created, STEAM_TICKET_PROCESS_TIME)
        {
            return k_EBeginAuthSessionResultDuplicateRequest;
        }
    }

    inbound.push_back(data);

    pFrameResult_t result(new FrameResult);
    ValidateAuthTicketResponse_t& vatr = result->CreateCallback<ValidateAuthTicketResponse_t>();
    vatr.m_SteamID = id;
    vatr.m_OwnerSteamID = id;
    vatr.m_eAuthSessionResponse = k_EAuthSessionResponseOK;
    
    result->done = true;
    _cb_manager->add_callback(nullptr, result);

    return k_EBeginAuthSessionResultOK;
}

bool Auth_Ticket_Manager::end_auth(CSteamID id)
{
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    bool erased = false;
    for(auto it = inbound.begin(); it != inbound.end(); )
    {
        if (it->id == id)
        {
            erased = true;
            it = inbound.erase(it);
        }
        else
        {
            ++it;
        }
    }

    return erased;
}

uint32 Auth_Ticket_Manager::count_inbound_auths()
{
    return inbound.size();
}

bool Auth_Ticket_Manager::SendUserConnectAndAuthenticate(uint32 unIPClient, const void* pvAuthBlob, uint32 cubAuthBlobSize, CSteamID* pSteamIDUser)
{
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    if (cubAuthBlobSize < steam_ticket_min_size)
        return false;

    auth_ticket_v2_t const* sTicket = reinterpret_cast<auth_ticket_v2_t const*>(pvAuthBlob);
    uint64 id = sTicket->header.steam_id;
    uint32 number = sTicket->header.ticket_id;
    
    Auth_Ticket_Data data;
    data.id = CSteamID(id);
    data.number = number;
    data.created = std::chrono::high_resolution_clock::now();
    if (pSteamIDUser)
        *pSteamIDUser = data.id;

    for (auto& t : inbound)
    {
        if (t.id == data.id)
        {
            pFrameResult_t result(new FrameResult);
            GSClientApprove_t& gsca = result->CreateCallback<GSClientApprove_t>();

            gsca.m_OwnerSteamID = id;
            gsca.m_SteamID = id;

            result->done = true;
            _cb_manager->add_callback(nullptr, result);

            return true;
        }
    }

    inbound.push_back(data);
    
    pFrameResult_t result(new FrameResult);
    GSClientApprove_t& gsca = result->CreateCallback<GSClientApprove_t>();

    gsca.m_OwnerSteamID = id;
    gsca.m_SteamID = id;

    result->done = true;
    _cb_manager->add_callback(nullptr, result);

    return true;
}

CSteamID Auth_Ticket_Manager::fake_user()
{
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    Auth_Ticket_Data data = {};
    data.id = generate_steam_anon_user();
    inbound.push_back(data);
    return data.id;
}

///////////////////////////////////////////////////////////////////////////////
//                           Network Send messages                           //
///////////////////////////////////////////////////////////////////////////////
bool Auth_Ticket_Manager::send_auth_ticket_cancel(Auth_Cancel_pb* auth)
{
    Network_Message_pb msg;
    Steam_Auth_pb* auth_msg = new Steam_Auth_pb;

    auth_msg->set_allocated_cancel(auth);

    msg.set_allocated_auth(auth_msg);

    msg.set_source_id(Settings::Inst().userid.ConvertToUint64());

    return !_network->ReliableSendToAllPeers(msg).empty();
}

///////////////////////////////////////////////////////////////////////////////
//                          Network Receive messages                         //
///////////////////////////////////////////////////////////////////////////////
bool Auth_Ticket_Manager::on_ticket_cancel(Network_Message_pb const& msg, Auth_Cancel_pb const& auth)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    uint32 number = auth.ticket_num();

    for (auto it = inbound.begin(); it != inbound.end(); )
    {
        if (it->id.ConvertToUint64() == msg.source_id() && it->number == number)
        {
            pFrameResult_t result(new FrameResult);
            ValidateAuthTicketResponse_t& vatr = result->CreateCallback<ValidateAuthTicketResponse_t>();
            vatr.m_SteamID = it->id;
            vatr.m_OwnerSteamID = it->id;
            vatr.m_eAuthSessionResponse = k_EAuthSessionResponseAuthTicketCanceled;
            
            result->done = true;
            _cb_manager->add_callback(nullptr, result);

            it = inbound.erase(it);
        }
        else
        {
            ++it;
        }
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//                                 IRunCallback                              //
///////////////////////////////////////////////////////////////////////////////
bool Auth_Ticket_Manager::RunNetwork(Network_Message_pb const& msg)
{
    Steam_Auth_pb const& auth_msg = msg.auth();
    switch (auth_msg.messages_case())
    {
        case Steam_Auth_pb::MessagesCase::kCancel: return on_ticket_cancel(msg, auth_msg.cancel());
        default: APP_LOG(Log::LogLevel::WARN, "Message unhandled %d", auth_msg.messages_case());
    }

    return false;
}
