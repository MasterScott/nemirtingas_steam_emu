/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"

 /* This class was taken from goldberg emulator */

  //Conan Exiles doesn't work with 512 or 128, 256 seems to be the good size
  //Steam returns 234
#define STEAM_AUTH_TICKET_SIZE 234
#define INITIATE_GAME_CONNECTION_TICKET_SIZE 206

struct Auth_Ticket_Data {
    CSteamID id;
    uint32 number;
    std::chrono::high_resolution_clock::time_point created;
};

class LOCAL_API Auth_Ticket_Manager:
    public IRunNetwork
{
    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;

    std::vector<struct Auth_Ticket_Data> inbound, outbound;
public:
    std::recursive_mutex _local_mutex;

    Auth_Ticket_Manager();
    virtual ~Auth_Ticket_Manager();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    virtual bool RunNetwork(Network_Message_pb const& msg);

    bool send_auth_ticket_cancel(Auth_Cancel_pb* auth);

    bool on_ticket_cancel(Network_Message_pb const& msg, Auth_Cancel_pb const& auth);

    Auth_Ticket_Data get_ticket_data(void* pTicket, int cbMaxTicket, uint32* pcbTicket);
    uint32 get_ticket(void* pTicket, int cbMaxTicket, uint32* pcbTicket);
    void cancel_ticket(uint32 number);
    EBeginAuthSessionResult begin_auth(const void* pAuthTicket, int cbAuthTicket, CSteamID steamID);
    bool end_auth(CSteamID id);
    uint32 count_inbound_auths();
    bool SendUserConnectAndAuthenticate(uint32 unIPClient, const void* pvAuthBlob, uint32 cubAuthBlobSize, CSteamID* pSteamIDUser);
    CSteamID fake_user();
};