/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "callback_manager.h"
#include "network.h"
#include "steam_client.h"

constexpr static std::chrono::seconds cleanup_timeout(60);

Callback_Manager::Callback_Manager()
{
    TRACE_FUNC();
}

Callback_Manager::~Callback_Manager()
{
    TRACE_FUNC();
}

void Callback_Manager::register_frame(IRunCallback* obj)
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    // We need a callback pointer
    assert(obj != nullptr);

    _frames_to_run.emplace_back(obj);
}

void Callback_Manager::unregister_frame(IRunCallback* obj)
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    // We need a callback pointer
    assert(obj != nullptr);

    auto it = std::find(_frames_to_run.begin(), _frames_to_run.end(), obj);
    if (it != _frames_to_run.end())
        _frames_to_run.erase(it);
}

void Callback_Manager::register_callbacks(IRunCallback* obj)
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    // We need a callback pointer
    assert(obj != nullptr);

    _callbacks_to_run[obj];
}

void Callback_Manager::unregister_callbacks(IRunCallback* obj)
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    // We need a callback pointer
    assert(obj != nullptr);

    auto it = _callbacks_to_run.find(obj);
    if (it != _callbacks_to_run.end())
    {
        _callbacks_to_run.erase(it);
    }
}

bool Callback_Manager::add_callback(IRunCallback* obj, pFrameResult_t res)
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    // We need a callback pointer, or we accept no callback pointer if the callback is ready
    assert(obj != nullptr || (obj == nullptr && res->done == true));

    res->type = FrameResult::callback;

    _callbacks_to_run[obj].emplace_back(std::move(res));

    return true;
}

bool Callback_Manager::add_apicall(IRunCallback* obj, pFrameResult_t res)
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    // We need a callback pointer, or we accept no callback pointer if the callback is ready
    assert(obj != nullptr || (obj == nullptr && res->done == true));

    res->type = FrameResult::apicall;

    _callbacks_to_run[obj].emplace_back(std::move(res));

    return true;
}

bool Callback_Manager::add_next_callback(IRunCallback* obj, pFrameResult_t res)
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    // We need a callback pointer, or we accept no callback pointer if the callback is ready
    assert(obj != nullptr || (obj == nullptr && res->done == true));

    res->type = FrameResult::callback;

    //_frames_to_run[pipe][obj].emplace_back(std::move(res));
    _next_callbacks_to_run[obj].emplace_back(std::move(res));

    return true;
}

bool Callback_Manager::add_next_apicall(IRunCallback* obj, pFrameResult_t res)
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    // We need a callback pointer, or we accept no callback pointer if the callback is ready
    assert(obj != nullptr || (obj == nullptr && res->done == true));

    res->type = FrameResult::apicall;

    //_frames_to_run[pipe][obj].emplace_back(std::move(res));
    _next_callbacks_to_run[obj].emplace_back(std::move(res));

    return true;
}

bool Callback_Manager::GetCallback(CallbackMsg_t* pCallbackMsg)
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    run_frames(); // Run all registered frames
    run_callbacks(); // Run callbacks if any

    if (!_callbacks.empty())
    {
        auto& pipe_last_callback = _last_callback;

        if (pCallbackMsg != nullptr)
        {
            pipe_last_callback = _callbacks.front();
            _callbacks.pop_front();
            *pCallbackMsg = pipe_last_callback->GetCallbackMsg();
        }

        return true;
    }
    return false;
}

void Callback_Manager::FreeLastCallback()
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _last_callback.reset();
}

pFrameResult_t Callback_Manager::GetAPICall(SteamAPICall_t hSteamAPICall)
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = std::find_if(_apicalls.begin(), _apicalls.end(), [hSteamAPICall](pFrameResult_t& res)
    {
        return (res->GetAPICall() == hSteamAPICall);
    });

    if (it == _apicalls.end())
        return pFrameResult_t();

    return *it;
}

ESteamAPICallFailure Callback_Manager::GetAPICallFailureReason(SteamAPICall_t hSteamAPICall)
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = std::find_if(_apicalls.begin(), _apicalls.end(), [hSteamAPICall](pFrameResult_t& res)
    {
        return (res->GetAPICall() == hSteamAPICall);
    });

    if (it == _apicalls.end())
        return k_ESteamAPICallFailureInvalidHandle;

    return (*it)->GetApiFailure();
}

bool Callback_Manager::IsAPICallCompleted(SteamAPICall_t hSteamAPICall, bool* pbFailed)
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    run_frames(); // Run all registered frames
    run_callbacks(); // Run callbacks if any

    auto it = std::find_if(_apicalls.begin(), _apicalls.end(), [hSteamAPICall](pFrameResult_t& res)
    {
        return (res->GetAPICall() == hSteamAPICall);
    });

    if (it == _apicalls.end())
    {
        if (hSteamAPICall == 1)
        {// Some games call IsAPICallCompleted with an apicall of 1.
            if (pbFailed != nullptr)
                *pbFailed = true;

            return true;
        }
        return false;
    }

    //APP_LOG(Log::LogLevel::DEBUG, "%llu: %s", hSteamAPICall, get_callback_name((*it)->res.m_iCallback).c_str());

    if (pbFailed != nullptr)
        *pbFailed = ((*it)->GetApiFailure() != k_ESteamAPICallFailureNone);

    return (*it)->done;
}

bool Callback_Manager::GetAPICallResult(SteamAPICall_t hSteamAPICall, void* pCallback, int cubCallback, int iCallbackExpected, bool* pbFailed)
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = std::find_if(_apicalls.begin(), _apicalls.end(), [hSteamAPICall](pFrameResult_t& res)
    {
        return (res->GetAPICall() == hSteamAPICall);
    });

    if (it == _apicalls.end())
    {
        if (pCallback != nullptr)
            memset(pCallback, 0, cubCallback);
        return false;
    }

    if ((*it)->ICallback() != iCallbackExpected)
    {
        //APP_LOG(Log::LogLevel::WARN, "Callback expected %d and we have %d", iCallbackExpected, (*it)->res.m_iCallback);
        if (pCallback != nullptr)
            memset(pCallback, 0, cubCallback);
        return false;
    }

    if (pCallback != nullptr)
        memcpy(pCallback, (*it)->PCallback(), std::min(cubCallback, (*it)->CallbackSize()));

    if (pbFailed != nullptr)
        *pbFailed = ((*it)->GetApiFailure() != k_ESteamAPICallFailureNone);

    bool done = (*it)->done;

    if (done)
        _apicalls.erase(it);

    return done;
}

void Callback_Manager::clean_callback_results()
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    // I don't care about creating the map if it doesn't exists, it will exists anyway, we will receive callbacks someday

    auto now = std::chrono::steady_clock::now();

    _callbacks.erase(std::remove_if(_callbacks.begin(), _callbacks.end(), [&now](pFrameResult_t& res)
    {
        bool remove = (res->remove_on_timeout && ((now - res->created_time) > cleanup_timeout));
        if (remove)
            APP_LOG(Log::LogLevel::DEBUG, "!!! Deleting callback \"%s\" result: timeout !!!", get_callback_name(res->ICallback()).c_str());
        return remove;
    }), _callbacks.end());

    _apicalls.erase(std::remove_if(_apicalls.begin(), _apicalls.end(), [&now](pFrameResult_t& res)
    {
        bool remove = (res->remove_on_timeout && ((now - res->created_time) > cleanup_timeout));
        if (remove)
            APP_LOG(Log::LogLevel::DEBUG, "!!! Deleting callapi \"%s\" result: timeout !!!", get_callback_name(res->ICallback()).c_str());
        return remove;
    }), _apicalls.end());
}

void Callback_Manager::run_frames()
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    // This will usually call broadcast or check for lobby/friends timeout
    for (auto& item : _frames_to_run)
        item->CBRunFrame();
}

void Callback_Manager::run_callbacks()
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    clean_callback_results();

    for (auto frame = _callbacks_to_run.begin(); frame != _callbacks_to_run.end(); ++frame)
    {
        IRunCallback& irun_frame = *frame->first;
        std::list<pFrameResult_t>& frame_results = frame->second;
        for (auto _result = frame_results.begin(); _result != frame_results.end(); )
        {
            pFrameResult_t& result = *_result;
            // Run Callback
            if (result->CallbackOKTimeout())
            {
                if (result->done || 
                    // If RunCallbacks returned True, then a result is ready
                    irun_frame.RunCallbacks(result))
                {
                    std::string cb_name = std::move(get_callback_name(result->ICallback()));
                    if (result->type == FrameResult::apicall)
                    {
                        //APP_LOG(Log::LogLevel::DEBUG, "Push apicall %s", cb_name.c_str());
                        _apicalls.push_back(result);

                        pFrameResult_t call_completed(new FrameResult);
                        SteamAPICallCompleted_t& sacc = call_completed->CreateCallback<SteamAPICallCompleted_t>(std::chrono::milliseconds(0));

                        sacc.m_hAsyncCall = result->GetAPICall();
                        sacc.m_iCallback = result->ICallback();
                        sacc.m_cubParam = result->CallbackSize();

                        call_completed->done = true;

                        _callbacks.push_back(call_completed);

                        // Build a copy of the current apicall to create a callback
                        // steam_api cleans the callback memory after Steam_BGetCallback.
                        // As the apicall & callback shared the same memory, it was freeing both calls.
                        // With this copy, callback memory is detached from the apicall and everything is fine.
                        // Do the copy here, so the user only have 1 result to build
                        pFrameResult_t cbResult(new FrameResult(*result));
                        _callbacks.push_back(cbResult);
                    }
                    else
                    {
                        //APP_LOG(Log::LogLevel::DEBUG, "Push callback %s", cb_name.c_str());
                        _callbacks.push_back(result);
                    }

                    _result = frame_results.erase(_result);
                }
                else
                {
                    ++_result;
                }
            }
            else // If the callback is not ready, check another one
            {
                ++_result;
            }
        }
    }

    // Add the delayed callbacks to the runnable callbacks
    for (auto it = _next_callbacks_to_run.begin(); it != _next_callbacks_to_run.end(); ++it)
    {
        std::move(it->second.begin(), it->second.end(), std::back_inserter(_callbacks_to_run[it->first]));
        it->second.clear();
    }
    _next_callbacks_to_run.clear();
}