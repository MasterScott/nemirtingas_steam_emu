/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "frame_result.h"

class IRunCallback
{
public:
    // RunFrame is always called when the BGetCallback is called
    virtual bool CBRunFrame() = 0;
    // RunCallbacks is run when you sent a callback
    // True  = FrameResult_t has been filled with a result
    // False = FrameResult_t is not changed
    virtual bool RunCallbacks(pFrameResult_t res) = 0;
};

class Callback_Manager
{
    std::vector<IRunCallback*> _frames_to_run;

    std::map<IRunCallback*, std::list<pFrameResult_t>> _callbacks_to_run;
    std::map<IRunCallback*, std::list<pFrameResult_t>> _next_callbacks_to_run;
    
    std::list<pFrameResult_t> _callbacks;
    pFrameResult_t            _last_callback;

    std::list<pFrameResult_t> _apicalls;

    void clean_callback_results();
    void run_frames();
    void run_callbacks();

public:
    std::recursive_mutex _local_mutex;

    Callback_Manager();
    ~Callback_Manager();

    void register_frame  (IRunCallback* obj);
    void unregister_frame(IRunCallback* obj);

    void register_callbacks  (IRunCallback* obj);
    void unregister_callbacks(IRunCallback* obj);

    bool add_callback(IRunCallback* obj, pFrameResult_t res);
    bool add_apicall (IRunCallback* obj, pFrameResult_t res);

    bool add_next_callback(IRunCallback* obj, pFrameResult_t res);
    bool add_next_apicall (IRunCallback* obj, pFrameResult_t res);

    bool GetCallback(CallbackMsg_t* pCallbackMsg);
    void FreeLastCallback();

    pFrameResult_t GetAPICall(SteamAPICall_t hSteamAPICall);

    ESteamAPICallFailure GetAPICallFailureReason(SteamAPICall_t hSteamAPICall);

    bool IsAPICallCompleted(SteamAPICall_t hSteamAPICall, bool* pbFailed);

    bool GetAPICallResult(SteamAPICall_t hSteamAPICall, void* pCallback, int cubCallback, int iCallbackExpected, bool* pbFailed);
};