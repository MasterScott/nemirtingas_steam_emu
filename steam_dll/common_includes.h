/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <utils.h>
#include <network_proto.pb.h>

#if defined(__WINDOWS__)
    #define WIN32_LEAN_AND_MEAN
    #define VC_EXTRALEAN
    #define NOMINMAX
    #include <Windows.h>
    #include <shlobj.h>   // (shell32.lib) Infos about current user folders
    #include <WinSock2.h> // Include before iphlpapi to enable winsock2 functions
    #include <iphlpapi.h> // (iphlpapi.lib) Infos about ethernet interfaces

#elif defined(__LINUX__) || defined(__APPLE__)
    #if defined(__LINUX__)
        #include <sys/sysinfo.h> // Get uptime (second resolution)
    #else
        #include <sys/sysctl.h>
        #include <mach-o/dyld_images.h>
    #endif

    #include <sys/types.h>
    #include <sys/ioctl.h> // get iface broadcast
    #include <sys/stat.h>  // stats on a file (is directory, size, mtime)

    #include <dirent.h> // to open directories
    #include <dlfcn.h>  // dlopen (like dll for linux)
    #include <net/if.h>
    #include <ifaddrs.h>// getifaddrs

    #include <limits.h> // PATH_MAX
    #include <unistd.h>

#else
    #error "unknown arch"
#endif

#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

#ifdef STEAMCLIENT_SHARED_LIBRARY
    #ifdef STEAMCLIENT_EXPORT
        #define EXPORT_STEAMCLIENT_API EXPORT_API(dllexport)
    #else
        #define EXPORT_STEAMCLIENT_API EXPORT_API(dllimport)
    #endif
#else
    #define LOCAL_API
#endif

#include <thread>
#include <mutex>
#include <condition_variable>
#include <limits>
#include <chrono>
#include <locale>
#include <codecvt>
#include <random>
#include <string>
#include <vector>
#include <list>
#include <queue>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <utility>
#include <type_traits>
#include <cstdarg>

#include <steam_sdk/steam_api.h>
#include <steam_sdk/steam_gameserver.h>
#include <steam_sdk/steamdatagram_tickets.h>
#include <nlohmann/json.hpp>
#include <nlohmann/fifo_map.hpp>
#include <utfcpp/utf8.h>

// Workaround to use fifo_map in json
// A workaround to use fifo_map as map, we are just ignoring the 'less' compare
template<class K, class V, class dummy_compare, class A>
using my_fifo_map = nlohmann::fifo_map<K, V, nlohmann::fifo_map_compare<K>, A>;
using fifo_json = nlohmann::basic_json<my_fifo_map>;

#include <ipv4/tcp_socket.h>
#include <ipv4/udp_socket.h>
#include <ipv6/ipv6_addr.h>
#include <mini_detour/mini_detour.h>

#include "library.h"
#include <image_manager.h>
#include <audio_manager.h>
#include <file_manager.h>

#include "os_funcs.h"
#include "Log.h"
#include "helper_funcs.h"

static constexpr HSteamPipe NULL_HSTEAMPIPE = 0;

static constexpr HSteamUser NULL_HSTEAMUSER = 0;
static constexpr HSteamUser CLIENT_HSTEAMUSER = 1;
static constexpr HSteamUser SERVER_HSTEAMUSER = 1;

static constexpr auto INITIATE_GAME_CONNECTION_TICKET_SIZE = 206;

static constexpr char emu_savepath[] = "NemirtingasSteamEmu";

static constexpr char user_avatar_id[] = "user_avatar";
static constexpr char default_avatar_id[] = "default_avatar";
static constexpr char notification_audio_id[] = "audio_notification";

#if defined(__WINDOWS_32__)
  #define _EMU_VARIANT_ "win32"
#elif defined(__WINDOWS_64__)
  #define _EMU_VARIANT_ "win64"
#elif defined(__LINUX_32__)
  #define _EMU_VARIANT_ "lin32"
  #elif defined(__LINUX_64__)
  #define _EMU_VARIANT_ "lin64"
#elif defined(__APPLE_32__)
  #define _EMU_VARIANT_ "mac32"
  #elif defined(__APPLE_64__)
  #define _EMU_VARIANT_ "mac64"
#else
  #define _EMU_VARIANT_ "unk"
#endif
constexpr char _EMU_VERSION_[] = "0.0.0" "-" _EMU_VARIANT_;

class Steam_Client;