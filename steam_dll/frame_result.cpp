/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "frame_result.h"

FrameResult::FrameResult():
    res({ 0 }),
    created_time(std::chrono::steady_clock::now()),
    done(false),
    apicall_res(ESteamAPICallFailure::k_ESteamAPICallFailureNone),
    remove_on_timeout(true)
{
    static SteamAPICall_t call_id = k_uAPICallInvalid;

    if (call_id == k_uAPICallInvalid)
        ++call_id;

    apicall_id = ++call_id;
}

FrameResult::FrameResult(FrameResult const& other):
    res({ 0 }),
    created_time(other.created_time),
    done(other.done),
    apicall_res(other.apicall_res),
    remove_on_timeout(other.remove_on_timeout),
    apicall_id(other.apicall_id),
    ok_timeout(other.ok_timeout),
    type(other.type)
{
    res.m_hSteamUser = other.res.m_hSteamUser;
    res.m_cubParam = other.res.m_cubParam;
    res.m_iCallback = other.res.m_iCallback;

    res.m_pubParam = new uint8[other.res.m_cubParam];
    memcpy(res.m_pubParam, other.res.m_pubParam, other.res.m_cubParam);
}

FrameResult::~FrameResult()
{
    delete[]res.m_pubParam;
}

void* FrameResult::AllocCallback(size_t size, int i_callback, std::chrono::milliseconds ok_timeout)
{
    uint8* buff = new uint8[size];
    SetCallback(buff, size, i_callback, ok_timeout);
    return buff;
}

void FrameResult::SetCallback(uint8_t* callback, size_t size, int i_callback, std::chrono::milliseconds ok_timeout)
{
    this->ok_timeout = ok_timeout;
    delete[]res.m_pubParam;
    res.m_iCallback = i_callback;
    res.m_cubParam = size;
    res.m_pubParam = callback;
}