/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"

class FrameResult
{
    std::chrono::milliseconds ok_timeout;
    CallbackMsg_t res;
    ESteamAPICallFailure apicall_res;
    SteamAPICall_t apicall_id;

public:
    const std::chrono::time_point<std::chrono::steady_clock> created_time;
    bool remove_on_timeout; // Remove the result if steam_api didn't read in too fast

    enum : uint8_t
    {
        apicall,
        callback,
    } type;

    bool done;    // Set this to true will tell the callback_manager to fire the callback/apicall

    FrameResult();
    FrameResult(FrameResult const& other);
    ~FrameResult();

    void* AllocCallback(size_t size, int i_callback, std::chrono::milliseconds ok_timeout = std::chrono::milliseconds(100));
    void SetCallback(uint8_t* callback, size_t size, int i_callback, std::chrono::milliseconds ok_timeout = std::chrono::milliseconds(100));

    inline CallbackMsg_t const& GetCallbackMsg() const { return res; }
    inline int ICallback() const { return res.m_iCallback; }
    inline void* PCallback() const { return res.m_pubParam; }
    inline int CallbackSize() const { return res.m_cubParam; }

    inline bool CallbackOKTimeout() { return ((std::chrono::steady_clock::now() - created_time) >= ok_timeout); }
    inline SteamAPICall_t GetAPICall() const { return apicall_id; }
    inline ESteamAPICallFailure GetApiFailure() const { return apicall_res; }
    inline void SetApiFailure(ESteamAPICallFailure error) { apicall_res = error; }

    template<typename T>
    inline void SetCallback(T* cb, std::chrono::milliseconds ok_timeout = std::chrono::milliseconds(100))
    {
        SetCallback(cb, sizeof(T), T::k_iCallback, ok_timeout);
    }

    template<typename T>
    inline T& GetCallback() 
    {
        assert(res.m_iCallback == T::k_iCallback);
        return *reinterpret_cast<T*>(res.m_pubParam);
    }

    template<typename T>
    inline T& CreateCallback(std::chrono::milliseconds ok_timeout = std::chrono::milliseconds(100))
    {
        void* buff = AllocCallback(sizeof(T), T::k_iCallback, ok_timeout);
        T* cb = new (buff) T;
        return *cb;
    }
};

using pFrameResult_t = std::shared_ptr<FrameResult>;