/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"

LOCAL_API std::random_device& get_rd();
LOCAL_API std::mt19937_64& get_gen();

LOCAL_API uint32 generate_account_id();
LOCAL_API uint32 generate_account_id_from_name(std::string const& username);
LOCAL_API CSteamID generate_steam_id_user();
LOCAL_API CSteamID generate_steam_id_user_from_name(std::string const& username);
LOCAL_API CSteamID generate_steam_anon_user();
LOCAL_API CSteamID generate_steam_id_server();
LOCAL_API CSteamID generate_steam_id_anonserver();
LOCAL_API CSteamID generate_steam_id_lobby();
LOCAL_API uint32 generate_steam_ticket_id();

LOCAL_API void fatal_throw(const char* msg);

LOCAL_API std::string get_callback_name(int iCallback);
