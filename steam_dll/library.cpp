/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "library.h"

#if defined(WIN64) || defined(_WIN64) || defined(__MINGW64__)
    #define __WINDOWS_64__
#elif defined(WIN32) || defined(_WIN32) || defined(__MINGW32__)
    #define __WINDOWS_32__
#endif

#if defined(__WINDOWS_32__) || defined(__WINDOWS_64__)
    #define __WINDOWS__
#endif

#if defined(__linux__) || defined(linux)
    #if defined(__x86_64__)
        #define __LINUX_64__
    #else
        #define __LINUX_32__
    #endif
#endif

#if defined(__LINUX_32__) || defined(__LINUX_64__)
    #define __LINUX__
#endif

#if defined(__APPLE__)
    #if defined(__x86_64__)
        #define __APPLE_64__
    #else
        #define __APPLE_32__
    #endif
#endif

#if defined(__WINDOWS__)
    #define WIN32_LEAN_AND_MEAN
    #define VC_EXTRALEAN
    #define NOMINMAX
    #include <Windows.h>

    constexpr char library_suffix[] = ".dll";

    inline void* open_library(std::string const& lib_name)
    {
        return LoadLibraryA(lib_name.c_str());
    }

    inline void close_library(void* handle)
    {
        FreeLibrary((HMODULE)handle);
    }

    inline void* get_symbol(void* handle, std::string const& symbol_name)
    {
        return GetProcAddress((HMODULE)handle, symbol_name.c_str());
    }

#elif defined(__LINUX__) || defined(__APPLE__)
    #include <dlfcn.h>
    #if defined(__LINUX__)
        constexpr char library_suffix[] = ".so";
    #else
        constexpr char library_suffix[] = ".dylib";
    #endif

    inline void* open_library(std::string const& lib_name)
    {
        return dlopen(lib_name.c_str(), RTLD_NOW);
    }

    inline void close_library(void* handle)
    {
        dlclose(handle);
    }

    inline void* get_symbol(void* handle, std::string const& symbol_name)
    {
        return dlsym(handle, symbol_name.c_str());
    }

#else
    #error "Unknown OS"
#endif

Library::Library()
{}

Library::Library(Library const& other)
{
    _handle = other._handle;
}

Library::Library(Library&& other) noexcept
{
    _handle = std::move(other._handle);
}

Library& Library::operator=(Library const& other)
{
    if (this != &other)
    {
        _handle = other._handle;
    }
    return *this;
}

Library& Library::operator=(Library && other) noexcept
{
    if (this != &other)
    {
        _handle = std::move(other._handle);
    }
    return *this;
}

bool Library::load_library(std::string const& library_name, bool append_extension)
{
    std::string lib_name = (append_extension ? library_name + library_suffix : library_name);

    void* lib = open_library(lib_name.c_str());

    if (lib == nullptr)
    {
        lib_name = "lib" + lib_name;
        lib = open_library(lib_name.c_str());
        if (lib == nullptr)
        {
            return false;
        }
    }

    _handle.reset(lib, close_library);
    return true;
}

void* Library::generic_get_func(std::string const& func_name)
{
    if (_handle == nullptr)
        return nullptr;
    
    return get_symbol(_handle.get(), func_name);
}