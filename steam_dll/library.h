/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <memory>
#include <functional>

class Library
{
    std::shared_ptr<void> _handle;

    void* generic_get_func(std::string const& func_name);

public:
    Library();
    Library(Library const&);
    Library(Library&&) noexcept;
    Library& operator=(Library const&);
    Library& operator=(Library&&) noexcept;

    bool load_library(std::string const& library_name, bool append_extension = true);

    template<typename T>
    inline std::function<T> get_func(std::string const& func_name)
    {
        return std::function<T>(reinterpret_cast<T*>(generic_get_func(func_name)));
    }

    inline bool is_loaded() const
    {
        return _handle != nullptr;
    }
};