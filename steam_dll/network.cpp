/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "network.h"

using namespace PortableAPI;

namespace std
{
    inline std::string to_string(std::string const& str)
    {
        return str;
    }
}

Network::Network():
    _advertise_rate(2000),
    _tcp_port(0)
{
    //APP_LOG(Log::LogLevel::DEBUG, "");
#if defined(NETWORK_COMPRESS)
    max_message_size = 0;
    max_compressed_message_size = 0;
    _zstd_ccontext = ZSTD_createCCtx();
    _zstd_dstream = ZSTD_createDStream();
#endif
}

Network::~Network()
{
#if defined(NETWORK_COMPRESS)
    APP_LOG(Log::LogLevel::DEBUG, "Shutting down Network, biggest message size was %llu, biggest compressed message size was %llu", max_message_size, max_compressed_message_size);
#else
    APP_LOG(Log::LogLevel::DEBUG, "Shutting down Network");
#endif

    _network_task.stop();
    _query_task.stop();
    _network_task.join();
    _query_task.join();

#if defined(NETWORK_COMPRESS)
    ZSTD_freeCCtx(_zstd_ccontext);
    ZSTD_freeDStream(_zstd_dstream);
#endif

    //APP_LOG(Log::LogLevel::DEBUG, "Network Thread Joined");
}

#if defined(NETWORK_COMPRESS)

std::string Network::compress(void const* data, size_t len)
{
    std::string res(ZSTD_compressBound(len), '\0');
    res.resize(ZSTD_compressCCtx(_zstd_ccontext, &res[0], res.length(), data, len, ZSTD_CLEVEL_DEFAULT));
    return res;
}

std::string Network::decompress(void const* data, size_t len)
{
    static size_t decompress_block_size = ZSTD_DStreamOutSize();
    static std::string res;

    res.resize(decompress_block_size);
    ZSTD_inBuffer inbuff{ data, len, 0 };
    ZSTD_outBuffer outbuff{ const_cast<char*>(res.data()), res.length(), 0 };

    while (inbuff.pos < inbuff.size)
    {
        size_t x = 0;
        x = ZSTD_decompressStream(_zstd_dstream, &outbuff, &inbuff);
        if (ZSTD_isError(x))
        {
            if (x == size_t(-70))
            {
                res.resize(res.length() + decompress_block_size);
                outbuff.size = res.length();
                outbuff.dst = const_cast<char*>(res.data());
            }
            else
            {
                auto str_error = ZSTD_getErrorName(x);
                APP_LOG(Log::LogLevel::WARN, "Decompression error: %s", str_error);
                return std::string((char*)data, ((char*)data) + len);
            }
        }
    }

    ZSTD_initDStream(_zstd_dstream);
    res.resize(outbuff.pos);
    return res;
}

#endif

bool Network::start_network()
{
    if (_network_task.is_running())
        return true;

    ipv4_addr addr;
    uint16_t port;
    addr.set_addr(ipv4_addr::any_addr);

    for (port = network_port; port < max_network_port; ++port)
    {
        addr.set_port(port);
        try
        {
            _udp_socket.bind(addr);
            break;
        }
        catch (...)
        {
        }
    }
    if (port == max_network_port)
    {
        //APP_LOG(Log::LogLevel::ERR, "Failed to start udp socket");
        _network_task.stop();
    }
    else
    {
        APP_LOG(Log::LogLevel::INFO, "UDP socket started on port: %hu", port);
        std::uniform_int_distribution<int64_t> dis;
        std::mt19937_64& gen = get_gen();
        int x;
        for (x = 0, port = (dis(gen) % 30000 + 30000); x < 100; ++x, port = (dis(gen) % 30000 + 30000))
        {
            addr.set_port(port);
            try
            {
                _tcp_socket.bind(addr);
                _tcp_socket.listen(32);
                addr.set_addr(ipv4_addr::loopback_addr);
                break;
            }
            catch (...)
            {
                APP_LOG(Log::LogLevel::WARN, "Failed to start tcp socket on port %hu", x);
            }
        }
        if (x == 100)
        {
            APP_LOG(Log::LogLevel::ERR, "Failed to start tcp socket");
            _udp_socket.close();
            _network_task.stop();
        }
        else
        {
            _tcp_port = port;
            APP_LOG(Log::LogLevel::INFO, "TCP socket started after %hu tries on port: %hu", x, port);

            _network_task = task();
            return _network_task.run(&Network::network_thread, this);
        }
    }

    return false;
}

void Network::stop_network()
{
    if (!_network_task.is_running())
        return;

    _network_task.stop();
    _network_task.join();

    _udp_socket.close();
    _tcp_socket.close();
    _network_msgs.clear();
    _peers.clear();
}

std::unordered_set<Network::peer_t> Network::get_all_peers()
{
    std::lock_guard<std::recursive_mutex> lk(local_mutex);
    std::unordered_set<peer_t> peers;

    for (auto& peer : _peers)
    {
        peers.emplace(peer.first);
    }

    return peers;
}

inline Network::next_packet_size_t Network::make_next_packet_size(std::string const& buff) const
{
    return utils::Endian::net_swap(next_packet_size_t(buff.length() - sizeof(next_packet_size_t)));
}

void Network::do_advertise()
{
    std::lock_guard<std::recursive_mutex> lk(local_mutex);

    auto now = std::chrono::steady_clock::now();
    if ((now - _last_advertise) < _advertise_rate)
        return;

    _last_advertise = now;
    
    try
    {
        if (_peer_id != peer_t())
        {
            Network_Message_pb msg;
            Network_Advertise_pb* network = new Network_Advertise_pb;
            Network_Port_pb* port = new Network_Port_pb;

            port->set_port(_tcp_port);
            network->set_allocated_port(port);
            msg.set_allocated_network_advertise(network);
            msg.set_source_id(_peer_id);

            SendBroadcast(msg);
        }
    }
    catch (...)
    {
        //APP_LOG(Log::LogLevel::DEBUG, "Advertising, failed");
    }
}


void Network::set_advertise_rate(std::chrono::milliseconds rate)
{
    std::lock_guard<std::recursive_mutex> lk(local_mutex);
    _advertise_rate = rate;
}

std::chrono::milliseconds Network::get_advertise_rate()
{
    std::lock_guard<std::recursive_mutex> lk(local_mutex);
    return _advertise_rate;
}

void Network::add_new_tcp_client(peer_t const& peer_id, peer_state_t& peer_state, bool send_accept)
{
    std::lock_guard<std::recursive_mutex> lk(local_mutex);
    if (_peers.count(peer_id) != 0)
        return;

    auto& new_peer = _peers[peer_id];
    new_peer = std::move(peer_state);
    _poll.add_socket(new_peer.tcp_socket); // Add the client to the poll
    _poll.set_events(new_peer.tcp_socket, Socket::poll_flags::in);

    Network_Message_pb msg;
    Network_Advertise_pb adv;
    Network_Peer_Connect_pb conn;
    
    adv.set_allocated_peer_connect(&conn);
    msg.set_allocated_network_advertise(&adv);
    
    {
        std::lock_guard<std::mutex> lk1(message_mutex);
        APP_LOG(Log::LogLevel::DEBUG, "Adding peer id %s to client %s", std::to_string(peer_id).c_str(), new_peer.tcp_socket.get_addr().to_string(true).c_str());
    
        msg.set_source_id(peer_id);
    
        for (auto& listener : _network_listeners[Network_Message_pb::MessagesCase::kNetworkAdvertise])
        {
            _pending_network_msgs.emplace_back(msg);
        }
    }
    
    adv.release_peer_connect();
    msg.release_network_advertise();
    
    if(send_accept)
    {
        Network_Message_pb msg;
        Network_Advertise_pb* adv = new Network_Advertise_pb;
        Network_Peer_Accept_pb* accept_peer = new Network_Peer_Accept_pb;
    
        adv->set_allocated_accept(accept_peer);
        msg.set_allocated_network_advertise(adv);
    
        msg.set_source_id(_peer_id);
        msg.set_dest_id(peer_id);

        std::string buff(sizeof(next_packet_size_t), 0);
        buff += std::move(msg.SerializeAsString());
    
        *reinterpret_cast<next_packet_size_t*>(&buff[0]) = make_next_packet_size(buff);
    
        new_peer.tcp_socket.send(buff.data(), buff.length());
    }
}

void Network::remove_tcp_peer(peer_t const& peer_id)
{
    std::lock_guard<std::recursive_mutex> lk(local_mutex);

    APP_LOG(Log::LogLevel::DEBUG, "TCP Client %s gone", std::to_string(peer_id).c_str());
    _poll.remove_socket(_peers[peer_id].tcp_socket);

    Network_Message_pb msg;
    Network_Advertise_pb adv;
    Network_Peer_Disconnect_pb disc;
    
    adv.set_allocated_peer_disconnect(&disc);
    msg.set_allocated_network_advertise(&adv);
    
    {
        std::lock_guard<std::mutex> lk1(message_mutex);
        msg.set_source_id(peer_id);
    
        for (auto& listener : _network_listeners[Network_Message_pb::MessagesCase::kNetworkAdvertise])
        {
            _pending_network_msgs.emplace_back(msg);
        }
    }
    
    adv.release_peer_disconnect();
    msg.release_network_advertise();
}

void Network::connect_to_peer(ipv4_addr &udp_addr, ipv4_addr &tcp_addr, peer_t const& peer_id)
{
    if (_waiting_out_tcp_clients.count(peer_id) != 0)
        return;

    bool connected = false;
    auto it = _waiting_connect_tcp_clients.find(peer_id);
    try
    {
        if (it == _waiting_connect_tcp_clients.end())
        {
            APP_LOG(Log::LogLevel::DEBUG, "Connecting to %s : %s", tcp_addr.to_string(true).c_str(), std::to_string(peer_id).c_str());
            
            _waiting_connect_tcp_clients.emplace(peer_id, peer_state_t{});
            it = _waiting_connect_tcp_clients.find(peer_id);
            it->second.tcp_socket.set_nonblocking(true);
            it->second.udp_addr = udp_addr;
        }
        it->second.tcp_socket.connect(tcp_addr);
        connected = true;
    }
    catch (is_connected &e)
    {
        connected = true;
    }
    catch (would_block &e)
    {}
    catch(in_progress &e)
    {}
    catch (std::exception &e)
    {
        _waiting_connect_tcp_clients.erase(it);
        APP_LOG(Log::LogLevel::WARN, "Failed to TCP connect to %s: %s", tcp_addr.to_string().c_str(), e.what());
    }

    if (connected)
    {
        Network_Message_pb msg;
        Network_Advertise_pb adv;
        Network_Peer_Request_pb req;

        std::string buff(sizeof(next_packet_size_t), 0);
        
        adv.set_allocated_req(&req);
        msg.set_allocated_network_advertise(&adv);
        msg.set_source_id(_peer_id);
        msg.set_dest_id(peer_id);

        buff += std::move(msg.SerializeAsString());
        *reinterpret_cast<next_packet_size_t*>(&buff[0]) = make_next_packet_size(buff);

        adv.release_req();
        msg.release_network_advertise();

        it->second.tcp_socket.send(buff.data(), buff.length());

        APP_LOG(Log::LogLevel::DEBUG, "Connected to %s : %s", it->second.tcp_socket.get_addr().to_string(true).c_str(), std::to_string(peer_id).c_str());

        _waiting_out_tcp_clients.emplace(peer_id, std::move(it->second));
        _waiting_connect_tcp_clients.erase(it);
    }
}

void Network::process_waiting_out_clients()
{
    if (_waiting_out_tcp_clients.empty())
        return;

    Network_Message_pb msg;
    for (auto it = _waiting_out_tcp_clients.begin(); it != _waiting_out_tcp_clients.end(); )
    {
        try
        {
            unsigned long count = 0;
            it->second.tcp_socket.ioctlsocket(Socket::cmd_name::fionread, &count);
            if (count > 0)
            {
                if (it->second.next_packet_size == 0 && count > sizeof(next_packet_size_t))
                {
                    it->second.tcp_socket.recv(&it->second.next_packet_size, sizeof(next_packet_size_t));
                    it->second.next_packet_size = utils::Endian::net_swap(it->second.next_packet_size);
                    count -= sizeof(next_packet_size_t);
                }
                if (it->second.next_packet_size > 0 && count >= it->second.next_packet_size)
                {
                    it->second.buffer.resize(it->second.next_packet_size);
                    it->second.tcp_socket.recv(it->second.buffer.data(), it->second.next_packet_size);

                    const void* message;
                    int message_size;

                    message = it->second.buffer.data();
                    message_size = it->second.buffer.size();
                    
                    if (msg.ParseFromArray(message, message_size) &&
                        msg.has_network_advertise() && 
                        msg.network_advertise().has_accept())
                    {
                        std::lock_guard<std::recursive_mutex> lk(local_mutex);

                        it->second.next_packet_size = 0;
                        it->second.buffer.clear();
                        it->second.tcp_socket.set_nonblocking(false);

                        if (_peers.count(msg.source_id()) == 0 && msg.dest_id() == _peer_id)
                        {
                            add_new_tcp_client(it->first, it->second, false);
                        }
                    }
                    it = _waiting_out_tcp_clients.erase(it);
                    continue;
                }
            }
            
            ++it;
        }
        catch (std::exception &e)
        {
            // Error while reading, connection closed ?
            APP_LOG(Log::LogLevel::WARN, "Failed peer pair: %s", e.what());
            it = _waiting_out_tcp_clients.erase(it);
        }
    }
}

void Network::process_waiting_in_client()
{
    Network_Message_pb msg;
    for (auto it = _waiting_in_tcp_clients.begin(); it != _waiting_in_tcp_clients.end(); )
    {
        try
        {
            unsigned long count = 0;
            it->tcp_socket.ioctlsocket(Socket::cmd_name::fionread, &count);
            if (count > 0)
            {
                if (it->next_packet_size == 0 && count > sizeof(next_packet_size_t))
                {
                    it->tcp_socket.recv(&it->next_packet_size, sizeof(next_packet_size_t));
                    it->next_packet_size = utils::Endian::net_swap(it->next_packet_size);
                    count -= sizeof(next_packet_size_t);
                }
                if (it->next_packet_size > 0 && count >= it->next_packet_size)
                {
                    it->buffer.resize(it->next_packet_size);
                    it->tcp_socket.recv(it->buffer.data(), it->next_packet_size);

                    const void* message = it->buffer.data();
                    int message_size = it->buffer.size();
                    
                    if (msg.ParseFromArray(message, message_size) &&
                        msg.has_network_advertise() && 
                        msg.network_advertise().has_req())
                    {
                        std::lock_guard<std::recursive_mutex> lk(local_mutex);

                        it->next_packet_size = 0;
                        it->buffer.clear();
                        it->tcp_socket.set_nonblocking(false);

                        auto const& peer_msg = msg.network_advertise().req();

                        if (_peers.count(msg.source_id()) == 0 && msg.dest_id() == _peer_id)
                        {
                            add_new_tcp_client(msg.source_id(), *it, true);
                        }
                    }
                    it = _waiting_in_tcp_clients.erase(it);
                    continue;
                }
            }
            
            ++it;
        }
        catch (std::exception &e)
        {
            // Error while reading, connection closed ?
            APP_LOG(Log::LogLevel::WARN, "Failed peer pair: %s", e.what());
            it = _waiting_in_tcp_clients.erase(it);
        }
    }
}

void Network::process_network_message(Network_Message_pb &msg)
{
    std::lock_guard<std::mutex> lk(message_mutex);

    std::chrono::system_clock::time_point msg_time(std::chrono::milliseconds(msg.timestamp()));
    
    //if ((std::chrono::system_clock::now() - msg_time) > std::chrono::milliseconds(1500))
    //{
    //    APP_LOG(Log::LogLevel::WARN, "Message dropped because it was too old");
    //    return;
    //}

    _pending_network_msgs.emplace_back(msg);
}

void Network::process_udp()
{
    try
    {
        ipv4_addr addr;
        std::array<uint8_t, 4096> buffer;
        Network_Message_pb msg;
        size_t len;
        
        len = _udp_socket.recvfrom(addr, buffer.data(), buffer.size());
        if (len > 0)
        {
            const void* message;
            int message_size;

            #if defined(NETWORK_COMPRESS)
                std::string buff(std::move(decompress(buffer.data(), len)));
                message = buff.data();
                message_size = buff.length();
            #else
                message = buffer.data();
                message_size = len;
            #endif

            if (msg.ParseFromArray(message, message_size))
            {
                if (msg.source_id() != peer_t())
                {
                    std::lock_guard<std::recursive_mutex> lk(local_mutex);

                    //APP_LOG(Log::LogLevel::TRACE, "Received UDP message from: %s - %s", addr.to_string().c_str(), msg.source_id().c_str());
                    if (msg.has_network_advertise())
                    {
                        if (_peer_id != msg.source_id() && _peers.count(msg.source_id()) == 0)
                        {
                            auto const& advertise = msg.network_advertise();
                            if (advertise.has_port())
                            {
                                PortableAPI::ipv4_addr tcp_addr;
                                tcp_addr.set_addr(addr.get_addr());
                                tcp_addr.set_port(advertise.port().port());
                                connect_to_peer(addr, tcp_addr, msg.source_id());
                            }
                        }
                    }
                    else if (_peers.count(msg.source_id()) != 0)
                    {
                        //APP_LOG(Log::LogLevel::DEBUG, "Received UDP message from %s type %d", addr.to_string(true).c_str(), msg.messages_case());
                        process_network_message(msg);
                    }
                }
                else
                {
                    APP_LOG(Log::LogLevel::DEBUG, "Dropping UDP data: peer_id is null");
                }
            }
            else
            {
                APP_LOG(Log::LogLevel::DEBUG, "Dropping UDP data: failed to parse protobuf");
            }
        }
    }
    catch (socket_exception & e)
    {
        //APP_LOG(Log::LogLevel::WARN, "Udp socket exception: %s", e.what());
    }
}

void Network::process_tcp_listen()
{
    try
    {
        peer_state_t new_peer{};
        new_peer.tcp_socket = std::move(_tcp_socket.accept());
        new_peer.tcp_socket.set_nonblocking(true);
        _waiting_in_tcp_clients.emplace_back(std::move(new_peer));
    }
    catch (socket_exception & e)
    {
        APP_LOG(Log::LogLevel::WARN, "TCP Listen exception: %s", e.what());
    }
}

void Network::process_tcp_data(peer_state_t& peer_state)
{
    // Don't lock here, its already locked in network_thread when needed
    Network_Message_pb msg;
    size_t len;

    unsigned long count = 0;
    peer_state.tcp_socket.ioctlsocket(Socket::cmd_name::fionread, &count);
    if (count > 0)
    {
        size_t buff_len = peer_state.buffer.size();
        peer_state.buffer.resize(buff_len + count); // We grow to the current size + stream size

        len = peer_state.tcp_socket.recv(peer_state.buffer.data() + buff_len, count);

        while(peer_state.buffer.size() > 0)
        {
            if (peer_state.next_packet_size == 0 && peer_state.buffer.size() >= sizeof(next_packet_size_t))
            {
                peer_state.next_packet_size = *reinterpret_cast<next_packet_size_t*>(&peer_state.buffer[0]);
                peer_state.next_packet_size = utils::Endian::net_swap(peer_state.next_packet_size);
                peer_state.buffer.erase(peer_state.buffer.begin(), peer_state.buffer.begin() + sizeof(peer_state.next_packet_size));
            }

            if (peer_state.next_packet_size > 0 && peer_state.buffer.size() >= peer_state.next_packet_size)
            {
                const void* message;
                int message_size;
            #if defined(NETWORK_COMPRESS)
                std::string buff = std::move(decompress(peer_state.buffer.data(), peer_state.next_packet_size));
                message = buff.data();
                message_size = buff.length();
            #else
                message = peer_state.buffer.data();
                message_size = peer_state.next_packet_size;
            #endif

                if (msg.ParseFromArray(message, message_size))
                {
                    //APP_LOG(Log::LogLevel::DEBUG, "Received TCP message from %s type %d", tcp_buffer.socket.get_addr().to_string(true).c_str(), msg.messages_case());
                    process_network_message(msg);
                }
                peer_state.buffer.erase(peer_state.buffer.begin(), peer_state.buffer.begin() + peer_state.next_packet_size);
                peer_state.next_packet_size = 0;
            }
            else
            {
                break;
            }
        }
    }
}

void Network::network_thread()
{
    int broadcast = 1;

    _udp_socket.setsockopt(Socket::level::sol_socket, Socket::option_name::so_broadcast, &broadcast, sizeof(broadcast));
    //_udp_socket.set_nonblocking();

    if (!_network_task.want_stop())
    {
        _poll.add_socket(_udp_socket);
        _poll.add_socket(_tcp_socket);
        for(auto i = 0; i < _poll.get_num_polls(); ++i)
            _poll.set_events(i, Socket::poll_flags::in);
    }

    while (!_network_task.want_stop())
    {
        do_advertise();

        auto res = _poll.poll(500);
        if (res == 0)
            continue;

        if ((_poll.get_revents(_udp_socket) & Socket::poll_flags::in_hup) != Socket::poll_flags::none)
            process_udp(); // Process udp datas & advertising

        if ((_poll.get_revents(_tcp_socket) & Socket::poll_flags::in_hup) != Socket::poll_flags::none)
            process_tcp_listen(); // Process the waiting incoming peers
        
        {
            std::lock_guard<std::recursive_mutex> lk(local_mutex);
            for (auto it = _peers.begin(); it != _peers.end();)
            {// Process the multiple tcp clients we have
                auto reevents = _poll.get_revents(it->second.tcp_socket);
                if ((reevents & Socket::poll_flags::hup) != Socket::poll_flags::none)
                {
                    remove_tcp_peer(it->first);
                    it = _peers.erase(it);
                }
                else if ((reevents & Socket::poll_flags::in_hup) != Socket::poll_flags::none)
                {
                    try
                    {
                        process_tcp_data(it->second);
                        ++it;
                    }
                    catch (std::exception & e)
                    {
                        remove_tcp_peer(it->first);
                        it = _peers.erase(it);
                    }
                }
                else
                    ++it;
            }
        }
        
        process_waiting_in_client();
        // We might have found a peer while he didn't find us yet, so begin the connection procedure
        process_waiting_out_clients();
    }

    _poll.clear();
}

void Network::set_peer_id(peer_t const& peerid)
{
    std::lock_guard<std::recursive_mutex> lk(local_mutex);
    _peer_id = peerid;
}

Network::peer_t const& Network::get_peer_id()
{
    std::lock_guard<std::recursive_mutex> lk(local_mutex);
    return _peer_id;
}

void Network::register_listener(IRunNetwork* listener, Network_Message_pb::MessagesCase type)
{
    std::lock_guard<std::recursive_mutex> lk(local_mutex);

    _network_listeners[type].push_back(listener);
}

void Network::unregister_listener(IRunNetwork* listener, Network_Message_pb::MessagesCase type)
{
    std::lock_guard<std::recursive_mutex> lk(local_mutex);

    auto& listeners = _network_listeners[type];
    listeners.erase(
        std::remove(listeners.begin(), listeners.end(), listener),
        listeners.end());
}

void Network::RunNetwork(Network_Message_pb::MessagesCase MessageFilter)
{
    {
        std::lock_guard<std::mutex> lk(message_mutex);

        if (!_pending_network_msgs.empty())
        {
            std::move(_pending_network_msgs.begin(), _pending_network_msgs.end(), std::back_inserter(_network_msgs));
            _pending_network_msgs.clear();
        }
    }

    {
        std::lock_guard<std::recursive_mutex> lk(local_mutex);
        for (auto it = _network_msgs.begin(); it != _network_msgs.end(); )
        {
            auto msg_case = it->messages_case();
            if (msg_case != Network_Message_pb::MessagesCase::MESSAGES_NOT_SET)
            {
                if (MessageFilter == Network_Message_pb::MessagesCase::MESSAGES_NOT_SET || MessageFilter == msg_case)
                {
                    auto& listeners = _network_listeners[msg_case];
                    for (auto& item : listeners)
                        item->RunNetwork(*it);

                    it = _network_msgs.erase(it);
                }
                else
                {
                    ++it;
                }
            }
            else
            {// Don't care about invalid message
                it = _network_msgs.erase(it);
            }
        }
    }
}

bool Network::SendBroadcast(Network_Message_pb& msg)
{
    std::lock_guard<std::recursive_mutex> lk(local_mutex);

    std::vector<ipv4_addr> broadcasts = std::move(get_broadcasts());

    assert(msg.source_id() != peer_t() && "Source id cannot be null");
    assert(msg.dest_id() == peer_t() && "Destination id should be null");

    //if (msg.appid() == 0)
    //    msg.set_appid(Settings::Inst().gameid.AppID());

    msg.set_timestamp(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count());

    std::string buffer;
    msg.SerializeToString(&buffer);
#if defined(NETWORK_COMPRESS)
    max_message_size = std::max<uint64_t>(max_message_size, buffer.length());

    buffer = std::move(compress(buffer.data(), buffer.length()));
    max_compressed_message_size = std::max<uint64_t>(max_compressed_message_size, buffer.length());
#endif

    for (auto& brd : broadcasts)
    {
        for (uint16_t port = network_port; port < max_network_port; ++port)
        {
            brd.set_port(port);
            try
            {
                _udp_socket.sendto(brd, buffer.data(), buffer.length());
                //APP_LOG(Log::LogLevel::TRACE, "Send broadcast");
            }
            catch (socket_exception & e)
            {
                //APP_LOG(Log::LogLevel::WARN, "Udp socket exception: %s", e.what());
                return false;
            }
        }
    }

    return true;
}

std::unordered_set<Network::peer_t> Network::UnreliableSendToAllPeers(Network_Message_pb& msg)
{
    std::lock_guard<std::recursive_mutex> lk(local_mutex);

    assert(msg.source_id() != peer_t() && "Source id cannot be null");
    assert(msg.app_id() != 0 && "Appid cannot be null");

    std::unordered_set<peer_t> peers_sent_to;

    std::for_each(_peers.begin(), _peers.end(), [&](std::pair<peer_t const, peer_state_t>& peer_infos)
    {
        msg.set_dest_id(peer_infos.first);
        msg.set_timestamp(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count());

        std::string buffer;
        msg.SerializeToString(&buffer);

    #if defined(NETWORK_COMPRESS)
        buffer = std::move(compress(buffer.data(), buffer.length()));

        max_message_size = std::max<uint64_t>(max_message_size, buffer.length());
        max_compressed_message_size = std::max<uint64_t>(max_compressed_message_size, buffer.length());
    #endif

        try
        {
            _udp_socket.sendto(peer_infos.second.udp_addr, buffer.data(), buffer.length());
            peers_sent_to.insert(peer_infos.first);
            //APP_LOG(Log::LogLevel::TRACE, "Sent message to %s", peer_infos.second.to_string().c_str());
        }
        catch (socket_exception & e)
        {
            //APP_LOG(Log::LogLevel::WARN, "Udp socket exception: %s on %s", e.what(), peer_infos.second.to_string().c_str());
        }
    });

    return peers_sent_to;
}

bool Network::UnreliableSendTo(Network_Message_pb& msg)
{
    assert(msg.source_id() != peer_t() && "Source id cannot be null");
    assert(msg.dest_id() != peer_t() && "Destination id cannot be null");
    assert(msg.app_id() != 0 && "Appid cannot be null");

    msg.set_timestamp(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count());
    if (_peer_id == msg.dest_id())
    {
        std::lock_guard<std::mutex> lk(message_mutex);
        _pending_network_msgs.emplace_back(std::move(msg));
    }
    else
    {
        std::lock_guard<std::recursive_mutex> lk(local_mutex);
        auto it = _peers.find(msg.dest_id());
        if (it == _peers.end())
        {
            //APP_LOG(Log::LogLevel::ERR, "No route to %llu", msg.dest_id());
            return false;
        }

        std::string buffer;
        msg.SerializeToString(&buffer);

#if defined(NETWORK_COMPRESS)
        max_message_size = std::max<uint64_t>(max_message_size, buffer.length());

        buffer = std::move(compress(buffer.data(), buffer.length()));
        max_compressed_message_size = std::max<uint64_t>(max_compressed_message_size, buffer.length());
#endif

        try
        {
            _udp_socket.sendto(it->second.udp_addr, buffer.data(), buffer.length());
            //APP_LOG(Log::LogLevel::DEBUG, "Sent message to peer_id: %s, addr: %s", std::to_string(msg.dest_id()).c_str(), it->second.udp_addr.to_string().c_str());
        }
        catch (socket_exception& e)
        {
            //APP_LOG(Log::LogLevel::WARN, "Udp socket exception: %s on %s", e.what(), it->second.to_string().c_str());
            return false;
        }
    }
    return true;
}

std::unordered_set<Network::peer_t> Network::ReliableSendToAllPeers(Network_Message_pb& msg)
{
    std::lock_guard<std::recursive_mutex> lk(local_mutex);

    std::unordered_set<peer_t> peers_sent_to;

    assert(msg.source_id() != peer_t() && "Source id cannot be null");
    assert(msg.app_id() != 0 && "Appid cannot be null");

    std::for_each(_peers.begin(), _peers.end(), [&](std::pair<peer_t const, peer_state_t>& client)
    {
        msg.set_dest_id(client.first);
        msg.set_timestamp(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count());

        std::string buffer(sizeof(next_packet_size_t), 0);

    #if defined(NETWORK_COMPRESS)
        std::string data;
        msg.SerializeToString(&data);

        max_message_size = std::max<uint64_t>(max_message_size, data.length());

        buffer += std::move(compress(data.data(), data.length()));
        max_compressed_message_size = std::max<uint64_t>(max_compressed_message_size, buffer.length());
    #else
        buffer += std::move(msg.SerializeAsString());
    #endif

        *reinterpret_cast<next_packet_size_t*>(&buffer[0]) = make_next_packet_size(buffer);

        try
        {
            client.second.tcp_socket.send(buffer.data(), buffer.length());
            peers_sent_to.insert(client.first);
            //APP_LOG(Log::LogLevel::TRACE, "Sent message to %s", peer_infos.second.to_string().c_str());
        }
        catch (socket_exception & e)
        {
            //APP_LOG(Log::LogLevel::WARN, "Tcp socket exception: %s on %s", e.what(), client.second->get_addr().to_string().c_str());
        }
    });

    return peers_sent_to;
}

bool Network::ReliableSendTo(Network_Message_pb& msg)
{
    std::lock_guard<std::recursive_mutex> lk(local_mutex);

    assert(msg.source_id() != peer_t() && "Source id cannot be null");
    assert(msg.dest_id() != peer_t() && "Destination id cannot be null");
    assert(msg.app_id() != 0 && "Appid cannot be null");

    msg.set_timestamp(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count());
    if (_peer_id == msg.dest_id())
    {
        std::lock_guard<std::mutex> lk(message_mutex);
        _pending_network_msgs.emplace_back(std::move(msg));
    }
    else
    {
        std::lock_guard<std::recursive_mutex> lk(local_mutex);
        auto it = _peers.find(msg.dest_id());
        if (it == _peers.end())
        {
            //APP_LOG(Log::LogLevel::ERR, "No route to %llu", msg.dest_id());
            return false;
        }

        std::string buffer(sizeof(next_packet_size_t), 0);

#if defined(NETWORK_COMPRESS)
        std::string data;
        msg.SerializeToString(&data);

        max_message_size = std::max<uint64_t>(max_message_size, data.length());

        buffer += std::move(compress(data.data(), data.length()));
        max_compressed_message_size = std::max<uint64_t>(max_compressed_message_size, buffer.length());
#else
        buffer += std::move(msg.SerializeAsString());
#endif

        *reinterpret_cast<next_packet_size_t*>(&buffer[0]) = make_next_packet_size(buffer);

        try
        {
            it->second.tcp_socket.send(buffer.data(), buffer.length());
            //APP_LOG(Log::LogLevel::TRACE, "Sent message to %s", it->second.to_string().c_str());
        }
        catch (socket_exception& e)
        {
            //APP_LOG(Log::LogLevel::WARN, "Tcp socket exception: %s on %s", e.what(), it->second->get_addr().to_string().c_str());
            return false;
        }
    }

    return true;
}

// Steam specific functions

#include "steam_client.h"

void Network::process_query_socket()
{
    Poll p;
    p.add_socket(_query_socket);
    p.set_events(0, Socket::poll_flags::in);
    while(!_query_task.want_stop())
    {
        try
        {
            while (p.poll(500) == 1)
            {
                ipv4_addr addr;
                std::array<uint8_t, 2048> buff;
                uint32 ip;
                uint16 port;
                size_t len = _query_socket.recvfrom(addr, buff.data(), buff.size());

                auto& gameserver = GetSteam_Gameserver();
                gameserver.HandleIncomingPacket(buff.data(), len, addr.get_ip(), addr.get_port());
                len = gameserver.GetNextOutgoingPacket(buff.data(), buff.size(), &ip, &port);

                addr.set_ip(ip);
                addr.set_port(port);

                _query_socket.sendto(addr, buff.data(), len);
            }
        }
        catch (...)
        {
        }
    }
}

PortableAPI::ipv4_addr Network::get_steamid_addr(uint64 steam_id)
{
    std::lock_guard<std::recursive_mutex> lk(local_mutex);

    auto it = _peers.find(steam_id);
    if (it == _peers.end())
        return ipv4_addr{};

    return it->second.udp_addr;
}

bool Network::StartQueryServer(PortableAPI::ipv4_addr& addr)
{
    if (addr.get_port() < 1024)
        return false;

    if (!_query_task.is_running())
    {
        std::lock_guard<std::recursive_mutex> lk(local_mutex);
        if (addr.get_port() == MASTERSERVERUPDATERPORT_USEGAMESOCKETSHARE)
        {
            //APP_LOG(Log::LogLevel::INFO, "Source Query in Shared Mode");
            return true;
        }

        int retry = 0;
        constexpr auto max_retry = 10;

        _query_socket.socket();
        while (retry++ < max_retry)
        {
            try
            {
                _query_socket.bind(addr);
                break;
            }
            catch (PortableAPI::socket_exception & e)
            {
                //APP_LOG(Log::LogLevel::ERR, "%s", e.what());
                Socket::SetLastError(0);
            }

            if (retry >= max_retry)
                return false;
        }
        retry = 0;

        _query_task = task();
        _query_task.run(&Network::process_query_socket, this);
        
        //APP_LOG(Log::LogLevel::INFO, "Started query server on %s", addr.to_string().c_str());
    }
    return true;
}

void Network::StopQueryServer()
{
    std::lock_guard<std::recursive_mutex> lk(local_mutex);
    _query_task.stop();
    _query_task.join();
    _query_socket.close();
}
