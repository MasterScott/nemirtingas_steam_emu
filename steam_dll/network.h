/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#if defined(NETWORK_COMPRESS)
#include <zstd.h>
#endif

#include "common_includes.h"
#include "task.h"

class IRunNetwork
{
public:
    // RunNetwork is run if you register to a network message and we received that message
    virtual bool RunNetwork(Network_Message_pb const& msg) = 0;
};

class LOCAL_API Network
{
public:
    using channel_t = int32_t;
    using peer_t = uint64_t;
    using next_packet_size_t = uint32_t;

    struct peer_state_t
    {
        PortableAPI::tcp_socket tcp_socket;
        std::vector<uint8_t> buffer;
        uint32_t next_packet_size;
        PortableAPI::ipv4_addr udp_addr;
    };

private:
    static constexpr uint16_t network_port = 56789;
    static constexpr uint16_t max_network_port = (network_port + 10);

#if defined(NETWORK_COMPRESS)
    // Performance counters
    uint64_t max_message_size;
    uint64_t max_compressed_message_size;

    ZSTD_CCtx* _zstd_ccontext;
    ZSTD_DStream* _zstd_dstream;

    std::string compress(void const* data, size_t len);
    std::string decompress(void const* data, size_t len);
#endif

    std::chrono::milliseconds _advertise_rate;
    std::chrono::steady_clock::time_point _last_advertise;
    peer_t _peer_id;
    uint16_t _tcp_port;

    PortableAPI::Poll _poll;
    PortableAPI::udp_socket _udp_socket;
    PortableAPI::udp_socket _query_socket;

    PortableAPI::tcp_socket _tcp_socket;
    std::unordered_map<peer_t, peer_state_t> _waiting_connect_tcp_clients;
    std::unordered_map<peer_t, peer_state_t> _waiting_out_tcp_clients;
    std::list<peer_state_t> _waiting_in_tcp_clients;

    std::unordered_map<peer_t, peer_state_t> _peers;

    std::map<Network_Message_pb::MessagesCase, std::vector<IRunNetwork*>> _network_listeners;

    // Lock message_mutex when accessing:
    //  _pending_network_msgs
    std::mutex message_mutex;
    // Lock local_mutex when accessing:
    //  _udp_addrs
    //  _tcp_clients
    //  _tcp_peers
    //  _my_peer_ids
    //  _network_listeners
    //  _advertise
    std::recursive_mutex local_mutex;

    inline next_packet_size_t make_next_packet_size(std::string const& buff) const;

    void do_advertise();
    void set_advertise_rate(std::chrono::milliseconds rate);
    std::chrono::milliseconds get_advertise_rate();

    void add_new_tcp_client(peer_t const& peer_id, peer_state_t& peer_state, bool send_accept);
    void remove_tcp_peer(peer_t const& peer_id);
    void connect_to_peer(PortableAPI::ipv4_addr& udp_addr, PortableAPI::ipv4_addr& tcp_addr, peer_t const& peer_id);
    void process_waiting_out_clients();
    void process_waiting_in_client();

    void process_network_message(Network_Message_pb& msg);
    void process_udp();
    void process_tcp_listen();
    void process_tcp_data(peer_state_t& tcp_buffer);
    void network_thread();
    task _network_task;

    std::list<Network_Message_pb> _pending_network_msgs;
    std::list<Network_Message_pb> _network_msgs;

public:
    Network();
    ~Network();

    void set_peer_id(peer_t const& peer_id);
    peer_t const& get_peer_id();

    bool start_network();
    void stop_network();

    std::unordered_set<peer_t> get_all_peers();

    void register_listener  (IRunNetwork* listener, Network_Message_pb::MessagesCase type);
    void unregister_listener(IRunNetwork* listener, Network_Message_pb::MessagesCase type);

    void RunNetwork(Network_Message_pb::MessagesCase MessageFilter = Network_Message_pb::MessagesCase::MESSAGES_NOT_SET);

    bool SendBroadcast(Network_Message_pb& msg); // Always UDP
    std::unordered_set<peer_t> UnreliableSendToAllPeers(Network_Message_pb& msg);
    bool UnreliableSendTo(Network_Message_pb& msg);

    std::unordered_set<peer_t> ReliableSendToAllPeers(Network_Message_pb& msg);
    bool ReliableSendTo(Network_Message_pb& msg);

    // Steam specific functions
private:
    task _query_task;
    void process_query_socket();

public:
    PortableAPI::ipv4_addr get_steamid_addr(uint64 steam_id);
    bool StartQueryServer(PortableAPI::ipv4_addr& addr);
    void StopQueryServer();
};