/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "settings.h"
#include "steam_client.h"
#include <unknown_avatar.h>
#include <notification.h>

constexpr decltype(Settings::settings_file_name) Settings::settings_file_name;
constexpr decltype(Settings::dlcs_file_name)     Settings::dlcs_file_name;

struct language_t
{
    const char* language_name;
    const char* language_key;
    const char* ui_selection;
    const char* panorama_selection;
    const char* panorama_language;
    const char* language_code1;
    const char* language_code2;
    int index;
    int lcid;
    int code;
};

#define LANGUAGE_ENTRY(LANG, KEY, LANG_CODE1, LANG_CODE2, INDEX, LCID, CODE) {LANG, KEY, "#SteamUI_Selection_" LANG, "#Panorama_Selection_" LANG, "#Panorama_Lang_" LANG, LANG_CODE1, LANG_CODE2, INDEX, LCID, CODE}
language_t languages[] =
{ 
    {"None" , "none" , "None" , "None" , "None" , "none" , "", -1, 0x0000, 0x0000},
    LANGUAGE_ENTRY("English"            , "english" , "en_US", "en"   , 0, 0x0409, 0x0000),
    LANGUAGE_ENTRY("German"             , "german"  , "de_DE", "de"   , 1, 0x0407, 0x0000),
    LANGUAGE_ENTRY("French"             , "french"  , "fr_FR", "fr"   , 2, 0x040C, 0x0000),
    LANGUAGE_ENTRY("Italian"            , "italian" , "it_IT", "it"   , 3, 0x0410, 0x0000),
    LANGUAGE_ENTRY("Korean"             , "korean"  , "ko_KR", "ko"   , 4, 0x0412, 0x0000),
    LANGUAGE_ENTRY("Spanish"            , "spanish" , "es_ES", "es"   , 5, 0x040A, 0x0000),
    LANGUAGE_ENTRY("Simplified_Chinese" , "schinese", "zh_CN", "zh-cn", 6, 0x0804, 0x0000),
    LANGUAGE_ENTRY("Traditional_Chinese", "tchinese", "zh_TW", "zh-tw", 7, 0x0404, 0x0000),
    LANGUAGE_ENTRY("Russian"            , "russian" , "ru_RU", "ru"   , 8, 0x0419, 0x0000),
};

template<typename T>
T get_setting(nlohmann::json& settings, std::string const& key, T default_val)
{
    T val;
    try
    {
        val = settings[key].get<T>();
    }
    catch (...)
    {
        val = default_val;
        settings[key] = default_val;
    }
    return val;
}

Settings::Settings()
{
    load_settings();
}

Settings::~Settings()
{

}

Settings& Settings::Inst()
{
    static std::shared_ptr<Settings> inst(new Settings);
    return *inst;
}

static bool load_json(std::string const& file_path, nlohmann::json& json)
{
    std::ifstream file(file_path);
    if (file)
    {
        file.seekg(0, std::ios::end);
        size_t size = static_cast<size_t>(file.tellg());
        file.seekg(0, std::ios::beg);

        std::string buffer(size, '\0');

        file.read(&buffer[0], size);
        file.close();

        try
        {
            json = std::move(nlohmann::json::parse(buffer));

            return true;
        }
        catch (std::exception& e)
        {
            APP_LOG(Log::LogLevel::ERR, "Error while parsing JSON %s: %s", file_path.c_str(), e.what());
        }
    }
    else
    {
        APP_LOG(Log::LogLevel::WARN, "File not found: %s", file_path.c_str());
    }
    return false;
}

static bool save_json(std::string const& file_path, nlohmann::json const& json)
{
    std::ofstream file(file_path, std::ios::trunc | std::ios::out);
    if (!file)
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to save: %s", file_path.c_str());
        return false;
    }
    file << std::setw(2) << json;
    return true;
}

void Settings::load_settings()
{
    APP_LOG(Log::LogLevel::DEBUG, "");

    bool default_json = false;

    nlohmann::json settings;
    config_path = std::move(FileManager::join(FileManager::dirname(get_executable_path()), settings_file_name));

    if (!load_json(config_path, settings))
    {
        default_json = true;
    }

    gameid = CGameID(get_setting(settings, "appid", 0));
    if (gameid.AppID() == 0)
    {
        try
        {
            gameid = CGameID(std::stoi(get_env_var("SteamAppId")));
        }
        catch (...)
        { }
        if (gameid.AppID() == 0)
        {
            std::string exe_path = std::move(get_executable_path());

            // check if there is a steam_appid
            std::ifstream steam_appid(exe_path + "steam_appid.txt");
            if (steam_appid)
            {
                uint32_t appid;
                steam_appid >> appid;
                gameid = CGameID(appid);
            }
        }
        settings["appid"] = gameid.AppID();
    }
    
    username       = get_setting(settings, "username"      , std::string(u8"DefaultName"));
    if (username.empty() || !utf8::is_valid(username.begin(), username.end()))
        username = u8"DefaultName";

    userid = get_setting(settings, "steamid", uint64(0));
    while (!userid.IsValid())
        userid = generate_steam_id_user_from_name(username);

    settings["steamid"] = uint64_t(userid.ConvertToUint64());
    
    language                  = get_setting(settings, "language"                 , std::string("english"));
    languages                 = get_setting(settings, "languages"                , std::string("english"));
    gamename                  = get_setting(settings, "gamename"                 , std::string("SpaceWar"));
    unlock_dlcs               = get_setting(settings, "unlock_dlcs"              , bool(true));
    enable_overlay            = get_setting(settings, "enable_overlay"           , bool(true));
    cloud_save                = get_setting(settings, "cloud_save"               , bool(true));
    beta                      = get_setting(settings, "beta"                     , bool(false));
    beta_name                 = get_setting(settings, "beta_name"                , std::string("public"));
    disable_online_networking = get_setting(settings, "disable_online_networking", bool(false));
    savepath                  = get_setting(settings, "savepath"                 , std::string("appdata"));

#ifndef DISABLE_LOG
    Log::LogLevel llvl;
    switchstr(get_setting(settings, "log_level", std::string("OFF")))
    {
        casestr("TRACE"): llvl = Log::LogLevel::TRACE; break;
        casestr("DEBUG"): llvl = Log::LogLevel::DEBUG; break;
        casestr("INFO") : llvl = Log::LogLevel::INFO ; break;
        casestr("WARN") : llvl = Log::LogLevel::WARN ; break;
        casestr("ERR")  : llvl = Log::LogLevel::ERR  ; break;
        casestr("FATAL"): llvl = Log::LogLevel::FATAL; break;
        casestr("OFF")  :
        default         : llvl = Log::LogLevel::OFF;
    }
    Log::set_loglevel(llvl);
#endif

    APP_LOG(Log::LogLevel::INFO, "Configuration Path: %s", config_path.c_str());
    if (default_json)
    {
        APP_LOG(Log::LogLevel::WARN, "Error while loading settings, building a default one");
    }

    APP_LOG(Log::LogLevel::INFO, "Setting log level to: %s", Log::loglevel_to_str(llvl));
    APP_LOG(Log::LogLevel::INFO, "Emulator version %s", _EMU_VERSION_);

    std::string settings_dir;
    if (savepath == "appdata")
    {
        settings_dir = get_userdata_path();
    }
    else if(FileManager::is_absolute(savepath))
    {
        settings_dir = savepath;
    }
    else
    {
        settings_dir = FileManager::join(FileManager::dirname(get_executable_path()), savepath);
    }

    AudioManager::load_audio(notification_ogg, notification_ogg_len, notification_audio_id);

    FileManager::set_root_dir(settings_dir);
    settings_dir = FileManager::root_dir();
    settings_dir = FileManager::join(settings_dir, emu_savepath, std::to_string(userid.ConvertToUint64()));

    load_avatar(settings_dir);

    FileManager::set_root_dir(FileManager::join(settings_dir, std::to_string(gameid.AppID())));

    load_dlcs();

    save_settings();
}

void Settings::load_avatar(std::string const& avatar_dir)
{
    std::string avatar_path(FileManager::join(avatar_dir, "user_avatar.png"));

    std::shared_ptr<Image> image;
    image = ImageManager::load_image(avatar_path, user_avatar_id);
    if (image == nullptr)
    {
        APP_LOG(Log::LogLevel::WARN, "No avatar available at %s", avatar_path.c_str());
        avatar_path = std::move(FileManager::join(avatar_dir, "user_avatar.jpg"));
        image = ImageManager::load_image(avatar_path, user_avatar_id);
        if (image == nullptr)
        {
            APP_LOG(Log::LogLevel::WARN, "No avatar available at %s", avatar_path.c_str());
            image = ImageManager::load_image(unknown_avatar_png, unknown_avatar_png_len, user_avatar_id);
            if (image == nullptr)
            {
                APP_LOG(Log::LogLevel::WARN, "Failed to load default avatar");
                image = ImageManager::create_image(nullptr, 184, 184, user_avatar_id);
            }
            else
            {
                APP_LOG(Log::LogLevel::WARN, "Loaded default avatar");
            }
        }
        else
        {
            APP_LOG(Log::LogLevel::WARN, "Avatar found at %s", avatar_path.c_str());
        }
    }
    else
    {
        APP_LOG(Log::LogLevel::WARN, "Avatar found at %s", avatar_path.c_str());
    }

    image = ImageManager::load_image(unknown_avatar_png, unknown_avatar_png_len, default_avatar_id);
    if (image == nullptr)
    {
        image = ImageManager::create_image(nullptr, 184, 184, default_avatar_id);
    }
}

void Settings::load_dlcs()
{
    std::string config_path(FileManager::canonical_path(dlcs_file_name));

    APP_LOG(Log::LogLevel::INFO, "Loading dlcs config: %s", config_path.c_str());
    FileManager::load_json(dlcs_file_name, _dlcs_db);
    for (auto dlc_it = _dlcs_db.begin(); dlc_it != _dlcs_db.end(); )
    {
        try
        {
            try
            {
                std::stoi(dlc_it.key());
            }
            catch (...)
            {
                APP_LOG(Log::LogLevel::WARN, "Removing dlc %s: Invalid dlc id, must be an integer.", dlc_it.key().c_str());
                dlc_it = _dlcs_db.erase(dlc_it);
                continue;
            }

            auto item_it = dlc_it.value().find("dlc_name");
            if (item_it == dlc_it.value().end())
            {
                APP_LOG(Log::LogLevel::WARN, "Removing dlc %s: Field \"dlc_name\" missing.", dlc_it.key().c_str());
                dlc_it = _dlcs_db.erase(dlc_it);
                continue;
            }
            else if(item_it.value().type() != nlohmann::detail::value_t::string)
            {
                APP_LOG(Log::LogLevel::WARN, "Removing dlc %s: Field \"dlc_name\" is not a string.", dlc_it.key().c_str());
                dlc_it = _dlcs_db.erase(dlc_it);
                continue;
            }
            item_it = dlc_it.value().find("enabled");
            if (item_it == dlc_it.value().end())
            {
                APP_LOG(Log::LogLevel::WARN, "Removing dlc %s: Field \"enabled\" missing.", dlc_it.key().c_str());
                dlc_it = _dlcs_db.erase(dlc_it);
            }
            else if (item_it.value().type() != nlohmann::detail::value_t::boolean)
            {
                APP_LOG(Log::LogLevel::WARN, "Removing dlc %s: Field \"enabled\" is not a boolean.", dlc_it.key().c_str());
                dlc_it = _dlcs_db.erase(dlc_it);
                continue;
            }

            ++dlc_it;
        }
        catch (...)
        {
            APP_LOG(Log::LogLevel::WARN, "Removing dlc %s: Invalid json type.", dlc_it.key().c_str());
            dlc_it = _dlcs_db.erase(dlc_it);
        }
    }
}

void Settings::save_settings()
{
    nlohmann::json settings;

    APP_LOG(Log::LogLevel::INFO, "Saving emu settings: %s", config_path.c_str());

    settings["appid"]                     = gameid.AppID();
    settings["cloud_save"]                = cloud_save;
    settings["steamid"]                   = userid.ConvertToUint64();
    settings["language"]                  = language;
    settings["languages"]                 = languages;
    settings["gamename"]                  = gamename;
    settings["unlock_dlcs"]               = unlock_dlcs;
    settings["enable_overlay"]            = enable_overlay;
    settings["beta"]                      = beta;
    settings["beta_name"]                 = beta_name;
    settings["disable_online_networking"] = disable_online_networking;
    settings["username"]                  = username;
#ifndef DISABLE_LOG
    settings["log_level"]                 = Log::loglevel_to_str();
#endif
    settings["savepath"]                  = savepath;

    save_json(config_path, settings);
}
