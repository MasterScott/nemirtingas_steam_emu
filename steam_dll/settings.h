/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"

class Settings
{
    Settings();
    Settings(Settings const&) = delete;
    Settings(Settings&&) = delete;
    Settings& operator=(Settings const&) = delete;
    Settings& operator=(Settings&&) = delete;
    
    static constexpr const char settings_file_name[] = "NemirtingasSteamEmu.json";
    static constexpr const char dlcs_file_name[] = "dlcs.json";

    std::string config_path;

    void load_avatar(std::string const& avatar_dir);
    void load_dlcs();

public:
    static Settings& Inst();

    CSteamID    userid;
    CGameID     gameid;
    std::string username;
    std::string languages;
    std::string language;
    std::string savepath;
    std::string gamename;
    std::map<AppId_t, std::string> install_paths;
    bool unlock_dlcs;
    bool cloud_save;
    bool enable_overlay;
    bool beta;
    std::string beta_name;
    bool disable_online_networking;
    nlohmann::json _dlcs_db;

    ~Settings();

    void load_settings();
    void save_settings();
};
