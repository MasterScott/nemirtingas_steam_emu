/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "source_query.h"
#include <utils.h>

enum class source_query_magic : uint32_t
{
    simple = 0xFFFFFFFFul,
    multi  = 0xFFFFFFFEul, // <--- TODO ?
};

enum class source_query_header : uint8_t
{
    A2S_INFO   = 'T',
    A2S_PLAYER = 'U',
    A2S_RULES  = 'V',
};

enum class source_response_header : uint8_t
{
    A2S_CHALLENGE = 'A',
    A2S_INFO      = 'I',
    A2S_PLAYER    = 'D',
    A2S_RULES     = 'E',
};

enum class source_server_type : uint8_t
{
    dedicated     = 'd',
    non_dedicated = 'i',
    source_tc     = 'p',
};

enum class source_server_env : uint8_t
{
    linux   = 'l',
    windows = 'w',
    old_mac = 'm',
    mac     = 'o',
};

enum class source_server_visibility : uint8_t
{
    _public  = 0,
    _private = 1,
};

enum class source_server_vac : uint8_t
{
    unsecured = 0,
    secured   = 1,
};

enum source_server_extra_flag : uint8_t
{
    none      = 0x00,
    gameid    = 0x01,
    steamid   = 0x10,
    keywords  = 0x20,
    spectator = 0x40,
    port      = 0x80,
};

#if defined(__WINDOWS__)
static constexpr source_server_env my_server_env = source_server_env::windows;
#elif defined(__LINUX__)
static constexpr source_server_env my_server_env = source_server_env::linux;
#elif defined(__APPLE__)
static constexpr source_server_env my_server_env = source_server_env::mac;
#endif

#pragma pack(push, 1)

constexpr char a2s_info_payload[]      = "Source Engine Query";
constexpr size_t a2s_info_payload_size = sizeof(a2s_info_payload);

struct source_query_data
{
    source_query_magic magic;
    source_query_header header;
    union
    {
        char a2s_info_payload[a2s_info_payload_size];
        uint32_t challenge;
    };
};

static constexpr size_t source_query_header_size = sizeof(source_query_magic) + sizeof(source_query_header);
static constexpr size_t a2s_query_info_size      = source_query_header_size + sizeof(source_query_data::a2s_info_payload);
static constexpr size_t a2s_query_challenge_size = source_query_header_size + sizeof(source_query_data::challenge);

struct source_response_data
{
    source_query_magic magic;
    source_response_header header;
    uint32_t challenge;
};

static constexpr size_t source_response_header_size = sizeof(source_response_data);

#pragma pack(pop)

void serialize_response(std::vector<uint8_t>& buffer, const void* _data, size_t len)
{
    const uint8_t* data = reinterpret_cast<const uint8_t*>(_data);

    buffer.insert(buffer.end(), data, data + len);
}

template<typename T>
void serialize_response(std::vector<uint8_t>& buffer, T const& v)
{
    uint8_t const* data = reinterpret_cast<uint8_t const*>(&v);
    serialize_response(buffer, data, sizeof(T));
}

template<>
void serialize_response<std::string>(std::vector<uint8_t>& buffer, std::string const& v)
{
    uint8_t const* str = reinterpret_cast<uint8_t const*>(v.c_str());
    serialize_response(buffer, str, v.length()+1);
}

template<typename T, int N>
void serialize_response(std::vector <uint8_t>& buffer, char(&str)[N])
{
    serialize_response(buffer, reinterpret_cast<uint8_t const*>(str), N);
}

void get_challenge(std::vector<uint8_t> &challenge_buff)
{
    // TODO: generate the challenge id
    serialize_response(challenge_buff, source_query_magic::simple);
    serialize_response(challenge_buff, source_response_header::A2S_CHALLENGE);
    serialize_response(challenge_buff, static_cast<uint32_t>(0x00112233ul));
}

std::vector<uint8_t> Source_Query::handle_source_query(const void* buffer, size_t len, query_server_infos_t& infos)
{
    std::vector<uint8_t> output_buffer;

    if (len < source_query_header_size) // its not at least 5 bytes long (0xFF 0xFF 0xFF 0xFF 0x??)
        return output_buffer;

    source_query_data const& query = *reinterpret_cast<source_query_data const*>(buffer);

    // || gs.max_player_count() == 0
    if (infos.offline || query.magic != source_query_magic::simple)
        return output_buffer;

    switch (query.header)
    {
    case source_query_header::A2S_INFO:
        if (len >= a2s_query_info_size && !strncmp(query.a2s_info_payload, a2s_info_payload, a2s_info_payload_size))
        {
            serialize_response(output_buffer, source_query_magic::simple);
            serialize_response(output_buffer, source_response_header::A2S_INFO);
            serialize_response(output_buffer, static_cast<uint8_t>(2));
            serialize_response(output_buffer, infos.name);
            serialize_response(output_buffer, infos.map_name);
            serialize_response(output_buffer, infos.mod_dir);
            serialize_response(output_buffer, infos.product);
            serialize_response(output_buffer, static_cast<uint16_t>(0));
            serialize_response(output_buffer, static_cast<uint8_t>(0));
            serialize_response(output_buffer, static_cast<uint8_t>(infos.max_player_count));
            serialize_response(output_buffer, static_cast<uint8_t>(infos.bot_player_count));
            serialize_response(output_buffer, (infos.dedicated ? source_server_type::dedicated : source_server_type::non_dedicated));;
            serialize_response(output_buffer, my_server_env);
            serialize_response(output_buffer, source_server_visibility::_public);
            serialize_response(output_buffer, (infos.secure ? source_server_vac::secured : source_server_vac::unsecured));
            serialize_response(output_buffer, infos.version);

            uint8_t flags = source_server_extra_flag::none;

            if (infos.server_port != 0)
                flags |= source_server_extra_flag::port;

            //if (gs. ? )
            //    flags |= source_server_extra_flag::steamid;

            if (infos.spectator_port != 0)
                flags |= source_server_extra_flag::spectator;

            if (CGameID(infos.appid).IsValid())
                flags |= source_server_extra_flag::gameid;


            if (flags != source_server_extra_flag::none)
                serialize_response(output_buffer, flags);

            if (flags & source_server_extra_flag::port)
                serialize_response(output_buffer, static_cast<uint16_t>(infos.server_port));

            //if (flags & source_server_extra_flag::steamid)
            //{
            //    serialize_response(output_buffer, ???);
            //}

            if (flags & source_server_extra_flag::spectator)
            {
                serialize_response(output_buffer, static_cast<uint16_t>(infos.spectator_port));
                serialize_response(output_buffer, infos.spectator_server);
            }

            // keywords

            if (flags & source_server_extra_flag::gameid)
                serialize_response(output_buffer, CGameID(infos.appid).ToUint64());
        }
        break;

    case source_query_header::A2S_PLAYER:
        if (len >= a2s_query_challenge_size)
        {
            if (query.challenge == 0xFFFFFFFFul)
            {
                get_challenge(output_buffer);
            }
            else if (query.challenge == 0x00112233ul)
            {
                serialize_response(output_buffer, source_query_magic::simple);
                serialize_response(output_buffer, source_response_header::A2S_PLAYER);
                serialize_response(output_buffer, static_cast<uint8_t>(infos.players.size())); // num_players

                int i = 0;
                for (auto &player : infos.players)
                {
                    serialize_response(output_buffer, static_cast<uint8_t>(i++)); // player index
                    serialize_response(output_buffer, player.second.name); // player name
                    serialize_response(output_buffer, static_cast<uint32_t>(player.second.score)); // player score
                    serialize_response(output_buffer, static_cast<float>(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now().time_since_epoch()).count()) - player.second.join_time); // player online time
                }

            }
        }
        break;

    case source_query_header::A2S_RULES:
        if (len >= a2s_query_challenge_size)
        {
            if (query.challenge == 0xFFFFFFFFul)
            {
                get_challenge(output_buffer);
            }
            else if (query.challenge == 0x00112233ul)
            {
                //auto values = gs.values();

                serialize_response(output_buffer, source_query_magic::simple);
                serialize_response(output_buffer, source_response_header::A2S_RULES);
                serialize_response(output_buffer, static_cast<uint16_t>(infos.rules.size()));

                for (auto const& i : infos.rules)
                {
                    serialize_response(output_buffer, i.first);
                    serialize_response(output_buffer, i.second);
                }
            }
        }
        break;
    }
    return output_buffer;
}

uint32_t Source_Query::handle_challenge(const void* _buffer, size_t len)
{
    const uint8_t* buffer = reinterpret_cast<const uint8_t*>(_buffer);
    size_t read_len = 0;

    if (len < source_query_header_size) // its not at least 5 bytes long (0xFF 0xFF 0xFF 0xFF 0x??)
        return 0xFFFFFFFF;

    source_response_data const& query = *reinterpret_cast<source_response_data const*>(buffer);

    if (query.magic != source_query_magic::simple || query.header != source_response_header::A2S_CHALLENGE)
        return 0xFFFFFFFF;

    return query.challenge;
}

std::vector<uint8_t> Source_Query::create_source_ping(uint32_t challenge)
{
    std::vector<uint8_t> buffer;

    //if (challenge == 0xFFFFFFFF)
    //{
    //    serialize_response(buffer, source_query_magic::simple);
    //    serialize_response(buffer, source_response_header::A2S_CHALLENGE);
    //    serialize_response(buffer, static_cast<uint32_t>(0x00112233ul));
    //}

    return buffer;
}

std::vector<uint8_t> Source_Query::create_source_players(uint32_t challenge)
{
    std::vector<uint8_t> buffer;

    serialize_response(buffer, source_query_magic::simple);
    serialize_response(buffer, source_query_header::A2S_PLAYER);
    serialize_response(buffer, static_cast<uint32_t>(challenge));

    return buffer;
}

std::vector<query_player_infos_t> Source_Query::handle_source_players(const void* _buffer, size_t len)
{
    std::vector<query_player_infos_t> players;
    const uint8_t* buffer = reinterpret_cast<const uint8_t*>(_buffer);
    size_t read_len = 0;

    if (len < source_query_header_size) // its not at least 5 bytes long (0xFF 0xFF 0xFF 0xFF 0x??)
        return players;

    source_query_data const& query = *reinterpret_cast<source_query_data const*>(buffer);

    if (query.magic != source_query_magic::simple || query.header != source_query_header::A2S_PLAYER)
        return players;

    read_len = 5;
    uint8_t num_players = buffer[read_len++];
    
    while (num_players--)
    {
        query_player_infos_t infos;
        if (read_len < len)
        {
            ++read_len; // skip player index
        }
        if (read_len < len)
        {
            infos.name = reinterpret_cast<const char*>(&buffer[read_len]);
            read_len += infos.name.length()+1;
        }
        if (read_len < len)
        {
            infos.score = *reinterpret_cast<const uint32_t*>(&buffer[read_len]);
            read_len += sizeof(uint32_t);
        }
        players.emplace_back(std::move(infos));
    }


    return players;
}

std::vector<uint8_t> Source_Query::create_source_rules(uint32_t challenge)
{
    std::vector<uint8_t> buffer;

    serialize_response(buffer, source_query_magic::simple);
    serialize_response(buffer, source_query_header::A2S_RULES);
    serialize_response(buffer, static_cast<uint32_t>(challenge));

    return buffer;
}

std::map<std::string, std::string> Source_Query::handle_source_rules(const void* _buffer, size_t len)
{
    std::map<std::string, std::string> rules;

    const uint8_t* buffer = reinterpret_cast<const uint8_t*>(_buffer);
    size_t read_len = 0;

    if (len < source_response_header_size) // its not at least 9 bytes long
        return rules;

    source_response_data const& query = *reinterpret_cast<source_response_data const*>(buffer);

    if (query.magic != source_query_magic::simple || query.header != source_response_header::A2S_RULES)
        return rules;

    read_len = 5;
    uint16_t num_rules = *reinterpret_cast<const uint16_t*>(&buffer[read_len]);
    read_len += sizeof(uint16_t);

    while (num_rules--)
    {
        std::string key;
        std::string val;
        if (read_len < len)
        {
            key = reinterpret_cast<const char*>(&buffer[read_len]);
            read_len += key.length() + 1;
        }
        if (read_len < len)
        {
            val = reinterpret_cast<const char*>(&buffer[read_len]);
            read_len += val.length() + 1;
        }
        rules[key] = val;
    }

    return rules;
}
