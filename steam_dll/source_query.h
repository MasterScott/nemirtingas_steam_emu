/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2019 Mr Goldberg
   This file is part of the Nemirtingas's Steam Emulator

   The Nemirtingas's Steam Emulator is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   The Nemirtingas's Steam Emulator is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the Nemirtingas's Steam Emulator; if not, see
   <http://www.gnu.org/licenses/>.  */

#ifndef __INCLUDED_SOURCE_QUERY__
#define __INCLUDED_SOURCE_QUERY__

#include <string>
#include <chrono>
#include <vector>
#include <map>
#include <steam_sdk/steam_api.h>

struct query_player_infos_t
{
    uint32 score;
    std::string name;
    float join_time;
};

struct query_server_infos_t
{
    uint32_t    ip;
    uint16_t    server_port;
    uint16_t    query_port;
    uint32_t    flags;
    AppId_t     appid;
    std::string version;
    bool        offline;
    std::string name;
    std::string map_name;
    std::string mod_dir;
    std::string product;
    std::string description;
    bool        dedicated;
    bool        lan;
    CSteamID    owner_id;
    CSteamID    server_id;
    std::vector<std::pair<CSteamID, query_player_infos_t>> players;
    bool        logged;
    bool        secure;
    int         max_player_count;
    int         bot_player_count;
    bool        has_password;
    uint16_t    spectator_port;
    std::string spectator_server;
    std::map<std::string, std::string> rules;
    std::string gametags;
    std::string gamedata;
    std::string region;
    std::string gametype;

    query_server_infos_t() :
        ip(0),
        server_port(0),
        query_port(0),
        flags(0),
        appid(0),
        version(),
        offline(false),
        name(),
        map_name(),
        mod_dir(),
        product(),
        description(),
        dedicated(false),
        owner_id(),
        logged(false),
        secure(true),
        max_player_count(0),
        bot_player_count(0),
        server_id(),
        has_password(false),
        spectator_port(0),
        spectator_server()
    {}
};



class Source_Query
{
    Source_Query () = delete;
    ~Source_Query() = delete;

public:
    static std::vector<uint8_t> handle_source_query(const void* buffer, size_t len, query_server_infos_t &infos);

    static uint32_t handle_challenge(const void* buffer, size_t len);

    static std::vector<uint8_t> create_source_ping(uint32_t challenge = 0xFFFFFFFF);

    static std::vector<uint8_t> create_source_players(uint32_t challenge = 0xFFFFFFFF);
    static std::vector<query_player_infos_t> handle_source_players(const void* buffer, size_t len);

    static std::vector<uint8_t> create_source_rules(uint32_t challenge = 0xFFFFFFFF);
    static std::map<std::string, std::string> handle_source_rules(const void* buffer, size_t len);
};

#endif