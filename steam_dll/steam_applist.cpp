/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_applist.h"
#include "steam_client.h"

Steam_AppList::Steam_AppList()
{
}

Steam_AppList::~Steam_AppList()
{
    emu_deinit();
}

void Steam_AppList::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _cb_manager = cb_manager;
    _network = network;
}

void Steam_AppList::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _network.reset();
    _cb_manager.reset();
}

uint32 Steam_AppList::GetNumInstalledApps()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

uint32 Steam_AppList::GetInstalledApps(AppId_t* pvecAppID, uint32 unMaxAppIDs)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

int  Steam_AppList::GetAppName(AppId_t nAppID, STEAM_OUT_STRING() char* pchName, int cchNameMax) // returns -1 if no name was found
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

int  Steam_AppList::GetAppInstallDir(AppId_t nAppID, char* pchDirectory, int cchNameMax) // returns -1 if no dir was found
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

int Steam_AppList::GetAppBuildId(AppId_t nAppID) // return the buildid of this app, may change at any time based on backend updates to the game
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}