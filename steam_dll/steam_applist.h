/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"

class LOCAL_API Steam_AppList :
    public ISteamAppList001
{
    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;

public:
    std::recursive_mutex _local_mutex;

    Steam_AppList();
    virtual ~Steam_AppList();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    virtual uint32 GetNumInstalledApps();
    virtual uint32 GetInstalledApps(AppId_t* pvecAppID, uint32 unMaxAppIDs);

    virtual int  GetAppName(AppId_t nAppID, STEAM_OUT_STRING() char* pchName, int cchNameMax); // returns -1 if no name was found
    virtual int  GetAppInstallDir(AppId_t nAppID, char* pchDirectory, int cchNameMax); // returns -1 if no dir was found

    virtual int GetAppBuildId(AppId_t nAppID); // return the buildid of this app, may change at any time based on backend updates to the game
};