/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_apps.h"
#include "steam_client.h"
#include "settings.h"

Steam_Apps::Steam_Apps():
    _buy_time(static_cast<uint32>(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count() - 10000))
{}

Steam_Apps::~Steam_Apps()
{
    emu_deinit();
}

void Steam_Apps::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _cb_manager = cb_manager;
    _network = network;
}

void Steam_Apps::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _network.reset();
    _cb_manager.reset();
}

// returns 0 if the key does not exist
// this may be true on first call, since the app data may not be cached locally yet
// If you expect it to exists wait for the AppDataChanged_t after the first failure and ask again
int Steam_Apps::GetAppData(AppId_t nAppID, const char* pchKey, char* pchValue, int cchValueMax)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

bool Steam_Apps::BIsSubscribed()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return true;
}

bool Steam_Apps::BIsLowViolence()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_Apps::BIsCybercafe()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_Apps::BIsVACBanned()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

const char* Steam_Apps::GetCurrentGameLanguage()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return Settings::Inst().language.c_str();
}

const char* Steam_Apps::GetAvailableGameLanguages()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return Settings::Inst().languages.c_str();
}

// only use this member if you need to check ownership of another game related to yours, a demo for example
bool Steam_Apps::BIsSubscribedApp(AppId_t appID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (appID == 0 || appID == Settings::Inst().gameid.AppID())
        return true;

    auto const& dlcs_db = Settings::Inst()._dlcs_db;
    bool res = false;
    try
    {
        auto it = dlcs_db.find(std::to_string(appID));
        if (it != dlcs_db.end())
            res = (*it)["enabled"].get<bool>();
    }
    catch (...)
    {
    }

    if (!res)
        res = Settings::Inst().unlock_dlcs;

    APP_LOG(Log::LogLevel::DEBUG, "%s", res ? "true" : "false");
    return res;
}

// Takes AppID of DLC and checks if the user owns the DLC & if the DLC is installed
bool Steam_Apps::BIsDlcInstalled(AppId_t appID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d", appID);
    
    if (appID == 0)
        return true;

    auto const& dlcs_db = Settings::Inst()._dlcs_db;
    bool res = false;
    try
    {
        auto it = dlcs_db.find(std::to_string(appID));
        if (it != dlcs_db.end())
            res = (*it)["enabled"].get<bool>();
    }
    catch (...)
    {
    }

    if(!res)
        res = Settings::Inst().unlock_dlcs;

    APP_LOG(Log::LogLevel::DEBUG, "%s", res ? "true" : "false");
    return res;
}

// returns the Unix time of the purchase of the app
uint32 Steam_Apps::GetEarliestPurchaseUnixTime(AppId_t nAppID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return _buy_time;
}

// Checks if the user is subscribed to the current app through a free weekend
// This function will return false for users who have a retail or other type of license
// Before using, please ask your Valve technical contact how to package and secure your free weekened
bool Steam_Apps::BIsSubscribedFromFreeWeekend()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Returns the number of DLC pieces for the running app
int Steam_Apps::GetDLCCount()
{
    APP_LOG(Log::LogLevel::TRACE, "");

    auto const& dlcs_db = Settings::Inst()._dlcs_db;
    return dlcs_db.size();
}

// Returns metadata for DLC by index, of range [0, GetDLCCount()]
bool Steam_Apps::BGetDLCDataByIndex(int iDLC, AppId_t* pAppID, bool* pbAvailable, char* pchName, int cchNameBufferSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    auto const& dlcs_db = Settings::Inst()._dlcs_db;
    auto it = dlcs_db.begin();

    std::advance(it, iDLC);

    if (it == dlcs_db.end())
        return false;

    if (pAppID != nullptr)
        *pAppID = std::stoi(it.key());

    if (pbAvailable != nullptr)
    {
        try
        {
            *pbAvailable = it.value()["enabled"].get<bool>();
        }
        catch (...)
        {
            APP_LOG(Log::LogLevel::ERR, "Error while parsing dlc datas, dlc %s will be disabled", it.key().c_str());
            *pbAvailable = false;
        }
    }

    if (pchName != nullptr && cchNameBufferSize > 0)
    {
        std::string const& dlc_name = it.value()["dlc_name"].get_ref<std::string const&>();
        cchNameBufferSize = std::min(static_cast<int>(dlc_name.length()), cchNameBufferSize-1);
        strncpy(pchName, dlc_name.c_str(), cchNameBufferSize);
        pchName[cchNameBufferSize] = 0;
    }

    return true;
}

// Install/Uninstall control for optional DLC
void Steam_Apps::InstallDLC(AppId_t nAppID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

void Steam_Apps::UninstallDLC(AppId_t nAppID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// Request legacy cd-key for yourself or owned DLC. If you are interested in this
// data then make sure you provide us with a list of valid keys to be distributed
// to users when they purchase the game, before the game ships.
// You'll receive an AppProofOfPurchaseKeyResponse_t callback when
// the key is available (which may be immediately).
void Steam_Apps::RequestAppProofOfPurchaseKey(AppId_t nAppID)
{
    APP_LOG(Log::LogLevel::TRACE, "");

}

bool Steam_Apps::GetCurrentBetaName(char* pchName, int cchNameBufferSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    auto& settings = Settings::Inst();

    if (settings.beta)
    {
        if (pchName != nullptr)
        {
            strncpy(pchName, settings.beta_name.c_str(), cchNameBufferSize);
            pchName[cchNameBufferSize - 1] = 0;
        }
        return true;
    }
    else
    {
        if (pchName != nullptr && cchNameBufferSize > 0)
            *pchName = 0;

        return false;
    }
} // returns current beta branch name, 'public' is the default branch

bool Steam_Apps::MarkContentCorrupt(bool bMissingFilesOnly)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return true;
} // signal Steam that game files seems corrupt or missing

uint32 Steam_Apps::GetInstalledDepots(DepotId_t* pvecDepots, uint32 cMaxDepots)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return GetInstalledDepots(Settings::Inst().gameid.AppID(), pvecDepots, cMaxDepots);
}

uint32 Steam_Apps::GetInstalledDepots(AppId_t appID, DepotId_t* pvecDepots, uint32 cMaxDepots)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
} // return installed depots in mount order

// returns current app install folder for AppID, returns folder name length
uint32 Steam_Apps::GetAppInstallDir(AppId_t appID, char* pchFolder, uint32 cchFolderBufferSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    std::string app_folder;
    
    auto path_it = Settings::Inst().install_paths.find(appID);

    if (path_it == Settings::Inst().install_paths.end())
        app_folder = std::move(FileManager::dirname(get_executable_path()));
    else
        app_folder = path_it->second;

    if (pchFolder != nullptr && cchFolderBufferSize > 0)
    {
        strncpy(pchFolder, app_folder.c_str(), cchFolderBufferSize);
        pchFolder[cchFolderBufferSize-1] = 0;
    }

    return app_folder.length()+1;
}

bool Steam_Apps::BIsAppInstalled(AppId_t appID)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    if (Settings::Inst().gameid.AppID() == appID)
        return true;

    auto const& dlcs_db = Settings::Inst()._dlcs_db;
    auto dlc_it = dlcs_db.find(std::to_string(appID));
    if (dlc_it == dlcs_db.end())
        return false;

    return dlc_it.value()["enabled"].get<bool>();
} // returns true if that app is installed (not necessarily owned)

// returns the SteamID of the original owner. If this CSteamID is different from ISteamUser::GetSteamID(),
// the user has a temporary license borrowed via Family Sharing
CSteamID Steam_Apps::GetAppOwner()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return Settings::Inst().userid;
}

// Returns the associated launch param if the game is run via steam://run/<appid>//?param1=value1&param2=value2&param3=value3 etc.
// Parameter names starting with the character '@' are reserved for internal use and will always return and empty string.
// Parameter names starting with an underscore '_' are reserved for steam features -- they can be queried by the game,
// but it is advised that you not param names beginning with an underscore for your own features.
// Check for new launch parameters on callback NewUrlLaunchParameters_t
const char* Steam_Apps::GetLaunchQueryParam(const char* pchKey)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return "";
}

// get download progress for optional DLC
bool Steam_Apps::GetDlcDownloadProgress(AppId_t nAppID, uint64* punBytesDownloaded, uint64* punBytesTotal)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    return false;
}

// return the buildid of this app, may change at any time based on backend updates to the game
int Steam_Apps::GetAppBuildId()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 10;
}

// Request all proof of purchase keys for the calling appid and asociated DLC.
// A series of AppProofOfPurchaseKeyResponse_t callbacks will be sent with
// appropriate appid values, ending with a final callback where the m_nAppId
// member is k_uAppIdInvalid (zero).
void Steam_Apps::RequestAllProofOfPurchaseKeys()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");

    return;

    auto const& dlcs_db = Settings::Inst()._dlcs_db;
    for (auto dlc_it = dlcs_db.begin(); dlc_it != dlcs_db.end(); ++dlc_it)
    {
        pFrameResult_t result(new FrameResult);
        AppProofOfPurchaseKeyResponse_t& apopkr = result->CreateCallback<AppProofOfPurchaseKeyResponse_t>();
        
        apopkr.m_nAppID = std::stoi(dlc_it.key());
        apopkr.m_eResult = k_EResultOK;
        strcpy(apopkr.m_rgchKey, "12345-67890-15926");
        apopkr.m_cchKeyLength = utils::static_strlen("12345-67890-15926")+1;

        result->done = true;
        _cb_manager->add_callback(nullptr, result);
    }

    pFrameResult_t result(new FrameResult);
    AppProofOfPurchaseKeyResponse_t& apopkr = result->CreateCallback<AppProofOfPurchaseKeyResponse_t>();

    apopkr.m_nAppID = k_uAppIdInvalid;
    apopkr.m_eResult = k_EResultOK;

    result->done = true;
    _cb_manager->add_callback(nullptr, result);
}

STEAM_CALL_RESULT(FileDetailsResult_t)
SteamAPICall_t Steam_Apps::GetFileDetails(const char* pszFileName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");

    return 0;
}

// Get command line if game was launched via Steam URL, e.g. steam://run/<appid>//<command line>/.
// This method of passing a connect string (used when joining via rich presence, accepting an
// invite, etc) is preferable to passing the connect string on the operating system command
// line, which is a security risk.  In order for rich presence joins to go through this
// path and not be placed on the OS command line, you must set a value in your app's
// configuration on Steam.  Ask Valve for help with this.
//
// If game was already running and launched again, the NewUrlLaunchParameters_t will be fired.
int Steam_Apps::GetLaunchCommandLine(char* pszCommandLine, int cubCommandLine)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// Check if user borrowed this game via Family Sharing, If true, call GetAppOwner() to get the lender SteamID
bool Steam_Apps::BIsSubscribedFromFamilySharing()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_Apps::BIsTimedTrial(uint32* punSecondsAllowed, uint32* punSecondsPlayed)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    if (punSecondsAllowed != nullptr)
        *punSecondsAllowed = 0;

    if (punSecondsPlayed != nullptr)
        *punSecondsPlayed = 0;

    return false;
}