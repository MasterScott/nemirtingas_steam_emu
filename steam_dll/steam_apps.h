/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"

class LOCAL_API Steam_Apps :
    public ISteamApps001,
    public ISteamApps002,
    public ISteamApps003,
    public ISteamApps004,
    public ISteamApps005,
    public ISteamApps006,
    public ISteamApps007,
    public ISteamApps008
{
    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;

    uint32 _buy_time;

public:
    std::recursive_mutex _local_mutex;

    Steam_Apps();
    virtual ~Steam_Apps();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    // returns 0 if the key does not exist
    // this may be true on first call, since the app data may not be cached locally yet
    // If you expect it to exists wait for the AppDataChanged_t after the first failure and ask again
    virtual int GetAppData(AppId_t nAppID, const char* pchKey, char* pchValue, int cchValueMax);

    virtual bool BIsSubscribed();
    virtual bool BIsLowViolence();
    virtual bool BIsCybercafe();
    virtual bool BIsVACBanned();
    virtual const char* GetCurrentGameLanguage();
    virtual const char* GetAvailableGameLanguages();

    // only use this member if you need to check ownership of another game related to yours, a demo for example
    virtual bool BIsSubscribedApp(AppId_t appID);

    // Takes AppID of DLC and checks if the user owns the DLC & if the DLC is installed
    virtual bool BIsDlcInstalled(AppId_t appID);

    // returns the Unix time of the purchase of the app
    virtual uint32 GetEarliestPurchaseUnixTime(AppId_t nAppID);

    // Checks if the user is subscribed to the current app through a free weekend
    // This function will return false for users who have a retail or other type of license
    // Before using, please ask your Valve technical contact how to package and secure your free weekened
    virtual bool BIsSubscribedFromFreeWeekend();

    // Returns the number of DLC pieces for the running app
    virtual int GetDLCCount();

    // Returns metadata for DLC by index, of range [0, GetDLCCount()]
    virtual bool BGetDLCDataByIndex(int iDLC, AppId_t* pAppID, bool* pbAvailable, char* pchName, int cchNameBufferSize);

    // Install/Uninstall control for optional DLC
    virtual void InstallDLC(AppId_t nAppID);
    virtual void UninstallDLC(AppId_t nAppID);

    // Request legacy cd-key for yourself or owned DLC. If you are interested in this
    // data then make sure you provide us with a list of valid keys to be distributed
    // to users when they purchase the game, before the game ships.
    // You'll receive an AppProofOfPurchaseKeyResponse_t callback when
    // the key is available (which may be immediately).
    virtual void RequestAppProofOfPurchaseKey(AppId_t nAppID);

    virtual bool GetCurrentBetaName(char* pchName, int cchNameBufferSize); // returns current beta branch name, 'public' is the default branch
    virtual bool MarkContentCorrupt(bool bMissingFilesOnly); // signal Steam that game files seems corrupt or missing
    virtual uint32 GetInstalledDepots(DepotId_t* pvecDepots, uint32 cMaxDepots); // return installed depots in mount order
    virtual uint32 GetInstalledDepots(AppId_t appID, DepotId_t* pvecDepots, uint32 cMaxDepots); // return installed depots in mount order

    // returns current app install folder for AppID, returns folder name length
    virtual uint32 GetAppInstallDir(AppId_t appID, char* pchFolder, uint32 cchFolderBufferSize);
    virtual bool BIsAppInstalled(AppId_t appID); // returns true if that app is installed (not necessarily owned)

    // returns the SteamID of the original owner. If this CSteamID is different from ISteamUser::GetSteamID(),
    // the user has a temporary license borrowed via Family Sharing
    virtual CSteamID GetAppOwner();

    // Returns the associated launch param if the game is run via steam://run/<appid>//?param1=value1&param2=value2&param3=value3 etc.
    // Parameter names starting with the character '@' are reserved for internal use and will always return and empty string.
    // Parameter names starting with an underscore '_' are reserved for steam features -- they can be queried by the game,
    // but it is advised that you not param names beginning with an underscore for your own features.
    // Check for new launch parameters on callback NewUrlLaunchParameters_t
    virtual const char* GetLaunchQueryParam(const char* pchKey);

    // get download progress for optional DLC
    virtual bool GetDlcDownloadProgress(AppId_t nAppID, uint64* punBytesDownloaded, uint64* punBytesTotal);

    // return the buildid of this app, may change at any time based on backend updates to the game
    virtual int GetAppBuildId();

    // Request all proof of purchase keys for the calling appid and asociated DLC.
    // A series of AppProofOfPurchaseKeyResponse_t callbacks will be sent with
    // appropriate appid values, ending with a final callback where the m_nAppId
    // member is k_uAppIdInvalid (zero).
    virtual void RequestAllProofOfPurchaseKeys();

    STEAM_CALL_RESULT(FileDetailsResult_t)
    virtual SteamAPICall_t GetFileDetails(const char* pszFileName);

    // Get command line if game was launched via Steam URL, e.g. steam://run/<appid>//<command line>/.
    // This method of passing a connect string (used when joining via rich presence, accepting an
    // invite, etc) is preferable to passing the connect string on the operating system command
    // line, which is a security risk.  In order for rich presence joins to go through this
    // path and not be placed on the OS command line, you must set a value in your app's
    // configuration on Steam.  Ask Valve for help with this.
    //
    // If game was already running and launched again, the NewUrlLaunchParameters_t will be fired.
    virtual int GetLaunchCommandLine(char* pszCommandLine, int cubCommandLine);

    // Check if user borrowed this game via Family Sharing, If true, call GetAppOwner() to get the lender SteamID
    virtual bool BIsSubscribedFromFamilySharing();

    // check if game is a timed trial with limited playtime
    virtual bool BIsTimedTrial(uint32* punSecondsAllowed, uint32* punSecondsPlayed);
};
