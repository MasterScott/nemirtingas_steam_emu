/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_appticket.h"
#include "settings.h"

Steam_AppTicket::Steam_AppTicket()
{}

Steam_AppTicket::~Steam_AppTicket()
{
}

uint32 Steam_AppTicket::GetAppOwnershipTicketData(uint32 nAppID, void* pvBuffer, uint32 cbBufferLength, uint32* piAppId, uint32* piSteamId, uint32* piSignature, uint32* pcbSignature)
{
    if (piAppId == nullptr || piSteamId == nullptr || piSignature == nullptr || pcbSignature == nullptr)
        return 0;

    uint8_t ticket_signature[128] = {};

    uint8_t* pBuffBase = (uint8_t*)pvBuffer;
    uint8_t* pBuff = pBuffBase;

    // Ticket size
    uint32 ticket_size = 
        sizeof(uint32) + // Ticket Size
        sizeof(uint32) + // Ticket Version ?
        sizeof(uint64) + // User SteamID
        sizeof(uint32) + // AppId
        sizeof(uint32) + // External IP
        sizeof(uint32) + // Internal IP
        sizeof(uint32) + // ?
        sizeof(uint32) + // Validity Start Timestamp
        sizeof(uint32) + // Validity End Timestamp (+21 days)
        sizeof(uint16) + // Owned AppIds count
        sizeof(uint32) + // Owned AppIds
        sizeof(uint16) + // Owned Dlcs count
        sizeof(uint32) * Settings::Inst()._dlcs_db.size() + // Owned Dlcs
        sizeof(uint16) + // ?
        sizeof(uint16) + // ?
        sizeof(ticket_signature);

    if (ticket_size > cbBufferLength)
    {
        *piAppId = 0;
        *piSteamId = 0;
        *piSignature = 0;
        *pcbSignature = 0;
        return 0;
    }

    *reinterpret_cast<uint32*>(pBuff) = ticket_size; pBuff += sizeof(uint32);
    // Ticket Version ?
    *reinterpret_cast<uint32*>(pBuff) = 4; pBuff += sizeof(uint32);
    // User Steam Id
    *piSteamId = pBuff - pBuffBase;
    *reinterpret_cast<uint64*>(pBuff) = Settings::Inst().userid.ConvertToUint64(); pBuff += sizeof(uint64);
    // Stub AppId
    *piAppId = pBuff - pBuffBase;
    *reinterpret_cast<uint32*>(pBuff) = nAppID; pBuff += sizeof(uint32);
    // External IP
    *reinterpret_cast<uint32*>(pBuff) = 0x7f000001; pBuff += sizeof(uint32);
    // Internal IP
    *reinterpret_cast<uint32*>(pBuff) = 0x7f000001; pBuff += sizeof(uint32);
    // ??
    *reinterpret_cast<uint32*>(pBuff) = 0; pBuff += sizeof(uint32);
    // Validity Start Timestamp
    uint32 validity_start = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    *reinterpret_cast<uint32*>(pBuff) = validity_start; pBuff += sizeof(uint32);
    // Validity End Timestamp                            (21 days)
    *reinterpret_cast<uint32*>(pBuff) = validity_start + (21*24*60*60); pBuff += sizeof(uint32);
    // Owned AppIds count
    *reinterpret_cast<uint16*>(pBuff) = 1; pBuff += sizeof(uint16);
    // Owned AppId 1
    *reinterpret_cast<uint32*>(pBuff) = Settings::Inst().gameid.AppID(); pBuff += sizeof(uint32);
    // Owned Dlcs count
    *reinterpret_cast<uint16*>(pBuff) = Settings::Inst()._dlcs_db.size(); pBuff += sizeof(uint16);
    // Owned Dlc 1
    for (auto dlc_it = Settings::Inst()._dlcs_db.begin(); dlc_it != Settings::Inst()._dlcs_db.end(); ++dlc_it)
    {
        *reinterpret_cast<uint32*>(pBuff) = std::stoi(dlc_it.key()); pBuff += sizeof(uint32);
    }
    // Thing Count
    *reinterpret_cast<uint16*>(pBuff) = 0; pBuff += sizeof(uint16);
    // Thing Count
    *reinterpret_cast<uint16*>(pBuff) = 0; pBuff += sizeof(uint16);

    // Ticket signature
    *piSignature = pBuff - pBuffBase;
    *pcbSignature = sizeof(ticket_signature);
    memcpy(pBuff, ticket_signature, sizeof(ticket_signature)); pBuff += sizeof(ticket_signature);

    return ticket_size;
}