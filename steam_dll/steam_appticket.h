/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"

struct ISteamAppTicket001;
using ISteamAppTicket = ISteamAppTicket001;
struct ISteamAppTicket001
{
    virtual uint32 GetAppOwnershipTicketData(uint32 nAppID, void* pvBuffer, uint32 cbBufferLength, uint32* piAppId, uint32* piSteamId, uint32* piSignature, uint32* pcbSignature) = 0;
};

#define STEAMAPPTICKET_INTERFACE_VERSION001 "STEAMAPPTICKET_INTERFACE_VERSION001"
#define STEAMAPPTICKET_INTERFACE_VERSION STEAMAPPTICKET_INTERFACE_VERSION001

class Steam_AppTicket :
    public ISteamAppTicket001
{
public:
    Steam_AppTicket();
    virtual ~Steam_AppTicket();

    virtual uint32 GetAppOwnershipTicketData(uint32 nAppID, void* pvBuffer, uint32 cbBufferLength, uint32* piAppId, uint32* piSteamId, uint32* piSignature, uint32* pcbSignature);
};
