/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_client.h"
#include "settings.h"

Steam_Client::Steam_Client() :
    _ipc_calls(0),
    _initialized(false),

    _client_cb_manager(new Callback_Manager),
    _client_network(new Network),

    _steam_utils(new Steam_Utils),
    _steam_user(new Steam_User),
    _steam_friends(new Steam_Friends),
    _steam_matchmaking(new Steam_Matchmaking),
    _steam_matchmakingservers(new Steam_MatchmakingServers),
    _steam_userstats(new Steam_UserStats),
    _steam_apps(new Steam_Apps),
    _steam_networking(new Steam_Networking),
    _steam_remotestorage(new Steam_RemoteStorage),
    _steam_screenshots(new Steam_Screenshots),
    _steam_http(new Steam_HTTP),
    _steam_unifiedmessages(new Steam_UnifiedMessages),
    _steam_controller(new Steam_Controller),
    _steam_ugc(new Steam_UGC),
    _steam_applist(new Steam_AppList),
    _steam_music(new Steam_Music),
    _steam_musicremote(new Steam_MusicRemote),
    _steam_htmlsurface(new Steam_HTMLSurface),
    _steam_inventory(new Steam_Inventory),
    _steam_video(new Steam_Video),
    _steam_masterserverupdater(new Steam_MasterServerUpdater),
    _steam_gamesearch(new Steam_GameSearch),
    _steam_remoteplay(new Steam_RemotePlay),
    _steam_parties(new Steam_Parties),
    _steam_parentalsettings(new Steam_ParentalSettings),
    _steam_networkingsockets(new Steam_NetworkingSockets),
    _steam_networkingutils(new Steam_NetworkingUtils),
    _steam_networkingmessages(new Steam_NetworkingMessages),

    _gameserver_cb_manager(new Callback_Manager),
    _gameserver_network(new Network),

    _steam_gameserver(new Steam_Gameserver),
    _steam_gameserver_utils(new Steam_Utils),
    _steam_gameserver_apps(new Steam_Apps),
    _steam_gameserver_networking(new Steam_Networking),
    _steam_gameserver_http(new Steam_HTTP),
    _steam_gameserver_ugc(new Steam_UGC),
    _steam_gameserverstats(new Steam_GameServerStats),
    _steam_gameserver_inventory(new Steam_Inventory),
    _steam_gameserver_networkingsockets(new Steam_NetworkingSockets),
    _steam_gameserver_networkingmessages(new Steam_NetworkingMessages),

    _steam_appticket(new Steam_AppTicket),
    _steam_overlay(new Steam_Overlay)
{
    APP_LOG(Log::LogLevel::DEBUG, "");

    if (Settings::Inst().disable_online_networking)
    {
        disable_online_networking();
    }
}

Steam_Client::~Steam_Client()
{
    TRACE_FUNC();

    delete_client();
    delete_gameserver();

    _steam_pipes.clear();

    APP_LOG(Log::LogLevel::DEBUG, "Steam Client destroyed");
}

Steam_Client& Steam_Client::Inst()
{
    static std::shared_ptr<Steam_Client> client(new Steam_Client);

    if(!client->_initialized)
    {
        client->_initialized = true;
        client->_steam_utils->emu_init              (client->_client_cb_manager, client->_client_network);
        client->_steam_user->emu_init               (client->_client_cb_manager, client->_client_network);
        client->_steam_friends->emu_init            (client->_client_cb_manager, client->_client_network);
        client->_steam_matchmaking->emu_init        (client->_client_cb_manager, client->_client_network);
        client->_steam_matchmakingservers->emu_init (client->_client_cb_manager, client->_client_network);
        client->_steam_userstats->emu_init          (client->_client_cb_manager, client->_client_network);
        client->_steam_apps->emu_init               (client->_client_cb_manager, client->_client_network);
        client->_steam_networking->emu_init         (client->_client_cb_manager, client->_client_network);
        client->_steam_remotestorage->emu_init      (client->_client_cb_manager, client->_client_network);
        client->_steam_screenshots->emu_init        (client->_client_cb_manager, client->_client_network);
        client->_steam_http->emu_init               (client->_client_cb_manager, client->_client_network);
        client->_steam_unifiedmessages->emu_init    (client->_client_cb_manager, client->_client_network);
        client->_steam_controller->emu_init         (client->_client_cb_manager, client->_client_network);
        client->_steam_ugc->emu_init                (client->_client_cb_manager, client->_client_network);
        client->_steam_applist->emu_init            (client->_client_cb_manager, client->_client_network);
        client->_steam_music->emu_init              (client->_client_cb_manager, client->_client_network);
        client->_steam_musicremote->emu_init        (client->_client_cb_manager, client->_client_network);
        client->_steam_htmlsurface->emu_init        (client->_client_cb_manager, client->_client_network);
        client->_steam_inventory->emu_init          (client->_client_cb_manager, client->_client_network);
        client->_steam_video->emu_init              (client->_client_cb_manager, client->_client_network);
        client->_steam_masterserverupdater->emu_init(client->_client_cb_manager, client->_client_network);
        client->_steam_gamesearch->emu_init         (client->_client_cb_manager, client->_client_network);
        client->_steam_remoteplay->emu_init         (client->_client_cb_manager, client->_client_network);
        client->_steam_parties->emu_init            (client->_client_cb_manager, client->_client_network);
        client->_steam_parentalsettings->emu_init   (client->_client_cb_manager, client->_client_network);
        client->_steam_networkingsockets->emu_init  (client->_client_cb_manager, client->_client_network);
        client->_steam_networkingutils->emu_init    (client->_client_cb_manager, client->_client_network);
        client->_steam_networkingmessages->emu_init (client->_client_cb_manager, client->_client_network);
        
        client->_steam_gameserver->emu_init                   (client->_gameserver_cb_manager, client->_gameserver_network);
        client->_steam_gameserver_utils->emu_init             (client->_gameserver_cb_manager, client->_gameserver_network);
        client->_steam_gameserver_apps->emu_init              (client->_gameserver_cb_manager, client->_gameserver_network);
        client->_steam_gameserver_networking->emu_init        (client->_gameserver_cb_manager, client->_gameserver_network);
        client->_steam_gameserver_http->emu_init              (client->_gameserver_cb_manager, client->_gameserver_network);
        client->_steam_gameserver_ugc->emu_init               (client->_gameserver_cb_manager, client->_gameserver_network);
        client->_steam_gameserverstats->emu_init              (client->_gameserver_cb_manager, client->_gameserver_network);
        client->_steam_gameserver_inventory->emu_init         (client->_gameserver_cb_manager, client->_gameserver_network);
        client->_steam_gameserver_networkingsockets->emu_init (client->_gameserver_cb_manager, client->_gameserver_network);
        client->_steam_gameserver_networkingmessages->emu_init(client->_gameserver_cb_manager, client->_gameserver_network);

        client->_steam_overlay->emu_init(client->_client_cb_manager, client->_client_network);
    }

    return *client;
}

bool Steam_Client::check_pipe_user(HSteamPipe pipe)
{
    return _steam_pipes.count(pipe) == 0 || _steam_pipes[pipe].type == steam_pipe_state_t::type_t::NONE;
}

bool Steam_Client::check_pipe_user(HSteamPipe pipe, HSteamUser user)
{
    return check_pipe_user(pipe) || user == NULL_HSTEAMUSER;
}

void Steam_Client::delete_client()
{
    TRACE_FUNC();

    _client_network->stop_network();

    _steam_utils->emu_deinit();
    _steam_user->emu_deinit();
    _steam_friends->emu_deinit();
    _steam_matchmaking->emu_deinit();
    _steam_matchmakingservers->emu_deinit();
    _steam_userstats->emu_deinit();
    _steam_apps->emu_deinit();
    _steam_networking->emu_deinit();
    _steam_remotestorage->emu_deinit();
    _steam_screenshots->emu_deinit();
    _steam_http->emu_deinit();
    _steam_unifiedmessages->emu_deinit();
    _steam_controller->emu_deinit();
    _steam_ugc->emu_deinit();
    _steam_applist->emu_deinit();
    _steam_music->emu_deinit();
    _steam_musicremote->emu_deinit();
    _steam_htmlsurface->emu_deinit();
    _steam_inventory->emu_deinit();
    _steam_video->emu_deinit();
    _steam_masterserverupdater->emu_deinit();
    _steam_gamesearch->emu_deinit();
    _steam_remoteplay->emu_deinit();
    _steam_parties->emu_deinit();
    _steam_parentalsettings->emu_deinit();
    _steam_networkingsockets->emu_deinit();
    _steam_networkingutils->emu_deinit();
    _steam_networkingmessages->emu_deinit();
}

void Steam_Client::delete_gameserver()
{
    TRACE_FUNC();

    _gameserver_network->stop_network();

    _steam_gameserver->emu_deinit();
    _steam_gameserver_utils->emu_deinit();
    _steam_gameserver_apps->emu_deinit();
    _steam_gameserver_networking->emu_deinit();
    _steam_gameserver_http->emu_deinit();
    _steam_gameserver_ugc->emu_deinit();
    _steam_gameserverstats->emu_deinit();
    _steam_gameserver_inventory->emu_deinit();
    _steam_gameserver_networkingsockets->emu_deinit();
    _steam_gameserver_networkingmessages->emu_deinit();
}

ISteamNetworkingSockets* Steam_Client::getisteamnetworkingsockets(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_networkingsockets = (_steam_pipes[hSteamPipe].type == steam_pipe_state_t::type_t::SERVER ? _steam_gameserver_networkingsockets : _steam_networkingsockets).get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMNETWORKINGSOCKETS_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamNetworkingSockets*>(static_cast<ISteamNetworkingSockets##VER*>(steam_networkingsockets))
        CASEINTERFACE(001);
        CASEINTERFACE(002);
        CASEINTERFACE(003);
        CASEINTERFACE(004);
        CASEINTERFACE(005);
        CASEINTERFACE(006);
        //CASEINTERFACE(007);
        CASEINTERFACE(008);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

ISteamNetworkingUtils* Steam_Client::getisteamnetworkingutils(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    // Looks like older than version 002 doesn't need the hSteamUser param and thus passing 0, so I don't check for HSteamUser validity.
    // No big deal as I don't even use it.
    if (check_pipe_user(hSteamPipe))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_networkingutils = _steam_networkingutils.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMNETWORKINGUTILS_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamNetworkingUtils*>(static_cast<ISteamNetworkingUtils##VER*>(steam_networkingutils))
        CASEINTERFACE(001);
        CASEINTERFACE(002);
        CASEINTERFACE(003);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

ISteamNetworkingMessages* Steam_Client::getisteamnetworkingmessages(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    // Looks like older than version 002 doesn't need the hSteamUser param and thus passing 0, so I don't check for HSteamUser validity.
    // No big deal as I don't even use it.
    if (check_pipe_user(hSteamPipe))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_networkingmessages = (_steam_pipes[hSteamPipe].type == steam_pipe_state_t::type_t::SERVER ? _steam_gameserver_networkingmessages : _steam_networkingmessages).get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMNETWORKINGMESSAGES_VERSION##VER): return reinterpret_cast<ISteamNetworkingMessages*>(static_cast<ISteamNetworkingMessages##VER*>(steam_networkingmessages))
        CASEINTERFACE(002);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

ISteamAppTicket* Steam_Client::getisteamappticket(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_appticket = _steam_appticket.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMAPPTICKET_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamAppTicket*>(static_cast<ISteamAppTicket##VER*>(steam_appticket))
        CASEINTERFACE(001);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// Creates a communication pipe to the Steam client.
    // NOT THREADSAFE - ensure that no other threads are accessing Steamworks API when calling
HSteamPipe Steam_Client::CreateSteamPipe()
{
    APP_LOG(Log::LogLevel::TRACE, "");

    HSteamPipe new_pipe = 1;
    
    while (_steam_pipes.count(new_pipe))
    {// Find the first free pipe
        ++new_pipe;
    }

    APP_LOG(Log::LogLevel::INFO, "Creating a SteamPipe %d", new_pipe);

    auto& pipe = _steam_pipes[new_pipe];
    pipe.hSteamUser = NULL_HSTEAMUSER;
    pipe.type = steam_pipe_state_t::type_t::NONE;

    return new_pipe;
}

// Releases a previously created communications pipe
// NOT THREADSAFE - ensure that no other threads are accessing Steamworks API when calling
bool Steam_Client::BReleaseSteamPipe(HSteamPipe hSteamPipe)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d", hSteamPipe);

    auto it = _steam_pipes.find(hSteamPipe);
    if (it != _steam_pipes.end())
    {
        steam_pipe_state_t::type_t pipe_type = it->second.type;
        _steam_pipes.erase(it);

        // Try to find another pipe of the same type (client or server)
        it = std::find_if(_steam_pipes.begin(), _steam_pipes.end(), [&pipe_type](std::pair<const HSteamPipe, steam_pipe_state_t>& val){
            return val.second.type == pipe_type;
        });
        
        if (it == _steam_pipes.end())
        {// If we don't have any other pipe of the same type, release the steam interfaces.
            if (pipe_type == steam_pipe_state_t::type_t::CLIENT)
            {
                delete_client();
            }
            else if (pipe_type == steam_pipe_state_t::type_t::SERVER)
            {
                delete_gameserver();
            }
        }
        
        return true;
    }
    return false;
}

// connects to an existing global user, failing if none exists
// used by the game to coordinate with the steamUI
// NOT THREADSAFE - ensure that no other threads are accessing Steamworks API when calling
HSteamUser Steam_Client::ConnectToGlobalUser(HSteamPipe hSteamPipe)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d", hSteamPipe);

    _steam_pipes[hSteamPipe].hSteamUser = CLIENT_HSTEAMUSER;
    _steam_pipes[hSteamPipe].type = steam_pipe_state_t::type_t::CLIENT;

    GetISteamUtils(hSteamPipe, STEAMUTILS_INTERFACE_VERSION);

    return CLIENT_HSTEAMUSER;
}

// used by game servers, create a steam user that won't be shared with anyone else
// NOT THREADSAFE - ensure that no other threads are accessing Steamworks API when calling
HSteamUser Steam_Client::CreateLocalUser(HSteamPipe* phSteamPipe)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    return CreateLocalUser(phSteamPipe, k_EAccountTypeGameServer);
}

HSteamUser Steam_Client::CreateLocalUser(HSteamPipe* phSteamPipe, EAccountType eAccountType)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%p %d", phSteamPipe, eAccountType);
    
    if (phSteamPipe)
    {
        *phSteamPipe = CreateSteamPipe();

        _steam_pipes[*phSteamPipe].hSteamUser = SERVER_HSTEAMUSER;
        _steam_pipes[*phSteamPipe].type = steam_pipe_state_t::type_t::SERVER;

        return SERVER_HSTEAMUSER;
    }
    return NULL_HSTEAMUSER;
}

// removes an allocated user
// NOT THREADSAFE - ensure that no other threads are accessing Steamworks API when calling
void Steam_Client::ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d", hSteamPipe, hUser);

}

// retrieves the ISteamUser interface associated with the handle
ISteamUser* Steam_Client::GetISteamUser(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_user = _steam_user.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMUSER_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamUser*>(static_cast<ISteamUser##VER*>(steam_user))
        CASEINTERFACE(009);
        CASEINTERFACE(010);
        CASEINTERFACE(011);
        CASEINTERFACE(012);
        CASEINTERFACE(013);
        CASEINTERFACE(014);
        CASEINTERFACE(015);
        CASEINTERFACE(016);
        CASEINTERFACE(017);
        CASEINTERFACE(018);
        CASEINTERFACE(019);
        CASEINTERFACE(020);
        CASEINTERFACE(021);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// retrieves the ISteamGameServer interface associated with the handle
ISteamGameServer* Steam_Client::GetISteamGameServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_gameserver = _steam_gameserver.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMGAMESERVER_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamGameServer*>(static_cast<ISteamGameServer##VER*>(steam_gameserver))
        CASEINTERFACE(005);
        //CASEINTERFACE(006);
        //CASEINTERFACE(007);
        CASEINTERFACE(008);
        CASEINTERFACE(009);
        CASEINTERFACE(010);
        CASEINTERFACE(011);
        CASEINTERFACE(012);
        CASEINTERFACE(013);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// set the local IP and Port to bind to
// this must be set before CreateLocalUser()
void Steam_Client::SetLocalIPBinding(uint32 unIP, uint16 usPort)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    SteamIPAddress_t addr;
    addr.m_eType = k_ESteamIPTypeIPv4;
    addr.m_unIPv4 = unIP;

    SetLocalIPBinding(addr, usPort);
}

void Steam_Client::SetLocalIPBinding(const SteamIPAddress_t& unIP, uint16 usPort)
{
    APP_LOG(Log::LogLevel::TRACE, "");

}

// returns the ISteamFriends interface
ISteamFriends* Steam_Client::GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_friends = _steam_friends.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMFRIENDS_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamFriends*>(static_cast<ISteamFriends##VER*>(steam_friends))
        CASEINTERFACE(003);
        CASEINTERFACE(004);
        CASEINTERFACE(005);
        CASEINTERFACE(006);
        CASEINTERFACE(007);
        CASEINTERFACE(008);
        CASEINTERFACE(009);
        CASEINTERFACE(010);
        CASEINTERFACE(011);
        CASEINTERFACE(012);
        CASEINTERFACE(013);
        CASEINTERFACE(014);
        CASEINTERFACE(015);
        CASEINTERFACE(016);
        CASEINTERFACE(017);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// returns the ISteamUtils interface
ISteamUtils* Steam_Client::GetISteamUtils(HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %s", hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %s", hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_utils = (_steam_pipes[hSteamPipe].type == steam_pipe_state_t::type_t::SERVER ? _steam_gameserver_utils : _steam_utils).get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMUTILS_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamUtils*>(static_cast<ISteamUtils##VER*>(steam_utils))
        CASEINTERFACE(002);
        CASEINTERFACE(003);
        CASEINTERFACE(004);
        CASEINTERFACE(005);
        CASEINTERFACE(006);
        CASEINTERFACE(007);
        CASEINTERFACE(008);
        CASEINTERFACE(009);
        CASEINTERFACE(010);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// returns the ISteamMatchmaking interface
ISteamMatchmaking* Steam_Client::GetISteamMatchmaking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_matchmaking = _steam_matchmaking.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMMATCHMAKING_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamMatchmaking*>(static_cast<ISteamMatchmaking##VER*>(steam_matchmaking))
        CASEINTERFACE(002);
        //CASEINTERFACE(003);
        CASEINTERFACE(004);
        //CASEINTERFACE(005);
        CASEINTERFACE(006);
        CASEINTERFACE(007);
        CASEINTERFACE(008);
        CASEINTERFACE(009);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// returns the ISteamContentServer interface
ISteamContentServer* Steam_Client::GetISteamContentServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// returns the ISteamMasterServerUpdater interface
ISteamMasterServerUpdater* Steam_Client::GetISteamMasterServerUpdater(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_masterserverupdater = _steam_masterserverupdater.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMMASTERSERVERUPDATER_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamMasterServerUpdater*>(static_cast<ISteamMasterServerUpdater##VER*>(steam_masterserverupdater))
        CASEINTERFACE(001);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// returns the ISteamMatchmakingServers interface
ISteamMatchmakingServers* Steam_Client::GetISteamMatchmakingServers(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_matchmakingservers = _steam_matchmakingservers.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMMATCHMAKINGSERVERS_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamMatchmakingServers*>(static_cast<ISteamMatchmakingServers##VER*>(steam_matchmakingservers))
        //CASEINTERFACE(001);
        CASEINTERFACE(002);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// returns the a generic interface
void* Steam_Client::GetISteamGenericInterface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

#define TRY_INTERFACE(NAME, FUNC) if (!strncmp(pchVersion, NAME, utils::static_strlen(NAME)-3)) return FUNC(hSteamUser, hSteamPipe, pchVersion)
    if (!strncmp(pchVersion, STEAMUTILS_INTERFACE_VERSION, utils::static_strlen(STEAMUTILS_INTERFACE_VERSION) - 3)) return GetISteamUtils(hSteamPipe, pchVersion);

    TRY_INTERFACE(STEAMUSERSTATS_INTERFACE_VERSION          , GetISteamUserStats);
    TRY_INTERFACE(STEAMUSER_INTERFACE_VERSION               , GetISteamUser);

    TRY_INTERFACE(STEAMMASTERSERVERUPDATER_INTERFACE_VERSION, GetISteamMasterServerUpdater);
    TRY_INTERFACE(STEAMMATCHMAKINGSERVERS_INTERFACE_VERSION , GetISteamMatchmakingServers);
    TRY_INTERFACE(STEAMMATCHMAKING_INTERFACE_VERSION        , GetISteamMatchmaking);

    TRY_INTERFACE(STEAMMUSICREMOTE_INTERFACE_VERSION        , GetISteamMusicRemote);
    TRY_INTERFACE(STEAMMUSIC_INTERFACE_VERSION              , GetISteamMusic);

    TRY_INTERFACE(STEAMGAMESERVERSTATS_INTERFACE_VERSION    , GetISteamGameServerStats);
    TRY_INTERFACE(STEAMGAMESERVER_INTERFACE_VERSION         , GetISteamGameServer);

    TRY_INTERFACE(STEAMAPPLIST_INTERFACE_VERSION            , GetISteamAppList);
    TRY_INTERFACE(STEAMAPPS_INTERFACE_VERSION               , GetISteamApps);

    TRY_INTERFACE(STEAMFRIENDS_INTERFACE_VERSION            , GetISteamFriends);
    //TRY_INTERFACE(                                          , GetISteamContentServer);
    
    TRY_INTERFACE(STEAMNETWORKINGSOCKETS_INTERFACE_VERSION  , getisteamnetworkingsockets);
    TRY_INTERFACE(STEAMNETWORKINGUTILS_INTERFACE_VERSION    , getisteamnetworkingutils);
    TRY_INTERFACE(STEAMNETWORKINGMESSAGES_VERSION           , getisteamnetworkingmessages);
    TRY_INTERFACE(STEAMNETWORKING_INTERFACE_VERSION         , GetISteamNetworking);
    TRY_INTERFACE(STEAMREMOTESTORAGE_INTERFACE_VERSION      , GetISteamRemoteStorage);
    TRY_INTERFACE(STEAMSCREENSHOTS_INTERFACE_VERSION        , GetISteamScreenshots);
    TRY_INTERFACE(STEAMGAMESEARCH_INTERFACE_VERSION         , GetISteamGameSearch);
    TRY_INTERFACE(STEAMHTTP_INTERFACE_VERSION               , GetISteamHTTP);
    TRY_INTERFACE(STEAMUNIFIEDMESSAGES_INTERFACE_VERSION    , GetISteamUnifiedMessages);
    TRY_INTERFACE(STEAMCONTROLLER_INTERFACE_VERSION         , GetISteamController);
    TRY_INTERFACE(STEAMUGC_INTERFACE_VERSION                , GetISteamUGC);
    TRY_INTERFACE(STEAMHTMLSURFACE_INTERFACE_VERSION        , GetISteamHTMLSurface);
    TRY_INTERFACE(STEAMINVENTORY_INTERFACE_VERSION          , GetISteamInventory);
    TRY_INTERFACE(STEAMVIDEO_INTERFACE_VERSION              , GetISteamVideo);
    TRY_INTERFACE(STEAMPARENTALSETTINGS_INTERFACE_VERSION   , GetISteamParentalSettings);
    TRY_INTERFACE(STEAMINPUT_INTERFACE_VERSION              , GetISteamInput);
    TRY_INTERFACE(STEAMPARTIES_INTERFACE_VERSION            , GetISteamParties);
    TRY_INTERFACE(STEAMREMOTEPLAY_INTERFACE_VERSION         , GetISteamRemotePlay);
    TRY_INTERFACE(STEAMAPPTICKET_INTERFACE_VERSION          , getisteamappticket);
#undef TRY_INTERFACE

    fatal_throw(pchVersion);
    return nullptr;
}

// returns the ISteamUserStats interface
ISteamUserStats* Steam_Client::GetISteamUserStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);
    
    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_userstats = _steam_userstats.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMUSERSTATS_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamUserStats*>(static_cast<ISteamUserStats##VER*>(steam_userstats))
        CASEINTERFACE(003);
        CASEINTERFACE(004);
        CASEINTERFACE(005);
        CASEINTERFACE(006);
        CASEINTERFACE(007);
        CASEINTERFACE(008);
        CASEINTERFACE(009);
        CASEINTERFACE(010);
        CASEINTERFACE(011);
        CASEINTERFACE(012);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// returns the ISteamGameServerStats interface
ISteamGameServerStats* Steam_Client::GetISteamGameServerStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);
    
    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_gameserverstats = _steam_gameserverstats.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMGAMESERVERSTATS_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamGameServerStats*>(static_cast<ISteamGameServerStats##VER*>(steam_gameserverstats))
        CASEINTERFACE(001);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// returns apps interface
ISteamApps* Steam_Client::GetISteamApps(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);
    
    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_apps = (_steam_pipes[hSteamPipe].type == steam_pipe_state_t::type_t::SERVER ? _steam_gameserver_apps : _steam_apps).get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMAPPS_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamApps*>(static_cast<ISteamApps##VER*>(steam_apps))
        CASEINTERFACE(001);
        CASEINTERFACE(002);
        CASEINTERFACE(003);
        CASEINTERFACE(004);
        CASEINTERFACE(005);
        CASEINTERFACE(006);
        CASEINTERFACE(007);
        CASEINTERFACE(008);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// networking
ISteamNetworking* Steam_Client::GetISteamNetworking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);
    
    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_networking = (_steam_pipes[hSteamPipe].type == steam_pipe_state_t::type_t::SERVER ? _steam_gameserver_networking : _steam_networking).get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMNETWORKING_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamNetworking*>(static_cast<ISteamNetworking##VER*>(steam_networking))
        CASEINTERFACE(001);
        CASEINTERFACE(002);
        CASEINTERFACE(003);
        CASEINTERFACE(004);
        CASEINTERFACE(005);
        CASEINTERFACE(006);
#undef CASEINTERFACE
    }
  
    fatal_throw(pchVersion);
    return nullptr;
}

// remote storage
ISteamRemoteStorage* Steam_Client::GetISteamRemoteStorage(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);
    
    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_remotestorage = _steam_remotestorage.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMREMOTESTORAGE_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamRemoteStorage*>(static_cast<ISteamRemoteStorage##VER*>(steam_remotestorage))
        CASEINTERFACE(001);
        CASEINTERFACE(002);
        CASEINTERFACE(003);
        CASEINTERFACE(004);
        CASEINTERFACE(005);
        CASEINTERFACE(006);
        CASEINTERFACE(007);
        CASEINTERFACE(008);
        CASEINTERFACE(009);
        CASEINTERFACE(010);
        CASEINTERFACE(011);
        CASEINTERFACE(012);
        CASEINTERFACE(013);
        CASEINTERFACE(014);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// user screenshots
ISteamScreenshots* Steam_Client::GetISteamScreenshots(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_screenshots = _steam_screenshots.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMSCREENSHOTS_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamScreenshots*>(static_cast<ISteamScreenshots##VER*>(steam_screenshots))
        CASEINTERFACE(001);
        CASEINTERFACE(002);
        CASEINTERFACE(003);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// game search
ISteamGameSearch* Steam_Client::GetISteamGameSearch(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_gamesearch = _steam_gamesearch.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMGAMESEARCH_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamGameSearch*>(static_cast<ISteamGameSearch##VER*>(steam_gamesearch))
        CASEINTERFACE(001);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// Deprecated. Applications should use SteamAPI_RunCallbacks() or SteamGameServer_RunCallbacks() instead.
void Steam_Client::RunFrame()
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// returns the number of IPC calls made since the last time this function was called
// Used for perf debugging so you can understand how many IPC calls your game makes per frame
// Every IPC call is at minimum a thread context switch if not a process one so you want to rate
// control how often you do them.
uint32 Steam_Client::GetIPCCallCount()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    _ipc_calls += 13;
    return _ipc_calls;
}

// API warning handling
// 'int' is the severity; 0 for msg, 1 for warning
// 'const char *' is the text of the message
// callbacks will occur directly after the API function is called that generated the warning or message.
void Steam_Client::SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// Trigger global shutdown for the DLL
bool Steam_Client::BShutdownIfAllPipesClosed()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return _steam_pipes.empty();
}

// Expose HTTP interface
ISteamHTTP* Steam_Client::GetISteamHTTP(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_http = (_steam_pipes[hSteamPipe].type == steam_pipe_state_t::type_t::SERVER ? _steam_gameserver_http : _steam_http).get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMHTTP_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamHTTP*>(static_cast<ISteamHTTP##VER*>(steam_http))
        CASEINTERFACE(001);
        CASEINTERFACE(002);
        CASEINTERFACE(003);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// Exposes the ISteamUnifiedMessages interface
ISteamUnifiedMessages* Steam_Client::GetISteamUnifiedMessages(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_unifiedmessages = _steam_unifiedmessages.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMUNIFIEDMESSAGES_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamUnifiedMessages*>(static_cast<ISteamUnifiedMessages##VER*>(steam_unifiedmessages))
        CASEINTERFACE(001);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// Deprecated - the ISteamUnifiedMessages interface is no longer intended for public consumption.
void* Steam_Client::DEPRECATED_GetISteamUnifiedMessages(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    return GetISteamUnifiedMessages(hSteamUser, hSteamPipe, pchVersion);
}

// Exposes the ISteamController interface - deprecated in favor of Steam Input
ISteamController* Steam_Client::GetISteamController(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_controller = _steam_controller.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMCONTROLLER_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamController*>(static_cast<ISteamController##VER*>(steam_controller))
        CASEINTERFACE(001);
        //CASEINTERFACE(002);
        CASEINTERFACE(003);
        CASEINTERFACE(004);
        CASEINTERFACE(005);
        CASEINTERFACE(006);
        CASEINTERFACE(007);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// Exposes the ISteamUGC interface
ISteamUGC* Steam_Client::GetISteamUGC(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_ugc = (_steam_pipes[hSteamPipe].type == steam_pipe_state_t::type_t::SERVER ? _steam_gameserver_ugc : _steam_ugc).get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMUGC_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamUGC*>(static_cast<ISteamUGC##VER*>(steam_ugc))
        CASEINTERFACE(001);
        CASEINTERFACE(002);
        CASEINTERFACE(003);
        CASEINTERFACE(004);
        CASEINTERFACE(005);
        CASEINTERFACE(006);
        CASEINTERFACE(007);
        CASEINTERFACE(008);
        CASEINTERFACE(009);
        CASEINTERFACE(010);
        //CASEINTERFACE(011);
        CASEINTERFACE(012);
        CASEINTERFACE(013);
        CASEINTERFACE(014);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// returns app list interface, only available on specially registered apps
ISteamAppList* Steam_Client::GetISteamAppList(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_applist = _steam_applist.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMAPPLIST_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamAppList*>(static_cast<ISteamAppList##VER*>(steam_applist))
        CASEINTERFACE(001);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// Music Player
ISteamMusic* Steam_Client::GetISteamMusic(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_music = _steam_music.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMMUSIC_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamMusic*>(static_cast<ISteamMusic##VER*>(steam_music))
        CASEINTERFACE(001);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// Music Player Remote
ISteamMusicRemote* Steam_Client::GetISteamMusicRemote(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_musicremote = _steam_musicremote.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMMUSICREMOTE_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamMusicRemote*>(static_cast<ISteamMusicRemote##VER*>(steam_musicremote))
        CASEINTERFACE(001);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// html page display
ISteamHTMLSurface* Steam_Client::GetISteamHTMLSurface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto& steam_htmlsurface = _steam_htmlsurface;

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMHTMLSURFACE_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamHTMLSurface*>(static_cast<ISteamHTMLSurface##VER*>(steam_htmlsurface.get()))
        CASEINTERFACE(001);
        CASEINTERFACE(002);
        CASEINTERFACE(003);
        CASEINTERFACE(004);
        CASEINTERFACE(005);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// Helper functions for internal Steam usage
void Steam_Client::Set_SteamAPI_CPostAPIResultInProcess(SteamAPI_PostAPIResultInProcess_t func)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%p", func);
}

void Steam_Client::Remove_SteamAPI_CPostAPIResultInProcess(SteamAPI_PostAPIResultInProcess_t func)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%p", func);
}

void Steam_Client::Set_SteamAPI_CCheckCallbackRegisteredInProcess(SteamAPI_CheckCallbackRegistered_t func)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    cb_registered = func;
    APP_LOG(Log::LogLevel::DEBUG, "%p", func);
}

void Steam_Client::DEPRECATED_Set_SteamAPI_CPostAPIResultInProcess(void (*func)())
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%p", func);
}

void Steam_Client::DEPRECATED_Remove_SteamAPI_CPostAPIResultInProcess(void (*func)())
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%p", func);
}

//STEAM_PRIVATE_API(void Set_SteamAPI_CCheckCallbackRegisteredInProcess(SteamAPI_CheckCallbackRegistered_t func); )

// inventory
ISteamInventory* Steam_Client::GetISteamInventory(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto& steam_inventory = (_steam_pipes[hSteamPipe].type == steam_pipe_state_t::type_t::SERVER ? _steam_gameserver_inventory : _steam_inventory);

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMINVENTORY_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamInventory*>(static_cast<ISteamInventory##VER*>(steam_inventory.get()))
        CASEINTERFACE(001);
        CASEINTERFACE(002);
        CASEINTERFACE(003);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// Video
ISteamVideo* Steam_Client::GetISteamVideo(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto& steam_video = _steam_video;

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMVIDEO_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamVideo*>(static_cast<ISteamVideo##VER*>(steam_video.get()))
        CASEINTERFACE(001);
        CASEINTERFACE(002);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// Parental controls
ISteamParentalSettings* Steam_Client::GetISteamParentalSettings(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_parentalsettings = _steam_parentalsettings.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMPARENTALSETTINGS_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamParentalSettings*>(static_cast<ISteamParentalSettings##VER*>(steam_parentalsettings))
        CASEINTERFACE(001);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return (ISteamParentalSettings*)this;
}

// Exposes the Steam Input interface for controller support
ISteamInput* Steam_Client::GetISteamInput(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_controller = _steam_controller.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMINPUT_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamInput*>(static_cast<ISteamInput##VER*>(steam_controller))
        CASEINTERFACE(001);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// Steam Parties interface
ISteamParties* Steam_Client::GetISteamParties(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_parties = _steam_parties.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMPARTIES_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamParties*>(static_cast<ISteamParties##VER*>(steam_parties))
        CASEINTERFACE(002);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

// Steam Remote Play interface
ISteamRemotePlay* Steam_Client::GetISteamRemotePlay(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%d %d %s", hSteamUser, hSteamPipe, pchVersion);

    if (check_pipe_user(hSteamPipe, hSteamUser))
    {
        APP_LOG(Log::LogLevel::ERR, "Failed to check pipe user: %d %d %s", hSteamUser, hSteamPipe, pchVersion);
        return nullptr;
    }

    auto steam_remoteplay = _steam_remoteplay.get();

    switchstr(pchVersion)
    {
#define CASEINTERFACE(VER) casestr(STEAMREMOTEPLAY_INTERFACE_VERSION##VER): return reinterpret_cast<ISteamRemotePlay*>(static_cast<ISteamRemotePlay##VER*>(steam_remoteplay))
        CASEINTERFACE(001);
#undef CASEINTERFACE
    }

    fatal_throw(pchVersion);
    return nullptr;
}

void Steam_Client::DestroyAllInterfaces()
{
    APP_LOG(Log::LogLevel::TRACE, "");
}