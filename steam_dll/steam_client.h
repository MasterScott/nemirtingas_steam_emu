/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"

#include "steam_utils.h"
#include "steam_user.h"
#include "steam_friends.h"
#include "steam_matchmaking.h"
#include "steam_matchmakingservers.h"
#include "steam_user_stats.h"
#include "steam_apps.h"
#include "steam_networking.h"
#include "steam_remotestorage.h"
#include "steam_screenshots.h"
#include "steam_http.h"
#include "steam_unifiedmessages.h"
#include "steam_controller.h"
#include "steam_ugc.h"
#include "steam_applist.h"
#include "steam_music.h"
#include "steam_musicremote.h"
#include "steam_htmlsurface.h"
#include "steam_inventory.h"
#include "steam_video.h"
#include "steam_gameserver.h"
#include "steam_masterserverupdater.h"
#include "steam_gameserverstats.h"
#include "steam_gamesearch.h"
#include "steam_remoteplay.h"
#include "steam_parties.h"
#include "steam_parentalsettings.h"
#include "steam_networkingsockets.h"
#include "steam_networkingutils.h"
#include "steam_networkingmessages.h"
#include "steam_appticket.h"

#include "../overlay/steam_overlay.h"

#include "callback_manager.h"
#include "network.h"

struct steam_pipe_state_t
{
    HSteamUser hSteamUser;
    enum class type_t
    {
        NONE,
        CLIENT,
        SERVER
    } type;
};

EXPORT_C_API EXPORT_STEAMCLIENT_API void* S_CALLTYPE CreateInterface(const char* pName, int* pReturnCode);

class LOCAL_API Steam_Client :
    public ISteamClient007,
    public ISteamClient008,
    public ISteamClient009,
    public ISteamClient010,
    public ISteamClient011,
    public ISteamClient012,
    public ISteamClient013,
    public ISteamClient014,
    public ISteamClient015,
    public ISteamClient016,
    public ISteamClient017,
    public ISteamClient018,
    public ISteamClient019,
    public ISteamClient020
{
    Steam_Client();
    Steam_Client(Steam_Client const&) = delete;
    Steam_Client(Steam_Client&&) = delete;
    Steam_Client& operator=(Steam_Client const&) = delete;
    Steam_Client& operator=(Steam_Client&&) = delete;

private:
    bool check_pipe_user(HSteamPipe pipe);
    bool check_pipe_user(HSteamPipe pipe, HSteamUser user);

    uint32 _ipc_calls;
    bool _initialized;

public:
    static Steam_Client& Inst();

    std::map<HSteamPipe, steam_pipe_state_t> _steam_pipes;

    SteamAPI_CheckCallbackRegistered_t cb_registered;

    std::shared_ptr<Callback_Manager>          _client_cb_manager;
    std::shared_ptr<Network>                   _client_network;

    std::shared_ptr<Steam_Utils>               _steam_utils;
    std::shared_ptr<Steam_User>                _steam_user;
    std::shared_ptr<Steam_Friends>             _steam_friends;
    std::shared_ptr<Steam_Matchmaking>         _steam_matchmaking;
    std::shared_ptr<Steam_MatchmakingServers>  _steam_matchmakingservers;
    std::shared_ptr<Steam_UserStats>           _steam_userstats;
    std::shared_ptr<Steam_Apps>                _steam_apps;
    std::shared_ptr<Steam_Networking>          _steam_networking;
    std::shared_ptr<Steam_RemoteStorage>       _steam_remotestorage;
    std::shared_ptr<Steam_Screenshots>         _steam_screenshots;
    std::shared_ptr<Steam_HTTP>                _steam_http;
    std::shared_ptr<Steam_UnifiedMessages>     _steam_unifiedmessages;
    std::shared_ptr<Steam_Controller>          _steam_controller;
    std::shared_ptr<Steam_UGC>                 _steam_ugc;
    std::shared_ptr<Steam_AppList>             _steam_applist;
    std::shared_ptr<Steam_Music>               _steam_music;
    std::shared_ptr<Steam_MusicRemote>         _steam_musicremote;
    std::shared_ptr<Steam_HTMLSurface>         _steam_htmlsurface;
    std::shared_ptr<Steam_Inventory>           _steam_inventory;
    std::shared_ptr<Steam_Video>               _steam_video;
    std::shared_ptr<Steam_MasterServerUpdater> _steam_masterserverupdater;
    std::shared_ptr<Steam_GameSearch>          _steam_gamesearch;
    std::shared_ptr<Steam_RemotePlay>          _steam_remoteplay;
    std::shared_ptr<Steam_Parties>             _steam_parties;
    std::shared_ptr<Steam_ParentalSettings>    _steam_parentalsettings;
    std::shared_ptr<Steam_NetworkingSockets>   _steam_networkingsockets;
    std::shared_ptr<Steam_NetworkingUtils>     _steam_networkingutils;
    std::shared_ptr<Steam_NetworkingMessages>  _steam_networkingmessages;


    std::shared_ptr<Callback_Manager>          _gameserver_cb_manager;
    std::shared_ptr<Network>                   _gameserver_network;

    std::shared_ptr<Steam_Gameserver>          _steam_gameserver;
    std::shared_ptr<Steam_Utils>               _steam_gameserver_utils;
    std::shared_ptr<Steam_Apps>                _steam_gameserver_apps;
    std::shared_ptr<Steam_Networking>          _steam_gameserver_networking;
    std::shared_ptr<Steam_HTTP>                _steam_gameserver_http;
    std::shared_ptr<Steam_UGC>                 _steam_gameserver_ugc;
    std::shared_ptr<Steam_GameServerStats>     _steam_gameserverstats;
    std::shared_ptr<Steam_Inventory>           _steam_gameserver_inventory;
    std::shared_ptr<Steam_NetworkingSockets>   _steam_gameserver_networkingsockets;
    std::shared_ptr<Steam_NetworkingMessages>  _steam_gameserver_networkingmessages;
    
    std::shared_ptr<Steam_AppTicket>           _steam_appticket;
    std::shared_ptr<Steam_Overlay>             _steam_overlay;

    //Steam_Client();
    virtual ~Steam_Client();

    void delete_client();
    void delete_gameserver();

    ISteamNetworkingSockets* getisteamnetworkingsockets(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);
    ISteamNetworkingUtils* getisteamnetworkingutils(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);
    ISteamNetworkingMessages* getisteamnetworkingmessages(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);
    ISteamAppTicket* getisteamappticket(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // Creates a communication pipe to the Steam client.
    // NOT THREADSAFE - ensure that no other threads are accessing Steamworks API when calling
    virtual HSteamPipe CreateSteamPipe();

    // Releases a previously created communications pipe
    // NOT THREADSAFE - ensure that no other threads are accessing Steamworks API when calling
    virtual bool BReleaseSteamPipe(HSteamPipe hSteamPipe);

    // connects to an existing global user, failing if none exists
    // used by the game to coordinate with the steamUI
    // NOT THREADSAFE - ensure that no other threads are accessing Steamworks API when calling
    virtual HSteamUser ConnectToGlobalUser(HSteamPipe hSteamPipe);

    // used by game servers, create a steam user that won't be shared with anyone else
    // NOT THREADSAFE - ensure that no other threads are accessing Steamworks API when calling
    virtual HSteamUser CreateLocalUser(HSteamPipe* phSteamPipe);
    virtual HSteamUser CreateLocalUser(HSteamPipe* phSteamPipe, EAccountType eAccountType);

    // removes an allocated user
    // NOT THREADSAFE - ensure that no other threads are accessing Steamworks API when calling
    virtual void ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser);

    // retrieves the ISteamUser interface associated with the handle
    virtual ISteamUser* GetISteamUser(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // retrieves the ISteamGameServer interface associated with the handle
    virtual ISteamGameServer* GetISteamGameServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // set the local IP and Port to bind to
    // this must be set before CreateLocalUser()
    virtual void SetLocalIPBinding(uint32 unIP, uint16 usPort);
    virtual void SetLocalIPBinding(const SteamIPAddress_t& unIP, uint16 usPort);

    // returns the ISteamFriends interface
    virtual ISteamFriends* GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // returns the ISteamUtils interface
    virtual ISteamUtils* GetISteamUtils(HSteamPipe hSteamPipe, const char* pchVersion);

    // returns the ISteamMatchmaking interface
    virtual ISteamMatchmaking* GetISteamMatchmaking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // returns the ISteamContentServer interface
    virtual ISteamContentServer* GetISteamContentServer(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // returns the ISteamMasterServerUpdater interface
    virtual ISteamMasterServerUpdater* GetISteamMasterServerUpdater(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // returns the ISteamMatchmakingServers interface
    virtual ISteamMatchmakingServers* GetISteamMatchmakingServers(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // returns the a generic interface
    virtual void* GetISteamGenericInterface(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // returns the ISteamUserStats interface
    virtual ISteamUserStats* GetISteamUserStats(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // returns the ISteamGameServerStats interface
    virtual ISteamGameServerStats* GetISteamGameServerStats(HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char* pchVersion);

    // returns apps interface
    virtual ISteamApps* GetISteamApps(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // networking
    virtual ISteamNetworking* GetISteamNetworking(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // remote storage
    virtual ISteamRemoteStorage* GetISteamRemoteStorage(HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char* pchVersion);

    // user screenshots
    virtual ISteamScreenshots* GetISteamScreenshots(HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char* pchVersion);

    // game search
    virtual ISteamGameSearch* GetISteamGameSearch(HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char* pchVersion);

    // Deprecated. Applications should use SteamAPI_RunCallbacks() or SteamGameServer_RunCallbacks() instead.
    STEAM_PRIVATE_API(virtual void RunFrame(); )

    // returns the number of IPC calls made since the last time this function was called
    // Used for perf debugging so you can understand how many IPC calls your game makes per frame
    // Every IPC call is at minimum a thread context switch if not a process one so you want to rate
    // control how often you do them.
    virtual uint32 GetIPCCallCount();

    // API warning handling
    // 'int' is the severity; 0 for msg, 1 for warning
    // 'const char *' is the text of the message
    // callbacks will occur directly after the API function is called that generated the warning or message.
    virtual void SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction);

    // Trigger global shutdown for the DLL
    virtual bool BShutdownIfAllPipesClosed();

    // Expose HTTP interface
    virtual ISteamHTTP* GetISteamHTTP(HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char* pchVersion);

    // Exposes the ISteamUnifiedMessages interface
    virtual ISteamUnifiedMessages* GetISteamUnifiedMessages(HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char* pchVersion);

    // Deprecated - the ISteamUnifiedMessages interface is no longer intended for public consumption.
    STEAM_PRIVATE_API(virtual void* DEPRECATED_GetISteamUnifiedMessages(HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char* pchVersion); )

        // Exposes the ISteamController interface - deprecated in favor of Steam Input
    virtual ISteamController* GetISteamController(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // Exposes the ISteamUGC interface
    virtual ISteamUGC* GetISteamUGC(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // returns app list interface, only available on specially registered apps
    virtual ISteamAppList* GetISteamAppList(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // Music Player
    virtual ISteamMusic* GetISteamMusic(HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char* pchVersion);

    // Music Player Remote
    virtual ISteamMusicRemote* GetISteamMusicRemote(HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char* pchVersion);

    // html page display
    virtual ISteamHTMLSurface* GetISteamHTMLSurface(HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char* pchVersion);

    // Helper functions for internal Steam usage
    virtual void Set_SteamAPI_CPostAPIResultInProcess(SteamAPI_PostAPIResultInProcess_t func);
    virtual void Remove_SteamAPI_CPostAPIResultInProcess(SteamAPI_PostAPIResultInProcess_t func);
    virtual void Set_SteamAPI_CCheckCallbackRegisteredInProcess(SteamAPI_CheckCallbackRegistered_t func);

    STEAM_PRIVATE_API(virtual void DEPRECATED_Set_SteamAPI_CPostAPIResultInProcess(void (*)()); )
    STEAM_PRIVATE_API(virtual void DEPRECATED_Remove_SteamAPI_CPostAPIResultInProcess(void (*)());)
    //STEAM_PRIVATE_API(virtual void Set_SteamAPI_CCheckCallbackRegisteredInProcess(SteamAPI_CheckCallbackRegistered_t func); )

    // inventory
    virtual ISteamInventory* GetISteamInventory(HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char* pchVersion);

    // Video
    virtual ISteamVideo* GetISteamVideo(HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char* pchVersion);

    // Parental controls
    virtual ISteamParentalSettings* GetISteamParentalSettings(HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char* pchVersion);

    // Exposes the Steam Input interface for controller support
    virtual ISteamInput* GetISteamInput(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // Steam Parties interface
    virtual ISteamParties* GetISteamParties(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    // Steam Remote Play interface
    virtual ISteamRemotePlay* GetISteamRemotePlay(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char* pchVersion);

    STEAM_PRIVATE_API(virtual void DestroyAllInterfaces(); )
};

inline static Steam_Client&            GetSteam_Client           () { return Steam_Client::Inst(); }
inline static Steam_Friends&           GetSteam_Friends          () { return *GetSteam_Client()._steam_friends; }
inline static Steam_User&              GetSteam_User             () { return *GetSteam_Client()._steam_user; }
inline static Steam_Utils&             GetSteam_Utils            () { return *GetSteam_Client()._steam_utils; }
inline static Steam_Matchmaking&       GetSteam_Matchmaking      () { return *GetSteam_Client()._steam_matchmaking; }
inline static Steam_Networking&        GetSteam_Networking       () { return *GetSteam_Client()._steam_networking; }
inline static Steam_NetworkingSockets& GetSteam_NetworkingSockets() { return *GetSteam_Client()._steam_networkingsockets; }
inline static Steam_Gameserver&        GetSteam_Gameserver       () { return *GetSteam_Client()._steam_gameserver; }
inline static Steam_Overlay&           GetSteam_Overlay          () { return *GetSteam_Client()._steam_overlay; }
inline static Callback_Manager&   GetClientCBManager  () { return *GetSteam_Client()._client_cb_manager; }
inline static Network&            GetClientNetwork    () { return *GetSteam_Client()._client_network; }

inline static Callback_Manager&   GetGameserverCBManager() { return *GetSteam_Client()._gameserver_cb_manager; }
inline static Network&            GetGameserverNetwork  () { return *GetSteam_Client()._gameserver_network; }

static HSteamPipe GetFirstClientPipe()
{
    auto& pipes = GetSteam_Client()._steam_pipes;
    for (auto const& pipe : pipes)
    {
        if (pipe.second.type == steam_pipe_state_t::type_t::CLIENT)
            return pipe.first;
    }
    return NULL_HSTEAMPIPE;
}

static HSteamPipe GetFirstServerPipe()
{
    auto& pipes = GetSteam_Client()._steam_pipes;
    for (auto const& pipe : pipes)
    {
        if (pipe.second.type == steam_pipe_state_t::type_t::SERVER)
            return pipe.first;
    }
    return NULL_HSTEAMPIPE;
}

static steam_pipe_state_t* GetSteamPipeState(HSteamPipe pipe)
{
    auto& pipes = GetSteam_Client()._steam_pipes;
    auto it = pipes.find(pipe);
    if (it == pipes.end())
        return nullptr;

    return &it->second;
}

//inline static Steam_& GetSteam_() { return *Steam_Client::Inst()._steam_; }
