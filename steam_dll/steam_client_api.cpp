/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "common_includes.h"
#include "steam_client.h"
#include "settings.h"

std::once_flag overlay_network_begin;

EXPORT_C_API EXPORT_STEAMCLIENT_API bool S_CALLTYPE Steam_BGetCallback(HSteamPipe hSteamPipe, CallbackMsg_t* pCallbackMsg)
{
    //LOG(Log::LogLevel::TRACE, "");
    bool res = false;
    auto* pipe_state = GetSteamPipeState(hSteamPipe);
    if (pipe_state != nullptr)
    {
        if (pipe_state->type == steam_pipe_state_t::type_t::CLIENT)
        {
            std::call_once(overlay_network_begin, []()
            {
                GetClientNetwork().start_network();
                if (Settings::Inst().enable_overlay)
                {
                    auto& overlay = GetSteam_Overlay();
                    overlay.SetupOverlay();
                }
            });

            res = GetClientCBManager().GetCallback(pCallbackMsg);
            GetClientNetwork().RunNetwork();
        }
        else if (pipe_state->type == steam_pipe_state_t::type_t::SERVER)
        {
            res = GetGameserverCBManager().GetCallback(pCallbackMsg);
            GetGameserverNetwork().RunNetwork();
        }
        if (res)
        {
            pCallbackMsg->m_hSteamUser = pipe_state->hSteamUser;
        }
    }

    return res;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void S_CALLTYPE Steam_FreeLastCallback(HSteamPipe hSteamPipe)
{
    //LOG(Log::LogLevel::TRACE, "");
    auto* pipe_state = GetSteamPipeState(hSteamPipe);
    if (pipe_state != nullptr)
    {
        if (pipe_state->type == steam_pipe_state_t::type_t::CLIENT)
        {
            GetClientCBManager().FreeLastCallback();
        }
        else if (pipe_state->type == steam_pipe_state_t::type_t::SERVER)
        {
            GetGameserverCBManager().FreeLastCallback();
        }
    }
}

EXPORT_C_API EXPORT_STEAMCLIENT_API bool S_CALLTYPE Steam_GetAPICallResult(HSteamPipe hSteamPipe, SteamAPICall_t hSteamAPICall, void* pCallback, int cubCallback, int iCallbackExpected, bool* pbFailed)
{
    //LOG(Log::LogLevel::TRACE, "");
    bool res = false;

    auto* pipe_state = GetSteamPipeState(hSteamPipe);
    if (pipe_state != nullptr)
    {
        if (pipe_state->type == steam_pipe_state_t::type_t::CLIENT)
        {
            res = GetClientCBManager().GetAPICallResult(hSteamAPICall, pCallback, cubCallback, iCallbackExpected, pbFailed);
            GetClientNetwork().RunNetwork();
        }
        else if (pipe_state->type == steam_pipe_state_t::type_t::SERVER)
        {
            res = GetGameserverCBManager().GetAPICallResult(hSteamAPICall, pCallback, cubCallback, iCallbackExpected, pbFailed);
            GetGameserverNetwork().RunNetwork();
        }
    }

    return res;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void * S_CALLTYPE CreateInterface(const char* pName, int* pReturnCode)
{
    Settings::Inst(); // Builds the settings object

    APP_LOG(Log::LogLevel::TRACE, "%s %p", pName, pReturnCode);
    if (pReturnCode != nullptr) 
        *pReturnCode = 0;

    switchstr(pName)
    {
#define CASEINTERFACE(VER) casestr(STEAMCLIENT_INTERFACE_VERSION##VER): return static_cast<ISteamClient##VER*>(&Steam_Client::Inst())
        CASEINTERFACE(007);
        CASEINTERFACE(008);
        CASEINTERFACE(009);
        CASEINTERFACE(010);
        CASEINTERFACE(011);
        CASEINTERFACE(012);
        CASEINTERFACE(013);
        CASEINTERFACE(014);
        CASEINTERFACE(015);
        CASEINTERFACE(016);
        CASEINTERFACE(017);
        CASEINTERFACE(018);
        CASEINTERFACE(019);
        CASEINTERFACE(020);
#undef CASEINTERFACE
    }

    if (pReturnCode != nullptr)
        *pReturnCode = -1;

    APP_LOG(Log::LogLevel::FATAL, "CreateInterface returned with nullptr: %s", pName);
    return nullptr;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void S_CALLTYPE Breakpad_SteamMiniDumpInit(uint32 a, const char* b, const char* c)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void S_CALLTYPE Breakpad_SteamSetAppID(uint32 unAppID)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void S_CALLTYPE Breakpad_SteamSetSteamID(uint64 ulSteamID)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void S_CALLTYPE Breakpad_SteamWriteMiniDumpSetComment(const char* pchMsg)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void S_CALLTYPE Breakpad_SteamWriteMiniDumpUsingExceptionInfoWithBuildId(int a, int b)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

EXPORT_C_API EXPORT_STEAMCLIENT_API bool S_CALLTYPE Steam_BConnected(HSteamUser hUser, HSteamPipe hSteamPipe)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API bool S_CALLTYPE Steam_BLoggedOn(HSteamUser hUser, HSteamPipe hSteamPipe)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API bool S_CALLTYPE Steam_BReleaseSteamPipe(HSteamPipe hSteamPipe)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API HSteamUser S_CALLTYPE Steam_ConnectToGlobalUser(HSteamPipe hSteamPipe)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return 0;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API HSteamUser S_CALLTYPE Steam_CreateGlobalUser(HSteamPipe* phSteamPipe)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return 0;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API HSteamUser S_CALLTYPE Steam_CreateLocalUser(HSteamPipe* phSteamPipe, EAccountType eAccountType)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return 0;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API HSteamPipe S_CALLTYPE Steam_CreateSteamPipe()
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return 0;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API bool S_CALLTYPE Steam_GSBLoggedOn(void* phSteamHandle)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API bool S_CALLTYPE Steam_GSBSecure(void* phSteamHandle)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API bool S_CALLTYPE Steam_GSGetSteam2GetEncryptionKeyToSendToNewClient(void* phSteamHandle, void* pvEncryptionKey, uint32* pcbEncryptionKey, uint32 cbMaxEncryptionKey)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API uint64 S_CALLTYPE Steam_GSGetSteamID()
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void S_CALLTYPE Steam_GSLogOff(void* phSteamHandle)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void S_CALLTYPE Steam_GSLogOn(void* phSteamHandle)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

EXPORT_C_API EXPORT_STEAMCLIENT_API bool S_CALLTYPE Steam_GSRemoveUserConnect(void* phSteamHandle, uint32 unUserID)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API bool S_CALLTYPE Steam_GSSendSteam2UserConnect(void* phSteamHandle, uint32 unUserID, const void* pvRawKey, uint32 unKeyLen, uint32 unIPPublic, uint16 usPort, const void* pvCookie, uint32 cubCookie)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API bool S_CALLTYPE Steam_GSSendSteam3UserConnect(void* phSteamHandle, uint64 steamID, uint32 unIPPublic, const void* pvCookie, uint32 cubCookie)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API bool S_CALLTYPE Steam_GSSendUserDisconnect(void* phSteamHandle, uint64 ulSteamID, uint32 unUserID)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API bool S_CALLTYPE Steam_GSSendUserStatusResponse(void* phSteamHandle, uint64 ulSteamID, int nSecondsConnected, int nSecondsSinceLast)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API bool S_CALLTYPE Steam_GSSetServerType(void* phSteamHandle, int32 nAppIdServed, uint32 unServerFlags, uint32 unGameIP, uint32 unGamePort, const char* pchGameDir, const char* pchVersion)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void S_CALLTYPE Steam_GSSetSpawnCount(void* phSteamHandle, uint32 ucSpawn)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

EXPORT_C_API EXPORT_STEAMCLIENT_API bool S_CALLTYPE Steam_GSUpdateStatus(void* phSteamHandle, int cPlayers, int cPlayersMax, int cBotPlayers, const char* pchServerName, const char* pchMapName)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void* S_CALLTYPE Steam_GetGSHandle(HSteamUser hUser, HSteamPipe hSteamPipe)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return nullptr;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API int S_CALLTYPE Steam_InitiateGameConnection(HSteamUser hUser, HSteamPipe hSteamPipe, void* pBlob, int cbMaxBlob, uint64 steamID, int nGameAppID, uint32 unIPServer, uint16 usPortServer, bool bSecure)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return 0;
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void S_CALLTYPE Steam_LogOff(HSteamUser hUser, HSteamPipe hSteamPipe)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void S_CALLTYPE Steam_LogOn(HSteamUser hUser, HSteamPipe hSteamPipe, uint64 ulSteamID)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void S_CALLTYPE Steam_ReleaseThreadLocalMemory(bool thread_exit)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void S_CALLTYPE Steam_ReleaseUser(HSteamPipe hSteamPipe, HSteamUser hUser)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void S_CALLTYPE Steam_SetLocalIPBinding(uint32 unIP, uint16 usLocalPort)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

EXPORT_C_API EXPORT_STEAMCLIENT_API void S_CALLTYPE Steam_TerminateGameConnection(HSteamUser hUser, HSteamPipe hSteamPipe, uint32 unIPServer, uint16 usPortServer)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

EXPORT_C_API bool S_CALLTYPE SteamInternal_GameServer_Init(uint32 unIP, uint16 usPort, uint16 usGamePort, uint16 usQueryPort, EServerMode eServerMode, const char* pchVersionString)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}