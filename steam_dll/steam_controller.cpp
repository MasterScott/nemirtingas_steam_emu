/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_controller.h"
#include "steam_client.h"
#include "settings.h"

Steam_Controller::Steam_Controller() :
    _initialized(false),
    _actionset_handle(1),
    _digitalaction_handle(1),
    _analogaction_handle(1)
{
}

Steam_Controller::~Steam_Controller()
{
    emu_deinit();
}

void Steam_Controller::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock(_local_mutex, cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(cb_manager->_local_mutex, std::adopt_lock);

    _cb_manager = cb_manager;
    _network = network;

    _cb_manager->register_frame(this);
}

void Steam_Controller::emu_deinit()
{
    TRACE_FUNC();
    if (_cb_manager != nullptr)
    {
        std::lock(_local_mutex, _cb_manager->_local_mutex);
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
        std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

        _cb_manager->unregister_frame(this);
    }

    {
        std::lock_guard<std::recursive_mutex> lk(_local_mutex);
        _network.reset();
        _cb_manager.reset();
    }
}

std::string const& Steam_Controller::get_localization_string(std::string const& key)
{
    auto it = _current_localization->find(key);
    if (it == _current_localization->end())
    {
        APP_LOG(Log::LogLevel::WARN, "Unable to find a localization for %s", key.c_str());
        return key;
    }
    
    return it->second;
}

digital_button_e Steam_Controller::str_to_digital_button(std::string button)
{
    button = to_lower(button);
    switchstr(button)
    {
#define CASESTR(x) casestr(#x): return digital_button_e::x
        CASESTR(none                     );
        CASESTR(dpad_north               );
        CASESTR(dpad_north_east          );
        CASESTR(dpad_east                );
        CASESTR(dpad_south_east          );
        CASESTR(dpad_south               );
        CASESTR(dpad_south_west          );
        CASESTR(dpad_west                );
        CASESTR(dpad_north_west          );
        CASESTR(button_y                 );
        CASESTR(button_b                 );
        CASESTR(button_x                 );
        CASESTR(button_a                 );
        CASESTR(click                    );
        CASESTR(doubletap                );
        CASESTR(edge                     );
        CASESTR(scroll_clockwise         );
        CASESTR(scroll_counterclockwise  );
        CASESTR(trigger_analog           );
        CASESTR(scroll_wheel_list_0      );
        CASESTR(scroll_wheel_list_1      );
        CASESTR(scroll_wheel_list_2      );
        CASESTR(scroll_wheel_list_3      );
        CASESTR(scroll_wheel_list_4      );
        CASESTR(scroll_wheel_list_5      );
        CASESTR(scroll_wheel_list_6      );
        CASESTR(scroll_wheel_list_7      );
        CASESTR(scroll_wheel_list_8      );
        CASESTR(scroll_wheel_list_9      );
        CASESTR(touch_menu_button_0      );
        CASESTR(touch_menu_button_1      );
        CASESTR(touch_menu_button_2      );
        CASESTR(touch_menu_button_3      );
        CASESTR(touch_menu_button_4      );
        CASESTR(touch_menu_button_5      );
        CASESTR(touch_menu_button_6      );
        CASESTR(touch_menu_button_7      );
        CASESTR(touch_menu_button_8      );
        CASESTR(touch_menu_button_9      );
        CASESTR(touch_menu_button_10     );
        CASESTR(touch_menu_button_11     );
        CASESTR(touch_menu_button_12     );
        CASESTR(touch_menu_button_13     );
        CASESTR(touch_menu_button_14     );
        CASESTR(touch_menu_button_15     );
        CASESTR(touch_menu_button_16     );
        CASESTR(touch_menu_button_17     );
        CASESTR(touch_menu_button_18     );
        CASESTR(touch_menu_button_19     );
        CASESTR(touch_menu_button_20     );
        CASESTR(touch                    );
        CASESTR(button_menu              );
        CASESTR(button_escape            );
        CASESTR(left_bumper              );
        CASESTR(right_bumper             );
        CASESTR(button_back_left         );
        CASESTR(button_back_right        );
        CASESTR(button_back_left_upper   );
        CASESTR(button_back_right_upper  );
        CASESTR(left_click               );
        CASESTR(button_lpad              );
        CASESTR(right_click              );
        CASESTR(button_rpad              );
        CASESTR(left_trigger             );
        CASESTR(button_ltrigger          );
        CASESTR(right_trigger            );
        CASESTR(button_rtrigger          );
        CASESTR(left_trigger_threshold   );
        CASESTR(button_ltrigger_threshold);
        CASESTR(right_trigger_threshold  );
        CASESTR(button_rtrigger_threshold);
        CASESTR(left_stick_click         );
        CASESTR(button_joystick          );
        CASESTR(button_steam             );
        CASESTR(always_on_action         );
        CASESTR(button_capture           );
        CASESTR(button_macro0            );
        CASESTR(button_macro1            );
        CASESTR(button_macro2            );
        CASESTR(button_macro3            );
        CASESTR(button_macro4            );
        CASESTR(button_macro5            );
        CASESTR(button_macro6            );
        CASESTR(button_macro7            );
#undef CASESTR
    }

    std::string error("Unknown digital button ");
    error += button;
    fatal_throw(error.c_str());
    return digital_button_e::none;
}

EInputSource Steam_Controller::str_to_analog_button(std::string button)
{
    button = to_lower(button);
    switchstr(button)
    {
        casestr("none"           ): return k_EInputSource_None;
        casestr("left_trackpad"  ): return k_EInputSource_LeftTrackpad;
        casestr("right_trackpad" ): return k_EInputSource_RightTrackpad;
        casestr("joystick"       ): return k_EInputSource_Joystick;
        casestr("button_diamond" ): return k_EInputSource_ABXY;
        casestr("switch"         ): return k_EInputSource_Switch;
        casestr("left_trigger"   ): return k_EInputSource_LeftTrigger;
        casestr("right_trigger"  ): return k_EInputSource_RightTrigger;
        casestr("left_bumper"    ): return k_EInputSource_LeftBumper;
        casestr("right_bumper"   ): return k_EInputSource_RightBumper;
        casestr("gyro"           ): return k_EInputSource_Gyro;
        casestr("center_trackpad"): return k_EInputSource_CenterTrackpad;
        casestr("right_joystick" ): return k_EInputSource_RightJoystick;
        casestr("dpad"           ): return k_EInputSource_DPad;
        //casestr("key"            ): return k_EControllerSource_Key;      // Keyboards with scan codes - Unused
        //casestr("mouse"          ): return k_EControllerSource_Mouse;    // Traditional mouse - Unused
        //casestr("left_gyro"      ): return k_EControllerSource_LeftGyro; // Secondary Gyro - Switch - Unused
    }

    std::string error("Unknown analog button ");
    error += button;
    fatal_throw(error.c_str());
    return k_EInputSource_None;
}

EInputSourceMode Steam_Controller::str_to_SourceMode(std::string source_mode)
{
    source_mode = to_lower(source_mode);
    switchstr(source_mode)
    {
        casestr("none"           ): return k_EInputSourceMode_None;
        casestr("dpad"           ): return k_EInputSourceMode_Dpad;
        casestr("buttons"        ): return k_EInputSourceMode_Buttons;
        casestr("four_buttons"   ): return k_EInputSourceMode_FourButtons;
        casestr("absolute_mouse" ): return k_EInputSourceMode_AbsoluteMouse;
        casestr("relative_mouse" ): return k_EInputSourceMode_RelativeMouse;
        casestr("joystick_move"  ): return k_EInputSourceMode_JoystickMove;
        casestr("joystick_mouse" ): return k_EInputSourceMode_JoystickMouse;
        casestr("joystick_camera"): return k_EInputSourceMode_JoystickCamera;
        casestr("scrollwheel"    ): return k_EInputSourceMode_ScrollWheel;
        casestr("trigger"        ): return k_EInputSourceMode_Trigger;
        casestr("touch_menu"     ): return k_EInputSourceMode_TouchMenu;
        casestr("mouse_joystick" ): return k_EInputSourceMode_MouseJoystick;
        casestr("mouse_region"   ): return k_EInputSourceMode_MouseRegion;
        casestr("radial_menu"    ): return k_EInputSourceMode_RadialMenu;
        casestr("single_button"  ): return k_EInputSourceMode_SingleButton;
        casestr("switches"       ): return k_EInputSourceMode_Switches;
    }

    std::string error("Unknown source mode ");
    error += source_mode;
    fatal_throw(error.c_str());
    return k_EInputSourceMode_None;
}

activator_e Steam_Controller::str_to_activator(std::string activator)
{
    activator = to_lower(activator);
    switchstr(activator)
    {
        casestr("none")        : return activator_e::none;
        casestr("full_press")  : return activator_e::full_press;
        casestr("soft_press")  : return activator_e::soft_press;
        casestr("start_press") : return activator_e::start_press;
        casestr("release")     : return activator_e::release;
        casestr("long_press")  : return activator_e::long_press;
        casestr("double_press"): return activator_e::double_press;
        casestr("analog")      : return activator_e::analog;
        casestr("chord")       : return activator_e::chord;
    }

    std::string error("Unknown activator ");
    error += activator;
    fatal_throw(error.c_str());
    return activator_e::none;
}

// Init and Shutdown must be called when starting/ending use of this interface
bool Steam_Controller::Init(const char* pchAbsolutePathToControllerConfigVDF)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%s", pchAbsolutePathToControllerConfigVDF);
    return Init();
}

bool Steam_Controller::Init()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (!_initialized)
    {
        nlohmann::json json;

        std::string config_path = FileManager::canonical_path("controller_config.json");
        FileManager::load_json("controller_config.json", json);

        try
        {
            auto& analog_actions = json["actions"]["analog"];
            for (auto analog_it = analog_actions.begin(); analog_it != analog_actions.end(); ++analog_it)
            {
                std::string action_name(analog_it.key());
                action_name = to_lower(action_name);

                auto& analog_obj = analog_it.value();

                auto& analog_action = _analog_bindings[action_name];
                analog_action.description = analog_obj["description"].get_ref<std::string const&>();
                analog_action.mode = str_to_SourceMode(analog_obj["mode"].get_ref<std::string const&>());
                analog_action.button = str_to_analog_button(analog_obj["button"]);
            }
        }
        catch (...)
        {
            APP_LOG(Log::LogLevel::ERR, "Failed to parse controller's analog config: %s", config_path.c_str());
        }

        try
        {
            auto& digital_actions = json["actions"]["digital"];
            for (auto digital_it = digital_actions.begin(); digital_it != digital_actions.end(); ++digital_it)
            {
                std::string action_name(digital_it.key());
                action_name = to_lower(action_name);

                auto& digital_obj = digital_it.value();

                auto& digital_action = _digital_bindings[action_name];
                digital_action.description = digital_obj["description"].get_ref<std::string const&>();
                digital_action.mode = str_to_SourceMode(digital_obj["mode"].get_ref<std::string const&>());
                digital_action.button = str_to_digital_button(digital_obj["button"]);
                digital_action.activator = str_to_activator(digital_obj["activator"]);
                try
                {
                    digital_action.threshold = digital_obj["threshold"];
                }
                catch(...)
                {
                    digital_action.threshold = 0.875f;
                }
            }
        }
        catch (...)
        {
            APP_LOG(Log::LogLevel::ERR, "Failed to parse controller's digital config: %s", config_path.c_str());
        }

        try
        {
            auto& actions_sets = json["actions_sets"];
            for (auto actions_set_it = actions_sets.begin(); actions_set_it != actions_sets.end(); ++actions_set_it)
            {
                auto& actions_set = _actions_sets[actions_set_it.key()];
                auto& actions_set_obj = actions_set_it.value();

                actions_set.title = actions_set_obj["title"].get_ref<std::string const&>();
                actions_set.legacy_set = actions_set_obj["legacy_set"].get_ref<std::string const&>() != "0";

                auto& analog_obj = actions_set_obj["actions"]["analog"];
                for (auto analog_it = analog_obj.begin(); analog_it != analog_obj.end(); ++analog_it)
                {
                    std::string action_name = analog_it.key();
                    action_name = to_lower(action_name);

                    auto it = _analog_bindings.find(action_name);
                    if (it != _analog_bindings.end())
                    {
                        actions_set.actions[action_name].analog_binding = &(it->second);
                        actions_set.actions[action_name].title = analog_it.value().get_ref<std::string const&>();
                    }
                }

                auto& digital_obj = actions_set_obj["actions"]["digital"];
                for (auto digital_it = digital_obj.begin(); digital_it != digital_obj.end(); ++digital_it)
                {
                    std::string action_name = digital_it.key();
                    action_name = to_lower(action_name);

                    auto it = _digital_bindings.find(action_name);
                    if (it != _digital_bindings.end())
                    {
                        actions_set.actions[action_name].digital_binding = &(it->second);
                        actions_set.actions[action_name].title = digital_it.value().get_ref<std::string const&>();
                    }
                }
            }
        }
        catch (...)
        {
            APP_LOG(Log::LogLevel::ERR, "Failed to parse controller's action set config: %s", config_path.c_str());
        }

        if (_actions_sets.empty())
            _actions_sets["default_action_set_because_theres_none"];

        typename decltype(_localizations)::iterator localization_it;

        try
        {
            auto& localizations = json["localizations"];
            for (auto it = localizations.begin(); it != localizations.end(); ++it)
            {
                std::string const& local_name = it.key();
                for (auto translation_it = it.value().begin(); translation_it != it.value().end(); ++translation_it)
                {
                    _localizations[local_name][translation_it.key()] = translation_it.value().get_ref<std::string const&>();
                }
            }

            localization_it = _localizations.find(Settings::Inst().language);
            if (localization_it == _localizations.end())
            {
                localization_it = _localizations.find("english");
                if (localization_it == _localizations.end())
                {
                    if (_localizations.empty())
                        _localizations["english"];

                    localization_it = _localizations.begin();
                }
            }
        }
        catch (...)
        {
            _localizations["english"];
            localization_it = _localizations.begin();
        }

        _current_localization = &(localization_it->second);

        _initialized = true;
    }
    return true;
}

bool Steam_Controller::Shutdown()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _initialized = false;

    return true;
}

// Synchronize API state with the latest Steam Controller inputs available. This
// is performed automatically by SteamAPI_RunCallbacks, but for the absolute lowest
// possible latency, you call this directly before reading controller state. This must
// be called from somewhere before GetConnectedControllers will return any handles
void Steam_Controller::RunFrame()
{
    //LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (!_initialized)
        return;

    auto &gamepads = Gamepad::get_gamepads(false);
    for (int i = 0; i < gamepads.max_size(); ++i)
        gamepads[i]->RunFrame();
}

// Get the state of the specified controller, returns false if that controller is not connected
bool Steam_Controller::GetControllerState(uint32 unControllerIndex, SteamControllerState001_t* pState)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized)
        return false;

    return false;
}

// Trigger a haptic pulse on the controller
void Steam_Controller::TriggerHapticPulse(uint32 unControllerIndex, ESteamControllerPad eTargetPad, unsigned short usDurationMicroSec)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized)
        return;
}

// Set the override mode which is used to choose to use different base/legacy bindings from your config file
void Steam_Controller::SetOverrideMode(const char* pchMode)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized)
        return;
}

// Enumerate currently connected controllers
// handlesOut should point to a STEAM_CONTROLLER_MAX_COUNT sized array of ControllerHandle_t handles
// Returns the number of handles written to handlesOut
int Steam_Controller::GetConnectedControllers(STEAM_OUT_ARRAY_COUNT(STEAM_CONTROLLER_MAX_COUNT, Receives list of connected controllers) ControllerHandle_t* handlesOut)
{
    //LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (!_initialized)
        return 0;

    auto& gamepads = Gamepad::get_gamepads(true);

    int count = 0;
    // Should check for STEAM_CONTROLLER_MAX_COUNT, but how convenient the gamepad library also has 16 controllers max
    for (size_t i = 0; i < gamepads.max_size(); ++i)
    {
        if (gamepads[i]->Enabled())
        {
            handlesOut[count++] = static_cast<ControllerHandle_t>(i+1);
            if (_controller_handle_actions_set.find(i+1) == _controller_handle_actions_set.end())
            {// When initializing, set a default actions_set
                std::string const& actions_set_name = _actions_sets.begin()->first;

                auto handle = GetActionSetHandle(actions_set_name.c_str());
                ActivateActionSet(i+1, handle);
            }
        }
    }

    return count;
}

//-----------------------------------------------------------------------------
// ACTION SETS
//-----------------------------------------------------------------------------

// Lookup the handle for an Action Set. Best to do this once on startup, and store the handles for all future API calls.
ControllerActionSetHandle_t Steam_Controller::GetActionSetHandle(const char* pszActionSetName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%s", pszActionSetName);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (!_initialized)
        return -1;

    for (auto it = _actions_set_handle.begin(); it != _actions_set_handle.end(); ++it)
    {
        if (it->second.first == pszActionSetName)
            return it->first;
    }

    auto it = _actions_sets.find(pszActionSetName);
    if (it == _actions_sets.end())
    {
        APP_LOG(Log::LogLevel::WARN, "Actions Set %s not found", pszActionSetName);
        return 0;
    }

    auto& actions_set = _actions_set_handle[_actionset_handle];

    actions_set.first = pszActionSetName;
    actions_set.second = &(it->second);

    return _actionset_handle++;
}

// Reconfigure the controller to use the specified action set (ie 'Menu', 'Walk' or 'Drive')
// This is cheap, and can be safely called repeatedly. It's often easier to repeatedly call it in
// your state loops, instead of trying to place it in all of your state transitions.
void Steam_Controller::ActivateActionSet(ControllerHandle_t controllerHandle, ControllerActionSetHandle_t actionSetHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return;
    
    _controller_handle_actions_set[controllerHandle] = actionSetHandle;
}

ControllerActionSetHandle_t Steam_Controller::GetCurrentActionSet(ControllerHandle_t controllerHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return -1;
    
    return _controller_handle_actions_set[controllerHandle];
}

// ACTION SET LAYERS
void Steam_Controller::ActivateActionSetLayer(ControllerHandle_t controllerHandle, ControllerActionSetHandle_t actionSetLayerHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return;
}

void Steam_Controller::DeactivateActionSetLayer(ControllerHandle_t controllerHandle, ControllerActionSetHandle_t actionSetLayerHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return;
}

void Steam_Controller::DeactivateAllActionSetLayers(ControllerHandle_t controllerHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return;
}
// Enumerate currently active layers
// handlesOut should point to a STEAM_CONTROLLER_MAX_ACTIVE_LAYERS sized array of ControllerActionSetHandle_t handles.
// Returns the number of handles written to handlesOut
int Steam_Controller::GetActiveActionSetLayers(ControllerHandle_t controllerHandle, STEAM_OUT_ARRAY_COUNT(STEAM_CONTROLLER_MAX_ACTIVE_LAYERS, Receives list of active layers) ControllerActionSetHandle_t* handlesOut)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return 0;

    return 0;
}

//-----------------------------------------------------------------------------
// ACTIONS
//-----------------------------------------------------------------------------

// Lookup the handle for a digital action. Best to do this once on startup, and store the handles for all future API calls.
ControllerDigitalActionHandle_t Steam_Controller::GetDigitalActionHandle(const char* pszActionName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (!_initialized)
        return -1;

    std::string action_name(pszActionName);
    action_name = to_lower(action_name);

    for (auto& handle : _digital_handles)
    {
        if (handle.second.first == action_name)
            return handle.first;
    }

    auto it = _digital_bindings.find(action_name);
    if (it == _digital_bindings.end())
    {
        APP_LOG(Log::LogLevel::WARN, "Digital Action %s not found", pszActionName);
        return 0;
    }

    auto& digital_handle = _digital_handles[_digitalaction_handle];

    digital_handle.first = action_name;
    digital_handle.second = &(it->second);

    return _digitalaction_handle++;
}

// Returns the current state of the supplied digital game action
ControllerDigitalActionData_t Steam_Controller::GetDigitalActionData(ControllerHandle_t controllerHandle, ControllerDigitalActionHandle_t digitalActionHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    ControllerDigitalActionData_t data = {};
    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return data;

    if (digitalActionHandle == 0)
        return data;

    auto& gp = *Gamepad::get_gamepads(false)[controllerHandle-1];

    auto& digital_action = _digital_handles[digitalActionHandle];

    switch (digital_action.second->button)
    {
        case digital_button_e::dpad_north               : data.bState = gp.up; break;
        case digital_button_e::dpad_north_east          : data.bState = (gp.up && gp.right); break;
        case digital_button_e::dpad_east                : data.bState = gp.right; break;
        case digital_button_e::dpad_south_east          : data.bState = (gp.down && gp.right); break;
        case digital_button_e::dpad_south               : data.bState = gp.down; break;
        case digital_button_e::dpad_south_west          : data.bState = (gp.down && gp.left); break;
        case digital_button_e::dpad_west                : data.bState = gp.left; break;
        case digital_button_e::dpad_north_west          : data.bState = (gp.up && gp.left); break;
        case digital_button_e::button_y                 : data.bState = gp.y; break;
        case digital_button_e::button_b                 : data.bState = gp.b; break;
        case digital_button_e::button_x                 : data.bState = gp.x; break;
        case digital_button_e::button_a                 : data.bState = gp.a; break;
        case digital_button_e::button_menu              : data.bState = gp.back; break;
        case digital_button_e::button_escape            : data.bState = gp.start; break;
        case digital_button_e::left_bumper              : data.bState = gp.left_shoulder; break;
        case digital_button_e::right_bumper             : data.bState = gp.right_shoulder; break;
        //case digital_button_e::left_click               : 
        case digital_button_e::button_lpad              : data.bState = gp.left_thumb; break;
        //case digital_button_e::right_click              : 
        case digital_button_e::button_rpad              : data.bState = gp.right_thumb; break;
        //case digital_button_e::left_trigger             : 
        case digital_button_e::button_ltrigger          : data.bState = gp.left_trigger >= digital_action.second->threshold; break;
        //case digital_button_e::right_trigger            : 
        case digital_button_e::button_rtrigger          : data.bState = gp.right_trigger >= digital_action.second->threshold; break;
        //case digital_button_e::left_stick_click         : 
        case digital_button_e::button_joystick          : data.bState = gp.left_thumb; break;
        case digital_button_e::button_steam             : data.bState = gp.guide; break;
        default: data.bState = false; break;
    }

    auto& actions_set_actions = _actions_set_handle[GetCurrentActionSet(controllerHandle)].second->actions;

    data.bActive = actions_set_actions.find(digital_action.first) != actions_set_actions.end();

    return data;
}

// Get the origin(s) for a digital action within an action set. Returns the number of origins supplied in originsOut. Use this to display the appropriate on-screen prompt for the action.
// originsOut should point to a STEAM_CONTROLLER_MAX_ORIGINS sized array of EControllerActionOrigin handles. The EControllerActionOrigin enum will get extended as support for new controller controllers gets added to
// the Steam client and will exceed the values from this header, please check bounds if you are using a look up table.
int Steam_Controller::GetDigitalActionOrigins(ControllerHandle_t controllerHandle, ControllerActionSetHandle_t actionSetHandle, ControllerDigitalActionHandle_t digitalActionHandle, STEAM_OUT_ARRAY_COUNT(STEAM_CONTROLLER_MAX_ORIGINS, Receives list of aciton origins) EControllerActionOrigin* originsOut)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return 0;

    return 0;
}

int Steam_Controller::GetDigitalActionOrigins(InputHandle_t inputHandle, InputActionSetHandle_t actionSetHandle, InputDigitalActionHandle_t digitalActionHandle, STEAM_OUT_ARRAY_COUNT(STEAM_INPUT_MAX_ORIGINS, Receives list of action origins) EInputActionOrigin* originsOut)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized || inputHandle == 0 || inputHandle > STEAM_CONTROLLER_MAX_COUNT)
        return 0;

    return 0;
}

// Lookup the handle for an analog action. Best to do this once on startup, and store the handles for all future API calls.
ControllerAnalogActionHandle_t Steam_Controller::GetAnalogActionHandle(const char* pszActionName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (!_initialized)
        return -1;

    std::string action_name(pszActionName);
    action_name = to_lower(action_name);

    for (auto& handle : _analog_handles)
    {
        if (handle.second.first == action_name)
            return handle.first;
    }

    auto it = _analog_bindings.find(action_name);
    if (it == _analog_bindings.end())
    {
        APP_LOG(Log::LogLevel::WARN, "Analog action %s not found", pszActionName);
        return 0;
    }

    auto& analog_handle = _analog_handles[_analogaction_handle];

    analog_handle.first = action_name;
    analog_handle.second = &(it->second);

    return _analogaction_handle++;
}

// Returns the current state of these supplied analog game action
ControllerAnalogActionData_t Steam_Controller::GetAnalogActionData(ControllerHandle_t controllerHandle, ControllerAnalogActionHandle_t analogActionHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    ControllerAnalogActionData_t data = {};

    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return data;

    if (analogActionHandle == 0)
        return data;

    auto& gp = *Gamepad::get_gamepads(false)[controllerHandle-1];

    auto& analog_action = _analog_handles[analogActionHandle];

    switch (analog_action.second->button)
    {
        case k_EInputSource_Joystick:
            data.x = gp.left_stick.x;
            data.y = gp.left_stick.y;
            data.eMode = analog_action.second->mode;
            break;

        case k_EInputSource_RightJoystick:
            data.x = gp.right_stick.x;
            data.y = gp.right_stick.y;
            data.eMode = analog_action.second->mode;
            break;
    }

    auto& actions_set_actions = _actions_set_handle[GetCurrentActionSet(controllerHandle)].second->actions;

    data.bActive = actions_set_actions.find(analog_action.first) != actions_set_actions.end();

    return data;
}

// Get a local path to art for on-screen glyph for a particular origin
const char* Steam_Controller::GetGlyphForActionOrigin(EInputActionOrigin eOrigin)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    switch (eOrigin)
    {
        // Steam Controller
        case k_EInputActionOrigin_SteamController_A                  : return "button_a.png";
        case k_EInputActionOrigin_SteamController_B                  : return "button_b.png";
        case k_EInputActionOrigin_SteamController_X                  : return "button_x.png";
        case k_EInputActionOrigin_SteamController_Y                  : return "button_y.png";
        case k_EInputActionOrigin_SteamController_LeftBumper         : return "shoulder_l.png";
        case k_EInputActionOrigin_SteamController_RightBumper        : return "shoulder_r.png";
        case k_EInputActionOrigin_SteamController_LeftGrip           : return "grip_l.png";
        case k_EInputActionOrigin_SteamController_RightGrip          : return "grip_r.png";
        case k_EInputActionOrigin_SteamController_Start              : return "button_start.png";
        case k_EInputActionOrigin_SteamController_Back               : return "button_select.png";
        case k_EInputActionOrigin_SteamController_LeftPad_Touch      : return "pad_l_touch.png";
        case k_EInputActionOrigin_SteamController_LeftPad_Swipe      : return "pad_l_swipe.png";
        case k_EInputActionOrigin_SteamController_LeftPad_Click      : return "pad_l_click.png";
        case k_EInputActionOrigin_SteamController_LeftPad_DPadNorth  : return "pad_l_dpad_n.png";
        case k_EInputActionOrigin_SteamController_LeftPad_DPadSouth  : return "pad_l_dpad_s.png";
        case k_EInputActionOrigin_SteamController_LeftPad_DPadWest   : return "pad_l_dpad_w.png";
        case k_EInputActionOrigin_SteamController_LeftPad_DPadEast   : return "pad_l_dpad_e.png";
        case k_EInputActionOrigin_SteamController_RightPad_Touch     : return "pad_r_touch.png";
        case k_EInputActionOrigin_SteamController_RightPad_Swipe     : return "pad_r_swipe.png";
        case k_EInputActionOrigin_SteamController_RightPad_Click     : return "pad_r_click.png";
        case k_EInputActionOrigin_SteamController_RightPad_DPadNorth : return "pad_r_dpad_n.png";
        case k_EInputActionOrigin_SteamController_RightPad_DPadSouth : return "pad_r_dpad_s.png";
        case k_EInputActionOrigin_SteamController_RightPad_DPadWest  : return "pad_r_dpad_w.png";
        case k_EInputActionOrigin_SteamController_RightPad_DPadEast  : return "pad_r_dpad_e.png";
        case k_EInputActionOrigin_SteamController_LeftTrigger_Pull   : return "trigger_l_pull.png";
        case k_EInputActionOrigin_SteamController_LeftTrigger_Click  : return "trigger_l_click.png";
        case k_EInputActionOrigin_SteamController_RightTrigger_Pull  : return "trigger_r_pull.png";
        case k_EInputActionOrigin_SteamController_RightTrigger_Click : return "trigger_r_click.png";
        case k_EInputActionOrigin_SteamController_LeftStick_Move     : return "stick_move.png";
        case k_EInputActionOrigin_SteamController_LeftStick_Click    : return "stick_click.png";
        case k_EInputActionOrigin_SteamController_LeftStick_DPadNorth: return "stick_dpad_n.png";
        case k_EInputActionOrigin_SteamController_LeftStick_DPadSouth: return "stick_dpad_s.png";
        case k_EInputActionOrigin_SteamController_LeftStick_DPadWest : return "stick_dpad_w.png";
        case k_EInputActionOrigin_SteamController_LeftStick_DPadEast : return "stick_dpad_e.png";
        case k_EInputActionOrigin_SteamController_Gyro_Move          : return "gyro.png";
        case k_EInputActionOrigin_SteamController_Gyro_Pitch         : return "gyro.png";
        case k_EInputActionOrigin_SteamController_Gyro_Yaw           : return "gyro.png";
        case k_EInputActionOrigin_SteamController_Gyro_Roll          : return "gyro.png";
        // PS4 Dual Shock
        case k_EInputActionOrigin_PS4_X                              : return "ps4_button_x.png";
        case k_EInputActionOrigin_PS4_Circle                         : return "ps4_button_circle.png";
        case k_EInputActionOrigin_PS4_Triangle                       : return "ps4_button_triangle.png";
        case k_EInputActionOrigin_PS4_Square                         : return "ps4_button_square.png";
        case k_EInputActionOrigin_PS4_LeftBumper                     : return "shoulder_l.png";
        case k_EInputActionOrigin_PS4_RightBumper                    : return "shoulder_r.png";
        case k_EInputActionOrigin_PS4_Options                        : return "ps4_button_options.png";
        case k_EInputActionOrigin_PS4_Share                          : return "ps4_button_share.png";
        case k_EInputActionOrigin_PS4_LeftPad_Touch                  : return "ps4_pad_l_touch.png";
        case k_EInputActionOrigin_PS4_LeftPad_Swipe                  : return "ps4_pad_l_swipe.png";
        case k_EInputActionOrigin_PS4_LeftPad_Click                  : return "ps4_pad_l_click.png";
        case k_EInputActionOrigin_PS4_LeftPad_DPadNorth              : return "ps4_pad_l_dpad_n.png";
        case k_EInputActionOrigin_PS4_LeftPad_DPadSouth              : return "ps4_pad_l_dpad_s.png";
        case k_EInputActionOrigin_PS4_LeftPad_DPadWest               : return "ps4_pad_l_dpad_w.png";
        case k_EInputActionOrigin_PS4_LeftPad_DPadEast               : return "ps4_pad_l_dpad_e.png";
        case k_EInputActionOrigin_PS4_RightPad_Touch                 : return "ps4_pad_r_touch.png";
        case k_EInputActionOrigin_PS4_RightPad_Swipe                 : return "ps4_pad_r_swipe.png";
        case k_EInputActionOrigin_PS4_RightPad_Click                 : return "ps4_pad_r_click.png";
        case k_EInputActionOrigin_PS4_RightPad_DPadNorth             : return "ps4_pad_r_dpad_n.png";
        case k_EInputActionOrigin_PS4_RightPad_DPadSouth             : return "ps4_pad_r_dpad_s.png";
        case k_EInputActionOrigin_PS4_RightPad_DPadWest              : return "ps4_pad_r_dpad_w.png";
        case k_EInputActionOrigin_PS4_RightPad_DPadEast              : return "ps4_pad_r_dpad_e.png";
        case k_EInputActionOrigin_PS4_CenterPad_Touch                : return "ps4_pad_center_touch.png";
        case k_EInputActionOrigin_PS4_CenterPad_Swipe                : return "ps4_pad_center_swipe.png";
        case k_EInputActionOrigin_PS4_CenterPad_Click                : return "ps4_pad_center_click.png";
        case k_EInputActionOrigin_PS4_CenterPad_DPadNorth            : return "ps4_pad_center_dpad_n.png";
        case k_EInputActionOrigin_PS4_CenterPad_DPadSouth            : return "ps4_pad_center_dpad_s.png";
        case k_EInputActionOrigin_PS4_CenterPad_DPadWest             : return "ps4_pad_center_dpad_w.png";
        case k_EInputActionOrigin_PS4_CenterPad_DPadEast             : return "ps4_pad_center_dpad_e.png";
        case k_EInputActionOrigin_PS4_LeftTrigger_Pull               : return "trigger_l_pull.png";
        case k_EInputActionOrigin_PS4_LeftTrigger_Click              : return "trigger_l_click.png";
        case k_EInputActionOrigin_PS4_RightTrigger_Pull              : return "trigger_r_pull.png";
        case k_EInputActionOrigin_PS4_RightTrigger_Click             : return "trigger_r_click.png";
        case k_EInputActionOrigin_PS4_LeftStick_Move                 : return "ps4_stick_l_move.png";
        case k_EInputActionOrigin_PS4_LeftStick_Click                : return "ps4_stick_l_click.png";
        case k_EInputActionOrigin_PS4_LeftStick_DPadNorth            : return "stick_dpad_n.png";
        case k_EInputActionOrigin_PS4_LeftStick_DPadSouth            : return "stick_dpad_s.png";
        case k_EInputActionOrigin_PS4_LeftStick_DPadWest             : return "stick_dpad_w.png";
        case k_EInputActionOrigin_PS4_LeftStick_DPadEast             : return "stick_dpad_e.png";
        case k_EInputActionOrigin_PS4_RightStick_Move                : return "ps4_stick_r_move.png";
        case k_EInputActionOrigin_PS4_RightStick_Click               : return "ps4_stick_r_click.png";
        case k_EInputActionOrigin_PS4_RightStick_DPadNorth           : return "stick_dpad_n.png";
        case k_EInputActionOrigin_PS4_RightStick_DPadSouth           : return "stick_dpad_s.png";
        case k_EInputActionOrigin_PS4_RightStick_DPadWest            : return "stick_dpad_w.png";
        case k_EInputActionOrigin_PS4_RightStick_DPadEast            : return "stick_dpad_e.png";
        case k_EInputActionOrigin_PS4_DPad_North                     : return "ps4_button_dpad_n.png";
        case k_EInputActionOrigin_PS4_DPad_South                     : return "ps4_button_dpad_s.png";
        case k_EInputActionOrigin_PS4_DPad_West                      : return "ps4_button_dpad_w.png";
        case k_EInputActionOrigin_PS4_DPad_East                      : return "ps4_button_dpad_e.png";
        case k_EInputActionOrigin_PS4_Gyro_Move                      : return "gyro.png";
        case k_EInputActionOrigin_PS4_Gyro_Pitch                     : return "gyro.png";
        case k_EInputActionOrigin_PS4_Gyro_Yaw                       : return "gyro.png";
        case k_EInputActionOrigin_PS4_Gyro_Roll                      : return "gyro.png";
        case k_EInputActionOrigin_PS4_DPad_Move                      : return "ps4_button_dpad_move.png";
        // XBox One
        case k_EInputActionOrigin_XBoxOne_A                          : return "button_a.png";
        case k_EInputActionOrigin_XBoxOne_B                          : return "button_b.png";
        case k_EInputActionOrigin_XBoxOne_X                          : return "button_x.png";
        case k_EInputActionOrigin_XBoxOne_Y                          : return "button_y.png";
        case k_EInputActionOrigin_XBoxOne_LeftBumper                 : return "shoulder_l.png";
        case k_EInputActionOrigin_XBoxOne_RightBumper                : return "shoulder_r.png";
        case k_EInputActionOrigin_XBoxOne_Menu                       : return "xbox_button_start.png";
        case k_EInputActionOrigin_XBoxOne_View                       : return "xbox_button_select.png";
        case k_EInputActionOrigin_XBoxOne_LeftTrigger_Pull           : return "trigger_l_pull.png";
        case k_EInputActionOrigin_XBoxOne_LeftTrigger_Click          : return "trigger_l_click.png";
        case k_EInputActionOrigin_XBoxOne_RightTrigger_Pull          : return "trigger_r_pull.png";
        case k_EInputActionOrigin_XBoxOne_RightTrigger_Click         : return "trigger_r_click.png";
        case k_EInputActionOrigin_XBoxOne_LeftStick_Move             : return "stick_l_move.png";
        case k_EInputActionOrigin_XBoxOne_LeftStick_Click            : return "stick_l_click.png";
        case k_EInputActionOrigin_XBoxOne_LeftStick_DPadNorth        : return "stick_dpad_n.png";
        case k_EInputActionOrigin_XBoxOne_LeftStick_DPadSouth        : return "stick_dpad_s.png";
        case k_EInputActionOrigin_XBoxOne_LeftStick_DPadWest         : return "stick_dpad_w.png";
        case k_EInputActionOrigin_XBoxOne_LeftStick_DPadEast         : return "stick_dpad_e.png";
        case k_EInputActionOrigin_XBoxOne_RightStick_Move            : return "stick_r_move.png";
        case k_EInputActionOrigin_XBoxOne_RightStick_Click           : return "stick_r_click.png";
        case k_EInputActionOrigin_XBoxOne_RightStick_DPadNorth       : return "stick_dpad_n.png";
        case k_EInputActionOrigin_XBoxOne_RightStick_DPadSouth       : return "stick_dpad_s.png";
        case k_EInputActionOrigin_XBoxOne_RightStick_DPadWest        : return "stick_dpad_w.png";
        case k_EInputActionOrigin_XBoxOne_RightStick_DPadEast        : return "stick_dpad_e.png";
        case k_EInputActionOrigin_XBoxOne_DPad_North                 : return "xbox_button_dpad_n.png";
        case k_EInputActionOrigin_XBoxOne_DPad_South                 : return "xbox_button_dpad_s.png";
        case k_EInputActionOrigin_XBoxOne_DPad_West                  : return "xbox_button_dpad_w.png";
        case k_EInputActionOrigin_XBoxOne_DPad_East                  : return "xbox_button_dpad_e.png";
        case k_EInputActionOrigin_XBoxOne_DPad_Move                  : return "xbox_button_dpad_move.png";
        // XBox 360
        case k_EInputActionOrigin_XBox360_A                          : return "button_a.png";
        case k_EInputActionOrigin_XBox360_B                          : return "button_b.png";
        case k_EInputActionOrigin_XBox360_X                          : return "button_x.png";
        case k_EInputActionOrigin_XBox360_Y                          : return "button_y.png";
        case k_EInputActionOrigin_XBox360_LeftBumper                 : return "shoulder_l.png";
        case k_EInputActionOrigin_XBox360_RightBumper                : return "shoulder_r.png";
        case k_EInputActionOrigin_XBox360_Start                      : return "xbox_button_start.png";
        case k_EInputActionOrigin_XBox360_Back                       : return "xbox_button_select.png";
        case k_EInputActionOrigin_XBox360_LeftTrigger_Pull           : return "trigger_l_pull.png";
        case k_EInputActionOrigin_XBox360_LeftTrigger_Click          : return "trigger_l_click.png";
        case k_EInputActionOrigin_XBox360_RightTrigger_Pull          : return "trigger_r_pull.png";
        case k_EInputActionOrigin_XBox360_RightTrigger_Click         : return "trigger_r_click.png";
        case k_EInputActionOrigin_XBox360_LeftStick_Move             : return "stick_l_move.png";
        case k_EInputActionOrigin_XBox360_LeftStick_Click            : return "stick_l_click.png";
        case k_EInputActionOrigin_XBox360_LeftStick_DPadNorth        : return "stick_dpad_n.png";
        case k_EInputActionOrigin_XBox360_LeftStick_DPadSouth        : return "stick_dpad_s.png";
        case k_EInputActionOrigin_XBox360_LeftStick_DPadWest         : return "stick_dpad_w.png";
        case k_EInputActionOrigin_XBox360_LeftStick_DPadEast         : return "stick_dpad_e.png";
        case k_EInputActionOrigin_XBox360_RightStick_Move            : return "stick_r_move.png";
        case k_EInputActionOrigin_XBox360_RightStick_Click           : return "stick_r_click.png";
        case k_EInputActionOrigin_XBox360_RightStick_DPadNorth       : return "stick_dpad_n.png";
        case k_EInputActionOrigin_XBox360_RightStick_DPadSouth       : return "stick_dpad_s.png";
        case k_EInputActionOrigin_XBox360_RightStick_DPadWest        : return "stick_dpad_w.png";
        case k_EInputActionOrigin_XBox360_RightStick_DPadEast        : return "stick_dpad_e.png";
        case k_EInputActionOrigin_XBox360_DPad_North                 : return "xbox_button_dpad_n.png";
        case k_EInputActionOrigin_XBox360_DPad_South                 : return "xbox_button_dpad_s.png";
        case k_EInputActionOrigin_XBox360_DPad_West                  : return "xbox_button_dpad_w.png";
        case k_EInputActionOrigin_XBox360_DPad_East                  : return "xbox_button_dpad_e.png";
        case k_EInputActionOrigin_XBox360_DPad_Move                  : return "xbox_button_dpad_move.png";
        // Switch - Pro or Joycons used as a single input device.
        // This does not apply to a single joycon
        case k_EInputActionOrigin_Switch_A                           : return "switch_button_a.png";
        case k_EInputActionOrigin_Switch_B                           : return "switch_button_b.png";
        case k_EInputActionOrigin_Switch_X                           : return "switch_button_x.png";
        case k_EInputActionOrigin_Switch_Y                           : return "switch_button_y.png";
        case k_EInputActionOrigin_Switch_LeftBumper                  : return "shoulder_l.png";
        case k_EInputActionOrigin_Switch_RightBumper                 : return "shoulder_r.png";
        case k_EInputActionOrigin_Switch_Plus                        : return "switch_button_plus.png";
        case k_EInputActionOrigin_Switch_Minus                       : return "switch_button_minus.png";
        case k_EInputActionOrigin_Switch_Capture                     : return "switch_button_capture.png";
        case k_EInputActionOrigin_Switch_LeftTrigger_Pull            : return "trigger_l_pull.png";
        case k_EInputActionOrigin_Switch_LeftTrigger_Click           : return "trigger_l_click.png";
        case k_EInputActionOrigin_Switch_RightTrigger_Pull           : return "trigger_r_pull.png";
        case k_EInputActionOrigin_Switch_RightTrigger_Click          : return "trigger_r_click.png";
        case k_EInputActionOrigin_Switch_LeftStick_Move              : return "stick_l_move.png";
        case k_EInputActionOrigin_Switch_LeftStick_Click             : return "stick_l_click.png";
        case k_EInputActionOrigin_Switch_LeftStick_DPadNorth         : return "stick_dpad_n.png";
        case k_EInputActionOrigin_Switch_LeftStick_DPadSouth         : return "stick_dpad_s.png";
        case k_EInputActionOrigin_Switch_LeftStick_DPadWest          : return "stick_dpad_w.png";
        case k_EInputActionOrigin_Switch_LeftStick_DPadEast          : return "stick_dpad_e.png";
        case k_EInputActionOrigin_Switch_RightStick_Move             : return "stick_r_move.png";
        case k_EInputActionOrigin_Switch_RightStick_Click            : return "stick_r_click.png";
        case k_EInputActionOrigin_Switch_RightStick_DPadNorth        : return "stick_dpad_n.png";
        case k_EInputActionOrigin_Switch_RightStick_DPadSouth        : return "stick_dpad_s.png";
        case k_EInputActionOrigin_Switch_RightStick_DPadWest         : return "stick_dpad_w.png";
        case k_EInputActionOrigin_Switch_RightStick_DPadEast         : return "stick_dpad_e.png";
        case k_EInputActionOrigin_Switch_DPad_North                  : return "switch_button_dpad_n.png";
        case k_EInputActionOrigin_Switch_DPad_South                  : return "switch_button_dpad_s.png";
        case k_EInputActionOrigin_Switch_DPad_West                   : return "switch_button_dpad_w.png";
        case k_EInputActionOrigin_Switch_DPad_East                   : return "switch_button_dpad_e.png";
        case k_EInputActionOrigin_Switch_ProGyro_Move                : return "gyro.png";
        case k_EInputActionOrigin_Switch_ProGyro_Pitch               : return "gyro.png";
        case k_EInputActionOrigin_Switch_ProGyro_Yaw                 : return "gyro.png";
        case k_EInputActionOrigin_Switch_ProGyro_Roll                : return "gyro.png";
        case k_EInputActionOrigin_Switch_DPad_Move                   : return "switch_button_dpad_move.png";
        // Switch JoyCon Specific
        case k_EInputActionOrigin_Switch_RightGyro_Move              : return "gyro.png";
        case k_EInputActionOrigin_Switch_RightGyro_Pitch             : return "gyro.png";
        case k_EInputActionOrigin_Switch_RightGyro_Yaw               : return "gyro.png";
        case k_EInputActionOrigin_Switch_RightGyro_Roll              : return "gyro.png";
        case k_EInputActionOrigin_Switch_LeftGyro_Move               : return "gyro.png";
        case k_EInputActionOrigin_Switch_LeftGyro_Pitch              : return "gyro.png";
        case k_EInputActionOrigin_Switch_LeftGyro_Yaw                : return "gyro.png";
        case k_EInputActionOrigin_Switch_LeftGyro_Roll               : return "gyro.png";
        case k_EInputActionOrigin_Switch_LeftGrip_Lower              : return "grip_l.png";
        case k_EInputActionOrigin_Switch_LeftGrip_Upper              : return "grip_l.png";
        case k_EInputActionOrigin_Switch_RightGrip_Lower             : return "grip_r.png";
        case k_EInputActionOrigin_Switch_RightGrip_Upper             : return "grip_r.png";

        default: 
            APP_LOG(Log::LogLevel::WARN, "Unknown Origin %d", eOrigin);
            return "";
    }
}

// Returns a localized string (from Steam's language setting) for the specified origin.
const char* Steam_Controller::GetStringForActionOrigin(EInputActionOrigin eOrigin)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    switch (eOrigin)
    {
        // Steam Controller
        case k_EInputActionOrigin_SteamController_A                  : return "#origin_steamcontroller_button_a";
        case k_EInputActionOrigin_SteamController_B                  : return "#origin_steamcontroller_button_b";
        case k_EInputActionOrigin_SteamController_X                  : return "#origin_steamcontroller_button_x";
        case k_EInputActionOrigin_SteamController_Y                  : return "#origin_steamcontroller_button_y";
        case k_EInputActionOrigin_SteamController_LeftBumper         : return "#origin_steamcontroller_shoulder_l";
        case k_EInputActionOrigin_SteamController_RightBumper        : return "#origin_steamcontroller_shoulder_r";
        case k_EInputActionOrigin_SteamController_LeftGrip           : return "#origin_steamcontroller_grip_l";
        case k_EInputActionOrigin_SteamController_RightGrip          : return "#origin_steamcontroller_grip_r";
        case k_EInputActionOrigin_SteamController_Start              : return "#origin_steamcontroller_button_start";
        case k_EInputActionOrigin_SteamController_Back               : return "#origin_steamcontroller_button_select";
        case k_EInputActionOrigin_SteamController_LeftPad_Touch      : return "#origin_steamcontroller_pad_l_touch";
        case k_EInputActionOrigin_SteamController_LeftPad_Swipe      : return "#origin_steamcontroller_pad_l_swipe";
        case k_EInputActionOrigin_SteamController_LeftPad_Click      : return "#origin_steamcontroller_pad_l_click";
        case k_EInputActionOrigin_SteamController_LeftPad_DPadNorth  : return "#origin_steamcontroller_pad_l_dpad_n";
        case k_EInputActionOrigin_SteamController_LeftPad_DPadSouth  : return "#origin_steamcontroller_pad_l_dpad_s";
        case k_EInputActionOrigin_SteamController_LeftPad_DPadWest   : return "#origin_steamcontroller_pad_l_dpad_w";
        case k_EInputActionOrigin_SteamController_LeftPad_DPadEast   : return "#origin_steamcontroller_pad_l_dpad_e";
        case k_EInputActionOrigin_SteamController_RightPad_Touch     : return "#origin_steamcontroller_pad_r_touch";
        case k_EInputActionOrigin_SteamController_RightPad_Swipe     : return "#origin_steamcontroller_pad_r_swipe";
        case k_EInputActionOrigin_SteamController_RightPad_Click     : return "#origin_steamcontroller_pad_r_click";
        case k_EInputActionOrigin_SteamController_RightPad_DPadNorth : return "#origin_steamcontroller_pad_r_dpad_n";
        case k_EInputActionOrigin_SteamController_RightPad_DPadSouth : return "#origin_steamcontroller_pad_r_dpad_s";
        case k_EInputActionOrigin_SteamController_RightPad_DPadWest  : return "#origin_steamcontroller_pad_r_dpad_w";
        case k_EInputActionOrigin_SteamController_RightPad_DPadEast  : return "#origin_steamcontroller_pad_r_dpad_e";
        case k_EInputActionOrigin_SteamController_LeftTrigger_Pull   : return "#origin_steamcontroller_trigger_l_pull";
        case k_EInputActionOrigin_SteamController_LeftTrigger_Click  : return "#origin_steamcontroller_trigger_l_click";
        case k_EInputActionOrigin_SteamController_RightTrigger_Pull  : return "#origin_steamcontroller_trigger_r_pull";
        case k_EInputActionOrigin_SteamController_RightTrigger_Click : return "#origin_steamcontroller_trigger_r_click";
        case k_EInputActionOrigin_SteamController_LeftStick_Move     : return "#origin_steamcontroller_stick_move";
        case k_EInputActionOrigin_SteamController_LeftStick_Click    : return "#origin_steamcontroller_stick_click";
        case k_EInputActionOrigin_SteamController_LeftStick_DPadNorth: return "#origin_steamcontroller_stick_dpad_n";
        case k_EInputActionOrigin_SteamController_LeftStick_DPadSouth: return "#origin_steamcontroller_stick_dpad_s";
        case k_EInputActionOrigin_SteamController_LeftStick_DPadWest : return "#origin_steamcontroller_stick_dpad_w";
        case k_EInputActionOrigin_SteamController_LeftStick_DPadEast : return "#origin_steamcontroller_stick_dpad_e";
        case k_EInputActionOrigin_SteamController_Gyro_Move          : return "#origin_steamcontroller_gyro_move";
        case k_EInputActionOrigin_SteamController_Gyro_Pitch         : return "#origin_steamcontroller_gyro_pitch";
        case k_EInputActionOrigin_SteamController_Gyro_Yaw           : return "#origin_steamcontroller_gyro_yaw";
        case k_EInputActionOrigin_SteamController_Gyro_Roll          : return "#origin_steamcontroller_gyro_roll";
        // PS4 Dual Shock
        case k_EInputActionOrigin_PS4_X                              : return "#origin_ps4_button_x";
        case k_EInputActionOrigin_PS4_Circle                         : return "#origin_ps4_button_circle";
        case k_EInputActionOrigin_PS4_Triangle                       : return "#origin_ps4_button_triangle";
        case k_EInputActionOrigin_PS4_Square                         : return "#origin_ps4_button_square";
        case k_EInputActionOrigin_PS4_LeftBumper                     : return "#origin_ps4_shoulder_l";
        case k_EInputActionOrigin_PS4_RightBumper                    : return "#origin_ps4_shoulder_r";
        case k_EInputActionOrigin_PS4_Options                        : return "#origin_ps4_button_options";
        case k_EInputActionOrigin_PS4_Share                          : return "#origin_ps4_button_share";
        case k_EInputActionOrigin_PS4_LeftPad_Touch                  : return "#origin_ps4_pad_l_touch";
        case k_EInputActionOrigin_PS4_LeftPad_Swipe                  : return "#origin_ps4_pad_l_swipe";
        case k_EInputActionOrigin_PS4_LeftPad_Click                  : return "#origin_ps4_pad_l_click";
        case k_EInputActionOrigin_PS4_LeftPad_DPadNorth              : return "#origin_ps4_pad_l_dpad_n";
        case k_EInputActionOrigin_PS4_LeftPad_DPadSouth              : return "#origin_ps4_pad_l_dpad_s";
        case k_EInputActionOrigin_PS4_LeftPad_DPadWest               : return "#origin_ps4_pad_l_dpad_w";
        case k_EInputActionOrigin_PS4_LeftPad_DPadEast               : return "#origin_ps4_pad_l_dpad_e";
        case k_EInputActionOrigin_PS4_RightPad_Touch                 : return "#origin_ps4_pad_r_touch";
        case k_EInputActionOrigin_PS4_RightPad_Swipe                 : return "#origin_ps4_pad_r_swipe";
        case k_EInputActionOrigin_PS4_RightPad_Click                 : return "#origin_ps4_pad_r_click";
        case k_EInputActionOrigin_PS4_RightPad_DPadNorth             : return "#origin_ps4_pad_r_dpad_n";
        case k_EInputActionOrigin_PS4_RightPad_DPadSouth             : return "#origin_ps4_pad_r_dpad_s";
        case k_EInputActionOrigin_PS4_RightPad_DPadWest              : return "#origin_ps4_pad_r_dpad_w";
        case k_EInputActionOrigin_PS4_RightPad_DPadEast              : return "#origin_ps4_pad_r_dpad_e";
        case k_EInputActionOrigin_PS4_CenterPad_Touch                : return "#origin_ps4_pad_center_touch";
        case k_EInputActionOrigin_PS4_CenterPad_Swipe                : return "#origin_ps4_pad_center_swipe";
        case k_EInputActionOrigin_PS4_CenterPad_Click                : return "#origin_ps4_pad_center_click";
        case k_EInputActionOrigin_PS4_CenterPad_DPadNorth            : return "#origin_ps4_pad_center_dpad_n";
        case k_EInputActionOrigin_PS4_CenterPad_DPadSouth            : return "#origin_ps4_pad_center_dpad_s";
        case k_EInputActionOrigin_PS4_CenterPad_DPadWest             : return "#origin_ps4_pad_center_dpad_w";
        case k_EInputActionOrigin_PS4_CenterPad_DPadEast             : return "#origin_ps4_pad_center_dpad_e";
        case k_EInputActionOrigin_PS4_LeftTrigger_Pull               : return "#origin_ps4_trigger_l_pull";
        case k_EInputActionOrigin_PS4_LeftTrigger_Click              : return "#origin_ps4_trigger_l_click";
        case k_EInputActionOrigin_PS4_RightTrigger_Pull              : return "#origin_ps4_trigger_r_pull";
        case k_EInputActionOrigin_PS4_RightTrigger_Click             : return "#origin_ps4_trigger_r_click";
        case k_EInputActionOrigin_PS4_LeftStick_Move                 : return "#origin_ps4_stick_l_move";
        case k_EInputActionOrigin_PS4_LeftStick_Click                : return "#origin_ps4_stick_l_click";
        case k_EInputActionOrigin_PS4_LeftStick_DPadNorth            : return "#origin_ps4_stick_l_dpad_n";
        case k_EInputActionOrigin_PS4_LeftStick_DPadSouth            : return "#origin_ps4_stick_l_dpad_s";
        case k_EInputActionOrigin_PS4_LeftStick_DPadWest             : return "#origin_ps4_stick_l_dpad_w";
        case k_EInputActionOrigin_PS4_LeftStick_DPadEast             : return "#origin_ps4_stick_l_dpad_e";
        case k_EInputActionOrigin_PS4_RightStick_Move                : return "#origin_ps4_stick_r_move";
        case k_EInputActionOrigin_PS4_RightStick_Click               : return "#origin_ps4_stick_r_click";
        case k_EInputActionOrigin_PS4_RightStick_DPadNorth           : return "#origin_ps4_stick_r_dpad_n";
        case k_EInputActionOrigin_PS4_RightStick_DPadSouth           : return "#origin_ps4_stick_r_dpad_s";
        case k_EInputActionOrigin_PS4_RightStick_DPadWest            : return "#origin_ps4_stick_r_dpad_w";
        case k_EInputActionOrigin_PS4_RightStick_DPadEast            : return "#origin_ps4_stick_r_dpad_e";
        case k_EInputActionOrigin_PS4_DPad_North                     : return "#origin_ps4_button_dpad_n";
        case k_EInputActionOrigin_PS4_DPad_South                     : return "#origin_ps4_button_dpad_s";
        case k_EInputActionOrigin_PS4_DPad_West                      : return "#origin_ps4_button_dpad_w";
        case k_EInputActionOrigin_PS4_DPad_East                      : return "#origin_ps4_button_dpad_e";
        case k_EInputActionOrigin_PS4_Gyro_Move                      : return "#origin_ps4_gyro_move";
        case k_EInputActionOrigin_PS4_Gyro_Pitch                     : return "#origin_ps4_gyro_pitch";
        case k_EInputActionOrigin_PS4_Gyro_Yaw                       : return "#origin_ps4_gyro_yaw";
        case k_EInputActionOrigin_PS4_Gyro_Roll                      : return "#origin_ps4_gyro_roll";
        case k_EInputActionOrigin_PS4_DPad_Move                      : return "#origin_ps4_button_dpad_move";
        // XBox One
        case k_EInputActionOrigin_XBoxOne_A                          : return "#origin_xboxone_button_a";
        case k_EInputActionOrigin_XBoxOne_B                          : return "#origin_xboxone_button_b";
        case k_EInputActionOrigin_XBoxOne_X                          : return "#origin_xboxone_button_x";
        case k_EInputActionOrigin_XBoxOne_Y                          : return "#origin_xboxone_button_y";
        case k_EInputActionOrigin_XBoxOne_LeftBumper                 : return "#origin_xboxone_shoulder_l";
        case k_EInputActionOrigin_XBoxOne_RightBumper                : return "#origin_xboxone_shoulder_r";
        case k_EInputActionOrigin_XBoxOne_Menu                       : return "#origin_xboxone_button_menu";
        case k_EInputActionOrigin_XBoxOne_View                       : return "#origin_xboxone_button_view";
        case k_EInputActionOrigin_XBoxOne_LeftTrigger_Pull           : return "#origin_xboxone_trigger_l_pull";
        case k_EInputActionOrigin_XBoxOne_LeftTrigger_Click          : return "#origin_xboxone_trigger_l_click";
        case k_EInputActionOrigin_XBoxOne_RightTrigger_Pull          : return "#origin_xboxone_trigger_r_pull";
        case k_EInputActionOrigin_XBoxOne_RightTrigger_Click         : return "#origin_xboxone_trigger_r_click";
        case k_EInputActionOrigin_XBoxOne_LeftStick_Move             : return "#origin_xboxone_stick_l_move";
        case k_EInputActionOrigin_XBoxOne_LeftStick_Click            : return "#origin_xboxone_stick_l_click";
        case k_EInputActionOrigin_XBoxOne_LeftStick_DPadNorth        : return "#origin_xboxone_stick_l_dpad_n";
        case k_EInputActionOrigin_XBoxOne_LeftStick_DPadSouth        : return "#origin_xboxone_stick_l_dpad_s";
        case k_EInputActionOrigin_XBoxOne_LeftStick_DPadWest         : return "#origin_xboxone_stick_l_dpad_w";
        case k_EInputActionOrigin_XBoxOne_LeftStick_DPadEast         : return "#origin_xboxone_stick_l_dpad_e";
        case k_EInputActionOrigin_XBoxOne_RightStick_Move            : return "#origin_xboxone_stick_r_move";
        case k_EInputActionOrigin_XBoxOne_RightStick_Click           : return "#origin_xboxone_stick_r_click";
        case k_EInputActionOrigin_XBoxOne_RightStick_DPadNorth       : return "#origin_xboxone_stick_r_dpad_n";
        case k_EInputActionOrigin_XBoxOne_RightStick_DPadSouth       : return "#origin_xboxone_stick_r_dpad_s";
        case k_EInputActionOrigin_XBoxOne_RightStick_DPadWest        : return "#origin_xboxone_stick_r_dpad_w";
        case k_EInputActionOrigin_XBoxOne_RightStick_DPadEast        : return "#origin_xboxone_stick_r_dpad_e";
        case k_EInputActionOrigin_XBoxOne_DPad_North                 : return "#origin_xboxone_button_dpad_n";
        case k_EInputActionOrigin_XBoxOne_DPad_South                 : return "#origin_xboxone_button_dpad_s";
        case k_EInputActionOrigin_XBoxOne_DPad_West                  : return "#origin_xboxone_button_dpad_w";
        case k_EInputActionOrigin_XBoxOne_DPad_East                  : return "#origin_xboxone_button_dpad_e";
        case k_EInputActionOrigin_XBoxOne_DPad_Move                  : return "#origin_xboxone_button_dpad_move";
        // XBox 360
        case k_EInputActionOrigin_XBox360_A                          : return "#origin_xbox360_button_a";
        case k_EInputActionOrigin_XBox360_B                          : return "#origin_xbox360_button_b";
        case k_EInputActionOrigin_XBox360_X                          : return "#origin_xbox360_button_x";
        case k_EInputActionOrigin_XBox360_Y                          : return "#origin_xbox360_button_y";
        case k_EInputActionOrigin_XBox360_LeftBumper                 : return "#origin_xbox360_shoulder_l";
        case k_EInputActionOrigin_XBox360_RightBumper                : return "#origin_xbox360_shoulder_r";
        case k_EInputActionOrigin_XBox360_Start                      : return "#origin_xbox360_button_start";
        case k_EInputActionOrigin_XBox360_Back                       : return "#origin_xbox360_button_back";
        case k_EInputActionOrigin_XBox360_LeftTrigger_Pull           : return "#origin_xbox360_trigger_l_pull";
        case k_EInputActionOrigin_XBox360_LeftTrigger_Click          : return "#origin_xbox360_trigger_l_click";
        case k_EInputActionOrigin_XBox360_RightTrigger_Pull          : return "#origin_xbox360_trigger_r_pull";
        case k_EInputActionOrigin_XBox360_RightTrigger_Click         : return "#origin_xbox360_trigger_r_click";
        case k_EInputActionOrigin_XBox360_LeftStick_Move             : return "#origin_xbox360_stick_l_move";
        case k_EInputActionOrigin_XBox360_LeftStick_Click            : return "#origin_xbox360_stick_l_click";
        case k_EInputActionOrigin_XBox360_LeftStick_DPadNorth        : return "#origin_xbox360_stick_l_dpad_n";
        case k_EInputActionOrigin_XBox360_LeftStick_DPadSouth        : return "#origin_xbox360_stick_l_dpad_s";
        case k_EInputActionOrigin_XBox360_LeftStick_DPadWest         : return "#origin_xbox360_stick_l_dpad_w";
        case k_EInputActionOrigin_XBox360_LeftStick_DPadEast         : return "#origin_xbox360_stick_l_dpad_e";
        case k_EInputActionOrigin_XBox360_RightStick_Move            : return "#origin_xbox360_stick_r_move";
        case k_EInputActionOrigin_XBox360_RightStick_Click           : return "#origin_xbox360_stick_r_click";
        case k_EInputActionOrigin_XBox360_RightStick_DPadNorth       : return "#origin_xbox360_stick_r_dpad_n";
        case k_EInputActionOrigin_XBox360_RightStick_DPadSouth       : return "#origin_xbox360_stick_r_dpad_s";
        case k_EInputActionOrigin_XBox360_RightStick_DPadWest        : return "#origin_xbox360_stick_r_dpad_w";
        case k_EInputActionOrigin_XBox360_RightStick_DPadEast        : return "#origin_xbox360_stick_r_dpad_e";
        case k_EInputActionOrigin_XBox360_DPad_North                 : return "#origin_xbox360_button_dpad_n";
        case k_EInputActionOrigin_XBox360_DPad_South                 : return "#origin_xbox360_button_dpad_s";
        case k_EInputActionOrigin_XBox360_DPad_West                  : return "#origin_xbox360_button_dpad_w";
        case k_EInputActionOrigin_XBox360_DPad_East                  : return "#origin_xbox360_button_dpad_e";
        case k_EInputActionOrigin_XBox360_DPad_Move                  : return "#origin_xbox360_button_dpad_move";
        // Switch - Pro or Joycons used as a single input device.
        // This does not apply to a single joycon
        case k_EInputActionOrigin_Switch_A                           : return "#origin_switch_button_a";
        case k_EInputActionOrigin_Switch_B                           : return "#origin_switch_button_b";
        case k_EInputActionOrigin_Switch_X                           : return "#origin_switch_button_x";
        case k_EInputActionOrigin_Switch_Y                           : return "#origin_switch_button_y";
        case k_EInputActionOrigin_Switch_LeftBumper                  : return "#origin_switch_shoulder_l";
        case k_EInputActionOrigin_Switch_RightBumper                 : return "#origin_switch_shoulder_r";
        case k_EInputActionOrigin_Switch_Plus                        : return "#origin_switch_button_plus";
        case k_EInputActionOrigin_Switch_Minus                       : return "#origin_switch_button_minus";
        case k_EInputActionOrigin_Switch_Capture                     : return "#origin_switch_button_capture";
        case k_EInputActionOrigin_Switch_LeftTrigger_Pull            : return "#origin_switch_trigger_l_pull";
        case k_EInputActionOrigin_Switch_LeftTrigger_Click           : return "#origin_switch_trigger_l_click";
        case k_EInputActionOrigin_Switch_RightTrigger_Pull           : return "#origin_switch_trigger_r_pull";
        case k_EInputActionOrigin_Switch_RightTrigger_Click          : return "#origin_switch_trigger_r_click";
        case k_EInputActionOrigin_Switch_LeftStick_Move              : return "#origin_switch_stick_l_move";
        case k_EInputActionOrigin_Switch_LeftStick_Click             : return "#origin_switch_stick_l_click";
        case k_EInputActionOrigin_Switch_LeftStick_DPadNorth         : return "#origin_switch_stick_l_dpad_n";
        case k_EInputActionOrigin_Switch_LeftStick_DPadSouth         : return "#origin_switch_stick_l_dpad_s";
        case k_EInputActionOrigin_Switch_LeftStick_DPadWest          : return "#origin_switch_stick_l_dpad_w";
        case k_EInputActionOrigin_Switch_LeftStick_DPadEast          : return "#origin_switch_stick_l_dpad_e";
        case k_EInputActionOrigin_Switch_RightStick_Move             : return "#origin_switch_stick_r_move";
        case k_EInputActionOrigin_Switch_RightStick_Click            : return "#origin_switch_stick_r_click";
        case k_EInputActionOrigin_Switch_RightStick_DPadNorth        : return "#origin_switch_stick_r_dpad_n";
        case k_EInputActionOrigin_Switch_RightStick_DPadSouth        : return "#origin_switch_stick_r_dpad_s";
        case k_EInputActionOrigin_Switch_RightStick_DPadWest         : return "#origin_switch_stick_r_dpad_w";
        case k_EInputActionOrigin_Switch_RightStick_DPadEast         : return "#origin_switch_stick_r_dpad_e";
        case k_EInputActionOrigin_Switch_DPad_North                  : return "#origin_switch_button_dpad_n";
        case k_EInputActionOrigin_Switch_DPad_South                  : return "#origin_switch_button_dpad_s";
        case k_EInputActionOrigin_Switch_DPad_West                   : return "#origin_switch_button_dpad_w";
        case k_EInputActionOrigin_Switch_DPad_East                   : return "#origin_switch_button_dpad_e";
        case k_EInputActionOrigin_Switch_ProGyro_Move                : return "#origin_switch_gyro_move";
        case k_EInputActionOrigin_Switch_ProGyro_Pitch               : return "#origin_switch_gyro_pitch";
        case k_EInputActionOrigin_Switch_ProGyro_Yaw                 : return "#origin_switch_gyro_yaw";
        case k_EInputActionOrigin_Switch_ProGyro_Roll                : return "#origin_switch_gyro_roll";
        case k_EInputActionOrigin_Switch_DPad_Move                   : return "#origin_switch_button_dpad_move";
        // Switch JoyCon Specific
        case k_EInputActionOrigin_Switch_RightGyro_Move              : return "#origin_switch_r_gyro_move";
        case k_EInputActionOrigin_Switch_RightGyro_Pitch             : return "#origin_switch_r_gyro_pitch";
        case k_EInputActionOrigin_Switch_RightGyro_Yaw               : return "#origin_switch_r_gyro_yaw";
        case k_EInputActionOrigin_Switch_RightGyro_Roll              : return "#origin_switch_r_gyro_roll";
        case k_EInputActionOrigin_Switch_LeftGyro_Move               : return "#origin_switch_l_gyro_move";
        case k_EInputActionOrigin_Switch_LeftGyro_Pitch              : return "#origin_switch_l_gyro_pitch";
        case k_EInputActionOrigin_Switch_LeftGyro_Yaw                : return "#origin_switch_l_gyro_yaw";
        case k_EInputActionOrigin_Switch_LeftGyro_Roll               : return "#origin_switch_l_gyro_roll";
        case k_EInputActionOrigin_Switch_LeftGrip_Lower              : return "#origin_switch_l_joycon_sr";
        case k_EInputActionOrigin_Switch_LeftGrip_Upper              : return "#origin_switch_l_joycon_sl";
        case k_EInputActionOrigin_Switch_RightGrip_Lower             : return "#origin_switch_r_joycon_sr";
        case k_EInputActionOrigin_Switch_RightGrip_Upper             : return "#origin_switch_r_joycon_sl";

        default:
            APP_LOG(Log::LogLevel::WARN, "Unknown Origin %d", eOrigin);
            return "#origin_none";
    }
}

// Get the origin(s) for an analog action within an action set. Returns the number of origins supplied in originsOut. Use this to display the appropriate on-screen prompt for the action.
// originsOut should point to a STEAM_CONTROLLER_MAX_ORIGINS sized array of EControllerActionOrigin handles. The EControllerActionOrigin enum will get extended as support for new controller controllers gets added to
// the Steam client and will exceed the values from this header, please check bounds if you are using a look up table.
int Steam_Controller::GetAnalogActionOrigins(ControllerHandle_t controllerHandle, ControllerActionSetHandle_t actionSetHandle, ControllerAnalogActionHandle_t analogActionHandle, STEAM_OUT_ARRAY_COUNT(STEAM_CONTROLLER_MAX_ORIGINS, Receives list of action origins) EControllerActionOrigin* originsOut)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return 0;

    return 0;
}

int Steam_Controller::GetAnalogActionOrigins(InputHandle_t inputHandle, InputActionSetHandle_t actionSetHandle, InputAnalogActionHandle_t analogActionHandle, STEAM_OUT_ARRAY_COUNT(STEAM_INPUT_MAX_ORIGINS, Receives list of action origins) EInputActionOrigin* originsOut)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized || inputHandle == 0 || inputHandle > STEAM_CONTROLLER_MAX_COUNT)
        return 0;

    return 0;
}

// Get a local path to art for on-screen glyph for a particular origin - this call is cheap
const char* Steam_Controller::GetGlyphForActionOrigin(EControllerActionOrigin eOrigin)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    switch (eOrigin)
    {
        // Steam Controller
        case k_EControllerActionOrigin_A                                  : return "button_a.png";
        case k_EControllerActionOrigin_B                                  : return "button_b.png";
        case k_EControllerActionOrigin_X                                  : return "button_x.png";
        case k_EControllerActionOrigin_Y                                  : return "button_y.png";
        case k_EControllerActionOrigin_LeftBumper                         : return "shoulder_l.png";
        case k_EControllerActionOrigin_RightBumper                        : return "shoulder_r.png";
        case k_EControllerActionOrigin_LeftGrip                           : return "grip_l.png";
        case k_EControllerActionOrigin_RightGrip                          : return "grip_r.png";
        case k_EControllerActionOrigin_Start                              : return "button_start.png";
        case k_EControllerActionOrigin_Back                               : return "button_select.png";
        case k_EControllerActionOrigin_LeftPad_Touch                      : return "pad_l_touch.png";
        case k_EControllerActionOrigin_LeftPad_Swipe                      : return "pad_l_swipe.png";
        case k_EControllerActionOrigin_LeftPad_Click                      : return "pad_l_click.png";
        case k_EControllerActionOrigin_LeftPad_DPadNorth                  : return "pad_l_dpad_n.png";
        case k_EControllerActionOrigin_LeftPad_DPadSouth                  : return "pad_l_dpad_s.png";
        case k_EControllerActionOrigin_LeftPad_DPadWest                   : return "pad_l_dpad_w.png";
        case k_EControllerActionOrigin_LeftPad_DPadEast                   : return "pad_l_dpad_e.png";
        case k_EControllerActionOrigin_RightPad_Touch                     : return "pad_r_touch.png";
        case k_EControllerActionOrigin_RightPad_Swipe                     : return "pad_r_swipe.png";
        case k_EControllerActionOrigin_RightPad_Click                     : return "pad_r_click.png";
        case k_EControllerActionOrigin_RightPad_DPadNorth                 : return "pad_r_dpad_n.png";
        case k_EControllerActionOrigin_RightPad_DPadSouth                 : return "pad_r_dpad_s.png";
        case k_EControllerActionOrigin_RightPad_DPadWest                  : return "pad_r_dpad_w.png";
        case k_EControllerActionOrigin_RightPad_DPadEast                  : return "pad_r_dpad_e.png";
        case k_EControllerActionOrigin_LeftTrigger_Pull                   : return "trigger_l_pull.png";
        case k_EControllerActionOrigin_LeftTrigger_Click                  : return "trigger_l_click.png";
        case k_EControllerActionOrigin_RightTrigger_Pull                  : return "trigger_r_pull.png";
        case k_EControllerActionOrigin_RightTrigger_Click                 : return "trigger_r_click.png";
        case k_EControllerActionOrigin_LeftStick_Move                     : return "stick_move.png";
        case k_EControllerActionOrigin_LeftStick_Click                    : return "stick_click.png";
        case k_EControllerActionOrigin_LeftStick_DPadNorth                : return "stick_dpad_n.png";
        case k_EControllerActionOrigin_LeftStick_DPadSouth                : return "stick_dpad_s.png";
        case k_EControllerActionOrigin_LeftStick_DPadWest                 : return "stick_dpad_w.png";
        case k_EControllerActionOrigin_LeftStick_DPadEast                 : return "stick_dpad_e.png";
        case k_EControllerActionOrigin_Gyro_Move                          : return "gyro.png";
        case k_EControllerActionOrigin_Gyro_Pitch                         : return "gyro.png";
        case k_EControllerActionOrigin_Gyro_Yaw                           : return "gyro.png";
        case k_EControllerActionOrigin_Gyro_Roll                          : return "gyro.png";
        // PS4 Dual Shock
        case k_EControllerActionOrigin_PS4_X                              : return "ps4_button_x.png";
        case k_EControllerActionOrigin_PS4_Circle                         : return "ps4_button_circle.png";
        case k_EControllerActionOrigin_PS4_Triangle                       : return "ps4_button_triangle.png";
        case k_EControllerActionOrigin_PS4_Square                         : return "ps4_button_square.png";
        case k_EControllerActionOrigin_PS4_LeftBumper                     : return "shoulder_l.png";
        case k_EControllerActionOrigin_PS4_RightBumper                    : return "shoulder_r.png";
        case k_EControllerActionOrigin_PS4_Options                        : return "ps4_button_options.png";
        case k_EControllerActionOrigin_PS4_Share                          : return "ps4_button_share.png";
        case k_EControllerActionOrigin_PS4_LeftPad_Touch                  : return "ps4_pad_l_touch.png";
        case k_EControllerActionOrigin_PS4_LeftPad_Swipe                  : return "ps4_pad_l_swipe.png";
        case k_EControllerActionOrigin_PS4_LeftPad_Click                  : return "ps4_pad_l_click.png";
        case k_EControllerActionOrigin_PS4_LeftPad_DPadNorth              : return "ps4_pad_l_dpad_n.png";
        case k_EControllerActionOrigin_PS4_LeftPad_DPadSouth              : return "ps4_pad_l_dpad_s.png";
        case k_EControllerActionOrigin_PS4_LeftPad_DPadWest               : return "ps4_pad_l_dpad_w.png";
        case k_EControllerActionOrigin_PS4_LeftPad_DPadEast               : return "ps4_pad_l_dpad_e.png";
        case k_EControllerActionOrigin_PS4_RightPad_Touch                 : return "ps4_pad_r_touch.png";
        case k_EControllerActionOrigin_PS4_RightPad_Swipe                 : return "ps4_pad_r_swipe.png";
        case k_EControllerActionOrigin_PS4_RightPad_Click                 : return "ps4_pad_r_click.png";
        case k_EControllerActionOrigin_PS4_RightPad_DPadNorth             : return "ps4_pad_r_dpad_n.png";
        case k_EControllerActionOrigin_PS4_RightPad_DPadSouth             : return "ps4_pad_r_dpad_s.png";
        case k_EControllerActionOrigin_PS4_RightPad_DPadWest              : return "ps4_pad_r_dpad_w.png";
        case k_EControllerActionOrigin_PS4_RightPad_DPadEast              : return "ps4_pad_r_dpad_e.png";
        case k_EControllerActionOrigin_PS4_CenterPad_Touch                : return "ps4_pad_center_touch.png";
        case k_EControllerActionOrigin_PS4_CenterPad_Swipe                : return "ps4_pad_center_swipe.png";
        case k_EControllerActionOrigin_PS4_CenterPad_Click                : return "ps4_pad_center_click.png";
        case k_EControllerActionOrigin_PS4_CenterPad_DPadNorth            : return "ps4_pad_center_dpad_n.png";
        case k_EControllerActionOrigin_PS4_CenterPad_DPadSouth            : return "ps4_pad_center_dpad_s.png";
        case k_EControllerActionOrigin_PS4_CenterPad_DPadWest             : return "ps4_pad_center_dpad_w.png";
        case k_EControllerActionOrigin_PS4_CenterPad_DPadEast             : return "ps4_pad_center_dpad_e.png";
        case k_EControllerActionOrigin_PS4_LeftTrigger_Pull               : return "trigger_l_pull.png";
        case k_EControllerActionOrigin_PS4_LeftTrigger_Click              : return "trigger_l_click.png";
        case k_EControllerActionOrigin_PS4_RightTrigger_Pull              : return "trigger_r_pull.png";
        case k_EControllerActionOrigin_PS4_RightTrigger_Click             : return "trigger_r_click.png";
        case k_EControllerActionOrigin_PS4_LeftStick_Move                 : return "ps4_stick_l_move.png";
        case k_EControllerActionOrigin_PS4_LeftStick_Click                : return "ps4_stick_l_click.png";
        case k_EControllerActionOrigin_PS4_LeftStick_DPadNorth            : return "stick_dpad_n.png";
        case k_EControllerActionOrigin_PS4_LeftStick_DPadSouth            : return "stick_dpad_s.png";
        case k_EControllerActionOrigin_PS4_LeftStick_DPadWest             : return "stick_dpad_w.png";
        case k_EControllerActionOrigin_PS4_LeftStick_DPadEast             : return "stick_dpad_e.png";
        case k_EControllerActionOrigin_PS4_RightStick_Move                : return "ps4_stick_r_move.png";
        case k_EControllerActionOrigin_PS4_RightStick_Click               : return "ps4_stick_r_click.png";
        case k_EControllerActionOrigin_PS4_RightStick_DPadNorth           : return "stick_dpad_n.png";
        case k_EControllerActionOrigin_PS4_RightStick_DPadSouth           : return "stick_dpad_s.png";
        case k_EControllerActionOrigin_PS4_RightStick_DPadWest            : return "stick_dpad_w.png";
        case k_EControllerActionOrigin_PS4_RightStick_DPadEast            : return "stick_dpad_e.png";
        case k_EControllerActionOrigin_PS4_DPad_North                     : return "ps4_button_dpad_n.png";
        case k_EControllerActionOrigin_PS4_DPad_South                     : return "ps4_button_dpad_s.png";
        case k_EControllerActionOrigin_PS4_DPad_West                      : return "ps4_button_dpad_w.png";
        case k_EControllerActionOrigin_PS4_DPad_East                      : return "ps4_button_dpad_e.png";
        case k_EControllerActionOrigin_PS4_Gyro_Move                      : return "gyro.png";
        case k_EControllerActionOrigin_PS4_Gyro_Pitch                     : return "gyro.png";
        case k_EControllerActionOrigin_PS4_Gyro_Yaw                       : return "gyro.png";
        case k_EControllerActionOrigin_PS4_Gyro_Roll                      : return "gyro.png";
        case k_EControllerActionOrigin_PS4_DPad_Move                      : return "ps4_button_dpad_move.png";
        // XBox One
        case k_EControllerActionOrigin_XBoxOne_A                          : return "button_a.png";
        case k_EControllerActionOrigin_XBoxOne_B                          : return "button_b.png";
        case k_EControllerActionOrigin_XBoxOne_X                          : return "button_x.png";
        case k_EControllerActionOrigin_XBoxOne_Y                          : return "button_y.png";
        case k_EControllerActionOrigin_XBoxOne_LeftBumper                 : return "shoulder_l.png";
        case k_EControllerActionOrigin_XBoxOne_RightBumper                : return "shoulder_r.png";
        case k_EControllerActionOrigin_XBoxOne_Menu                       : return "xbox_button_start.png";
        case k_EControllerActionOrigin_XBoxOne_View                       : return "xbox_button_select.png";
        case k_EControllerActionOrigin_XBoxOne_LeftTrigger_Pull           : return "trigger_l_pull.png";
        case k_EControllerActionOrigin_XBoxOne_LeftTrigger_Click          : return "trigger_l_click.png";
        case k_EControllerActionOrigin_XBoxOne_RightTrigger_Pull          : return "trigger_r_pull.png";
        case k_EControllerActionOrigin_XBoxOne_RightTrigger_Click         : return "trigger_r_click.png";
        case k_EControllerActionOrigin_XBoxOne_LeftStick_Move             : return "stick_l_move.png";
        case k_EControllerActionOrigin_XBoxOne_LeftStick_Click            : return "stick_l_click.png";
        case k_EControllerActionOrigin_XBoxOne_LeftStick_DPadNorth        : return "stick_dpad_n.png";
        case k_EControllerActionOrigin_XBoxOne_LeftStick_DPadSouth        : return "stick_dpad_s.png";
        case k_EControllerActionOrigin_XBoxOne_LeftStick_DPadWest         : return "stick_dpad_w.png";
        case k_EControllerActionOrigin_XBoxOne_LeftStick_DPadEast         : return "stick_dpad_e.png";
        case k_EControllerActionOrigin_XBoxOne_RightStick_Move            : return "stick_r_move.png";
        case k_EControllerActionOrigin_XBoxOne_RightStick_Click           : return "stick_r_click.png";
        case k_EControllerActionOrigin_XBoxOne_RightStick_DPadNorth       : return "stick_dpad_n.png";
        case k_EControllerActionOrigin_XBoxOne_RightStick_DPadSouth       : return "stick_dpad_s.png";
        case k_EControllerActionOrigin_XBoxOne_RightStick_DPadWest        : return "stick_dpad_w.png";
        case k_EControllerActionOrigin_XBoxOne_RightStick_DPadEast        : return "stick_dpad_e.png";
        case k_EControllerActionOrigin_XBoxOne_DPad_North                 : return "xbox_button_dpad_n.png";
        case k_EControllerActionOrigin_XBoxOne_DPad_South                 : return "xbox_button_dpad_s.png";
        case k_EControllerActionOrigin_XBoxOne_DPad_West                  : return "xbox_button_dpad_w.png";
        case k_EControllerActionOrigin_XBoxOne_DPad_East                  : return "xbox_button_dpad_e.png";
        case k_EControllerActionOrigin_XBoxOne_DPad_Move                  : return "xbox_button_dpad_move.png";
        // XBox 360
        case k_EControllerActionOrigin_XBox360_A                          : return "button_a.png";
        case k_EControllerActionOrigin_XBox360_B                          : return "button_b.png";
        case k_EControllerActionOrigin_XBox360_X                          : return "button_x.png";
        case k_EControllerActionOrigin_XBox360_Y                          : return "button_y.png";
        case k_EControllerActionOrigin_XBox360_LeftBumper                 : return "shoulder_l.png";
        case k_EControllerActionOrigin_XBox360_RightBumper                : return "shoulder_r.png";
        case k_EControllerActionOrigin_XBox360_Start                      : return "xbox_button_start.png";
        case k_EControllerActionOrigin_XBox360_Back                       : return "xbox_button_select.png";
        case k_EControllerActionOrigin_XBox360_LeftTrigger_Pull           : return "trigger_l_pull.png";
        case k_EControllerActionOrigin_XBox360_LeftTrigger_Click          : return "trigger_l_click.png";
        case k_EControllerActionOrigin_XBox360_RightTrigger_Pull          : return "trigger_r_pull.png";
        case k_EControllerActionOrigin_XBox360_RightTrigger_Click         : return "trigger_r_click.png";
        case k_EControllerActionOrigin_XBox360_LeftStick_Move             : return "stick_l_move.png";
        case k_EControllerActionOrigin_XBox360_LeftStick_Click            : return "stick_l_click.png";
        case k_EControllerActionOrigin_XBox360_LeftStick_DPadNorth        : return "stick_dpad_n.png";
        case k_EControllerActionOrigin_XBox360_LeftStick_DPadSouth        : return "stick_dpad_s.png";
        case k_EControllerActionOrigin_XBox360_LeftStick_DPadWest         : return "stick_dpad_w.png";
        case k_EControllerActionOrigin_XBox360_LeftStick_DPadEast         : return "stick_dpad_e.png";
        case k_EControllerActionOrigin_XBox360_RightStick_Move            : return "stick_r_move.png";
        case k_EControllerActionOrigin_XBox360_RightStick_Click           : return "stick_r_click.png";
        case k_EControllerActionOrigin_XBox360_RightStick_DPadNorth       : return "stick_dpad_n.png";
        case k_EControllerActionOrigin_XBox360_RightStick_DPadSouth       : return "stick_dpad_s.png";
        case k_EControllerActionOrigin_XBox360_RightStick_DPadWest        : return "stick_dpad_w.png";
        case k_EControllerActionOrigin_XBox360_RightStick_DPadEast        : return "stick_dpad_e.png";
        case k_EControllerActionOrigin_XBox360_DPad_North                 : return "xbox_button_dpad_n.png";
        case k_EControllerActionOrigin_XBox360_DPad_South                 : return "xbox_button_dpad_s.png";
        case k_EControllerActionOrigin_XBox360_DPad_West                  : return "xbox_button_dpad_w.png";
        case k_EControllerActionOrigin_XBox360_DPad_East                  : return "xbox_button_dpad_e.png";
        case k_EControllerActionOrigin_XBox360_DPad_Move                  : return "xbox_button_dpad_move.png";
        // Switch - Pro or Joycons used as a single input device.
        // This does not apply to a single joycon
        case k_EControllerActionOrigin_Switch_A                           : return "switch_button_a.png";
        case k_EControllerActionOrigin_Switch_B                           : return "switch_button_b.png";
        case k_EControllerActionOrigin_Switch_X                           : return "switch_button_x.png";
        case k_EControllerActionOrigin_Switch_Y                           : return "switch_button_y.png";
        case k_EControllerActionOrigin_Switch_LeftBumper                  : return "shoulder_l.png";
        case k_EControllerActionOrigin_Switch_RightBumper                 : return "shoulder_r.png";
        case k_EControllerActionOrigin_Switch_Plus                        : return "switch_button_plus.png";
        case k_EControllerActionOrigin_Switch_Minus                       : return "switch_button_minus.png";
        case k_EControllerActionOrigin_Switch_Capture                     : return "switch_button_capture.png";
        case k_EControllerActionOrigin_Switch_LeftTrigger_Pull            : return "trigger_l_pull.png";
        case k_EControllerActionOrigin_Switch_LeftTrigger_Click           : return "trigger_l_click.png";
        case k_EControllerActionOrigin_Switch_RightTrigger_Pull           : return "trigger_r_pull.png";
        case k_EControllerActionOrigin_Switch_RightTrigger_Click          : return "trigger_r_click.png";
        case k_EControllerActionOrigin_Switch_LeftStick_Move              : return "stick_l_move.png";
        case k_EControllerActionOrigin_Switch_LeftStick_Click             : return "stick_l_click.png";
        case k_EControllerActionOrigin_Switch_LeftStick_DPadNorth         : return "stick_dpad_n.png";
        case k_EControllerActionOrigin_Switch_LeftStick_DPadSouth         : return "stick_dpad_s.png";
        case k_EControllerActionOrigin_Switch_LeftStick_DPadWest          : return "stick_dpad_w.png";
        case k_EControllerActionOrigin_Switch_LeftStick_DPadEast          : return "stick_dpad_e.png";
        case k_EControllerActionOrigin_Switch_RightStick_Move             : return "stick_r_move.png";
        case k_EControllerActionOrigin_Switch_RightStick_Click            : return "stick_r_click.png";
        case k_EControllerActionOrigin_Switch_RightStick_DPadNorth        : return "stick_dpad_n.png";
        case k_EControllerActionOrigin_Switch_RightStick_DPadSouth        : return "stick_dpad_s.png";
        case k_EControllerActionOrigin_Switch_RightStick_DPadWest         : return "stick_dpad_w.png";
        case k_EControllerActionOrigin_Switch_RightStick_DPadEast         : return "stick_dpad_e.png";
        case k_EControllerActionOrigin_Switch_DPad_North                  : return "switch_button_dpad_n.png";
        case k_EControllerActionOrigin_Switch_DPad_South                  : return "switch_button_dpad_s.png";
        case k_EControllerActionOrigin_Switch_DPad_West                   : return "switch_button_dpad_w.png";
        case k_EControllerActionOrigin_Switch_DPad_East                   : return "switch_button_dpad_e.png";
        case k_EControllerActionOrigin_Switch_ProGyro_Move                : return "gyro.png";
        case k_EControllerActionOrigin_Switch_ProGyro_Pitch               : return "gyro.png";
        case k_EControllerActionOrigin_Switch_ProGyro_Yaw                 : return "gyro.png";
        case k_EControllerActionOrigin_Switch_ProGyro_Roll                : return "gyro.png";
        case k_EControllerActionOrigin_Switch_DPad_Move                   : return "switch_button_dpad_move.png";
        // Switch JoyCon Specific
        case k_EControllerActionOrigin_Switch_RightGyro_Move              : return "gyro.png";
        case k_EControllerActionOrigin_Switch_RightGyro_Pitch             : return "gyro.png";
        case k_EControllerActionOrigin_Switch_RightGyro_Yaw               : return "gyro.png";
        case k_EControllerActionOrigin_Switch_RightGyro_Roll              : return "gyro.png";
        case k_EControllerActionOrigin_Switch_LeftGyro_Move               : return "gyro.png";
        case k_EControllerActionOrigin_Switch_LeftGyro_Pitch              : return "gyro.png";
        case k_EControllerActionOrigin_Switch_LeftGyro_Yaw                : return "gyro.png";
        case k_EControllerActionOrigin_Switch_LeftGyro_Roll               : return "gyro.png";
        case k_EControllerActionOrigin_Switch_LeftGrip_Lower              : return "grip_l.png";
        case k_EControllerActionOrigin_Switch_LeftGrip_Upper              : return "grip_l.png";
        case k_EControllerActionOrigin_Switch_RightGrip_Lower             : return "grip_r.png";
        case k_EControllerActionOrigin_Switch_RightGrip_Upper             : return "grip_r.png";

        default: 
            APP_LOG(Log::LogLevel::WARN, "Unknown Origin %d", eOrigin);
            return "";
    }
}

// Returns a localized string (from Steam's language setting) for the specified origin - this call is serialized
const char* Steam_Controller::GetStringForActionOrigin(EControllerActionOrigin eOrigin)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    switch (eOrigin)
    {
        // Steam Controller
        case k_EControllerActionOrigin_A                                  : return "#origin_steamcontroller_button_a";
        case k_EControllerActionOrigin_B                                  : return "#origin_steamcontroller_button_b";
        case k_EControllerActionOrigin_X                                  : return "#origin_steamcontroller_button_x";
        case k_EControllerActionOrigin_Y                                  : return "#origin_steamcontroller_button_y";
        case k_EControllerActionOrigin_LeftBumper                         : return "#origin_steamcontroller_shoulder_l";
        case k_EControllerActionOrigin_RightBumper                        : return "#origin_steamcontroller_shoulder_r";
        case k_EControllerActionOrigin_LeftGrip                           : return "#origin_steamcontroller_grip_l";
        case k_EControllerActionOrigin_RightGrip                          : return "#origin_steamcontroller_grip_r";
        case k_EControllerActionOrigin_Start                              : return "#origin_steamcontroller_button_start";
        case k_EControllerActionOrigin_Back                               : return "#origin_steamcontroller_button_select";
        case k_EControllerActionOrigin_LeftPad_Touch                      : return "#origin_steamcontroller_pad_l_touch";
        case k_EControllerActionOrigin_LeftPad_Swipe                      : return "#origin_steamcontroller_pad_l_swipe";
        case k_EControllerActionOrigin_LeftPad_Click                      : return "#origin_steamcontroller_pad_l_click";
        case k_EControllerActionOrigin_LeftPad_DPadNorth                  : return "#origin_steamcontroller_pad_l_dpad_n";
        case k_EControllerActionOrigin_LeftPad_DPadSouth                  : return "#origin_steamcontroller_pad_l_dpad_s";
        case k_EControllerActionOrigin_LeftPad_DPadWest                   : return "#origin_steamcontroller_pad_l_dpad_w";
        case k_EControllerActionOrigin_LeftPad_DPadEast                   : return "#origin_steamcontroller_pad_l_dpad_e";
        case k_EControllerActionOrigin_RightPad_Touch                     : return "#origin_steamcontroller_pad_r_touch";
        case k_EControllerActionOrigin_RightPad_Swipe                     : return "#origin_steamcontroller_pad_r_swipe";
        case k_EControllerActionOrigin_RightPad_Click                     : return "#origin_steamcontroller_pad_r_click";
        case k_EControllerActionOrigin_RightPad_DPadNorth                 : return "#origin_steamcontroller_pad_r_dpad_n";
        case k_EControllerActionOrigin_RightPad_DPadSouth                 : return "#origin_steamcontroller_pad_r_dpad_s";
        case k_EControllerActionOrigin_RightPad_DPadWest                  : return "#origin_steamcontroller_pad_r_dpad_w";
        case k_EControllerActionOrigin_RightPad_DPadEast                  : return "#origin_steamcontroller_pad_r_dpad_e";
        case k_EControllerActionOrigin_LeftTrigger_Pull                   : return "#origin_steamcontroller_trigger_l_pull";
        case k_EControllerActionOrigin_LeftTrigger_Click                  : return "#origin_steamcontroller_trigger_l_click";
        case k_EControllerActionOrigin_RightTrigger_Pull                  : return "#origin_steamcontroller_trigger_r_pull";
        case k_EControllerActionOrigin_RightTrigger_Click                 : return "#origin_steamcontroller_trigger_r_click";
        case k_EControllerActionOrigin_LeftStick_Move                     : return "#origin_steamcontroller_stick_move";
        case k_EControllerActionOrigin_LeftStick_Click                    : return "#origin_steamcontroller_stick_click";
        case k_EControllerActionOrigin_LeftStick_DPadNorth                : return "#origin_steamcontroller_stick_dpad_n";
        case k_EControllerActionOrigin_LeftStick_DPadSouth                : return "#origin_steamcontroller_stick_dpad_s";
        case k_EControllerActionOrigin_LeftStick_DPadWest                 : return "#origin_steamcontroller_stick_dpad_w";
        case k_EControllerActionOrigin_LeftStick_DPadEast                 : return "#origin_steamcontroller_stick_dpad_e";
        case k_EControllerActionOrigin_Gyro_Move                          : return "#origin_steamcontroller_gyro_move";
        case k_EControllerActionOrigin_Gyro_Pitch                         : return "#origin_steamcontroller_gyro_pitch";
        case k_EControllerActionOrigin_Gyro_Yaw                           : return "#origin_steamcontroller_gyro_yaw";
        case k_EControllerActionOrigin_Gyro_Roll                          : return "#origin_steamcontroller_gyro_roll";
        // PS4 Dual Shock
        case k_EControllerActionOrigin_PS4_X                              : return "#origin_ps4_button_x";
        case k_EControllerActionOrigin_PS4_Circle                         : return "#origin_ps4_button_circle";
        case k_EControllerActionOrigin_PS4_Triangle                       : return "#origin_ps4_button_triangle";
        case k_EControllerActionOrigin_PS4_Square                         : return "#origin_ps4_button_square";
        case k_EControllerActionOrigin_PS4_LeftBumper                     : return "#origin_ps4_shoulder_l";
        case k_EControllerActionOrigin_PS4_RightBumper                    : return "#origin_ps4_shoulder_r";
        case k_EControllerActionOrigin_PS4_Options                        : return "#origin_ps4_button_options";
        case k_EControllerActionOrigin_PS4_Share                          : return "#origin_ps4_button_share";
        case k_EControllerActionOrigin_PS4_LeftPad_Touch                  : return "#origin_ps4_pad_l_touch";
        case k_EControllerActionOrigin_PS4_LeftPad_Swipe                  : return "#origin_ps4_pad_l_swipe";
        case k_EControllerActionOrigin_PS4_LeftPad_Click                  : return "#origin_ps4_pad_l_click";
        case k_EControllerActionOrigin_PS4_LeftPad_DPadNorth              : return "#origin_ps4_pad_l_dpad_n";
        case k_EControllerActionOrigin_PS4_LeftPad_DPadSouth              : return "#origin_ps4_pad_l_dpad_s";
        case k_EControllerActionOrigin_PS4_LeftPad_DPadWest               : return "#origin_ps4_pad_l_dpad_w";
        case k_EControllerActionOrigin_PS4_LeftPad_DPadEast               : return "#origin_ps4_pad_l_dpad_e";
        case k_EControllerActionOrigin_PS4_RightPad_Touch                 : return "#origin_ps4_pad_r_touch";
        case k_EControllerActionOrigin_PS4_RightPad_Swipe                 : return "#origin_ps4_pad_r_swipe";
        case k_EControllerActionOrigin_PS4_RightPad_Click                 : return "#origin_ps4_pad_r_click";
        case k_EControllerActionOrigin_PS4_RightPad_DPadNorth             : return "#origin_ps4_pad_r_dpad_n";
        case k_EControllerActionOrigin_PS4_RightPad_DPadSouth             : return "#origin_ps4_pad_r_dpad_s";
        case k_EControllerActionOrigin_PS4_RightPad_DPadWest              : return "#origin_ps4_pad_r_dpad_w";
        case k_EControllerActionOrigin_PS4_RightPad_DPadEast              : return "#origin_ps4_pad_r_dpad_e";
        case k_EControllerActionOrigin_PS4_CenterPad_Touch                : return "#origin_ps4_pad_center_touch";
        case k_EControllerActionOrigin_PS4_CenterPad_Swipe                : return "#origin_ps4_pad_center_swipe";
        case k_EControllerActionOrigin_PS4_CenterPad_Click                : return "#origin_ps4_pad_center_click";
        case k_EControllerActionOrigin_PS4_CenterPad_DPadNorth            : return "#origin_ps4_pad_center_dpad_n";
        case k_EControllerActionOrigin_PS4_CenterPad_DPadSouth            : return "#origin_ps4_pad_center_dpad_s";
        case k_EControllerActionOrigin_PS4_CenterPad_DPadWest             : return "#origin_ps4_pad_center_dpad_w";
        case k_EControllerActionOrigin_PS4_CenterPad_DPadEast             : return "#origin_ps4_pad_center_dpad_e";
        case k_EControllerActionOrigin_PS4_LeftTrigger_Pull               : return "#origin_ps4_trigger_l_pull";
        case k_EControllerActionOrigin_PS4_LeftTrigger_Click              : return "#origin_ps4_trigger_l_click";
        case k_EControllerActionOrigin_PS4_RightTrigger_Pull              : return "#origin_ps4_trigger_r_pull";
        case k_EControllerActionOrigin_PS4_RightTrigger_Click             : return "#origin_ps4_trigger_r_click";
        case k_EControllerActionOrigin_PS4_LeftStick_Move                 : return "#origin_ps4_stick_l_move";
        case k_EControllerActionOrigin_PS4_LeftStick_Click                : return "#origin_ps4_stick_l_click";
        case k_EControllerActionOrigin_PS4_LeftStick_DPadNorth            : return "#origin_ps4_stick_l_dpad_n";
        case k_EControllerActionOrigin_PS4_LeftStick_DPadSouth            : return "#origin_ps4_stick_l_dpad_s";
        case k_EControllerActionOrigin_PS4_LeftStick_DPadWest             : return "#origin_ps4_stick_l_dpad_w";
        case k_EControllerActionOrigin_PS4_LeftStick_DPadEast             : return "#origin_ps4_stick_l_dpad_e";
        case k_EControllerActionOrigin_PS4_RightStick_Move                : return "#origin_ps4_stick_r_move";
        case k_EControllerActionOrigin_PS4_RightStick_Click               : return "#origin_ps4_stick_r_click";
        case k_EControllerActionOrigin_PS4_RightStick_DPadNorth           : return "#origin_ps4_stick_r_dpad_n";
        case k_EControllerActionOrigin_PS4_RightStick_DPadSouth           : return "#origin_ps4_stick_r_dpad_s";
        case k_EControllerActionOrigin_PS4_RightStick_DPadWest            : return "#origin_ps4_stick_r_dpad_w";
        case k_EControllerActionOrigin_PS4_RightStick_DPadEast            : return "#origin_ps4_stick_r_dpad_e";
        case k_EControllerActionOrigin_PS4_DPad_North                     : return "#origin_ps4_button_dpad_n";
        case k_EControllerActionOrigin_PS4_DPad_South                     : return "#origin_ps4_button_dpad_s";
        case k_EControllerActionOrigin_PS4_DPad_West                      : return "#origin_ps4_button_dpad_w";
        case k_EControllerActionOrigin_PS4_DPad_East                      : return "#origin_ps4_button_dpad_e";
        case k_EControllerActionOrigin_PS4_Gyro_Move                      : return "#origin_ps4_gyro_move";
        case k_EControllerActionOrigin_PS4_Gyro_Pitch                     : return "#origin_ps4_gyro_pitch";
        case k_EControllerActionOrigin_PS4_Gyro_Yaw                       : return "#origin_ps4_gyro_yaw";
        case k_EControllerActionOrigin_PS4_Gyro_Roll                      : return "#origin_ps4_gyro_roll";
        case k_EControllerActionOrigin_PS4_DPad_Move                      : return "#origin_ps4_button_dpad_move";
        // XBox One
        case k_EControllerActionOrigin_XBoxOne_A                          : return "#origin_xboxone_button_a";
        case k_EControllerActionOrigin_XBoxOne_B                          : return "#origin_xboxone_button_b";
        case k_EControllerActionOrigin_XBoxOne_X                          : return "#origin_xboxone_button_x";
        case k_EControllerActionOrigin_XBoxOne_Y                          : return "#origin_xboxone_button_y";
        case k_EControllerActionOrigin_XBoxOne_LeftBumper                 : return "#origin_xboxone_shoulder_l";
        case k_EControllerActionOrigin_XBoxOne_RightBumper                : return "#origin_xboxone_shoulder_r";
        case k_EControllerActionOrigin_XBoxOne_Menu                       : return "#origin_xboxone_button_menu";
        case k_EControllerActionOrigin_XBoxOne_View                       : return "#origin_xboxone_button_view";
        case k_EControllerActionOrigin_XBoxOne_LeftTrigger_Pull           : return "#origin_xboxone_trigger_l_pull";
        case k_EControllerActionOrigin_XBoxOne_LeftTrigger_Click          : return "#origin_xboxone_trigger_l_click";
        case k_EControllerActionOrigin_XBoxOne_RightTrigger_Pull          : return "#origin_xboxone_trigger_r_pull";
        case k_EControllerActionOrigin_XBoxOne_RightTrigger_Click         : return "#origin_xboxone_trigger_r_click";
        case k_EControllerActionOrigin_XBoxOne_LeftStick_Move             : return "#origin_xboxone_stick_l_move";
        case k_EControllerActionOrigin_XBoxOne_LeftStick_Click            : return "#origin_xboxone_stick_l_click";
        case k_EControllerActionOrigin_XBoxOne_LeftStick_DPadNorth        : return "#origin_xboxone_stick_l_dpad_n";
        case k_EControllerActionOrigin_XBoxOne_LeftStick_DPadSouth        : return "#origin_xboxone_stick_l_dpad_s";
        case k_EControllerActionOrigin_XBoxOne_LeftStick_DPadWest         : return "#origin_xboxone_stick_l_dpad_w";
        case k_EControllerActionOrigin_XBoxOne_LeftStick_DPadEast         : return "#origin_xboxone_stick_l_dpad_e";
        case k_EControllerActionOrigin_XBoxOne_RightStick_Move            : return "#origin_xboxone_stick_r_move";
        case k_EControllerActionOrigin_XBoxOne_RightStick_Click           : return "#origin_xboxone_stick_r_click";
        case k_EControllerActionOrigin_XBoxOne_RightStick_DPadNorth       : return "#origin_xboxone_stick_r_dpad_n";
        case k_EControllerActionOrigin_XBoxOne_RightStick_DPadSouth       : return "#origin_xboxone_stick_r_dpad_s";
        case k_EControllerActionOrigin_XBoxOne_RightStick_DPadWest        : return "#origin_xboxone_stick_r_dpad_w";
        case k_EControllerActionOrigin_XBoxOne_RightStick_DPadEast        : return "#origin_xboxone_stick_r_dpad_e";
        case k_EControllerActionOrigin_XBoxOne_DPad_North                 : return "#origin_xboxone_button_dpad_n";
        case k_EControllerActionOrigin_XBoxOne_DPad_South                 : return "#origin_xboxone_button_dpad_s";
        case k_EControllerActionOrigin_XBoxOne_DPad_West                  : return "#origin_xboxone_button_dpad_w";
        case k_EControllerActionOrigin_XBoxOne_DPad_East                  : return "#origin_xboxone_button_dpad_e";
        case k_EControllerActionOrigin_XBoxOne_DPad_Move                  : return "#origin_xboxone_button_dpad_move";
        // XBox 360
        case k_EControllerActionOrigin_XBox360_A                          : return "#origin_xbox360_button_a";
        case k_EControllerActionOrigin_XBox360_B                          : return "#origin_xbox360_button_b";
        case k_EControllerActionOrigin_XBox360_X                          : return "#origin_xbox360_button_x";
        case k_EControllerActionOrigin_XBox360_Y                          : return "#origin_xbox360_button_y";
        case k_EControllerActionOrigin_XBox360_LeftBumper                 : return "#origin_xbox360_shoulder_l";
        case k_EControllerActionOrigin_XBox360_RightBumper                : return "#origin_xbox360_shoulder_r";
        case k_EControllerActionOrigin_XBox360_Start                      : return "#origin_xbox360_button_start";
        case k_EControllerActionOrigin_XBox360_Back                       : return "#origin_xbox360_button_back";
        case k_EControllerActionOrigin_XBox360_LeftTrigger_Pull           : return "#origin_xbox360_trigger_l_pull";
        case k_EControllerActionOrigin_XBox360_LeftTrigger_Click          : return "#origin_xbox360_trigger_l_click";
        case k_EControllerActionOrigin_XBox360_RightTrigger_Pull          : return "#origin_xbox360_trigger_r_pull";
        case k_EControllerActionOrigin_XBox360_RightTrigger_Click         : return "#origin_xbox360_trigger_r_click";
        case k_EControllerActionOrigin_XBox360_LeftStick_Move             : return "#origin_xbox360_stick_l_move";
        case k_EControllerActionOrigin_XBox360_LeftStick_Click            : return "#origin_xbox360_stick_l_click";
        case k_EControllerActionOrigin_XBox360_LeftStick_DPadNorth        : return "#origin_xbox360_stick_l_dpad_n";
        case k_EControllerActionOrigin_XBox360_LeftStick_DPadSouth        : return "#origin_xbox360_stick_l_dpad_s";
        case k_EControllerActionOrigin_XBox360_LeftStick_DPadWest         : return "#origin_xbox360_stick_l_dpad_w";
        case k_EControllerActionOrigin_XBox360_LeftStick_DPadEast         : return "#origin_xbox360_stick_l_dpad_e";
        case k_EControllerActionOrigin_XBox360_RightStick_Move            : return "#origin_xbox360_stick_r_move";
        case k_EControllerActionOrigin_XBox360_RightStick_Click           : return "#origin_xbox360_stick_r_click";
        case k_EControllerActionOrigin_XBox360_RightStick_DPadNorth       : return "#origin_xbox360_stick_r_dpad_n";
        case k_EControllerActionOrigin_XBox360_RightStick_DPadSouth       : return "#origin_xbox360_stick_r_dpad_s";
        case k_EControllerActionOrigin_XBox360_RightStick_DPadWest        : return "#origin_xbox360_stick_r_dpad_w";
        case k_EControllerActionOrigin_XBox360_RightStick_DPadEast        : return "#origin_xbox360_stick_r_dpad_e";
        case k_EControllerActionOrigin_XBox360_DPad_North                 : return "#origin_xbox360_button_dpad_n";
        case k_EControllerActionOrigin_XBox360_DPad_South                 : return "#origin_xbox360_button_dpad_s";
        case k_EControllerActionOrigin_XBox360_DPad_West                  : return "#origin_xbox360_button_dpad_w";
        case k_EControllerActionOrigin_XBox360_DPad_East                  : return "#origin_xbox360_button_dpad_e";
        case k_EControllerActionOrigin_XBox360_DPad_Move                  : return "#origin_xbox360_button_dpad_move";
        // Switch - Pro or Joycons used as a single input device.
        // This does not apply to a single joycon
        case k_EControllerActionOrigin_Switch_A                           : return "#origin_switch_button_a";
        case k_EControllerActionOrigin_Switch_B                           : return "#origin_switch_button_b";
        case k_EControllerActionOrigin_Switch_X                           : return "#origin_switch_button_x";
        case k_EControllerActionOrigin_Switch_Y                           : return "#origin_switch_button_y";
        case k_EControllerActionOrigin_Switch_LeftBumper                  : return "#origin_switch_shoulder_l";
        case k_EControllerActionOrigin_Switch_RightBumper                 : return "#origin_switch_shoulder_r";
        case k_EControllerActionOrigin_Switch_Plus                        : return "#origin_switch_button_plus";
        case k_EControllerActionOrigin_Switch_Minus                       : return "#origin_switch_button_minus";
        case k_EControllerActionOrigin_Switch_Capture                     : return "#origin_switch_button_capture";
        case k_EControllerActionOrigin_Switch_LeftTrigger_Pull            : return "#origin_switch_trigger_l_pull";
        case k_EControllerActionOrigin_Switch_LeftTrigger_Click           : return "#origin_switch_trigger_l_click";
        case k_EControllerActionOrigin_Switch_RightTrigger_Pull           : return "#origin_switch_trigger_r_pull";
        case k_EControllerActionOrigin_Switch_RightTrigger_Click          : return "#origin_switch_trigger_r_click";
        case k_EControllerActionOrigin_Switch_LeftStick_Move              : return "#origin_switch_stick_l_move";
        case k_EControllerActionOrigin_Switch_LeftStick_Click             : return "#origin_switch_stick_l_click";
        case k_EControllerActionOrigin_Switch_LeftStick_DPadNorth         : return "#origin_switch_stick_l_dpad_n";
        case k_EControllerActionOrigin_Switch_LeftStick_DPadSouth         : return "#origin_switch_stick_l_dpad_s";
        case k_EControllerActionOrigin_Switch_LeftStick_DPadWest          : return "#origin_switch_stick_l_dpad_w";
        case k_EControllerActionOrigin_Switch_LeftStick_DPadEast          : return "#origin_switch_stick_l_dpad_e";
        case k_EControllerActionOrigin_Switch_RightStick_Move             : return "#origin_switch_stick_r_move";
        case k_EControllerActionOrigin_Switch_RightStick_Click            : return "#origin_switch_stick_r_click";
        case k_EControllerActionOrigin_Switch_RightStick_DPadNorth        : return "#origin_switch_stick_r_dpad_n";
        case k_EControllerActionOrigin_Switch_RightStick_DPadSouth        : return "#origin_switch_stick_r_dpad_s";
        case k_EControllerActionOrigin_Switch_RightStick_DPadWest         : return "#origin_switch_stick_r_dpad_w";
        case k_EControllerActionOrigin_Switch_RightStick_DPadEast         : return "#origin_switch_stick_r_dpad_e";
        case k_EControllerActionOrigin_Switch_DPad_North                  : return "#origin_switch_button_dpad_n";
        case k_EControllerActionOrigin_Switch_DPad_South                  : return "#origin_switch_button_dpad_s";
        case k_EControllerActionOrigin_Switch_DPad_West                   : return "#origin_switch_button_dpad_w";
        case k_EControllerActionOrigin_Switch_DPad_East                   : return "#origin_switch_button_dpad_e";
        case k_EControllerActionOrigin_Switch_ProGyro_Move                : return "#origin_switch_gyro_move";
        case k_EControllerActionOrigin_Switch_ProGyro_Pitch               : return "#origin_switch_gyro_pitch";
        case k_EControllerActionOrigin_Switch_ProGyro_Yaw                 : return "#origin_switch_gyro_yaw";
        case k_EControllerActionOrigin_Switch_ProGyro_Roll                : return "#origin_switch_gyro_roll";
        case k_EControllerActionOrigin_Switch_DPad_Move                   : return "#origin_switch_button_dpad_move";
        // Switch JoyCon Specific
        case k_EControllerActionOrigin_Switch_RightGyro_Move              : return "#origin_switch_r_gyro_move";
        case k_EControllerActionOrigin_Switch_RightGyro_Pitch             : return "#origin_switch_r_gyro_pitch";
        case k_EControllerActionOrigin_Switch_RightGyro_Yaw               : return "#origin_switch_r_gyro_yaw";
        case k_EControllerActionOrigin_Switch_RightGyro_Roll              : return "#origin_switch_r_gyro_roll";
        case k_EControllerActionOrigin_Switch_LeftGyro_Move               : return "#origin_switch_l_gyro_move";
        case k_EControllerActionOrigin_Switch_LeftGyro_Pitch              : return "#origin_switch_l_gyro_pitch";
        case k_EControllerActionOrigin_Switch_LeftGyro_Yaw                : return "#origin_switch_l_gyro_yaw";
        case k_EControllerActionOrigin_Switch_LeftGyro_Roll               : return "#origin_switch_l_gyro_roll";
        case k_EControllerActionOrigin_Switch_LeftGrip_Lower              : return "#origin_switch_l_joycon_sr";
        case k_EControllerActionOrigin_Switch_LeftGrip_Upper              : return "#origin_switch_l_joycon_sl";
        case k_EControllerActionOrigin_Switch_RightGrip_Lower             : return "#origin_switch_r_joycon_sr";
        case k_EControllerActionOrigin_Switch_RightGrip_Upper             : return "#origin_switch_r_joycon_sl";

        default:
            APP_LOG(Log::LogLevel::WARN, "Unknown Origin %d", eOrigin);
            return "#origin_none";
    }
}

void Steam_Controller::StopAnalogActionMomentum(ControllerHandle_t controllerHandle, ControllerAnalogActionHandle_t eAction)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return;
}

// Returns raw motion data from the specified controller
ControllerMotionData_t Steam_Controller::GetMotionData(ControllerHandle_t controllerHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    ControllerMotionData_t data = {};
    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return data;

    return data;
}

bool Steam_Controller::ShowDigitalActionOrigins(ControllerHandle_t controllerHandle, ControllerDigitalActionHandle_t digitalActionHandle, float flScale, float flXPosition, float flYPosition)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return false;

    return false;
}

bool Steam_Controller::ShowAnalogActionOrigins(ControllerHandle_t controllerHandle, ControllerAnalogActionHandle_t analogActionHandle, float flScale, float flXPosition, float flYPosition)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return false;

    return false;
}

//-----------------------------------------------------------------------------
// OUTPUTS
//-----------------------------------------------------------------------------

// Trigger a haptic pulse on a controller
void Steam_Controller::TriggerHapticPulse(ControllerHandle_t controllerHandle, ESteamControllerPad eTargetPad, unsigned short usDurationMicroSec)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return;
}

// Trigger a pulse with a duty cycle of usDurationMicroSec / usOffMicroSec, unRepeat times.
// nFlags is currently unused and reserved for future use.
void Steam_Controller::TriggerRepeatedHapticPulse(ControllerHandle_t controllerHandle, ESteamControllerPad eTargetPad, unsigned short usDurationMicroSec, unsigned short usOffMicroSec, unsigned short unRepeat, unsigned int nFlags)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    if (!_initialized)
        return;
}

// Trigger a vibration event on supported controllers.  
void Steam_Controller::TriggerVibration(ControllerHandle_t controllerHandle, unsigned short usLeftSpeed, unsigned short usRightSpeed)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return;

    auto& gp = *Gamepad::get_gamepads(false)[controllerHandle-1];
    gp.SetVibration(usLeftSpeed, usRightSpeed);
}

// Set the controller LED color on supported controllers.  
void Steam_Controller::SetLEDColor(ControllerHandle_t controllerHandle, uint8 nColorR, uint8 nColorG, uint8 nColorB, unsigned int nFlags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return;

    auto& gp = *Gamepad::get_gamepads(false)[controllerHandle-1];
    gp.SetLed(nColorR, nColorG, nColorB);
}

//-----------------------------------------------------------------------------
// Utility functions availible without using the rest of Steam Input API
//-----------------------------------------------------------------------------

// Invokes the Steam overlay and brings up the binding screen if the user is using Big Picture Mode
// If the user is not in Big Picture Mode it will open up the binding in a new window
bool Steam_Controller::ShowBindingPanel(ControllerHandle_t controllerHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return false;

    return true;
}

// Returns the input type for a particular handle
ESteamInputType Steam_Controller::GetInputTypeForHandle(ControllerHandle_t controllerHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (!_initialized || controllerHandle == 0 || controllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return k_ESteamInputType_Unknown;

    auto& gp = *Gamepad::get_gamepads(false)[controllerHandle-1];

    auto it = Gamepad::gamepads_ids.find(gp.id);
    if(it == Gamepad::gamepads_ids.end())
        return k_ESteamInputType_XBox360Controller;

    switch (it->second.type)
    {
        case gamepad_type_t::type_e::Xbox360: return k_ESteamInputType_XBox360Controller;
        case gamepad_type_t::type_e::XboxOne: return k_ESteamInputType_XBoxOneController;
        case gamepad_type_t::type_e::PS3: return k_ESteamInputType_PS3Controller;
        case gamepad_type_t::type_e::PS4: return k_ESteamInputType_PS4Controller;
        //case gamepad_type_t::type_e::Switch: return k_ESteamInputType_SwitchJoyConSingle;
        default: return k_ESteamInputType_Unknown;
    }
}

// Returns the associated controller handle for the specified emulated gamepad - can be used with the above 2 functions
// to identify controllers presented to your game over Xinput. Returns 0 if the Xinput index isn't associated with Steam Input
ControllerHandle_t Steam_Controller::GetControllerForGamepadIndex(int nIndex)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (!_initialized)
        return 0;

    if (nIndex < Gamepad::get_gamepads(false).max_size())
        return 0;

    auto& gp = *Gamepad::get_gamepads(false)[nIndex];

    return (gp.Enabled() ? nIndex + 1 : 0);
}

// Returns the associated gamepad index for the specified controller, if emulating a gamepad or -1 if not associated with an Xinput index
int Steam_Controller::GetGamepadIndexForController(ControllerHandle_t ulControllerHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (!_initialized || ulControllerHandle == 0 || ulControllerHandle > STEAM_CONTROLLER_MAX_COUNT)
        return -1;

    auto& gp = *Gamepad::get_gamepads(false)[ulControllerHandle - 1];

    return (gp.Enabled() ? ulControllerHandle - 1 : -1);
}

// Returns a localized string (from Steam's language setting) for the specified Xbox controller origin.
const char* Steam_Controller::GetStringForXboxOrigin(EXboxOrigin eOrigin)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    //k_EXboxOrigin_A,
    //k_EXboxOrigin_B,
    //k_EXboxOrigin_X,
    //k_EXboxOrigin_Y,
    //k_EXboxOrigin_LeftBumper,
    //k_EXboxOrigin_RightBumper,
    //k_EXboxOrigin_Menu,  //Start
    //k_EXboxOrigin_View,  //Back
    //k_EXboxOrigin_LeftTrigger_Pull,
    //k_EXboxOrigin_LeftTrigger_Click,
    //k_EXboxOrigin_RightTrigger_Pull,
    //k_EXboxOrigin_RightTrigger_Click,
    //k_EXboxOrigin_LeftStick_Move,
    //k_EXboxOrigin_LeftStick_Click,
    //k_EXboxOrigin_LeftStick_DPadNorth,
    //k_EXboxOrigin_LeftStick_DPadSouth,
    //k_EXboxOrigin_LeftStick_DPadWest,
    //k_EXboxOrigin_LeftStick_DPadEast,
    //k_EXboxOrigin_RightStick_Move,
    //k_EXboxOrigin_RightStick_Click,
    //k_EXboxOrigin_RightStick_DPadNorth,
    //k_EXboxOrigin_RightStick_DPadSouth,
    //k_EXboxOrigin_RightStick_DPadWest,
    //k_EXboxOrigin_RightStick_DPadEast,
    //k_EXboxOrigin_DPad_North,
    //k_EXboxOrigin_DPad_South,
    //k_EXboxOrigin_DPad_West,
    //k_EXboxOrigin_DPad_East,

    return "";
}

// Get a local path to art for on-screen glyph for a particular Xbox controller origin. 
const char* Steam_Controller::GetGlyphForXboxOrigin(EXboxOrigin eOrigin)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    //k_EXboxOrigin_A,
    //k_EXboxOrigin_B,
    //k_EXboxOrigin_X,
    //k_EXboxOrigin_Y,
    //k_EXboxOrigin_LeftBumper,
    //k_EXboxOrigin_RightBumper,
    //k_EXboxOrigin_Menu,  //Start
    //k_EXboxOrigin_View,  //Back
    //k_EXboxOrigin_LeftTrigger_Pull,
    //k_EXboxOrigin_LeftTrigger_Click,
    //k_EXboxOrigin_RightTrigger_Pull,
    //k_EXboxOrigin_RightTrigger_Click,
    //k_EXboxOrigin_LeftStick_Move,
    //k_EXboxOrigin_LeftStick_Click,
    //k_EXboxOrigin_LeftStick_DPadNorth,
    //k_EXboxOrigin_LeftStick_DPadSouth,
    //k_EXboxOrigin_LeftStick_DPadWest,
    //k_EXboxOrigin_LeftStick_DPadEast,
    //k_EXboxOrigin_RightStick_Move,
    //k_EXboxOrigin_RightStick_Click,
    //k_EXboxOrigin_RightStick_DPadNorth,
    //k_EXboxOrigin_RightStick_DPadSouth,
    //k_EXboxOrigin_RightStick_DPadWest,
    //k_EXboxOrigin_RightStick_DPadEast,
    //k_EXboxOrigin_DPad_North,
    //k_EXboxOrigin_DPad_South,
    //k_EXboxOrigin_DPad_West,
    //k_EXboxOrigin_DPad_East,

    return "";
}

// Get the equivalent ActionOrigin for a given Xbox controller origin this can be chained with GetGlyphForActionOrigin to provide future proof glyphs for
// non-Steam Input API action games. Note - this only translates the buttons directly and doesn't take into account any remapping a user has made in their configuration
EControllerActionOrigin Steam_Controller::GetActionOriginFromXboxOrigin_(ControllerHandle_t controllerHandle, EXboxOrigin eOrigin)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized)
        return k_EControllerActionOrigin_None;

    return k_EControllerActionOrigin_None;
}

// Convert an origin to another controller type - for inputs not present on the other controller type this will return k_EControllerActionOrigin_None
EControllerActionOrigin Steam_Controller::TranslateActionOrigin(ESteamInputType eDestinationInputType, EControllerActionOrigin eSourceOrigin)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EControllerActionOrigin_None;
}

// Get the binding revision for a given device. Returns false if the handle was not valid or if a mapping is not yet loaded for the device
bool Steam_Controller::GetControllerBindingRevision(ControllerHandle_t controllerHandle, int* pMajor, int* pMinor)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized)
        return false;

    return false;
}

// Get the equivalent ActionOrigin for a given Xbox controller origin this can be chained with GetGlyphForActionOrigin to provide future proof glyphs for
    // non-Steam Input API action games. Note - this only translates the buttons directly and doesn't take into account any remapping a user has made in their configuration
EInputActionOrigin Steam_Controller::GetActionOriginFromXboxOrigin(InputHandle_t inputHandle, EXboxOrigin eOrigin)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized)
        return k_EInputActionOrigin_None;

    return k_EInputActionOrigin_None;
}

// Convert an origin to another controller type - for inputs not present on the other controller type this will return k_EInputActionOrigin_None
// When a new input type is added you will be able to pass in k_ESteamInputType_Unknown and the closest origin that your version of the SDK recognized will be returned
// ex: if a Playstation 5 controller was released this function would return Playstation 4 origins.
EInputActionOrigin Steam_Controller::TranslateActionOrigin(ESteamInputType eDestinationInputType, EInputActionOrigin eSourceOrigin)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EInputActionOrigin_None;
}

// Get the binding revision for a given device. Returns false if the handle was not valid or if a mapping is not yet loaded for the device
bool Steam_Controller::GetDeviceBindingRevision(InputHandle_t inputHandle, int* pMajor, int* pMinor)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized)
        return false;

    return false;
}

// Get the Steam Remote Play session ID associated with a device, or 0 if there is no session associated with it
// See isteamremoteplay.h for more information on Steam Remote Play sessions
uint32 Steam_Controller::GetRemotePlaySessionID(InputHandle_t inputHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (!_initialized)
        return 0;

    return 0;
}

///////////////////////////////////////////////////////////////////////////////
//                           Network Send messages                           //
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//                          Network Receive messages                         //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//                                 IRunCallback                              //
///////////////////////////////////////////////////////////////////////////////
bool Steam_Controller::CBRunFrame()
{
    RunFrame();

    return true;
}

bool Steam_Controller::RunNetwork(Network_Message_pb const& msg)
{
    return false;
}

bool Steam_Controller::RunCallbacks(pFrameResult_t res)
{
    return res->done;
}