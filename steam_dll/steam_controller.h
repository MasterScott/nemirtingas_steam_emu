/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"
#include <gamepad.h>

enum class activator_e
{
    none = 0,
    full_press,
    soft_press,
    start_press,
    release,
    long_press,
    double_press,
    analog,
    chord,
};

enum class digital_button_e
{
    none                      = 0 ,
    dpad_north                = 1 ,
    dpad_north_east           = 2 ,
    dpad_east                 = 3 ,
    dpad_south_east           = 4 ,
    dpad_south                = 5 ,
    dpad_south_west           = 6 ,
    dpad_west                 = 7 ,
    dpad_north_west           = 8 ,
    button_y                  = 9 ,
    button_b                  = 10,
    button_x                  = 11,
    button_a                  = 12,
    click                     = 13,
    doubletap                 = 14,
    edge                      = 15,
    scroll_clockwise          = 16,
    scroll_counterclockwise   = 17,
    trigger_analog            = 18,
    scroll_wheel_list_0       = 19, // ?
    scroll_wheel_list_1       = 20, // ?
    scroll_wheel_list_2       = 21, // ?
    scroll_wheel_list_3       = 22, // ?
    scroll_wheel_list_4       = 23, // ?
    scroll_wheel_list_5       = 24, // ?
    scroll_wheel_list_6       = 25, // ?
    scroll_wheel_list_7       = 26, // ?
    scroll_wheel_list_8       = 27, // ?
    scroll_wheel_list_9       = 28, // ?
    touch_menu_button_0       = 29, // ?
    touch_menu_button_1       = 30, // ?
    touch_menu_button_2       = 31, // ?
    touch_menu_button_3       = 32, // ?
    touch_menu_button_4       = 33, // ?
    touch_menu_button_5       = 34, // ?
    touch_menu_button_6       = 35, // ?
    touch_menu_button_7       = 36, // ?
    touch_menu_button_8       = 37, // ?
    touch_menu_button_9       = 38, // ?
    touch_menu_button_10      = 39, // ?
    touch_menu_button_11      = 40, // ?
    touch_menu_button_12      = 41, // ?
    touch_menu_button_13      = 42, // ?
    touch_menu_button_14      = 43, // ?
    touch_menu_button_15      = 44, // ?
    touch_menu_button_16      = 45, // ?
    touch_menu_button_17      = 46, // ?
    touch_menu_button_18      = 47, // ?
    touch_menu_button_19      = 48, // ?
    touch_menu_button_20      = 49, // ?
    touch                     = 50, // ?
    button_menu               = 51, // back/select
    button_escape             = 52, // start
    left_bumper               = 53, // left shoulder  (L1)
    right_bumper              = 54, // right shoulder (R1)
    button_back_left          = 55,
    button_back_right         = 56,
    button_back_left_upper    = 57,
    button_back_right_upper   = 58,
    left_click                = 59,
    button_lpad               = 59,
    right_click               = 60,
    button_rpad               = 60,
    left_trigger              = 61,
    button_ltrigger           = 61,
    right_trigger             = 62,
    button_rtrigger           = 62,
    left_trigger_threshold    = 63,
    button_ltrigger_threshold = 63,
    right_trigger_threshold   = 64,
    button_rtrigger_threshold = 64,
    left_stick_click          = 65,
    button_joystick           = 65,
    button_steam              = 66, // guide
    always_on_action          = 67,
    button_capture            = 68,
    button_macro0             = 69,
    button_macro1             = 70,
    button_macro2             = 71,
    button_macro3             = 72,
    button_macro4             = 73,
    button_macro5             = 74,
    button_macro6             = 75,
    button_macro7             = 76,
};

struct digital_binding_t
{
    std::string description;
    EInputSourceMode mode;
    digital_button_e button;
    activator_e activator;
    float threshold;
};

struct analog_binding_t
{
    std::string description;
    EInputSourceMode mode;
    EInputSource button;
};

struct set_action_binding_t
{
    std::string title;
    union
    {
        digital_binding_t* digital_binding;
        analog_binding_t* analog_binding;
    };
};

struct actions_set_t
{
    std::string title;
    bool legacy_set;
    std::map<std::string, set_action_binding_t> actions;
};


class LOCAL_API Steam_Controller :
    public IRunCallback,
    public ISteamController001,
    public ISteamController003,
    public ISteamController004,
    public ISteamController005,
    public ISteamController006,
    public ISteamController007,
    public ISteamInput001
{
    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;
    
    ControllerActionSetHandle_t _actionset_handle;
    ControllerDigitalActionHandle_t _digitalaction_handle;
    ControllerAnalogActionHandle_t _analogaction_handle;

    bool _initialized;

    std::unordered_map<std::string, digital_binding_t> _digital_bindings;
    std::unordered_map<std::string, analog_binding_t> _analog_bindings;
    std::unordered_map<std::string, actions_set_t> _actions_sets;

    std::unordered_map<std::string, std::unordered_map<std::string, std::string>> _localizations;

    std::unordered_map<std::string, std::string>* _current_localization;

    std::unordered_map<ControllerHandle_t, ControllerActionSetHandle_t> _controller_handle_actions_set;
    //                                    an _actions_sets entry
    std::unordered_map<ControllerActionSetHandle_t, std::pair<std::string, actions_set_t*>> _actions_set_handle;
    std::unordered_map<ControllerDigitalActionHandle_t, std::pair<std::string, digital_binding_t*>> _digital_handles;
    std::unordered_map<ControllerAnalogActionHandle_t , std::pair<std::string, analog_binding_t*>> _analog_handles;

public:
    std::recursive_mutex _local_mutex;

    Steam_Controller();
    ~Steam_Controller();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    std::string const& get_localization_string(std::string const& key);
    EInputSource str_to_analog_button(std::string button);
    digital_button_e str_to_digital_button(std::string button);
    EInputSourceMode str_to_SourceMode(std::string source_mode);
    activator_e str_to_activator(std::string activator);

    virtual bool CBRunFrame();
    virtual bool RunNetwork(Network_Message_pb const& msg);
    virtual bool RunCallbacks(pFrameResult_t res);

    // Init and Shutdown must be called when starting/ending use of this interface
    virtual bool Init(const char* pchAbsolutePathToControllerConfigVDF);
    virtual bool Init();
    virtual bool Shutdown();

    // Synchronize API state with the latest Steam Controller inputs available. This
    // is performed automatically by SteamAPI_RunCallbacks, but for the absolute lowest
    // possible latency, you call this directly before reading controller state. This must
    // be called from somewhere before GetConnectedControllers will return any handles
    virtual void RunFrame();

    // Get the state of the specified controller, returns false if that controller is not connected
    virtual bool GetControllerState(uint32 unControllerIndex, SteamControllerState001_t* pState);

    // Trigger a haptic pulse on the controller
    virtual void TriggerHapticPulse(uint32 unControllerIndex, ESteamControllerPad eTargetPad, unsigned short usDurationMicroSec);

    // Set the override mode which is used to choose to use different base/legacy bindings from your config file
    virtual void SetOverrideMode(const char* pchMode);

    // Enumerate currently connected controllers
    // handlesOut should point to a STEAM_CONTROLLER_MAX_COUNT sized array of ControllerHandle_t handles
    // Returns the number of handles written to handlesOut
    virtual int GetConnectedControllers(STEAM_OUT_ARRAY_COUNT(STEAM_CONTROLLER_MAX_COUNT, Receives list of connected controllers) ControllerHandle_t* handlesOut);

    //-----------------------------------------------------------------------------
    // ACTION SETS
    //-----------------------------------------------------------------------------

    // Lookup the handle for an Action Set. Best to do this once on startup, and store the handles for all future API calls.
    virtual ControllerActionSetHandle_t GetActionSetHandle(const char* pszActionSetName);

    // Reconfigure the controller to use the specified action set (ie 'Menu', 'Walk' or 'Drive')
    // This is cheap, and can be safely called repeatedly. It's often easier to repeatedly call it in
    // your state loops, instead of trying to place it in all of your state transitions.
    virtual void ActivateActionSet(ControllerHandle_t controllerHandle, ControllerActionSetHandle_t actionSetHandle);
    virtual ControllerActionSetHandle_t GetCurrentActionSet(ControllerHandle_t controllerHandle);

    // ACTION SET LAYERS
    virtual void ActivateActionSetLayer(ControllerHandle_t controllerHandle, ControllerActionSetHandle_t actionSetLayerHandle);
    virtual void DeactivateActionSetLayer(ControllerHandle_t controllerHandle, ControllerActionSetHandle_t actionSetLayerHandle);
    virtual void DeactivateAllActionSetLayers(ControllerHandle_t controllerHandle);
    // Enumerate currently active layers
    // handlesOut should point to a STEAM_CONTROLLER_MAX_ACTIVE_LAYERS sized array of ControllerActionSetHandle_t handles.
    // Returns the number of handles written to handlesOut
    virtual int GetActiveActionSetLayers(ControllerHandle_t controllerHandle, STEAM_OUT_ARRAY_COUNT(STEAM_CONTROLLER_MAX_ACTIVE_LAYERS, Receives list of active layers) ControllerActionSetHandle_t* handlesOut);

    //-----------------------------------------------------------------------------
    // ACTIONS
    //-----------------------------------------------------------------------------

    // Lookup the handle for a digital action. Best to do this once on startup, and store the handles for all future API calls.
    virtual ControllerDigitalActionHandle_t GetDigitalActionHandle(const char* pszActionName);

    // Returns the current state of the supplied digital game action
    virtual ControllerDigitalActionData_t GetDigitalActionData(ControllerHandle_t controllerHandle, ControllerDigitalActionHandle_t digitalActionHandle);

    // Get the origin(s) for a digital action within an action set. Returns the number of origins supplied in originsOut. Use this to display the appropriate on-screen prompt for the action.
    // originsOut should point to a STEAM_CONTROLLER_MAX_ORIGINS sized array of EControllerActionOrigin handles. The EControllerActionOrigin enum will get extended as support for new controller controllers gets added to
    // the Steam client and will exceed the values from this header, please check bounds if you are using a look up table.
    virtual int GetDigitalActionOrigins(ControllerHandle_t controllerHandle, ControllerActionSetHandle_t actionSetHandle, ControllerDigitalActionHandle_t digitalActionHandle, STEAM_OUT_ARRAY_COUNT(STEAM_CONTROLLER_MAX_ORIGINS, Receives list of aciton origins) EControllerActionOrigin* originsOut);
    virtual int GetDigitalActionOrigins(InputHandle_t inputHandle, InputActionSetHandle_t actionSetHandle, InputDigitalActionHandle_t digitalActionHandle, STEAM_OUT_ARRAY_COUNT(STEAM_INPUT_MAX_ORIGINS, Receives list of action origins) EInputActionOrigin* originsOut);

    // Lookup the handle for an analog action. Best to do this once on startup, and store the handles for all future API calls.
    virtual ControllerAnalogActionHandle_t GetAnalogActionHandle(const char* pszActionName);

    // Returns the current state of these supplied analog game action
    virtual ControllerAnalogActionData_t GetAnalogActionData(ControllerHandle_t controllerHandle, ControllerAnalogActionHandle_t analogActionHandle);

    // Get a local path to art for on-screen glyph for a particular origin
    virtual const char* GetGlyphForActionOrigin(EInputActionOrigin eOrigin);

    // Returns a localized string (from Steam's language setting) for the specified origin.
    virtual const char* GetStringForActionOrigin(EInputActionOrigin eOrigin);

    // Get the origin(s) for an analog action within an action set. Returns the number of origins supplied in originsOut. Use this to display the appropriate on-screen prompt for the action.
    // originsOut should point to a STEAM_CONTROLLER_MAX_ORIGINS sized array of EControllerActionOrigin handles. The EControllerActionOrigin enum will get extended as support for new controller controllers gets added to
    // the Steam client and will exceed the values from this header, please check bounds if you are using a look up table.
    virtual int GetAnalogActionOrigins(ControllerHandle_t controllerHandle, ControllerActionSetHandle_t actionSetHandle, ControllerAnalogActionHandle_t analogActionHandle, STEAM_OUT_ARRAY_COUNT(STEAM_CONTROLLER_MAX_ORIGINS, Receives list of action origins) EControllerActionOrigin* originsOut);
    virtual int GetAnalogActionOrigins(InputHandle_t inputHandle, InputActionSetHandle_t actionSetHandle, InputAnalogActionHandle_t analogActionHandle, STEAM_OUT_ARRAY_COUNT(STEAM_INPUT_MAX_ORIGINS, Receives list of action origins) EInputActionOrigin* originsOut);

    // Get a local path to art for on-screen glyph for a particular origin - this call is cheap
    virtual const char* GetGlyphForActionOrigin(EControllerActionOrigin eOrigin);

    // Returns a localized string (from Steam's language setting) for the specified origin - this call is serialized
    virtual const char* GetStringForActionOrigin(EControllerActionOrigin eOrigin);

    virtual void StopAnalogActionMomentum(ControllerHandle_t controllerHandle, ControllerAnalogActionHandle_t eAction);

    // Returns raw motion data from the specified controller
    virtual ControllerMotionData_t GetMotionData(ControllerHandle_t controllerHandle);

    virtual bool ShowDigitalActionOrigins(ControllerHandle_t controllerHandle, ControllerDigitalActionHandle_t digitalActionHandle, float flScale, float flXPosition, float flYPosition);

    virtual bool ShowAnalogActionOrigins(ControllerHandle_t controllerHandle, ControllerAnalogActionHandle_t analogActionHandle, float flScale, float flXPosition, float flYPosition);

    //-----------------------------------------------------------------------------
    // OUTPUTS
    //-----------------------------------------------------------------------------

    // Trigger a haptic pulse on a controller
    virtual void TriggerHapticPulse(ControllerHandle_t controllerHandle, ESteamControllerPad eTargetPad, unsigned short usDurationMicroSec);

    // Trigger a pulse with a duty cycle of usDurationMicroSec / usOffMicroSec, unRepeat times.
    // nFlags is currently unused and reserved for future use.
    virtual void TriggerRepeatedHapticPulse(ControllerHandle_t controllerHandle, ESteamControllerPad eTargetPad, unsigned short usDurationMicroSec, unsigned short usOffMicroSec, unsigned short unRepeat, unsigned int nFlags);

    // Trigger a vibration event on supported controllers.  
    virtual void TriggerVibration(ControllerHandle_t controllerHandle, unsigned short usLeftSpeed, unsigned short usRightSpeed);

    // Set the controller LED color on supported controllers.  
    virtual void SetLEDColor(ControllerHandle_t controllerHandle, uint8 nColorR, uint8 nColorG, uint8 nColorB, unsigned int nFlags);

    //-----------------------------------------------------------------------------
    // Utility functions availible without using the rest of Steam Input API
    //-----------------------------------------------------------------------------

    // Invokes the Steam overlay and brings up the binding screen if the user is using Big Picture Mode
    // If the user is not in Big Picture Mode it will open up the binding in a new window
    virtual bool ShowBindingPanel(ControllerHandle_t controllerHandle);

    // Returns the input type for a particular handle
    virtual ESteamInputType GetInputTypeForHandle(ControllerHandle_t controllerHandle);

    // Returns the associated controller handle for the specified emulated gamepad - can be used with the above 2 functions
    // to identify controllers presented to your game over Xinput. Returns 0 if the Xinput index isn't associated with Steam Input
    virtual ControllerHandle_t GetControllerForGamepadIndex(int nIndex);

    // Returns the associated gamepad index for the specified controller, if emulating a gamepad or -1 if not associated with an Xinput index
    virtual int GetGamepadIndexForController(ControllerHandle_t ulControllerHandle);

    // Returns a localized string (from Steam's language setting) for the specified Xbox controller origin.
    virtual const char* GetStringForXboxOrigin(EXboxOrigin eOrigin);

    // Get a local path to art for on-screen glyph for a particular Xbox controller origin. 
    virtual const char* GetGlyphForXboxOrigin(EXboxOrigin eOrigin);

    // Get the equivalent ActionOrigin for a given Xbox controller origin this can be chained with GetGlyphForActionOrigin to provide future proof glyphs for
    // non-Steam Input API action games. Note - this only translates the buttons directly and doesn't take into account any remapping a user has made in their configuration
    virtual EControllerActionOrigin GetActionOriginFromXboxOrigin_(ControllerHandle_t controllerHandle, EXboxOrigin eOrigin);

    // Convert an origin to another controller type - for inputs not present on the other controller type this will return k_EControllerActionOrigin_None
    virtual EControllerActionOrigin TranslateActionOrigin(ESteamInputType eDestinationInputType, EControllerActionOrigin eSourceOrigin);

    // Get the binding revision for a given device. Returns false if the handle was not valid or if a mapping is not yet loaded for the device
    virtual bool GetControllerBindingRevision(ControllerHandle_t controllerHandle, int* pMajor, int* pMinor);

    // Get the equivalent ActionOrigin for a given Xbox controller origin this can be chained with GetGlyphForActionOrigin to provide future proof glyphs for
    // non-Steam Input API action games. Note - this only translates the buttons directly and doesn't take into account any remapping a user has made in their configuration
    virtual EInputActionOrigin GetActionOriginFromXboxOrigin(InputHandle_t inputHandle, EXboxOrigin eOrigin);

    // Convert an origin to another controller type - for inputs not present on the other controller type this will return k_EInputActionOrigin_None
    // When a new input type is added you will be able to pass in k_ESteamInputType_Unknown and the closest origin that your version of the SDK recognized will be returned
    // ex: if a Playstation 5 controller was released this function would return Playstation 4 origins.
    virtual EInputActionOrigin TranslateActionOrigin(ESteamInputType eDestinationInputType, EInputActionOrigin eSourceOrigin);

    // Get the binding revision for a given device. Returns false if the handle was not valid or if a mapping is not yet loaded for the device
    virtual bool GetDeviceBindingRevision(InputHandle_t inputHandle, int* pMajor, int* pMinor);

    // Get the Steam Remote Play session ID associated with a device, or 0 if there is no session associated with it
    // See isteamremoteplay.h for more information on Steam Remote Play sessions
    virtual uint32 GetRemotePlaySessionID(InputHandle_t inputHandle);
};