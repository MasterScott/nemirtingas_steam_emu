/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_friends.h"
#include "steam_client.h"
#include "settings.h"

constexpr decltype(Steam_Friends::friend_infos_rate)    Steam_Friends::friend_infos_rate;

Steam_Friends::Steam_Friends()
{
}

Steam_Friends::~Steam_Friends()
{
    emu_deinit();
}

void Steam_Friends::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock(_local_mutex, cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(cb_manager->_local_mutex, std::adopt_lock);

    _cb_manager = cb_manager;
    _network = network;

    _cb_manager->register_frame(this);
    _cb_manager->register_callbacks(this);
    _network->register_listener(this, Network_Message_pb::MessagesCase::kFriends);
    _network->register_listener(this, Network_Message_pb::MessagesCase::kNetworkAdvertise);

    auto& steam_user = GetSteam_User();
    auto& steam_utils = GetSteam_Utils();
    auto& steam_overlay = GetSteam_Overlay();

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();
    // Create the first entry in friend list: ourself
    friend_state_t& frd_state = _friends[steam_id];
    frd_state.infos.set_level(steam_user.GetPlayerSteamLevel());
    frd_state.infos.set_name(Settings::Inst().username.c_str());
    frd_state.infos.set_state(k_EPersonaStateOnline);
    frd_state.app_id = Settings::Inst().gameid.AppID();

    APP_LOG(Log::LogLevel::INFO, "Creating steam avatar images");

    auto img = ImageManager::get_image(user_avatar_id);

    frd_state.small_image_handle = steam_utils.generate_image_handle(img, 32, 32);
    frd_state.medium_image_handle = steam_utils.generate_image_handle(img, 64, 64);
    frd_state.large_image_handle = steam_utils.generate_image_handle(img, 184, 184);
}

void Steam_Friends::emu_deinit()
{
    TRACE_FUNC();
    if (_cb_manager != nullptr)
    {
        std::lock(_local_mutex, _cb_manager->_local_mutex);
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
        std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

        _cb_manager->unregister_callbacks(this);
        _cb_manager->unregister_frame(this);
    }
    if (_network != nullptr)
    {
        _network->unregister_listener(this, Network_Message_pb::MessagesCase::kNetworkAdvertise);
        _network->unregister_listener(this, Network_Message_pb::MessagesCase::kFriends);
    }

    {
        std::lock_guard<std::recursive_mutex> lk(_local_mutex);
        _network.reset();
        _cb_manager.reset();
    }
}

void Steam_Friends::trigger_persona_state_change(uint64 friend_id, EPersonaChange new_state)
{
    pFrameResult_t result(new FrameResult);
    PersonaStateChange_t& psc = result->CreateCallback<PersonaStateChange_t>();

    psc.m_nChangeFlags = k_EPersonaChangeName;
    psc.m_ulSteamID = myself()->first;

    result->done = true;
    _cb_manager->add_next_callback(this, result);
}

void Steam_Friends::new_friend(uint64 friend_id)
{
    auto& friend_ = _friends[friend_id];

    friend_.has_avatar = false;
    friend_.connected = false;
    friend_.relationship = k_EFriendRelationshipFriend;
}

void Steam_Friends::disconnect_friend(decltype(Steam_Friends::_friends)::iterator friend_it)
{
    assert(friend_it != _friends.end());

    trigger_persona_state_change(friend_it->first, k_EPersonaChangeGoneOffline);

    friend_it->second.infos.set_state(k_EPersonaStateOffline);

    Network_Message_pb msg;
    Network_Advertise_pb* adv = new Network_Advertise_pb;
    Network_Peer_Disconnect_pb* disc = new Network_Peer_Disconnect_pb;

    adv->set_allocated_peer_disconnect(disc);
    msg.set_allocated_network_advertise(adv);

    msg.set_source_id(friend_it->first);

    auto& overlay = GetSteam_Overlay();
    auto& matchmaking = GetSteam_Matchmaking();
    auto& networking = GetSteam_Networking();
    auto& networking_socket = GetSteam_NetworkingSockets();

    overlay.FriendDisconnect(static_cast<uint64>(msg.source_id()));
    matchmaking.on_peer_disconnect(msg, *disc);
    networking.on_peer_disconnect(msg, *disc);
    networking_socket.on_peer_disconnect(msg, *disc);
}

typename decltype(Steam_Friends::_friends)::iterator Steam_Friends::myself()
{
    return _friends.begin();
}

typename decltype(Steam_Friends::_friends)::iterator Steam_Friends::get_friend(uint64 id)
{
    return _friends.find(id);
}

// returns the local players name - guaranteed to not be NULL.
// this is the same name as on the users community profile page
// this is stored in UTF-8 format
// like all the other interface functions that return a char *, it's important that this pointer is not saved
// off; it will eventually be free'd or re-allocated
const char* Steam_Friends::GetPersonaName()
{
    //LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
    return myself()->second.infos.name().c_str();
}

// Sets the player name, stores it on the server and publishes the changes to all friends who are online.
// Changes take place locally immediately, and a PersonaStateChange_t is posted, presuming success.
//
// The final results are available through the return value SteamAPICall_t, using SetPersonaNameResponse_t.
//
// If the name change fails to happen on the server, then an additional global PersonaStateChange_t will be posted
// to change the name back, in addition to the SetPersonaNameResponse_t callback.
STEAM_CALL_RESULT(SetPersonaNameResponse_t)
SteamAPICall_t Steam_Friends::SetPersonaName(const char* pchPersonaName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    pFrameResult_t result(new FrameResult);
    result->CreateCallback<SetPersonaNameResponse_t>();
    _cb_manager->add_apicall(this, result);
    result->done = true;
    // This will not fail

    if (pchPersonaName != nullptr && *pchPersonaName)
    {
        myself()->second.infos.set_name(pchPersonaName);
    }
    else
    {// If it fails, send again a PersonaChangeName
        trigger_persona_state_change(myself()->first, k_EPersonaChangeName);
    }

    return result->GetAPICall();
}

void Steam_Friends::SetPersonaName_old(const char* pchPersonaName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    SetPersonaName(pchPersonaName);
}

// gets the status of the current user
EPersonaState Steam_Friends::GetPersonaState()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
    return (EPersonaState)myself()->second.infos.state();
}

// friend iteration
// takes a set of k_EFriendFlags, and returns the number of users the client knows about who meet that criteria
// then GetFriendByIndex() can then be used to return the id's of each of those users
int Steam_Friends::GetFriendCount(EFriendFlags eFriendFlags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return GetFriendCount(static_cast<int>(eFriendFlags));
}

int Steam_Friends::GetFriendCount(int iFriendFlags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO: Get friends by flag");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (iFriendFlags & k_EFriendFlagImmediate ||
        iFriendFlags & k_EFriendFlagAll)
    {
        return _friends.size() - 1;
    }
    
    return 0;
}

// returns the steamID of a user
// iFriend is a index of range [0, GetFriendCount())
// iFriendsFlags must be the same value as used in GetFriendCount()
// the returned CSteamID can then be used by all the functions below to access details about the user
CSteamID Steam_Friends::GetFriendByIndex(int iFriend, EFriendFlags eFriendFlags)
{
    return GetFriendByIndex(iFriend, static_cast<int>(eFriendFlags));
}

CSteamID Steam_Friends::GetFriendByIndex(int iFriend, int iFriendFlags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto it = _friends.begin();
    std::advance(it, iFriend + 1);
    return it->first;
}

// returns a relationship to a user
EFriendRelationship Steam_Friends::GetFriendRelationship(CSteamID steamIDFriend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto finfos_it = get_friend(steamIDFriend.ConvertToUint64());
    if (finfos_it == _friends.end())
        return k_EFriendRelationshipNone;

    return finfos_it->second.relationship;
}

// returns the current status of the specified user
// this will only be known by the local user if steamIDFriend is in their friends list; on the same game server; in a chat room or lobby; or in a small group with the local user
EPersonaState Steam_Friends::GetFriendPersonaState(CSteamID steamIDFriend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto finfos_it = get_friend(steamIDFriend.ConvertToUint64());
    if (finfos_it == _friends.end())
        return k_EPersonaStateOffline;

    return (EPersonaState)finfos_it->second.infos.state();
}

// returns the name another user - guaranteed to not be NULL.
// same rules as GetFriendPersonaState() apply as to whether or not the user knowns the name of the other user
// note that on first joining a lobby, chat room or game server the local user will not known the name of the other users automatically; that information will arrive asyncronously
// 
const char* Steam_Friends::GetFriendPersonaName(CSteamID steamIDFriend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto finfos_it = get_friend(steamIDFriend.ConvertToUint64());
    if (finfos_it == _friends.end())
        return "";

    return finfos_it->second.infos.name().c_str();
}

int Steam_Friends::GetFriendAvatar(CSteamID steamIDFriend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return GetFriendAvatar(steamIDFriend, 32);
}

int Steam_Friends::GetFriendAvatar(CSteamID steamIDFriend, int eAvatarSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    switch (eAvatarSize)
    {
        case 32:
            return GetSmallFriendAvatar(steamIDFriend);

        case 64:
            return GetMediumFriendAvatar(steamIDFriend);

        case 184:
            return GetLargeFriendAvatar(steamIDFriend);
    }

    return 0;
}

// returns true if the friend is actually in a game, and fills in pFriendGameInfo with an extra details 
bool Steam_Friends::GetFriendGamePlayed(CSteamID steamIDFriend, STEAM_OUT_STRUCT() FriendGameInfo_t* pFriendGameInfo)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
        
    auto finfos_it = get_friend(steamIDFriend.ConvertToUint64());
    if (finfos_it == _friends.end())
        return false;

    bool res = true;

    if (pFriendGameInfo != nullptr)
    {
        pFriendGameInfo->m_steamIDLobby = static_cast<uint64>(finfos_it->second.infos.lobby_id());
        pFriendGameInfo->m_gameID = CGameID(static_cast<uint32>(finfos_it->second.app_id));
        pFriendGameInfo->m_unGameIP = finfos_it->second.infos.game_ip();
        pFriendGameInfo->m_usGamePort = finfos_it->second.infos.game_port();
        pFriendGameInfo->m_usQueryPort = finfos_it->second.infos.query_port();
    }

    return res;
}

bool Steam_Friends::GetFriendGamePlayed(CSteamID steamIDFriend, uint64* pulGameID, uint32* punGameIP, uint16* pusGamePort, uint16* pusQueryPort)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    FriendGameInfo_t friend_game_infos;
    auto res = GetFriendGamePlayed(steamIDFriend, &friend_game_infos);
    if (res)
    {
        if (pulGameID != nullptr)
            *pulGameID = *friend_game_infos.m_gameID.GetUint64Ptr();

        if (punGameIP != nullptr)
            *punGameIP = friend_game_infos.m_unGameIP;

        if (pusGamePort != nullptr)
            *pusGamePort = friend_game_infos.m_usGamePort;

        if (pusQueryPort != nullptr)
            *pusQueryPort = friend_game_infos.m_usQueryPort;
    }

    return true;
}

// accesses old friends names - returns an empty string when their are no more items in the history
const char* Steam_Friends::GetFriendPersonaNameHistory(CSteamID steamIDFriend, int iPersonaName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return u8"";
}
// friends steam level
int Steam_Friends::GetFriendSteamLevel(CSteamID steamIDFriend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto finfos_it = get_friend(steamIDFriend.ConvertToUint64());
    if (finfos_it == _friends.end())
        return 0;

    return finfos_it->second.infos.level();
}

// Returns nickname the current user has set for the specified player. Returns NULL if the no nickname has been set for that player.
// DEPRECATED: GetPersonaName follows the Steam nickname preferences, so apps shouldn't need to care about nicknames explicitly.
const char* Steam_Friends::GetPlayerNickname(CSteamID steamIDPlayer)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto finfos_it = get_friend(steamIDPlayer.ConvertToUint64());
    if (finfos_it == _friends.end())
        return u8"Unknown Friend";

    return finfos_it->second.infos.name().c_str();
}

// friend grouping (tag) apis
// returns the number of friends groups
int Steam_Friends::GetFriendsGroupCount()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}
// returns the friends group ID for the given index (invalid indices return k_FriendsGroupID_Invalid)
FriendsGroupID_t Steam_Friends::GetFriendsGroupIDByIndex(int iFG)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}
// returns the name for the given friends group (NULL in the case of invalid friends group IDs)
const char* Steam_Friends::GetFriendsGroupName(FriendsGroupID_t friendsGroupID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return "";
}
// returns the number of members in a given friends group
int Steam_Friends::GetFriendsGroupMembersCount(FriendsGroupID_t friendsGroupID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}
// gets up to nMembersCount members of the given friends group, if fewer exist than requested those positions' SteamIDs will be invalid
void Steam_Friends::GetFriendsGroupMembersList(FriendsGroupID_t friendsGroupID, STEAM_OUT_ARRAY_CALL(nMembersCount, GetFriendsGroupMembersCount, friendsGroupID) CSteamID* pOutSteamIDMembers, int nMembersCount)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// returns true if the specified user meets any of the criteria specified in iFriendFlags
// iFriendFlags can be the union (binary or, |) of one or more k_EFriendFlags values
bool Steam_Friends::HasFriend(CSteamID steamIDFriend, EFriendFlags eFriendFlags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return HasFriend(steamIDFriend, static_cast<int>(eFriendFlags));
}

bool Steam_Friends::HasFriend(CSteamID steamIDFriend, int iFriendFlags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    int refuse_flags = k_EFriendFlagBlocked | k_EFriendFlagIgnored | k_EFriendFlagIgnoredFriend | k_EFriendFlagFriendshipRequested;

    auto finfos_it = get_friend(steamIDFriend.ConvertToUint64());
    if (finfos_it == _friends.end()
     || iFriendFlags & refuse_flags)
        return false;

    return true;
}

// clan (group) iteration and access functions
int Steam_Friends::GetClanCount()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

CSteamID Steam_Friends::GetClanByIndex(int iClan)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_steamIDNil;
}

const char* Steam_Friends::GetClanName(CSteamID steamIDClan)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return u8"";
}

const char* Steam_Friends::GetClanTag(CSteamID steamIDClan)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return u8"";
}
// returns the most recent information we have about what's happening in a clan
bool Steam_Friends::GetClanActivityCounts(CSteamID steamIDClan, int* pnOnline, int* pnInGame, int* pnChatting)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}
// for clans a user is a member of, they will have reasonably up-to-date information, but for others you'll have to download the info to have the latest
SteamAPICall_t Steam_Friends::DownloadClanActivityCounts(STEAM_ARRAY_COUNT(cClansToRequest) CSteamID* psteamIDClans, int cClansToRequest)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// iterators for getting users in a chat room, lobby, game server or clan
// note that large clans that cannot be iterated by the local user
// note that the current user must be in a lobby to retrieve CSteamIDs of other users in that lobby
// steamIDSource can be the steamID of a group, game server, lobby or chat room
int Steam_Friends::GetFriendCountFromSource(CSteamID steamIDSource)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

CSteamID Steam_Friends::GetFriendFromSourceByIndex(CSteamID steamIDSource, int iFriend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_steamIDNil;
}

// returns true if the local user can see that steamIDUser is a member or in steamIDSource
bool Steam_Friends::IsUserInSource(CSteamID steamIDUser, CSteamID steamIDSource)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    if (steamIDUser == Settings::Inst().userid)
    {
        auto& matchmaking = GetSteam_Matchmaking();
        auto it = std::find_if(matchmaking._joined_lobbies.begin(), matchmaking._joined_lobbies.end(), [&steamIDSource]( std::pair<uint64 const, lobby_state_t*>& lobby)
        {
            return lobby.first == steamIDSource.ConvertToUint64();
        });
        return it != matchmaking._joined_lobbies.end();
    }
    else
    {
        auto frd = get_friend(steamIDUser.ConvertToUint64());
        if (frd != _friends.end())
        {
            return frd->second.infos.lobby_id() == steamIDSource.ConvertToUint64();
        }
    }

    return false;
}

// User is in a game pressing the talk button (will suppress the microphone for all voice comms from the Steam friends UI)
void Steam_Friends::SetInGameVoiceSpeaking(CSteamID steamIDUser, bool bSpeaking)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// activates the game overlay, with an optional dialog to open 
// valid options include "Friends", "Community", "Players", "Settings", "OfficialGameGroup", "Stats", "Achievements",
// "chatroomgroup/nnnn"
void Steam_Friends::ActivateGameOverlay(const char* pchDialog)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    auto& overlay = GetSteam_Overlay();
    overlay.ShowOverlay(true);
}

// activates game overlay to a specific place
// valid options are
//		"steamid" - opens the overlay web browser to the specified user or groups profile
//		"chat" - opens a chat window to the specified user, or joins the group chat 
//		"jointrade" - opens a window to a Steam Trading session that was started with the ISteamEconomy/StartTrade Web API
//		"stats" - opens the overlay web browser to the specified user's stats
//		"achievements" - opens the overlay web browser to the specified user's achievements
//		"friendadd" - opens the overlay in minimal mode prompting the user to add the target user as a friend
//		"friendremove" - opens the overlay in minimal mode prompting the user to remove the target friend
//		"friendrequestaccept" - opens the overlay in minimal mode prompting the user to accept an incoming friend invite
//		"friendrequestignore" - opens the overlay in minimal mode prompting the user to ignore an incoming friend invite
void Steam_Friends::ActivateGameOverlayToUser(const char* pchDialog, CSteamID steamID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    auto& overlay = GetSteam_Overlay();
    overlay.ShowOverlay(true);
}

// activates game overlay web browser directly to the specified URL
// full address with protocol type is required, e.g. http://www.steamgames.com/
void Steam_Friends::ActivateGameOverlayToWebPage(const char* pchURL)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    ActivateGameOverlayToWebPage(pchURL, k_EActivateGameOverlayToWebPageMode_Default);
}

void Steam_Friends::ActivateGameOverlayToWebPage(const char* pchURL, EActivateGameOverlayToWebPageMode eMode)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    auto& overlay = GetSteam_Overlay();
    overlay.ShowOverlay(true);
}

// activates game overlay to store page for app
void Steam_Friends::ActivateGameOverlayToStore(AppId_t nAppID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    return ActivateGameOverlayToStore(nAppID, k_EOverlayToStoreFlag_None);
}

void Steam_Friends::ActivateGameOverlayToStore(AppId_t nAppID, EOverlayToStoreFlag eFlag)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    auto& overlay = GetSteam_Overlay();
    overlay.ShowOverlay(true);
}

// Mark a target user as 'played with'. This is a client-side only feature that requires that the calling user is 
// in game 
void Steam_Friends::SetPlayedWith(CSteamID steamIDUserPlayedWith)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// activates game overlay to open the invite dialog. Invitations will be sent for the provided lobby.
void Steam_Friends::ActivateGameOverlayInviteDialog(CSteamID steamIDLobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    auto& overlay = GetSteam_Overlay();
    overlay.ShowOverlay(true);
}

// gets the small (32x32) avatar of the current user, which is a handle to be used in IClientUtils::GetImageRGBA(), or 0 if none set
int Steam_Friends::GetSmallFriendAvatar(CSteamID steamIDFriend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto finfos_it = get_friend(steamIDFriend.ConvertToUint64());
    if (finfos_it == _friends.end())
        return 0;

    return finfos_it->second.small_image_handle;
}

// gets the medium (64x64) avatar of the current user, which is a handle to be used in IClientUtils::GetImageRGBA(), or 0 if none set
int Steam_Friends::GetMediumFriendAvatar(CSteamID steamIDFriend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto finfos_it = get_friend(steamIDFriend.ConvertToUint64());
    if (finfos_it == _friends.end())
        return 0;

    return finfos_it->second.medium_image_handle;
}

// gets the large (184x184) avatar of the current user, which is a handle to be used in IClientUtils::GetImageRGBA(), or 0 if none set
// returns -1 if this image has yet to be loaded, in this case wait for a AvatarImageLoaded_t callback and then call this again
int Steam_Friends::GetLargeFriendAvatar(CSteamID steamIDFriend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto finfos_it = get_friend(steamIDFriend.ConvertToUint64());
    if (finfos_it == _friends.end())
        return 0;

    return finfos_it->second.large_image_handle;
}

// requests information about a user - persona name & avatar
// if bRequireNameOnly is set, then the avatar of a user isn't downloaded 
// - it's a lot slower to download avatars and churns the local cache, so if you don't need avatars, don't request them
// if returns true, it means that data is being requested, and a PersonaStateChange_t callback will be posted when it's retrieved
// if returns false, it means that we already have all the details about that user, and functions can be called immediately
bool Steam_Friends::RequestUserInformation(CSteamID steamIDUser, bool bRequireNameOnly)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto finfos_it = _friends.find(steamIDUser.ConvertToUint64());
    if (finfos_it == _friends.end() || !finfos_it->second.connected)
    {
        return true;
    }

    //pFrameResult_t result(new FrameResult_t);
    //PersonaStateChange_t& psc = result->CreateCallback<PersonaStateChange_t>();
    //psc.m_ulSteamID = steamIDUser.ConvertToUint64();
    //psc.m_nChangeFlags = k_EPersonaChangeName;
    //if (!bRequireNameOnly)
    //{
    //    psc.m_nChangeFlags |= k_EPersonaChangeAvatar;
    //}
    //
    //GetCBManager(_pipe).add_callback(this, result);

    return false;
}

// requests information about a clan officer list
// when complete, data is returned in ClanOfficerListResponse_t call result
// this makes available the calls below
// you can only ask about clans that a user is a member of
// note that this won't download avatars automatically; if you get an officer,
// and no avatar image is available, call RequestUserInformation( steamID, false ) to download the avatar
STEAM_CALL_RESULT(ClanOfficerListResponse_t)
SteamAPICall_t Steam_Friends::RequestClanOfficerList(CSteamID steamIDClan)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// iteration of clan officers - can only be done when a RequestClanOfficerList() call has completed

// returns the steamID of the clan owner
CSteamID Steam_Friends::GetClanOwner(CSteamID steamIDClan)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_steamIDNil;
}
// returns the number of officers in a clan (including the owner)
int Steam_Friends::GetClanOfficerCount(CSteamID steamIDClan)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}
// returns the steamID of a clan officer, by index, of range [0,GetClanOfficerCount)
CSteamID Steam_Friends::GetClanOfficerByIndex(CSteamID steamIDClan, int iOfficer)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_steamIDNil;
}

// if current user is chat restricted, he can't send or receive any text/voice chat messages.
// the user can't see custom avatars. But the user can be online and send/recv game invites.
// a chat restricted user can't add friends or join any groups.
EUserRestriction Steam_Friends::GetUserRestrictions_old()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return static_cast<EUserRestriction>(GetUserRestrictions());
}

uint32 Steam_Friends::GetUserRestrictions()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_nUserRestrictionNone;
}

// Rich Presence data is automatically shared between friends who are in the same game
// Each user has a set of Key/Value pairs
// Note the following limits: k_cchMaxRichPresenceKeys, k_cchMaxRichPresenceKeyLength, k_cchMaxRichPresenceValueLength
// There are two magic keys:
//		"status"  - a UTF-8 string that will show up in the 'view game info' dialog in the Steam friends list
//		"connect" - a UTF-8 string that contains the command-line for how a friend can connect to a game
// GetFriendRichPresence() returns an empty string "" if no value is set
// SetRichPresence() to a NULL or an empty string deletes the key
// You can iterate the current set of keys for a friend with GetFriendRichPresenceKeyCount()
// and GetFriendRichPresenceKeyByIndex() (typically only used for debugging)
bool Steam_Friends::SetRichPresence(const char* pchKey, const char* pchValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (pchKey == nullptr || *pchKey == '\0')
        return false;

    auto& presence = *myself()->second.rich_presence.mutable_rich_presence();
    if (pchValue == nullptr || *pchValue == 0)
    {
        auto it = presence.find(pchKey);
        if (it != presence.end())
        {
            presence.erase(it);
        }
    }
    else
    {
        presence[pchKey] = pchValue;
    }

    return true;
}

void Steam_Friends::ClearRichPresence()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    myself()->second.rich_presence.mutable_rich_presence()->clear();
}

const char* Steam_Friends::GetFriendRichPresence(CSteamID steamIDFriend, const char* pchKey)
{
    //LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (pchKey == nullptr || *pchKey == '\0')
        return "";

    auto finfos_it = get_friend(steamIDFriend.ConvertToUint64());
    if (finfos_it == _friends.end())
        return "";

    auto& presence = finfos_it->second.rich_presence.rich_presence();
    auto it = presence.find(pchKey);
    if (it == presence.end())
        return "";

    return it->second.c_str();
}

int Steam_Friends::GetFriendRichPresenceKeyCount(CSteamID steamIDFriend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto finfos_it = get_friend(steamIDFriend.ConvertToUint64());
    if (finfos_it == _friends.end())
        return 0;

    return finfos_it->second.rich_presence.rich_presence_size();
}

const char* Steam_Friends::GetFriendRichPresenceKeyByIndex(CSteamID steamIDFriend, int iKey)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto finfos_it = get_friend(steamIDFriend.ConvertToUint64());
    if (finfos_it == _friends.end())
        return "";

    if (iKey >= finfos_it->second.rich_presence.rich_presence_size())
        return "";

    auto it = finfos_it->second.rich_presence.rich_presence().begin();
    std::advance(it, iKey);

    return it->second.c_str();
}
// Requests rich presence for a specific user.
void Steam_Friends::RequestFriendRichPresence(CSteamID steamIDFriend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    Friend_Rich_Presence_Request_pb* rich_request = new Friend_Rich_Presence_Request_pb;

    send_rich_request(steamIDFriend.ConvertToUint64(), rich_request);
}

// Rich invite support.
// If the target accepts the invite, a GameRichPresenceJoinRequested_t callback is posted containing the connect string.
// (Or you can configure yout game so that it is passed on the command line instead.  This is a deprecated path; ask us if you really need this.)
// Invites can only be sent to friends.
bool Steam_Friends::InviteUserToGame(CSteamID steamIDFriend, const char* pchConnectString)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (pchConnectString == nullptr || *pchConnectString == 0)
        return false;

    Friend_Rich_Invite_pb* rich_invite = new Friend_Rich_Invite_pb;
    rich_invite->set_connect_str(pchConnectString);

    return send_rich_invite(steamIDFriend.ConvertToUint64(), rich_invite);
}

// recently-played-with friends iteration
// this iterates the entire list of users recently played with, across games
// GetFriendCoplayTime() returns as a unix time
int Steam_Friends::GetCoplayFriendCount()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

CSteamID Steam_Friends::GetCoplayFriend(int iCoplayFriend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return CSteamID();
}

int Steam_Friends::GetFriendCoplayTime(CSteamID steamIDFriend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

AppId_t Steam_Friends::GetFriendCoplayGame(CSteamID steamIDFriend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// chat interface for games
// this allows in-game access to group (clan) chats from in the game
// the behavior is somewhat sophisticated, because the user may or may not be already in the group chat from outside the game or in the overlay
// use ActivateGameOverlayToUser( "chat", steamIDClan ) to open the in-game overlay version of the chat
STEAM_CALL_RESULT(JoinClanChatRoomCompletionResult_t)
SteamAPICall_t Steam_Friends::JoinClanChatRoom(CSteamID steamIDClan)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

bool Steam_Friends::LeaveClanChatRoom(CSteamID steamIDClan)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

int Steam_Friends::GetClanChatMemberCount(CSteamID steamIDClan)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

CSteamID Steam_Friends::GetChatMemberByIndex(CSteamID steamIDClan, int iUser)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return CSteamID();
}

bool Steam_Friends::SendClanChatMessage(CSteamID steamIDClanChat, const char* pchText)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

int Steam_Friends::GetClanChatMessage(CSteamID steamIDClanChat, int iMessage, void* prgchText, int cchTextMax, EChatEntryType* peChatEntryType, STEAM_OUT_STRUCT() CSteamID* psteamidChatter)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}
bool Steam_Friends::IsClanChatAdmin(CSteamID steamIDClanChat, CSteamID steamIDUser)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// interact with the Steam (game overlay / desktop)
bool Steam_Friends::IsClanChatWindowOpenInSteam(CSteamID steamIDClanChat)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_Friends::OpenClanChatWindowInSteam(CSteamID steamIDClanChat)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_Friends::CloseClanChatWindowInSteam(CSteamID steamIDClanChat)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// peer-to-peer chat interception
// this is so you can show P2P chats inline in the game
bool Steam_Friends::SetListenForFriendsMessages(bool bInterceptEnabled)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_Friends::ReplyToFriendMessage(CSteamID steamIDFriend, const char* pchMsgToSend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

int Steam_Friends::GetFriendMessage(CSteamID steamIDFriend, int iMessageID, void* pvData, int cubData, EChatEntryType* peChatEntryType)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// following apis
STEAM_CALL_RESULT(FriendsGetFollowerCount_t)
SteamAPICall_t Steam_Friends::GetFollowerCount(CSteamID steamID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

STEAM_CALL_RESULT(FriendsIsFollowing_t)
SteamAPICall_t Steam_Friends::IsFollowing(CSteamID steamID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

STEAM_CALL_RESULT(FriendsEnumerateFollowingList_t)
SteamAPICall_t Steam_Friends::EnumerateFollowingList(uint32 unStartIndex)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

bool Steam_Friends::IsClanPublic(CSteamID steamIDClan)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_Friends::IsClanOfficialGameGroup(CSteamID steamIDClan)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

/// Return the number of chats (friends or chat rooms) with unread messages.
/// A "priority" message is one that would generate some sort of toast or
/// notification, and depends on user settings.
///
/// You can register for UnreadChatMessagesChanged_t callbacks to know when this
/// has potentially changed.
int Steam_Friends::GetNumChatsWithUnreadPriorityMessages()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// activates game overlay to open the remote play together invite dialog. Invitations will be sent for remote play together
void Steam_Friends::ActivateGameOverlayRemotePlayTogetherInviteDialog(CSteamID steamIDLobby)
{

}

// Call this before calling ActivateGameOverlayToWebPage() to have the Steam Overlay Browser block navigations
// to your specified protocol (scheme) uris and instead dispatch a OverlayBrowserProtocolNavigation_t callback to your game.
// ActivateGameOverlayToWebPage() must have been called with k_EActivateGameOverlayToWebPageMode_Modal
bool Steam_Friends::RegisterProtocolInOverlayBrowser(const char* pchProtocol)
{
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//                           Network Send messages                           //
///////////////////////////////////////////////////////////////////////////////
bool Steam_Friends::send_friend_infos_request(uint64 remote_id, Friend_Infos_Request_pb* ffinfos_req)
{
    TRACE_FUNC();

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();
    // Request the user infos
    Network_Message_pb msg;
    Steam_Friends_pb* req_frd_pb = new Steam_Friends_pb;

    req_frd_pb->set_allocated_friend_infos_request(ffinfos_req);

    msg.set_allocated_friends(req_frd_pb);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

bool Steam_Friends::send_friend_infos(uint64 remote_id, Friend_Infos_pb* ffinfos)
{
    TRACE_FUNC();

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Friends_pb* resp_frd_pb = new Steam_Friends_pb;

    resp_frd_pb->set_allocated_friend_infos(ffinfos);

    msg.set_allocated_friends(resp_frd_pb);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    auto res = _network->ReliableSendTo(msg);
    resp_frd_pb->release_friend_infos();

    return res;
}

bool Steam_Friends::send_rich_request(uint64 remote_id, Friend_Rich_Presence_Request_pb* rich_request)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Friends_pb* req_frd_pb = new Steam_Friends_pb;

    req_frd_pb->set_allocated_rich_request(rich_request);

    msg.set_allocated_friends(req_frd_pb);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

bool Steam_Friends::send_rich_presence(uint64 remote_id, Friend_Rich_Presence_pb* rp_resp)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Friends_pb* req_frd_pb = new Steam_Friends_pb;
    
    req_frd_pb->set_allocated_rich_presence(rp_resp);
    msg.set_allocated_friends(req_frd_pb);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    auto res = _network->ReliableSendTo(msg);
    req_frd_pb->release_rich_presence();

    return res;
}

bool Steam_Friends::send_rich_invite(uint64 remote_id, Friend_Rich_Invite_pb* rich_invite)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Friends_pb* req_frd_pb = new Steam_Friends_pb;

    req_frd_pb->set_allocated_rich_invite(rich_invite);

    msg.set_allocated_friends(req_frd_pb);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

bool Steam_Friends::send_avatar_request(uint64 remote_id, Friend_Avatar_Request_pb* av_req)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Friends_pb* frd = new Steam_Friends_pb;

    frd->set_allocated_avatar_request(av_req);
    msg.set_allocated_friends(frd);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

bool Steam_Friends::send_avatar_response(uint64 remote_id, Friend_Avatar_Response_pb* av_resp)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Friends_pb* frd = new Steam_Friends_pb;

    frd->set_allocated_avatar_response(av_resp);
    msg.set_allocated_friends(frd);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

///////////////////////////////////////////////////////////////////////////////
//                          Network Receive messages                         //
///////////////////////////////////////////////////////////////////////////////
bool Steam_Friends::on_peer_connect(Network_Message_pb const& msg, Network_Peer_Connect_pb const& conn)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "Friend connected: %llu", msg.source_id());
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto frd_it = get_friend(msg.source_id());

    if (frd_it == _friends.end() || !frd_it->second.connected)
    {// New friend
        new_friend(msg.source_id());
        frd_it = get_friend(msg.source_id());
    }
    frd_it->second.connected = true;

    return true;
}

bool Steam_Friends::on_peer_disconnect(Network_Message_pb const& msg, Network_Peer_Disconnect_pb const& disc)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "Friend disconnected: %llu", msg.source_id());
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto frd_it = get_friend(msg.source_id());

    if (frd_it->second.infos.state() != k_EPersonaStateOffline)
    {// New friend
        disconnect_friend(frd_it);
    }
    frd_it->second.connected = false;

    return true;
}

bool Steam_Friends::on_friend_infos_request(Network_Message_pb const& msg, Friend_Infos_Request_pb const& frd_req)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    return send_friend_infos(msg.source_id(), &myself()->second.infos);
}

bool Steam_Friends::on_friend_infos_received(Network_Message_pb const& msg, Friend_Infos_pb const& frd_infos)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    // If its not an individual (gameserver, ...)
    if (!CSteamID(uint64(msg.source_id())).BIndividualAccount())
    {
        APP_LOG(Log::LogLevel::INFO, "Received Gameserver infos from: %llu", msg.source_id());
        return true; // Don't add him to the friend list
    }

    APP_LOG(Log::LogLevel::INFO, "Received friend infos from: %llu", msg.source_id());

    auto finfos_it = get_friend(msg.source_id());

    // New friend
    if (finfos_it == _friends.end())
    {// We didn't receive the on_peer_connect ?
        new_friend(msg.source_id());
        finfos_it = get_friend(msg.source_id());
    }

    uint32_t old_state = finfos_it->second.infos.state();
    finfos_it->second.app_id = msg.app_id();
    finfos_it->second.infos = frd_infos;

    if (old_state != finfos_it->second.infos.state())
    {
        switch (finfos_it->second.infos.state())
        {
            case EPersonaState::k_EPersonaStateOnline:
            {
                auto& overlay = GetSteam_Overlay();
                overlay.FriendConnect(static_cast<uint64>(msg.source_id()), finfos_it->second.infos.name(), finfos_it->second.app_id);

                trigger_persona_state_change(msg.source_id(), k_EPersonaChangeComeOnline);
            }
            break;

            case EPersonaState::k_EPersonaStateOffline:
            {
                disconnect_friend(finfos_it);
            }
            break;
        }
    }

    finfos_it->second.last_friend_infos = std::chrono::steady_clock::now();

    return true;
}

bool Steam_Friends::on_friend_rich_request(Network_Message_pb const& msg, Friend_Rich_Presence_Request_pb const& rp_req)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    return send_rich_presence(msg.source_id(), &myself()->second.rich_presence);
}

bool Steam_Friends::on_friend_rich_presence(Network_Message_pb const& msg, Friend_Rich_Presence_pb const& rp_resp)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    auto finfos_it = get_friend(msg.source_id());
    if (finfos_it != _friends.end())
    {
        *finfos_it->second.rich_presence.mutable_rich_presence() = rp_resp.rich_presence();

        pFrameResult_t result(new FrameResult);
        FriendRichPresenceUpdate_t& frpu = result->CreateCallback<FriendRichPresenceUpdate_t>();

        frpu.m_nAppID = Settings::Inst().gameid.AppID();
        frpu.m_steamIDFriend = static_cast<uint64>(msg.source_id());

        result->done = true;
        _cb_manager->add_callback(this, result);
    }

    return true;
}

bool Steam_Friends::on_friend_rich_invite(Network_Message_pb const& msg, Friend_Rich_Invite_pb const& rich_invite)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto& overlay = GetSteam_Overlay();
    overlay.SetRichInvite(static_cast<uint64>(msg.source_id()), rich_invite.connect_str().c_str());

    //pFrameResult_t result(new FrameResult);
    //GameRichPresenceJoinRequested_t& grpjr = result->CreateCallback<GameRichPresenceJoinRequested_t>();
    //
    //grpjr.m_steamIDFriend = static_cast<uint64>(msg.source_id());
    //strncpy(grpjr.m_rgchConnect, rich_invite.connect_str().c_str(), k_cchMaxRichPresenceValueLength - 1);
    //
    //result->done = true;
    //GetCBManager()->add_callback(this, result);

    return true;
}

bool Steam_Friends::on_friend_avatar_request(Network_Message_pb const& msg, Friend_Avatar_Request_pb const& avatar_request)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    Friend_Avatar_Response_pb* resp = new Friend_Avatar_Response_pb();
    resp->set_ready(false);

    auto const& finfos = myself()->second;

    if (finfos.small_image_handle &&
        finfos.medium_image_handle && 
        finfos.large_image_handle)
    {
        uint32 w, h;
        std::string* img;
        int img_handle;
        auto& utils = GetSteam_Utils();

        img_handle = finfos.small_image_handle;
        img = resp->mutable_small_avatar();
        utils.GetImageSize(img_handle, &w, &h);
        img->assign(w * h * 4, '\0');
        utils.GetImageRGBA(img_handle, reinterpret_cast<uint8*>(const_cast<char*>(img->data())), w * h * 4);

        img_handle = finfos.medium_image_handle;
        img = resp->mutable_medium_avatar();
        utils.GetImageSize(img_handle, &w, &h);
        img->assign(w * h * 4, '\0');
        utils.GetImageRGBA(img_handle, reinterpret_cast<uint8*>(const_cast<char*>(img->data())), w * h * 4);

        img_handle = finfos.large_image_handle;
        img = resp->mutable_large_avatar();
        utils.GetImageSize(img_handle, &w, &h);
        img->assign(w * h * 4, '\0');
        utils.GetImageRGBA(img_handle, reinterpret_cast<uint8*>(const_cast<char*>(img->data())), w * h * 4);

        resp->set_ready(true);
    }

    return send_avatar_response(msg.source_id(), resp);
}

bool Steam_Friends::on_friend_avatar_response(Network_Message_pb const& msg, Friend_Avatar_Response_pb const& avatar_response)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    if (avatar_response.ready())
    {
        auto finfos_it = get_friend(msg.source_id());
        if (finfos_it != _friends.end())
        {
            auto& utils = GetSteam_Utils();
            auto& overlay = GetSteam_Overlay();

            if (finfos_it->second.small_image_handle == 0)
            {
                finfos_it->second.small_image_handle = utils.generate_image_handle(32, 32);
                auto image = utils.get_image(finfos_it->second.small_image_handle);
                memcpy(image->get_raw_pointer(), avatar_response.small_avatar().data(), std::min<size_t>(image->raw_size(), avatar_response.small_avatar().size()));
            }
            if (finfos_it->second.medium_image_handle == 0)
            {
                finfos_it->second.medium_image_handle = utils.generate_image_handle(64, 64);
                auto image = utils.get_image(finfos_it->second.medium_image_handle);
                memcpy(image->get_raw_pointer(), avatar_response.medium_avatar().data(), std::min<size_t>(image->raw_size(), avatar_response.medium_avatar().size()));
            }
            if (finfos_it->second.large_image_handle == 0)
            {
                finfos_it->second.large_image_handle = utils.generate_image_handle(184, 184);
                auto image = utils.get_image(finfos_it->second.large_image_handle);
                memcpy(image->get_raw_pointer(), avatar_response.large_avatar().data(), std::min<size_t>(image->raw_size(), avatar_response.large_avatar().size()));

                overlay.FriendImageLoaded(static_cast<uint64>(msg.source_id()), image);
            }

            pFrameResult_t res(new FrameResult);
            AvatarImageLoaded_t& ail = res->CreateCallback<AvatarImageLoaded_t>(std::chrono::milliseconds(0));

            ail.m_iImage = finfos_it->second.large_image_handle;
            ail.m_iTall = 184;
            ail.m_iWide = 184;
            ail.m_steamID = uint64(msg.source_id());

            _cb_manager->add_callback(this, res);
            res->done = true;

            finfos_it->second.has_avatar = true;
        }
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//                                IRunCallback                               //
///////////////////////////////////////////////////////////////////////////////
// Maybe delegate this to a thread
bool Steam_Friends::CBRunFrame()
{
    //LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    auto now_time = std::chrono::steady_clock::now();

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    for (auto it = ++_friends.begin(); it != _friends.end(); ++it)
    {
        if (!it->second.connected)
            continue;

        if ((now_time - it->second.last_friend_infos) > friend_infos_rate)
        {
            //PRINT_DEBUG("Request full infos");
            // Request the user infos
            {
                Friend_Infos_Request_pb* request = new Friend_Infos_Request_pb;
                send_friend_infos_request(it->first, request);
            }
            {
                Friend_Rich_Presence_Request_pb* request = new Friend_Rich_Presence_Request_pb;
                send_rich_request(it->first, request);
            }

            it->second.last_friend_infos = now_time;

            if (!it->second.has_avatar)
            {
                Friend_Avatar_Request_pb* req = new Friend_Avatar_Request_pb;
                send_avatar_request(it->first, req);
            }
        }
    }

    return false;
}

bool Steam_Friends::RunNetwork(Network_Message_pb const& msg)
{
    if (msg.source_id() == Settings::Inst().userid.ConvertToUint64())
        return false;

    switch (msg.messages_case())
    {
        case Network_Message_pb::MessagesCase::kNetworkAdvertise:
        {
            Network_Advertise_pb const& adv_pb = msg.network_advertise();
            switch (adv_pb.message_case())
            {
                case Network_Advertise_pb::MessageCase::kPeerConnect   : return on_peer_connect(msg, adv_pb.peer_connect());
                case Network_Advertise_pb::MessageCase::kPeerDisconnect: return on_peer_disconnect(msg, adv_pb.peer_disconnect());
                default: break;
            }
        }
        break;

        case Network_Message_pb::MessagesCase::kFriends:
        {
            Steam_Friends_pb const& frd_pb = msg.friends();
            switch (frd_pb.messages_case())
            {
                case Steam_Friends_pb::MessagesCase::kFriendInfosRequest: return on_friend_infos_request  (msg, frd_pb.friend_infos_request());
                case Steam_Friends_pb::MessagesCase::kFriendInfos       : return on_friend_infos_received (msg, frd_pb.friend_infos());
                case Steam_Friends_pb::MessagesCase::kRichRequest       : return on_friend_rich_request   (msg, frd_pb.rich_request());
                case Steam_Friends_pb::MessagesCase::kRichPresence      : return on_friend_rich_presence  (msg, frd_pb.rich_presence());
                case Steam_Friends_pb::MessagesCase::kRichInvite        : return on_friend_rich_invite    (msg, frd_pb.rich_invite());
                case Steam_Friends_pb::MessagesCase::kAvatarRequest     : return on_friend_avatar_request (msg, frd_pb.avatar_request());
                case Steam_Friends_pb::MessagesCase::kAvatarResponse    : return on_friend_avatar_response(msg, frd_pb.avatar_response());
                default: break;
            }
        }
        break;

        default: break;
    }

    return false;
}

bool Steam_Friends::RunCallbacks(pFrameResult_t res)
{
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    switch (res->ICallback())
    {
        case PersonaStateChange_t::k_iCallback:
        {
            PersonaStateChange_t& psc = res->GetCallback<PersonaStateChange_t>();
            res->done = true;
        }
        break;
    }
    return res->done;
}
