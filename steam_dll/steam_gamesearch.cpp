/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_gamesearch.h"

Steam_GameSearch::Steam_GameSearch()
{
}

Steam_GameSearch::~Steam_GameSearch()
{
    emu_deinit();
}

void Steam_GameSearch::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _cb_manager = cb_manager;
    _network = network;
}

void Steam_GameSearch::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _network.reset();
    _cb_manager.reset();
}

// =============================================================================================
// Game Player APIs

// a keyname and a list of comma separated values: one of which is must be found in order for the match to qualify
// fails if a search is currently in progress
EGameSearchErrorCode_t Steam_GameSearch::AddGameSearchParams(const char* pchKeyToFind, const char* pchValuesToFind)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EGameSearchErrorCode_Failed_Unknown_Error;
}

// all players in lobby enter the queue and await a SearchForGameNotificationCallback_t callback. fails if another search is currently in progress
// if not the owner of the lobby or search already in progress this call fails
// periodic callbacks will be sent as queue time estimates change
EGameSearchErrorCode_t Steam_GameSearch::SearchForGameWithLobby(CSteamID steamIDLobby, int nPlayerMin, int nPlayerMax)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EGameSearchErrorCode_Failed_Unknown_Error;
}

// user enter the queue and await a SearchForGameNotificationCallback_t callback. fails if another search is currently in progress
// periodic callbacks will be sent as queue time estimates change
EGameSearchErrorCode_t Steam_GameSearch::SearchForGameSolo(int nPlayerMin, int nPlayerMax)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EGameSearchErrorCode_Failed_Unknown_Error;
}

// after receiving SearchForGameResultCallback_t, accept or decline the game
// multiple SearchForGameResultCallback_t will follow as players accept game until the host starts or cancels the game
EGameSearchErrorCode_t Steam_GameSearch::AcceptGame()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EGameSearchErrorCode_Failed_Unknown_Error;
}
EGameSearchErrorCode_t Steam_GameSearch::DeclineGame()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EGameSearchErrorCode_Failed_Unknown_Error;
}

// after receiving GameStartedByHostCallback_t get connection details to server
EGameSearchErrorCode_t Steam_GameSearch::RetrieveConnectionDetails(CSteamID steamIDHost, char* pchConnectionDetails, int cubConnectionDetails)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EGameSearchErrorCode_Failed_Unknown_Error;
}

// leaves queue if still waiting
EGameSearchErrorCode_t Steam_GameSearch::EndGameSearch()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EGameSearchErrorCode_Failed_Unknown_Error;
}

// =============================================================================================
// Game Host APIs

// a keyname and a list of comma separated values: all the values you allow
EGameSearchErrorCode_t Steam_GameSearch::SetGameHostParams(const char* pchKey, const char* pchValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EGameSearchErrorCode_Failed_Unknown_Error;
}

// set connection details for players once game is found so they can connect to this server
EGameSearchErrorCode_t Steam_GameSearch::SetConnectionDetails(const char* pchConnectionDetails, int cubConnectionDetails)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EGameSearchErrorCode_Failed_Unknown_Error;
}

// mark server as available for more players with nPlayerMin,nPlayerMax desired
// accept no lobbies with playercount greater than nMaxTeamSize
// the set of lobbies returned must be partitionable into teams of no more than nMaxTeamSize
// RequestPlayersForGameNotificationCallback_t callback will be sent when the search has started
// multple RequestPlayersForGameResultCallback_t callbacks will follow when players are found
EGameSearchErrorCode_t Steam_GameSearch::RequestPlayersForGame(int nPlayerMin, int nPlayerMax, int nMaxTeamSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EGameSearchErrorCode_Failed_Unknown_Error;
}

// accept the player list and release connection details to players
// players will only be given connection details and host steamid when this is called
// ( allows host to accept after all players confirm, some confirm, or none confirm. decision is entirely up to the host )
EGameSearchErrorCode_t Steam_GameSearch::HostConfirmGameStart(uint64 ullUniqueGameID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EGameSearchErrorCode_Failed_Unknown_Error;
}

// cancel request and leave the pool of game hosts looking for players
// if a set of players has already been sent to host, all players will receive SearchForGameHostFailedToConfirm_t
EGameSearchErrorCode_t Steam_GameSearch::CancelRequestPlayersForGame()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EGameSearchErrorCode_Failed_Unknown_Error;
}

// submit a result for one player. does not end the game. ullUniqueGameID continues to describe this game
EGameSearchErrorCode_t Steam_GameSearch::SubmitPlayerResult(uint64 ullUniqueGameID, CSteamID steamIDPlayer, EPlayerResult_t EPlayerResult)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EGameSearchErrorCode_Failed_Unknown_Error;
}

// ends the game. no further SubmitPlayerResults for ullUniqueGameID will be accepted
// any future requests will provide a new ullUniqueGameID
EGameSearchErrorCode_t Steam_GameSearch::EndGame(uint64 ullUniqueGameID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EGameSearchErrorCode_Failed_Unknown_Error;
}