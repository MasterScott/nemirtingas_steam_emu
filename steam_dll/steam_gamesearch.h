/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"

class LOCAL_API Steam_GameSearch:
    public ISteamGameSearch
{
    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;

public:
    std::recursive_mutex _local_mutex;

    Steam_GameSearch();
    virtual ~Steam_GameSearch();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    // =============================================================================================
    // Game Player APIs

    // a keyname and a list of comma separated values: one of which is must be found in order for the match to qualify
    // fails if a search is currently in progress
    virtual EGameSearchErrorCode_t AddGameSearchParams(const char* pchKeyToFind, const char* pchValuesToFind);

    // all players in lobby enter the queue and await a SearchForGameNotificationCallback_t callback. fails if another search is currently in progress
    // if not the owner of the lobby or search already in progress this call fails
    // periodic callbacks will be sent as queue time estimates change
    virtual EGameSearchErrorCode_t SearchForGameWithLobby(CSteamID steamIDLobby, int nPlayerMin, int nPlayerMax);

    // user enter the queue and await a SearchForGameNotificationCallback_t callback. fails if another search is currently in progress
    // periodic callbacks will be sent as queue time estimates change
    virtual EGameSearchErrorCode_t SearchForGameSolo(int nPlayerMin, int nPlayerMax);

    // after receiving SearchForGameResultCallback_t, accept or decline the game
    // multiple SearchForGameResultCallback_t will follow as players accept game until the host starts or cancels the game
    virtual EGameSearchErrorCode_t AcceptGame();
    virtual EGameSearchErrorCode_t DeclineGame();

    // after receiving GameStartedByHostCallback_t get connection details to server
    virtual EGameSearchErrorCode_t RetrieveConnectionDetails(CSteamID steamIDHost, char* pchConnectionDetails, int cubConnectionDetails);

    // leaves queue if still waiting
    virtual EGameSearchErrorCode_t EndGameSearch();

    // =============================================================================================
    // Game Host APIs

    // a keyname and a list of comma separated values: all the values you allow
    virtual EGameSearchErrorCode_t SetGameHostParams(const char* pchKey, const char* pchValue);

    // set connection details for players once game is found so they can connect to this server
    virtual EGameSearchErrorCode_t SetConnectionDetails(const char* pchConnectionDetails, int cubConnectionDetails);

    // mark server as available for more players with nPlayerMin,nPlayerMax desired
    // accept no lobbies with playercount greater than nMaxTeamSize
    // the set of lobbies returned must be partitionable into teams of no more than nMaxTeamSize
    // RequestPlayersForGameNotificationCallback_t callback will be sent when the search has started
    // multple RequestPlayersForGameResultCallback_t callbacks will follow when players are found
    virtual EGameSearchErrorCode_t RequestPlayersForGame(int nPlayerMin, int nPlayerMax, int nMaxTeamSize);

    // accept the player list and release connection details to players
    // players will only be given connection details and host steamid when this is called
    // ( allows host to accept after all players confirm, some confirm, or none confirm. decision is entirely up to the host )
    virtual EGameSearchErrorCode_t HostConfirmGameStart(uint64 ullUniqueGameID);

    // cancel request and leave the pool of game hosts looking for players
    // if a set of players has already been sent to host, all players will receive SearchForGameHostFailedToConfirm_t
    virtual EGameSearchErrorCode_t CancelRequestPlayersForGame();

    // submit a result for one player. does not end the game. ullUniqueGameID continues to describe this game
    virtual EGameSearchErrorCode_t SubmitPlayerResult(uint64 ullUniqueGameID, CSteamID steamIDPlayer, EPlayerResult_t EPlayerResult);

    // ends the game. no further SubmitPlayerResults for ullUniqueGameID will be accepted
    // any future requests will provide a new ullUniqueGameID
    virtual EGameSearchErrorCode_t EndGame(uint64 ullUniqueGameID);
};