/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_gameserver.h"
#include "steam_client.h"
#include "settings.h"

Steam_Gameserver::Steam_Gameserver() :
    _auth_manager(nullptr),
    _policy_response_called(false)
{
}

Steam_Gameserver::~Steam_Gameserver()
{
    APP_LOG(Log::LogLevel::DEBUG, "");
    
    emu_deinit();
}

void Steam_Gameserver::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    std::lock(_local_mutex, cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(cb_manager->_local_mutex, std::adopt_lock);

    _cb_manager = cb_manager;
    _network = network;

    _auth_manager = new Auth_Ticket_Manager();
    _auth_manager->emu_init(cb_manager, network);

    _cb_manager->register_callbacks(this);
    _cb_manager->register_frame(this);
    _network->register_listener(this, Network_Message_pb::MessagesCase::kGameserver);
}

void Steam_Gameserver::emu_deinit()
{
    if (_cb_manager != nullptr)
    {
        std::lock(_local_mutex, _cb_manager->_local_mutex);
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
        std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

        _cb_manager->unregister_callbacks(this);
        _cb_manager->unregister_frame(this);
    }

    if (_network != nullptr)
    {
        _network->unregister_listener(this, Network_Message_pb::MessagesCase::kGameserver);
    }

    {
        std::lock_guard<std::recursive_mutex> lk(_local_mutex);
        delete _auth_manager; _auth_manager = nullptr;
        _network.reset();
        _cb_manager.reset();
    }
}

//
// Basic server data.  These properties, if set, must be set before before calling LogOn.  They
// may not be changed after logged in.
//

/// This is called by SteamGameServer_Init, and you will usually not need to call it directly
bool Steam_Gameserver::InitGameServer(uint32 unIP, uint16 usGamePort, uint16 usQueryPort, uint32 unFlags, AppId_t nGameAppId, const char* pchVersionString)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "unIP %d, usGamePort %hd, usQueryPort %hd, unFlags %d, nGameAppId %d, pchVersionString %s", unIP, usGamePort, usQueryPort, unFlags, nGameAppId, pchVersionString);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    std::string str_version(pchVersionString);
    
    _server_infos.version = str_version;
    for (auto it = str_version.begin(); it != str_version.end();)
    {
        if (*it < '0' || *it > '9')
            it = str_version.erase(it);
        else
            ++it;
    }

    uint32_t int_version = std::stoi(str_version);

    _network->stop_network();

    _server_infos.pb_infos.set_ip(unIP);
    _server_infos.pb_infos.set_port(usGamePort);
    _server_infos.pb_infos.set_query_port(usQueryPort);
    _server_infos.flags = unFlags;
    _server_infos.pb_infos.set_appid(nGameAppId);
    _server_infos.pb_infos.set_version(int_version);
    _server_infos.owner_id = Settings::Inst().userid.ConvertToUint64();
    _server_infos.pb_infos.set_gameserver_id(generate_steam_id_server().ConvertToUint64());
    _server_infos.gamename = Settings::Inst().gamename;

    _policy_response_called = false;

    return true;
}

/// Game product identifier.  This is currently used by the master server for version checking purposes.
/// It's a required field, but will eventually will go away, and the AppID will be used for this purpose.
void Steam_Gameserver::SetProduct(const char* pszProduct)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "%s", pszProduct);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _server_infos.product = pszProduct;
}

/// Description of the game.  This is a required field and is displayed in the steam server browser....for now.
/// This is a required field, but it will go away eventually, as the data should be determined from the AppID.
void Steam_Gameserver::SetGameDescription(const char* pszGameDescription)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "%s", pszGameDescription);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _server_infos.pb_infos.set_description(pszGameDescription);
}

/// If your game is a "mod," pass the string that identifies it.  The default is an empty string, meaning
/// this application is the original game, not a mod.
///
/// @see k_cbMaxGameServerGameDir
void Steam_Gameserver::SetModDir(const char* pszModDir)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "%s", pszModDir);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _server_infos.mod_dir = pszModDir;
}

/// Is this is a dedicated server?  The default value is false.
void Steam_Gameserver::SetDedicatedServer(bool bDedicated)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "%d", (int)bDedicated);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _server_infos.dedicated = bDedicated;
}

//
// Login
//

    /// Begin process to login to a persistent game server account
    ///
    /// You need to register for callbacks to determine the result of this operation.
    /// @see SteamServersConnected_t
    /// @see SteamServerConnectFailure_t
    /// @see SteamServersDisconnected_t
void Steam_Gameserver::LogOn()
{
    LogOnAnonymous();
}

void Steam_Gameserver::LogOn(const char* pszAccountName, const char* pszPassword)
{
    LogOn();
}

void Steam_Gameserver::LogOn(const char* pszToken)
{
    LogOn();
}

/// Login to a generic, anonymous account.
///
/// Note: in previous versions of the SDK, this was automatically called within SteamGameServer_Init,
/// but this is no longer the case.
void Steam_Gameserver::LogOnAnonymous()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    uint64 gs_id = (uint64_t)_server_infos.pb_infos.gameserver_id();

    pFrameResult_t result(new FrameResult);
    SteamServersConnected_t& ssc = result->CreateCallback<SteamServersConnected_t>();

    result->done = true;
    _cb_manager->add_callback(this, result);

    _network->set_peer_id(gs_id);
    _network->start_network();

    if (_server_infos.pb_infos.query_port() != 0)
    {
        PortableAPI::ipv4_addr addr;
        addr.set_ip(_server_infos.pb_infos.ip());
        addr.set_port(_server_infos.pb_infos.query_port());
        _network->StartQueryServer(addr);
    }

    _server_infos.logged = true;
}

/// Begin process of logging game server out of steam
void Steam_Gameserver::LogOff()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    _server_infos.logged = false;

    pFrameResult_t result(new FrameResult);
    SteamServersDisconnected_t& ssd = result->CreateCallback<SteamServersDisconnected_t>();
    ssd.m_eResult = k_EResultOK;

    result->done = true;
    _cb_manager->add_callback(this, result);

    _network->stop_network();
    _network->StopQueryServer();
}

// status functions
bool Steam_Gameserver::BLoggedOn()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    return _server_infos.logged;
}

bool Steam_Gameserver::BSecure()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    return _server_infos.pb_infos.secured();
}

CSteamID Steam_Gameserver::GetSteamID()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    return CSteamID(uint64(_server_infos.pb_infos.gameserver_id()));
}

/// Returns true if the master server has requested a restart.
/// Only returns true once per request.
bool Steam_Gameserver::WasRestartRequested()
{
    APP_LOG(Log::LogLevel::TRACE, "");

    return false;
}

//
// Server state.  These properties may be changed at any time.
//

/// Max player count that will be reported to server browser and client queries
void Steam_Gameserver::SetMaxPlayerCount(int cPlayersMax)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "%d", cPlayersMax);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _server_infos.pb_infos.set_max_players(cPlayersMax);
}

/// Number of bots.  Default value is zero
void Steam_Gameserver::SetBotPlayerCount(int cBotplayers)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "%d", cBotplayers);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _server_infos.pb_infos.set_bots(cBotplayers);
}

/// Set the name of server as it will appear in the server browser
///
/// @see k_cbMaxGameServerName
void Steam_Gameserver::SetServerName(const char* pszServerName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "%s", pszServerName);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (pszServerName == nullptr)
        pszServerName = "";

    _server_infos.pb_infos.set_serv_name(pszServerName);
}

/// Set name of map to report in the server browser
///
/// @see k_cbMaxGameServerName
void Steam_Gameserver::SetMapName(const char* pszMapName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "%s", pszMapName);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (pszMapName == nullptr)
        pszMapName = "";

    _server_infos.pb_infos.set_map(pszMapName);
}

/// Let people know if your server will require a password
void Steam_Gameserver::SetPasswordProtected(bool bPasswordProtected)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "%d", (int)bPasswordProtected);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _server_infos.pb_infos.set_password(bPasswordProtected);
}

/// Spectator server.  The default value is zero, meaning the service
/// is not used.
void Steam_Gameserver::SetSpectatorPort(uint16 unSpectatorPort)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "%hd", unSpectatorPort);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _server_infos.spectator_port = unSpectatorPort;
}

/// Name of the spectator server.  (Only used if spectator port is nonzero.)
///
/// @see k_cbMaxGameServerMapName
void Steam_Gameserver::SetSpectatorServerName(const char* pszSpectatorServerName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "%s", pszSpectatorServerName);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (pszSpectatorServerName == nullptr)
        pszSpectatorServerName = "";

    _server_infos.spectator_server = pszSpectatorServerName;
}

/// Call this to clear the whole list of key/values that are sent in rules queries.
void Steam_Gameserver::ClearAllKeyValues()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _server_infos.rules.clear();
}

/// Call this to add/update a key/value pair.
void Steam_Gameserver::SetKeyValue(const char* pKey, const char* pValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "%s:%s", pKey, pValue);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (pKey == nullptr)
        return;

    auto it = _server_infos.rules.find(pKey);
    
    if (pValue == nullptr)
        pValue = "";

    if (*pValue == 0)
    {
        if (it != _server_infos.rules.end())
        {
            _server_infos.rules.erase(it);
        }
    }
    else
    {
        _server_infos.rules[pKey] = pValue;
    }
}

/// Sets a string defining the "gametags" for this server, this is optional, but if it is set
/// it allows users to filter in the matchmaking/server-browser interfaces based on the value
///
/// @see k_cbMaxGameServerTags
void Steam_Gameserver::SetGameTags(const char* pchGameTags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "%s", pchGameTags);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (pchGameTags == nullptr)
        pchGameTags = "";

    _server_infos.pb_infos.set_gametags(pchGameTags);
}

/// Sets a string defining the "gamedata" for this server, this is optional, but if it is set
/// it allows users to filter in the matchmaking/server-browser interfaces based on the value
/// don't set this unless it actually changes, its only uploaded to the master once (when
/// acknowledged)
///
/// @see k_cbMaxGameServerGameData
void Steam_Gameserver::SetGameData(const char* pchGameData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "%s", pchGameData);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (pchGameData == nullptr)
        pchGameData = "";

    _server_infos.pb_infos.set_gamedata(pchGameData);
}

/// Region identifier.  This is an optional field, the default value is empty, meaning the "world" region
void Steam_Gameserver::SetRegion(const char* pszRegion)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "%s", pszRegion);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (pszRegion == nullptr)
        pszRegion = "";

    _server_infos.region = pszRegion;
}

//
// Player list management / authentication
//

// Handles receiving a new connection from a Steam user.  This call will ask the Steam
// servers to validate the users identity, app ownership, and VAC status.  If the Steam servers 
// are off-line, then it will validate the cached ticket itself which will validate app ownership 
// and identity.  The AuthBlob here should be acquired on the game client using SteamUser()->InitiateGameConnection()
// and must then be sent up to the game server for authentication.
//
// Return Value: returns true if the users ticket passes basic checks. pSteamIDUser will contain the Steam ID of this user. pSteamIDUser must NOT be NULL
// If the call succeeds then you should expect a GSClientApprove_t or GSClientDeny_t callback which will tell you whether authentication
// for the user has succeeded or failed (the steamid in the callback will match the one returned by this call)
bool Steam_Gameserver::SendUserConnectAndAuthenticate(uint32 unIPClient, const void* pvAuthBlob, uint32 cubAuthBlobSize, CSteamID* pSteamIDUser)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
    
    CSteamID player_id;
    bool res = _auth_manager->SendUserConnectAndAuthenticate(unIPClient, pvAuthBlob, cubAuthBlobSize, &player_id);

    if (res)
    {
        Gameserver_player_infos_pb infos;
        if (pSteamIDUser != nullptr)
            *pSteamIDUser = player_id;

        infos.set_join_time(static_cast<float>(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now().time_since_epoch()).count()));
        infos.set_score(0);
        infos.set_name("Player");

        (*_server_infos.pb_infos.mutable_players_infos())[player_id.ConvertToUint64()] = infos;
    }

    return res;
}

// Creates a fake user (ie, a bot) which will be listed as playing on the server, but skips validation.  
// 
// Return Value: Returns a SteamID for the user to be tracked with, you should call HandleUserDisconnect()
// when this user leaves the server just like you would for a real user.
CSteamID Steam_Gameserver::CreateUnauthenticatedUserConnection()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    CSteamID bot_id = _auth_manager->fake_user();
    
    Gameserver_player_infos_pb infos;
    
    infos.set_join_time(static_cast<float>(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now().time_since_epoch()).count()));
    infos.set_score(0);
    infos.set_name("Bot");

    _server_infos.pb_infos.mutable_players_infos()->at(bot_id.ConvertToUint64()) = infos;

    return bot_id;
}

// Should be called whenever a user leaves our game server, this lets Steam internally
// track which users are currently on which servers for the purposes of preventing a single
// account being logged into multiple servers, showing who is currently on a server, etc.
void Steam_Gameserver::SendUserDisconnect(CSteamID steamIDUser)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "%llu", steamIDUser.ConvertToUint64());
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto player_it = _server_infos.pb_infos.mutable_players_infos()->find(steamIDUser.ConvertToUint64());

    if (player_it != _server_infos.pb_infos.mutable_players_infos()->end())
        _server_infos.pb_infos.mutable_players_infos()->erase(player_it);

    _auth_manager->end_auth(steamIDUser);
}

// Update the data to be displayed in the server browser and matchmaking interfaces for a user
// currently connected to the server.  For regular users you must call this after you receive a
// GSUserValidationSuccess callback.
// 
// Return Value: true if successful, false if failure (ie, steamIDUser wasn't for an active player)
bool Steam_Gameserver::BUpdateUserData(CSteamID steamIDUser, const char* pchPlayerName, uint32 uScore)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "steamIDUser %llu, pchPlayerName %s, uScore %d", steamIDUser.ConvertToUint64(), pchPlayerName, uScore);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto player_it = _server_infos.pb_infos.mutable_players_infos()->find(steamIDUser.ConvertToUint64());

    if (player_it == _server_infos.pb_infos.mutable_players_infos()->end())
        return false;

    if (pchPlayerName != nullptr)
        player_it->second.set_name(pchPlayerName);

    player_it->second.set_score(uScore);

    return true;
}

bool Steam_Gameserver::BSetServerType(uint32 unServerFlags, uint32 unGameIP, uint16 unGamePort,
    uint16 unSpectatorPort, uint16 usQueryPort, const char* pchGameDir, const char* pchVersion, bool bLANMode)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "unServerFlags %d, unGameIP %d, unGamePort %hd, unSpectatorPort %hd, usQueryPort %hd, pchGameDir %s, pchVersion %d, bLANMode %d", unServerFlags, unGameIP, unGamePort, unSpectatorPort, usQueryPort, pchGameDir, pchVersion, (int)bLANMode);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _server_infos.flags = unServerFlags;
    _server_infos.pb_infos.set_ip(unGameIP);
    _server_infos.pb_infos.set_port(unGamePort);
    _server_infos.spectator_port = unSpectatorPort;
    _server_infos.pb_infos.set_query_port(usQueryPort);
    _server_infos.mod_dir = pchGameDir;
    _server_infos.version = pchVersion;
    _server_infos.lan = bLANMode;

    _network->StopQueryServer();

    PortableAPI::ipv4_addr addr;
    addr.set_ip(unGameIP);
    addr.set_port(usQueryPort);

    _network->StartQueryServer(addr);

    return true;
}

// Updates server status values which shows up in the server browser and matchmaking APIs
void Steam_Gameserver::UpdateServerStatus(int cPlayers, int cPlayersMax, int cBotPlayers, const char* pchServerName, const char* pSpectatorServerName, const char* pchMapName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "cPlayers %d, cPlayersMax %d, cBotPlayers %d, pchServerName %s, pSpercatorServerName %s, pchMapName %s", cPlayers, cPlayersMax, cBotPlayers, pchServerName, pSpectatorServerName, pchMapName);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    //_server_infos.pb_infos.set_players(cPlayers);
    _server_infos.pb_infos.set_max_players(cPlayersMax);
    _server_infos.pb_infos.set_bots(cBotPlayers);
    _server_infos.pb_infos.set_serv_name(pchServerName);
    _server_infos.spectator_server = pSpectatorServerName;
    _server_infos.pb_infos.set_map(pchMapName);
}

// This can be called if spectator goes away or comes back (passing 0 means there is no spectator server now).
void Steam_Gameserver::UpdateSpectatorPort(uint16 unSpectatorPort)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _server_infos.spectator_port = unSpectatorPort;
}

// Sets a string defining the "gametype" for this server, this is optional, but if it is set
// it allows users to filter in the matchmaking/server-browser interfaces based on the value
void Steam_Gameserver::SetGameType(const char* pchGameType)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _server_infos.pb_infos.set_gametype(pchGameType);
}

// Ask if a user has a specific achievement for this game, will get a callback on reply
bool Steam_Gameserver::BGetUserAchievementStatus(CSteamID steamID, const char* pchAchievementName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    return false;
}

// New auth system APIs - do not mix with the old auth system APIs.
// ----------------------------------------------------------------

// Retrieve ticket to be sent to the entity who wishes to authenticate you ( using BeginAuthSession API ). 
// pcbTicket retrieves the length of the actual ticket.
HAuthTicket Steam_Gameserver::GetAuthSessionTicket(void* pTicket, int cbMaxTicket, uint32* pcbTicket)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    return _auth_manager->get_ticket(pTicket, cbMaxTicket, pcbTicket);
}

// Authenticate ticket ( from GetAuthSessionTicket ) from entity steamID to be sure it is valid and isnt reused
// Registers for callbacks if the entity goes offline or cancels the ticket ( see ValidateAuthTicketResponse_t callback and EAuthSessionResponse )
EBeginAuthSessionResult Steam_Gameserver::BeginAuthSession(const void* pAuthTicket, int cbAuthTicket, CSteamID steamID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    return _auth_manager->begin_auth(pAuthTicket, cbAuthTicket, steamID);
}

// Stop tracking started by BeginAuthSession - called when no longer playing game with this entity
void Steam_Gameserver::EndAuthSession(CSteamID steamID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _auth_manager->end_auth(steamID);
}

// Cancel auth ticket from GetAuthSessionTicket, called when no longer playing game with the entity you gave the ticket to
void Steam_Gameserver::CancelAuthTicket(HAuthTicket hAuthTicket)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _auth_manager->cancel_ticket(hAuthTicket);
}

// After receiving a user's authentication data, and passing it to SendUserConnectAndAuthenticate, use this function
// to determine if the user owns downloadable content specified by the provided AppID.
EUserHasLicenseForAppResult Steam_Gameserver::UserHasLicenseForApp(CSteamID steamID, AppId_t appID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    return k_EUserHasLicenseResultHasLicense;
}

// Ask if a user in in the specified group, results returns async by GSUserGroupStatus_t
// returns false if we're not connected to the steam servers and thus cannot ask
bool Steam_Gameserver::RequestUserGroupStatus(CSteamID steamIDUser, CSteamID steamIDGroup)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");

    // GSUserGroupStatus_t
    return false;
}


// these two functions s are deprecated, and will not return results
// they will be removed in a future version of the SDK
void Steam_Gameserver::GetGameplayStats()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

}

STEAM_CALL_RESULT(GSReputation_t)
SteamAPICall_t Steam_Gameserver::GetServerReputation()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");

    return k_uAPICallInvalid;
}

// Returns the public IP of the server according to Steam, useful when the server is 
// behind NAT and you want to advertise its IP in a lobby for other clients to directly
// connect to
uint32 Steam_Gameserver::GetPublicIP_old()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");

    return 0x7f000001;
}

SteamIPAddress_t Steam_Gameserver::GetPublicIP()
{
    APP_LOG(Log::LogLevel::TRACE, "");

    SteamIPAddress_t addr;
    addr.m_unIPv4 = GetPublicIP_old();
    addr.m_eType = k_ESteamIPTypeIPv4;

    return addr;
}

// These are in GameSocketShare mode, where instead of ISteamGameServer creating its own
// socket to talk to the master server on, it lets the game use its socket to forward messages
// back and forth. This prevents us from requiring server ops to open up yet another port
// in their firewalls.
//
// the IP address and port should be in host order, i.e 127.0.0.1 == 0x7f000001

// These are used when you've elected to multiplex the game server's UDP socket
// rather than having the master server updater use its own sockets.
// 
// Source games use this to simplify the job of the server admins, so they 
// don't have to open up more ports on their firewalls.

// Call this when a packet that starts with 0xFFFFFFFF comes in. That means
// it's for us.
bool Steam_Gameserver::HandleIncomingPacket(const void* pData, int cbData, uint32 srcIP, uint16 srcPort)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    query_datas_t query_response;

    query_server_infos_t serv_infos;

    serv_infos.appid            = _server_infos.pb_infos.appid();
    serv_infos.dedicated        = _server_infos.dedicated;
    serv_infos.description      = _server_infos.pb_infos.description();
    serv_infos.bot_player_count = _server_infos.pb_infos.bots();
    serv_infos.gamedata         = _server_infos.pb_infos.gamedata();
    serv_infos.gametags         = _server_infos.pb_infos.gametags();
    serv_infos.gametype         = _server_infos.pb_infos.gametype();
    serv_infos.has_password     = _server_infos.pb_infos.password();
    serv_infos.ip               = _server_infos.pb_infos.ip();
    serv_infos.lan              = _server_infos.lan;
    serv_infos.logged           = _server_infos.logged;
    serv_infos.map_name         = _server_infos.pb_infos.map();
    serv_infos.max_player_count = _server_infos.pb_infos.max_players();
    serv_infos.mod_dir          = _server_infos.mod_dir;
    serv_infos.name             = _server_infos.pb_infos.serv_name();
    serv_infos.owner_id         = _server_infos.owner_id;

    for (auto& player : _server_infos.pb_infos.players_infos())
    {
        std::pair<uint64, query_player_infos_t> infos;
        infos.first = player.first;
        infos.second.join_time = player.second.join_time();
        infos.second.name = player.second.name();
        infos.second.score = player.second.score();
        serv_infos.players.emplace_back(std::move(infos));
    }

    serv_infos.product          = _server_infos.product;
    serv_infos.query_port       = _server_infos.pb_infos.query_port();
    serv_infos.region           = _server_infos.region;
    serv_infos.rules            = _server_infos.rules;
    serv_infos.secure           = _server_infos.pb_infos.secured();
    serv_infos.server_id        = CSteamID(uint64(_server_infos.pb_infos.gameserver_id()));
    serv_infos.server_port      = _server_infos.pb_infos.port();
    serv_infos.spectator_port   = _server_infos.spectator_port;
    serv_infos.spectator_server = _server_infos.spectator_server;
    serv_infos.version          = std::to_string(_server_infos.pb_infos.version());

    query_response.buffer = std::move(Source_Query::handle_source_query(pData, cbData, serv_infos));
    query_response.addr.set_ip(srcIP);
    query_response.addr.set_port(srcPort);

    _outgoing_packets.emplace(query_response);

    return true;
}

// AFTER calling HandleIncomingPacket for any packets that came in that frame, call this.
// This gets a packet that the master server updater needs to send out on UDP.
// It returns the length of the packet it wants to send, or 0 if there are no more packets to send.
// Call this each frame until it returns 0.
int Steam_Gameserver::GetNextOutgoingPacket(void* pOut, int cbMaxOut, uint32* pNetAdr, uint16* pPort)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (_outgoing_packets.empty())
        return 0;

    int len = 0;

    query_datas_t& outgoing = _outgoing_packets.front();

    *pNetAdr = outgoing.addr.get_ip();
    *pPort = outgoing.addr.get_port();

    len = std::min(size_t(cbMaxOut), outgoing.buffer.size());

    memcpy(pOut, outgoing.buffer.data(), len);
    _outgoing_packets.pop();

    return len;
}

//
// Control heartbeats / advertisement with master server
//

    // Call this as often as you like to tell the master server updater whether or not
    // you want it to be active (default: off).
void Steam_Gameserver::EnableHeartbeats(bool bActive)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
}

// You usually don't need to modify this.
// Pass -1 to use the default value for iHeartbeatInterval.
// Some mods change this.
void Steam_Gameserver::SetHeartbeatInterval(int iHeartbeatInterval)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
}

// Force a heartbeat to steam at the next opportunity
void Steam_Gameserver::ForceHeartbeat()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
}

// associate this game server with this clan for the purposes of computing player compat
STEAM_CALL_RESULT(AssociateWithClanResult_t)
SteamAPICall_t Steam_Gameserver::AssociateWithClan(CSteamID steamIDClan)
{
    

    return 0;
}

// ask if any of the current players dont want to play with this new player - or vice versa
STEAM_CALL_RESULT(ComputeNewPlayerCompatibilityResult_t)
SteamAPICall_t Steam_Gameserver::ComputeNewPlayerCompatibility(CSteamID steamIDNewPlayer)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    

    return 0;
}

///////////////////////////////////////////////////////////////////////////////
//                           Network Send messages                           //
///////////////////////////////////////////////////////////////////////////////
bool Steam_Gameserver::send_gameserver_list_response(uint64 remote_id, Gameserver_List_Response_pb* resp_list)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    Network_Message_pb msg;
    Steam_Gameserver_pb* gameserver = new Steam_Gameserver_pb;

    gameserver->set_allocated_list_resp(resp_list);

    msg.set_allocated_gameserver(gameserver);

    msg.set_source_id(_server_infos.pb_infos.gameserver_id());
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

bool Steam_Gameserver::send_gameserver_infos_response(uint64 remote_id, Gameserver_Infos_Response_pb* resp)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    Network_Message_pb msg;
    Steam_Gameserver_pb* gameserver = new Steam_Gameserver_pb;

    gameserver->set_allocated_server_infos_resp(resp);

    msg.set_allocated_gameserver(gameserver);

    msg.set_source_id(_server_infos.pb_infos.gameserver_id());
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

///////////////////////////////////////////////////////////////////////////////
//                          Network Receive messages                         //
///////////////////////////////////////////////////////////////////////////////
bool Steam_Gameserver::on_peer_disconnect(Network_Message_pb const& msg, Network_Peer_Disconnect_pb const& conn)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    return true;
}

bool Steam_Gameserver::on_gameserver_list_request(Network_Message_pb const& msg, Gameserver_List_Request_pb const& list)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    Gameserver_List_Response_pb* resp = new Gameserver_List_Response_pb;

    resp->set_list_handle(list.list_handle());

    return send_gameserver_list_response(msg.source_id(), resp);
}

bool Steam_Gameserver::on_gameserver_infos_request(Network_Message_pb const& msg, Gameserver_Infos_Request_pb const& req)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    Gameserver_Infos_Response_pb* serv_infos = new Gameserver_Infos_Response_pb;
    Gameserver_infos_pb* infos = new Gameserver_infos_pb(_server_infos.pb_infos);

    serv_infos->set_list_handle(req.list_handle());
    serv_infos->set_allocated_server_infos(infos);
    serv_infos->set_ping_start(req.ping_start());

    return send_gameserver_infos_response(msg.source_id(), serv_infos);
}

///////////////////////////////////////////////////////////////////////////////
//                              IRunCallback                                 //
///////////////////////////////////////////////////////////////////////////////
bool Steam_Gameserver::CBRunFrame()
{
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    if (_server_infos.logged && !_policy_response_called)
    {
        pFrameResult_t result(new FrameResult);
        GSPolicyResponse_t& gspr = result->CreateCallback<GSPolicyResponse_t>();

        gspr.m_bSecure = (_server_infos.flags & eServerModeAuthenticationAndSecure);

        result->done = true;
        _cb_manager->add_callback(this, result);
        
        _policy_response_called = true;
    }

    return false;
}

bool Steam_Gameserver::RunNetwork(Network_Message_pb const& msg)
{
    if (msg.source_id() == Settings::Inst().userid.ConvertToUint64())
        return false;

    switch (msg.messages_case())
    {
        case Network_Message_pb::MessagesCase::kNetworkAdvertise:
        {
            Network_Advertise_pb const& adv_pb = msg.network_advertise();
            switch (adv_pb.message_case())
            {
                //case Network_Advertise_pb::MessageCase::kPeerConnect   : return on_peer_connect(msg, adv_pb.peer_connect());
                case Network_Advertise_pb::MessageCase::kPeerDisconnect: return on_peer_disconnect(msg, adv_pb.peer_disconnect());
                default: break;
            }
        }
        break;

        case Network_Message_pb::MessagesCase::kGameserver:
        {
            auto const& gameserver_msg = msg.gameserver();
            switch (gameserver_msg.messages_case())
            {
                // This will be handled in steam_matchmakingservers
                //case Steam_Gameserver_pb::MessagesCase::kListResp:
                //case Steam_Gameserver_pb::MessagesCase::kServerInfosResp:
                //    break;

                case Steam_Gameserver_pb::MessagesCase::kListReq: return on_gameserver_list_request(msg, gameserver_msg.list_req());
                case Steam_Gameserver_pb::MessagesCase::kServerInfosReq: return on_gameserver_infos_request(msg, gameserver_msg.server_infos_req());
                default: APP_LOG(Log::LogLevel::WARN, "Message unhandled %d", gameserver_msg.messages_case());
            }
        }
        break;
    }

    return false;
}

bool Steam_Gameserver::RunCallbacks(pFrameResult_t res)
{
    //switch (res->ICallback())
    //{
    //}
    return res->done;
}