/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "auth_ticket_manager.h"
#include "callback_manager.h"
#include "network.h"
#include "source_query.h"

struct query_datas_t
{
    std::vector<uint8_t> buffer;
    PortableAPI::ipv4_addr addr;
};

struct gameserver_infos_t
{
    Gameserver_infos_pb pb_infos;
    uint32_t flags;
    std::string version;
    uint64 owner_id;
    std::string gamename;
    std::string product;
    std::string mod_dir;
    bool dedicated;
    bool logged;
    uint16_t spectator_port;
    std::string spectator_server;
    std::map<std::string, std::string> rules;
    std::string region;
    bool lan;
};

class LOCAL_API Steam_Gameserver:
    public IRunCallback,
    public IRunNetwork,
    public ISteamGameServer005,
    //public ISteamGameServer006,
    //public ISteamGameServer007,
    public ISteamGameServer008,
    public ISteamGameServer009,
    public ISteamGameServer010,
    public ISteamGameServer011,
    public ISteamGameServer012,
    public ISteamGameServer013
{
    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;

    bool _policy_response_called;

    std::queue<query_datas_t> _outgoing_packets;
    Auth_Ticket_Manager* _auth_manager;

public:
    std::recursive_mutex _local_mutex;

    gameserver_infos_t _server_infos;

    Steam_Gameserver();
    virtual ~Steam_Gameserver();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    virtual bool CBRunFrame();
    virtual bool RunNetwork(Network_Message_pb const& msg);
    virtual bool RunCallbacks(pFrameResult_t res);

    // Send Network messages
    bool send_gameserver_list_response(uint64 remote_id, Gameserver_List_Response_pb* resp_list);
    bool send_gameserver_infos_response(uint64 remote_id, Gameserver_Infos_Response_pb* resp);
    // Receive Network messages
    bool on_peer_disconnect(Network_Message_pb const& msg, Network_Peer_Disconnect_pb const& conn);
    bool on_gameserver_list_request(Network_Message_pb const& msg, Gameserver_List_Request_pb const& list);
    bool on_gameserver_infos_request(Network_Message_pb const& msg,Gameserver_Infos_Request_pb const& req);

    //
    // Basic server data.  These properties, if set, must be set before before calling LogOn.  They
    // may not be changed after logged in.
    //

    /// This is called by SteamGameServer_Init, and you will usually not need to call it directly
    virtual bool InitGameServer(uint32 unIP, uint16 usGamePort, uint16 usQueryPort, uint32 unFlags, AppId_t nGameAppId, const char* pchVersionString);

    /// Game product identifier.  This is currently used by the master server for version checking purposes.
    /// It's a required field, but will eventually will go away, and the AppID will be used for this purpose.
    virtual void SetProduct(const char* pszProduct);

    /// Description of the game.  This is a required field and is displayed in the steam server browser....for now.
    /// This is a required field, but it will go away eventually, as the data should be determined from the AppID.
    virtual void SetGameDescription(const char* pszGameDescription);

    /// If your game is a "mod," pass the string that identifies it.  The default is an empty string, meaning
    /// this application is the original game, not a mod.
    ///
    /// @see k_cbMaxGameServerGameDir
    virtual void SetModDir(const char* pszModDir);

    /// Is this is a dedicated server?  The default value is false.
    virtual void SetDedicatedServer(bool bDedicated);

    //
    // Login
    //

    /// Begin process to login to a persistent game server account
    ///
    /// You need to register for callbacks to determine the result of this operation.
    /// @see SteamServersConnected_t
    /// @see SteamServerConnectFailure_t
    /// @see SteamServersDisconnected_t
    virtual void LogOn();
    virtual void LogOn(const char* pszAccountName, const char* pszPassword);
    virtual void LogOn(const char* pszToken);

    /// Login to a generic, anonymous account.
    ///
    /// Note: in previous versions of the SDK, this was automatically called within SteamGameServer_Init,
    /// but this is no longer the case.
    virtual void LogOnAnonymous();

    /// Begin process of logging game server out of steam
    virtual void LogOff();

    // status functions
    virtual bool BLoggedOn();
    virtual bool BSecure();
    virtual CSteamID GetSteamID();

    /// Returns true if the master server has requested a restart.
    /// Only returns true once per request.
    virtual bool WasRestartRequested();

    //
    // Server state.  These properties may be changed at any time.
    //

        /// Max player count that will be reported to server browser and client queries
    virtual void SetMaxPlayerCount(int cPlayersMax);

    /// Number of bots.  Default value is zero
    virtual void SetBotPlayerCount(int cBotplayers);

    /// Set the name of server as it will appear in the server browser
    ///
    /// @see k_cbMaxGameServerName
    virtual void SetServerName(const char* pszServerName);

    /// Set name of map to report in the server browser
    ///
    /// @see k_cbMaxGameServerName
    virtual void SetMapName(const char* pszMapName);

    /// Let people know if your server will require a password
    virtual void SetPasswordProtected(bool bPasswordProtected);

    /// Spectator server.  The default value is zero, meaning the service
    /// is not used.
    virtual void SetSpectatorPort(uint16 unSpectatorPort);

    /// Name of the spectator server.  (Only used if spectator port is nonzero.)
    ///
    /// @see k_cbMaxGameServerMapName
    virtual void SetSpectatorServerName(const char* pszSpectatorServerName);

    /// Call this to clear the whole list of key/values that are sent in rules queries.
    virtual void ClearAllKeyValues();

    /// Call this to add/update a key/value pair.
    virtual void SetKeyValue(const char* pKey, const char* pValue);

    /// Sets a string defining the "gametags" for this server, this is optional, but if it is set
    /// it allows users to filter in the matchmaking/server-browser interfaces based on the value
    ///
    /// @see k_cbMaxGameServerTags
    virtual void SetGameTags(const char* pchGameTags);

    /// Sets a string defining the "gamedata" for this server, this is optional, but if it is set
    /// it allows users to filter in the matchmaking/server-browser interfaces based on the value
    /// don't set this unless it actually changes, its only uploaded to the master once (when
    /// acknowledged)
    ///
    /// @see k_cbMaxGameServerGameData
    virtual void SetGameData(const char* pchGameData);

    /// Region identifier.  This is an optional field, the default value is empty, meaning the "world" region
    virtual void SetRegion(const char* pszRegion);

    //
    // Player list management / authentication
    //

    // Handles receiving a new connection from a Steam user.  This call will ask the Steam
    // servers to validate the users identity, app ownership, and VAC status.  If the Steam servers 
    // are off-line, then it will validate the cached ticket itself which will validate app ownership 
    // and identity.  The AuthBlob here should be acquired on the game client using SteamUser()->InitiateGameConnection()
    // and must then be sent up to the game server for authentication.
    //
    // Return Value: returns true if the users ticket passes basic checks. pSteamIDUser will contain the Steam ID of this user. pSteamIDUser must NOT be NULL
    // If the call succeeds then you should expect a GSClientApprove_t or GSClientDeny_t callback which will tell you whether authentication
    // for the user has succeeded or failed (the steamid in the callback will match the one returned by this call)
    virtual bool SendUserConnectAndAuthenticate(uint32 unIPClient, const void* pvAuthBlob, uint32 cubAuthBlobSize, CSteamID* pSteamIDUser);

    // Creates a fake user (ie, a bot) which will be listed as playing on the server, but skips validation.  
    // 
    // Return Value: Returns a SteamID for the user to be tracked with, you should call HandleUserDisconnect()
    // when this user leaves the server just like you would for a real user.
    virtual CSteamID CreateUnauthenticatedUserConnection();

    // Should be called whenever a user leaves our game server, this lets Steam internally
    // track which users are currently on which servers for the purposes of preventing a single
    // account being logged into multiple servers, showing who is currently on a server, etc.
    virtual void SendUserDisconnect(CSteamID steamIDUser);

    // Update the data to be displayed in the server browser and matchmaking interfaces for a user
    // currently connected to the server.  For regular users you must call this after you receive a
    // GSUserValidationSuccess callback.
    // 
    // Return Value: true if successful, false if failure (ie, steamIDUser wasn't for an active player)
    virtual bool BUpdateUserData(CSteamID steamIDUser, const char* pchPlayerName, uint32 uScore);

    virtual bool BSetServerType(uint32 unServerFlags, uint32 unGameIP, uint16 unGamePort,
        uint16 unSpectatorPort, uint16 usQueryPort, const char* pchGameDir, const char* pchVersion, bool bLANMode);

    // Updates server status values which shows up in the server browser and matchmaking APIs
    virtual void UpdateServerStatus(int cPlayers, int cPlayersMax, int cBotPlayers,
        const char* pchServerName, const char* pSpectatorServerName,
        const char* pchMapName);

    // This can be called if spectator goes away or comes back (passing 0 means there is no spectator server now).
    virtual void UpdateSpectatorPort(uint16 unSpectatorPort);

    // Sets a string defining the "gametype" for this server, this is optional, but if it is set
    // it allows users to filter in the matchmaking/server-browser interfaces based on the value
    virtual void SetGameType(const char* pchGameType);

    // Ask if a user has a specific achievement for this game, will get a callback on reply
    virtual bool BGetUserAchievementStatus(CSteamID steamID, const char* pchAchievementName);

    // New auth system APIs - do not mix with the old auth system APIs.
    // ----------------------------------------------------------------

    // Retrieve ticket to be sent to the entity who wishes to authenticate you ( using BeginAuthSession API ). 
    // pcbTicket retrieves the length of the actual ticket.
    virtual HAuthTicket GetAuthSessionTicket(void* pTicket, int cbMaxTicket, uint32* pcbTicket);

    // Authenticate ticket ( from GetAuthSessionTicket ) from entity steamID to be sure it is valid and isnt reused
    // Registers for callbacks if the entity goes offline or cancels the ticket ( see ValidateAuthTicketResponse_t callback and EAuthSessionResponse )
    virtual EBeginAuthSessionResult BeginAuthSession(const void* pAuthTicket, int cbAuthTicket, CSteamID steamID);

    // Stop tracking started by BeginAuthSession - called when no longer playing game with this entity
    virtual void EndAuthSession(CSteamID steamID);

    // Cancel auth ticket from GetAuthSessionTicket, called when no longer playing game with the entity you gave the ticket to
    virtual void CancelAuthTicket(HAuthTicket hAuthTicket);

    // After receiving a user's authentication data, and passing it to SendUserConnectAndAuthenticate, use this function
    // to determine if the user owns downloadable content specified by the provided AppID.
    virtual EUserHasLicenseForAppResult UserHasLicenseForApp(CSteamID steamID, AppId_t appID);

    // Ask if a user in in the specified group, results returns async by GSUserGroupStatus_t
    // returns false if we're not connected to the steam servers and thus cannot ask
    virtual bool RequestUserGroupStatus(CSteamID steamIDUser, CSteamID steamIDGroup);


    // these two functions s are deprecated, and will not return results
    // they will be removed in a future version of the SDK
    virtual void GetGameplayStats();
    STEAM_CALL_RESULT(GSReputation_t)
    virtual SteamAPICall_t GetServerReputation();

    // Returns the public IP of the server according to Steam, useful when the server is 
    // behind NAT and you want to advertise its IP in a lobby for other clients to directly
    // connect to
    virtual uint32 GetPublicIP_old();
    virtual SteamIPAddress_t GetPublicIP();

    // These are in GameSocketShare mode, where instead of ISteamGameServer creating its own
    // socket to talk to the master server on, it lets the game use its socket to forward messages
    // back and forth. This prevents us from requiring server ops to open up yet another port
    // in their firewalls.
    //
    // the IP address and port should be in host order, i.e 127.0.0.1 == 0x7f000001

    // These are used when you've elected to multiplex the game server's UDP socket
    // rather than having the master server updater use its own sockets.
    // 
    // Source games use this to simplify the job of the server admins, so they 
    // don't have to open up more ports on their firewalls.

    // Call this when a packet that starts with 0xFFFFFFFF comes in. That means
    // it's for us.
    virtual bool HandleIncomingPacket(const void* pData, int cbData, uint32 srcIP, uint16 srcPort);

    // AFTER calling HandleIncomingPacket for any packets that came in that frame, call this.
    // This gets a packet that the master server updater needs to send out on UDP.
    // It returns the length of the packet it wants to send, or 0 if there are no more packets to send.
    // Call this each frame until it returns 0.
    virtual int GetNextOutgoingPacket(void* pOut, int cbMaxOut, uint32* pNetAdr, uint16* pPort);

    //
    // Control heartbeats / advertisement with master server
    //

    // Call this as often as you like to tell the master server updater whether or not
    // you want it to be active (default: off).
    virtual void EnableHeartbeats(bool bActive);

    // You usually don't need to modify this.
    // Pass -1 to use the default value for iHeartbeatInterval.
    // Some mods change this.
    virtual void SetHeartbeatInterval(int iHeartbeatInterval);

    // Force a heartbeat to steam at the next opportunity
    virtual void ForceHeartbeat();

    // associate this game server with this clan for the purposes of computing player compat
    STEAM_CALL_RESULT(AssociateWithClanResult_t)
    virtual SteamAPICall_t AssociateWithClan(CSteamID steamIDClan);

    // ask if any of the current players dont want to play with this new player - or vice versa
    STEAM_CALL_RESULT(ComputeNewPlayerCompatibilityResult_t)
    virtual SteamAPICall_t ComputeNewPlayerCompatibility(CSteamID steamIDNewPlayer);
};