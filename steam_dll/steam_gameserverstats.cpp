/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_gameserverstats.h"

Steam_GameServerStats::Steam_GameServerStats()
{
}

Steam_GameServerStats::~Steam_GameServerStats()
{
    emu_deinit();
}

void Steam_GameServerStats::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _cb_manager = cb_manager;
    _network = network;
}

void Steam_GameServerStats::emu_deinit()
{
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _network.reset();
    _cb_manager.reset();
}

// downloads stats for the user
// returns a GSStatsReceived_t callback when completed
// if the user has no stats, GSStatsReceived_t.m_eResult will be set to k_EResultFail
// these stats will only be auto-updated for clients playing on the server. For other
// users you'll need to call RequestUserStats() again to refresh any data
STEAM_CALL_RESULT(GSStatsReceived_t)
SteamAPICall_t Steam_GameServerStats::RequestUserStats(CSteamID steamIDUser)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

// requests stat information for a user, usable after a successful call to RequestUserStats()
bool Steam_GameServerStats::GetUserStat(CSteamID steamIDUser, const char* pchName, int32* pData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_GameServerStats::GetUserStat(CSteamID steamIDUser, const char* pchName, float* pData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_GameServerStats::GetUserAchievement(CSteamID steamIDUser, const char* pchName, bool* pbAchieved)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Set / update stats and achievements. 
// Note: These updates will work only on stats game servers are allowed to edit and only for 
// game servers that have been declared as officially controlled by the game creators. 
// Set the IP range of your official servers on the Steamworks page
bool Steam_GameServerStats::SetUserStat(CSteamID steamIDUser, const char* pchName, int32 nData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_GameServerStats::SetUserStat(CSteamID steamIDUser, const char* pchName, float fData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_GameServerStats::UpdateUserAvgRateStat(CSteamID steamIDUser, const char* pchName, float flCountThisSession, double dSessionLength)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_GameServerStats::SetUserAchievement(CSteamID steamIDUser, const char* pchName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_GameServerStats::ClearUserAchievement(CSteamID steamIDUser, const char* pchName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Store the current data on the server, will get a GSStatsStored_t callback when set.
//
// If the callback has a result of k_EResultInvalidParam, one or more stats 
// uploaded has been rejected, either because they broke constraints
// or were out of date. In this case the server sends back updated values.
// The stats should be re-iterated to keep in sync.
STEAM_CALL_RESULT(GSStatsStored_t)
SteamAPICall_t Steam_GameServerStats::StoreUserStats(CSteamID steamIDUser)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}