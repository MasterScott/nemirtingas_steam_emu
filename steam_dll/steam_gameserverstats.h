/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"

class LOCAL_API Steam_GameServerStats:
    public ISteamGameServerStats001
{
    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;

public:
    std::recursive_mutex _local_mutex;

    Steam_GameServerStats();
    virtual ~Steam_GameServerStats();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    // downloads stats for the user
    // returns a GSStatsReceived_t callback when completed
    // if the user has no stats, GSStatsReceived_t.m_eResult will be set to k_EResultFail
    // these stats will only be auto-updated for clients playing on the server. For other
    // users you'll need to call RequestUserStats() again to refresh any data
    STEAM_CALL_RESULT(GSStatsReceived_t)
    virtual SteamAPICall_t RequestUserStats(CSteamID steamIDUser);

    // requests stat information for a user, usable after a successful call to RequestUserStats()
    virtual bool GetUserStat(CSteamID steamIDUser, const char* pchName, int32* pData);
    virtual bool GetUserStat(CSteamID steamIDUser, const char* pchName, float* pData);
    virtual bool GetUserAchievement(CSteamID steamIDUser, const char* pchName, bool* pbAchieved);

    // Set / update stats and achievements. 
    // Note: These updates will work only on stats game servers are allowed to edit and only for 
    // game servers that have been declared as officially controlled by the game creators. 
    // Set the IP range of your official servers on the Steamworks page
    virtual bool SetUserStat(CSteamID steamIDUser, const char* pchName, int32 nData);
    virtual bool SetUserStat(CSteamID steamIDUser, const char* pchName, float fData);
    virtual bool UpdateUserAvgRateStat(CSteamID steamIDUser, const char* pchName, float flCountThisSession, double dSessionLength);

    virtual bool SetUserAchievement(CSteamID steamIDUser, const char* pchName);
    virtual bool ClearUserAchievement(CSteamID steamIDUser, const char* pchName);

    // Store the current data on the server, will get a GSStatsStored_t callback when set.
    //
    // If the callback has a result of k_EResultInvalidParam, one or more stats 
    // uploaded has been rejected, either because they broke constraints
    // or were out of date. In this case the server sends back updated values.
    // The stats should be re-iterated to keep in sync.
    STEAM_CALL_RESULT(GSStatsStored_t)
    virtual SteamAPICall_t StoreUserStats(CSteamID steamIDUser);
};