/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_htmlsurface.h"
#include "steam_client.h"
#include "settings.h"

Steam_HTMLSurface::Steam_HTMLSurface()
{
}

Steam_HTMLSurface::~Steam_HTMLSurface()
{
    emu_deinit();
}

void Steam_HTMLSurface::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _cb_manager = cb_manager;
    _network = network;
}

void Steam_HTMLSurface::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _network.reset();
    _cb_manager.reset();
}

// Must call init and shutdown when starting/ending use of the interface
bool Steam_HTMLSurface::Init()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return true;
}
bool Steam_HTMLSurface::Shutdown()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return true;
}

// Create a browser object for display of a html page, when creation is complete the call handle
// will return a HTML_BrowserReady_t callback for the HHTMLBrowser of your new browser.
//   The user agent string is a substring to be added to the general user agent string so you can
// identify your client on web servers.
//   The userCSS string lets you apply a CSS style sheet to every displayed page, leave null if
// you do not require this functionality.
//
// YOU MUST HAVE IMPLEMENTED HANDLERS FOR HTML_BrowserReady_t, HTML_StartRequest_t,
// HTML_JSAlert_t, HTML_JSConfirm_t, and HTML_FileOpenDialog_t! See the CALLBACKS
// section of this interface (AllowStartRequest, etc) for more details. If you do
// not implement these callback handlers, the browser may appear to hang instead of
// navigating to new pages or triggering javascript popups.
//
STEAM_CALL_RESULT(HTML_BrowserReady_t)
SteamAPICall_t Steam_HTMLSurface::CreateBrowser(const char* pchUserAgent, const char* pchUserCSS)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    pFrameResult_t res(new FrameResult);

    HTML_BrowserReady_t& hbr = res->CreateCallback<HTML_BrowserReady_t>();
    hbr.unBrowserHandle = 0x12345678;

    res->done = true;

    return res->GetAPICall();
}

// Call this when you are done with a html surface, this lets us free the resources being used by it
void Steam_HTMLSurface::RemoveBrowser(HHTMLBrowser unBrowserHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// Navigate to this URL, results in a HTML_StartRequest_t as the request commences 
void Steam_HTMLSurface::LoadURL(HHTMLBrowser unBrowserHandle, const char* pchURL, const char* pchPostData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    static char url[256];
    strncpy(url, pchURL, sizeof(url));
    static char target[] = "_self";
    static char title[] = "title";

    {
        pFrameResult_t res(new FrameResult);
        HTML_StartRequest_t& hst = res->CreateCallback<HTML_StartRequest_t>();
        hst.unBrowserHandle = unBrowserHandle;
        hst.pchURL = url;
        hst.pchTarget = target;
        hst.pchPostData = "";
        hst.bIsRedirect = false;

        res->done = true;
        _cb_manager->add_callback(this, res);
    }

    {
        pFrameResult_t res(new FrameResult);
        HTML_FinishedRequest_t& hfr = res->CreateCallback<HTML_FinishedRequest_t>(std::chrono::milliseconds(800));
        hfr.unBrowserHandle = unBrowserHandle;
        hfr.pchURL = url;
        hfr.pchPageTitle = title;

        res->done = true;
        _cb_manager->add_callback(this, res);
    }

}

// Tells the surface the size in pixels to display the surface
void Steam_HTMLSurface::SetSize(HHTMLBrowser unBrowserHandle, uint32 unWidth, uint32 unHeight)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// Stop the load of the current html page
void Steam_HTMLSurface::StopLoad(HHTMLBrowser unBrowserHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}
// Reload (most likely from local cache) the current page
void Steam_HTMLSurface::Reload(HHTMLBrowser unBrowserHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}
// navigate back in the page history
void Steam_HTMLSurface::GoBack(HHTMLBrowser unBrowserHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}
// navigate forward in the page history
void Steam_HTMLSurface::GoForward(HHTMLBrowser unBrowserHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// add this header to any url requests from this browser
void Steam_HTMLSurface::AddHeader(HHTMLBrowser unBrowserHandle, const char* pchKey, const char* pchValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}
// run this javascript script in the currently loaded page
void Steam_HTMLSurface::ExecuteJavascript(HHTMLBrowser unBrowserHandle, const char* pchScript)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// Mouse click and mouse movement commands
void Steam_HTMLSurface::MouseUp(HHTMLBrowser unBrowserHandle, EHTMLMouseButton eMouseButton)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}
void Steam_HTMLSurface::MouseDown(HHTMLBrowser unBrowserHandle, EHTMLMouseButton eMouseButton)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}
void Steam_HTMLSurface::MouseDoubleClick(HHTMLBrowser unBrowserHandle, EHTMLMouseButton eMouseButton)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}
// x and y are relative to the HTML bounds
void Steam_HTMLSurface::MouseMove(HHTMLBrowser unBrowserHandle, int x, int y)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}
// nDelta is pixels of scroll
void Steam_HTMLSurface::MouseWheel(HHTMLBrowser unBrowserHandle, int32 nDelta)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// keyboard interactions, native keycode is the key code value from your OS, system key flags the key to not
// be sent as a typed character as well as a key down
void Steam_HTMLSurface::KeyDown(HHTMLBrowser unBrowserHandle, uint32 nNativeKeyCode, EHTMLKeyModifiers eHTMLKeyModifiers)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

void Steam_HTMLSurface::KeyDown(HHTMLBrowser unBrowserHandle, uint32 nNativeKeyCode, EHTMLKeyModifiers eHTMLKeyModifiers, bool bIsSystemKey)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}
void Steam_HTMLSurface::KeyUp(HHTMLBrowser unBrowserHandle, uint32 nNativeKeyCode, EHTMLKeyModifiers eHTMLKeyModifiers)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}
// cUnicodeChar is the unicode character point for this keypress (and potentially multiple chars per press)
void Steam_HTMLSurface::KeyChar(HHTMLBrowser unBrowserHandle, uint32 cUnicodeChar, EHTMLKeyModifiers eHTMLKeyModifiers)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// programmatically scroll this many pixels on the page
void Steam_HTMLSurface::SetHorizontalScroll(HHTMLBrowser unBrowserHandle, uint32 nAbsolutePixelScroll)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}
void Steam_HTMLSurface::SetVerticalScroll(HHTMLBrowser unBrowserHandle, uint32 nAbsolutePixelScroll)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// tell the html control if it has key focus currently, controls showing the I-beam cursor in text controls amongst other things
void Steam_HTMLSurface::SetKeyFocus(HHTMLBrowser unBrowserHandle, bool bHasKeyFocus)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// open the current pages html code in the local editor of choice, used for debugging
void Steam_HTMLSurface::ViewSource(HHTMLBrowser unBrowserHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}
// copy the currently selected text on the html page to the local clipboard
void Steam_HTMLSurface::CopyToClipboard(HHTMLBrowser unBrowserHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}
// paste from the local clipboard to the current html page
void Steam_HTMLSurface::PasteFromClipboard(HHTMLBrowser unBrowserHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// find this string in the browser, if bCurrentlyInFind is true then instead cycle to the next matching element
void Steam_HTMLSurface::Find(HHTMLBrowser unBrowserHandle, const char* pchSearchStr, bool bCurrentlyInFind, bool bReverse)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}
// cancel a currently running find
void Steam_HTMLSurface::StopFind(HHTMLBrowser unBrowserHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// return details about the link at position x,y on the current page
void Steam_HTMLSurface::GetLinkAtPosition(HHTMLBrowser unBrowserHandle, int x, int y)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// set a webcookie for the hostname in question
void Steam_HTMLSurface::SetCookie(const char* pchHostname, const char* pchKey, const char* pchValue, const char* pchPath, RTime32 nExpires, bool bSecure, bool bHTTPOnly)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// Zoom the current page by flZoom ( from 0.0 to 2.0, so to zoom to 120% use 1.2 ), zooming around point X,Y in the page (use 0,0 if you don't care)
void Steam_HTMLSurface::SetPageScaleFactor(HHTMLBrowser unBrowserHandle, float flZoom, int nPointX, int nPointY)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// Enable/disable low-resource background mode, where javascript and repaint timers are throttled, resources are
// more aggressively purged from memory, and audio/video elements are paused. When background mode is enabled,
// all HTML5 video and audio objects will execute ".pause()" and gain the property "._steam_background_paused = 1".
// When background mode is disabled, any video or audio objects with that property will resume with ".play()".
void Steam_HTMLSurface::SetBackgroundMode(HHTMLBrowser unBrowserHandle, bool bBackgroundMode)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// Scale the output display space by this factor, this is useful when displaying content on high dpi devices.
// Specifies the ratio between physical and logical pixels.
void Steam_HTMLSurface::SetDPIScalingFactor(HHTMLBrowser unBrowserHandle, float flDPIScaling)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// Open HTML/JS developer tools
void Steam_HTMLSurface::OpenDeveloperTools(HHTMLBrowser unBrowserHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// CALLBACKS
//
//  These set of functions are used as responses to callback requests
//

// You MUST call this in response to a HTML_StartRequest_t callback
//  Set bAllowed to true to allow this navigation, false to cancel it and stay 
// on the current page. You can use this feature to limit the valid pages
// allowed in your HTML surface.
void Steam_HTMLSurface::AllowStartRequest(HHTMLBrowser unBrowserHandle, bool bAllowed)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// You MUST call this in response to a HTML_JSAlert_t or HTML_JSConfirm_t callback
//  Set bResult to true for the OK option of a confirm, use false otherwise
void Steam_HTMLSurface::JSDialogResponse(HHTMLBrowser unBrowserHandle, bool bResult)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// You MUST call this in response to a HTML_FileOpenDialog_t callback
STEAM_IGNOREATTR()
void Steam_HTMLSurface::FileLoadDialogResponse(HHTMLBrowser unBrowserHandle, const char** pchSelectedFiles)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

///////////////////////////////////////////////////////////////////////////////
//                                 IRunCallback                                 //
///////////////////////////////////////////////////////////////////////////////
bool Steam_HTMLSurface::CBRunFrame()
{
    return true;
}
bool Steam_HTMLSurface::RunNetwork(Network_Message_pb const& msg)
{
    return true;
}

bool Steam_HTMLSurface::RunCallbacks(pFrameResult_t res)
{
    return res->done;
}