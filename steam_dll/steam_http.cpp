/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_http.h"
#include "steam_client.h"

Steam_HTTP::Steam_HTTP():
    _request_handle(INVALID_HTTPREQUEST_HANDLE)
{
}

Steam_HTTP::~Steam_HTTP()
{
    emu_deinit();
}

void Steam_HTTP::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _cb_manager = cb_manager;
    _network = network;
}

void Steam_HTTP::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _network.reset();
    _cb_manager.reset();
}

HTTPRequestHandle Steam_HTTP::gen_request()
{
    return ++_request_handle;
}

http_request_t* Steam_HTTP::get_request(HTTPRequestHandle handle)
{
    auto it = _requests.find(handle);
    return (it == _requests.end() ? nullptr : &it->second);
}

// Initializes a new HTTP request, returning a handle to use in further operations on it.  Requires
// the method (GET or POST) and the absolute URL for the request.  Both http and https are supported,
// so this string must start with http:// or https:// and should look like http://store.steampowered.com/app/250/ 
// or such.
HTTPRequestHandle Steam_HTTP::CreateHTTPRequest(EHTTPMethod eHTTPRequestMethod, const char* pchAbsoluteURL)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (pchAbsoluteURL == nullptr)
        return INVALID_HTTPREQUEST_HANDLE;

    HTTPRequestHandle h = gen_request();

    auto& req = _requests[h];
    req.method = eHTTPRequestMethod;
    req.url = pchAbsoluteURL;

    return h;
}

// Set a context value for the request, which will be returned in the HTTPRequestCompleted_t callback after
// sending the request.  This is just so the caller can easily keep track of which callbacks go with which request data.
bool Steam_HTTP::SetHTTPRequestContextValue(HTTPRequestHandle hRequest, uint64 ulContextValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr)
        return false;

    pRequest->context_value = ulContextValue;

    return true;
}

// Set a timeout in seconds for the HTTP request, must be called prior to sending the request.  Default
// timeout is 60 seconds if you don't call this.  Returns false if the handle is invalid, or the request
// has already been sent.
bool Steam_HTTP::SetHTTPRequestNetworkActivityTimeout(HTTPRequestHandle hRequest, uint32 unTimeoutSeconds)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr)
        return false;

    return true;
}

// Set a request header value for the request, must be called prior to sending the request.  Will 
// return false if the handle is invalid or the request is already sent.
bool Steam_HTTP::SetHTTPRequestHeaderValue(HTTPRequestHandle hRequest, const char* pchHeaderName, const char* pchHeaderValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr || pRequest->sent || pchHeaderName == nullptr || pchHeaderValue == nullptr)
        return false;

    pRequest->headers[pchHeaderName] = pchHeaderValue;

    return true;
}

// Set a GET or POST parameter value on the request, which is set will depend on the EHTTPMethod specified
// when creating the request.  Must be called prior to sending the request.  Will return false if the 
// handle is invalid or the request is already sent.
bool Steam_HTTP::SetHTTPRequestGetOrPostParameter(HTTPRequestHandle hRequest, const char* pchParamName, const char* pchParamValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr || pRequest->sent || pchParamName == nullptr || pchParamValue == nullptr)
        return false;

    pRequest->form_parameters[pchParamName] = pchParamValue;

    return true;
}

// Sends the HTTP request, will return false on a bad handle, otherwise use SteamCallHandle to wait on
// asynchronous response via callback.
//
// Note: If the user is in offline mode in Steam, then this will add a only-if-cached cache-control 
// header and only do a local cache lookup rather than sending any actual remote request.
bool Steam_HTTP::SendHTTPRequest(HTTPRequestHandle hRequest, SteamAPICall_t* pCallHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr)
        return false;

    pRequest->sent = true;

    pFrameResult_t res(new FrameResult);

    HTTPRequestCompleted_t& hrc = res->CreateCallback<HTTPRequestCompleted_t>();
    hrc.m_bRequestSuccessful = false;
    hrc.m_eStatusCode = k_EHTTPStatusCode404NotFound;
    hrc.m_unBodySize = 0;
    hrc.m_ulContextValue = pRequest->context_value;
    hrc.m_hRequest = hRequest;

    res->done = true;
    _cb_manager->add_apicall(nullptr, res);

    if (pCallHandle != nullptr)
    {
        *pCallHandle = res->GetAPICall();
    }

    return true;
}

// Sends the HTTP request, will return false on a bad handle, otherwise use SteamCallHandle to wait on
// asynchronous response via callback for completion, and listen for HTTPRequestHeadersReceived_t and 
// HTTPRequestDataReceived_t callbacks while streaming.
bool Steam_HTTP::SendHTTPRequestAndStreamResponse(HTTPRequestHandle hRequest, SteamAPICall_t* pCallHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr)
        return false;

    if (pCallHandle != nullptr)
    {
        *pCallHandle = k_uAPICallInvalid;
    }

    return true;
}

// Defers a request you have sent, the actual HTTP client code may have many requests queued, and this will move
// the specified request to the tail of the queue.  Returns false on invalid handle, or if the request is not yet sent.
bool Steam_HTTP::DeferHTTPRequest(HTTPRequestHandle hRequest)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr || !pRequest->sent)
        return false;

    return true;
}

// Prioritizes a request you have sent, the actual HTTP client code may have many requests queued, and this will move
// the specified request to the head of the queue.  Returns false on invalid handle, or if the request is not yet sent.
bool Steam_HTTP::PrioritizeHTTPRequest(HTTPRequestHandle hRequest)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr || !pRequest->sent)
        return false;

    return true;
}

// Checks if a response header is present in a HTTP response given a handle from HTTPRequestCompleted_t, also 
// returns the size of the header value if present so the caller and allocate a correctly sized buffer for
// GetHTTPResponseHeaderValue.
bool Steam_HTTP::GetHTTPResponseHeaderSize(HTTPRequestHandle hRequest, const char* pchHeaderName, uint32* unResponseHeaderSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr)
        return false;

    return false;
}

// Gets header values from a HTTP response given a handle from HTTPRequestCompleted_t, will return false if the
// header is not present or if your buffer is too small to contain it's value.  You should first call 
// BGetHTTPResponseHeaderSize to check for the presence of the header and to find out the size buffer needed.
bool Steam_HTTP::GetHTTPResponseHeaderValue(HTTPRequestHandle hRequest, const char* pchHeaderName, uint8* pHeaderValueBuffer, uint32 unBufferSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr)
        return false;

    return false;
}

// Gets the size of the body data from a HTTP response given a handle from HTTPRequestCompleted_t, will return false if the 
// handle is invalid.
bool Steam_HTTP::GetHTTPResponseBodySize(HTTPRequestHandle hRequest, uint32* unBodySize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr)
        return false;

    if (unBodySize != nullptr)
    {
        *unBodySize = 0;
    }

    return true;
}

// Gets the body data from a HTTP response given a handle from HTTPRequestCompleted_t, will return false if the 
// handle is invalid or is to a streaming response, or if the provided buffer is not the correct size.  Use BGetHTTPResponseBodySize first to find out
// the correct buffer size to use.
bool Steam_HTTP::GetHTTPResponseBodyData(HTTPRequestHandle hRequest, uint8* pBodyDataBuffer, uint32 unBufferSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr || pBodyDataBuffer == nullptr || unBufferSize == 0)
        return false;

    *pBodyDataBuffer = 0;

    return true;
}

// Gets the body data from a streaming HTTP response given a handle from HTTPRequestDataReceived_t. Will return false if the 
// handle is invalid or is to a non-streaming response (meaning it wasn't sent with SendHTTPRequestAndStreamResponse), or if the buffer size and offset 
// do not match the size and offset sent in HTTPRequestDataReceived_t.
bool Steam_HTTP::GetHTTPStreamingResponseBodyData(HTTPRequestHandle hRequest, uint32 cOffset, uint8* pBodyDataBuffer, uint32 unBufferSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr)
        return false;

    return false;
}

// Releases an HTTP response handle, should always be called to free resources after receiving a HTTPRequestCompleted_t
// callback and finishing using the response.
bool Steam_HTTP::ReleaseHTTPRequest(HTTPRequestHandle hRequest)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto it = _requests.find(hRequest);
    if (it == _requests.end())
        return false;

    _requests.erase(it);

    return true;
}

// Gets progress on downloading the body for the request.  This will be zero unless a response header has already been
// received which included a content-length field.  For responses that contain no content-length it will report
// zero for the duration of the request as the size is unknown until the connection closes.
bool Steam_HTTP::GetHTTPDownloadProgressPct(HTTPRequestHandle hRequest, float* pflPercentOut)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr)
        return false;

    return false;
}

// Sets the body for an HTTP Post request.  Will fail and return false on a GET request, and will fail if POST params
// have already been set for the request.  Setting this raw body makes it the only contents for the post, the pchContentType
// parameter will set the content-type header for the request so the server may know how to interpret the body.
bool Steam_HTTP::SetHTTPRequestRawPostBody(HTTPRequestHandle hRequest, const char* pchContentType, uint8* pubBody, uint32 unBodyLen)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr)
        return false;

    return true;
}

// Creates a cookie container handle which you must later free with ReleaseCookieContainer().  If bAllowResponsesToModify=true
// than any response to your requests using this cookie container may add new cookies which may be transmitted with
// future requests.  If bAllowResponsesToModify=false than only cookies you explicitly set will be sent.  This API is just for
// during process lifetime, after steam restarts no cookies are persisted and you have no way to access the cookie container across
// repeat executions of your process.
HTTPCookieContainerHandle Steam_HTTP::CreateCookieContainer(bool bAllowResponsesToModify)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// Release a cookie container you are finished using, freeing it's memory
bool Steam_HTTP::ReleaseCookieContainer(HTTPCookieContainerHandle hCookieContainer)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Adds a cookie to the specified cookie container that will be used with future requests.
bool Steam_HTTP::SetCookie(HTTPCookieContainerHandle hCookieContainer, const char* pchHost, const char* pchUrl, const char* pchCookie)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Set the cookie container to use for a HTTP request
bool Steam_HTTP::SetHTTPRequestCookieContainer(HTTPRequestHandle hRequest, HTTPCookieContainerHandle hCookieContainer)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr)
        return false;

    return false;
}

// Set the extra user agent info for a request, this doesn't clobber the normal user agent, it just adds the extra info on the end
bool Steam_HTTP::SetHTTPRequestUserAgentInfo(HTTPRequestHandle hRequest, const char* pchUserAgentInfo)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr || pchUserAgentInfo == nullptr)
        return false;

    pRequest->user_agent = pchUserAgentInfo;

    return true;
}

// Disable or re-enable verification of SSL/TLS certificates.
// By default, certificates are checked for all HTTPS requests.
bool Steam_HTTP::SetHTTPRequestRequiresVerifiedCertificate(HTTPRequestHandle hRequest, bool bRequireVerifiedCertificate)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr)
        return false;

    pRequest->verify_cert = bRequireVerifiedCertificate;

    return true;
}

// Set an absolute timeout on the HTTP request, this is just a total time timeout different than the network activity timeout
// which can bump everytime we get more data
bool Steam_HTTP::SetHTTPRequestAbsoluteTimeoutMS(HTTPRequestHandle hRequest, uint32 unMilliseconds)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr)
        return false;

    return true;
}

// Check if the reason the request failed was because we timed it out (rather than some harder failure)
bool Steam_HTTP::GetHTTPRequestWasTimedOut(HTTPRequestHandle hRequest, bool* pbWasTimedOut)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    http_request_t* pRequest = get_request(hRequest);
    if (pRequest == nullptr)
        return false;

    return true;
}
