/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_inventory.h"
#include "steam_client.h"
#include "settings.h"

decltype(Steam_Inventory::db_inventory)   Steam_Inventory::db_inventory("db_inventory.json");
decltype(Steam_Inventory::inventory_path) Steam_Inventory::inventory_path("inventory.json");

Steam_Inventory::Steam_Inventory() :
    _db_loaded(false),
    _inventory_loaded(false),
    _prices_requested(false),
    _result_handle(1)
{
    _inventory_db = nlohmann::json::object();
}

Steam_Inventory::~Steam_Inventory()
{
    emu_deinit();
}

void Steam_Inventory::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock(_local_mutex, cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(cb_manager->_local_mutex, std::adopt_lock);

    _cb_manager = cb_manager;
    _network = network;

    _cb_manager->register_callbacks(this);
}

void Steam_Inventory::emu_deinit()
{
    TRACE_FUNC();
    if (_cb_manager != nullptr)
    {
        std::lock(_local_mutex, _cb_manager->_local_mutex);
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
        std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

        _cb_manager->unregister_callbacks(this);
    }

    {
        std::lock_guard<std::recursive_mutex> lk(_local_mutex);
        _network.reset();
        _cb_manager.reset();
    }
}

std::pair<SteamInventoryResult_t, inventory_result_info_t&> Steam_Inventory::gen_inventory_result()
{
    inventory_result_info_t& infos = _inventory_results[_result_handle];
    infos.creation_time = std::chrono::steady_clock::now();
    infos.status = k_EResultOK;

    return std::pair<SteamInventoryResult_t, inventory_result_info_t&>{ _result_handle++, infos };
}

// INVENTORY ASYNC RESULT MANAGEMENT
//
// Asynchronous inventory queries always output a result handle which can be used with
// GetResultStatus, GetResultItems, etc. A SteamInventoryResultReady_t callback will
// be triggered when the asynchronous result becomes ready (or fails).
//

// Find out the status of an asynchronous inventory result handle. Possible values:
//  k_EResultPending - still in progress
//  k_EResultOK - done, result ready
//  k_EResultExpired - done, result ready, maybe out of date (see DeserializeResult)
//  k_EResultInvalidParam - ERROR: invalid API call parameters
//  k_EResultServiceUnavailable - ERROR: service temporarily down, you may retry later
//  k_EResultLimitExceeded - ERROR: operation would exceed per-user inventory limits
//  k_EResultFail - ERROR: unknown / generic error
STEAM_METHOD_DESC(Find out the status of an asynchronous inventory result handle.)
EResult Steam_Inventory::GetResultStatus(SteamInventoryResult_t resultHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto it = _inventory_results.find(resultHandle);
    if (it == _inventory_results.end())
    {
        return k_EResultFail;
    }

    return it->second.status;
}

// Copies the contents of a result set into a flat array. The specific
// contents of the result set depend on which query which was used.
STEAM_METHOD_DESC(Copies the contents of a result set into a flat array.The specific contents of the result set depend on which query which was used.)
bool Steam_Inventory::GetResultItems(SteamInventoryResult_t resultHandle,
    STEAM_OUT_ARRAY_COUNT(punOutItemsArraySize, Output array) SteamItemDetails_t* pOutItemsArray,
    uint32* punOutItemsArraySize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (punOutItemsArraySize == nullptr)
        return false;

    auto it = _inventory_results.find(resultHandle);
    if (it == _inventory_results.end())
    {
        APP_LOG(Log::LogLevel::WARN, "Result not found: %d", resultHandle);
        return false;
    }
    
    if (pOutItemsArray == nullptr)
    {
        *punOutItemsArraySize = (it->second.full_inventory ? _inventory.size() : it->second.items.size());
    }
    else
    {
        if (it->second.full_inventory)
        {
            auto& items = _inventory;
            if (*punOutItemsArraySize > items.size())
                *punOutItemsArraySize = items.size();

            int max_items = *punOutItemsArraySize;
            for (auto item = items.begin(); item != items.end() && max_items; ++item, --max_items)
            {
                SteamItemDef_t itemdefid = item.value()["itemdefid"];
                pOutItemsArray->m_iDefinition = itemdefid;
                pOutItemsArray->m_itemId = _itemdef_to_index[itemdefid];

                try
                {
                    pOutItemsArray->m_unQuantity = item.value().get<uint16>();
                }
                catch (...)
                {
                    APP_LOG(Log::LogLevel::INFO, "Failed to parse inventory item quantity: %d", itemdefid);
                    pOutItemsArray->m_unQuantity = 0;
                }
                pOutItemsArray->m_unFlags = k_ESteamItemNoTrade;
                ++pOutItemsArray;
            }
        }
        else
        {
            auto& items = it->second.items;
            if (*punOutItemsArraySize > items.size())
                *punOutItemsArraySize = items.size();

            int max_items = *punOutItemsArraySize;
            for (auto item = items.begin(); item != items.end() && max_items; ++item, --max_items)
            {
                SteamItemDef_t itemdefid = (**item)["itemdefid"];
                pOutItemsArray->m_iDefinition = itemdefid;
                pOutItemsArray->m_itemId = _itemdef_to_index[itemdefid];

                try
                {
                    pOutItemsArray->m_unQuantity = (*item)->get<uint16>();
                }
                catch (...)
                {
                    APP_LOG(Log::LogLevel::INFO, "Failed to parse inventory item quantity: %d", itemdefid);
                    pOutItemsArray->m_unQuantity = 0;
                }
                pOutItemsArray->m_unFlags = k_ESteamItemNoTrade;
                ++pOutItemsArray;
            }
        }
    }

    return true;
}

// In combination with GetResultItems, you can use GetResultItemProperty to retrieve
// dynamic string properties for a given item returned in the result set.
// 
// Property names are always composed of ASCII letters, numbers, and/or underscores.
//
// Pass a NULL pointer for pchPropertyName to get a comma - separated list of available
// property names.
//
// If pchValueBuffer is NULL, *punValueBufferSize will contain the 
// suggested buffer size. Otherwise it will be the number of bytes actually copied
// to pchValueBuffer. If the results do not fit in the given buffer, partial 
// results may be copied.
bool Steam_Inventory::GetResultItemProperty(SteamInventoryResult_t resultHandle,
    uint32 unItemIndex,
    const char* pchPropertyName,
    STEAM_OUT_STRING_COUNT(punValueBufferSizeOut) char* pchValueBuffer, uint32* punValueBufferSizeOut)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO", resultHandle);

    return false;
}

// Returns the server time at which the result was generated. Compare against
// the value of IClientUtils::GetServerRealTime() to determine age.
STEAM_METHOD_DESC(Returns the server time at which the result was generated.Compare against the value of IClientUtils::GetServerRealTime() to determine age.)
uint32 Steam_Inventory::GetResultTimestamp(SteamInventoryResult_t resultHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto it = _inventory_results.find(resultHandle);
    if (it == _inventory_results.end())
    {
        APP_LOG(Log::LogLevel::WARN, "Result not found: %d", resultHandle);
        return 0;
    }

    return std::chrono::duration_cast<std::chrono::duration<uint32>>(it->second.creation_time.time_since_epoch()).count();
}

// Returns true if the result belongs to the target steam ID, false if the
// result does not. This is important when using DeserializeResult, to verify
// that a remote player is not pretending to have a different user's inventory.
STEAM_METHOD_DESC(Returns true if the result belongs to the target steam ID or false if the result does not.This is important when using DeserializeResult to verify that a remote player is not pretending to have a different users inventory.)
bool Steam_Inventory::CheckResultSteamID(SteamInventoryResult_t resultHandle, CSteamID steamIDExpected)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto it = _inventory_results.find(resultHandle);
    if (it == _inventory_results.end())
    {
        APP_LOG(Log::LogLevel::WARN, "Result not found: %d", resultHandle);
        return false;
    }
    return true;
}

// Destroys a result handle and frees all associated memory.
STEAM_METHOD_DESC(Destroys a result handle and frees all associated memory.)
void Steam_Inventory::DestroyResult(SteamInventoryResult_t resultHandle)
{
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto it = _inventory_results.find(resultHandle);
    if (it != _inventory_results.end())
        _inventory_results.erase(it);
}


// INVENTORY ASYNC QUERY
//

// Captures the entire state of the current user's Steam inventory.
// You must call DestroyResult on this handle when you are done with it.
// Returns false and sets *pResultHandle to zero if inventory is unavailable.
// Note: calls to this function are subject to rate limits and may return
// cached results if called too frequently. It is suggested that you call
// this function only when you are about to display the user's full inventory,
// or if you expect that the inventory may have changed.
STEAM_METHOD_DESC(Captures the entire state of the current users Steam inventory.)
bool Steam_Inventory::GetAllItems(SteamInventoryResult_t * pResultHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    std::pair<SteamInventoryResult_t, inventory_result_info_t&> res = gen_inventory_result();

    if (pResultHandle != nullptr)
        *pResultHandle = res.first;
    
    res.second.full_inventory = true;
    res.second.result_ready = true;

    pFrameResult_t result(new FrameResult);
    result->remove_on_timeout = false;
    SteamInventoryFullUpdate_t& sifu = result->CreateCallback<SteamInventoryFullUpdate_t>();
    sifu.m_handle = res.first;

    _cb_manager->add_callback(this, result);

    return true;
}


// Captures the state of a subset of the current user's Steam inventory,
// identified by an array of item instance IDs. The results from this call
// can be serialized and passed to other players to "prove" that the current
// user owns specific items, without exposing the user's entire inventory.
// For example, you could call GetItemsByID with the IDs of the user's
// currently equipped cosmetic items and serialize this to a buffer, and
// then transmit this buffer to other players upon joining a game.
STEAM_METHOD_DESC(Captures the state of a subset of the current users Steam inventory identified by an array of item instance IDs.)
bool Steam_Inventory::GetItemsByID(SteamInventoryResult_t * pResultHandle, STEAM_ARRAY_COUNT(unCountInstanceIDs) const SteamItemInstanceID_t * pInstanceIDs, uint32 unCountInstanceIDs)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    std::pair<SteamInventoryResult_t, inventory_result_info_t&> res = gen_inventory_result();

    if (pResultHandle != nullptr)
        *pResultHandle = res.first;

    res.second.full_inventory = false;
    res.second.items.reserve(unCountInstanceIDs);
    res.second.result_ready = true;

    while (unCountInstanceIDs--)
    {
        res.second.items.push_back(&_inventory_db[*pInstanceIDs++]);
        ++pInstanceIDs;
    }

    pFrameResult_t result(new FrameResult);
    result->remove_on_timeout = false;
    SteamInventoryResultReady_t& sirr = result->CreateCallback<SteamInventoryResultReady_t>();
    sirr.m_handle = res.first;
    sirr.m_result = k_EResultOK;

    _cb_manager->add_callback(this, result);

    return true;
}


// RESULT SERIALIZATION AND AUTHENTICATION
//
// Serialized result sets contain a short signature which can't be forged
// or replayed across different game sessions. A result set can be serialized
// on the local client, transmitted to other players via your game networking,
// and deserialized by the remote players. This is a secure way of preventing
// hackers from lying about posessing rare/high-value items.

// Serializes a result set with signature bytes to an output buffer. Pass
// NULL as an output buffer to get the required size via punOutBufferSize.
// The size of a serialized result depends on the number items which are being
// serialized. When securely transmitting items to other players, it is
// recommended to use "GetItemsByID" first to create a minimal result set.
// Results have a built-in timestamp which will be considered "expired" after
// an hour has elapsed. See DeserializeResult for expiration handling.

bool Steam_Inventory::SerializeResult(SteamInventoryResult_t resultHandle, STEAM_OUT_BUFFER_COUNT(punOutBufferSize) void* pOutBuffer, uint32 * punOutBufferSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (punOutBufferSize == nullptr)
        return false;

    auto it = _inventory_results.find(resultHandle);
    if (it == _inventory_results.end())
    {
        return false;
    }

    auto& serialized = it->second.serialized_result;

    if (serialized.empty())
    {// If the result hasn't been serialized, serialize it now
        serialized.reserve(1024 * 1024 * 512);
        std::string user_id(std::to_string(uint64_t(Settings::Inst().userid.ConvertToUint64())));

        serialized.resize(user_id.length());
        std::copy(user_id.begin(), user_id.end(), &serialized[0]);

        std::for_each(it->second.items.begin(), it->second.items.end(), [&serialized](nlohmann::json* item)
        {
            try
            {
                auto& item_id = (*item)["itemdefid"].get_ref<std::string&>();
                uint16_t str_len = item_id.length();
                size_t buff_len = serialized.size();

                serialized.resize(buff_len + sizeof(str_len) + str_len);
                *reinterpret_cast<decltype(str_len)*>(&serialized[buff_len]) = utils::Endian::net_swap(str_len);
                std::copy(item_id.begin(), item_id.end(), &serialized[buff_len + sizeof(str_len)]);
            }
            catch (...)
            {
            }
        });
    }

    if (pOutBuffer == nullptr)
    {
        *punOutBufferSize = serialized.size();
        return true;
    }
    
    if (*punOutBufferSize < serialized.size())
    {
        *punOutBufferSize = serialized.size();
        return false;
    }
    
    std::copy(serialized.begin(), serialized.end(), reinterpret_cast<uint8_t*>(pOutBuffer));
    *punOutBufferSize = serialized.size();
    return true;
}

// Deserializes a result set and verifies the signature bytes. Returns false
// if bRequireFullOnlineVerify is set but Steam is running in Offline mode.
// Otherwise returns true and then delivers error codes via GetResultStatus.
//
// The bRESERVED_MUST_BE_FALSE flag is reserved for future use and should not
// be set to true by your game at this time.
//
// DeserializeResult has a potential soft-failure mode where the handle status
// is set to k_EResultExpired. GetResultItems() still succeeds in this mode.
// The "expired" result could indicate that the data may be out of date - not
// just due to timed expiration (one hour), but also because one of the items
// in the result set may have been traded or consumed since the result set was
// generated. You could compare the timestamp from GetResultTimestamp() to
// ISteamUtils::GetServerRealTime() to determine how old the data is. You could
// simply ignore the "expired" result code and continue as normal, or you
// could challenge the player with expired data to send an updated result set.
bool Steam_Inventory::DeserializeResult(SteamInventoryResult_t * pOutResultHandle, STEAM_BUFFER_COUNT(punOutBufferSize) const void* pBuffer, uint32 unBufferSize, bool bRESERVED_MUST_BE_FALSE)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    if (pOutResultHandle != nullptr)
    {
        std::pair<SteamInventoryResult_t, inventory_result_info_t&> res = gen_inventory_result();
        *pOutResultHandle = res.first;

        pFrameResult_t result(new FrameResult);
        result->remove_on_timeout = false;
        SteamInventoryResultReady_t& sirr = result->CreateCallback<SteamInventoryResultReady_t>();
        sirr.m_handle = res.first;
        sirr.m_result = k_EResultOK;

        res.second.items.reserve(5000); // Reserve 5000 items, thats 5000 * sizeof(void*)
        res.second.result_ready = true;
        res.second.full_inventory = false;
        
        auto* pEnd = reinterpret_cast<const uint8_t*>(pBuffer) + unBufferSize;
        uint16_t str_len;
        std::string item_id;
        
        for (auto* it = reinterpret_cast<const uint8_t*>(pBuffer); it < pEnd; )
        {
            if ((pEnd - it) >= sizeof(str_len))
            {
                str_len = utils::Endian::net_swap(*reinterpret_cast<const decltype(str_len)*>(it));
                it += sizeof(str_len);
                if ((pEnd - it) >= str_len)
                {
                    item_id.resize(str_len);
                    std::copy(it, it + str_len, &item_id[0]);
        
                    auto item_it = _inventory_db.find(item_id);
                    if (item_it != _inventory_db.end())
                    {
                        res.second.items.emplace_back(&item_it.value());
                    }
                }
            }
            else
            {
                break;
            }
        }

        _cb_manager->add_callback(this, result);

        return true;
    }

    return false;
}


// INVENTORY ASYNC MODIFICATION
//

// GenerateItems() creates one or more items and then generates a SteamInventoryCallback_t
// notification with a matching nCallbackContext parameter. This API is only intended
// for prototyping - it is only usable by Steam accounts that belong to the publisher group 
// for your game.
// If punArrayQuantity is not NULL, it should be the same length as pArrayItems and should
// describe the quantity of each item to generate.
bool Steam_Inventory::GenerateItems(SteamInventoryResult_t * pResultHandle, STEAM_ARRAY_COUNT(unArrayLength) const SteamItemDef_t * pArrayItemDefs, STEAM_ARRAY_COUNT(unArrayLength) const uint32 * punArrayQuantity, uint32 unArrayLength)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// GrantPromoItems() checks the list of promotional items for which the user may be eligible
// and grants the items (one time only).  On success, the result set will include items which
// were granted, if any. If no items were granted because the user isn't eligible for any
// promotions, this is still considered a success.
STEAM_METHOD_DESC(GrantPromoItems() checks the list of promotional items for which the user may be eligibleand grants the items(one time only).)
bool Steam_Inventory::GrantPromoItems(SteamInventoryResult_t * pResultHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    if (this == GetSteam_Client()._steam_gameserver_inventory.get())
        return false;

    std::pair<SteamInventoryResult_t, inventory_result_info_t&> res = gen_inventory_result();

    if (pResultHandle != nullptr)
        *pResultHandle = res.first;

    res.second.full_inventory = false;
    res.second.result_ready = true;

    pFrameResult_t result(new FrameResult);
    result->remove_on_timeout = false;
    SteamInventoryResultReady_t& sirr = result->CreateCallback<SteamInventoryResultReady_t>();
    sirr.m_handle = res.first;
    sirr.m_result = k_EResultOK;

    _cb_manager->add_next_callback(this, result);

    return true;
}

// AddPromoItem() / AddPromoItems() are restricted versions of GrantPromoItems(). Instead of
// scanning for all eligible promotional items, the check is restricted to a single item
// definition or set of item definitions. This can be useful if your game has custom UI for
// showing a specific promo item to the user.
bool Steam_Inventory::AddPromoItem(SteamInventoryResult_t * pResultHandle, SteamItemDef_t itemDef)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    return AddPromoItems(pResultHandle, &itemDef, 1);
}

bool Steam_Inventory::AddPromoItems(SteamInventoryResult_t * pResultHandle, STEAM_ARRAY_COUNT(unArrayLength) const SteamItemDef_t * pArrayItemDefs, uint32 unArrayLength)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    std::pair<SteamInventoryResult_t, inventory_result_info_t&> res = gen_inventory_result();

    if (pResultHandle != nullptr)
        *pResultHandle = res.first;

    res.second.full_inventory = false;
    res.second.result_ready = true;

    pFrameResult_t result(new FrameResult);
    result->remove_on_timeout = false;
    SteamInventoryResultReady_t& sirr = result->CreateCallback<SteamInventoryResultReady_t>();
    sirr.m_handle = res.first;
    sirr.m_result = k_EResultOK;

    _cb_manager->add_next_callback(this, result);

    return true;
}

// ConsumeItem() removes items from the inventory, permanently. They cannot be recovered.
// Not for the faint of heart - if your game implements item removal at all, a high-friction
// UI confirmation process is highly recommended.
STEAM_METHOD_DESC(ConsumeItem() removes items from the inventory permanently.)
bool Steam_Inventory::ConsumeItem(SteamInventoryResult_t * pResultHandle, SteamItemInstanceID_t itemConsume, uint32 unQuantity)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// ExchangeItems() is an atomic combination of item generation and consumption. 
// It can be used to implement crafting recipes or transmutations, or items which unpack 
// themselves into other items (e.g., a chest). 
// Exchange recipes are defined in the ItemDef, and explicitly list the required item 
// types and resulting generated type. 
// Exchange recipes are evaluated atomically by the Inventory Service; if the supplied
// components do not match the recipe, or do not contain sufficient quantity, the 
// exchange will fail.
bool Steam_Inventory::ExchangeItems(SteamInventoryResult_t * pResultHandle,
    STEAM_ARRAY_COUNT(unArrayGenerateLength) const SteamItemDef_t * pArrayGenerate, STEAM_ARRAY_COUNT(unArrayGenerateLength) const uint32 * punArrayGenerateQuantity, uint32 unArrayGenerateLength,
    STEAM_ARRAY_COUNT(unArrayDestroyLength) const SteamItemInstanceID_t * pArrayDestroy, STEAM_ARRAY_COUNT(unArrayDestroyLength) const uint32 * punArrayDestroyQuantity, uint32 unArrayDestroyLength)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    return false;
}


// TransferItemQuantity() is intended for use with items which are "stackable" (can have
// quantity greater than one). It can be used to split a stack into two, or to transfer
// quantity from one stack into another stack of identical items. To split one stack into
// two, pass k_SteamItemInstanceIDInvalid for itemIdDest and a new item will be generated.
bool Steam_Inventory::TransferItemQuantity(SteamInventoryResult_t * pResultHandle, SteamItemInstanceID_t itemIdSource, uint32 unQuantity, SteamItemInstanceID_t itemIdDest)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    return false;
}


// TIMED DROPS AND PLAYTIME CREDIT
//

// Deprecated. Calling this method is not required for proper playtime accounting.
STEAM_METHOD_DESC(Deprecated method.Playtime accounting is performed on the Steam servers.)
void Steam_Inventory::SendItemDropHeartbeat()
{
    //LOG(Log::LogLevel::TRACE, "");
}

// Playtime credit must be consumed and turned into item drops by your game. Only item
// definitions which are marked as "playtime item generators" can be spawned. The call
// will return an empty result set if there is not enough playtime credit for a drop.
// Your game should call TriggerItemDrop at an appropriate time for the user to receive
// new items, such as between rounds or while the player is dead. Note that players who
// hack their clients could modify the value of "dropListDefinition", so do not use it
// to directly control rarity.
// See your Steamworks configuration to set playtime drop rates for individual itemdefs.
// The client library will suppress too-frequent calls to this method.
STEAM_METHOD_DESC(Playtime credit must be consumed and turned into item drops by your game.)
bool Steam_Inventory::TriggerItemDrop(SteamInventoryResult_t * pResultHandle, SteamItemDef_t dropListDefinition)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    if (this == GetSteam_Client()._steam_gameserver_inventory.get())
        return false;

    std::pair<SteamInventoryResult_t, inventory_result_info_t&> res = gen_inventory_result();

    if (pResultHandle != nullptr)
        *pResultHandle = res.first;

    res.second.full_inventory = false;
    res.second.result_ready = true;

    pFrameResult_t result(new FrameResult);
    result->remove_on_timeout = false;
    SteamInventoryResultReady_t& sirr = result->CreateCallback<SteamInventoryResultReady_t>();
    sirr.m_handle = res.first;
    sirr.m_result = k_EResultOK;

    _cb_manager->add_next_callback(this, result);

    return true;
}


// Deprecated. This method is not supported.
bool Steam_Inventory::TradeItems(SteamInventoryResult_t * pResultHandle, CSteamID steamIDTradePartner,
    STEAM_ARRAY_COUNT(nArrayGiveLength) const SteamItemInstanceID_t * pArrayGive, STEAM_ARRAY_COUNT(nArrayGiveLength) const uint32 * pArrayGiveQuantity, uint32 nArrayGiveLength,
    STEAM_ARRAY_COUNT(nArrayGetLength) const SteamItemInstanceID_t * pArrayGet, STEAM_ARRAY_COUNT(nArrayGetLength) const uint32 * pArrayGetQuantity, uint32 nArrayGetLength)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    return false;
}


// ITEM DEFINITIONS
//
// Item definitions are a mapping of "definition IDs" (integers between 1 and 1000000)
// to a set of string properties. Some of these properties are required to display items
// on the Steam community web site. Other properties can be defined by applications.
// Use of these functions is optional; there is no reason to call LoadItemDefinitions
// if your game hardcodes the numeric definition IDs (eg, purple face mask = 20, blue
// weapon mod = 55) and does not allow for adding new item types without a client patch.
//

// LoadItemDefinitions triggers the automatic load and refresh of item definitions.
// Every time new item definitions are available (eg, from the dynamic addition of new
// item types while players are still in-game), a SteamInventoryDefinitionUpdate_t
// callback will be fired.
STEAM_METHOD_DESC(LoadItemDefinitions triggers the automatic load and refresh of item definitions.)
bool Steam_Inventory::LoadItemDefinitions()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    pFrameResult_t result(new FrameResult);
    result->remove_on_timeout = false;
    SteamInventoryDefinitionUpdate_t& sidu = result->CreateCallback<SteamInventoryDefinitionUpdate_t>();

    _cb_manager->add_callback(this, result);

    return true;
}

// GetItemDefinitionIDs returns the set of all defined item definition IDs (which are
// defined via Steamworks configuration, and not necessarily contiguous integers).
// If pItemDefIDs is null, the call will return true and *punItemDefIDsArraySize will
// contain the total size necessary for a subsequent call. Otherwise, the call will
// return false if and only if there is not enough space in the output array.
bool Steam_Inventory::GetItemDefinitionIDs(
    STEAM_OUT_ARRAY_COUNT(punItemDefIDsArraySize, List of item definition IDs) SteamItemDef_t * pItemDefIDs,
    STEAM_DESC(Size of array is passed in and actual size used is returned in this param) uint32 * punItemDefIDsArraySize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (punItemDefIDsArraySize == nullptr)
        return false;

    if (pItemDefIDs == nullptr)
    {
        *punItemDefIDsArraySize = _inventory_db.size();
        return true;
    }

    if (*punItemDefIDsArraySize < _inventory_db.size())
        return false;

    for (auto item = _inventory_db.begin(); item != _inventory_db.end(); ++item)
        *pItemDefIDs++ = item.value()["itemdefid"].get<uint32_t>();

    return true;
}

// GetItemDefinitionProperty returns a string property from a given item definition.
// Note that some properties (for example, "name") may be localized and will depend
// on the current Steam language settings (see ISteamApps::GetCurrentGameLanguage).
// Property names are always composed of ASCII letters, numbers, and/or underscores.
// Pass a NULL pointer for pchPropertyName to get a comma - separated list of available
// property names. If pchValueBuffer is NULL, *punValueBufferSize will contain the 
// suggested buffer size. Otherwise it will be the number of bytes actually copied
// to pchValueBuffer. If the results do not fit in the given buffer, partial 
// results may be copied.
bool Steam_Inventory::GetItemDefinitionProperty(SteamItemDef_t iDefinition, const char* pchPropertyName,
    STEAM_OUT_STRING_COUNT(punValueBufferSizeOut) char* pchValueBuffer, uint32 * punValueBufferSizeOut)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (punValueBufferSizeOut == nullptr)
        return false;

    auto it = _itemdef_to_index.find(iDefinition);
    if (it == _itemdef_to_index.end())
        return false;

    nlohmann::json& item = _inventory_db[it->second];

    // Pass a NULL pointer for pchPropertyName to get a comma - separated list of available property names.
    if (pchPropertyName == nullptr)
    {
        if (pchValueBuffer == nullptr)
        {
            *punValueBufferSizeOut = 0;
            for (auto i = item.begin(); i != item.end(); ++i)
                *punValueBufferSizeOut += i.key().length() + 1; // Size of key + comma, and the last is not a comma but null char
        }
        else
        {
            // strncat always add the null terminator, so remove 1 to the string length
            uint32_t len = *punValueBufferSizeOut - 1;
            *punValueBufferSizeOut = 0;
            memset(pchValueBuffer, 0, len);
            for (auto i = item.begin(); i != item.end() && len > 0; ++i)
            {
                strncat(pchValueBuffer, i.key().c_str(), len);
                // Count how many chars we copied
                // Either the string length or the buffer size if its too small
                uint32 x = std::min(len, static_cast<uint32>(i.key().length()));
                *punValueBufferSizeOut += x;
                len -= x;

                if (len && std::distance(i, item.end()) != 1) // If this is not the last item, add a comma
                    strncat(pchValueBuffer, ",", len--);

                // Always add 1, its a comma or the null terminator
                ++*punValueBufferSizeOut;
            }
        }
    }
    else
    {
        try
        {
            std::string const& val = item[pchPropertyName].get_ref<std::string const&>();
            if (pchValueBuffer == nullptr)
            {
                *punValueBufferSizeOut = val.length() + 1;
            }
            else
            {
                strncpy(pchValueBuffer, val.c_str(), *punValueBufferSizeOut - 1);
                *punValueBufferSizeOut = std::min(static_cast<uint32>(val.length() + 1), *punValueBufferSizeOut);
            }
        }
        catch(...)
        {
            APP_LOG(Log::LogLevel::ERR, "Failed to parse item(%d)[%s] as a string", it->second, pchPropertyName);
            return false;
        }
    }

    return true;
}

// Request the list of "eligible" promo items that can be manually granted to the given
// user.  These are promo items of type "manual" that won't be granted automatically.
// An example usage of this is an item that becomes available every week.
STEAM_CALL_RESULT(SteamInventoryEligiblePromoItemDefIDs_t)
SteamAPICall_t Steam_Inventory::RequestEligiblePromoItemDefinitionsIDs(CSteamID steamID)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    return 0;
}

// After handling a SteamInventoryEligiblePromoItemDefIDs_t call result, use this
// function to pull out the list of item definition ids that the user can be
// manually granted via the AddPromoItems() call.
bool Steam_Inventory::GetEligiblePromoItemDefinitionIDs(CSteamID steamID, STEAM_OUT_ARRAY_COUNT(punItemDefIDsArraySize, List of item definition IDs) SteamItemDef_t * pItemDefIDs,
    STEAM_DESC(Size of array is passed in and actual size used is returned in this param) uint32 * punItemDefIDsArraySize)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    return false;
}

// Starts the purchase process for the given item definitions.  The callback SteamInventoryStartPurchaseResult_t
// will be posted if Steam was able to initialize the transaction.
// 
// Once the purchase has been authorized and completed by the user, the callback SteamInventoryResultReady_t 
// will be posted.
STEAM_CALL_RESULT(SteamInventoryStartPurchaseResult_t)
SteamAPICall_t Steam_Inventory::StartPurchase(STEAM_ARRAY_COUNT(unArrayLength) const SteamItemDef_t * pArrayItemDefs, STEAM_ARRAY_COUNT(unArrayLength) const uint32 * punArrayQuantity, uint32 unArrayLength)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    pFrameResult_t result(new FrameResult);
    SteamInventoryStartPurchaseResult_t& sispr = result->CreateCallback<SteamInventoryStartPurchaseResult_t>();

    sispr.m_result = k_EResultOK;
    sispr.m_ulOrderID = 0;
    sispr.m_ulTransID = 0;

    _cb_manager->add_apicall(this, result);

    // Create a callback to SteamInventoryResultReady_t to add the new item

    return result->GetAPICall();
}

// Request current prices for all applicable item definitions
STEAM_CALL_RESULT(SteamInventoryRequestPricesResult_t)
SteamAPICall_t Steam_Inventory::RequestPrices()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    pFrameResult_t result(new FrameResult);
    SteamInventoryRequestPricesResult_t& sirpr = result->CreateCallback<SteamInventoryRequestPricesResult_t>();
    sirpr.m_result = k_EResultOK;
    sirpr.m_rgchCurrency[0] = 'E';
    sirpr.m_rgchCurrency[1] = 'U';
    sirpr.m_rgchCurrency[2] = 'R';
    sirpr.m_rgchCurrency[3] = 0;

    result->done = true;
    _cb_manager->add_apicall(this, result);

    _prices_requested = true;

    return result->GetAPICall();
}

// Returns the number of items with prices.  Need to call RequestPrices() first.
uint32 Steam_Inventory::GetNumItemsWithPrices()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (!_prices_requested)
        return 0;

    return 0;
}

// Returns item definition ids and their prices in the user's local currency.
// Need to call RequestPrices() first.
bool Steam_Inventory::GetItemsWithPrices(STEAM_ARRAY_COUNT(unArrayLength) STEAM_OUT_ARRAY_COUNT(pArrayItemDefs, Items with prices) SteamItemDef_t* pArrayItemDefs,
    STEAM_ARRAY_COUNT(unArrayLength) STEAM_OUT_ARRAY_COUNT(pPrices, List of prices for the given item defs) uint64* pPrices,
    uint32 unArrayLength)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return GetItemsWithPrices(pArrayItemDefs, pPrices, nullptr, unArrayLength);
}

bool Steam_Inventory::GetItemsWithPrices(STEAM_ARRAY_COUNT(unArrayLength) STEAM_OUT_ARRAY_COUNT(pArrayItemDefs, Items with prices) SteamItemDef_t * pArrayItemDefs,
    STEAM_ARRAY_COUNT(unArrayLength) STEAM_OUT_ARRAY_COUNT(pPrices, List of prices for the given item defs) uint64* pCurrentPrices,
    STEAM_ARRAY_COUNT(unArrayLength) STEAM_OUT_ARRAY_COUNT(pPrices, List of prices for the given item defs) uint64* pBasePrices,
    uint32 unArrayLength)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (!_prices_requested)
        return false;

    return false;
}

// Retrieves the price for the item definition id
// Returns false if there is no price stored for the item definition.
bool Steam_Inventory::GetItemPrice(SteamItemDef_t iDefinition, uint64* pPrice)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return GetItemPrice(iDefinition, pPrice, nullptr);
}

bool Steam_Inventory::GetItemPrice(SteamItemDef_t iDefinition, uint64 * pCurrentPrice, uint64 * pBasePrice)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (pCurrentPrice != nullptr)
        *pCurrentPrice = 0;

    if (pBasePrice != nullptr)
        *pBasePrice = 100; // 1$ ?

    return true;
}

// Create a request to update properties on items
SteamInventoryUpdateHandle_t Steam_Inventory::StartUpdateProperties()
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    return k_SteamInventoryUpdateHandleInvalid;
}
// Remove the property on the item
bool Steam_Inventory::RemoveProperty(SteamInventoryUpdateHandle_t handle, SteamItemInstanceID_t nItemID, const char* pchPropertyName)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    return false;
}
// Accessor methods to set properties on items
bool Steam_Inventory::SetProperty(SteamInventoryUpdateHandle_t handle, SteamItemInstanceID_t nItemID, const char* pchPropertyName, const char* pchPropertyValue)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    return false;
}
bool Steam_Inventory::SetProperty(SteamInventoryUpdateHandle_t handle, SteamItemInstanceID_t nItemID, const char* pchPropertyName, bool bValue)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    return false;
}
bool Steam_Inventory::SetProperty(SteamInventoryUpdateHandle_t handle, SteamItemInstanceID_t nItemID, const char* pchPropertyName, int64 nValue)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    return false;
}
bool Steam_Inventory::SetProperty(SteamInventoryUpdateHandle_t handle, SteamItemInstanceID_t nItemID, const char* pchPropertyName, float flValue)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    return false;
}
// Submit the update request by handle
bool Steam_Inventory::SubmitUpdateProperties(SteamInventoryUpdateHandle_t handle, SteamInventoryResult_t * pResultHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    return false;
}

STEAM_METHOD_DESC(Look up the given tokenand return a pseudo - Inventory item.)
bool Steam_Inventory::InspectItem(SteamInventoryResult_t * pResultHandle, const char* pchItemToken)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    return false;
}

///////////////////////////////////////////////////////////////////////////////
//                           Network Send messages                           //
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//                          Network Receive messages                         //
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//                                 IRunCallback                                 //
///////////////////////////////////////////////////////////////////////////////
bool Steam_Inventory::CBRunFrame()
{
    return false;
}

bool Steam_Inventory::RunNetwork(Network_Message_pb const& msg)
{
    return false;
}

bool Steam_Inventory::RunCallbacks(pFrameResult_t res)
{
    switch(res->ICallback())
    {
        case SteamInventoryDefinitionUpdate_t::k_iCallback:
        {
            std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

            APP_LOG(Log::LogLevel::INFO, "Item DB empty, parsing now...");
            if (_inventory_db.empty())
            {
                if (FileManager::load_json(db_inventory, _inventory_db))
                {
                    for (size_t i = 0; i < _inventory_db.size(); ++i)
                    {
                        nlohmann::json::value_type& item = _inventory_db[i];
                        for (auto attr = item.begin(); attr != item.end(); ++attr)
                        {
                            nlohmann::json& val = attr.value();
                            if (attr.key() != "itemdefid")
                            {
                                // Transform all items value to string so we don't have to do it in GetItemDefinitionProperty
                                // But don't do it in the json, cause like this we can just copy the steam dump
                                switch (attr.value().type())
                                {
                                    case nlohmann::json::value_t::boolean:
                                        val = (val.get<bool>() ? "true" : "false");
                                        break;

                                    case nlohmann::json::value_t::number_integer:
                                        val = std::to_string(val.get<uint32_t>());
                                        break;

                                    case nlohmann::json::value_t::number_float:
                                        val = std::to_string(val.get<float>());
                                        break;
                                }
                            }
                            else
                            {
                                try
                                {
                                    // itemdefid is an int, so transform it into an int
                                    val = std::stoi(val.get_ref<std::string const&>());
                                    // Create a map between the item index and the itemdefid
                                    _itemdef_to_index[val] = i;
                                }
                                catch (...)
                                {
                                    try
                                    {
                                        APP_LOG(Log::LogLevel::ERR, "Error while parsing item DB, item %s", val.get_ref<std::string const&>().c_str());
                                    }
                                    catch (...)
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
                _db_loaded = true;

                {
                    std::pair<SteamInventoryResult_t, inventory_result_info_t&> res = gen_inventory_result();

                    res.second.result_ready = true;
                    res.second.full_inventory = false;

                    pFrameResult_t result(new FrameResult);
                    result->remove_on_timeout = false;
                    SteamInventoryResultReady_t& sirr = result->CreateCallback<SteamInventoryResultReady_t>();
                    sirr.m_handle = res.first;
                    sirr.m_result = k_EResultOK;

                    _cb_manager->add_next_callback(this, result);
                }
            }
            res->done = true;
        }
        break;

        case SteamInventoryResultReady_t::k_iCallback:
        {
            std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

            SteamInventoryResultReady_t& sirr = res->GetCallback<SteamInventoryResultReady_t>();

            if (_db_loaded)
            {
                // Do some async work if needed
                auto it = _inventory_results.find(sirr.m_handle);
                if (it != _inventory_results.end())
                {
                    if (it->second.result_ready)
                    {
                        res->done = true;
                    }
                    else
                    {

                    }
                }
                else
                {// We didn't find the result but we ran a callback ?
                    res->done = true;
                }
            }
        }
        break;

        case SteamInventoryFullUpdate_t::k_iCallback:
        {
            std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

            SteamInventoryFullUpdate_t& sifu = res->GetCallback<SteamInventoryFullUpdate_t>();

            if (_db_loaded)
            {
                if (!_inventory_loaded)
                {
                    _inventory = nlohmann::json::object();
                    if (!FileManager::load_json(inventory_path, _inventory))
                    {
                        std::ofstream inv_file(FileManager::canonical_path(inventory_path), std::ios::out | std::ios::trunc);
                        if (inv_file)
                        {
                            inv_file << std::setw(2) << _inventory;
                        }
                    }
                }
                pFrameResult_t result(new FrameResult);
                result->remove_on_timeout = false;
                SteamInventoryResultReady_t& sirr = result->CreateCallback<SteamInventoryResultReady_t>();

                sirr.m_handle = sifu.m_handle;

                _cb_manager->add_next_apicall(this, result);

                res->done = true;
            }
        }
        break;
    }
    return res->done;
}
