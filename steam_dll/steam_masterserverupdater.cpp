/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_masterserverupdater.h"

Steam_MasterServerUpdater::Steam_MasterServerUpdater()
{
}

Steam_MasterServerUpdater::~Steam_MasterServerUpdater()
{
    emu_deinit();
}

void Steam_MasterServerUpdater::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _cb_manager = cb_manager;
    _network = network;
}

void Steam_MasterServerUpdater::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _network.reset();
    _cb_manager.reset();
}

// Call this as often as you like to tell the master server updater whether or not
// you want it to be active (default: off).
void Steam_MasterServerUpdater::SetActive(bool bActive)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

// You usually don't need to modify this.
// Pass -1 to use the default value for iHeartbeatInterval.
// Some mods change this.
void Steam_MasterServerUpdater::SetHeartbeatInterval(int iHeartbeatInterval)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
}


// These are in GameSocketShare mode, where instead of ISteamMasterServerUpdater creating its own
// socket to talk to the master server on, it lets the game use its socket to forward messages
// back and forth. This prevents us from requiring server ops to open up yet another port
// in their firewalls.
//
// the IP address and port should be in host order, i.e 127.0.0.1 == 0x7f000001

// These are used when you've elected to multiplex the game server's UDP socket
// rather than having the master server updater use its own sockets.
// 
// Source games use this to simplify the job of the server admins, so they 
// don't have to open up more ports on their firewalls.

// Call this when a packet that starts with 0xFFFFFFFF comes in. That means
// it's for us.
bool Steam_MasterServerUpdater::HandleIncomingPacket(const void* pData, int cbData, uint32 srcIP, uint16 srcPort)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

// AFTER calling HandleIncomingPacket for any packets that came in that frame, call this.
// This gets a packet that the master server updater needs to send out on UDP.
// It returns the length of the packet it wants to send, or 0 if there are no more packets to send.
// Call this each frame until it returns 0.
int Steam_MasterServerUpdater::GetNextOutgoingPacket(void* pOut, int cbMaxOut, uint32* pNetAdr, uint16* pPort)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return 0;
}


// Functions to set various fields that are used to respond to queries.

    // Call this to set basic data that is passed to the server browser.
void Steam_MasterServerUpdater::SetBasicServerData(unsigned short nProtocolVersion, bool bDedicatedServer, const char* pRegionName, const char* pProductName,
    unsigned short nMaxReportedClients, bool bPasswordProtected, const char* pGameDescription)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

// Call this to clear the whole list of key/values that are sent in rules queries.
void Steam_MasterServerUpdater::ClearAllKeyValues()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

// Call this to add/update a key/value pair.
void Steam_MasterServerUpdater::SetKeyValue(const char* pKey, const char* pValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
}


// You can call this upon shutdown to clear out data stored for this game server and
// to tell the master servers that this server is going away.
void Steam_MasterServerUpdater::NotifyShutdown()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

// Returns true if the master server has requested a restart.
// Only returns true once per request.
bool Steam_MasterServerUpdater::WasRestartRequested()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

// Force it to request a heartbeat from the master servers.
void Steam_MasterServerUpdater::ForceHeartbeat()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

// Manually edit and query the master server list.
// It will provide name resolution and use the default master server port if none is provided.
bool Steam_MasterServerUpdater::AddMasterServer(const char* pServerAddress)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

bool Steam_MasterServerUpdater::RemoveMasterServer(const char* pServerAddress)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

int Steam_MasterServerUpdater::GetNumMasterServers()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return 0;
}

// Returns the # of bytes written to pOut.
int Steam_MasterServerUpdater::GetMasterServerAddress(int iServer, char* pOut, int outBufferSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return 0;
}