/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_matchmaking.h"
#include "steam_client.h"
#include "settings.h"

constexpr decltype(Steam_Matchmaking::lobby_join_timeout)    Steam_Matchmaking::lobby_join_timeout;
constexpr decltype(Steam_Matchmaking::lobby_list_timeout)    Steam_Matchmaking::lobby_list_timeout;
constexpr decltype(Steam_Matchmaking::lobby_request_timeout) Steam_Matchmaking::lobby_request_timeout;

Steam_Matchmaking::Steam_Matchmaking()
{
}

Steam_Matchmaking::~Steam_Matchmaking()
{
    emu_deinit();
}

void Steam_Matchmaking::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock(_local_mutex, cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(cb_manager->_local_mutex, std::adopt_lock);

    _cb_manager = cb_manager;
    _network = network;

    _cb_manager->register_callbacks(this);
    _network->register_listener(this, Network_Message_pb::MessagesCase::kMatchmaking);
}

void Steam_Matchmaking::emu_deinit()
{
    TRACE_FUNC();
    if (_cb_manager != nullptr)
    {
        std::lock(_local_mutex, _cb_manager->_local_mutex);
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
        std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

        _cb_manager->unregister_callbacks(this);
    }

    if (_network != nullptr)
    {
        _network->unregister_listener(this, Network_Message_pb::MessagesCase::kNetwork);
    }

    {
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
        _network.reset();
        _cb_manager.reset();
    }
}

template<typename T>
bool compare_attribute_values(T&& v1, ELobbyComparison op, T&& v2)
{
    try
    {
        switch (op)
        {
            case ELobbyComparison::k_ELobbyComparisonEqual               : return v1 == v2;
            case ELobbyComparison::k_ELobbyComparisonNotEqual            : return v1 != v2;
            case ELobbyComparison::k_ELobbyComparisonGreaterThan         : return v1 > v2;
            case ELobbyComparison::k_ELobbyComparisonEqualToOrGreaterThan: return v1 >= v2;
            case ELobbyComparison::k_ELobbyComparisonLessThan            : return v1 < v2;
            case ELobbyComparison::k_ELobbyComparisonEqualToOrLessThan   : return v1 <= v2;
        }
    }
    catch (...)
    {
        return false;
    }

    // Default return true
    return true;
}

void Steam_Matchmaking::trigger_lobby_update(uint64 member_id, uint64 lobby_id, bool success)
{
    pFrameResult_t result(new FrameResult);
    LobbyDataUpdate_t& ldu = result->CreateCallback<LobbyDataUpdate_t>();

    ldu.m_bSuccess = success;
    ldu.m_ulSteamIDLobby = lobby_id;
    ldu.m_ulSteamIDMember = member_id;

    result->done = true;
    _cb_manager->add_callback(this, result);
}

void Steam_Matchmaking::trigger_chat_member(uint64 member_id, uint64 lobby_id, EChatMemberStateChange v)
{
    pFrameResult_t res(new FrameResult);
    LobbyChatUpdate_t& lcu = res->CreateCallback<LobbyChatUpdate_t>();

    lcu.m_ulSteamIDLobby = lobby_id;
    lcu.m_ulSteamIDMakingChange = member_id;
    lcu.m_ulSteamIDUserChanged = member_id;
    lcu.m_rgfChatMemberStateChange = v;

    res->done = true;
    _cb_manager->add_callback(this, res);
}

std::pair<uint32_t, uint32_t> Steam_Matchmaking::get_invisible_visible_lobby_count()
{
    std::pair<uint32_t, uint32_t> count{};
    for (auto& lobby : _joined_lobbies)
    {
        switch (lobby.second->infos.lobby_type())
        {
            case k_ELobbyTypePrivate      :
            case k_ELobbyTypeFriendsOnly  :
            case k_ELobbyTypePublic       :
            case k_ELobbyTypePrivateUnique:
                ++count.second;
                break;

            case k_ELobbyTypeInvisible    :
                ++count.first;
        }
    }

    return count;
}

bool Steam_Matchmaking::i_am_owner(lobby_state_t* lobby)
{
    assert(lobby != nullptr);
    return lobby->infos.owner() == Settings::Inst().userid.ConvertToUint64();
}

bool Steam_Matchmaking::i_am_in_lobby(lobby_state_t* lobby)
{
    return is_lobby_member(Settings::Inst().userid.ConvertToUint64(), lobby);
}

bool Steam_Matchmaking::is_lobby_member(uint64 steam_id, lobby_state_t* lobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    assert(lobby != nullptr);
    return lobby->infos.members().find(steam_id) != lobby->infos.members().end();
}

bool Steam_Matchmaking::add_lobby_member(uint64 steam_id, lobby_state_t* lobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    assert(lobby != nullptr);

    if (is_lobby_member(steam_id, lobby))
        return false;

    (*lobby->infos.mutable_members())[steam_id];
    return true;
}

bool Steam_Matchmaking::remove_lobby_member(uint64 steam_id, lobby_state_t* lobby)
{
    assert(lobby != nullptr);

    auto& members = *lobby->infos.mutable_members();
    auto member_it = members.find(steam_id);
    if (member_it != members.end())
    {
        members.erase(member_it);
        return true;
    }

    return false;
}

lobby_state_t* Steam_Matchmaking::get_lobby_by_id(uint64 lobby_id)
{
    auto it = _lobbies.find(lobby_id);
    if (it == _lobbies.end())
        return nullptr;

    return &it->second;
}

google::protobuf::Map<std::string, std::string>::iterator Steam_Matchmaking::find_lobbydata(google::protobuf::Map<std::string, std::string>& container, std::string key)
{
    key = to_lower(key);
    return std::find_if(container.begin(), container.end(), [&key]( google::protobuf::MapPair<std::string, std::string>& lobbydata)
    {
        return to_lower(lobbydata.first) == key;
    });
}

std::vector<lobby_state_t*> Steam_Matchmaking::get_lobbies_from_filter(Lobby_Search_pb const& search)
{
    std::vector<lobby_state_t*> lobbies;
    for (auto& lobby : _joined_lobbies)
    {
        auto& infos = lobby.second->infos;
        if (i_am_owner(lobby.second) && infos.joinable())
        {
            switch (infos.lobby_type())
            {
                case k_ELobbyTypeFriendsOnly:
                case k_ELobbyTypePublic:
                case k_ELobbyTypeInvisible:
                {
                    if (search.slots_available() <= 0 || (infos.max_members() - infos.members_size()) >= search.slots_available())
                    {
                        lobbies.emplace_back(lobby.second);
                    }
                }
            }
        }
    }

    return lobbies;
}


// game server favorites storage
// saves basic details about a multiplayer game server locally

// returns the number of favorites servers the user has stored
int Steam_Matchmaking::GetFavoriteGameCount()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    return 0;
}

// returns the details of the game server
// iGame is of range [0,GetFavoriteGameCount())
// *pnIP, *pnConnPort are filled in the with IP:port of the game server
// *punFlags specify whether the game server was stored as an explicit favorite or in the history of connections
// *pRTime32LastPlayedOnServer is filled in the with the Unix time the favorite was added
bool Steam_Matchmaking::GetFavoriteGame(int iGame, AppId_t* pnAppID, uint32* pnIP, uint16* pnConnPort, uint16* pnQueryPort, uint32* punFlags, uint32* pRTime32LastPlayedOnServer)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    return false;
}

// adds the game server to the local list; updates the time played of the server if it already exists in the list
int Steam_Matchmaking::AddFavoriteGame(AppId_t nAppID, uint32 nIP, uint16 nConnPort, uint16 nQueryPort, uint32 unFlags, uint32 rTime32LastPlayedOnServer)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    return 0;
}

// removes the game server from the local storage; returns true if one was removed
bool Steam_Matchmaking::RemoveFavoriteGame(AppId_t nAppID, uint32 nIP, uint16 nConnPort, uint16 nQueryPort, uint32 unFlags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    return false;
}

///////
// Game lobby functions

// Get a list of relevant lobbies
// this is an asynchronous request
// results will be returned by LobbyMatchList_t callback & call result, with the number of lobbies found
// this will never return lobbies that are full
// to add more filter, the filter calls below need to be call before each and every RequestLobbyList() call
// use the CCallResult<> object in steam_api.h to match the SteamAPICall_t call result to a function in an object, e.g.
/*
    class CMyLobbyListManager
    {
        CCallResult<CMyLobbyListManager, LobbyMatchList_t> m_CallResultLobbyMatchList;
        void FindLobbies()
        {
            // SteamMatchmaking()->AddRequestLobbyListFilter*() functions would be called here, before RequestLobbyList()
            SteamAPICall_t hSteamAPICall = SteamMatchmaking()->RequestLobbyList();
            m_CallResultLobbyMatchList.Set( hSteamAPICall, this, &CMyLobbyListManager::OnLobbyMatchList );
        }

        void OnLobbyMatchList( LobbyMatchList_t *pLobbyMatchList, bool bIOFailure )
        {
            // lobby list has be retrieved from Steam back-end, use results
        }
    }
*/
// 
STEAM_CALL_RESULT(LobbyMatchList_t)
SteamAPICall_t Steam_Matchmaking::RequestLobbyList()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    if (_search_state.callback.get())
    {
        LobbyMatchList_t& lml = _search_state.callback->GetCallback<LobbyMatchList_t>();
        lml.m_nLobbiesMatching = 0;

        _search_state.callback->done = true;
    }
    
    _search_state.callback.reset(new FrameResult);
    // Steam says 300ms min
    LobbyMatchList_t& lml = _search_state.callback->CreateCallback<LobbyMatchList_t>(std::chrono::milliseconds(300));
    lml.m_nLobbiesMatching = 0;
    
    _cb_manager->add_next_apicall(this, _search_state.callback);

    send_lobby_search(&_search_state.search_pb);

    _search_state.lobbies.clear();
    _search_state.search_pb = Lobby_Search_pb();
    _search_state.max_results = -1;
    
    return _search_state.callback->GetAPICall();
}

bool Steam_Matchmaking::RequestFriendsLobbies()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");

    return false;
}

// filters for lobbies
// this needs to be called before RequestLobbyList() to take effect
// these are cleared on each call to RequestLobbyList()
void Steam_Matchmaking::AddRequestLobbyListFilter(const char* pchKeyToMatch, const char* pchValueToMatch)
{
    AddRequestLobbyListStringFilter(pchKeyToMatch, pchValueToMatch, k_ELobbyComparisonEqual);
}

void Steam_Matchmaking::AddRequestLobbyListStringFilter(const char* pchKeyToMatch, const char* pchValueToMatch, ELobbyComparison eComparisonType)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (pchKeyToMatch == nullptr || *pchKeyToMatch == '\0' || pchValueToMatch == nullptr)
        return;

    auto& filters = *_search_state.search_pb.mutable_filters();
    (*filters[pchKeyToMatch].mutable_filter())[eComparisonType].set_s(pchValueToMatch);
}

// numerical comparison
void Steam_Matchmaking::AddRequestLobbyListNumericalFilter(const char* pchKeyToMatch, int nValueToMatch, int nComparisonType)
{
    AddRequestLobbyListNumericalFilter(pchKeyToMatch, nValueToMatch, (ELobbyComparison)nComparisonType);
}

void Steam_Matchmaking::AddRequestLobbyListNumericalFilter(const char* pchKeyToMatch, int nValueToMatch, ELobbyComparison eComparisonType)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (pchKeyToMatch == nullptr || *pchKeyToMatch == '\0')
        return;

    auto& filters = *_search_state.search_pb.mutable_filters();
    (*filters[pchKeyToMatch].mutable_filter())[eComparisonType].set_i(nValueToMatch);
}

void Steam_Matchmaking::AddRequestLobbyListSlotsAvailableFilter()
{
    AddRequestLobbyListFilterSlotsAvailable(-1);
}

// returns only lobbies with the specified number of slots available
void Steam_Matchmaking::AddRequestLobbyListFilterSlotsAvailable(int nSlotsAvailable)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _search_state.search_pb.set_slots_available(nSlotsAvailable);
}

// returns results closest to the specified value. Multiple near filters can be added, with early filters taking precedence
void Steam_Matchmaking::AddRequestLobbyListNearValueFilter(const char* pchKeyToMatch, int nValueToBeCloseTo)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    // ?
}

// sets the distance for which we should search for lobbies (based on users IP address to location map on the Steam backed)
void Steam_Matchmaking::AddRequestLobbyListDistanceFilter(ELobbyDistanceFilter eLobbyDistanceFilter)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    // ?
}

// sets how many results to return, the lower the count the faster it is to download the lobby results & details to the client
void Steam_Matchmaking::AddRequestLobbyListResultCountFilter(int cMaxResults)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _search_state.max_results = cMaxResults;
}

void Steam_Matchmaking::AddRequestLobbyListCompatibleMembersFilter(CSteamID steamIDLobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    // ?
}

// returns the CSteamID of a lobby, as retrieved by a RequestLobbyList call
// should only be called after a LobbyMatchList_t callback is received
// iLobby is of the range [0, LobbyMatchList_t::m_nLobbiesMatching)
// the returned CSteamID::IsValid() will be false if iLobby is out of range
CSteamID Steam_Matchmaking::GetLobbyByIndex(int iLobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (_search_state.callback.get() != nullptr || iLobby >= _search_state.lobbies.size())
    {// Still looking for lobbies or iLobby is out of range
        return k_steamIDNil;
    }
    
    auto it = _search_state.lobbies.begin();
    std::advance(it, iLobby);
    return CSteamID(uint64(it->second->infos.lobby_id()));
}

// Create a lobby on the Steam servers.
// If private, then the lobby will not be returned by any RequestLobbyList() call; the CSteamID
// of the lobby will need to be communicated via game channels or via InviteUserToLobby()
// this is an asynchronous request
// results will be returned by LobbyCreated_t callback and call result; lobby is joined & ready to use at this point
// a LobbyEnter_t callback will also be received (since the local user is joining their own lobby)
void Steam_Matchmaking::CreateLobby(bool bPrivate)
{
    CreateLobby(bPrivate ? k_ELobbyTypePrivate : k_ELobbyTypePublic);
}

SteamAPICall_t Steam_Matchmaking::CreateLobby(ELobbyType eLobbyType)
{
    return CreateLobby(eLobbyType, 250);
}

// This must send 3 callbacks on the same frame:
// LobbyCreated_t
// LobbyEnter_t
// LobbyDataUpdate_t
STEAM_CALL_RESULT(LobbyCreated_t)
SteamAPICall_t Steam_Matchmaking::CreateLobby(ELobbyType eLobbyType, int cMaxMembers)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    if (cMaxMembers > 250)
        return k_uAPICallInvalid;
    
    pFrameResult_t result(new FrameResult);
    LobbyCreated_t& lc = result->CreateCallback<LobbyCreated_t>(std::chrono::milliseconds(200));

    if (eLobbyType == k_ELobbyTypePrivateUnique)
    {
        lc.m_eResult = k_EResultFail;
        lc.m_ulSteamIDLobby = 0;

        result->done = true;
    }
    else
    {
        auto counts = get_invisible_visible_lobby_count();

        switch (eLobbyType)
        {
            case k_ELobbyTypePrivate    :
            case k_ELobbyTypeFriendsOnly:
            case k_ELobbyTypePublic:
            {
                if (counts.second >= 1)
                {
                    lc.m_eResult = k_EResultFail;
                    lc.m_ulSteamIDLobby = 0;

                    result->done = true;
                }
            }
            break;

            case k_ELobbyTypeInvisible  :
            {
                if (counts.first >= 2)
                {
                    lc.m_eResult = k_EResultFail;
                    lc.m_ulSteamIDLobby = 0;

                    result->done = true;
                }
            }
            break;
        }

        if (!result->done)
        {
            uint64_t new_lobby_id = generate_steam_id_lobby().ConvertToUint64();
            auto& lobby = _lobbies[new_lobby_id];
            _joined_lobbies[new_lobby_id] = &lobby;

            lobby.infos.set_lobby_id(new_lobby_id);
            lobby.infos.set_lobby_type(eLobbyType);
            lobby.infos.set_creation_time(std::chrono::system_clock::now().time_since_epoch().count());
            lobby.infos.set_max_members(cMaxMembers);
            lobby.infos.set_owner(Settings::Inst().userid.ConvertToUint64());
            lobby.infos.set_joinable(true);

            lc.m_eResult = k_EResultOK;
            lc.m_ulSteamIDLobby = new_lobby_id;
        }
    }

    _cb_manager->add_apicall(this, result);
    return result->GetAPICall();
}

// Joins an existing lobby
// this is an asynchronous request
// results will be returned by LobbyEnter_t callback & call result, check m_EChatRoomEnterResponse to see if was successful
// lobby metadata is available to use immediately on this call completing
STEAM_CALL_RESULT(LobbyEnter_t)
SteamAPICall_t Steam_Matchmaking::JoinLobby(CSteamID steamIDLobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    if (_join_state.callback.get() != nullptr)
    {
        if(steamIDLobby.ConvertToUint64() == _join_state.callback->GetCallback<LobbyEnter_t>().m_ulSteamIDLobby)
            return _join_state.callback->GetAPICall();

        // Allow only 1 join request at a time
        return k_uAPICallInvalid;
    }

    pFrameResult_t res(new FrameResult);
    LobbyEnter_t& le = res->CreateCallback<LobbyEnter_t>();
    le.m_bLocked = true;
    le.m_rgfChatPermissions = 0;
    le.m_ulSteamIDLobby = steamIDLobby.ConvertToUint64();

    auto counts = get_invisible_visible_lobby_count();
    uint64 lobby_id = steamIDLobby.ConvertToUint64();
    uint64 owner_id = 0;

    _cb_manager->add_apicall(this, res);

    auto it = _lobbies.find(lobby_id);
    if (it != _lobbies.end())
    {
        if (it->second.infos.lobby_type() == k_ELobbyTypeInvisible)
        {
            // Allow 2 invisible lobbies
            if (counts.first < 2)
            {
                owner_id = it->second.infos.owner();
            }
        }
        else
        {
            // Allow only 1 visible lobby
            if (counts.second == 0)
            {
                owner_id = it->second.infos.owner();
            }
        }
    }
    else
    {// Did not find lobby in search, lets assume its a lobby from a friend
     // If so, its a visible lobby
        // Allow only 1 visible lobby
        if (counts.second >= 1)
            return k_uAPICallInvalid;

        for (auto& frd : GetSteam_Friends()._friends)
        {
            if (frd.second.infos.lobby_id() == lobby_id)
            {
                owner_id = frd.second.infos.lobby_owner();
            }
        }
    }

    if (owner_id != 0)
    {
        Lobby_Join_Request_pb* request = new Lobby_Join_Request_pb;

        request->set_lobby_id(lobby_id);

        if (!send_lobby_join_request(owner_id, request))
        {// Failed to send lobby join request
            le.m_EChatRoomEnterResponse = k_EChatRoomEnterResponseDoesntExist;
            res->done = true;
        }
        else
        {
            _join_state.callback = res;
        }
    }
    else
    {
        le.m_EChatRoomEnterResponse = k_EChatRoomEnterResponseDoesntExist;
        res->done = true;
    }

    return res->GetAPICall();
}

// Leave a lobby; this will take effect immediately on the client side
// other users in the lobby will be notified by a LobbyChatUpdate_t callback
void Steam_Matchmaking::LeaveLobby(CSteamID steamIDLobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto lobby_it = _joined_lobbies.find(steamIDLobby.ConvertToUint64());
    if (lobby_it != _joined_lobbies.end())
    {
        uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

        Lobby_Member_Leave_pb* leave = new Lobby_Member_Leave_pb;
        leave->set_lobby_id(lobby_it->second->infos.lobby_id());
        leave->set_member_id(steam_id);
        leave->set_reason(k_EChatMemberStateChangeLeft);

        send_lobby_member_leave(leave, lobby_it->second);

        if (i_am_owner(lobby_it->second))
        {
            lobby_it->second->infos.set_owner(0);
        }

        if (lobby_it->second->infos.lobby_type() != k_ELobbyTypeInvisible)
        {
            auto me = GetSteam_Friends().myself();
            me->second.infos.set_lobby_id(0);
            me->second.infos.set_lobby_owner(0);
        }

        _joined_lobbies.erase(lobby_it);
    }
}

// Invite another user to the lobby
// the target user will receive a LobbyInvite_t callback
// will return true if the invite is successfully sent, whether or not the target responds
// returns false if the local user is not connected to the Steam servers
// if the other user clicks the join link, a GameLobbyJoinRequested_t will be posted if the user is in-game,
// or if the game isn't running yet the game will be launched with the parameter +connect_lobby <64-bit lobby id>
bool Steam_Matchmaking::InviteUserToLobby(CSteamID steamIDLobby, CSteamID steamIDInvitee)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
   
    lobby_state_t* pLobby = get_lobby_by_id(steamIDLobby.ConvertToUint64());

    if (pLobby == nullptr)
        return false;

    if (i_am_in_lobby(pLobby))
    {
        Lobby_Invite_pb* li = new Lobby_Invite_pb;
        *li->mutable_lobby() = pLobby->infos;

        return send_lobby_invite(steamIDInvitee.ConvertToUint64(), li);
    }

    return true;
}

// Lobby iteration, for viewing details of users in a lobby
// only accessible if the lobby user is a member of the specified lobby
// persona information for other lobby members (name, avatar, etc.) will be asynchronously received
// and accessible via ISteamFriends interface

// returns the number of users in the specified lobby
int Steam_Matchmaking::GetNumLobbyMembers(CSteamID steamIDLobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    lobby_state_t* pLobby = get_lobby_by_id(steamIDLobby.ConvertToUint64());
    if (pLobby == nullptr)
        return 0;

    return pLobby->infos.members_size();
}
// returns the CSteamID of a user in the lobby
// iMember is of range [0,GetNumLobbyMembers())
// note that the current user must be in a lobby to retrieve CSteamIDs of other users in that lobby
CSteamID Steam_Matchmaking::GetLobbyMemberByIndex(CSteamID steamIDLobby, int iMember)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    lobby_state_t* pLobby = get_lobby_by_id(steamIDLobby.ConvertToUint64());
    if(pLobby == nullptr || iMember >= pLobby->infos.members_size())
        return k_steamIDNil;

    auto it = pLobby->infos.members().begin();
    std::advance(it, iMember);

    return CSteamID(static_cast<uint64>(it->first));
}

// Get data associated with this lobby
// takes a simple key, and returns the string associated with it
// "" will be returned if no value is set, or if steamIDLobby is invalid
const char* Steam_Matchmaking::GetLobbyData(CSteamID steamIDLobby, const char* pchKey)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (pchKey == nullptr)
        return "";

    uint64 lobby_id = steamIDLobby.ConvertToUint64();

    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);
    if (pLobby == nullptr)
    {
        return "";
    }

    auto& lobby_datas = *pLobby->infos.mutable_lobby_datas();
    auto it = find_lobbydata(lobby_datas, pchKey);
    if (it == lobby_datas.end())
        return "";

    return it->second.c_str();
}
// Sets a key/value pair in the lobby metadata
// each user in the lobby will be broadcast this new value, and any new users joining will receive any existing data
// this can be used to set lobby names, map, etc.
// to reset a key, just set it to ""
// other users in the lobby will receive notification of the lobby data change via a LobbyDataUpdate_t callback
bool Steam_Matchmaking::SetLobbyData(CSteamID steamIDLobby, const char* pchKey, const char* pchValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    size_t key_len;
    if (pchKey == nullptr || (key_len = strlen(pchKey)) > k_nMaxLobbyKeyLength || key_len == 0)
        return false;

    if (pchValue == nullptr || strlen(pchValue) > k_cubChatMetadataMax)
        return false;

    uint64 lobby_id = steamIDLobby.ConvertToUint64();
    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);
    if (pLobby == nullptr || !i_am_owner(pLobby))
        return false;

    auto& lobby_datas = *pLobby->infos.mutable_lobby_datas();
    auto data_it = find_lobbydata(lobby_datas, pchKey);

    if (data_it == lobby_datas.end())
        lobby_datas[pchKey] = pchValue;
    else
        data_it->second = pchValue;

    trigger_lobby_update(lobby_id, lobby_id);
    send_lobby_data_update(pLobby);

    return true;
}

// returns the number of metadata keys set on the specified lobby
int Steam_Matchmaking::GetLobbyDataCount(CSteamID steamIDLobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    uint64 lobby_id = steamIDLobby.ConvertToUint64();

    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);
    if (pLobby == nullptr)
    {
        return 0;
    }

    return pLobby->infos.lobby_datas_size();
}

// returns a lobby metadata key/values pair by index, of range [0, GetLobbyDataCount())
bool Steam_Matchmaking::GetLobbyDataByIndex(CSteamID steamIDLobby, int iLobbyData, char* pchKey, int cchKeyBufferSize, char* pchValue, int cchValueBufferSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    uint64 lobby_id = steamIDLobby.ConvertToUint64();

    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);
    if (pLobby == nullptr || iLobbyData >= pLobby->infos.lobby_datas_size())
    {
        if (pchKey != nullptr && cchKeyBufferSize > 0)
            *pchKey = 0;

        if (pchValue != nullptr && cchValueBufferSize > 0)
            *pchValue = 0;

        return false;
    }

    auto it = pLobby->infos.lobby_datas().begin();
    std::advance(it, iLobbyData);

    if (pchKey != nullptr)
        strncpy(pchKey, it->first.c_str(), cchKeyBufferSize);

    if (pchValue != nullptr)
        strncpy(pchValue, it->second.c_str(), cchValueBufferSize);

    return true;
}

// removes a metadata key from the lobby
bool Steam_Matchmaking::DeleteLobbyData(CSteamID steamIDLobby, const char* pchKey)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    if (pchKey == nullptr || *pchKey == '\0')
    {
        return false;
    }

    uint64 lobby_id = steamIDLobby.ConvertToUint64();
    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);
    if (pLobby == nullptr || !i_am_owner(pLobby))
    {
        return false;
    }

    auto& lobby_datas = *pLobby->infos.mutable_lobby_datas();
    auto it = find_lobbydata(lobby_datas, pchKey);
    if (it != lobby_datas.end())
    {
        lobby_datas.erase(it);

        trigger_lobby_update(lobby_id, lobby_id);
        send_lobby_data_update(pLobby);
    }

    return true;
}

// Gets per-user metadata for someone in this lobby
const char* Steam_Matchmaking::GetLobbyMemberData(CSteamID steamIDLobby, CSteamID steamIDUser, const char* pchKey)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
    
    if (pchKey == nullptr || *pchKey == '\0')
        return "";

    uint64 lobby_id = steamIDLobby.ConvertToUint64();
    uint64 member_id = steamIDUser.ConvertToUint64();

    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);
    if (pLobby == nullptr)
    {
        return "";
    }

    auto& members = *pLobby->infos.mutable_members();
    auto member_it = members.find(member_id);

    if (member_it == members.end())
        return "";

    auto& member_datas = *member_it->second.mutable_member_datas();
    auto member_datas_it = find_lobbydata(member_datas, pchKey);
    if (member_datas_it == member_datas.end())
        return "";

    return member_datas_it->second.c_str();
}
// Sets per-user metadata (for the local user implicitly)
void Steam_Matchmaking::SetLobbyMemberData(CSteamID steamIDLobby, const char* pchKey, const char* pchValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    if (pchKey == nullptr || *pchKey == '\0' || pchValue == nullptr)
        return;

    uint64 lobby_id = steamIDLobby.ConvertToUint64();
    uint64 member_id = Settings::Inst().userid.ConvertToUint64();

    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);
    if (pLobby == nullptr || !i_am_in_lobby(pLobby))
        return;

    auto& member = (*pLobby->infos.mutable_members())[member_id];
    auto& member_datas = *member.mutable_member_datas();
    auto member_datas_it = find_lobbydata(member_datas, pchKey);

    if (member_datas_it == member_datas.end())
        member_datas[pchKey] = pchValue;
    else
        member_datas_it->second = pchValue;

    send_lobby_member_data_update(member_id, pLobby);
    trigger_lobby_update(member_id, lobby_id);
}

// Broadcasts a chat message to the all the users in the lobby
// users in the lobby (including the local user) will receive a LobbyChatMsg_t callback
// returns true if the message is successfully sent
// pvMsgBody can be binary or text data, up to 4k
// if pvMsgBody is text, cubMsgBody should be strlen( text ) + 1, to include the null terminator
bool Steam_Matchmaking::SendLobbyChatMsg(CSteamID steamIDLobby, const void* pvMsgBody, int cubMsgBody)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (pvMsgBody == nullptr)
        return false;

    uint64 lobby_id = steamIDLobby.ConvertToUint64();
    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);
    if (pLobby == nullptr || !i_am_in_lobby(pLobby))
        return false;

    Lobby_Chat_Message_pb* message = new Lobby_Chat_Message_pb;

    message->set_lobby_id(lobby_id);
    message->mutable_entry()->set_member_id(steam_id);
    message->mutable_entry()->set_message_type(k_EChatEntryTypeChatMsg);
    message->mutable_entry()->mutable_message()->assign(reinterpret_cast<const char*>(pvMsgBody), cubMsgBody);

    // send_lobby_chat_message doesn't free the message
    auto res = send_lobby_chat_message(message, pLobby);

    if (res)
    {
        pLobby->infos.mutable_chat_entries()->Add(std::move(*message->mutable_entry()));
    }

    delete message;

    return res;
}
// Get a chat message as specified in a LobbyChatMsg_t callback
// iChatID is the LobbyChatMsg_t::m_iChatID value in the callback
// *pSteamIDUser is filled in with the CSteamID of the member
// *pvData is filled in with the message itself
// return value is the number of bytes written into the buffer
int Steam_Matchmaking::GetLobbyChatEntry(CSteamID steamIDLobby, int iChatID, STEAM_OUT_STRUCT() CSteamID* pSteamIDUser, void* pvData, int cubData, EChatEntryType* peChatEntryType)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    uint64 lobby_id = steamIDLobby.ConvertToUint64();
    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);
    if (pLobby == nullptr || !i_am_in_lobby(pLobby) || iChatID >= pLobby->infos.chat_entries_size())
    {
        if (pSteamIDUser != nullptr)
            *pSteamIDUser = k_steamIDNil;

        if (peChatEntryType != nullptr)
            *peChatEntryType = k_EChatEntryTypeInvalid;

        return 0;
    }

    auto& entry = pLobby->infos.chat_entries()[iChatID];
    if (peChatEntryType != nullptr)
        *peChatEntryType = static_cast<EChatEntryType>(entry.message_type());

    if(pSteamIDUser != nullptr)
        *pSteamIDUser = CSteamID(static_cast<uint64>(entry.member_id()));

    if (pvData != nullptr)
    {
        cubData = std::min<int>(cubData, entry.message().length());
        memcpy(pvData, entry.message().data(), cubData);
    }

    return cubData;
}

// Refreshes metadata for a lobby you're not necessarily in right now
// you never do this for lobbies you're a member of, only if your
// this will send down all the metadata associated with a lobby
// this is an asynchronous call
// returns false if the local user is not connected to the Steam servers
// results will be returned by a LobbyDataUpdate_t callback
// if the specified lobby doesn't exist, LobbyDataUpdate_t::m_bSuccess will be set to false
bool Steam_Matchmaking::RequestLobbyData(CSteamID steamIDLobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    uint64 lobby_id = steamIDLobby.ConvertToUint64();
    auto& request = _lobbies[lobby_id].request;

    Lobby_Infos_Request_pb* request_pb = new Lobby_Infos_Request_pb;

    request_pb->set_lobby_id(lobby_id);

    request.request_start = std::chrono::steady_clock::now();

    send_lobby_infos_request(request_pb);
    if (request.callback.get() == nullptr)
    {
        request.callback.reset(new FrameResult);
        LobbyDataUpdate_t& ldu = request.callback->CreateCallback<LobbyDataUpdate_t>();
        ldu.m_ulSteamIDLobby = steamIDLobby.ConvertToUint64();
        ldu.m_ulSteamIDMember = ldu.m_ulSteamIDLobby;

        _cb_manager->add_callback(this, request.callback);
    }

    return true;
}

// sets the game server associated with the lobby
// usually at this point, the users will join the specified game server
// either the IP/Port or the steamID of the game server has to be valid, depending on how you want the clients to be able to connect
void Steam_Matchmaking::SetLobbyGameServer(CSteamID steamIDLobby, uint32 unGameServerIP, uint16 unGameServerPort, CSteamID steamIDGameServer)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    uint64 lobby_id = steamIDLobby.ConvertToUint64();
    uint64 member_id = Settings::Inst().userid.ConvertToUint64();

    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);

    if (pLobby == nullptr || !i_am_owner(pLobby))
    {
        // Send callback with false as result ?
        trigger_lobby_update(member_id, lobby_id, false);
        return;
    }

    pLobby->infos.set_gameserver_ip(unGameServerIP);
    pLobby->infos.set_gameserver_port(unGameServerPort);
    pLobby->infos.set_gameserver_id(steamIDGameServer.ConvertToUint64());

    if (pLobby->infos.lobby_type() != k_ELobbyTypeInvisible)
    {
        auto my_friend_infos = GetSteam_Friends().myself();
        my_friend_infos->second.infos.set_game_ip(unGameServerIP);
        my_friend_infos->second.infos.set_game_port(unGameServerIP);
    }

    trigger_lobby_update(lobby_id, lobby_id);
    send_lobby_update(pLobby);

}
// returns the details of a game server set in a lobby - returns false if there is no game server set, or that lobby doesn't exist
bool Steam_Matchmaking::GetLobbyGameServer(CSteamID steamIDLobby, uint32* punGameServerIP, uint16* punGameServerPort, STEAM_OUT_STRUCT() CSteamID* psteamIDGameServer)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    uint64 lobby_id = steamIDLobby.ConvertToUint64();

    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);
    if (pLobby == nullptr)
    {
        return false;
    }

    if(punGameServerIP != nullptr)
        *punGameServerIP = pLobby->infos.gameserver_ip();

    if(punGameServerPort != nullptr)
        *punGameServerPort = pLobby->infos.gameserver_port();

    if( psteamIDGameServer != nullptr)
        *psteamIDGameServer = static_cast<uint64>(pLobby->infos.gameserver_id());

    return true;
}

// set the limit on the # of users who can join the lobby
bool Steam_Matchmaking::SetLobbyMemberLimit(CSteamID steamIDLobby, int cMaxMembers)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    if (cMaxMembers > 250 || cMaxMembers < 0)
        return false;

    uint64 lobby_id = steamIDLobby.ConvertToUint64();
    uint64 member_id = Settings::Inst().userid.ConvertToUint64();

    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);

    if (pLobby == nullptr || !i_am_owner(pLobby))
    {
        // Send callback with false as result ?
        trigger_lobby_update(member_id, lobby_id, false);
        return false;
    }

    pLobby->infos.set_max_members(cMaxMembers);
    trigger_lobby_update(lobby_id, lobby_id);
    send_lobby_update(pLobby);

    return true;
}
// returns the current limit on the # of users who can join the lobby; returns 0 if no limit is defined
int Steam_Matchmaking::GetLobbyMemberLimit(CSteamID steamIDLobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    uint64 lobby_id = steamIDLobby.ConvertToUint64();
    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);

    if (pLobby == nullptr)
    {
        return 0;
    }

    return pLobby->infos.max_members();
}

// updates which type of lobby it is
// only lobbies that are k_ELobbyTypePublic or k_ELobbyTypeInvisible, and are set to joinable, will be returned by RequestLobbyList() calls
bool Steam_Matchmaking::SetLobbyType(CSteamID steamIDLobby, ELobbyType eLobbyType)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    uint64 lobby_id = steamIDLobby.ConvertToUint64();
    uint64 member_id = Settings::Inst().userid.ConvertToUint64();

    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);
    if (pLobby == nullptr || !i_am_owner(pLobby))
    {
        // Send callback with false as result ?
        trigger_lobby_update(member_id, lobby_id, false);
        return false;
    }

    pLobby->infos.set_lobby_type(eLobbyType);
    trigger_lobby_update(lobby_id, lobby_id, true);
    send_lobby_update(pLobby);

    auto me = GetSteam_Friends().myself();
    if (eLobbyType != k_ELobbyTypeInvisible && me->second.infos.lobby_id() == 0)
    {
        me->second.infos.set_lobby_id(pLobby->infos.lobby_id());
        me->second.infos.set_lobby_owner(pLobby->infos.owner());
    }
    else if (eLobbyType == k_ELobbyTypeInvisible && me->second.infos.lobby_id() == pLobby->infos.lobby_id())
    {
        me->second.infos.set_lobby_id(0);
        me->second.infos.set_lobby_owner(0);
    }

    return true;
}

// sets whether or not a lobby is joinable - defaults to true for a new lobby
// if set to false, no user can join, even if they are a friend or have been invited
bool Steam_Matchmaking::SetLobbyJoinable(CSteamID steamIDLobby, bool bLobbyJoinable)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    uint64 lobby_id = steamIDLobby.ConvertToUint64();
    uint64 member_id = Settings::Inst().userid.ConvertToUint64();

    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);
    if (pLobby == nullptr || !i_am_owner(pLobby))
    {
        return false;
    }

    pLobby->infos.set_joinable(bLobbyJoinable);
    trigger_lobby_update(lobby_id, lobby_id, true);
    send_lobby_update(pLobby);

    return true;
}

// returns the current lobby owner
// you must be a member of the lobby to access this
// there always one lobby owner - if the current owner leaves, another user will become the owner
// it is possible (bur rare) to join a lobby just as the owner is leaving, thus entering a lobby with self as the owner
CSteamID Steam_Matchmaking::GetLobbyOwner(CSteamID steamIDLobby)
{
    //LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    uint64 lobby_id = steamIDLobby.ConvertToUint64();
    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);

    if (pLobby == nullptr)
    {
        return k_steamIDNil;
    }

    return CSteamID(static_cast<uint64>(pLobby->infos.owner()));
}

// changes who the lobby owner is
// you must be the lobby owner for this to succeed, and steamIDNewOwner must be in the lobby
// after completion, the local user will no longer be the owner
bool Steam_Matchmaking::SetLobbyOwner(CSteamID steamIDLobby, CSteamID steamIDNewOwner)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);
    
    uint64 lobby_id = steamIDLobby.ConvertToUint64();
    uint64 member_id = Settings::Inst().userid.ConvertToUint64();
    
    lobby_state_t* pLobby = get_lobby_by_id(lobby_id);
    if (pLobby == nullptr || !i_am_owner(pLobby) || !is_lobby_member(steamIDNewOwner.ConvertToUint64(), pLobby))
    {
        // Send callback with false as result ?
        trigger_lobby_update(member_id, lobby_id, false);
        return false;
    }
    
    return send_lobby_member_promote(member_id, pLobby);
}

// link two lobbies for the purposes of checking player compatibility
// you must be the lobby owner of both lobbies
bool Steam_Matchmaking::SetLinkedLobby(CSteamID steamIDLobby, CSteamID steamIDLobbyDependent)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    return false;
}

#ifdef _PS3
// changes who the lobby owner is
// you must be the lobby owner for this to succeed, and steamIDNewOwner must be in the lobby
// after completion, the local user will no longer be the owner
void Steam_Matchmaking::CheckForPSNGameBootInvite(unsigned int iGameBootAttributes)
{

}
#endif

///////////////////////////////////////////////////////////////////////////////
//                           Network Send messages                           //
///////////////////////////////////////////////////////////////////////////////
bool Steam_Matchmaking::send_to_owner(Network_Message_pb& msg, lobby_state_t* lobby)
{
    assert(lobby != nullptr);

    msg.set_dest_id(lobby->infos.owner());
    return _network->ReliableSendTo(msg);
}

bool Steam_Matchmaking::send_to_all_members(Network_Message_pb& msg, lobby_state_t* lobby)
{
    assert(lobby != nullptr);

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    for (auto& member : lobby->infos.members())
    {
        if (member.first != steam_id)
        {
            msg.set_dest_id(member.first);
            _network->ReliableSendTo(msg);
        }
    }

    return true;
}

bool Steam_Matchmaking::send_to_all_members_or_owner(Network_Message_pb& msg, lobby_state_t* lobby)
{
    if (i_am_owner(lobby))
        return send_to_all_members(msg, lobby);
    else
        return send_to_owner(msg, lobby);
}

bool Steam_Matchmaking::send_lobby_infos_request(Lobby_Infos_Request_pb* request)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Matchmaking_pb* mm = new Steam_Matchmaking_pb;

    mm->set_allocated_lobby_infos_request(request);
    msg.set_allocated_matchmaking(mm);

    msg.set_source_id(steam_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    _network->ReliableSendToAllPeers(msg);
    return true;
}

bool Steam_Matchmaking::send_lobby_infos_response(Network::peer_t const& peer_id, Lobby_Infos_Response_pb* response)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Matchmaking_pb* mm = new Steam_Matchmaking_pb;

    mm->set_allocated_lobby_infos_response(response);
    msg.set_allocated_matchmaking(mm);

    msg.set_source_id(steam_id);
    msg.set_dest_id(peer_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

bool Steam_Matchmaking::send_lobby_join_request(Network::peer_t const& peer_id, Lobby_Join_Request_pb* request)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Matchmaking_pb* mm = new Steam_Matchmaking_pb;

    mm->set_allocated_lobby_join_request(request);
    msg.set_allocated_matchmaking(mm);

    msg.set_source_id(steam_id);
    msg.set_dest_id(peer_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

bool Steam_Matchmaking::send_lobby_join_response(Network::peer_t const& peer_id, Lobby_Join_Response_pb* response)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Matchmaking_pb* mm = new Steam_Matchmaking_pb;

    mm->set_allocated_lobby_join_response(response);
    msg.set_allocated_matchmaking(mm);

    msg.set_source_id(steam_id);
    msg.set_dest_id(peer_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

bool Steam_Matchmaking::send_lobby_invite(Network::peer_t const& peer_id, Lobby_Invite_pb* invite)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Matchmaking_pb* mm = new Steam_Matchmaking_pb;

    mm->set_allocated_lobby_invite(invite);
    msg.set_allocated_matchmaking(mm);

    msg.set_source_id(steam_id);
    msg.set_dest_id(peer_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

bool Steam_Matchmaking::send_lobby_update(lobby_state_t* lobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    assert(lobby != nullptr);
    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Matchmaking_pb* mm = new Steam_Matchmaking_pb;
    Lobby_Update_pb* update = new Lobby_Update_pb;

    update->set_lobby_id(lobby->infos.lobby_id());
    update->set_gameserver_id(lobby->infos.gameserver_id());
    update->set_gameserver_ip(lobby->infos.gameserver_ip());
    update->set_gameserver_port(lobby->infos.gameserver_port());
    update->set_lobby_type(lobby->infos.lobby_type());
    update->set_joinable(lobby->infos.joinable());

    mm->set_allocated_lobby_update(update);
    msg.set_allocated_matchmaking(mm);

    msg.set_source_id(steam_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return send_to_all_members(msg, lobby);
}

bool Steam_Matchmaking::send_lobby_data_update(lobby_state_t* lobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    assert(lobby != nullptr);
    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Matchmaking_pb* mm = new Steam_Matchmaking_pb;
    Lobby_Data_Update_pb* update = new Lobby_Data_Update_pb;

    update->set_lobby_id(lobby->infos.lobby_id());
    (*update->mutable_lobby_datas()) = lobby->infos.lobby_datas();

    mm->set_allocated_lobby_data_update(update);
    msg.set_allocated_matchmaking(mm);

    msg.set_source_id(steam_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return send_to_all_members(msg, lobby);
}

bool Steam_Matchmaking::send_lobby_chat_message(Lobby_Chat_Message_pb* message, lobby_state_t* lobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    assert(lobby != nullptr);
    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Matchmaking_pb* mm = new Steam_Matchmaking_pb;

    mm->set_allocated_lobby_chat_message(message);
    msg.set_allocated_matchmaking(mm);

    msg.set_source_id(steam_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    auto res = send_to_all_members_or_owner(msg, lobby);

    mm->release_lobby_chat_message();

    return res;
}

bool Steam_Matchmaking::send_lobby_member_join(Network::peer_t const& member_id, lobby_state_t* lobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    assert(lobby != nullptr);

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Matchmaking_pb* mm = new Steam_Matchmaking_pb;
    Lobby_Member_Join_pb* join = new Lobby_Member_Join_pb;

    join->set_lobby_id(lobby->infos.lobby_id());
    join->set_member_id(member_id);

    mm->set_allocated_lobby_member_join(join);

    msg.set_allocated_matchmaking(mm);
    msg.set_source_id(steam_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return send_to_all_members(msg, lobby);
}

bool Steam_Matchmaking::send_lobby_member_leave(Lobby_Member_Leave_pb* leave, lobby_state_t* lobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    assert(lobby != nullptr);

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Matchmaking_pb* mm = new Steam_Matchmaking_pb;

    mm->set_allocated_lobby_member_leave(leave);

    msg.set_allocated_matchmaking(mm);
    msg.set_source_id(steam_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return send_to_all_members_or_owner(msg, lobby);
}

bool Steam_Matchmaking::send_lobby_member_data_update(Network::peer_t const& member_id, lobby_state_t* lobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    assert(lobby != nullptr);

    auto it = lobby->infos.members().find(member_id);
    if (it != lobby->infos.members().end())
    {
        uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

        Network_Message_pb msg;
        Steam_Matchmaking_pb* mm = new Steam_Matchmaking_pb;

        Lobby_Member_Data_Update_pb* update = new Lobby_Member_Data_Update_pb;

        update->set_lobby_id(lobby->infos.lobby_id());
        *update->mutable_datas() = it->second;

        mm->set_allocated_lobby_member_data_update(update);
        msg.set_allocated_matchmaking(mm);

        msg.set_source_id(steam_id);
        msg.set_app_id(Settings::Inst().gameid.AppID());

        return send_to_all_members_or_owner(msg, lobby);
    }
    return false;
}

bool Steam_Matchmaking::send_lobby_member_promote(Network::peer_t const& member_id, lobby_state_t* lobby)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    assert(lobby != nullptr);

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Matchmaking_pb* mm = new Steam_Matchmaking_pb;
    Lobby_Member_Promote_pb* promote = new Lobby_Member_Promote_pb;

    promote->set_lobby_id(lobby->infos.lobby_id());
    promote->set_member_id(member_id);

    mm->set_allocated_lobby_member_promote(promote);

    msg.set_allocated_matchmaking(mm);

    msg.set_source_id(steam_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return send_to_all_members(msg, lobby);
}

bool Steam_Matchmaking::send_lobby_search(Lobby_Search_pb* search)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Matchmaking_pb* mm = new Steam_Matchmaking_pb;


    mm->set_allocated_lobby_search(search);

    msg.set_allocated_matchmaking(mm);

    msg.set_source_id(steam_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    _search_state.peers = std::move(_network->ReliableSendToAllPeers(msg));

    mm->release_lobby_search();

    return true;
}

bool Steam_Matchmaking::send_lobby_search_response(Network::peer_t const& peer_id, Lobby_Search_Response_pb* response)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Matchmaking_pb* mm = new Steam_Matchmaking_pb;

    mm->set_allocated_lobby_search_response(response);

    msg.set_allocated_matchmaking(mm);

    msg.set_dest_id(peer_id);
    msg.set_source_id(steam_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

///////////////////////////////////////////////////////////////////////////////
//                          Network Receive messages                         //
///////////////////////////////////////////////////////////////////////////////
bool Steam_Matchmaking::on_peer_disconnect(Network_Message_pb const& msg, Network_Peer_Disconnect_pb const& conn)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    for (auto& lobby : _joined_lobbies)
    {
        // If I'm the owner, notify members that someone left
        // members will update lobby on Lobby_Leave_pb
        if (i_am_owner(lobby.second) && remove_lobby_member(msg.source_id(), lobby.second))
        {
            Lobby_Member_Leave_pb* resp_ll = new Lobby_Member_Leave_pb;

            resp_ll->set_lobby_id(lobby.second->infos.lobby_id());
            resp_ll->set_member_id(msg.source_id());
            resp_ll->set_reason(EChatMemberStateChange::k_EChatMemberStateChangeDisconnected);

            send_lobby_member_leave(resp_ll, lobby.second);
        }
        
        //trigger_chat_member(msg.source_id(), (uint64)lobby.second->infos.lobby_id(), EChatMemberStateChange::k_EChatMemberStateChangeDisconnected);
        trigger_chat_member(msg.source_id(), (uint64)lobby.second->infos.lobby_id(), EChatMemberStateChange::k_EChatMemberStateChangeDisconnected);
        trigger_lobby_update(msg.source_id(), (uint64_t)lobby.second->infos.lobby_id());
    }
    return true;
}

bool Steam_Matchmaking::on_lobby_infos_request(Network_Message_pb const& msg, Lobby_Infos_Request_pb const& request)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto it = _joined_lobbies.find(request.lobby_id());
    if (it != _joined_lobbies.end() && i_am_owner(it->second))
    {
        Lobby_Infos_Response_pb* response = new Lobby_Infos_Response_pb;
        Lobby_Infos_pb* lobby_infos = new Lobby_Infos_pb(it->second->infos);

        response->set_allocated_infos(lobby_infos);
        send_lobby_infos_response(msg.source_id(), response);
    }

    return true;
}

bool Steam_Matchmaking::on_lobby_infos_response(Network_Message_pb const& msg, Lobby_Infos_Response_pb const& response)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    lobby_state_t& lobby = _lobbies[response.infos().lobby_id()];
    if (lobby.request.callback.get() != nullptr)
    {
        lobby.infos = response.infos();
        LobbyDataUpdate_t& ldu = lobby.request.callback->GetCallback<LobbyDataUpdate_t>();

        ldu.m_bSuccess = 1;
        lobby.request.callback->done = true;
        lobby.request.callback.reset();
    }

    return true;
}

bool Steam_Matchmaking::on_lobby_join_request(Network_Message_pb const& msg, Lobby_Join_Request_pb const& request)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "source_id: %llu", msg.source_id());
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    Lobby_Join_Response_pb* response = new Lobby_Join_Response_pb;
    lobby_state_t* pLobby = get_lobby_by_id(request.lobby_id());

    if (pLobby != nullptr)
    {
        *response->mutable_infos() = pLobby->infos;

        if (i_am_owner(pLobby) && pLobby->infos.joinable())
        {
            if (pLobby->infos.members_size() < pLobby->infos.max_members())
            {
                response->set_reason(k_EChatRoomEnterResponseSuccess);

                send_lobby_member_join(msg.source_id(), pLobby);

                add_lobby_member(msg.source_id(), pLobby);
                trigger_lobby_update(pLobby->infos.lobby_id(), (uint64)pLobby->infos.lobby_id());
            }
            else
            {
                response->set_reason(k_EChatRoomEnterResponseFull);
            }
        }
        else
        {
            response->set_reason(k_EChatRoomEnterResponseNotAllowed);
        }
    }
    else
    {
        response->mutable_infos()->set_lobby_id(request.lobby_id());
        response->set_reason(k_EChatRoomEnterResponseDoesntExist);
    }

    return send_lobby_join_response(msg.source_id(), response);
}

bool Steam_Matchmaking::on_lobby_join_response(Network_Message_pb const& msg, Lobby_Join_Response_pb const& response)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "source_id: %llu, lobby_id: %llu", msg.source_id(), response.infos().lobby_id());
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    if (_join_state.callback.get() != nullptr)
    {
        LobbyEnter_t& le = _join_state.callback->GetCallback<LobbyEnter_t>();
        if (le.m_ulSteamIDLobby = response.infos().lobby_id())
        {
            le.m_EChatRoomEnterResponse = response.reason();

            switch (response.reason())
            {
                case EChatRoomEnterResponse::k_EChatRoomEnterResponseSuccess:
                {
                    lobby_state_t& lobby = _lobbies[response.infos().lobby_id()];

                    _joined_lobbies[response.infos().lobby_id()] = &lobby;
                    lobby.infos = response.infos();

                    add_lobby_member(msg.dest_id(), &lobby);
                    trigger_lobby_update(le.m_ulSteamIDLobby, (uint64)lobby.infos.lobby_id());
                }
                break;

                //case EChatRoomEnterResponse::k_EChatRoomEnterResponseDoesntExist:
                //case EChatRoomEnterResponse::k_EChatRoomEnterResponseNotAllowed:
                //case EChatRoomEnterResponse::k_EChatRoomEnterResponseFull:
                //case EChatRoomEnterResponse::k_EChatRoomEnterResponseError:
                //case EChatRoomEnterResponse::k_EChatRoomEnterResponseBanned:
                //case EChatRoomEnterResponse::k_EChatRoomEnterResponseLimited:
                //case EChatRoomEnterResponse::k_EChatRoomEnterResponseClanDisabled:
                //case EChatRoomEnterResponse::k_EChatRoomEnterResponseCommunityBan:
                //case EChatRoomEnterResponse::k_EChatRoomEnterResponseMemberBlockedYou:
                //case EChatRoomEnterResponse::k_EChatRoomEnterResponseYouBlockedMember:
                //case EChatRoomEnterResponse::k_EChatRoomEnterResponseNoRankingDataLobby: // No longer used
                //case EChatRoomEnterResponse::k_EChatRoomEnterResponseNoRankingDataUser: //  No longer used
                //case EChatRoomEnterResponse::k_EChatRoomEnterResponseRankOutOfRange: //  No longer used
                //case EChatRoomEnterResponse::k_EChatRoomEnterResponseRatelimitExceeded:
            }

            _join_state.callback->done = true;
            _join_state.callback.reset();
        }
    }
        
    return true;
}

bool Steam_Matchmaking::on_lobby_invite(Network_Message_pb const& msg, Lobby_Invite_pb const& li)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    uint64 lobby_id = li.lobby().lobby_id();
    GetSteam_Overlay().SetLobbyInvite(uint64(msg.source_id()), lobby_id);
    
    //pFrameResult_t result(new FrameResult);
    //GameLobbyJoinRequested_t& gljr = result->CreateCallback<GameLobbyJoinRequested_t>();
    //gljr.m_steamIDFriend = static_cast<uint64>(msg.source_id());
    //gljr.m_steamIDLobby  = lobby_id;
    //
    //result->done = true;
    //GetCBManager()->add_callback(this, result);

    return true;
}

bool Steam_Matchmaking::on_lobby_update(Network_Message_pb const& msg, Lobby_Update_pb const& update)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    lobby_state_t* pLobby = get_lobby_by_id(update.lobby_id());
    if (pLobby != nullptr)
    {
        pLobby->infos.set_gameserver_id(update.gameserver_id());
        pLobby->infos.set_gameserver_ip(update.gameserver_ip());
        pLobby->infos.set_gameserver_port(update.gameserver_port());
        pLobby->infos.set_lobby_type(update.lobby_type());
        pLobby->infos.set_joinable(update.joinable());

        trigger_lobby_update(update.lobby_id(), (uint64)pLobby->infos.lobby_id());
    }

    return true;
}

bool Steam_Matchmaking::on_lobby_data_update(Network_Message_pb const& msg, Lobby_Data_Update_pb const& update)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    lobby_state_t* pLobby = get_lobby_by_id(update.lobby_id());
    if (pLobby != nullptr)
    {
        *pLobby->infos.mutable_lobby_datas() = update.lobby_datas();

        trigger_lobby_update(update.lobby_id(), (uint64)pLobby->infos.lobby_id());
    }

    return true;
}

bool Steam_Matchmaking::on_lobby_chat_message(Network_Message_pb const& msg, Lobby_Chat_Message_pb const& message)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    lobby_state_t* pLobby = get_lobby_by_id(message.lobby_id());
    if (pLobby != nullptr)
    {
        pFrameResult_t res(new FrameResult);
        LobbyChatMsg_t& lcm = res->CreateCallback<LobbyChatMsg_t>();
        
        auto& chat_entries = *pLobby->infos.mutable_chat_entries();
        auto& entry = message.entry();

        lcm.m_iChatID = chat_entries.size();
        lcm.m_ulSteamIDLobby = message.lobby_id();
        lcm.m_eChatEntryType = entry.message_type();
        lcm.m_ulSteamIDUser = entry.member_id();
        
        *chat_entries.Add() = entry;

        res->done = true;
        _cb_manager->add_callback(this, res);
    }

    return true;
}

bool Steam_Matchmaking::on_lobby_member_join(Network_Message_pb const& msg, Lobby_Member_Join_pb const& join)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "source_id: %llu, lobby_id: %llu", msg.source_id(), join.lobby_id());
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    lobby_state_t* pLobby = get_lobby_by_id(join.lobby_id());

    if (pLobby != nullptr)
    {
        add_lobby_member(join.member_id(), pLobby);
        
        trigger_chat_member(join.member_id(), (uint64)pLobby->infos.lobby_id(), EChatMemberStateChange::k_EChatMemberStateChangeKicked);
        trigger_lobby_update(join.lobby_id(), (uint64)pLobby->infos.lobby_id());
    }

    return true;
}

bool Steam_Matchmaking::on_lobby_member_leave(Network_Message_pb const& msg, Lobby_Member_Leave_pb const& leave)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "source_id: %llu", msg.source_id());
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    lobby_state_t* pLobby = get_lobby_by_id(leave.lobby_id());
    if (pLobby != nullptr && remove_lobby_member(leave.member_id(), pLobby))
    {
        if (i_am_owner(pLobby))
        {
            Lobby_Member_Leave_pb* leave_pb = new Lobby_Member_Leave_pb(leave);
            send_lobby_member_leave(leave_pb, pLobby);
        }

        trigger_chat_member(leave.member_id(), (uint64)pLobby->infos.lobby_id(), (EChatMemberStateChange)leave.reason());
        trigger_lobby_update(leave.lobby_id(), (uint64)pLobby->infos.lobby_id());
    }

    return true;
}

bool Steam_Matchmaking::on_lobby_member_data_update(Network_Message_pb const& msg, Lobby_Member_Data_Update_pb const& update)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "source_id: %llu", msg.source_id());
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    lobby_state_t* pLobby = get_lobby_by_id(update.lobby_id());
    if (pLobby != nullptr)
    {
        auto& members = *pLobby->infos.mutable_members();
        auto member_it = members.find(update.member_id());
        if (member_it != members.end())
        {
            if (i_am_owner(pLobby))
            {
                uint64 steam_id = Settings::Inst().userid.ConvertToUint64();
                Network_Message_pb resp_msg;
                Steam_Matchmaking_pb* mm = new Steam_Matchmaking_pb;
                Lobby_Member_Data_Update_pb* update_pb = new Lobby_Member_Data_Update_pb(update);

                mm->set_allocated_lobby_member_data_update(update_pb);
                resp_msg.set_allocated_matchmaking(mm);
                resp_msg.set_source_id(steam_id);

                send_to_all_members(resp_msg, pLobby);
            }

            member_it->second = update.datas();
            trigger_lobby_update(update.member_id(), (uint64)pLobby->infos.lobby_id());
        }
    }

    return true;
}

bool Steam_Matchmaking::on_lobby_member_promote(Network_Message_pb const& msg, Lobby_Member_Promote_pb const& promote)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "source_id: %llu", msg.source_id());
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    lobby_state_t* pLobby = get_lobby_by_id(promote.lobby_id());
    if (pLobby != nullptr)
    {
        pLobby->infos.set_owner(promote.member_id());
        trigger_lobby_update(promote.lobby_id(), (uint64)pLobby->infos.lobby_id());
    }

    return true;
}

bool Steam_Matchmaking::on_lobby_search(Network_Message_pb const& msg, Lobby_Search_pb const& search)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "source_id: %llu", msg.source_id());
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    Lobby_Search_Response_pb* response = new Lobby_Search_Response_pb;

    if (msg.app_id() == Settings::Inst().gameid.AppID())
    {
        std::vector<lobby_state_t*> lobbies = std::move(get_lobbies_from_filter(search));
        for (auto lobby : lobbies)
        {
            *response->add_lobbies() = lobby->infos;
        }
    }

    return send_lobby_search_response(msg.source_id(), response);
}

bool Steam_Matchmaking::on_lobby_search_response(Network_Message_pb const& msg, Lobby_Search_Response_pb const& response)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "source_id: %llu", msg.source_id());
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    if (_search_state.callback.get() != nullptr)
    {
        auto it = _search_state.peers.find(msg.source_id());
        if (it != _search_state.peers.end())
        {
            for (auto& lobby : response.lobbies())
            {
                lobby_state_t& ref_lobby = _lobbies[lobby.lobby_id()];
                if (!i_am_in_lobby(&ref_lobby))
                {// If I'm not in the lobby, set its infos
                    ref_lobby.infos = lobby;
                }
                _search_state.lobbies[lobby.lobby_id()] = &ref_lobby;

                if (_search_state.lobbies.size() >= _search_state.max_results)
                {
                    _search_state.peers.clear();
                    break;
                }
            }

            _search_state.peers.erase(it);
        }
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//                                 IRunCallback                                 //
///////////////////////////////////////////////////////////////////////////////
bool Steam_Matchmaking::CBRunFrame()
{
    //GLOBAL_LOCK();

    return false;
}

bool Steam_Matchmaking::RunNetwork(Network_Message_pb const& msg)
{
    switch (msg.messages_case())
    {
        case Network_Message_pb::MessagesCase::kMatchmaking:
        {
            Steam_Matchmaking_pb const& matchmaking_msg = msg.matchmaking();
            switch (matchmaking_msg.messages_case())
            {
                // Lobby related messages
                case Steam_Matchmaking_pb::MessagesCase::kLobbyInfosRequest    : return on_lobby_infos_request     (msg, matchmaking_msg.lobby_infos_request());
                case Steam_Matchmaking_pb::MessagesCase::kLobbyInfosResponse   : return on_lobby_infos_response    (msg, matchmaking_msg.lobby_infos_response());
                case Steam_Matchmaking_pb::MessagesCase::kLobbyJoinRequest     : return on_lobby_join_request      (msg, matchmaking_msg.lobby_join_request());
                case Steam_Matchmaking_pb::MessagesCase::kLobbyJoinResponse    : return on_lobby_join_response     (msg, matchmaking_msg.lobby_join_response());
                case Steam_Matchmaking_pb::MessagesCase::kLobbyInvite          : return on_lobby_invite            (msg, matchmaking_msg.lobby_invite());
                case Steam_Matchmaking_pb::MessagesCase::kLobbyUpdate          : return on_lobby_update            (msg, matchmaking_msg.lobby_update());
                case Steam_Matchmaking_pb::MessagesCase::kLobbyDataUpdate      : return on_lobby_data_update       (msg, matchmaking_msg.lobby_data_update());
                case Steam_Matchmaking_pb::MessagesCase::kLobbyChatMessage     : return on_lobby_chat_message      (msg, matchmaking_msg.lobby_chat_message());
                // Lobby Member related messages
                case Steam_Matchmaking_pb::MessagesCase::kLobbyMemberJoin      : return on_lobby_member_join       (msg, matchmaking_msg.lobby_member_join());
                case Steam_Matchmaking_pb::MessagesCase::kLobbyMemberLeave     : return on_lobby_member_leave      (msg, matchmaking_msg.lobby_member_leave());
                case Steam_Matchmaking_pb::MessagesCase::kLobbyMemberDataUpdate: return on_lobby_member_data_update(msg, matchmaking_msg.lobby_member_data_update());
                case Steam_Matchmaking_pb::MessagesCase::kLobbyMemberPromote   : return on_lobby_member_promote    (msg, matchmaking_msg.lobby_member_promote());
                // Lobby search related messages
                case Steam_Matchmaking_pb::MessagesCase::kLobbySearch          : return on_lobby_search            (msg, matchmaking_msg.lobby_search());
                case Steam_Matchmaking_pb::MessagesCase::kLobbySearchResponse  : return on_lobby_search_response   (msg, matchmaking_msg.lobby_search_response());

                default: APP_LOG(Log::LogLevel::WARN, "Message unhandled %d", matchmaking_msg.messages_case());
            }
        }
        break;
    }

    return false;
}

bool Steam_Matchmaking::RunCallbacks(pFrameResult_t res)
{
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    auto now = std::chrono::steady_clock::now();

    switch (res->ICallback())
    {
        case LobbyCreated_t::k_iCallback:
        {
            LobbyCreated_t& lc = res->GetCallback<LobbyCreated_t>();

            lobby_state_t* pLobby = get_lobby_by_id(lc.m_ulSteamIDLobby);
            if (pLobby == nullptr)
            {
                lc.m_eResult = k_EResultFail;
                lc.m_ulSteamIDLobby = 0;
            }
            else
            {
                // Don't call JoinLobby cause we need a 0ms timeout to trigger this
                pFrameResult_t result(new FrameResult);
                LobbyEnter_t& le = result->CreateCallback<LobbyEnter_t>(std::chrono::milliseconds(0));
                le.m_bLocked = true;
                le.m_EChatRoomEnterResponse = k_EChatRoomEnterResponseSuccess;
                le.m_rgfChatPermissions = 0;
                le.m_ulSteamIDLobby = lc.m_ulSteamIDLobby;

                result->done = true;
                _cb_manager->add_callback(this, result);

                if (pLobby->infos.lobby_type() != k_ELobbyTypeInvisible)
                {
                    auto me = GetSteam_Friends().myself();
                    me->second.infos.set_lobby_id(lc.m_ulSteamIDLobby);
                    me->second.infos.set_lobby_owner(steam_id);
                }

                trigger_lobby_update(lc.m_ulSteamIDLobby, (uint64)pLobby->infos.lobby_id());

                add_lobby_member(steam_id, pLobby);
            }

            res->done = true;
        }
        break;

        case LobbyEnter_t::k_iCallback:
        {
            LobbyEnter_t& le = res->GetCallback<LobbyEnter_t>();

            if ((now - res->created_time) > lobby_join_timeout)
            {
                le.m_EChatRoomEnterResponse = k_EChatRoomEnterResponseDoesntExist;
                
                res->done = true;
                break;
            }
        }
        break;

        case LobbyMatchList_t::k_iCallback:
        {
            if(_search_state.peers.empty() || (now - res->created_time) >= lobby_list_timeout)
            {
                LobbyMatchList_t& lml = res->GetCallback<LobbyMatchList_t>();
                lml.m_nLobbiesMatching = _search_state.lobbies.size();

                res->done = true;
                _search_state.callback.reset();
            }
        }
        break;

        case LobbyDataUpdate_t::k_iCallback:
        {
            LobbyDataUpdate_t& ldu = res->GetCallback<LobbyDataUpdate_t>();
            lobby_state_t* pLobby = get_lobby_by_id(ldu.m_ulSteamIDLobby);
            if (pLobby == nullptr && pLobby->request.callback == res)
            {
                if ((now - pLobby->request.request_start) >= lobby_request_timeout)
                {
                    ldu.m_bSuccess = 0;
                    res->done = true;
                    pLobby->request.callback.reset();
                }
            }
        }
        break;
    }
    return res->done;
}
