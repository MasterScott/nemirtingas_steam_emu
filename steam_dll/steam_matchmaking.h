/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"

struct lobby_request_t
{
    std::chrono::steady_clock::time_point request_start;
    pFrameResult_t callback;
};

struct lobby_state_t
{
    Lobby_Infos_pb infos;
    lobby_request_t request;
};

struct search_state_t
{
    pFrameResult_t callback;
    std::unordered_set<Network::peer_t> peers;
    nlohmann::fifo_map<uint64, lobby_state_t*> lobbies;
    Lobby_Search_pb search_pb;
    int32_t max_results;
};

struct join_state_t
{
    pFrameResult_t callback;
};

class LOCAL_API Steam_Matchmaking :
    public IRunCallback,
    public IRunNetwork,
    public ISteamMatchmaking002,
    public ISteamMatchmaking004,
    public ISteamMatchmaking006,
    public ISteamMatchmaking007,
    public ISteamMatchmaking008,
    public ISteamMatchmaking009
{
    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;

    std::chrono::steady_clock::time_point _last_hearbeat;

    search_state_t _search_state;
    join_state_t   _join_state;

    // Timeout on JoinLobby
    static constexpr auto lobby_join_timeout = std::chrono::seconds(10);
    // Lobby List timeout
    static constexpr auto lobby_list_timeout = std::chrono::seconds(1);
    // RequestLobbyData timeout
    static constexpr auto lobby_request_timeout = std::chrono::seconds(2);

public:
    std::recursive_mutex _local_mutex;

    //     lobby_id, lobby infos
    nlohmann::fifo_map<uint64, lobby_state_t> _lobbies;
    nlohmann::fifo_map<uint64, lobby_state_t*> _joined_lobbies;

    Steam_Matchmaking();
    virtual ~Steam_Matchmaking();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    virtual bool CBRunFrame();
    virtual bool RunNetwork(Network_Message_pb const& msg);
    virtual bool RunCallbacks(pFrameResult_t res);
    
    // Send Network messages
    bool send_to_owner(Network_Message_pb& msg, lobby_state_t* lobby);
    bool send_to_all_members(Network_Message_pb& msg, lobby_state_t* lobby);
    bool send_to_all_members_or_owner(Network_Message_pb& msg, lobby_state_t* lobby);
    bool send_lobby_infos_request(Lobby_Infos_Request_pb* request);
    bool send_lobby_infos_response(Network::peer_t const& peer_id, Lobby_Infos_Response_pb* response);
    bool send_lobby_join_request(Network::peer_t const& peer_id, Lobby_Join_Request_pb* request);
    bool send_lobby_join_response(Network::peer_t const& peer_id, Lobby_Join_Response_pb* response);
    bool send_lobby_invite(Network::peer_t const& peer_id, Lobby_Invite_pb* invite);
    bool send_lobby_update(lobby_state_t* lobby);
    bool send_lobby_data_update(lobby_state_t* lobby);
    bool send_lobby_chat_message(Lobby_Chat_Message_pb* message, lobby_state_t* lobby);

    bool send_lobby_member_join(Network::peer_t const& member_id, lobby_state_t* lobby);
    bool send_lobby_member_leave(Lobby_Member_Leave_pb* leave, lobby_state_t* lobby);
    bool send_lobby_member_data_update(Network::peer_t const& member_id, lobby_state_t* lobby);
    bool send_lobby_member_promote(Network::peer_t const& member_id, lobby_state_t* lobby);

    bool send_lobby_search(Lobby_Search_pb *search);
    bool send_lobby_search_response(Network::peer_t const& peer_id, Lobby_Search_Response_pb* response);

    // Receive Network messages
    bool on_peer_disconnect(Network_Message_pb const& msg, Network_Peer_Disconnect_pb const& conn);
    bool on_lobby_infos_request(Network_Message_pb const& msg, Lobby_Infos_Request_pb const& request);
    bool on_lobby_infos_response(Network_Message_pb const& msg, Lobby_Infos_Response_pb const& response);
    bool on_lobby_join_request(Network_Message_pb const& msg, Lobby_Join_Request_pb const& request);
    bool on_lobby_join_response(Network_Message_pb const& msg, Lobby_Join_Response_pb const& response);
    bool on_lobby_invite(Network_Message_pb const& msg, Lobby_Invite_pb const& invite);
    bool on_lobby_update(Network_Message_pb const& msg, Lobby_Update_pb const& update);
    bool on_lobby_data_update(Network_Message_pb const& msg, Lobby_Data_Update_pb const& update);
    bool on_lobby_chat_message(Network_Message_pb const& msg, Lobby_Chat_Message_pb const& message);

    bool on_lobby_member_join(Network_Message_pb const& msg, Lobby_Member_Join_pb const& join);
    bool on_lobby_member_leave(Network_Message_pb const& msg, Lobby_Member_Leave_pb const& leave);
    bool on_lobby_member_data_update(Network_Message_pb const& msg, Lobby_Member_Data_Update_pb const& update);
    bool on_lobby_member_promote(Network_Message_pb const& msg, Lobby_Member_Promote_pb const& promote);

    bool on_lobby_search(Network_Message_pb const& msg, Lobby_Search_pb const& search);
    bool on_lobby_search_response(Network_Message_pb const& msg, Lobby_Search_Response_pb const& response);

    // Class funcs
    void trigger_lobby_update(uint64 member_id, uint64 lobby_id, bool success = true);
    void trigger_chat_member(uint64 member_id, uint64 lobby_id, EChatMemberStateChange v);

    std::pair<uint32_t, uint32_t> get_invisible_visible_lobby_count();
    bool i_am_owner(lobby_state_t* lobby);
    bool i_am_in_lobby(lobby_state_t* lobby);

    bool is_lobby_member(uint64 steam_id, lobby_state_t* lobby);
    bool add_lobby_member(uint64 steam_id, lobby_state_t* lobby);
    bool remove_lobby_member(uint64 steam_id, lobby_state_t* lobby);

    lobby_state_t* get_lobby_by_id(uint64 lobby_id);
    google::protobuf::Map<std::string, std::string>::iterator find_lobbydata(google::protobuf::Map<std::string, std::string>& container, std::string key);
    std::vector<lobby_state_t*> get_lobbies_from_filter(Lobby_Search_pb const& search);

    // game server favorites storage
    // saves basic details about a multiplayer game server locally

    // returns the number of favorites servers the user has stored
    virtual int GetFavoriteGameCount();

    // returns the details of the game server
    // iGame is of range [0,GetFavoriteGameCount())
    // *pnIP, *pnConnPort are filled in the with IP:port of the game server
    // *punFlags specify whether the game server was stored as an explicit favorite or in the history of connections
    // *pRTime32LastPlayedOnServer is filled in the with the Unix time the favorite was added
    virtual bool GetFavoriteGame(int iGame, AppId_t* pnAppID, uint32* pnIP, uint16* pnConnPort, uint16* pnQueryPort, uint32* punFlags, uint32* pRTime32LastPlayedOnServer);

    // adds the game server to the local list; updates the time played of the server if it already exists in the list
    virtual int AddFavoriteGame(AppId_t nAppID, uint32 nIP, uint16 nConnPort, uint16 nQueryPort, uint32 unFlags, uint32 rTime32LastPlayedOnServer);

    // removes the game server from the local storage; returns true if one was removed
    virtual bool RemoveFavoriteGame(AppId_t nAppID, uint32 nIP, uint16 nConnPort, uint16 nQueryPort, uint32 unFlags);

    ///////
    // Game lobby functions

    // Get a list of relevant lobbies
    // this is an asynchronous request
    // results will be returned by LobbyMatchList_t callback & call result, with the number of lobbies found
    // this will never return lobbies that are full
    // to add more filter, the filter calls below need to be call before each and every RequestLobbyList() call
    // use the CCallResult<> object in steam_api.h to match the SteamAPICall_t call result to a function in an object, e.g.
    /*
        class CMyLobbyListManager
        {
            CCallResult<CMyLobbyListManager, LobbyMatchList_t> m_CallResultLobbyMatchList;
            void FindLobbies()
            {
                // SteamMatchmaking()->AddRequestLobbyListFilter*() functions would be called here, before RequestLobbyList()
                SteamAPICall_t hSteamAPICall = SteamMatchmaking()->RequestLobbyList();
                m_CallResultLobbyMatchList.Set( hSteamAPICall, this, &CMyLobbyListManager::OnLobbyMatchList );
            }

            void OnLobbyMatchList( LobbyMatchList_t *pLobbyMatchList, bool bIOFailure )
            {
                // lobby list has be retrieved from Steam back-end, use results
            }
        }
    */
    // 
    STEAM_CALL_RESULT(LobbyMatchList_t)
    virtual SteamAPICall_t RequestLobbyList();

    virtual bool RequestFriendsLobbies();

    // filters for lobbies
    // this needs to be called before RequestLobbyList() to take effect
    // these are cleared on each call to RequestLobbyList()
    virtual void AddRequestLobbyListFilter(const char* pchKeyToMatch, const char* pchValueToMatch);
    virtual void AddRequestLobbyListStringFilter(const char* pchKeyToMatch, const char* pchValueToMatch, ELobbyComparison eComparisonType);
    // numerical comparison
    virtual void AddRequestLobbyListNumericalFilter(const char* pchKeyToMatch, int nValueToMatch, int nComparisonType);
    virtual void AddRequestLobbyListNumericalFilter(const char* pchKeyToMatch, int nValueToMatch, ELobbyComparison eComparisonType);
    // sets RequestLobbyList() to only returns lobbies which aren't yet full - needs SetLobbyMemberLimit() called on the lobby to set an initial limit
    virtual void AddRequestLobbyListSlotsAvailableFilter();
    // returns results closest to the specified value. Multiple near filters can be added, with early filters taking precedence
    virtual void AddRequestLobbyListNearValueFilter(const char* pchKeyToMatch, int nValueToBeCloseTo);
    // returns only lobbies with the specified number of slots available
    virtual void AddRequestLobbyListFilterSlotsAvailable(int nSlotsAvailable);
    // sets the distance for which we should search for lobbies (based on users IP address to location map on the Steam backed)
    virtual void AddRequestLobbyListDistanceFilter(ELobbyDistanceFilter eLobbyDistanceFilter);
    // sets how many results to return, the lower the count the faster it is to download the lobby results & details to the client
    virtual void AddRequestLobbyListResultCountFilter(int cMaxResults);

    virtual void AddRequestLobbyListCompatibleMembersFilter(CSteamID steamIDLobby);

    // returns the CSteamID of a lobby, as retrieved by a RequestLobbyList call
    // should only be called after a LobbyMatchList_t callback is received
    // iLobby is of the range [0, LobbyMatchList_t::m_nLobbiesMatching)
    // the returned CSteamID::IsValid() will be false if iLobby is out of range
    virtual CSteamID GetLobbyByIndex(int iLobby);

    // Create a lobby on the Steam servers.
    // If private, then the lobby will not be returned by any RequestLobbyList() call; the CSteamID
    // of the lobby will need to be communicated via game channels or via InviteUserToLobby()
    // this is an asynchronous request
    // results will be returned by LobbyCreated_t callback and call result; lobby is joined & ready to use at this point
    // a LobbyEnter_t callback will also be received (since the local user is joining their own lobby)
    virtual void CreateLobby(bool bPrivate);
    virtual SteamAPICall_t CreateLobby(ELobbyType eLobbyType);
    STEAM_CALL_RESULT(LobbyCreated_t)
    virtual SteamAPICall_t CreateLobby(ELobbyType eLobbyType, int cMaxMembers);

    // Joins an existing lobby
    // this is an asynchronous request
    // results will be returned by LobbyEnter_t callback & call result, check m_EChatRoomEnterResponse to see if was successful
    // lobby metadata is available to use immediately on this call completing
    STEAM_CALL_RESULT(LobbyEnter_t)
    virtual SteamAPICall_t JoinLobby(CSteamID steamIDLobby);

    // Leave a lobby; this will take effect immediately on the client side
    // other users in the lobby will be notified by a LobbyChatUpdate_t callback
    virtual void LeaveLobby(CSteamID steamIDLobby);

    // Invite another user to the lobby
    // the target user will receive a LobbyInvite_t callback
    // will return true if the invite is successfully sent, whether or not the target responds
    // returns false if the local user is not connected to the Steam servers
    // if the other user clicks the join link, a GameLobbyJoinRequested_t will be posted if the user is in-game,
    // or if the game isn't running yet the game will be launched with the parameter +connect_lobby <64-bit lobby id>
    virtual bool InviteUserToLobby(CSteamID steamIDLobby, CSteamID steamIDInvitee);

    // Lobby iteration, for viewing details of users in a lobby
    // only accessible if the lobby user is a member of the specified lobby
    // persona information for other lobby members (name, avatar, etc.) will be asynchronously received
    // and accessible via ISteamFriends interface

    // returns the number of users in the specified lobby
    virtual int GetNumLobbyMembers(CSteamID steamIDLobby);
    // returns the CSteamID of a user in the lobby
    // iMember is of range [0,GetNumLobbyMembers())
    // note that the current user must be in a lobby to retrieve CSteamIDs of other users in that lobby
    virtual CSteamID GetLobbyMemberByIndex(CSteamID steamIDLobby, int iMember);

    // Get data associated with this lobby
    // takes a simple key, and returns the string associated with it
    // "" will be returned if no value is set, or if steamIDLobby is invalid
    virtual const char* GetLobbyData(CSteamID steamIDLobby, const char* pchKey);
    // Sets a key/value pair in the lobby metadata
    // each user in the lobby will be broadcast this new value, and any new users joining will receive any existing data
    // this can be used to set lobby names, map, etc.
    // to reset a key, just set it to ""
    // other users in the lobby will receive notification of the lobby data change via a LobbyDataUpdate_t callback
    virtual bool SetLobbyData(CSteamID steamIDLobby, const char* pchKey, const char* pchValue);

    // returns the number of metadata keys set on the specified lobby
    virtual int GetLobbyDataCount(CSteamID steamIDLobby);

    // returns a lobby metadata key/values pair by index, of range [0, GetLobbyDataCount())
    virtual bool GetLobbyDataByIndex(CSteamID steamIDLobby, int iLobbyData, char* pchKey, int cchKeyBufferSize, char* pchValue, int cchValueBufferSize);

    // removes a metadata key from the lobby
    virtual bool DeleteLobbyData(CSteamID steamIDLobby, const char* pchKey);

    // Gets per-user metadata for someone in this lobby
    virtual const char* GetLobbyMemberData(CSteamID steamIDLobby, CSteamID steamIDUser, const char* pchKey);
    // Sets per-user metadata (for the local user implicitly)
    virtual void SetLobbyMemberData(CSteamID steamIDLobby, const char* pchKey, const char* pchValue);

    // Broadcasts a chat message to the all the users in the lobby
    // users in the lobby (including the local user) will receive a LobbyChatMsg_t callback
    // returns true if the message is successfully sent
    // pvMsgBody can be binary or text data, up to 4k
    // if pvMsgBody is text, cubMsgBody should be strlen( text ) + 1, to include the null terminator
    virtual bool SendLobbyChatMsg(CSteamID steamIDLobby, const void* pvMsgBody, int cubMsgBody);
    // Get a chat message as specified in a LobbyChatMsg_t callback
    // iChatID is the LobbyChatMsg_t::m_iChatID value in the callback
    // *pSteamIDUser is filled in with the CSteamID of the member
    // *pvData is filled in with the message itself
    // return value is the number of bytes written into the buffer
    virtual int GetLobbyChatEntry(CSteamID steamIDLobby, int iChatID, STEAM_OUT_STRUCT() CSteamID* pSteamIDUser, void* pvData, int cubData, EChatEntryType* peChatEntryType);

    // Refreshes metadata for a lobby you're not necessarily in right now
    // you never do this for lobbies you're a member of, only if your
    // this will send down all the metadata associated with a lobby
    // this is an asynchronous call
    // returns false if the local user is not connected to the Steam servers
    // results will be returned by a LobbyDataUpdate_t callback
    // if the specified lobby doesn't exist, LobbyDataUpdate_t::m_bSuccess will be set to false
    virtual bool RequestLobbyData(CSteamID steamIDLobby);

    // sets the game server associated with the lobby
    // usually at this point, the users will join the specified game server
    // either the IP/Port or the steamID of the game server has to be valid, depending on how you want the clients to be able to connect
    virtual void SetLobbyGameServer(CSteamID steamIDLobby, uint32 unGameServerIP, uint16 unGameServerPort, CSteamID steamIDGameServer);
    // returns the details of a game server set in a lobby - returns false if there is no game server set, or that lobby doesn't exist
    virtual bool GetLobbyGameServer(CSteamID steamIDLobby, uint32* punGameServerIP, uint16* punGameServerPort, STEAM_OUT_STRUCT() CSteamID* psteamIDGameServer);

    // set the limit on the # of users who can join the lobby
    virtual bool SetLobbyMemberLimit(CSteamID steamIDLobby, int cMaxMembers);
    // returns the current limit on the # of users who can join the lobby; returns 0 if no limit is defined
    virtual int GetLobbyMemberLimit(CSteamID steamIDLobby);

    // updates which type of lobby it is
    // only lobbies that are k_ELobbyTypePublic or k_ELobbyTypeInvisible, and are set to joinable, will be returned by RequestLobbyList() calls
    virtual bool SetLobbyType(CSteamID steamIDLobby, ELobbyType eLobbyType);

    // sets whether or not a lobby is joinable - defaults to true for a new lobby
    // if set to false, no user can join, even if they are a friend or have been invited
    virtual bool SetLobbyJoinable(CSteamID steamIDLobby, bool bLobbyJoinable);

    // returns the current lobby owner
    // you must be a member of the lobby to access this
    // there always one lobby owner - if the current owner leaves, another user will become the owner
    // it is possible (bur rare) to join a lobby just as the owner is leaving, thus entering a lobby with self as the owner
    virtual CSteamID GetLobbyOwner(CSteamID steamIDLobby);

    // changes who the lobby owner is
    // you must be the lobby owner for this to succeed, and steamIDNewOwner must be in the lobby
    // after completion, the local user will no longer be the owner
    virtual bool SetLobbyOwner(CSteamID steamIDLobby, CSteamID steamIDNewOwner);

    // link two lobbies for the purposes of checking player compatibility
    // you must be the lobby owner of both lobbies
    virtual bool SetLinkedLobby(CSteamID steamIDLobby, CSteamID steamIDLobbyDependent);

#ifdef _PS3
    // changes who the lobby owner is
    // you must be the lobby owner for this to succeed, and steamIDNewOwner must be in the lobby
    // after completion, the local user will no longer be the owner
    virtual void CheckForPSNGameBootInvite(unsigned int iGameBootAttributes);
#endif
    STEAM_CALL_BACK(LobbyChatUpdate_t)
};
