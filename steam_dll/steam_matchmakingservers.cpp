/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_matchmakingservers.h"
#include "steam_client.h"
#include "settings.h"

constexpr decltype(Steam_MatchmakingServers::request_timeout) Steam_MatchmakingServers::request_timeout;
constexpr decltype(Steam_MatchmakingServers::query_timeout)   Steam_MatchmakingServers::query_timeout;

Steam_MatchmakingServers::Steam_MatchmakingServers():
    _query_handle(0)
{
}

Steam_MatchmakingServers::~Steam_MatchmakingServers()
{
    emu_deinit();
}

void Steam_MatchmakingServers::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock(_local_mutex, cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(cb_manager->_local_mutex, std::adopt_lock);

    _cb_manager = cb_manager;
    _network = network;

    _network->register_listener(this, Network_Message_pb::MessagesCase::kGameserver);
    _cb_manager->register_frame(this);
}

void Steam_MatchmakingServers::emu_deinit()
{
    TRACE_FUNC();
    if (_cb_manager != nullptr)
    {
        std::lock(_local_mutex, _cb_manager->_local_mutex);
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
        std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

        _cb_manager->unregister_frame(this);
    }

    if (_network != nullptr)
    {
        _network->unregister_listener(this, Network_Message_pb::MessagesCase::kNetwork);
    }

    {
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
        _network.reset();
        _cb_manager.reset();
    }
}

HServerQuery Steam_MatchmakingServers::create_query_handle()
{
    if (_query_handle == std::numeric_limits<HServerQuery>::max())
        _query_handle = 0;

    return _query_handle++;
}

// Request a new list of servers of a particular type.  These calls each correspond to one of the EMatchMakingType values.
// Each call allocates a new asynchronous request object.
// Request object must be released by calling ReleaseRequest( hServerListRequest )
HServerListRequest Steam_MatchmakingServers::RequestInternetServerList(AppId_t iApp, STEAM_ARRAY_COUNT(nFilters) MatchMakingKeyValuePair_t** ppchFilters, uint32 nFilters, ISteamMatchmakingServerListResponse* pRequestServersResponse)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return RequestLANServerList(iApp, pRequestServersResponse);
}

HServerListRequest Steam_MatchmakingServers::RequestLANServerList(AppId_t iApp, ISteamMatchmakingServerListResponse* pRequestServersResponse)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    server_list_request_t* request = new server_list_request_t;

    request->request_start = std::chrono::steady_clock::now();
    request->allow_add = true;
    request->canceled = false;
    request->released = false;
    request->response = pRequestServersResponse;
    _server_requests[request];

    Gameserver_List_Request_pb* req = new Gameserver_List_Request_pb;

    req->set_type(Gameserver_List_Request_pb_Type_LAN);
    req->set_list_handle(reinterpret_cast<uint64_t>(request));

    send_gameserver_list_request(req);

    return request;
}

HServerListRequest Steam_MatchmakingServers::RequestFriendsServerList(AppId_t iApp, STEAM_ARRAY_COUNT(nFilters) MatchMakingKeyValuePair_t** ppchFilters, uint32 nFilters, ISteamMatchmakingServerListResponse* pRequestServersResponse)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return RequestLANServerList(iApp, pRequestServersResponse);
}

HServerListRequest Steam_MatchmakingServers::RequestFavoritesServerList(AppId_t iApp, STEAM_ARRAY_COUNT(nFilters) MatchMakingKeyValuePair_t** ppchFilters, uint32 nFilters, ISteamMatchmakingServerListResponse* pRequestServersResponse)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return RequestLANServerList(iApp, pRequestServersResponse);
}

HServerListRequest Steam_MatchmakingServers::RequestHistoryServerList(AppId_t iApp, STEAM_ARRAY_COUNT(nFilters) MatchMakingKeyValuePair_t** ppchFilters, uint32 nFilters, ISteamMatchmakingServerListResponse* pRequestServersResponse)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return RequestLANServerList(iApp, pRequestServersResponse);
}

HServerListRequest Steam_MatchmakingServers::RequestSpectatorServerList(AppId_t iApp, STEAM_ARRAY_COUNT(nFilters) MatchMakingKeyValuePair_t** ppchFilters, uint32 nFilters, ISteamMatchmakingServerListResponse* pRequestServersResponse)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return RequestLANServerList(iApp, pRequestServersResponse);
}

// Releases the asynchronous request object and cancels any pending query on it if there's a pending query in progress.
// RefreshComplete callback is not posted when request is released.
void Steam_MatchmakingServers::ReleaseRequest(HServerListRequest _hServerListRequest)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto *hServerListRequest = reinterpret_cast<server_list_request_t*>(_hServerListRequest);

    auto it = _server_requests.find(hServerListRequest);
    if (it != _server_requests.end())
    {
        it->first->released = true;
        it->first->allow_add = false;
    }
}

/* the filter operation codes that go in the key part of MatchMakingKeyValuePair_t should be one of these:

    "map"
        - Server passes the filter if the server is playing the specified map.
    "gamedataand"
        - Server passes the filter if the server's game data (ISteamGameServer::SetGameData) contains all of the
        specified strings.  The value field is a comma-delimited list of strings to match.
    "gamedataor"
        - Server passes the filter if the server's game data (ISteamGameServer::SetGameData) contains at least one of the
        specified strings.  The value field is a comma-delimited list of strings to match.
    "gamedatanor"
        - Server passes the filter if the server's game data (ISteamGameServer::SetGameData) does not contain any
        of the specified strings.  The value field is a comma-delimited list of strings to check.
    "gametagsand"
        - Server passes the filter if the server's game tags (ISteamGameServer::SetGameTags) contains all
        of the specified strings.  The value field is a comma-delimited list of strings to check.
    "gametagsnor"
        - Server passes the filter if the server's game tags (ISteamGameServer::SetGameTags) does not contain any
        of the specified strings.  The value field is a comma-delimited list of strings to check.
    "and" (x1 && x2 && ... && xn)
    "or" (x1 || x2 || ... || xn)
    "nand" !(x1 && x2 && ... && xn)
    "nor" !(x1 || x2 || ... || xn)
        - Performs Boolean operation on the following filters.  The operand to this filter specifies
        the "size" of the Boolean inputs to the operation, in Key/value pairs.  (The keyvalue
        pairs must immediately follow, i.e. this is a prefix logical operator notation.)
        In the simplest case where Boolean expressions are not nested, this is simply
        the number of operands.

        For example, to match servers on a particular map or with a particular tag, would would
        use these filters.

            ( server.map == "cp_dustbowl" || server.gametags.contains("payload") )
            "or", "2"
            "map", "cp_dustbowl"
            "gametagsand", "payload"

        If logical inputs are nested, then the operand specifies the size of the entire
        "length" of its operands, not the number of immediate children.

            ( server.map == "cp_dustbowl" || ( server.gametags.contains("payload") && !server.gametags.contains("payloadrace") ) )
            "or", "4"
            "map", "cp_dustbowl"
            "and", "2"
            "gametagsand", "payload"
            "gametagsnor", "payloadrace"

        Unary NOT can be achieved using either "nand" or "nor" with a single operand.

    "addr"
        - Server passes the filter if the server's query address matches the specified IP or IP:port.
    "gameaddr"
        - Server passes the filter if the server's game address matches the specified IP or IP:port.

    The following filter operations ignore the "value" part of MatchMakingKeyValuePair_t

    "dedicated"
        - Server passes the filter if it passed true to SetDedicatedServer.
    "secure"
        - Server passes the filter if the server is VAC-enabled.
    "notfull"
        - Server passes the filter if the player count is less than the reported max player count.
    "hasplayers"
        - Server passes the filter if the player count is greater than zero.
    "noplayers"
        - Server passes the filter if it doesn't have any players.
    "linux"
        - Server passes the filter if it's a linux server
*/

//gameserveritem_t* Steam_MatchmakingServers::GetServerDetails(EMatchMakingType eType, int iServer)
//{
//    APP_LOG(Log::LogLevel::TRACE, "");
//    return GetServerDetails(_server_requests.front(), iServer);
//}

gameserveritem_t* Steam_MatchmakingServers::GetServerDetails(HServerListRequest _hRequest, int iServer)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto* hRequest = reinterpret_cast<server_list_request_t*>(_hRequest);

    auto it = _server_requests.find(hRequest);
    if (it != _server_requests.end())
    {

        return &(it->second[iServer].gs_item);
    }

    return nullptr;
}

// Cancel an request which is operation on the given list type.  You should call this to cancel
// any in-progress requests before destructing a callback object that may have been passed 
// to one of the above list request calls.  Not doing so may result in a crash when a callback
// occurs on the destructed object.
// Canceling a query does not release the allocated request handle.
// The request handle must be released using ReleaseRequest( hRequest )
//void Steam_MatchmakingServers::CancelQuery(EMatchMakingType eType)
//{
//    APP_LOG(Log::LogLevel::TRACE, "");
//    CancelQuery(_server_requests.front());
//}

void Steam_MatchmakingServers::CancelQuery(HServerListRequest _hRequest)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto* hRequest = reinterpret_cast<server_list_request_t*>(_hRequest);

    auto it = _server_requests.find(hRequest);
    if (it != _server_requests.end())
    {
        it->first->canceled = true;
    }
}

// Ping every server in your list again but don't update the list of servers
// Query callback installed when the server list was requested will be used
// again to post notifications and RefreshComplete, so the callback must remain
// valid until another RefreshComplete is called on it or the request
// is released with ReleaseRequest( hRequest )
//void Steam_MatchmakingServers::RefreshQuery(EMatchMakingType eType)
//{
//    APP_LOG(Log::LogLevel::TRACE, "");
//    RefreshQuery(_server_requests.front());
//}

void Steam_MatchmakingServers::RefreshQuery(HServerListRequest _hRequest)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto* hRequest = reinterpret_cast<server_list_request_t*>(_hRequest);

    auto it = _server_requests.find(hRequest);
    if (it != _server_requests.end())
    {
        for (auto& gs : it->second)
        {
            Gameserver_Infos_Request_pb* req = new Gameserver_Infos_Request_pb;

            req->set_list_handle(reinterpret_cast<uint64_t>(hRequest));

            gs.is_refreshing = send_gameserver_infos_request(gs.owner_id, req);
        }
    }
}

// Returns true if the list is currently refreshing its server list
//bool Steam_MatchmakingServers::IsRefreshing(EMatchMakingType eType)
//{
//    APP_LOG(Log::LogLevel::TRACE, "");
//    return IsRefreshing(_server_requests.front());
//}

bool Steam_MatchmakingServers::IsRefreshing(HServerListRequest _hRequest)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto* hRequest = reinterpret_cast<server_list_request_t*>(_hRequest);

    auto it = _server_requests.find(hRequest);
    if (it != _server_requests.end())
    {
        auto server_it = std::find_if(it->second.begin(), it->second.end(), []( mm_server_infos_t &infos)
        {// Try to find a server currently refreshing
            return infos.is_refreshing;
        });

        if (server_it != it->second.end())
            return true;
    }

    return false;
}

// How many servers in the given list, GetServerDetails above takes 0... GetServerCount() - 1
//int Steam_MatchmakingServers::GetServerCount(EMatchMakingType eType)
//{
//    APP_LOG(Log::LogLevel::TRACE, "");
//    return GetServerCount(_server_requests.front());
//}

int Steam_MatchmakingServers::GetServerCount(HServerListRequest _hRequest)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto* hRequest = reinterpret_cast<server_list_request_t*>(_hRequest);

    auto it = _server_requests.find(hRequest);
    if (it != _server_requests.end())
    {
        return static_cast<int>(it->second.size());
    }

    return 0;
}

// Refresh a single server inside of a query (rather than all the servers )
//void Steam_MatchmakingServers::RefreshServer(EMatchMakingType eType, int iServer)
//{
//    APP_LOG(Log::LogLevel::TRACE, "");
//    RefreshServer(_server_requests.front(), iServer);
//}

void Steam_MatchmakingServers::RefreshServer(HServerListRequest _hRequest, int iServer)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto* hRequest = reinterpret_cast<server_list_request_t*>(_hRequest);

    auto it = _server_requests.find(hRequest);
    if (it != _server_requests.end())
    {
        if (it->second.size() > iServer && !it->second[iServer].is_refreshing)
        {
            auto& gs = it->second[iServer];
            gs.is_refreshing = true;
            
            Gameserver_Infos_Request_pb* req = new Gameserver_Infos_Request_pb;
            req->set_list_handle(reinterpret_cast<uint64_t>(_hRequest));
            req->set_ping_start(static_cast<uint64_t>(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count()));

            send_gameserver_infos_request(gs.owner_id, req);
        }
    }
}


//-----------------------------------------------------------------------------
// Queries to individual servers directly via IP/Port
//-----------------------------------------------------------------------------

// Request updated ping time and other details from a single server
HServerQuery Steam_MatchmakingServers::PingServer(uint32 unIP, uint16 usPort, ISteamMatchmakingPingResponse* pRequestServersResponse)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto h = create_query_handle();

    auto& d = _server_queries[h];
    d.type = mm_server_query_t::e_type_ping;
    d.addr.set_ip(unIP);
    d.addr.set_port(usPort);

    return h;
}

// Request the list of players currently playing on a server
HServerQuery Steam_MatchmakingServers::PlayerDetails(uint32 unIP, uint16 usPort, ISteamMatchmakingPlayersResponse* pRequestServersResponse)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto h = create_query_handle();

    auto& d = _server_queries[h];
    d.players = pRequestServersResponse;
    d.type = mm_server_query_t::e_type_players;
    d.addr.set_ip(unIP);
    d.addr.set_port(usPort);

    return h;
}

// Request the list of rules that the server is running (See ISteamGameServer::SetKeyValue() to set the rules server side)
HServerQuery Steam_MatchmakingServers::ServerRules(uint32 unIP, uint16 usPort, ISteamMatchmakingRulesResponse* pRequestServersResponse)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto h = create_query_handle();

    auto& d = _server_queries[h];
    d.rules = pRequestServersResponse;
    d.type = mm_server_query_t::e_type_rules;
    d.addr.set_ip(unIP);
    d.addr.set_port(usPort);

    return h;
}

// Cancel an outstanding Ping/Players/Rules query from above.  You should call this to cancel
// any in-progress requests before destructing a callback object that may have been passed 
// to one of the above calls to avoid crashing when callbacks occur.
void Steam_MatchmakingServers::CancelServerQuery(HServerQuery hServerQuery)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto it = _server_queries.find(hServerQuery);
    if (it != _server_queries.end())
        _server_queries.erase(it);
}

///////////////////////////////////////////////////////////////////////////////
//                           Network Send messages                           //
///////////////////////////////////////////////////////////////////////////////
bool Steam_MatchmakingServers::send_gameserver_list_request(Gameserver_List_Request_pb* req)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Gameserver_pb* gameserver = new Steam_Gameserver_pb;

    gameserver->set_allocated_list_req(req);

    msg.set_allocated_gameserver(gameserver);

    msg.set_source_id(steam_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return !_network->ReliableSendToAllPeers(msg).empty();
}

bool Steam_MatchmakingServers::send_gameserver_infos_request(uint64 remote_id, Gameserver_Infos_Request_pb* req)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Gameserver_pb* gameserver = new Steam_Gameserver_pb;

    gameserver->set_allocated_server_infos_req(req);

    msg.set_allocated_gameserver(gameserver);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

///////////////////////////////////////////////////////////////////////////////
//                          Network Receive messages                         //
///////////////////////////////////////////////////////////////////////////////
bool Steam_MatchmakingServers::on_gameserver_list_response(Network_Message_pb const& msg, Gameserver_List_Response_pb const& resp)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    Gameserver_Infos_Request_pb* req = new Gameserver_Infos_Request_pb;

    req->set_list_handle(resp.list_handle());
    req->set_ping_start(static_cast<uint64_t>(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count()));

    return send_gameserver_infos_request(msg.source_id(), req);
}

bool Steam_MatchmakingServers::on_gameserver_infos_response(Network_Message_pb const& msg, Gameserver_Infos_Response_pb const& serv_infos)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    server_list_request_t* request = reinterpret_cast<server_list_request_t*>(serv_infos.list_handle());

    auto request_it = _server_requests.find(request);
    if (request_it != _server_requests.end())
    {   
        auto const& gs_infos = serv_infos.server_infos();

        auto gs_it = std::find_if(request_it->second.begin(), request_it->second.end(), [&gs_infos](mm_server_infos_t const& serv_infos)
        {
            return serv_infos.gs_item.m_steamID == uint64(gs_infos.gameserver_id());
        });

        if (gs_it != request_it->second.end())
        {
            if (!gs_it->is_refreshing)
                return false;
        }
        else if(!request_it->first->allow_add)
        {
            return false;
        }

        mm_server_infos_t server_infos;

        server_infos.owner_id = msg.source_id();
        server_infos.gs_item.m_bDoNotRefresh = false;
        server_infos.gs_item.m_bHadSuccessfulResponse = true;
        server_infos.gs_item.m_bPassword = gs_infos.password();
        server_infos.gs_item.m_bSecure = gs_infos.secured();
        server_infos.gs_item.m_nAppID = gs_infos.appid();
        server_infos.gs_item.m_nBotPlayers = gs_infos.bots();
        server_infos.gs_item.m_NetAdr.SetIP(gs_infos.ip());

        if (server_infos.gs_item.m_NetAdr.GetIP() == 0)
        {
            auto const& addr = _network->get_steamid_addr(msg.source_id());

            server_infos.gs_item.m_NetAdr.SetIP(addr.get_ip());
        }

        server_infos.gs_item.m_NetAdr.SetConnectionPort(gs_infos.port());
        server_infos.gs_item.m_NetAdr.SetQueryPort(gs_infos.query_port());
        server_infos.gs_item.m_nMaxPlayers = gs_infos.max_players();

        uint64_t ping = static_cast<uint64_t>(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count()) - serv_infos.ping_start();

        server_infos.gs_item.m_nPing = static_cast<int>(ping);
        server_infos.gs_item.m_nPlayers = gs_infos.players_infos_size();
        server_infos.gs_item.m_nServerVersion = gs_infos.version();
        server_infos.gs_item.m_steamID = uint64(gs_infos.gameserver_id());
        strncpy(server_infos.gs_item.m_szGameDescription, gs_infos.description().c_str(), k_cbMaxGameServerGameDescription);
        server_infos.gs_item.m_szGameDescription[k_cbMaxGameServerGameDescription - 1] = 0;

        strncpy(server_infos.gs_item.m_szGameDir, gs_infos.gamedir().c_str(), k_cbMaxGameServerGameDir);
        server_infos.gs_item.m_szGameDir[k_cbMaxGameServerGameDir - 1] = 0;

        strncpy(server_infos.gs_item.m_szGameTags, gs_infos.gametags().c_str(), k_cbMaxGameServerTags);
        server_infos.gs_item.m_szGameTags[k_cbMaxGameServerTags - 1] = 0;

        strncpy(server_infos.gs_item.m_szMap, gs_infos.map().c_str(), k_cbMaxGameServerMapName);
        server_infos.gs_item.m_szMap[k_cbMaxGameServerMapName - 1] = 0;

        server_infos.gs_item.m_ulTimeLastPlayed = 0;

        server_infos.is_refreshing = false;

        int server_index = -1;

        // If the gameserver already exists and is refreshing
        if (gs_it != request_it->second.end())
        {// Refresh infos
            std::swap(*gs_it, server_infos);
            server_index = std::distance(request_it->second.begin(), gs_it);
        }
        else
        {// Else add the new server
            server_index = request_it->second.size();
            request_it->second.emplace_back(std::move(server_infos));
            request_it->second.rbegin()->is_refreshing = false;
        }

        // Refresh players infos
        request_it->second.rbegin()->players.clear();
        for (auto& pinfos : gs_infos.players_infos())
            request_it->second.rbegin()->players[pinfos.first] = pinfos.second;

        if (server_index != -1 && request_it->first->response != nullptr)
            request_it->first->response->ServerResponded(request_it->first, server_index);

        return true;
    }

    return false;
}

///////////////////////////////////////////////////////////////////////////////
//                                 IRunCallback                                 //
///////////////////////////////////////////////////////////////////////////////
bool Steam_MatchmakingServers::CBRunFrame()
{
    //LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto now = std::chrono::steady_clock::now();

    for (auto req_it = _server_requests.begin(); req_it != _server_requests.end();)
    {
        if (req_it->first->released)
        {
            delete req_it->first;
            req_it = _server_requests.erase(req_it);
            continue;
        }
        if (!req_it->first->canceled)
        {
            if ((now - req_it->first->request_start) > request_timeout)
            {
                if (req_it->first->allow_add && req_it->first->response != nullptr)
                    req_it->first->response->RefreshComplete(req_it->first, EMatchMakingServerResponse::eServerResponded);
                req_it->first->allow_add = false;
            }
            else if (req_it->first->allow_add && (now - req_it->first->last_query) > std::chrono::seconds(2))
            {// Every 2 seconds, resend a query to list servers, then after timeout, only refresh will be available
                req_it->first->last_query = std::chrono::steady_clock::now();

                Gameserver_List_Request_pb* list_req = new Gameserver_List_Request_pb;

                list_req->set_type(Gameserver_List_Request_pb_Type_LAN);
                list_req->set_list_handle(reinterpret_cast<uint64_t>(req_it->first));

                send_gameserver_list_request(list_req);
            }
        }
        ++req_it;
    }

    for(auto req_it = _server_queries.begin(); req_it != _server_queries.end();)
    {
        auto& query = req_it->second;

        if (query.status == mm_server_query_t::e_status_done)
            continue;

        if (query.status != mm_server_query_t::e_status_none && (std::chrono::steady_clock::now() - query.request_start) > query_timeout)
        {
            query.request_failed();
            req_it = _server_queries.erase(req_it);
            continue;
        }

        switch (query.status)
        {
            case mm_server_query_t::e_status_none:
            {// Status is none, so send the challenge request
                if(query.type != mm_server_query_t::e_type_none)
                {
                    std::vector<uint8_t> buffer;
                    switch (query.type)
                    {
                        case mm_server_query_t::e_type_ping:
                            buffer = std::move(Source_Query::create_source_ping(query.challenge));
                            break;

                        case mm_server_query_t::e_type_players:
                            buffer = std::move(Source_Query::create_source_players(query.challenge));
                            break;

                        case mm_server_query_t::e_type_rules:
                            buffer = std::move(Source_Query::create_source_rules(query.challenge));
                            break;
                        default: break;
                    }

                    if (query.conn.sendto(query.addr, buffer.data(), buffer.size()) != buffer.size())
                    {
                        query.request_failed();
                        req_it = _server_queries.erase(req_it);
                        continue;
                    }
                    else
                    {
                        query.status = mm_server_query_t::e_status_challenge;
                        query.request_start = std::chrono::steady_clock::now();
                    }
                }
            }
            break;

            case mm_server_query_t::e_status_challenge:
            {// Status is challenge, try to get the challenge_id
                std::vector<uint8_t> buffer(2048);
                PortableAPI::ipv4_addr addr;
                size_t len = query.conn.recvfrom(addr, buffer.data(), buffer.size());
                if (len != 0)
                {
                    buffer.resize(len);
                    query.challenge = Source_Query::handle_challenge(buffer.data(), buffer.size());
                    if (query.challenge == 0xFFFFFFFF)
                    {
                        query.request_failed();
                        req_it = _server_queries.erase(req_it);
                        continue;
                    }
                    else
                    {
                        switch (query.type)
                        {
                            case mm_server_query_t::e_type_ping:
                                break;

                            case mm_server_query_t::e_type_players:
                                buffer = std::move(Source_Query::create_source_players(query.challenge));
                                break;

                            case mm_server_query_t::e_type_rules:
                                buffer = std::move(Source_Query::create_source_rules(query.challenge));
                                break;
                            default: break;
                        }

                        if (query.conn.sendto(query.addr, buffer.data(), buffer.size()) != buffer.size())
                        {
                            query.request_failed();
                            req_it = _server_queries.erase(req_it);
                            continue;
                        }
                        else
                        {
                            query.status = mm_server_query_t::e_status_info;
                            query.request_start = std::chrono::steady_clock::now();
                        }
                    }
                }
            }
            break;

            case mm_server_query_t::e_status_info:
            {// Status is info, get the query infos
                std::vector<uint8_t> buffer(2048);
                PortableAPI::ipv4_addr addr;
                size_t len = query.conn.recvfrom(addr, buffer.data(), buffer.size());
                if (len != -1)
                {
                    buffer.resize(len);
                    switch (query.type)
                    {
                        case mm_server_query_t::e_type_ping:
                        {
                        }
                        break;

                        case mm_server_query_t::e_type_players:
                        {
                            std::vector<query_player_infos_t> players = std::move(Source_Query::handle_source_players(buffer.data(), buffer.size()));
                            if (query.players != nullptr)
                            {
                                float now = static_cast<float>(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now().time_since_epoch()).count());
                                for (auto& p : players)
                                {
                                    query.players->AddPlayerToList(p.name.c_str(), p.score, now-p.join_time);
                                }
                                query.players->PlayersRefreshComplete();
                            }
                        }
                        break;

                        case mm_server_query_t::e_type_rules:
                        {
                            std::map<std::string, std::string> rules = std::move(Source_Query::handle_source_rules(buffer.data(), buffer.size()));
                            if (query.rules != nullptr)
                            {
                                float now = static_cast<float>(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now().time_since_epoch()).count());
                                for (auto& p : rules)
                                {
                                    query.rules->RulesResponded(p.first.c_str(), p.second.c_str());
                                }
                                query.rules->RulesRefreshComplete();
                            }
                        }
                        break;
                    }

                    query.status = mm_server_query_t::e_status_done;
                    req_it = _server_queries.erase(req_it);
                    continue;
                }
            }
            break;
            default: break;
        }
        ++req_it;
    }

    return true;
}

bool Steam_MatchmakingServers::RunNetwork(Network_Message_pb const& msg)
{
    Steam_Gameserver_pb const& gameserver_msg = msg.gameserver();

    switch (gameserver_msg.messages_case())
    {
        //case Steam_MatchmakingServers_pb::MessagesCase::kListReq: return on_matchmakingservers_list_request(msg, mm_serv.list_req());
        case Steam_Gameserver_pb::MessagesCase::kListResp: return on_gameserver_list_response(msg, gameserver_msg.list_resp());
        //case Steam_MatchmakingServers_pb::MessagesCase::kServerInfosReq: return on_matchmakingservers_server_infos_request(msg, mm_serv.server_infos_req());
        case Steam_Gameserver_pb::MessagesCase::kServerInfosResp: return on_gameserver_infos_response(msg, gameserver_msg.server_infos_resp());
        default: APP_LOG(Log::LogLevel::WARN, "Message unhandled %d", gameserver_msg.messages_case());
    }

    return false;
}

bool Steam_MatchmakingServers::RunCallbacks(pFrameResult_t res)
{
    return res->done;
}
