/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"

struct server_list_request_t
{
    bool released;
    bool allow_add;
    bool canceled;
    std::chrono::steady_clock::time_point request_start;
    std::chrono::steady_clock::time_point last_query;
    std::vector<MatchMakingKeyValuePair_t> filters;
    ISteamMatchmakingServerListResponse* response;
};

struct mm_server_infos_t
{
    gameserveritem_t gs_item;
    bool is_refreshing;
    uint64 owner_id;
    std::map<uint64, Gameserver_player_infos_pb> players;
};

struct mm_server_query_t
{
    enum
    {
        e_type_none,
        e_type_ping,
        e_type_players,
        e_type_rules
    }type;
    union
    {
        ISteamMatchmakingPingResponse* ping;
        ISteamMatchmakingPlayersResponse* players;
        ISteamMatchmakingRulesResponse* rules;
    };
    enum
    {
        e_status_none,
        e_status_challenge,
        e_status_info,
        e_status_done
    } status;
    PortableAPI::udp_socket conn;
    PortableAPI::ipv4_addr addr;
    uint32_t challenge;
    std::chrono::steady_clock::time_point request_start;

    mm_server_query_t() :
        type(e_type_none),
        ping(nullptr),
        status(e_status_none),
        challenge(0xFFFFFFFF)
    {
        conn.set_nonblocking();
    }

    void request_failed()
    {
        switch (type)
        {
            case mm_server_query_t::e_type_ping:
                if (ping != nullptr)
                    ping->ServerFailedToRespond();
                break;

            case mm_server_query_t::e_type_players:
                if (players != nullptr)
                    players->PlayersFailedToRespond();
                break;

            case mm_server_query_t::e_type_rules:
                if (rules != nullptr)
                    rules->RulesFailedToRespond();
                break;
            default: break;
        }
        status = mm_server_query_t::e_status_done;
        conn.close();
    }
};

class LOCAL_API Steam_MatchmakingServers :
    public IRunCallback,
    public IRunNetwork,
    //public ISteamMatchmakingServers001,
    public ISteamMatchmakingServers002
{
    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;

    std::unordered_map<server_list_request_t*, std::vector<mm_server_infos_t>> _server_requests;
    std::unordered_map<HServerQuery, mm_server_query_t> _server_queries;

    static constexpr auto request_timeout = std::chrono::seconds(5);
    static constexpr auto query_timeout   = std::chrono::seconds(3);

    HServerQuery _query_handle;

public:
    std::recursive_mutex _local_mutex;

    Steam_MatchmakingServers();
    virtual ~Steam_MatchmakingServers();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    HServerQuery create_query_handle();

    virtual bool CBRunFrame();
    virtual bool RunNetwork(Network_Message_pb const& msg);
    virtual bool RunCallbacks(pFrameResult_t res);

    // Send Network messages
    bool send_gameserver_list_request(Gameserver_List_Request_pb* req_list);
    bool send_gameserver_infos_request(uint64 remote_id, Gameserver_Infos_Request_pb* req);
    // Receive Network messages
    bool on_gameserver_list_response(Network_Message_pb const& msg, Gameserver_List_Response_pb const& resp);
    bool on_gameserver_infos_response(Network_Message_pb const& msg, Gameserver_Infos_Response_pb const& serv_infos);

    // Request a new list of servers of a particular type.  These calls each correspond to one of the EMatchMakingType values.
    // Each call allocates a new asynchronous request object.
    // Request object must be released by calling ReleaseRequest( hServerListRequest )
    virtual HServerListRequest RequestInternetServerList(AppId_t iApp, STEAM_ARRAY_COUNT(nFilters) MatchMakingKeyValuePair_t** ppchFilters, uint32 nFilters, ISteamMatchmakingServerListResponse* pRequestServersResponse);
    virtual HServerListRequest RequestLANServerList(AppId_t iApp, ISteamMatchmakingServerListResponse* pRequestServersResponse);
    virtual HServerListRequest RequestFriendsServerList(AppId_t iApp, STEAM_ARRAY_COUNT(nFilters) MatchMakingKeyValuePair_t** ppchFilters, uint32 nFilters, ISteamMatchmakingServerListResponse* pRequestServersResponse);
    virtual HServerListRequest RequestFavoritesServerList(AppId_t iApp, STEAM_ARRAY_COUNT(nFilters) MatchMakingKeyValuePair_t** ppchFilters, uint32 nFilters, ISteamMatchmakingServerListResponse* pRequestServersResponse);
    virtual HServerListRequest RequestHistoryServerList(AppId_t iApp, STEAM_ARRAY_COUNT(nFilters) MatchMakingKeyValuePair_t** ppchFilters, uint32 nFilters, ISteamMatchmakingServerListResponse* pRequestServersResponse);
    virtual HServerListRequest RequestSpectatorServerList(AppId_t iApp, STEAM_ARRAY_COUNT(nFilters) MatchMakingKeyValuePair_t** ppchFilters, uint32 nFilters, ISteamMatchmakingServerListResponse* pRequestServersResponse);

    // Releases the asynchronous request object and cancels any pending query on it if there's a pending query in progress.
    // RefreshComplete callback is not posted when request is released.
    virtual void ReleaseRequest(HServerListRequest hServerListRequest);

    /* the filter operation codes that go in the key part of MatchMakingKeyValuePair_t should be one of these:

        "map"
            - Server passes the filter if the server is playing the specified map.
        "gamedataand"
            - Server passes the filter if the server's game data (ISteamGameServer::SetGameData) contains all of the
            specified strings.  The value field is a comma-delimited list of strings to match.
        "gamedataor"
            - Server passes the filter if the server's game data (ISteamGameServer::SetGameData) contains at least one of the
            specified strings.  The value field is a comma-delimited list of strings to match.
        "gamedatanor"
            - Server passes the filter if the server's game data (ISteamGameServer::SetGameData) does not contain any
            of the specified strings.  The value field is a comma-delimited list of strings to check.
        "gametagsand"
            - Server passes the filter if the server's game tags (ISteamGameServer::SetGameTags) contains all
            of the specified strings.  The value field is a comma-delimited list of strings to check.
        "gametagsnor"
            - Server passes the filter if the server's game tags (ISteamGameServer::SetGameTags) does not contain any
            of the specified strings.  The value field is a comma-delimited list of strings to check.
        "and" (x1 && x2 && ... && xn)
        "or" (x1 || x2 || ... || xn)
        "nand" !(x1 && x2 && ... && xn)
        "nor" !(x1 || x2 || ... || xn)
            - Performs Boolean operation on the following filters.  The operand to this filter specifies
            the "size" of the Boolean inputs to the operation, in Key/value pairs.  (The keyvalue
            pairs must immediately follow, i.e. this is a prefix logical operator notation.)
            In the simplest case where Boolean expressions are not nested, this is simply
            the number of operands.

            For example, to match servers on a particular map or with a particular tag, would would
            use these filters.

                ( server.map == "cp_dustbowl" || server.gametags.contains("payload") )
                "or", "2"
                "map", "cp_dustbowl"
                "gametagsand", "payload"

            If logical inputs are nested, then the operand specifies the size of the entire
            "length" of its operands, not the number of immediate children.

                ( server.map == "cp_dustbowl" || ( server.gametags.contains("payload") && !server.gametags.contains("payloadrace") ) )
                "or", "4"
                "map", "cp_dustbowl"
                "and", "2"
                "gametagsand", "payload"
                "gametagsnor", "payloadrace"

            Unary NOT can be achieved using either "nand" or "nor" with a single operand.

        "addr"
            - Server passes the filter if the server's query address matches the specified IP or IP:port.
        "gameaddr"
            - Server passes the filter if the server's game address matches the specified IP or IP:port.

        The following filter operations ignore the "value" part of MatchMakingKeyValuePair_t

        "dedicated"
            - Server passes the filter if it passed true to SetDedicatedServer.
        "secure"
            - Server passes the filter if the server is VAC-enabled.
        "notfull"
            - Server passes the filter if the player count is less than the reported max player count.
        "hasplayers"
            - Server passes the filter if the player count is greater than zero.
        "noplayers"
            - Server passes the filter if it doesn't have any players.
        "linux"
            - Server passes the filter if it's a linux server
    */

    // Get details on a given server in the list, you can get the valid range of index
    // values by calling GetServerCount().  You will also receive index values in 
    // ISteamMatchmakingServerListResponse::ServerResponded() callbacks
    //virtual gameserveritem_t* GetServerDetails(EMatchMakingType eType, int iServer);
    virtual gameserveritem_t* GetServerDetails(HServerListRequest hRequest, int iServer);

    // Cancel an request which is operation on the given list type.  You should call this to cancel
    // any in-progress requests before destructing a callback object that may have been passed 
    // to one of the above list request calls.  Not doing so may result in a crash when a callback
    // occurs on the destructed object.
    // Canceling a query does not release the allocated request handle.
    // The request handle must be released using ReleaseRequest( hRequest )
    //virtual void CancelQuery(EMatchMakingType eType);
    virtual void CancelQuery(HServerListRequest hRequest);

    // Ping every server in your list again but don't update the list of servers
    // Query callback installed when the server list was requested will be used
    // again to post notifications and RefreshComplete, so the callback must remain
    // valid until another RefreshComplete is called on it or the request
    // is released with ReleaseRequest( hRequest )
    //virtual void RefreshQuery(EMatchMakingType eType);
    virtual void RefreshQuery(HServerListRequest hRequest);

    // Returns true if the list is currently refreshing its server list
    //virtual bool IsRefreshing(EMatchMakingType eType);
    virtual bool IsRefreshing(HServerListRequest hRequest);

    // How many servers in the given list, GetServerDetails above takes 0... GetServerCount() - 1
    //virtual int GetServerCount(EMatchMakingType eType);
    virtual int GetServerCount(HServerListRequest hRequest);

    // Refresh a single server inside of a query (rather than all the servers )
    //virtual void RefreshServer(EMatchMakingType eType, int iServer);
    virtual void RefreshServer(HServerListRequest hRequest, int iServer);


    //-----------------------------------------------------------------------------
    // Queries to individual servers directly via IP/Port
    //-----------------------------------------------------------------------------

    // Request updated ping time and other details from a single server
    virtual HServerQuery PingServer(uint32 unIP, uint16 usPort, ISteamMatchmakingPingResponse* pRequestServersResponse);

    // Request the list of players currently playing on a server
    virtual HServerQuery PlayerDetails(uint32 unIP, uint16 usPort, ISteamMatchmakingPlayersResponse* pRequestServersResponse);

    // Request the list of rules that the server is running (See ISteamGameServer::SetKeyValue() to set the rules server side)
    virtual HServerQuery ServerRules(uint32 unIP, uint16 usPort, ISteamMatchmakingRulesResponse* pRequestServersResponse);

    // Cancel an outstanding Ping/Players/Rules query from above.  You should call this to cancel
    // any in-progress requests before destructing a callback object that may have been passed 
    // to one of the above calls to avoid crashing when callbacks occur.
    virtual void CancelServerQuery(HServerQuery hServerQuery);
};
