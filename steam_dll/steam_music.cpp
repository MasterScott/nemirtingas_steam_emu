/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_music.h"

Steam_Music::Steam_Music()
{
}

Steam_Music::~Steam_Music()
{
    emu_deinit();
}

void Steam_Music::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _cb_manager = cb_manager;
    _network = network;
}

void Steam_Music::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _network.reset();
    _cb_manager.reset();
}

bool Steam_Music::BIsEnabled()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_Music::BIsPlaying()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

AudioPlayback_Status Steam_Music::GetPlaybackStatus()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return AudioPlayback_Undefined;
}

void Steam_Music::Play()
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

void Steam_Music::Pause()
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

void Steam_Music::PlayPrevious()
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

void Steam_Music::PlayNext()
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// volume is between 0.0 and 1.0
void Steam_Music::SetVolume(float flVolume)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

float Steam_Music::GetVolume()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 1.0f;
}