/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_musicremote.h"

Steam_MusicRemote::Steam_MusicRemote()
{
}

Steam_MusicRemote::~Steam_MusicRemote()
{
    emu_deinit();
}

void Steam_MusicRemote::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _cb_manager = cb_manager;
    _network = network;
}

void Steam_MusicRemote::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    _network.reset();
    _cb_manager.reset();
}

// Service Definition
bool Steam_MusicRemote::RegisterSteamMusicRemote(const char* pchName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::DeregisterSteamMusicRemote()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::BIsCurrentMusicRemote()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::BActivationSuccess(bool bValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::SetDisplayName(const char* pchDisplayName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::SetPNGIcon_64x64(void* pvBuffer, uint32 cbBufferLength)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Abilities for the user interface
bool Steam_MusicRemote::EnablePlayPrevious(bool bValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::EnablePlayNext(bool bValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::EnableShuffled(bool bValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::EnableLooped(bool bValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::EnableQueue(bool bValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::EnablePlaylists(bool bValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Status
bool Steam_MusicRemote::UpdatePlaybackStatus(AudioPlayback_Status nStatus)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::UpdateShuffled(bool bValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::UpdateLooped(bool bValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::UpdateVolume(float flValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} // volume is between 0.0 and 1.0

// Current Entry
bool Steam_MusicRemote::CurrentEntryWillChange()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::CurrentEntryIsAvailable(bool bAvailable)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::UpdateCurrentEntryText(const char* pchText)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::UpdateCurrentEntryElapsedSeconds(int nValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::UpdateCurrentEntryCoverArt(void* pvBuffer, uint32 cbBufferLength)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::CurrentEntryDidChange()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Queue
bool Steam_MusicRemote::QueueWillChange()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::ResetQueueEntries()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::SetQueueEntry(int nID, int nPosition, const char* pchEntryText)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::SetCurrentQueueEntry(int nID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::QueueDidChange()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Playlist
bool Steam_MusicRemote::PlaylistWillChange()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::ResetPlaylistEntries()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::SetPlaylistEntry(int nID, int nPosition, const char* pchEntryText)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::SetCurrentPlaylistEntry(int nID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_MusicRemote::PlaylistDidChange()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}