/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"

class LOCAL_API Steam_MusicRemote :
    public ISteamMusicRemote001
{
    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;

public:
    std::recursive_mutex _local_mutex;

    Steam_MusicRemote();
    virtual ~Steam_MusicRemote();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    // Service Definition
    virtual bool RegisterSteamMusicRemote(const char* pchName);
    virtual bool DeregisterSteamMusicRemote();
    virtual bool BIsCurrentMusicRemote();
    virtual bool BActivationSuccess(bool bValue);

    virtual bool SetDisplayName(const char* pchDisplayName);
    virtual bool SetPNGIcon_64x64(void* pvBuffer, uint32 cbBufferLength);

    // Abilities for the user interface
    virtual bool EnablePlayPrevious(bool bValue);
    virtual bool EnablePlayNext(bool bValue);
    virtual bool EnableShuffled(bool bValue);
    virtual bool EnableLooped(bool bValue);
    virtual bool EnableQueue(bool bValue);
    virtual bool EnablePlaylists(bool bValue);

    // Status
    virtual bool UpdatePlaybackStatus(AudioPlayback_Status nStatus);
    virtual bool UpdateShuffled(bool bValue);
    virtual bool UpdateLooped(bool bValue);
    virtual bool UpdateVolume(float flValue); // volume is between 0.0 and 1.0

    // Current Entry
    virtual bool CurrentEntryWillChange();
    virtual bool CurrentEntryIsAvailable(bool bAvailable);
    virtual bool UpdateCurrentEntryText(const char* pchText);
    virtual bool UpdateCurrentEntryElapsedSeconds(int nValue);
    virtual bool UpdateCurrentEntryCoverArt(void* pvBuffer, uint32 cbBufferLength);
    virtual bool CurrentEntryDidChange();

    // Queue
    virtual bool QueueWillChange();
    virtual bool ResetQueueEntries();
    virtual bool SetQueueEntry(int nID, int nPosition, const char* pchEntryText);
    virtual bool SetCurrentQueueEntry(int nID);
    virtual bool QueueDidChange();

    // Playlist
    virtual bool PlaylistWillChange();
    virtual bool ResetPlaylistEntries();
    virtual bool SetPlaylistEntry(int nID, int nPosition, const char* pchEntryText);
    virtual bool SetCurrentPlaylistEntry(int nID);
    virtual bool PlaylistDidChange();
};