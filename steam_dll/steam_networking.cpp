/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_networking.h"
#include "steam_client.h"
#include "settings.h"

constexpr decltype(Steam_Networking::session_timeout) Steam_Networking::session_timeout;

Steam_Networking::Steam_Networking():
    _next_out_buffer_id(0)
{
}

Steam_Networking::~Steam_Networking()
{
    emu_deinit();
}

void Steam_Networking::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock(_local_mutex, cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(cb_manager->_local_mutex, std::adopt_lock);

    _cb_manager = cb_manager;
    _network = network;

    _cb_manager->register_callbacks(this);
    _cb_manager->register_frame(this);
    _network->register_listener(this, Network_Message_pb::MessagesCase::kNetwork);
}

void Steam_Networking::emu_deinit()
{
    TRACE_FUNC();
    if (_cb_manager != nullptr)
    {
        std::lock(_local_mutex, _cb_manager->_local_mutex);
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
        std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

        _cb_manager->unregister_callbacks(this);
        _cb_manager->unregister_frame(this);
    }

    if (_network != nullptr)
    {
        _network->unregister_listener(this, Network_Message_pb::MessagesCase::kNetwork);
    }

    {
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
        _network.reset();
        _cb_manager.reset();
    }
}

connection_state_t& Steam_Networking::get_connection(uint64 remote_id)
{
    return _connections_state[remote_id];
}

void Steam_Networking::clean_connexion(uint64 remote_id)
{
    for (auto buffs_it = _p2p_in_buffers.begin(); buffs_it != _p2p_in_buffers.end(); ++buffs_it)
    {
        for (auto buff_it = buffs_it->second.begin(); buff_it != buffs_it->second.end();)
        {
            if (buff_it->remote_id == remote_id)
                buff_it = buffs_it->second.erase(buff_it);
            else
                ++buff_it;
        }
    }

    auto it = _connections_state.find(remote_id);
    if (it != _connections_state.end())
        it->second.connected = it->second.connecting = false;
}

////////////////////////////////////////////////////////////////////////////////////////////
//
// UDP-style (connectionless) networking interface.  These functions send messages using
// an API organized around the destination.  Reliable and unreliable messages are supported.
//
// For a more TCP-style interface (meaning you have a connection handle), see the functions below.
// Both interface styles can send both reliable and unreliable messages.
//
// Automatically establishes NAT-traversing or Relay server connections

// Sends a P2P packet to the specified user
// UDP-like, unreliable and a max packet size of 1200 bytes
// the first packet send may be delayed as the NAT-traversal code runs
// if we can't get through to the user, an error will be posted via the callback P2PSessionConnectFail_t
// see EP2PSend enum above for the descriptions of the different ways of sending packets
//
// nChannel is a routing number you can use to help route message to different systems 	- you'll have to call ReadP2PPacket() 
// with the same channel number in order to retrieve the data on the other end
// using different channels to talk to the same user will still use the same underlying p2p connection, saving on resources
bool Steam_Networking::SendP2PPacket(CSteamID steamIDRemote, const void* pubData, uint32 cubData, EP2PSend eP2PSendType)
{
    return SendP2PPacket(steamIDRemote, pubData, cubData, eP2PSendType, default_channel);
}

bool Steam_Networking::SendP2PPacket(CSteamID steamIDRemote, const void* pubData, uint32 cubData, EP2PSend eP2PSendType, int nChannel)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    APP_LOG(Log::LogLevel::INFO, "%llu %p %d %d %d", steamIDRemote.ConvertToUint64(), pubData, cubData, eP2PSendType, nChannel);

    auto& conn = get_connection(steamIDRemote.ConvertToUint64());
    if (!conn.connected && !conn.connecting)
    {
        conn.reliable = (eP2PSendType == k_EP2PSendReliable || eP2PSendType == k_EP2PSendReliableWithBuffering);
        conn.connecting = true;

        Networking_P2P_Connect_Request_pb* request = new Networking_P2P_Connect_Request_pb;
        send_p2p_connection_request(steamIDRemote.ConvertToUint64(), request);
    }

    if (eP2PSendType == k_EP2PSendUnreliableNoDelay)
    {
        Networking_P2P_Data_pb* datas = new Networking_P2P_Data_pb;

        datas->set_channel(nChannel);
        datas->set_datas(reinterpret_cast<const char*>(pubData), cubData);
        datas->set_message_id(0);
        datas->set_type(eP2PSendType);

        send_p2p_data(steamIDRemote.ConvertToUint64(), datas);
    }
    else if (eP2PSendType == k_EP2PSendUnreliable)
    {
        net_buffer_t buffer;
        buffer.buffer.assign(reinterpret_cast<const char*>(pubData), cubData);
        buffer.remote_id = steamIDRemote.ConvertToUint64();
        buffer.id = 0;
        buffer.type = eP2PSendType;
        buffer.status = net_buffer_t::pending;
        buffer.channel = nChannel;

        _p2p_out_buffers.emplace_back(std::move(buffer));
    }
    else
    {
        net_buffer_t buffer;
        buffer.buffer.assign(reinterpret_cast<const char*>(pubData), cubData);
        buffer.remote_id = steamIDRemote.ConvertToUint64();
        buffer.id = _next_out_buffer_id++;
        buffer.type = eP2PSendType;
        buffer.status = net_buffer_t::pending;
        buffer.channel = nChannel;

        _p2p_out_buffers.emplace_back(std::move(buffer));
    }

    return true;
}

// returns true if any data is available for read, and the amount of data that will need to be read
bool Steam_Networking::IsP2PPacketAvailable(uint32* pcubMsgSize)
{
    return IsP2PPacketAvailable(pcubMsgSize, default_channel);
}

bool Steam_Networking::IsP2PPacketAvailable(uint32* pcubMsgSize, int nChannel)
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto& buffers = _p2p_in_buffers[nChannel];
    auto buff_it = buffers.begin();
    for (; buff_it != buffers.end(); ++buff_it)
    {
        if (get_connection(buff_it->remote_id).connected)
            break;
    }

    if (buff_it == buffers.end())
    {
        //CBRunFrame();
        //GetNetwork()->CBRunFrame(_pipe, Network_Message_pb::MessagesCase::kNetwork);
        return false;
    }

    if (pcubMsgSize != nullptr)
        *pcubMsgSize = buffers.front().buffer.length();

    return true;
}

// reads in a packet that has been sent from another user via SendP2PPacket()
// returns the size of the message and the steamID of the user who sent it in the last two parameters
// if the buffer passed in is too small, the message will be truncated
// this call is not blocking, and will return false if no data is available
bool Steam_Networking::ReadP2PPacket(void* pubDest, uint32 cubDest, uint32* pcubMsgSize, CSteamID* psteamIDRemote)
{
    return ReadP2PPacket(pubDest, cubDest, pcubMsgSize, psteamIDRemote, default_channel);
}

bool Steam_Networking::ReadP2PPacket(void* pubDest, uint32 cubDest, uint32* pcubMsgSize, CSteamID* psteamIDRemote, int nChannel)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto& buffers = _p2p_in_buffers[nChannel];
    auto buff_it = buffers.begin();
    for (; buff_it != buffers.end(); ++buff_it)
    {
        if (get_connection(buff_it->remote_id).connected)
            break;
    }

    if (buff_it == buffers.end())
    {
        APP_LOG(Log::LogLevel::ERR, "Didn't read P2P packet");
        _network->RunNetwork(Network_Message_pb::MessagesCase::kNetwork);
        return false;
    }

    size_t len = std::min(static_cast<uint32>(buff_it->buffer.length()), cubDest);

    if (pubDest != nullptr)
        memcpy(pubDest, buff_it->buffer.data(), len);

    if (pcubMsgSize != nullptr)
        *pcubMsgSize = len;

    if (psteamIDRemote != nullptr)
        *psteamIDRemote = buff_it->remote_id;

    APP_LOG(Log::LogLevel::INFO, "Read P2P packet: %llu %d", buff_it->remote_id, len);
    buffers.erase(buff_it);

    return true;
}

// AcceptP2PSessionWithUser() should only be called in response to a P2PSessionRequest_t callback
// P2PSessionRequest_t will be posted if another user tries to send you a packet that you haven't talked to yet
// if you don't want to talk to the user, just ignore the request
// if the user continues to send you packets, another P2PSessionRequest_t will be posted periodically
// this may be called multiple times for a single user
// (if you've called SendP2PPacket() on the other user, this implicitly accepts the session request)
bool Steam_Networking::AcceptP2PSessionWithUser(CSteamID steamIDRemote)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto& conn = get_connection(steamIDRemote.ConvertToUint64());

    Networking_P2P_Connect_Response_pb* response = new Networking_P2P_Connect_Response_pb;
    response->set_state(Networking_P2P_Connect_Response_pb_State::Networking_P2P_Connect_Response_pb_State_accepted);
    send_p2p_connection_response(steamIDRemote.ConvertToUint64(), response);

    conn.connecting = false;
    conn.connected = true;

    return true;
}

// call CloseP2PSessionWithUser() when you're done talking to a user, will free up resources under-the-hood
// if the remote user tries to send data to you again, another P2PSessionRequest_t callback will be posted
bool Steam_Networking::CloseP2PSessionWithUser(CSteamID steamIDRemote)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    uint64_t remoteId = steamIDRemote.ConvertToUint64();

    clean_connexion(remoteId);
    //_connections_state.erase(remoteId);
    
    return true;
}

// call CloseP2PChannelWithUser() when you're done talking to a user on a specific channel. Once all channels
// open channels to a user have been closed, the open session to the user will be closed and new data from this
// user will trigger a P2PSessionRequest_t callback
bool Steam_Networking::CloseP2PChannelWithUser(CSteamID steamIDRemote, int nChannel)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    uint64_t remoteId = steamIDRemote.ConvertToUint64();

    auto& conn = get_connection(remoteId);

    conn.channels.erase(nChannel);
    for (auto buff_it = _p2p_in_buffers[nChannel].begin(); buff_it != _p2p_in_buffers[nChannel].end();)
    {
        if (buff_it->remote_id == remoteId)
            buff_it = _p2p_in_buffers[nChannel].erase(buff_it);
        else
            ++buff_it;
    }

    if (conn.channels.empty())
        _connections_state.erase(remoteId);

    return true;
}

// fills out P2PSessionState_t structure with details about the underlying connection to the user
// should only needed for debugging purposes
// returns false if no connection exists to the specified user
bool Steam_Networking::GetP2PSessionState(CSteamID steamIDRemote, P2PSessionState_t* pConnectionState)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    uint64 remote_id = steamIDRemote.ConvertToUint64();
    auto conn_it = _connections_state.find(remote_id);
    if (conn_it != _connections_state.end())
    {
        if (pConnectionState != nullptr)
        {
            pConnectionState->m_bConnecting = conn_it->second.connecting;
            pConnectionState->m_bConnectionActive = conn_it->second.connected;
            pConnectionState->m_bUsingRelay = false;
            pConnectionState->m_eP2PSessionError = k_EP2PSessionErrorNone;
            pConnectionState->m_nBytesQueuedForSend = 0;
            pConnectionState->m_nPacketsQueuedForSend = 0;

            auto const& addr = _network->get_steamid_addr(remote_id);

            pConnectionState->m_nRemoteIP = addr.get_ip();
            pConnectionState->m_nRemotePort = addr.get_port();
        }

        return conn_it->second.connected;
    }

    if (pConnectionState != nullptr)
    {
        APP_LOG(Log::LogLevel::WARN, "No connection state found");
        pConnectionState->m_bConnecting = 0;
        pConnectionState->m_bConnectionActive = 0;
        pConnectionState->m_bUsingRelay = 0;
        pConnectionState->m_eP2PSessionError = k_EP2PSessionErrorNone;
        pConnectionState->m_nBytesQueuedForSend = 0;
        pConnectionState->m_nPacketsQueuedForSend = 0;
        pConnectionState->m_nRemoteIP = 0;
        pConnectionState->m_nRemotePort = 0;
    }
    return false;
}

// Allow P2P connections to fall back to being relayed through the Steam servers if a direct connection
// or NAT-traversal cannot be established. Only applies to connections created after setting this value,
// or to existing connections that need to automatically reconnect after this value is set.
//
// P2P packet relay is allowed by default
bool Steam_Networking::AllowP2PPacketRelay(bool bAllow)
{
    TRACE_FUNC();
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return true;
}


////////////////////////////////////////////////////////////////////////////////////////////
//
// LISTEN / CONNECT connection-oriented interface functions
//
// These functions are more like a client-server TCP API.  One side is the "server"
// and "listens" for incoming connections, which then must be "accepted."  The "client"
// initiates a connection by "connecting."  Sending and receiving is done through a
// connection handle.
//
// For a more UDP-style interface, where you do not track connection handles but
// simply send messages to a SteamID, use the UDP-style functions above.
//
// Both methods can send both reliable and unreliable methods.
//
////////////////////////////////////////////////////////////////////////////////////////////


// creates a socket and listens others to connect
// will trigger a SocketStatusCallback_t callback on another client connecting
// nVirtualP2PPort is the unique ID that the client will connect to, in case you have multiple ports
//		this can usually just be 0 unless you want multiple sets of connections
// unIP is the local IP address to bind to
//		pass in 0 if you just want the default local IP
// unPort is the port to use
//		pass in 0 if you don't want users to be able to connect via IP/Port, but expect to be always peer-to-peer connections only
SNetListenSocket_t Steam_Networking::CreateListenSocket(int nVirtualP2PPort, uint32 nIP, uint16 nPort)
{
    return CreateListenSocket(nVirtualP2PPort, nIP, nPort, true);
}

SNetListenSocket_t Steam_Networking::CreateListenSocket(int nVirtualP2PPort, uint32 nIP, uint16 nPort, bool bAllowUseOfPacketRelay)
{
    SteamIPAddress_t addr;

    addr.m_unIPv4 = nIP;
    addr.m_eType = k_ESteamIPTypeIPv4;

    return CreateListenSocket(nVirtualP2PPort, addr, nPort, bAllowUseOfPacketRelay);
}

SNetListenSocket_t Steam_Networking::CreateListenSocket(int nVirtualP2PPort, SteamIPAddress_t nIP, uint16 nPort, bool bAllowUseOfPacketRelay)
{
    TRACE_FUNC();
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return 0;
}

// creates a socket and begin connection to a remote destination
// can connect via a known steamID (client or game server), or directly to an IP
// on success will trigger a SocketStatusCallback_t callback
// on failure or timeout will trigger a SocketStatusCallback_t callback with a failure code in m_eSNetSocketState
SNetSocket_t Steam_Networking::CreateP2PConnectionSocket(CSteamID steamIDTarget, int nVirtualPort, int nTimeoutSec)
{
    return CreateP2PConnectionSocket(steamIDTarget, nVirtualPort, nTimeoutSec, true);
}

SNetSocket_t Steam_Networking::CreateP2PConnectionSocket(CSteamID steamIDTarget, int nVirtualPort, int nTimeoutSec, bool bAllowUseOfPacketRelay)
{
    TRACE_FUNC();
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return 0;
}

SNetSocket_t Steam_Networking::CreateConnectionSocket(uint32 nIP, uint16 nPort, int nTimeoutSec)
{
    SteamIPAddress_t addr;
    addr.m_unIPv4 = nIP;
    addr.m_eType = k_ESteamIPTypeIPv4;
    return CreateConnectionSocket(addr, nPort, nTimeoutSec);
}

SNetSocket_t Steam_Networking::CreateConnectionSocket(SteamIPAddress_t nIP, uint16 nPort, int nTimeoutSec)
{
    TRACE_FUNC();
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return 0;
}

// disconnects the connection to the socket, if any, and invalidates the handle
// any unread data on the socket will be thrown away
// if bNotifyRemoteEnd is set, socket will not be completely destroyed until the remote end acknowledges the disconnect
bool Steam_Networking::DestroySocket(SNetSocket_t hSocket, bool bNotifyRemoteEnd)
{
    TRACE_FUNC();
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}
// destroying a listen socket will automatically kill all the regular sockets generated from it
bool Steam_Networking::DestroyListenSocket(SNetListenSocket_t hSocket, bool bNotifyRemoteEnd)
{
    TRACE_FUNC();
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

// sending data
// must be a handle to a connected socket
// data is all sent via UDP, and thus send sizes are limited to 1200 bytes; after this, many routers will start dropping packets
// use the reliable flag with caution; although the resend rate is pretty aggressive,
// it can still cause stalls in receiving data (like TCP)
bool Steam_Networking::SendDataOnSocket(SNetSocket_t hSocket, void* pubData, uint32 cubData, bool bReliable)
{
    TRACE_FUNC();
    APP_LOG(Log::LogLevel::INFO, "TODO");;
    return false;
}

// receiving data
// returns false if there is no data remaining
// fills out *pcubMsgSize with the size of the next message, in bytes
bool Steam_Networking::IsDataAvailableOnSocket(SNetSocket_t hSocket, uint32* pcubMsgSize)
{
    TRACE_FUNC();
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

// fills in pubDest with the contents of the message
// messages are always complete, of the same size as was sent (i.e. packetized, not streaming)
// if *pcubMsgSize < cubDest, only partial data is written
// returns false if no data is available
bool Steam_Networking::RetrieveDataFromSocket(SNetSocket_t hSocket, void* pubDest, uint32 cubDest, uint32* pcubMsgSize)
{
    TRACE_FUNC();
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

// checks for data from any socket that has been connected off this listen socket
// returns false if there is no data remaining
// fills out *pcubMsgSize with the size of the next message, in bytes
// fills out *phSocket with the socket that data is available on
bool Steam_Networking::IsDataAvailable(SNetListenSocket_t hListenSocket, uint32* pcubMsgSize, SNetSocket_t* phSocket)
{
    TRACE_FUNC();
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

// retrieves data from any socket that has been connected off this listen socket
// fills in pubDest with the contents of the message
// messages are always complete, of the same size as was sent (i.e. packetized, not streaming)
// if *pcubMsgSize < cubDest, only partial data is written
// returns false if no data is available
// fills out *phSocket with the socket that data is available on
bool Steam_Networking::RetrieveData(SNetListenSocket_t hListenSocket, void* pubDest, uint32 cubDest, uint32* pcubMsgSize, SNetSocket_t* phSocket)
{
    TRACE_FUNC();
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

// returns information about the specified socket, filling out the contents of the pointers
bool Steam_Networking::GetSocketInfo(SNetSocket_t hSocket, CSteamID* pSteamIDRemote, int* peSocketStatus, uint32* punIPRemote, uint16* punPortRemote)
{
    SteamIPAddress_t addr;

    auto res = GetSocketInfo(hSocket, pSteamIDRemote, peSocketStatus, &addr, punPortRemote);

    if (punIPRemote != nullptr)
    {
        if (addr.m_eType == k_ESteamIPTypeIPv4)
            *punIPRemote = addr.m_unIPv4;
        else
            *punIPRemote = 0;
    }

    return res;
}

bool Steam_Networking::GetSocketInfo(SNetSocket_t hSocket, CSteamID* pSteamIDRemote, int* peSocketStatus, SteamIPAddress_t* punIPRemote, uint16* punPortRemote)
{
    TRACE_FUNC();
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

// returns which local port the listen socket is bound to
// *pnIP and *pnPort will be 0 if the socket is set to listen for P2P connections only
bool Steam_Networking::GetListenSocketInfo(SNetListenSocket_t hListenSocket, uint32* pnIP, uint16* pnPort)
{
    SteamIPAddress_t addr;

    auto res = GetListenSocketInfo(hListenSocket, &addr, pnPort);

    if (pnIP != nullptr)
    {
        if (addr.m_eType == k_ESteamIPTypeIPv4)
            *pnIP = addr.m_unIPv4;
        else
            *pnIP = 0;
    }

    return res;
}

bool Steam_Networking::GetListenSocketInfo(SNetListenSocket_t hListenSocket, SteamIPAddress_t* pnIP, uint16* pnPort)
{
    TRACE_FUNC();
    APP_LOG(Log::LogLevel::INFO, "TODO");

    return false;
}

// returns true to describe how the socket ended up connecting
ESNetSocketConnectionType Steam_Networking::GetSocketConnectionType(SNetSocket_t hSocket)
{
    TRACE_FUNC();
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return k_ESNetSocketConnectionTypeNotConnected;
}

// max packet size, in bytes
int Steam_Networking::GetMaxPacketSize(SNetSocket_t hSocket)
{
    TRACE_FUNC();
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return 1200;
}

///////////////////////////////////////////////////////////////////////////////
//                           Network Send messages                           //
///////////////////////////////////////////////////////////////////////////////
bool Steam_Networking::send_p2p_connection_request(uint64 remote_id, Networking_P2P_Connect_Request_pb* request)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    auto& conn = get_connection(remote_id);
    conn.has_timeout = true;
    conn.timeout_start = std::chrono::steady_clock::now();

    Network_Message_pb msg;
    Steam_Networking_pb* nw = new Steam_Networking_pb;

    nw->set_allocated_p2p_connect_request(request);

    msg.set_allocated_network(nw);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

bool Steam_Networking::send_p2p_connection_response(uint64 remote_id, Networking_P2P_Connect_Response_pb* response)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Networking_pb* nw = new Steam_Networking_pb;

    nw->set_allocated_p2p_connect_response(response);

    msg.set_allocated_network(nw);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

bool Steam_Networking::send_p2p_data(uint64 remote_id, Networking_P2P_Data_pb* datas)
{
    APP_LOG(Log::LogLevel::DEBUG, "Sending datas, size: %d", datas->datas().size());
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);
    TRACE_FUNC();

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    auto& conn = get_connection(remote_id);
    conn.has_timeout = true;
    conn.timeout_start = std::chrono::steady_clock::now();

    Network_Message_pb msg;
    Steam_Networking_pb* nw = new Steam_Networking_pb;

    nw->set_allocated_p2p_data(datas);

    msg.set_allocated_network(nw);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    if (datas->type() == k_EP2PSendUnreliable || datas->type() == k_EP2PSendUnreliableNoDelay)
        return _network->UnreliableSendTo(msg);
    else
        return _network->ReliableSendTo(msg);
}

bool Steam_Networking::send_p2p_data_ack(uint64 remote_id, Networking_P2P_Data_Ack_pb* ack)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    Network_Message_pb msg;
    Steam_Networking_pb* nw = new Steam_Networking_pb;

    nw->set_allocated_p2p_data_ack(ack);

    msg.set_allocated_network(nw);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}
///////////////////////////////////////////////////////////////////////////////
//                          Network Receive messages                         //
///////////////////////////////////////////////////////////////////////////////
bool Steam_Networking::on_peer_disconnect(Network_Message_pb const& msg, Network_Peer_Disconnect_pb const& conn)
{
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    if (_connections_state.count(msg.source_id()))
    {
        pFrameResult_t result(new FrameResult);
        P2PSessionConnectFail_t& p2pscf = result->CreateCallback<P2PSessionConnectFail_t>(std::chrono::milliseconds(800));

        p2pscf.m_eP2PSessionError = EP2PSessionError::k_EP2PSessionErrorDestinationNotLoggedIn;
        p2pscf.m_steamIDRemote = static_cast<uint64>(msg.source_id());

        result->done = true;

        _cb_manager->add_callback(this, result);
    }

    return true;
}

bool Steam_Networking::on_p2p_connection_request(Network_Message_pb const& msg, Networking_P2P_Connect_Request_pb const& request)
{
    TRACE_FUNC();
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    auto& conn = get_connection(msg.source_id());
    conn.connecting = true;
    conn.has_timeout = true;
    conn.timeout_start = std::chrono::steady_clock::now();

    pFrameResult_t result(new FrameResult);
    P2PSessionRequest_t& p2psr = result->CreateCallback<P2PSessionRequest_t>(std::chrono::milliseconds(1500));

    p2psr.m_steamIDRemote = static_cast<uint64>(msg.source_id());

    result->done = true;

    return _cb_manager->add_callback(this, result);
}

bool Steam_Networking::on_p2p_connection_response(Network_Message_pb const& msg, Networking_P2P_Connect_Response_pb const& response)
{
    TRACE_FUNC();
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    auto& conn = get_connection(msg.source_id());
    conn.has_timeout = false;

    if (response.state() == Networking_P2P_Connect_Response_pb_State::Networking_P2P_Connect_Response_pb_State_accepted)
    {
        conn.connected = true;
        conn.connecting = false;
    }
    else
    {
        pFrameResult_t res(new FrameResult);
        P2PSessionConnectFail_t& p2pscf = res->CreateCallback<P2PSessionConnectFail_t>(std::chrono::milliseconds(0));

        p2pscf.m_steamIDRemote = static_cast<uint64>(msg.source_id());
        p2pscf.m_eP2PSessionError = k_EP2PSessionErrorTimeout;

        res->done = true;
        _cb_manager->add_callback(this, res);

        clean_connexion(msg.source_id());
    }

    return true;
}

bool Steam_Networking::on_p2p_data_receive(Network_Message_pb const& msg, Networking_P2P_Data_pb const& p2p_datas)
{
    APP_LOG(Log::LogLevel::TRACE, "Receiving datas, size %d", p2p_datas.datas().length());
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);
    TRACE_FUNC();

    auto& conn = get_connection(msg.source_id());
    conn.has_timeout = false;

    if (conn.connected)
    {
        conn.connecting = false;

        net_buffer_t buffer;
        buffer.remote_id = msg.source_id();
        buffer.buffer = p2p_datas.datas();
        buffer.channel = p2p_datas.channel();
        buffer.type = (EP2PSend)p2p_datas.type();
        _p2p_in_buffers[p2p_datas.channel()].emplace_back(std::move(buffer));

        if (buffer.type == k_EP2PSendReliable || buffer.type == k_EP2PSendReliableWithBuffering)
        {
            Networking_P2P_Data_Ack_pb* ack = new Networking_P2P_Data_Ack_pb;
            send_p2p_data_ack(msg.source_id(), ack);
        }
    }

    return true;
}

bool Steam_Networking::on_p2p_data_ack(Network_Message_pb const& msg, Networking_P2P_Data_Ack_pb const& ack)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto conn_it = _connections_state.find(msg.source_id());

    if (conn_it != _connections_state.end())
    {
        conn_it->second.has_timeout = false;

        for (auto buff_it = _p2p_out_buffers.begin(); buff_it != _p2p_out_buffers.end();)
        {
            if (ack.message_id() == buff_it->id)
            {
                buff_it = _p2p_out_buffers.erase(buff_it);
                break;
            }
            else
                ++buff_it;
        }
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//                                 IRunCallback                              //
///////////////////////////////////////////////////////////////////////////////
bool Steam_Networking::CBRunFrame()
{
    //TRACE_FUNC();
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    auto now = std::chrono::steady_clock::now();
    for (auto conn_it = _connections_state.begin(); conn_it != _connections_state.end();)
    {
        if (conn_it->second.has_timeout && (now - conn_it->second.timeout_start) > session_timeout)
        {
            conn_it->second.has_timeout = false;

            pFrameResult_t res(new FrameResult);
            P2PSessionConnectFail_t& p2pscf = res->CreateCallback<P2PSessionConnectFail_t>(std::chrono::milliseconds(0));

            p2pscf.m_steamIDRemote = static_cast<uint64>(conn_it->first);
            p2pscf.m_eP2PSessionError = k_EP2PSessionErrorTimeout;

            res->done = true;
            _cb_manager->add_callback(this, res);

            clean_connexion(conn_it->first);
            conn_it = _connections_state.erase(conn_it);
        }
        else
            ++conn_it;
    }

    for (auto buff_it = _p2p_out_buffers.begin(); buff_it != _p2p_out_buffers.end();)
    {
        auto conn_it = _connections_state.find(buff_it->remote_id);
        if (conn_it != _connections_state.end())
        {
            if (conn_it->second.connected && buff_it->status == net_buffer_t::pending)
            {
                Networking_P2P_Data_pb* datas = new Networking_P2P_Data_pb;

                datas->set_channel(buff_it->channel);
                datas->set_datas(std::move(buff_it->buffer));
                datas->set_message_id(buff_it->id);
                datas->set_type(buff_it->type);

                send_p2p_data(buff_it->remote_id, datas);

                if (buff_it->type == k_EP2PSendUnreliable || buff_it->type == k_EP2PSendUnreliableNoDelay)
                    buff_it = _p2p_out_buffers.erase(buff_it);
                else
                {
                    buff_it = _p2p_out_buffers.erase(buff_it);
                    // TODO: resend if we didn't received the ack
                    //buff_it->status = net_buffer_t::sent;
                    //buff_it->buffer_time = std::chrono::steady_clock::now();
                }
            }
            else
                ++buff_it;
        }
        else
            buff_it = _p2p_out_buffers.erase(buff_it);
    }

    return true;
}

bool Steam_Networking::RunNetwork(Network_Message_pb const& msg)
{
    TRACE_FUNC();

    Steam_Networking_pb const& network_msg = msg.network();

    switch (network_msg.messages_case())
    {
        case Steam_Networking_pb::MessagesCase::kP2PConnectRequest : return on_p2p_connection_request(msg, network_msg.p2p_connect_request());
        case Steam_Networking_pb::MessagesCase::kP2PConnectResponse: return on_p2p_connection_response(msg, network_msg.p2p_connect_response());
        case Steam_Networking_pb::MessagesCase::kP2PData           : return on_p2p_data_receive(msg, network_msg.p2p_data());
        case Steam_Networking_pb::MessagesCase::kP2PDataAck        : return on_p2p_data_ack(msg, network_msg.p2p_data_ack());
        default: APP_LOG(Log::LogLevel::WARN, "Message unhandled %d", network_msg.messages_case());
    }

    return false;
}

bool Steam_Networking::RunCallbacks(pFrameResult_t res)
{
    TRACE_FUNC();
    //GLOBAL_LOCK();
    //std::lock_guard<std::recursive_mutex> lk(local_mutex);

    //auto now_time = std::chrono::steady_clock::now();
    //
    //switch (res->ICallback())
    //{
    //}
    return res->done;
}
