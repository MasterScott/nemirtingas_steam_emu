/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"

class LOCAL_API Steam_NetworkingMessages :
    public ISteamNetworkingMessages002
{
	std::shared_ptr<Callback_Manager> _cb_manager;
	std::shared_ptr<Network>          _network;

public:
	std::recursive_mutex _local_mutex;

    Steam_NetworkingMessages();
    virtual ~Steam_NetworkingMessages();
	void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
	void emu_deinit();

	virtual EResult SendMessageToUser(const SteamNetworkingIdentity& identityRemote, const void* pubData, uint32 cubData, int nSendFlags, int nRemoteChannel);
	virtual int ReceiveMessagesOnChannel(int nLocalChannel, SteamNetworkingMessage_t** ppOutMessages, int nMaxMessages);
	virtual bool AcceptSessionWithUser(const SteamNetworkingIdentity& identityRemote);
	virtual bool CloseSessionWithUser(const SteamNetworkingIdentity& identityRemote);
	virtual bool CloseChannelWithUser(const SteamNetworkingIdentity& identityRemote, int nLocalChannel);
	virtual ESteamNetworkingConnectionState GetSessionConnectionInfo(const SteamNetworkingIdentity& identityRemote, SteamNetConnectionInfo_t* pConnectionInfo, SteamNetworkingQuickConnectionStatus* pQuickStatus);
};