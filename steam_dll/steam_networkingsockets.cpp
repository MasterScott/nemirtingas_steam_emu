/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_networkingsockets.h"
#include "steam_client.h"
#include "settings.h"

using socket_buffer_t = typename Steam_NetworkingSockets::socket_buffer_t;
using listen_socket_t = typename Steam_NetworkingSockets::listen_socket_t;
using net_socket_t    = typename Steam_NetworkingSockets::net_socket_t;

decltype(Steam_NetworkingSockets::connect_timeout)      Steam_NetworkingSockets::connect_timeout;

decltype(Steam_NetworkingSockets::_next_listen_socket)  Steam_NetworkingSockets::_next_listen_socket(k_HSteamListenSocket_Invalid);
decltype(Steam_NetworkingSockets::_next_net_socket)     Steam_NetworkingSockets::_next_net_socket(k_HSteamNetConnection_Invalid);
decltype(Steam_NetworkingSockets::_next_netpoll_socket) Steam_NetworkingSockets::_next_netpoll_socket(k_HSteamNetPollGroup_Invalid);

Steam_NetworkingSockets::Steam_NetworkingSockets()
{
}

Steam_NetworkingSockets::~Steam_NetworkingSockets()
{
    emu_deinit();
}

void Steam_NetworkingSockets::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock(_local_mutex, cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(cb_manager->_local_mutex, std::adopt_lock);

    _cb_manager = cb_manager;
    _network = network;

    _cb_manager->register_callbacks(this);
    _cb_manager->register_frame(this);
    _network->register_listener(this, Network_Message_pb::MessagesCase::kNetworkingSocket);
}

void Steam_NetworkingSockets::emu_deinit()
{
    TRACE_FUNC();
    if (_cb_manager != nullptr)
    {
        std::lock(_local_mutex, _cb_manager->_local_mutex);
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
        std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

        _cb_manager->unregister_callbacks(this);
        _cb_manager->unregister_frame(this);
    }

    if (_network != nullptr)
    {
        _network->unregister_listener(this, Network_Message_pb::MessagesCase::kNetworkingSocket);
    }

    {
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
        _network.reset();
        _cb_manager.reset();
    }
}

HSteamListenSocket Steam_NetworkingSockets::alloc_listen_socket()
{
    ++_next_listen_socket;
    return _next_listen_socket;
}

HSteamListenSocket Steam_NetworkingSockets::create_listen_socket(int virtual_port)
{
    auto it = get_listen_from_port(virtual_port);
    if (it != _listen_sockets.end())
        return k_HSteamListenSocket_Invalid;

    HSteamListenSocket hSocket = alloc_listen_socket();
    if (hSocket != k_HSteamListenSocket_Invalid)
    {
        auto& listen_socket = _listen_sockets[hSocket];
        listen_socket.virtual_port = virtual_port;
        listen_socket.poll_group = k_HSteamNetPollGroup_Invalid;
    }
    return hSocket;
}

HSteamNetConnection Steam_NetworkingSockets::alloc_net_socket()
{
    ++_next_net_socket;
    return _next_net_socket;
}

HSteamNetConnection Steam_NetworkingSockets::create_net_socket(SteamNetworkingIdentity const& identity, int virtual_port, HSteamListenSocket parent)
{
    HSteamNetConnection hSocket = alloc_net_socket();

    if (hSocket != k_HSteamNetConnection_Invalid)
    {
        auto &infos = _net_sockets[hSocket];
        infos.poll_group = k_HSteamNetPollGroup_Invalid;
        infos.remote_net_id = 0;
        infos.parent = parent;
        infos.virtual_port = virtual_port;
        infos.identity = identity;
        infos.stats.m_eState = ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_None;
        infos.user_data = -1;
        infos.next_message_number = 0;

        infos.stats.m_flConnectionQualityLocal = 1.0f;
        infos.stats.m_flConnectionQualityRemote = 1.0f;
        infos.stats.m_flInBytesPerSec    = 2048.0;
        infos.stats.m_flInPacketsPerSec  = 10.0;
        infos.stats.m_flOutBytesPerSec   = 2048.0;
        infos.stats.m_flOutPacketsPerSec = 10.0;
        infos.stats.m_usecQueueTime = 50;
        infos.stats.m_nSendRateBytesPerSecond = 2048;
    }

    return hSocket;
}

HSteamNetPollGroup Steam_NetworkingSockets::alloc_poll_group()
{
    ++_next_netpoll_socket;
    return _next_netpoll_socket;
}

HSteamNetPollGroup Steam_NetworkingSockets::create_poll_group()
{
    HSteamNetPollGroup pollgroup = alloc_poll_group();
    if (pollgroup != k_HSteamNetPollGroup_Invalid)
    {
        _net_poll_groups[pollgroup];
    }
    return pollgroup;
}

std::unordered_map<HSteamListenSocket, listen_socket_t>::iterator Steam_NetworkingSockets::get_listen_from_port(int virtual_port)
{
    return std::find_if(_listen_sockets.begin(), _listen_sockets.end(), [virtual_port](std::pair<HSteamListenSocket const, listen_socket_t>& item)
    {
        return item.second.virtual_port == virtual_port;
    });
}

std::unordered_map<HSteamListenSocket, net_socket_t>::iterator Steam_NetworkingSockets::get_net_from_local_id(uint64 remote_id, uint32_t net_id)
{
    return std::find_if(_net_sockets.begin(), _net_sockets.end(), [&]( std::pair<HSteamNetConnection const, net_socket_t> &item)
    {
        return remote_id == item.second.identity.GetSteamID64() && net_id == item.first;
    });
}

std::unordered_map<HSteamListenSocket, net_socket_t>::iterator Steam_NetworkingSockets::get_net_from_remote_id(uint64 remote_id, uint32_t net_id)
{
    return std::find_if(_net_sockets.begin(), _net_sockets.end(), [&](std::pair<HSteamNetConnection const, net_socket_t>& item)
    {
        return remote_id == item.second.identity.GetSteamID64() && net_id == item.second.remote_net_id;
    });
}

size_t Steam_NetworkingSockets::get_old_message(HSteamListenSocket sock_id, net_socket_t* pConn, SteamNetworkingMessage001_t** ppOutMessages, size_t out_size)
{
    size_t max_messages = 0;
    max_messages = std::min<size_t>(pConn->buffers.size(), out_size);

    auto pfnRelease = [](SteamNetworkingMessage001_t* _this)
    {
        free(_this->m_pData);
        delete _this;
    };

    size_t i;
    for (i = 0; i < max_messages; ++i)
    {
        auto& message = ppOutMessages[i];
        message = new SteamNetworkingMessage001_t;
        memset(message, 0, sizeof(*message));

        auto& buffer = pConn->buffers.front();
        message->m_cbSize = buffer.datas.length();
        message->m_pData = malloc(buffer.datas.length() * sizeof(uint8_t));
        buffer.datas.copy((char*)message->m_pData, buffer.datas.length());
        message->m_conn = sock_id;
        message->m_steamIDSender = pConn->identity.GetSteamID();
        message->m_nConnUserData = pConn->user_data;
        message->m_nChannel = 0;

        message->m_pfnRelease = pfnRelease;

        pConn->buffers.pop();
    }

    //for (; i < out_size; ++i)
    //{
    //    ppOutMessages[i] = nullptr;
    //}

    return max_messages;
}

size_t Steam_NetworkingSockets::get_message(HSteamListenSocket sock_id, net_socket_t* pConn, SteamNetworkingMessage_t** ppOutMessages, size_t out_size)
{
    size_t max_messages = std::min<size_t>(pConn->buffers.size(), out_size);

    auto pfnFreeData = [](SteamNetworkingMessage_t* _this)
    {
        free(_this->m_pData);
        _this->m_pData = nullptr;
    };

    auto pfnRelease = [](SteamNetworkingMessage_t* _this)
    {
        if (_this->m_pfnFreeData != nullptr)
            _this->m_pfnFreeData(_this);

        delete _this;
    };

    size_t i;
    for (i = 0; i < max_messages; ++i)
    {
        auto& message = ppOutMessages[i];
        message = new SteamNetworkingMessage_t;
        memset(message, 0, sizeof(*message));

        auto& buffer = pConn->buffers.front();
        message->m_cbSize = buffer.datas.length();
        message->m_pData = malloc(buffer.datas.length() * sizeof(uint8_t));
        buffer.datas.copy((char*)message->m_pData, buffer.datas.length());
        message->m_conn = sock_id;
        message->m_identityPeer = pConn->identity;
        message->m_nConnUserData = pConn->user_data;
        message->m_usecTimeReceived = buffer.received_time.count();
        message->m_nMessageNumber = buffer.message_number;
        message->m_nChannel = 0;

        message->m_pfnFreeData = pfnFreeData;
        message->m_pfnRelease = pfnRelease;

        pConn->buffers.pop();
    }

    //for (; i < out_size; ++i)
    //{
    //    ppOutMessages[i] = nullptr;
    //}

    return max_messages;
}

void Steam_NetworkingSockets::trigger_net_callback(HSteamNetConnection const& hSocket, net_socket_t& infos, ESteamNetworkingConnectionState new_state, ESteamNetConnectionEnd end_reason, std::chrono::milliseconds timeout)
{
    pFrameResult_t res(new FrameResult);

    SteamNetConnectionStatusChangedCallback_t& sncscc = res->CreateCallback<SteamNetConnectionStatusChangedCallback_t>(timeout);

    memset(&sncscc, 0, sizeof(sncscc));
    sncscc.m_eOldState = infos.stats.m_eState;
    sncscc.m_hConn = hSocket;
    sncscc.m_info.m_identityRemote.SetSteamID(infos.identity.GetSteamID());
    sncscc.m_info.m_hListenSocket = infos.parent;
    sncscc.m_info.m_nUserData = infos.user_data;
    sncscc.m_info.m_eEndReason = end_reason;

    infos.stats.m_eState = new_state;
    sncscc.m_info.m_eState = infos.stats.m_eState;

    res->done = true;
    _cb_manager->add_callback(this, res);
}

/// Creates a "server" socket that listens for clients to connect to, either by calling
/// ConnectSocketBySteamID or ConnectSocketByIPv4Address.
///
/// nSteamConnectVirtualPort specifies how clients can connect to this socket using
/// ConnectBySteamID.  A negative value indicates that this functionality is
/// disabled and clients must connect by IP address.  It's very common for applications
/// to only have one listening socket; in that case, use zero.  If you need to open
/// multiple listen sockets and have clients be able to connect to one or the other, then
/// nSteamConnectVirtualPort should be a small integer constant unique to each listen socket
/// you create.
///
/// In the open-source version of this API, you must pass -1 for nSteamConnectVirtualPort
///
/// If you want clients to connect to you by your IPv4 addresses using
/// ConnectByIPv4Address, then you must set nPort to be nonzero.  Steam will
/// bind a UDP socket to the specified local port, and clients will send packets using
/// ordinary IP routing.  It's up to you to take care of NAT, protecting your server
/// from DoS, etc.  If you don't need clients to connect to you by IP, then set nPort=0.
/// Use nIP if you wish to bind to a particular local interface.  Typically you will use 0,
/// which means to listen on all interfaces, and accept the default outbound IP address.
/// If nPort is zero, then nIP must also be zero.
///
/// A SocketStatusCallback_t callback when another client attempts a connection.
HSteamListenSocket Steam_NetworkingSockets::CreateListenSocket(int nSteamConnectVirtualPort, uint32 nIP, uint16 nPort)
{
    TRACE_FUNC();
    return CreateListenSocketP2P(nSteamConnectVirtualPort);
}

HSteamListenSocket Steam_NetworkingSockets::CreateListenSocketIP(const SteamNetworkingIPAddr& localAddress)
{
    return CreateListenSocketIP(localAddress, 0, nullptr);
}

HSteamListenSocket Steam_NetworkingSockets::CreateListenSocketIP(const SteamNetworkingIPAddr& localAddress, int nOptions, const SteamNetworkingConfigValue_t* pOptions)
{
    TRACE_FUNC();
    return k_HSteamListenSocket_Invalid;
}

/// Creates a connection and begins talking to a remote destination.  The remote host
/// must be listening with a matching call to CreateListenSocket.
///
/// Use ConnectBySteamID to connect using the SteamID (client or game server) as the network address.
/// Use ConnectByIPv4Address to connect by IP address.
///
/// A SteamNetConnectionStatusChangedCallback_t callback will be triggered when we start connecting,
/// and then another one on timeout or successful connection
HSteamNetConnection Steam_NetworkingSockets::ConnectByIPv4Address(uint32 nIP, uint16 nPort)
{
    SteamNetworkingIPAddr addr;
    addr.SetIPv4(nIP, nPort);
    return ConnectByIPAddress(addr);
}

HSteamNetConnection Steam_NetworkingSockets::ConnectByIPAddress(const SteamNetworkingIPAddr& address)
{
    return ConnectByIPAddress(address, 0, nullptr);
}

HSteamNetConnection Steam_NetworkingSockets::ConnectByIPAddress(const SteamNetworkingIPAddr& address, int nOptions, const SteamNetworkingConfigValue_t* pOptions)
{
    TRACE_FUNC();
    return k_HSteamNetConnection_Invalid;
}

/// Like CreateListenSocketIP, but clients will connect using ConnectP2P
///
/// nVirtualPort specifies how clients can connect to this socket using
/// ConnectP2P.  It's very common for applications to only have one listening socket;
/// in that case, use zero.  If you need to open multiple listen sockets and have clients
/// be able to connect to one or the other, then nVirtualPort should be a small integer (<1000)
/// unique to each listen socket you create.
///
/// If you use this, you probably want to call ISteamNetworkingUtils::InitializeRelayNetworkAccess()
/// when your app initializes
HSteamListenSocket Steam_NetworkingSockets::CreateListenSocketP2P(int nVirtualPort)
{
    return CreateListenSocketP2P(nVirtualPort, 0, nullptr);
}

HSteamListenSocket Steam_NetworkingSockets::CreateListenSocketP2P(int nVirtualPort, int nOptions, const SteamNetworkingConfigValue_t* pOptions)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    return create_listen_socket(nVirtualPort);
}

/// Begin connecting to a server that is identified using a platform-specific identifier.
/// This requires some sort of third party rendezvous service, and will depend on the
/// platform and what other libraries and services you are integrating with.
///
/// At the time of this writing, there is only one supported rendezvous service: Steam.
/// Set the SteamID (whether "user" or "gameserver") and Steam will determine if the
/// client is online and facilitate a relay connection.  Note that all P2P connections on
/// Steam are currently relayed.
///
/// If you use this, you probably want to call ISteamNetworkingUtils::InitializeRelayNetworkAccess()
/// when your app initializes
HSteamNetConnection Steam_NetworkingSockets::ConnectBySteamID(CSteamID steamIDTarget, int nVirtualPort)
{
    TRACE_FUNC();

    SteamNetworkingIdentity identity;
    identity.SetSteamID(steamIDTarget);

    return ConnectP2P(identity, nVirtualPort);
}

HSteamNetConnection Steam_NetworkingSockets::ConnectP2P(const SteamNetworkingIdentity& identityRemote, int nVirtualPort)
{
    return ConnectP2P(identityRemote, nVirtualPort, 0, nullptr);
}

HSteamNetConnection Steam_NetworkingSockets::ConnectP2P(const SteamNetworkingIdentity& identityRemote, int nVirtualPort, int nOptions, const SteamNetworkingConfigValue_t* pOptions)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    switch (identityRemote.m_eType)
    {
        case ESteamNetworkingIdentityType::k_ESteamNetworkingIdentityType_SteamID:
        {
            HSteamNetConnection hSocket = create_net_socket(identityRemote, nVirtualPort);

            if (hSocket != k_HSteamNetConnection_Invalid)
            {
                auto& infos = _net_sockets[hSocket];
                NetworkingSockets_Connect_pb* connect_pb = new NetworkingSockets_Connect_pb;
                connect_pb->set_virtual_port(nVirtualPort);
                connect_pb->set_local_net_id(hSocket);

                infos.stats.m_eState = ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_Connecting;
                infos.connect_start = std::chrono::steady_clock::now();
                if (!send_connect(identityRemote.GetSteamID64(), connect_pb))
                {// Failed to send, we didn't find peer
                    trigger_net_callback(
                        hSocket,
                        infos,
                        ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_ProblemDetectedLocally,
                        ESteamNetConnectionEnd::k_ESteamNetConnectionEnd_Remote_NotLoggedIn,
                        std::chrono::milliseconds(2000)
                    );
                }
            }

            return hSocket;
        }
    }

    return k_HSteamNetConnection_Invalid;
}

/// Accept an incoming connection that has been received on a listen socket.
///
/// When a connection attempt is received (perhaps after a few basic handshake
/// packets have been exchanged to prevent trivial spoofing), a connection interface
/// object is created in the k_ESteamNetworkingConnectionState_Connecting state
/// and a SteamNetConnectionStatusChangedCallback_t is posted.  At this point, your
/// application MUST either accept or close the connection.  (It may not ignore it.)
/// Accepting the connection will transition it either into the connected state,
/// of the finding route state, depending on the connection type.
///
/// You should take action within a second or two, because accepting the connection is
/// what actually sends the reply notifying the client that they are connected.  If you
/// delay taking action, from the client's perspective it is the same as the network
/// being unresponsive, and the client may timeout the connection attempt.  In other
/// words, the client cannot distinguish between a delay caused by network problems
/// and a delay caused by the application.
///
/// This means that if your application goes for more than a few seconds without
/// processing callbacks (for example, while loading a map), then there is a chance
/// that a client may attempt to connect in that interval and fail due to timeout.
///
/// If the application does not respond to the connection attempt in a timely manner,
/// and we stop receiving communication from the client, the connection attempt will
/// be timed out locally, transitioning the connection to the
/// k_ESteamNetworkingConnectionState_ProblemDetectedLocally state.  The client may also
/// close the connection before it is accepted, and a transition to the
/// k_ESteamNetworkingConnectionState_ClosedByPeer is also possible depending the exact
/// sequence of events.
///
/// Returns k_EResultInvalidParam if the handle is invalid.
/// Returns k_EResultInvalidState if the connection is not in the appropriate state.
/// (Remember that the connection state could change in between the time that the
/// notification being posted to the queue and when it is received by the application.)
EResult Steam_NetworkingSockets::AcceptConnection(HSteamNetConnection hConn)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    auto net_it = _net_sockets.find(hConn);
    if (net_it == _net_sockets.end())
    {
        APP_LOG(Log::LogLevel::INFO, "Accept failed, hConn not found");
        return EResult::k_EResultInvalidParam;
    }

    if (net_it->second.stats.m_eState == ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_Connected)
    {
        APP_LOG(Log::LogLevel::INFO, "Accept called but connection was already accepted");
        return EResult::k_EResultOK;
    }

    if (net_it->second.stats.m_eState != ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_Connecting)
    {
        APP_LOG(Log::LogLevel::INFO, "Accept failed, hConn state is not connecting");
        return EResult::k_EResultInvalidState;
    }

    auto listen_it = _listen_sockets.find(net_it->second.parent);
    if (listen_it == _listen_sockets.end())
        return EResult::k_EResultInvalidParam;

    NetworkingSockets_Accept_pb* accept_pb = new NetworkingSockets_Accept_pb;

    accept_pb->set_state(NetworkingSockets_Accept_pb_State::NetworkingSockets_Accept_pb_State_accepted);
    accept_pb->set_local_net_id(net_it->first);
    accept_pb->set_remote_net_id(net_it->second.remote_net_id);

    if (send_accept(net_it->second.identity.GetSteamID64(), accept_pb))
    {
        trigger_net_callback(
            net_it->first,
            net_it->second,
            ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_Connected
        );
    }
    else
    {
        trigger_net_callback(
            net_it->first,
            net_it->second,
            ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_ProblemDetectedLocally,
            ESteamNetConnectionEnd::k_ESteamNetConnectionEnd_Remote_NotLoggedIn
        );
    }

    return EResult::k_EResultOK;
}

/// Disconnects from the remote host and invalidates the connection handle.
/// Any unread data on the connection is discarded.
///
/// nReason is an application defined code that will be received on the other
/// end and recorded (when possible) in backend analytics.  The value should
/// come from a restricted range.  (See ESteamNetConnectionEnd.)  If you don't need
/// to communicate any information to the remote host, and do not want analytics to
/// be able to distinguish "normal" connection terminations from "exceptional" ones,
/// You may pass zero, in which case the generic value of
/// k_ESteamNetConnectionEnd_App_Generic will be used.
///
/// pszDebug is an optional human-readable diagnostic string that will be received
/// by the remote host and recorded (when possible) in backend analytics.
///
/// If you wish to put the socket into a "linger" state, where an attempt is made to
/// flush any remaining sent data, use bEnableLinger=true.  Otherwise reliable data
/// is not flushed.
///
/// If the connection has already ended and you are just freeing up the
/// connection interface, the reason code, debug string, and linger flag are
/// ignored.
bool Steam_NetworkingSockets::CloseConnection(HSteamNetConnection hPeer, int nReason, const char* pszDebug, bool bEnableLinger)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = _net_sockets.find(hPeer);
    if(it == _net_sockets.end())
        return false;

    NetworkingSockets_Close_pb* close_pb = new NetworkingSockets_Close_pb;
    close_pb->set_local_net_id(it->first);
    close_pb->set_remote_net_id(it->second.remote_net_id);

    send_close(it->second.identity.GetSteamID64(), close_pb);

    for (auto& poll : _net_poll_groups)
    {
        auto poll_it = poll.second.find(&it->second);
        if (poll_it != poll.second.end())
        {
            poll.second.erase(poll_it);
        }
    }

    _net_sockets.erase(it);

    return true;
}

/// Destroy a listen socket, and all the client sockets generated by accepting connections
/// on the listen socket.
///
/// pszNotifyRemoteReason determines what cleanup actions are performed on the client
/// sockets being destroyed.  (See DestroySocket for more details.)
///
/// Note that if cleanup is requested and you have requested the listen socket bound to a
/// particular local port to facilitate direct UDP/IPv4 connections, then the underlying UDP
/// socket must remain open until all clients have been cleaned up.
bool Steam_NetworkingSockets::CloseListenSocket(HSteamListenSocket hSocket, const char* pszNotifyRemoteReason)
{
    return CloseListenSocket(hSocket);
}

/// Destroy a listen socket.  All the connections that were accepting on the listen
/// socket are closed ungracefully.
bool Steam_NetworkingSockets::CloseListenSocket(HSteamListenSocket hSocket)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = _listen_sockets.find(hSocket);
    if (it == _listen_sockets.end())
        return false;

    _listen_sockets.erase(it);
    for (auto it = _net_sockets.begin(); it != _net_sockets.end();)
    {
        if (it->second.parent == hSocket)
        {
            NetworkingSockets_Close_pb* close_pb = new NetworkingSockets_Close_pb;
            close_pb->set_local_net_id(it->first);
            close_pb->set_remote_net_id(it->second.remote_net_id);

            send_close(it->second.identity.GetSteamID64(), close_pb);

            it = _net_sockets.erase(it);
        }
        else
            ++it;
    }

    return true;
}

/// Set connection user data.  Returns false if the handle is invalid.
bool Steam_NetworkingSockets::SetConnectionUserData(HSteamNetConnection hPeer, int64 nUserData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = _net_sockets.find(hPeer);
    if (it == _net_sockets.end())
        return false;

    it->second.user_data = nUserData;

    return true;
}

/// Fetch connection user data.  Returns -1 if handle is invalid
/// or if you haven't set any userdata on the connection.
int64 Steam_NetworkingSockets::GetConnectionUserData(HSteamNetConnection hPeer)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = _net_sockets.find(hPeer);
    if (it == _net_sockets.end())
        return -1;

    return it->second.user_data;
}

/// Set a name for the connection, used mostly for debugging
void Steam_NetworkingSockets::SetConnectionName(HSteamNetConnection hPeer, const char* pszName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = _net_sockets.find(hPeer);
    if (it == _net_sockets.end())
        return;

    if (pszName == nullptr)
        it->second.name.clear();
    else
        it->second.name = pszName;
}

/// Fetch connection name.  Returns false if handle is invalid
bool Steam_NetworkingSockets::GetConnectionName(HSteamNetConnection hPeer, char* pszName, int nMaxLen)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = _net_sockets.find(hPeer);
    if (it == _net_sockets.end())
        return false;

    if (pszName != nullptr)
    {
        strncpy(pszName, it->second.name.c_str(), nMaxLen);
        pszName[nMaxLen - 1] = '\0';
    }

    return true;
}

/// Send a message to the remote host on the connected socket.
///
/// eSendType determines the delivery guarantees that will be provided,
/// when data should be buffered, etc.
///
/// Note that the semantics we use for messages are not precisely
/// the same as the semantics of a standard "stream" socket.
/// (SOCK_STREAM)  For an ordinary stream socket, the boundaries
/// between chunks are not considered relevant, and the sizes of
/// the chunks of data written will not necessarily match up to
/// the sizes of the chunks that are returned by the reads on
/// the other end.  The remote host might read a partial chunk,
/// or chunks might be coalesced.  For the message semantics 
/// used here, however, the sizes WILL match.  Each send call 
/// will match a successful read call on the remote host 
/// one-for-one.  If you are porting existing stream-oriented 
/// code to the semantics of reliable messages, your code should 
/// work the same, since reliable message semantics are more 
/// strict than stream semantics.  The only caveat is related to 
/// performance: there is per-message overhead to retain the 
/// messages sizes, and so if your code sends many small chunks 
/// of data, performance will suffer. Any code based on stream 
/// sockets that does not write excessively small chunks will 
/// work without any changes. 
/// Returns:
/// - k_EResultInvalidParam: invalid connection handle, or the individual message is too big.
///   (See k_cbMaxSteamNetworkingSocketsMessageSizeSend)
/// - k_EResultInvalidState: connection is in an invalid state
/// - k_EResultNoConnection: connection has ended
/// - k_EResultIgnored: You used k_nSteamNetworkingSend_NoDelay, and the message was dropped because
///   we were not ready to send it.
/// - k_EResultLimitExceeded: there was already too much data queued to be sent.
///   (See k_ESteamNetworkingConfig_SendBufferSize)
EResult Steam_NetworkingSockets::SendMessageToConnection(HSteamNetConnection hConn, const void* pData, uint32 cbData, ESteamNetworkingSendType eSendType)
{
    return SendMessageToConnection(hConn, pData, cbData, (int)eSendType);
}

EResult Steam_NetworkingSockets::SendMessageToConnection(HSteamNetConnection hConn, const void* pData, uint32 cbData, int nSendFlags)
{
    return SendMessageToConnection(hConn, pData, cbData, nSendFlags, nullptr);
}

EResult Steam_NetworkingSockets::SendMessageToConnection(HSteamNetConnection hConn, const void* pData, uint32 cbData, int nSendFlags, int64* pOutMessageNumber)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = _net_sockets.find(hConn);
    if (it == _net_sockets.end())
        return EResult::k_EResultInvalidParam;

    switch (it->second.stats.m_eState)
    {
        case ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_ClosedByPeer:
        case ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_Dead        :
        case ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_None        :
            return EResult::k_EResultNoConnection;

        case ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_Connected:
            break;

        default:
            return EResult::k_EResultInvalidState;
    }

    NetworkingSockets_Data_pb* data_pb = new NetworkingSockets_Data_pb;
    data_pb->set_local_net_id(it->first);
    data_pb->set_remote_net_id(it->second.remote_net_id);
    data_pb->set_datas(pData, cbData);
    data_pb->set_message_number(it->second.next_message_number++);

    send_data(it->second.identity.GetSteamID64(), data_pb, nSendFlags);

    return EResult::k_EResultOK;
}

/// Send one or more messages without copying the message payload.
/// This is the most efficient way to send messages. To use this
/// function, you must first allocate a message object using
/// ISteamNetworkingUtils::AllocateMessage.  (Do not declare one
/// on the stack or allocate your own.)
///
/// You should fill in the message payload.  You can either let
/// it allocate the buffer for you and then fill in the payload,
/// or if you already have a buffer allocated, you can just point
/// m_pData at your buffer and set the callback to the appropriate function
/// to free it.  Note that if you use your own buffer, it MUST remain valid
/// until the callback is executed.  And also note that your callback can be
/// invoked at ant time from any thread (perhaps even before SendMessages
/// returns!), so it MUST be fast and threadsafe.
///
/// You MUST also fill in:
/// - m_conn - the handle of the connection to send the message to
/// - m_nFlags - bitmask of k_nSteamNetworkingSend_xxx flags.
///
/// All other fields are currently reserved and should not be modified.
///
/// The library will take ownership of the message structures.  They may
/// be modified or become invalid at any time, so you must not read them
/// after passing them to this function.
///
/// pOutMessageNumberOrResult is an optional array that will receive,
/// for each message, the message number that was assigned to the message
/// if sending was successful.  If sending failed, then a negative EResult
/// valid is placed into the array.  For example, the array will hold
/// -k_EResultInvalidState if the connection was in an invalid state.
/// See ISteamNetworkingSockets::SendMessageToConnection for possible
/// failure codes.
void Steam_NetworkingSockets::SendMessages(int nMessages, SteamNetworkingMessage_t* const* pMessages, int64* pOutMessageNumberOrResult)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

}

/// If Nagle is enabled (its on by default) then when calling 
/// SendMessageToConnection the message will be queued up the Nagle time
/// before being sent to merge small messages into the same packet.
///
/// Call this function to flush any queued messages and send them immediately
/// on the next transmission time (often that means right now).
EResult Steam_NetworkingSockets::FlushMessagesOnConnection(HSteamNetConnection hConn)
{
    //LOG(Log::LogLevel::TRACE, "");
    return EResult::k_EResultOK;
}

/// Fetch the next available message(s) from the socket, if any.
/// Returns the number of messages returned into your array, up to nMaxMessages.
/// If the connection handle is invalid, -1 is returned.
///
/// The order of the messages returned in the array is relevant.
/// Reliable messages will be received in the order they were sent (and with the
/// same sizes --- see SendMessageToConnection for on this subtle difference from a stream socket).
///
/// FIXME - We're still debating the exact set of guarantees for unreliable, so this might change.
/// Unreliable messages may not be received.  The order of delivery of unreliable messages
/// is NOT specified.  They may be received out of order with respect to each other or
/// reliable messages.  They may be received multiple times!
///
/// If any messages are returned, you MUST call Release() to each of them free up resources
/// after you are done.  It is safe to keep the object alive for a little while (put it
/// into some queue, etc), and you may call Release() from any thread.
int Steam_NetworkingSockets::ReceiveMessagesOnConnection(HSteamNetConnection hConn, SteamNetworkingMessage001_t** ppOutMessages, int nMaxMessages)
{
    //LOG(Log::LogLevel::TRACE, "old");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = _net_sockets.find(hConn);
    if (it == _net_sockets.end())
        return -1;

    if (ppOutMessages == nullptr || nMaxMessages == 0)
        return 0;

    return get_old_message(it->first, &it->second, ppOutMessages, nMaxMessages);
}

int Steam_NetworkingSockets::ReceiveMessagesOnConnection(HSteamNetConnection hConn, SteamNetworkingMessage_t** ppOutMessages, int nMaxMessages)
{
    //LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = _net_sockets.find(hConn);
    if (it == _net_sockets.end())
        return -1;

    if (ppOutMessages == nullptr || nMaxMessages == 0)
        return 0;

    return get_message(it->first, &it->second, ppOutMessages, nMaxMessages);
}

/// Same as ReceiveMessagesOnConnection, but will return the next message available
/// on any client socket that was accepted through the specified listen socket.  Examine
/// SteamNetworkingMessage_t::m_conn to know which client connection.
///
/// Delivery order of messages among different clients is not defined.  They may
/// be returned in an order different from what they were actually received.  (Delivery
/// order of messages from the same client is well defined, and thus the order of the
/// messages is relevant!)
int Steam_NetworkingSockets::ReceiveMessagesOnListenSocket(HSteamListenSocket hSocket, SteamNetworkingMessage001_t** ppOutMessages, int nMaxMessages)
{
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = _listen_sockets.find(hSocket);
    if (it == _listen_sockets.end())
        return -1;

    size_t free_messages = nMaxMessages;

    for (auto net_it = _net_sockets.begin(); net_it != _net_sockets.end(); ++net_it)
    {
        if (net_it->second.parent == hSocket && !net_it->second.buffers.empty())
        {
            size_t messages_read = get_old_message(net_it->first, &net_it->second, ppOutMessages, free_messages);
            free_messages -= messages_read;
            ppOutMessages += messages_read;
            if (free_messages == 0)
                break;
        }
    }

    return nMaxMessages - free_messages;
}

int Steam_NetworkingSockets::ReceiveMessagesOnListenSocket(HSteamListenSocket hSocket, SteamNetworkingMessage_t** ppOutMessages, int nMaxMessages)
{
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = _listen_sockets.find(hSocket);
    if (it == _listen_sockets.end())
        return -1;

    size_t free_messages = nMaxMessages;

    for (auto net_it = _net_sockets.begin(); net_it != _net_sockets.end(); ++net_it)
    {
        if (net_it->second.parent == hSocket && !net_it->second.buffers.empty())
        {
            size_t messages_read = get_message(net_it->first, &net_it->second, ppOutMessages, free_messages);
            free_messages -= messages_read;
            ppOutMessages += messages_read;
            if (free_messages == 0)
                break;
        }
    }

    return nMaxMessages - free_messages;
}

/// Returns information about the specified connection.
bool Steam_NetworkingSockets::GetConnectionInfo(HSteamNetConnection hConn, SteamNetConnectionInfo001_t* pInfo)
{
    APP_LOG(Log::LogLevel::TRACE, "old");

    if (pInfo == nullptr)
        return false;

    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = _net_sockets.find(hConn);
    if (it == _net_sockets.end())
        return false;

    memset(pInfo, 0, sizeof(*pInfo));
    pInfo->m_nUserData = it->second.user_data;
    pInfo->m_eState = it->second.stats.m_eState;
    pInfo->m_steamIDRemote = it->second.identity.GetSteamID();

    return true;
}

bool Steam_NetworkingSockets::GetConnectionInfo(HSteamNetConnection hConn, SteamNetConnectionInfo_t* pInfo)
{
    TRACE_FUNC();

    if (pInfo == nullptr)
        return false;

    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = _net_sockets.find(hConn);
    if (it == _net_sockets.end())
        return false;

    auto addr = _network->get_steamid_addr(it->second.identity.GetSteamID64());

    memset(pInfo, 0, sizeof(*pInfo));
    pInfo->m_nUserData = it->second.user_data;
    pInfo->m_eState = it->second.stats.m_eState;
    pInfo->m_identityRemote.SetSteamID(it->second.identity.GetSteamID());
    pInfo->m_hListenSocket = it->second.parent;
    pInfo->m_addrRemote.SetIPv4(addr.get_ip(), addr.get_port());

    return true;
}

/// Returns brief set of connection status that you might want to display
/// to the user in game.
bool Steam_NetworkingSockets::GetQuickConnectionStatus(HSteamNetConnection hConn, SteamNetworkingQuickConnectionStatus* pStats)
{
    TRACE_FUNC();

    auto it = _net_sockets.find(hConn);
    if (it == _net_sockets.end())
        return false;

    if (pStats != nullptr)
    {
        *pStats = it->second.stats;
    }

    return true;
}

/// Returns detailed connection stats in text format.  Useful
/// for dumping to a log, etc.
///
/// Returns:
/// -1 failure (bad connection handle)
/// 0 OK, your buffer was filled in and '\0'-terminated
/// >0 Your buffer was either nullptr, or it was too small and the text got truncated.  Try again with a buffer of at least N bytes.
int Steam_NetworkingSockets::GetDetailedConnectionStatus(HSteamNetConnection hConn, char* pszBuf, int cbBuf)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = _net_sockets.find(hConn);
    if(it == _net_sockets.end())
        return -1;

    constexpr size_t len = utils::static_strlen("EmuConnStatus") + 1;

    if (pszBuf == nullptr || cbBuf < len)
        return len;

    strncpy(pszBuf, "EmuConnStatus", len);

    return 0;
}

/// Returns local IP and port that a listen socket created using CreateListenSocketIP is bound to.
///
/// An IPv6 address of ::0 means "any IPv4 or IPv6"
/// An IPv6 address of ::ffff:0000:0000 means "any IPv4"
bool Steam_NetworkingSockets::GetListenSocketAddress(HSteamListenSocket hSocket, SteamNetworkingIPAddr* address)
{
    TRACE_FUNC();
    return false;
}

/// Create a pair of connections that are talking to each other, e.g. a loopback connection.
/// This is very useful for testing, or so that your client/server code can work the same
/// even when you are running a local "server".
///
/// The two connections will immediately be placed into the connected state, and no callbacks
/// will be posted immediately.  After this, if you close either connection, the other connection
/// will receive a callback, exactly as if they were communicating over the network.  You must
/// close *both* sides in order to fully clean up the resources!
///
/// By default, internal buffers are used, completely bypassing the network, the chopping up of
/// messages into packets, encryption, copying the payload, etc.  This means that loopback
/// packets, by default, will not simulate lag or loss.  Passing true for bUseNetworkLoopback will
/// cause the socket pair to send packets through the local network loopback device (127.0.0.1)
/// on ephemeral ports.  Fake lag and loss are supported in this case, and CPU time is expended
/// to encrypt and decrypt.
///
/// If you wish to assign a specific identity to either connection, you may pass a particular
/// identity.  Otherwise, if you pass nullptr, the respective connection will assume a generic
/// "localhost" identity.  If you use real network loopback, this might be translated to the
/// actual bound loopback port.  Otherwise, the port will be zero.
bool Steam_NetworkingSockets::CreateSocketPair(HSteamNetConnection* pOutConnection1, HSteamNetConnection* pOutConnection2, bool bUseNetworkLoopback)
{
    return CreateSocketPair(pOutConnection1, pOutConnection2, bUseNetworkLoopback, nullptr, nullptr);
}

bool Steam_NetworkingSockets::CreateSocketPair(HSteamNetConnection* pOutConnection1, HSteamNetConnection* pOutConnection2, bool bUseNetworkLoopback, const SteamNetworkingIdentity* pIdentity1, const SteamNetworkingIdentity* pIdentity2)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (pOutConnection1 == nullptr || pOutConnection2 == nullptr)
        return false;

    //SteamNetworkingIdentity identity;
    //identity.SetSteamID(Settings::Inst().userid);
    //HSteamNetConnection conn1 = create_net_socket(identity, 0);
    //HSteamNetConnection conn2 = create_net_socket(identity, 0);

    //auto& net_socket1 = _net_sockets[conn1];
    //net_socket1.remote_net_id = conn2;
    //net_socket1.stats.m_eState = ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_Connected;

    //auto& net_socket2 = _net_sockets[conn2];
    //net_socket2.remote_net_id = conn1;
    //net_socket2.stats.m_eState = ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_Connected;

    //*pOutConnection1 = conn1;
    //*pOutConnection2 = conn2;

    return false;
}

/// Get the identity assigned to this interface.
/// E.g. on Steam, this is the user's SteamID, or for the gameserver interface, the SteamID assigned
/// to the gameserver.  Returns false and sets the result to an invalid identity if we don't know
/// our identity yet.  (E.g. GameServer has not logged in.  On Steam, the user will know their SteamID
/// even if they are not signed into Steam.)
bool Steam_NetworkingSockets::GetIdentity(SteamNetworkingIdentity* pIdentity)
{
    TRACE_FUNC();
    //GLOBAL_LOCK();

    if (pIdentity != nullptr)
    {
        pIdentity->SetSteamID64(uint64(_network->get_peer_id()));
    }
    return true;
}

/// Indicate our desire to be ready participate in authenticated communications.
/// If we are currently not ready, then steps will be taken to obtain the necessary
/// certificates.   (This includes a certificate for us, as well as any CA certificates
/// needed to authenticate peers.)
///
/// You can call this at program init time if you know that you are going to
/// be making authenticated connections, so that we will be ready immediately when
/// those connections are attempted.  (Note that essentially all connections require
/// authentication, with the exception of ordinary UDP connections with authentication
/// disabled using k_ESteamNetworkingConfig_IP_AllowWithoutAuth.)  If you don't call
/// this function, we will wait until a feature is utilized that that necessitates
/// these resources.
///
/// You can also call this function to force a retry, if failure has occurred.
/// Once we make an attempt and fail, we will not automatically retry.
/// In this respect, the behavior of the system after trying and failing is the same
/// as before the first attempt: attempting authenticated communication or calling
/// this function will call the system to attempt to acquire the necessary resources.
///
/// You can use GetAuthenticationStatus or listen for SteamNetAuthenticationStatus_t
/// to monitor the status.
///
/// Returns the current value that would be returned from GetAuthenticationStatus.
ESteamNetworkingAvailability Steam_NetworkingSockets::InitAuthentication()
{
    TRACE_FUNC();
    return ESteamNetworkingAvailability::k_ESteamNetworkingAvailability_Current;
}

/// Query our readiness to participate in authenticated communications.  A
/// SteamNetAuthenticationStatus_t callback is posted any time this status changes,
/// but you can use this function to query it at any time.
///
/// The value of SteamNetAuthenticationStatus_t::m_eAvail is returned.  If you only
/// want this high level status, you can pass NULL for pDetails.  If you want further
/// details, pass non-NULL to receive them.
ESteamNetworkingAvailability Steam_NetworkingSockets::GetAuthenticationStatus(SteamNetAuthenticationStatus_t* pDetails)
{
    TRACE_FUNC();
    return ESteamNetworkingAvailability::k_ESteamNetworkingAvailability_Current;
}

//
// Poll groups.  A poll group is a set of connections that can be polled efficiently.
// (In our API, to "poll" a connection means to retrieve all pending messages.  We
// actually don't have an API to "poll" the connection *state*, like BSD sockets.)
//

/// Create a new poll group.
///
/// You should destroy the poll group when you are done using DestroyPollGroup
HSteamNetPollGroup Steam_NetworkingSockets::CreatePollGroup()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    return create_poll_group();
}

/// Destroy a poll group created with CreatePollGroup().
///
/// If there are any connections in the poll group, they are removed from the group,
/// and left in a state where they are not part of any poll group.
/// Returns false if passed an invalid poll group handle.
bool Steam_NetworkingSockets::DestroyPollGroup(HSteamNetPollGroup hPollGroup)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto it = _net_poll_groups.find(hPollGroup);
    if (it == _net_poll_groups.end())
        return false;

    for (auto& net_sock : it->second)
    {
        net_sock->poll_group = k_HSteamNetPollGroup_Invalid;
    }

    _net_poll_groups.erase(it);
    return true;
}

/// Assign a connection to a poll group.  Note that a connection may only belong to a
/// single poll group.  Adding a connection to a poll group implicitly removes it from
/// any other poll group it is in.
///
/// You can pass k_HSteamNetPollGroup_Invalid to remove a connection from its current
/// poll group without adding it to a new poll group.
///
/// If there are received messages currently pending on the connection, an attempt
/// is made to add them to the queue of messages for the poll group in approximately
/// the order that would have applied if the connection was already part of the poll
/// group at the time that the messages were received.
///
/// Returns false if the connection handle is invalid, or if the poll group handle
/// is invalid (and not k_HSteamNetPollGroup_Invalid).
bool Steam_NetworkingSockets::SetConnectionPollGroup(HSteamNetConnection hConn, HSteamNetPollGroup hPollGroup)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto net_it = _net_sockets.find(hConn);
    if (net_it == _net_sockets.end())
        return false;

    net_socket_t* net_sock = &net_it->second;

    if (hPollGroup != k_HSteamNetPollGroup_Invalid)
    {
        auto poll_it = _net_poll_groups.find(hPollGroup);
        if (poll_it == _net_poll_groups.end())
            return false;

        poll_it->second.insert(net_sock);
    }

    if (net_sock->poll_group != k_HSteamNetPollGroup_Invalid)
    {
        auto it = _net_poll_groups.find(net_sock->poll_group);
        if (it != _net_poll_groups.end())
        {
            it->second.erase(net_sock);
        }
    }

    net_sock->poll_group = hPollGroup;
    return true;
}

/// Same as ReceiveMessagesOnConnection, but will return the next messages available
/// on any connection in the poll group.  Examine SteamNetworkingMessage_t::m_conn
/// to know which connection.  (SteamNetworkingMessage_t::m_nConnUserData might also
/// be useful.)
///
/// Delivery order of messages among different connections will usually match the
/// order that the last packet was received which completed the message.  But this
/// is not a strong guarantee, especially for packets received right as a connection
/// is being assigned to poll group.
///
/// Delivery order of messages on the same connection is well defined and the
/// same guarantees are present as mentioned in ReceiveMessagesOnConnection.
/// (But the messages are not grouped by connection, so they will not necessarily
/// appear consecutively in the list; they may be interleaved with messages for
/// other connections.)
int Steam_NetworkingSockets::ReceiveMessagesOnPollGroup(HSteamNetPollGroup hPollGroup, SteamNetworkingMessage_t** ppOutMessages, int nMaxMessages)
{
    //TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto poll_it = _net_poll_groups.find(hPollGroup);
    if (poll_it == _net_poll_groups.end())
        return 0;

    size_t messages_read = 0;
    size_t read_loop = 1;

    // Messages may be interleaved, so loop all sockets on poll
    while (read_loop != 0 && messages_read != nMaxMessages)
    {
        read_loop = messages_read;
        for (auto& net_sock : poll_it->second)
        {
            if (!net_sock->buffers.empty())
            {
                messages_read += get_message(net_sock->parent, net_sock, &ppOutMessages[messages_read], std::min<size_t>(2, nMaxMessages - messages_read));
                if (messages_read == nMaxMessages)
                    break;
            }
        }
        read_loop = messages_read - read_loop;
    }

    return static_cast<int>(messages_read);
}

/// Returns information about the listen socket.
///
/// *pnIP and *pnPort will be 0 if the socket is set to listen for connections based
/// on SteamID only.  If your listen socket accepts connections on IPv4, then both
/// fields will return nonzero, even if you originally passed a zero IP.  However,
/// note that the address returned may be a private address (e.g. 10.0.0.x or 192.168.x.x),
/// and may not be reachable by a general host on the Internet.
bool Steam_NetworkingSockets::GetListenSocketInfo(HSteamListenSocket hSocket, uint32* pnIP, uint16* pnPort)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");

    auto it = _listen_sockets.find(hSocket);
    if (it == _listen_sockets.end())
        return false;

    if (pnIP != nullptr)
        *pnIP = 0;
    if (pnPort != nullptr)
        *pnPort = 0;

    return true;
}

//
// Clients connecting to dedicated servers hosted in a data center,
// using central-authority-granted tickets.
//

/// Called when we receive a ticket from our central matchmaking system.  Puts the
/// ticket into a persistent cache, and optionally returns the parsed ticket.
///
/// See stamdatagram_ticketgen.h for more details.
bool Steam_NetworkingSockets::ReceivedRelayAuthTicket(const void* pvTicket, int cbTicket, SteamDatagramRelayAuthTicket* pOutParsedTicket)
{
    TRACE_FUNC();
    return 0;
}

/// Search cache for a ticket to talk to the server on the specified port.
/// If found, returns the number of second until the ticket expires, and optionally
/// the complete cracked ticket.  Returns 0 if we don't have a ticket.
///
/// Typically this is useful just to confirm that you have a ticket, before you
/// call ConnectToHostedDedicatedServer to connect to the server.
int Steam_NetworkingSockets::FindRelayAuthTicketForServer(CSteamID steamID, int nVirtualPort, SteamDatagramRelayAuthTicket* pOutParsedTicket)
{
    SteamNetworkingIdentity identity;
    identity.SetSteamID(steamID);
    return FindRelayAuthTicketForServer(identity, nVirtualPort, pOutParsedTicket);
}

int Steam_NetworkingSockets::FindRelayAuthTicketForServer(const SteamNetworkingIdentity& identityGameServer, int nVirtualPort, SteamDatagramRelayAuthTicket* pOutParsedTicket)
{
    TRACE_FUNC();
    return 0;
}

/// Client call to connect to a server hosted in a Valve data center, on the specified virtual
/// port.  You should have received a ticket for this server, or else this connect call will fail!
///
/// You may wonder why tickets are stored in a cache, instead of simply being passed as an argument
/// here.  The reason is to make reconnection to a gameserver robust, even if the client computer loses
/// connection to Steam or the central backend, or the app is restarted or crashes, etc.
HSteamNetConnection Steam_NetworkingSockets::ConnectToHostedDedicatedServer(CSteamID steamIDTarget, int nVirtualPort)
{
    SteamNetworkingIdentity identity;
    identity.SetSteamID(steamIDTarget);
    return ConnectToHostedDedicatedServer(identity, nVirtualPort);
}

HSteamNetConnection Steam_NetworkingSockets::ConnectToHostedDedicatedServer(const SteamNetworkingIdentity& identityTarget, int nVirtualPort)
{
    return ConnectToHostedDedicatedServer(identityTarget, nVirtualPort, 0, nullptr);
}

HSteamNetConnection Steam_NetworkingSockets::ConnectToHostedDedicatedServer(const SteamNetworkingIdentity& identityTarget, int nVirtualPort, int nOptions, const SteamNetworkingConfigValue_t* pOptions)
{
    TRACE_FUNC();
    return k_HSteamNetConnection_Invalid;
}

//
// Servers hosted in Valve data centers
//

/// Returns the value of the SDR_LISTEN_PORT environment variable.
uint16 Steam_NetworkingSockets::GetHostedDedicatedServerPort()
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    return 27054;
}

/// If you are running in a production data center, this will return the data
/// center code.  Returns 0 otherwise.
SteamNetworkingPOPID Steam_NetworkingSockets::GetHostedDedicatedServerPOPID()
{
    TRACE_FUNC();
    return 0;
}

/// Return info about the hosted server.  You will need to send this information to your
/// backend, and put it in tickets, so that the relays will know how to forward traffic from
/// clients to your server.  See SteamDatagramRelayAuthTicket for more info.
///
/// NOTE ABOUT DEVELOPMENT ENVIRONMENTS:
/// In production in our data centers, these parameters are configured via environment variables.
/// In development, the only one you need to set is SDR_LISTEN_PORT, which is the local port you
/// want to listen on.  Furthermore, if you are running your server behind a corporate firewall,
/// you probably will not be able to put the routing information returned by this function into
/// tickets.   Instead, it should be a public internet address that the relays can use to send
/// data to your server.  So you might just end up hardcoding a public address and setup port
/// forwarding on your corporate firewall.  In that case, the port you put into the ticket
/// needs to be the public-facing port opened on your firewall, if it is different from the
/// actual server port.
///
/// This function will fail if SteamDatagramServer_Init has not been called.
///
/// Returns false if the SDR_LISTEN_PORT environment variable is not set.
bool Steam_NetworkingSockets::GetHostedDedicatedServerAddress001(SteamDatagramHostedAddress* pRouting)
{
    TRACE_FUNC();
    return GetHostedDedicatedServerAddress(pRouting) == k_EResultOK;
}

EResult Steam_NetworkingSockets::GetHostedDedicatedServerAddress(SteamDatagramHostedAddress* pRouting)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    //GLOBAL_LOCK();

    if (pRouting != nullptr)
    {
        pRouting->SetDevAddress(0x7F000001, 27054);
    }
    return EResult::k_EResultOK;
}

/// Create a listen socket on the specified port.  The physical UDP port to use
/// will be determined by the SDR_LISTEN_PORT environment variable.  If a UDP port is not
/// configured, this call will fail.
///
/// Note that this call MUST be made through the SteamNetworkingSocketsGameServer() interface
HSteamListenSocket Steam_NetworkingSockets::CreateHostedDedicatedServerListenSocket(int nVirtualPort)
{
    return CreateHostedDedicatedServerListenSocket(nVirtualPort, 0, nullptr);
}

HSteamListenSocket Steam_NetworkingSockets::CreateHostedDedicatedServerListenSocket(int nVirtualPort, int nOptions, const SteamNetworkingConfigValue_t* pOptions)
{
    APP_LOG(Log::LogLevel::TRACE, "TODO");
    return k_HSteamListenSocket_Invalid;
}

/// Generate an authentication blob that can be used to securely login with
/// your backend, using SteamDatagram_ParseHostedServerLogin.  (See
/// steamdatagram_gamecoordinator.h)
///
/// Before calling the function:
/// - Populate the app data in pLoginInfo (m_cbAppData and m_appData).  You can leave
///   all other fields uninitialized.
/// - *pcbSignedBlob contains the size of the buffer at pBlob.  (It should be
///   at least k_cbMaxSteamDatagramGameCoordinatorServerLoginSerialized.)
///
/// On a successful exit:
/// - k_EResultOK is returned
/// - All of the remaining fields of pLoginInfo will be filled out.
/// - *pcbSignedBlob contains the size of the serialized blob that has been
///   placed into pBlob.
///
/// Unsuccessful exit:
/// - Something other than k_EResultOK is returned.
/// - k_EResultNotLoggedOn: you are not logged in (yet)
/// - See GetHostedDedicatedServerAddress for more potential failure return values.
/// - A non-localized diagnostic debug message will be placed in pBlob that describes
///   the cause of the failure.
///
/// This works by signing the contents of the SteamDatagramGameCoordinatorServerLogin
/// with the cert that is issued to this server.  In dev environments, it's OK if you do
/// not have a cert.  (You will need to enable insecure dev login in SteamDatagram_ParseHostedServerLogin.)
/// Otherwise, you will need a signed cert.
///
/// NOTE: The routing blob returned here is not encrypted.  Send it to your backend
///       and don't share it directly with clients.
EResult Steam_NetworkingSockets::GetGameCoordinatorServerLogin(SteamDatagramGameCoordinatorServerLogin* pLoginInfo, int* pcbSignedBlob, void* pBlob)
{
    TRACE_FUNC();
    return k_EResultFail;
}

//
// Relayed connections using custom signaling protocol
//
// This is used if you have your own method of sending out-of-band
// signaling / rendezvous messages through a mutually trusted channel.
//

/// Create a P2P "client" connection that does signaling over a custom
/// rendezvous/signaling channel.
///
/// pSignaling points to a new object that you create just for this connection.
/// It must stay valid until Release() is called.  Once you pass the
/// object to this function, it assumes ownership.  Release() will be called
/// from within the function call if the call fails.  Furthermore, until Release()
/// is called, you should be prepared for methods to be invoked on your
/// object from any thread!  You need to make sure your object is threadsafe!
/// Furthermore, you should make sure that dispatching the methods is done
/// as quickly as possible.
///
/// This function will immediately construct a connection in the "connecting"
/// state.  Soon after (perhaps before this function returns, perhaps in another thread),
/// the connection will begin sending signaling messages by calling
/// ISteamNetworkingConnectionCustomSignaling::SendSignal.
///
/// When the remote peer accepts the connection (See
/// ISteamNetworkingCustomSignalingRecvContext::OnConnectRequest),
/// it will begin sending signaling messages.  When these messages are received,
/// you can pass them to the connection using ReceivedP2PCustomSignal.
///
/// If you know the identity of the peer that you expect to be on the other end,
/// you can pass their identity to improve debug output or just detect bugs.
/// If you don't know their identity yet, you can pass NULL, and their
/// identity will be established in the connection handshake.  
///
/// If you use this, you probably want to call ISteamNetworkingUtils::InitRelayNetworkAccess()
/// when your app initializes
///
/// If you need to set any initial config options, pass them here.  See
/// SteamNetworkingConfigValue_t for more about why this is preferable to
/// setting the options "immediately" after creation.
HSteamNetConnection Steam_NetworkingSockets::ConnectP2PCustomSignaling(ISteamNetworkingConnectionCustomSignaling* pSignaling, const SteamNetworkingIdentity* pPeerIdentity, int nOptions, const SteamNetworkingConfigValue_t* pOptions)
{
    return ConnectP2PCustomSignaling(pSignaling, pPeerIdentity, 0, nOptions, pOptions);
}

HSteamNetConnection Steam_NetworkingSockets::ConnectP2PCustomSignaling(ISteamNetworkingConnectionCustomSignaling* pSignaling, const SteamNetworkingIdentity* pPeerIdentity, int nRemoteVirtualPort, int nOptions, const SteamNetworkingConfigValue_t* pOptions)
{
    TRACE_FUNC();
    return k_HSteamNetConnection_Invalid;
}

/// Called when custom signaling has received a message.  When your
/// signaling channel receives a message, it should save off whatever
/// routing information was in the envelope into the context object,
/// and then pass the payload to this function.
///
/// A few different things can happen next, depending on the message:
///
/// - If the signal is associated with existing connection, it is dealt
///   with immediately.  If any replies need to be sent, they will be
///   dispatched using the ISteamNetworkingConnectionCustomSignaling
///   associated with the connection.
/// - If the message represents a connection request (and the request
///   is not redundant for an existing connection), a new connection
///   will be created, and ReceivedConnectRequest will be called on your
///   context object to determine how to proceed.
/// - Otherwise, the message is for a connection that does not
///   exist (anymore).  In this case, we *may* call SendRejectionReply
///   on your context object.
///
/// In any case, we will not save off pContext or access it after this
/// function returns.
///
/// Returns true if the message was parsed and dispatched without anything
/// unusual or suspicious happening.  Returns false if there was some problem
/// with the message that prevented ordinary handling.  (Debug output will
/// usually have more information.)
///
/// If you expect to be using relayed connections, then you probably want
/// to call ISteamNetworkingUtils::InitRelayNetworkAccess() when your app initializes
bool Steam_NetworkingSockets::ReceivedP2PCustomSignal(const void* pMsg, int cbMsg, ISteamNetworkingCustomSignalingRecvContext* pContext)
{
    TRACE_FUNC();
    return false;
}

//
// Certificate provision by the application.  On Steam, we normally handle all this automatically
// and you will not need to use these advanced functions.
//

/// Get blob that describes a certificate request.  You can send this to your game coordinator.
/// Upon entry, *pcbBlob should contain the size of the buffer.  On successful exit, it will
/// return the number of bytes that were populated.  You can pass pBlob=NULL to query for the required
/// size.  (256 bytes is a very conservative estimate.)
///
/// Pass this blob to your game coordinator and call SteamDatagram_CreateCert.
bool Steam_NetworkingSockets::GetCertificateRequest(int* pcbBlob, void* pBlob, SteamNetworkingErrMsg& errMsg)
{
    TRACE_FUNC();
    return false;
}

/// Set the certificate.  The certificate blob should be the output of
/// SteamDatagram_CreateCert.
bool Steam_NetworkingSockets::SetCertificate(const void* pCertificate, int cbCertificate, SteamNetworkingErrMsg& errMsg)
{
    TRACE_FUNC();
    return false;
}

//
// Gets some debug text from the connection
//
bool Steam_NetworkingSockets::GetConnectionDebugText(HSteamNetConnection hConn, char* pOut, int nOutCCH)
{
    TRACE_FUNC();
    return false;
}

//
// Set and get configuration values, see ESteamNetworkingConfigurationValue for individual descriptions.
//
// Returns the value or -1 is eConfigValue is invalid
int32 Steam_NetworkingSockets::GetConfigurationValue(ESteamNetworkingConfigurationValue eConfigValue)
{
    TRACE_FUNC();
    return -1;
}

// Returns true if successfully set
bool Steam_NetworkingSockets::SetConfigurationValue(ESteamNetworkingConfigurationValue eConfigValue, int32 nValue)
{
    TRACE_FUNC();
    return false;
}

// Return the name of an int configuration value, or NULL if config value isn't known
const char* Steam_NetworkingSockets::GetConfigurationValueName(ESteamNetworkingConfigurationValue eConfigValue)
{
    TRACE_FUNC();
    return nullptr;
}

//
// Set and get configuration strings, see ESteamNetworkingConfigurationString for individual descriptions.
//
// Get the configuration string, returns length of string needed if pDest is nullpr or destSize is 0
// returns -1 if the eConfigValue is invalid
int32 Steam_NetworkingSockets::GetConfigurationString(ESteamNetworkingConfigurationString eConfigString, char* pDest, int32 destSize)
{
    TRACE_FUNC();
    return -1;
}

bool Steam_NetworkingSockets::SetConfigurationString(ESteamNetworkingConfigurationString eConfigString, const char* pString)
{
    TRACE_FUNC();
    return false;
}

// Return the name of a string configuration value, or NULL if config value isn't known
const char* Steam_NetworkingSockets::GetConfigurationStringName(ESteamNetworkingConfigurationString eConfigString)
{
    TRACE_FUNC();
    return 0;
}

//
// Set and get configuration values, see ESteamNetworkingConnectionConfigurationValue for individual descriptions.
//
// Returns the value or -1 is eConfigValue is invalid
int32 Steam_NetworkingSockets::GetConnectionConfigurationValue(HSteamNetConnection hConn, ESteamNetworkingConnectionConfigurationValue eConfigValue)
{
    TRACE_FUNC();
    return -1;
}

// Returns true if successfully set
bool Steam_NetworkingSockets::SetConnectionConfigurationValue(HSteamNetConnection hConn, ESteamNetworkingConnectionConfigurationValue eConfigValue, int32 nValue)
{
    TRACE_FUNC();
    return false;
}

// TEMP KLUDGE Call to invoke all queued callbacks.
// Eventually this function will go away, and callwacks will be ordinary Steamworks callbacks.
// You should call this at the same time you call SteamAPI_RunCallbacks and SteamGameServer_RunCallbacks
// to minimize potential changes in timing when that change happens.
void Steam_NetworkingSockets::RunCallbacks(ISteamNetworkingSocketsCallbacks* pCallbacks)
{

}

/// Invoke all callback functions queued for this interface.
/// See k_ESteamNetworkingConfig_Callback_ConnectionStatusChanged, etc
///
/// You don't need to call this if you are using Steam's callback dispatch
/// mechanism (SteamAPI_RunCallbacks and SteamGameserver_RunCallbacks).
void Steam_NetworkingSockets::RunCallbacks()
{

}

///////////////////////////////////////////////////////////////////////////////
//                           Network Send messages                           //
///////////////////////////////////////////////////////////////////////////////
bool Steam_NetworkingSockets::send_connect(uint64 remote_id, NetworkingSockets_Connect_pb* connect_pb)
{
    TRACE_FUNC();

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    // Request the user infos
    Network_Message_pb msg;
    Steam_NetworkingSockets_pb* socket_pb = new Steam_NetworkingSockets_pb;

    socket_pb->set_allocated_connect(connect_pb);

    msg.set_allocated_networking_socket(socket_pb);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

bool Steam_NetworkingSockets::send_accept(uint64 remote_id, NetworkingSockets_Accept_pb* accept_pb)
{
    TRACE_FUNC();

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    // Request the user infos
    Network_Message_pb msg;
    Steam_NetworkingSockets_pb* socket_pb = new Steam_NetworkingSockets_pb;

    socket_pb->set_allocated_accept(accept_pb);

    msg.set_allocated_networking_socket(socket_pb);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

bool Steam_NetworkingSockets::send_close(uint64 remote_id, NetworkingSockets_Close_pb* close_pb)
{
    TRACE_FUNC();

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    // Request the user infos
    Network_Message_pb msg;
    Steam_NetworkingSockets_pb* socket_pb = new Steam_NetworkingSockets_pb;

    socket_pb->set_allocated_close(close_pb);

    msg.set_allocated_networking_socket(socket_pb);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    return _network->ReliableSendTo(msg);
}

bool Steam_NetworkingSockets::send_data(uint64 remote_id, NetworkingSockets_Data_pb* data_pb, int send_type)
{
    //TRACE_FUNC();

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    // Request the user infos
    Network_Message_pb msg;
    Steam_NetworkingSockets_pb* socket_pb = new Steam_NetworkingSockets_pb;

    socket_pb->set_allocated_data(data_pb);

    msg.set_allocated_networking_socket(socket_pb);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    if (send_type == k_nSteamNetworkingSend_Unreliable)
        return _network->UnreliableSendTo(msg);
    else
        return _network->ReliableSendTo(msg);
}

bool Steam_NetworkingSockets::send_ack(uint64 remote_id, NetworkingSockets_Ack_pb* ack_pb, int send_type)
{
    //TRACE_FUNC();

    uint64 steam_id = Settings::Inst().userid.ConvertToUint64();

    // Request the user infos
    Network_Message_pb msg;
    Steam_NetworkingSockets_pb* socket_pb = new Steam_NetworkingSockets_pb;

    socket_pb->set_allocated_ack(ack_pb);

    msg.set_allocated_networking_socket(socket_pb);

    msg.set_source_id(steam_id);
    msg.set_dest_id(remote_id);
    msg.set_app_id(Settings::Inst().gameid.AppID());

    if (send_type == k_nSteamNetworkingSend_Unreliable)
        return _network->UnreliableSendTo(msg);
    else
        return _network->ReliableSendTo(msg);
}

///////////////////////////////////////////////////////////////////////////////
//                          Network Receive messages                         //
///////////////////////////////////////////////////////////////////////////////
bool Steam_NetworkingSockets::on_peer_disconnect(Network_Message_pb const& msg, Network_Peer_Disconnect_pb const& conn)
{
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    auto it = std::find_if(_net_sockets.begin(), _net_sockets.end(), [&msg](std::pair<HSteamNetConnection const, net_socket_t> &item)
    {
        if (item.second.identity.m_eType != ESteamNetworkingIdentityType::k_ESteamNetworkingIdentityType_SteamID)
            return false;

        return item.second.identity.GetSteamID64() == msg.source_id();
    });

    if (it != _net_sockets.end())
    {
        trigger_net_callback(it->first, it->second, ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_ProblemDetectedLocally, ESteamNetConnectionEnd::k_ESteamNetConnectionEnd_Remote_Timeout);
    }

    return true;
}

bool Steam_NetworkingSockets::on_connect(Network_Message_pb const& msg, NetworkingSockets_Connect_pb const& connect_pb)
{
    TRACE_FUNC();
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    auto it = get_listen_from_port(connect_pb.virtual_port());
    if (it != _listen_sockets.end())
    {
        // local_net_id is the local id of the message sent. So for us, its the remote_net_id
        if (get_net_from_remote_id(msg.source_id(), connect_pb.local_net_id()) == _net_sockets.end())
        {
            SteamNetworkingIdentity identity = {};
            identity.SetSteamID64(msg.source_id());

            HSteamNetConnection hSocket = create_net_socket(identity, connect_pb.virtual_port(), it->first);
            if (hSocket == k_HSteamNetConnection_Invalid)
            {
                NetworkingSockets_Accept_pb* accept_pb = new NetworkingSockets_Accept_pb;
                accept_pb->set_state(NetworkingSockets_Accept_pb_State::NetworkingSockets_Accept_pb_State_refused);
                accept_pb->set_local_net_id(hSocket);
                accept_pb->set_remote_net_id(connect_pb.local_net_id());
                send_accept(msg.source_id(), accept_pb);
            }
            else
            {
                auto& infos = _net_sockets[hSocket];
                infos.stats.m_eState = ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_None;
                infos.remote_net_id = connect_pb.local_net_id();
                trigger_net_callback(hSocket, infos, ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_Connecting);
            }
        }
    }
    else
    {
        NetworkingSockets_Accept_pb* accept_pb = new NetworkingSockets_Accept_pb;
        accept_pb->set_state(NetworkingSockets_Accept_pb_State::NetworkingSockets_Accept_pb_State_refused);
        accept_pb->set_local_net_id(0);
        accept_pb->set_remote_net_id(connect_pb.local_net_id());
        send_accept(msg.source_id(), accept_pb);
    }

    return true;
}

bool Steam_NetworkingSockets::on_accept(Network_Message_pb const& msg, NetworkingSockets_Accept_pb const& accept_pb)
{
    TRACE_FUNC();
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    // remote_net_id is the remote id of the message sent. So for us, its the local_net_id
    auto it = get_net_from_local_id(msg.source_id(), accept_pb.remote_net_id());

    if (it != _net_sockets.end() && it->second.stats.m_eState == ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_Connecting)
    {
        it->second.remote_net_id = accept_pb.local_net_id();
        if (accept_pb.state() == NetworkingSockets_Accept_pb_State_accepted)
        {
            trigger_net_callback(it->first, it->second, ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_Connected);
        }
        else
        {
            trigger_net_callback(it->first, it->second, ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_ClosedByPeer);
        }
    }

    return true;
}

bool Steam_NetworkingSockets::on_close(Network_Message_pb const& msg, NetworkingSockets_Close_pb const& close_pb)
{
    TRACE_FUNC();
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    auto it = get_net_from_local_id(msg.source_id(), close_pb.remote_net_id());

    if (it != _net_sockets.end())
    {
        switch (it->second.stats.m_eState)
        {
            case ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_Connected:
            case ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_Connecting:
                trigger_net_callback(it->first, it->second, ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_ClosedByPeer);
                break;
        }
    }

    return true;
}

bool Steam_NetworkingSockets::on_data(Network_Message_pb const& msg, NetworkingSockets_Data_pb const& data_pb)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = get_net_from_local_id(msg.source_id(), data_pb.remote_net_id());

    if (it != _net_sockets.end())
    {
        socket_buffer_t buffer;
        buffer.datas = data_pb.datas();
        buffer.received_time = std::chrono::microseconds(msg.timestamp());
        buffer.message_number = data_pb.message_number();

        it->second.buffers.emplace(std::move(buffer));

        NetworkingSockets_Ack_pb* ack = new NetworkingSockets_Ack_pb;

        ack->set_local_net_id(data_pb.remote_net_id());
        ack->set_remote_net_id(data_pb.local_net_id());

        send_ack(msg.source_id(), ack, k_nSteamNetworkingSend_Reliable);
    }

    return true;
}

bool Steam_NetworkingSockets::on_ack(Network_Message_pb const& msg, NetworkingSockets_Ack_pb const& ack_pb)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    auto it = get_net_from_local_id(msg.source_id(), ack_pb.remote_net_id());

    if (it != _net_sockets.end())
    {
        std::chrono::microseconds sent_timestamp(msg.timestamp());

        it->second.stats.m_nPing = (
            std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()) -
            std::chrono::microseconds(msg.timestamp())).count();
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//                                 IRunCallback                                 //
///////////////////////////////////////////////////////////////////////////////
// Maybe delegate this to a thread
bool Steam_NetworkingSockets::CBRunFrame()
{
    //LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    auto now = std::chrono::steady_clock::now();
    for (auto& net : _net_sockets)
    {
        if (net.second.parent == k_HSteamListenSocket_Invalid &&
            net.second.stats.m_eState == ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_Connecting)
        {
            if ((now - net.second.connect_start) >= connect_timeout)
            {
                trigger_net_callback(net.first, net.second, ESteamNetworkingConnectionState::k_ESteamNetworkingConnectionState_ProblemDetectedLocally, ESteamNetConnectionEnd::k_ESteamNetConnectionEnd_Remote_Timeout);
            }
        }
    }

    return true;
}

bool Steam_NetworkingSockets::RunNetwork(Network_Message_pb const& msg)
{
    Steam_NetworkingSockets_pb const& socket_msg = msg.networking_socket();

    switch (socket_msg.messages_case())
    {
        case Steam_NetworkingSockets_pb::MessagesCase::kAccept : return on_accept (msg, socket_msg.accept());
        case Steam_NetworkingSockets_pb::MessagesCase::kConnect: return on_connect(msg, socket_msg.connect());
        case Steam_NetworkingSockets_pb::MessagesCase::kClose  : return on_close  (msg, socket_msg.close());
        case Steam_NetworkingSockets_pb::MessagesCase::kData   : return on_data   (msg, socket_msg.data());
        case Steam_NetworkingSockets_pb::MessagesCase::kAck    : return on_ack    (msg, socket_msg.ack());
        default: APP_LOG(Log::LogLevel::WARN, "Message unhandled %d", socket_msg.messages_case());
    }

    return false;
}

bool Steam_NetworkingSockets::RunCallbacks(pFrameResult_t res)
{
    return res->done;
}
