/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_networkingutils.h"
#include "steam_client.h"

Steam_NetworkingUtils::Steam_NetworkingUtils():
    _relay_initialized(false),
    _initialized_time(std::chrono::steady_clock::now()),
    _pfnDebug(nullptr)
{
}

Steam_NetworkingUtils::~Steam_NetworkingUtils()
{
    emu_deinit();
}

void Steam_NetworkingUtils::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _cb_manager = cb_manager;
    _network = network;
}

void Steam_NetworkingUtils::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _network.reset();
    _cb_manager.reset();
}

//
// Efficient message sending
//

/// Allocate and initialize a message object.  Usually the reason
/// you call this is to pass it to ISteamNetworkingSockets::SendMessages.
/// The returned object will have all of the relevant fields cleared to zero.
///
/// Optionally you can also request that this system allocate space to
/// hold the payload itself.  If cbAllocateBuffer is nonzero, the system
/// will allocate memory to hold a payload of at least cbAllocateBuffer bytes.
/// m_pData will point to the allocated buffer, m_cbSize will be set to the
/// size, and m_pfnFreeData will be set to the proper function to free up
/// the buffer.
///
/// If cbAllocateBuffer=0, then no buffer is allocated.  m_pData will be NULL,
/// m_cbSize will be zero, and m_pfnFreeData will be NULL.  You will need to
/// set each of these.
///
/// You can use SteamNetworkingMessage_t::Release to free up the message
/// bookkeeping object and any associated buffer.  See
/// ISteamNetworkingSockets::SendMessages for details on reference
/// counting and ownership.
SteamNetworkingMessage_t* Steam_NetworkingUtils::AllocateMessage(int cbAllocateBuffer)
{
    SteamNetworkingMessage_t* msg = new SteamNetworkingMessage_t;
    if (cbAllocateBuffer != 0)
    {
        msg->m_cbSize = cbAllocateBuffer;
        msg->m_pData = malloc(cbAllocateBuffer * sizeof(uint8_t));

        msg->m_pfnFreeData = [](SteamNetworkingMessage_t* _this)
        {
            free(_this->m_pData);
            _this->m_pData = nullptr;
        };
    }
    else
    {
        msg->m_cbSize = 0;
        msg->m_pData = nullptr;
        msg->m_pfnFreeData = nullptr;
    }

    msg->m_pfnRelease = [](SteamNetworkingMessage_t* _this)
    {
        if (_this->m_pfnFreeData != nullptr)
            _this->m_pfnFreeData(_this);

        delete _this;
    };

    return msg;
}

/// Fetch current status of the relay network.
///
/// SteamRelayNetworkStatus_t is also a callback.  It will be triggered on
/// both the user and gameserver interfaces any time the status changes, or
/// ping measurement starts or stops.
///
/// SteamRelayNetworkStatus_t::m_eAvail is returned.  If you want
/// more details, you can pass a non-NULL value.
ESteamNetworkingAvailability Steam_NetworkingUtils::GetRelayNetworkStatus(SteamRelayNetworkStatus_t* pDetails)
{
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (pDetails != nullptr)
    {
        //TODO: check if this is how real steam returns it
        if (_relay_initialized)
        {
            pDetails->m_eAvail = k_ESteamNetworkingAvailability_Current;
            pDetails->m_bPingMeasurementInProgress = 0;
            pDetails->m_eAvailAnyRelay = k_ESteamNetworkingAvailability_Current;
            pDetails->m_eAvailNetworkConfig = k_ESteamNetworkingAvailability_Current;
            strcpy(pDetails->m_debugMsg, "OK");
        }
        else
        {
            memset(pDetails, 0, sizeof(*pDetails));
        }
    }

    return k_ESteamNetworkingAvailability_Current;
}

//
// "Ping location" functions
//
// We use the ping times to the valve relays deployed worldwide to
// generate a "marker" that describes the location of an Internet host.
// Given two such markers, we can estimate the network latency between
// two hosts, without sending any packets.  The estimate is based on the
// optimal route that is found through the Valve network.  If you are
// using the Valve network to carry the traffic, then this is precisely
// the ping you want.  If you are not, then the ping time will probably
// still be a reasonable estimate.
//
// This is extremely useful to select peers for matchmaking!
//
// The markers can also be converted to a string, so they can be transmitted.
// We have a separate library you can use on your backend to manipulate
// these objects.  (See steamdatagram_ticketgen.h)

/// Return location info for the current host.  Returns the approximate
/// age of the data, in seconds, or -1 if no data is available.
///
/// It takes a few seconds to initialize access to the relay network.  If
/// you call this very soon after calling InitializeRelayNetworkAccess,
/// the data may not be available yet.
///
/// This always return the most up-to-date information we have available
/// right now, even if we are in the middle of re-calculating ping times.
float Steam_NetworkingUtils::GetLocalPingLocation(SteamNetworkPingLocation_t& result)
{
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (_relay_initialized)
    {
        result.m_data[2] = 123;
        result.m_data[8] = 67;
        return 2.0;
    }

    return -1;
}

/// Estimate the round-trip latency between two arbitrary locations, in
/// milliseconds.  This is a conservative estimate, based on routing through
/// the relay network.  For most basic relayed connections, this ping time
/// will be pretty accurate, since it will be based on the route likely to
/// be actually used.
///
/// If a direct IP route is used (perhaps via NAT traversal), then the route
/// will be different, and the ping time might be better.  Or it might actually
/// be a bit worse!  Standard IP routing is frequently suboptimal!
///
/// But even in this case, the estimate obtained using this method is a
/// reasonable upper bound on the ping time.  (Also it has the advantage
/// of returning immediately and not sending any packets.)
///
/// In a few cases we might not able to estimate the route.  In this case
/// a negative value is returned.  k_nSteamNetworkingPing_Failed means
/// the reason was because of some networking difficulty.  (Failure to
/// ping, etc)  k_nSteamNetworkingPing_Unknown is returned if we cannot
/// currently answer the question for some other reason.
///
/// Do you need to be able to do this from a backend/matchmaking server?
/// You are looking for the "ticketgen" library.
int Steam_NetworkingUtils::EstimatePingTimeBetweenTwoLocations(const SteamNetworkPingLocation_t& location1, const SteamNetworkPingLocation_t& location2)
{
    return 10;
}

/// Same as EstimatePingTime, but assumes that one location is the local host.
/// This is a bit faster, especially if you need to calculate a bunch of
/// these in a loop to find the fastest one.
///
/// In rare cases this might return a slightly different estimate than combining
/// GetLocalPingLocation with EstimatePingTimeBetweenTwoLocations.  That's because
/// this function uses a slightly more complete set of information about what
/// route would be taken.
int Steam_NetworkingUtils::EstimatePingTimeFromLocalHost(const SteamNetworkPingLocation_t& remoteLocation)
{
    return 10;
}

/// Convert a ping location into a text format suitable for sending over the wire.
/// The format is a compact and human readable.  However, it is subject to change
/// so please do not parse it yourself.  Your buffer must be at least
/// k_cchMaxSteamNetworkingPingLocationString bytes.
void Steam_NetworkingUtils::ConvertPingLocationToString(const SteamNetworkPingLocation_t& location, char* pszBuf, int cchBufSize)
{
    if (pszBuf != nullptr)
    {
        strncpy(pszBuf, "fra=10+2", cchBufSize);
    }
}

/// Parse back SteamNetworkPingLocation_t string.  Returns false if we couldn't understand
/// the string.
bool Steam_NetworkingUtils::ParsePingLocationString(const char* pszString, SteamNetworkPingLocation_t& result)
{
    return true;
}

//
// Initialization / ping measurement status
//

/// Check if the ping data of sufficient recency is available, and if
/// it's too old, start refreshing it.
///
/// Please only call this function when you *really* do need to force an
/// immediate refresh of the data.  (For example, in response to a specific
/// user input to refresh this information.)  Don't call it "just in case",
/// before every connection, etc.  That will cause extra traffic to be sent
/// for no benefit. The library will automatically refresh the information
/// as needed.
///
/// Returns true if sufficiently recent data is already available.
///
/// Returns false if sufficiently recent data is not available.  In this
/// case, ping measurement is initiated, if it is not already active.
/// (You cannot restart a measurement already in progress.)
bool Steam_NetworkingUtils::CheckPingDataUpToDate(float flMaxAgeSeconds)
{
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _relay_initialized = true;
    return true;
}

/// Return true if we are taking ping measurements to update our ping
/// location or select optimal routing.  Ping measurement typically takes
/// a few seconds, perhaps up to 10 seconds.
bool Steam_NetworkingUtils::IsPingMeasurementInProgress()
{
    return false;
}

//
// List of Valve data centers, and ping times to them.  This might
// be useful to you if you are use our hosting, or just need to measure
// latency to a cloud data center where we are running relays.
//

/// Fetch ping time of best available relayed route from this host to
/// the specified data center.
int Steam_NetworkingUtils::GetPingToDataCenter(SteamNetworkingPOPID popID, SteamNetworkingPOPID* pViaRelayPoP)
{
    return 10;
}

/// Get *direct* ping time to the relays at the data center.
int Steam_NetworkingUtils::GetDirectPingToPOP(SteamNetworkingPOPID popID)
{
    return 0;
}

/// Get number of network points of presence in the config
int Steam_NetworkingUtils::GetPOPCount()
{
    return 0;
}

/// Get list of all POP IDs.  Returns the number of entries that were filled into
/// your list.
int Steam_NetworkingUtils::GetPOPList(SteamNetworkingPOPID* list, int nListSz)
{
    return 0;
}

//
// Misc
//

/// Fetch current timestamp.  This timer has the following properties:
///
/// - Monotonicity is guaranteed.
/// - The initial value will be at least 24*3600*30*1e6, i.e. about
///   30 days worth of microseconds.  In this way, the timestamp value of
///   0 will always be at least "30 days ago".  Also, negative numbers
///   will never be returned.
/// - Wraparound / overflow is not a practical concern.
///
/// If you are running under the debugger and stop the process, the clock
/// might not advance the full wall clock time that has elapsed between
/// calls.  If the process is not blocked from normal operation, the
/// timestamp values will track wall clock time, even if you don't call
/// the function frequently.
///
/// The value is only meaningful for this run of the process.  Don't compare
/// it to values obtained on another computer, or other runs of the same process.
SteamNetworkingMicroseconds Steam_NetworkingUtils::GetLocalTimestamp()
{
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);
    return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - _initialized_time).count() + (SteamNetworkingMicroseconds)24 * 3600 * 30 * 1e6;
}

/// Set a function to receive network-related information that is useful for debugging.
/// This can be very useful during development, but it can also be useful for troubleshooting
/// problems with tech savvy end users.  If you have a console or other log that customers
/// can examine, these log messages can often be helpful to troubleshoot network issues.
/// (Especially any warning/error messages.)
///
/// The detail level indicates what message to invoke your callback on.  Lower numeric
/// value means more important, and the value you pass is the lowest priority (highest
/// numeric value) you wish to receive callbacks for.
///
/// Except when debugging, you should only use k_ESteamNetworkingSocketsDebugOutputType_Msg
/// or k_ESteamNetworkingSocketsDebugOutputType_Warning.  For best performance, do NOT
/// request a high detail level and then filter out messages in your callback.  Instead,
/// call function function to adjust the desired level of detail.
///
/// IMPORTANT: This may be called from a service thread, while we own a mutex, etc.
/// Your output function must be threadsafe and fast!  Do not make any other
/// Steamworks calls from within the handler.
void Steam_NetworkingUtils::SetDebugOutputFunction(ESteamNetworkingSocketsDebugOutputType eDetailLevel, FSteamNetworkingSocketsDebugOutput pfnFunc)
{
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (eDetailLevel != k_ESteamNetworkingSocketsDebugOutputType_None)
    {
        _pfnDebug = pfnFunc;
    }
}

/// Set a configuration value.
/// - eValue: which value is being set
/// - eScope: Onto what type of object are you applying the setting?
/// - scopeArg: Which object you want to change?  (Ignored for global scope).  E.g. connection handle, listen socket handle, interface pointer, etc.
/// - eDataType: What type of data is in the buffer at pValue?  This must match the type of the variable exactly!
/// - pArg: Value to set it to.  You can pass NULL to remove a non-global sett at this scope,
///   causing the value for that object to use global defaults.  Or at global scope, passing NULL
///   will reset any custom value and restore it to the system default.
///   NOTE: When setting callback functions, do not pass the function pointer directly.
///   Your argument should be a pointer to a function pointer.
bool Steam_NetworkingUtils::SetConfigValue(ESteamNetworkingConfigValue eValue, ESteamNetworkingConfigScope eScopeType, intptr_t scopeObj,
    ESteamNetworkingConfigDataType eDataType, const void* pArg)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

/// Get a configuration value.
/// - eValue: which value to fetch
/// - eScopeType: query setting on what type of object
/// - eScopeArg: the object to query the setting for
/// - pOutDataType: If non-NULL, the data type of the value is returned.
/// - pResult: Where to put the result.  Pass NULL to query the required buffer size.  (k_ESteamNetworkingGetConfigValue_BufferTooSmall will be returned.)
/// - cbResult: IN: the size of your buffer.  OUT: the number of bytes filled in or required.
ESteamNetworkingGetConfigValueResult Steam_NetworkingUtils::GetConfigValue(ESteamNetworkingConfigValue eValue, ESteamNetworkingConfigScope eScopeType, intptr_t scopeObj,
    ESteamNetworkingConfigDataType* pOutDataType, void* pResult, size_t* cbResult)
{
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return ESteamNetworkingGetConfigValueResult::k_ESteamNetworkingGetConfigValue_BadValue;
}

/// Returns info about a configuration value.  Returns false if the value does not exist.
/// pOutNextValue can be used to iterate through all of the known configuration values.
/// (Use GetFirstConfigValue() to begin the iteration, will be k_ESteamNetworkingConfig_Invalid on the last value)
/// Any of the output parameters can be NULL if you do not need that information.
bool Steam_NetworkingUtils::GetConfigValueInfo(ESteamNetworkingConfigValue eValue, const char** pOutName, ESteamNetworkingConfigDataType* pOutDataType, ESteamNetworkingConfigScope* pOutScope, ESteamNetworkingConfigValue* pOutNextValue)
{
    return false;
}

/// Return the lowest numbered configuration value available in the current environment.
ESteamNetworkingConfigValue Steam_NetworkingUtils::GetFirstConfigValue()
{
    return ESteamNetworkingConfigValue::k_ESteamNetworkingConfig_Invalid;
}

// String conversions.  You'll usually access these using the respective
// inline methods.
void Steam_NetworkingUtils::SteamNetworkingIPAddr_ToString(const SteamNetworkingIPAddr& addr, char* buf, size_t cbBuf, bool bWithPort)
{
    if (buf == nullptr || cbBuf == 0)
        return;

    std::string str_addr;
    if (addr.IsIPv4())
    {
        PortableAPI::ipv4_addr addr4;
        addr4.set_ip(addr.GetIPv4());
        addr4.set_port(addr.m_port);
        str_addr = std::move(addr4.to_string(bWithPort));
    }
    else
    {
        PortableAPI::ipv6_addr addr6;
        in6_addr ipv6_addr;
        memcpy(ipv6_addr.s6_addr, addr.m_ipv6, sizeof(ipv6_addr.s6_addr));

        addr6.set_addr(ipv6_addr);
        addr6.set_port(addr.m_port);
        str_addr = std::move(addr6.to_string(bWithPort));
    }

    cbBuf = std::min<size_t>(cbBuf, str_addr.length() + 1);
    strncpy(buf, str_addr.c_str(), cbBuf);
    buf[cbBuf - 1] = '\0';
}

bool Steam_NetworkingUtils::SteamNetworkingIPAddr_ParseString(SteamNetworkingIPAddr* pAddr, const char* pszStr)
{
    bool valid = false;

    if (pAddr == nullptr || pszStr == nullptr)
        return valid;

    PortableAPI::ipv4_addr addr4;
    if (addr4.from_string(pszStr))
    {
        valid = true;
        pAddr->SetIPv4(addr4.get_ip(), addr4.get_port());
    }
    else
    {
        PortableAPI::ipv6_addr addr6;
        if (addr6.from_string(pszStr))
        {
            valid = true;
            pAddr->SetIPv6(addr6.get_addr().s6_addr, addr6.get_port());
        }
    }

    if (!valid)
    {
        pAddr->Clear();
    }

    return valid;
}

void Steam_NetworkingUtils::SteamNetworkingIdentity_ToString(const SteamNetworkingIdentity& identity, char* buf, size_t cbBuf)
{
    if (buf == nullptr)
        return;

    std::string str;
    str.reserve(SteamNetworkingIdentity::k_cchMaxString);
    switch (identity.m_eType)
    {
        case k_ESteamNetworkingIdentityType_SteamID:
        {
            str = "steamid:";
            str += std::move(std::to_string(identity.GetSteamID64()));
        }
        break;

        case k_ESteamNetworkingIdentityType_IPAddress:
        {
            str = "ip:";
            char buff[SteamNetworkingIPAddr::k_cchMaxString];
            auto& addr = *identity.GetIPAddr();
            SteamNetworkingIPAddr_ToString(addr, buff, sizeof(buff), true);
            str += buff;
        }
        break;

        case k_ESteamNetworkingIdentityType_GenericBytes:
        {
            int generic_len;
            const uint8* pBuf = identity.GetGenericBytes(generic_len);

            str = "gen:";
            str.resize(utils::static_strlen("gen:") + (generic_len * 2));

            char* pDest = &str[utils::static_strlen("gen:")];
            while(generic_len--)
            {
                // I don't care for the last char, I've reserved the max string size
                snprintf(pDest, 3, "%02x", *pBuf); 
                ++pBuf;
                pDest += 2;
            }
        }
        break;

        case k_ESteamNetworkingIdentityType_GenericString:
        {
            str = "str:";
            str += identity.GetGenericString();
        }
        break;

        case k_ESteamNetworkingIdentityType_UnknownType:
        {
            str = identity.m_szUnknownRawString;
        }
        break;
    }
    cbBuf = std::min<size_t>(cbBuf, str.length() + 1);
    strncpy(buf, str.c_str(), cbBuf);
    buf[cbBuf - 1] = '\0';

}

bool Steam_NetworkingUtils::SteamNetworkingIdentity_ParseString(SteamNetworkingIdentity* pIdentity, const char* pszStr)
{
    bool valid = false;
    if (pIdentity == nullptr)
    {
        return valid;
    }

    if (pszStr != nullptr)
    {
        const char* end = strchr(pszStr, ':');
        if (end != nullptr)
        {
            ++end;
            switchstr(std::string(pszStr, end))
            {
                casestr("gen:") :
                {
                    size_t length = strlen(end);
                    if (!(length % 2) && length <= (sizeof(pIdentity->m_genericBytes) * 2))
                    {// Must be even
                        valid = true;
                        length /= 2;
                        pIdentity->m_eType = k_ESteamNetworkingIdentityType_GenericBytes;
                        pIdentity->m_cbSize = length;
                        uint8* pBytes = pIdentity->m_genericBytes;

                        char hex[3] = { 0,0,0 };
                        while (length)
                        {
                            hex[0] = end[0];
                            hex[1] = end[1];
                            // Steam doesn't check if wasn't a hex char
                            *pBytes = strtol(hex, nullptr, 16);

                            ++pBytes;
                            end += 2;
                            --length;
                        }
                    }
                }
                break;

                casestr("steamid:") :
                {
                    CSteamID steam_id(uint64(strtoull(end, nullptr, 10)));
                    if (steam_id.IsValid())
                    {
                        valid = true;
                        pIdentity->SetSteamID(steam_id);
                    }
                }
                break;

                casestr("str:") :
                {
                    valid = pIdentity->SetGenericString(end);
                }
                break;

                casestr("ip:") :
                {
                    SteamNetworkingIPAddr steam_addr;
                    if (SteamNetworkingIPAddr_ParseString(&steam_addr, end))
                    {
                        valid = true;
                        pIdentity->SetIPAddr(steam_addr);
                    }
                }
                break;
            }
        }
    }

    return valid;
}