/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_parentalsettings.h"
#include "steam_client.h"

Steam_ParentalSettings::Steam_ParentalSettings()
{
}

Steam_ParentalSettings::~Steam_ParentalSettings()
{
    emu_deinit();
}

void Steam_ParentalSettings::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _cb_manager = cb_manager;
    _network = network;
}

void Steam_ParentalSettings::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _network.reset();
    _cb_manager.reset();
}

bool Steam_ParentalSettings::BIsParentalLockEnabled()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_ParentalSettings::BIsParentalLockLocked()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_ParentalSettings::BIsAppBlocked(AppId_t nAppID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_ParentalSettings::BIsAppInBlockList(AppId_t nAppID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_ParentalSettings::BIsFeatureBlocked(EParentalFeature eFeature)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_ParentalSettings::BIsFeatureInBlockList(EParentalFeature eFeature)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}