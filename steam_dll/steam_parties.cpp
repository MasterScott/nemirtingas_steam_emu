/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_parties.h"
#include "steam_client.h"

Steam_Parties::Steam_Parties()
{
}

Steam_Parties::~Steam_Parties()
{
    emu_deinit();
}

void Steam_Parties::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _cb_manager = cb_manager;
    _network = network;
}

void Steam_Parties::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _network.reset();
    _cb_manager.reset();
}

// =============================================================================================
    // Party Client APIs

    // Enumerate any active beacons for parties you may wish to join
uint32 Steam_Parties::GetNumActiveBeacons()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

PartyBeaconID_t Steam_Parties::GetBeaconByIndex(uint32 unIndex)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

bool Steam_Parties::GetBeaconDetails(PartyBeaconID_t ulBeaconID, CSteamID* pSteamIDBeaconOwner, STEAM_OUT_STRUCT() SteamPartyBeaconLocation_t* pLocation, STEAM_OUT_STRING_COUNT(cchMetadata) char* pchMetadata, int cchMetadata)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Join an open party. Steam will reserve one beacon slot for your SteamID,
// and return the necessary JoinGame string for you to use to connect
STEAM_CALL_RESULT(JoinPartyCallback_t)
SteamAPICall_t Steam_Parties::JoinParty(PartyBeaconID_t ulBeaconID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

// =============================================================================================
// Party Host APIs

// Get a list of possible beacon locations
bool Steam_Parties::GetNumAvailableBeaconLocations(uint32* puNumLocations)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}
bool Steam_Parties::GetAvailableBeaconLocations(SteamPartyBeaconLocation_t* pLocationList, uint32 uMaxNumLocations)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Create a new party beacon and activate it in the selected location.
// unOpenSlots is the maximum number of users that Steam will send to you.
// When people begin responding to your beacon, Steam will send you
// PartyReservationCallback_t callbacks to let you know who is on the way.
STEAM_CALL_RESULT(CreateBeaconCallback_t)
SteamAPICall_t Steam_Parties::CreateBeacon(uint32 unOpenSlots, SteamPartyBeaconLocation_t* pBeaconLocation, const char* pchConnectString, const char* pchMetadata)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

// Call this function when a user that had a reservation (see callback below) 
// has successfully joined your party.
// Steam will manage the remaining open slots automatically.
void Steam_Parties::OnReservationCompleted(PartyBeaconID_t ulBeacon, CSteamID steamIDUser)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// To cancel a reservation (due to timeout or user input), call this.
// Steam will open a new reservation slot.
// Note: The user may already be in-flight to your game, so it's possible they will still connect and try to join your party.
void Steam_Parties::CancelReservation(PartyBeaconID_t ulBeacon, CSteamID steamIDUser)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// Change the number of open beacon reservation slots.
// Call this if, for example, someone without a reservation joins your party (eg a friend, or via your own matchmaking system).
STEAM_CALL_RESULT(ChangeNumOpenSlotsCallback_t)
SteamAPICall_t Steam_Parties::ChangeNumOpenSlots(PartyBeaconID_t ulBeacon, uint32 unOpenSlots)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

// Turn off the beacon. 
bool Steam_Parties::DestroyBeacon(PartyBeaconID_t ulBeacon)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Utils
bool Steam_Parties::GetBeaconLocationData(SteamPartyBeaconLocation_t BeaconLocation, ESteamPartyBeaconLocationData eData, STEAM_OUT_STRING_COUNT(cchDataStringOut) char* pchDataStringOut, int cchDataStringOut)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}