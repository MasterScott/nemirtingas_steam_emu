/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"

class LOCAL_API LOCAL_API Steam_Parties :
    public ISteamParties
{
    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;

public:
    std::recursive_mutex _local_mutex;

    Steam_Parties();
    virtual ~Steam_Parties();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    // =============================================================================================
    // Party Client APIs

    // Enumerate any active beacons for parties you may wish to join
    virtual uint32 GetNumActiveBeacons();
    virtual PartyBeaconID_t GetBeaconByIndex(uint32 unIndex);
    virtual bool GetBeaconDetails(PartyBeaconID_t ulBeaconID, CSteamID* pSteamIDBeaconOwner, STEAM_OUT_STRUCT() SteamPartyBeaconLocation_t* pLocation, STEAM_OUT_STRING_COUNT(cchMetadata) char* pchMetadata, int cchMetadata);

    // Join an open party. Steam will reserve one beacon slot for your SteamID,
    // and return the necessary JoinGame string for you to use to connect
    STEAM_CALL_RESULT(JoinPartyCallback_t)
    virtual SteamAPICall_t JoinParty(PartyBeaconID_t ulBeaconID);

    // =============================================================================================
    // Party Host APIs

    // Get a list of possible beacon locations
    virtual bool GetNumAvailableBeaconLocations(uint32* puNumLocations);
    virtual bool GetAvailableBeaconLocations(SteamPartyBeaconLocation_t* pLocationList, uint32 uMaxNumLocations);

    // Create a new party beacon and activate it in the selected location.
    // unOpenSlots is the maximum number of users that Steam will send to you.
    // When people begin responding to your beacon, Steam will send you
    // PartyReservationCallback_t callbacks to let you know who is on the way.
    STEAM_CALL_RESULT(CreateBeaconCallback_t)
    virtual SteamAPICall_t CreateBeacon(uint32 unOpenSlots, SteamPartyBeaconLocation_t* pBeaconLocation, const char* pchConnectString, const char* pchMetadata);

    // Call this function when a user that had a reservation (see callback below) 
    // has successfully joined your party.
    // Steam will manage the remaining open slots automatically.
    virtual void OnReservationCompleted(PartyBeaconID_t ulBeacon, CSteamID steamIDUser);

    // To cancel a reservation (due to timeout or user input), call this.
    // Steam will open a new reservation slot.
    // Note: The user may already be in-flight to your game, so it's possible they will still connect and try to join your party.
    virtual void CancelReservation(PartyBeaconID_t ulBeacon, CSteamID steamIDUser);

    // Change the number of open beacon reservation slots.
    // Call this if, for example, someone without a reservation joins your party (eg a friend, or via your own matchmaking system).
    STEAM_CALL_RESULT(ChangeNumOpenSlotsCallback_t)
        virtual SteamAPICall_t ChangeNumOpenSlots(PartyBeaconID_t ulBeacon, uint32 unOpenSlots);

    // Turn off the beacon. 
    virtual bool DestroyBeacon(PartyBeaconID_t ulBeacon);

    // Utils
    virtual bool GetBeaconLocationData(SteamPartyBeaconLocation_t BeaconLocation, ESteamPartyBeaconLocationData eData, STEAM_OUT_STRING_COUNT(cchDataStringOut) char* pchDataStringOut, int cchDataStringOut);
};