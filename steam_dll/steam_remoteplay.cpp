/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_remoteplay.h"
#include "steam_client.h"

Steam_RemotePlay::Steam_RemotePlay()
{
}

Steam_RemotePlay::~Steam_RemotePlay()
{
    emu_deinit();
}

void Steam_RemotePlay::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _cb_manager = cb_manager;
    _network = network;
}

void Steam_RemotePlay::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _network.reset();
    _cb_manager.reset();
}

// Get the number of currently connected Steam Remote Play sessions
uint32 Steam_RemotePlay::GetSessionCount()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// Get the currently connected Steam Remote Play session ID at the specified index. Returns zero if index is out of bounds.
uint32 Steam_RemotePlay::GetSessionID(int iSessionIndex)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// Get the SteamID of the connected user
CSteamID Steam_RemotePlay::GetSessionSteamID(uint32 unSessionID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_steamIDNil;
}

// Get the name of the session client device
// This returns NULL if the sessionID is not valid
const char* Steam_RemotePlay::GetSessionClientName(uint32 unSessionID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return "";
}

// Get the form factor of the session client device
ESteamDeviceFormFactor Steam_RemotePlay::GetSessionClientFormFactor(uint32 unSessionID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_ESteamDeviceFormFactorUnknown;
}

// Get the resolution, in pixels, of the session client device
// This is set to 0x0 if the resolution is not available
bool Steam_RemotePlay::BGetSessionClientResolution(uint32 unSessionID, int* pnResolutionX, int* pnResolutionY)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_RemotePlay::BSendRemotePlayTogetherInvite(CSteamID steamIDFriend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}