/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"

class LOCAL_API Steam_RemotePlay:
    public ISteamRemotePlay001
{
    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;

public:
    std::recursive_mutex _local_mutex;

    Steam_RemotePlay();
    virtual ~Steam_RemotePlay();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    // Get the number of currently connected Steam Remote Play sessions
    virtual uint32 GetSessionCount();

    // Get the currently connected Steam Remote Play session ID at the specified index. Returns zero if index is out of bounds.
    virtual uint32 GetSessionID(int iSessionIndex);

    // Get the SteamID of the connected user
    virtual CSteamID GetSessionSteamID(uint32 unSessionID);

    // Get the name of the session client device
    // This returns NULL if the sessionID is not valid
    virtual const char* GetSessionClientName(uint32 unSessionID);

    // Get the form factor of the session client device
    virtual ESteamDeviceFormFactor GetSessionClientFormFactor(uint32 unSessionID);

    // Get the resolution, in pixels, of the session client device
    // This is set to 0x0 if the resolution is not available
    virtual bool BGetSessionClientResolution(uint32 unSessionID, int* pnResolutionX, int* pnResolutionY);

    // Invite a friend to Remote Play Together
    // This returns false if the invite can't be sent
    virtual bool BSendRemotePlayTogetherInvite(CSteamID steamIDFriend);
};