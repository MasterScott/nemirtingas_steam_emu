/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_remotestorage.h"
#include "steam_client.h"
#include "settings.h"

decltype(Steam_RemoteStorage::local_directory)  Steam_RemoteStorage::local_directory("local");
decltype(Steam_RemoteStorage::remote_directory) Steam_RemoteStorage::remote_directory("remote");

Steam_RemoteStorage::Steam_RemoteStorage() :
    _cloud(Settings::Inst().cloud_save),
    _write_handle(1)
{
}

Steam_RemoteStorage::~Steam_RemoteStorage()
{
    emu_deinit();
}

void Steam_RemoteStorage::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _cb_manager = cb_manager;
    _network = network;
}

void Steam_RemoteStorage::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _network.reset();
    _cb_manager.reset();
}

UGCFileWriteStreamHandle_t Steam_RemoteStorage::generate_write_handle()
{
    return _write_handle++;
}

bool  Steam_RemoteStorage::file_write(std::string const& base_folder, std::string const& file, const void* pvData, int32 len)
{
    std::string save_path = FileManager::join(base_folder, FileManager::clean_path(file));
    std::ofstream out_file(FileManager::open_write(save_path, std::ios::trunc | std::ios::binary));

    if (out_file)
    {
        out_file.write(reinterpret_cast<const char*>(pvData), len);
        return true;
    }

    return false;
}

int32 Steam_RemoteStorage::file_read(std::string const& base_folder, std::string const& file, void* pvData, int32 len, int32 offset)
{
    std::string save_path = FileManager::join(base_folder, FileManager::clean_path(file));
    std::ifstream in_file(FileManager::open_read(save_path, std::ios::in | std::ios::binary));

    if (in_file)
    {
        in_file.seekg(offset, std::ios::beg);
        int32 read_len = in_file.tellg();

        in_file.read(reinterpret_cast<char*>(pvData), len);

        read_len = int32(in_file.tellg()) - read_len;

        return read_len;
    }

    return 0;
}

// NOTE
//
// Filenames are case-insensitive, and will be converted to lowercase automatically.
// So "foo.bar" and "Foo.bar" are the same file, and if you write "Foo.bar" then
// iterate the files, the filename returned will be "foo.bar".
//

// file operations
bool	Steam_RemoteStorage::FileWrite(const char* pchFile, const void* pvData, int32 cubData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return file_write(remote_directory, pchFile, pvData, cubData);
}

int32	Steam_RemoteStorage::FileRead(const char* pchFile, void* pvData, int32 cubDataToRead)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return file_read(remote_directory, pchFile, pvData, cubDataToRead);
}

STEAM_CALL_RESULT(RemoteStorageFileWriteAsyncComplete_t)
SteamAPICall_t Steam_RemoteStorage::FileWriteAsync(const char* pchFile, const void* pvData, uint32 cubData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    FileWrite(pchFile, pvData, cubData);

    pFrameResult_t result(new FrameResult);
    RemoteStorageFileWriteAsyncComplete_t& rsfwac = result->CreateCallback<RemoteStorageFileWriteAsyncComplete_t>();
    rsfwac.m_eResult = k_EResultOK;

    result->done = true;
    _cb_manager->add_apicall(this, result);
    return result->GetAPICall();
}

STEAM_CALL_RESULT(RemoteStorageFileReadAsyncComplete_t)
SteamAPICall_t Steam_RemoteStorage::FileReadAsync(const char* pchFile, uint32 nOffset, uint32 cubToRead)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    pFrameResult_t result(new FrameResult);
    RemoteStorageFileReadAsyncComplete_t& rsfwac = result->CreateCallback<RemoteStorageFileReadAsyncComplete_t>(std::chrono::milliseconds(0));
    rsfwac.m_eResult = k_EResultOK;
    rsfwac.m_nOffset = nOffset;
    rsfwac.m_cubRead = cubToRead;
    rsfwac.m_hFileReadAsync = result->GetAPICall();

    _async_file_name[result->GetAPICall()] = pchFile;

    _cb_manager->add_apicall(this, result);
    return result->GetAPICall();
}

bool	Steam_RemoteStorage::FileReadAsyncComplete(SteamAPICall_t hReadCall, void* pvBuffer, uint32 cubToRead)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    pFrameResult_t res = _cb_manager->GetAPICall(hReadCall);

    auto it = _async_file_name.find(hReadCall);

    if (res == nullptr)
    {
        if (it != _async_file_name.end())
            _async_file_name.erase(it);

        return false;
    }

    if (it == _async_file_name.end())
        return false;

    RemoteStorageFileReadAsyncComplete_t& rsfwac = res->GetCallback<RemoteStorageFileReadAsyncComplete_t>();
    rsfwac.m_nOffset += file_read(remote_directory, it->second.c_str(), pvBuffer, cubToRead, rsfwac.m_nOffset);
    res->done = true;

    return true;
}

bool	Steam_RemoteStorage::FileForget(const char* pchFile)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    if (pchFile == nullptr)
        return false;

    return FileManager::delete_file(FileManager::join(remote_directory, FileManager::clean_path(pchFile)));
}

bool Steam_RemoteStorage::FileDelete(const char* pchFile)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    // TODO: Manager local & remote storage
    return FileForget(pchFile);
}

STEAM_CALL_RESULT(RemoteStorageFileShareResult_t)
SteamAPICall_t Steam_RemoteStorage::FileShare(const char* pchFile)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    pFrameResult_t res(new FrameResult);

    RemoteStorageFileShareResult_t& rsfsr = res->CreateCallback<RemoteStorageFileShareResult_t>();

    rsfsr.m_hFile = k_UGCHandleInvalid;
    strncpy(rsfsr.m_rgchFilename, pchFile, sizeof(rsfsr.m_rgchFilename));
    rsfsr.m_rgchFilename[sizeof(rsfsr.m_rgchFilename) - 1] = '\0';
    rsfsr.m_eResult = EResult::k_EResultFail;

    _cb_manager->add_apicall(this, res);
    res->done = true;
    return res->GetAPICall();
}

bool	Steam_RemoteStorage::SetSyncPlatforms(const char* pchFile, ERemoteStoragePlatform eRemoteStoragePlatform)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return true;
}

// file operations that cause network IO
UGCFileWriteStreamHandle_t Steam_RemoteStorage::FileWriteStreamOpen(const char* pchFile)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    if (pchFile == nullptr)
        return k_UGCFileStreamHandleInvalid;

    UGCFileWriteStreamHandle_t handle = generate_write_handle();
    _file_streams[handle].file = pchFile;

    return handle;
}

bool Steam_RemoteStorage::FileWriteStreamWriteChunk(UGCFileWriteStreamHandle_t writeHandle, const void* pvData, int32 cubData)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    if (pvData == nullptr)
        return false;

    auto it = _file_streams.find(writeHandle);
    if (it == _file_streams.end())
        return false;

    const char* pBuff = reinterpret_cast<const char*>(pvData);

    it->second.data.reserve(it->second.data.length() + cubData);
    std::copy(pBuff, pBuff + cubData, std::back_inserter(it->second.data));

    return true;
}

bool Steam_RemoteStorage::FileWriteStreamClose(UGCFileWriteStreamHandle_t writeHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    auto it = _file_streams.find(writeHandle);
    if (it == _file_streams.end())
        return false;

    auto res = file_write(remote_directory, it->second.file, it->second.data.data(), it->second.data.length());
    _file_streams.erase(it);
    return res;
}

bool Steam_RemoteStorage::FileWriteStreamCancel(UGCFileWriteStreamHandle_t writeHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    auto it = _file_streams.find(writeHandle);
    if (it == _file_streams.end())
        return false;

    _file_streams.erase(it);

    return true;
}

// file information
bool	Steam_RemoteStorage::FileExists(const char* pchFile)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%s", pchFile);

    return FileManager::is_file(FileManager::join(remote_directory, FileManager::clean_path(pchFile)));
}

bool	Steam_RemoteStorage::FilePersisted(const char* pchFile)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

int32	Steam_RemoteStorage::GetFileSize(const char* pchFile)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%s", pchFile);

    return FileManager::file_size(FileManager::join(remote_directory, FileManager::clean_path(pchFile)));
}

int64	Steam_RemoteStorage::GetFileTimestamp(const char* pchFile)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "%s", pchFile);

    if (pchFile == nullptr)
        return 0;

    return FileManager::file_mtime(FileManager::join(remote_directory, FileManager::clean_path(pchFile)));
}

ERemoteStoragePlatform Steam_RemoteStorage::GetSyncPlatforms(const char* pchFile)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_ERemoteStoragePlatformAll;
}

// iteration
int32 Steam_RemoteStorage::GetFileCount()
{
    APP_LOG(Log::LogLevel::TRACE, "");

    _files_cache.clear();
    _files_cache = std::move(FileManager::list_files(remote_directory, true));
    
    for (auto& file_name : _files_cache)
        std::replace(file_name.begin(), file_name.end(), '\\', '/');

    return _files_cache.size();
}

const char* Steam_RemoteStorage::GetFileNameAndSize(int iFile, int32* pnFileSizeInBytes)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    if (iFile >= _files_cache.size())
    {
        if (pnFileSizeInBytes != nullptr)
            *pnFileSizeInBytes = 0;

        return "";
    }
    if (pnFileSizeInBytes != nullptr)
        *pnFileSizeInBytes = FileManager::file_size(FileManager::join(remote_directory, _files_cache[iFile]));

    APP_LOG(Log::LogLevel::DEBUG, "%s", _files_cache[iFile].c_str());
    return _files_cache[iFile].c_str();
}

// configuration management
bool Steam_RemoteStorage::GetQuota(int32* pnTotalBytes, int32* puAvailableBytes)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    if (pnTotalBytes != nullptr)
        *pnTotalBytes = 2 << 26;
    if (puAvailableBytes != nullptr)
        *puAvailableBytes = 2 << 26;

    return true;
}

bool Steam_RemoteStorage::GetQuota(uint64* pnTotalBytes, uint64* puAvailableBytes)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    if(pnTotalBytes != nullptr)
        *pnTotalBytes = 2 << 26;
    if(puAvailableBytes != nullptr)
        *puAvailableBytes = 2 << 26;

    return true;
}

bool Steam_RemoteStorage::IsCloudEnabledForAccount()
{
    APP_LOG(Log::LogLevel::TRACE, "");

    return _cloud;
}

bool Steam_RemoteStorage::IsCloudEnabledForApp()
{
    APP_LOG(Log::LogLevel::TRACE, "");

    return _cloud;
}

void Steam_RemoteStorage::SetCloudEnabledForApp(bool bEnabled)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    _cloud = bEnabled;
}

// user generated content

// Downloads a UGC file.  A priority value of 0 will download the file immediately,
// otherwise it will wait to download the file until all downloads with a lower priority
// value are completed.  Downloads with equal priority will occur simultaneously.
SteamAPICall_t Steam_RemoteStorage::UGCDownload(UGCHandle_t hContent)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    return UGCDownload(hContent, 0);
}

STEAM_CALL_RESULT(RemoteStorageDownloadUGCResult_t)
SteamAPICall_t Steam_RemoteStorage::UGCDownload(UGCHandle_t hContent, uint32 unPriority)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

// Gets the amount of data downloaded so far for a piece of content. pnBytesExpected can be 0 if function returns false
// or if the transfer hasn't started yet, so be careful to check for that before dividing to get a percentage
bool    Steam_RemoteStorage::GetUGCDownloadProgress(UGCHandle_t hContent, uint32* puDownloadedBytes, uint32* puTotalBytes)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return GetUGCDownloadProgress(hContent, reinterpret_cast<int32*>(puDownloadedBytes), reinterpret_cast<int32*>(puTotalBytes));
}

bool	Steam_RemoteStorage::GetUGCDownloadProgress(UGCHandle_t hContent, int32* pnBytesDownloaded, int32* pnBytesExpected)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Gets metadata for a file after it has been downloaded. This is the same metadata given in the RemoteStorageDownloadUGCResult_t call result
bool	Steam_RemoteStorage::GetUGCDetails(UGCHandle_t hContent, AppId_t* pnAppID, STEAM_OUT_STRING() char** ppchName, int32* pnFileSizeInBytes, STEAM_OUT_STRUCT() CSteamID* pSteamIDOwner)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// After download, gets the content of the file.  
// Small files can be read all at once by calling this function with an offset of 0 and cubDataToRead equal to the size of the file.
// Larger files can be read in chunks to reduce memory usage (since both sides of the IPC client and the game itself must allocate
// enough memory for each chunk).  Once the last byte is read, the file is implicitly closed and further calls to UGCRead will fail
// unless UGCDownload is called again.
// For especially large files (anything over 100MB) it is a requirement that the file is read in chunks.
int32	Steam_RemoteStorage::UGCRead(UGCHandle_t hContent, void* pvData, int32 cubDataToRead)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return UGCRead(hContent, pvData, cubDataToRead, 0);
}

int32	Steam_RemoteStorage::UGCRead(UGCHandle_t hContent, void* pvData, int32 cubDataToRead, uint32 cOffset)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return UGCRead(hContent, pvData, cubDataToRead, cOffset, k_EUGCRead_ContinueReadingUntilFinished);
}

int32	Steam_RemoteStorage::UGCRead(UGCHandle_t hContent, void* pvData, int32 cubDataToRead, uint32 cOffset, EUGCReadAction eAction)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// Functions to iterate through UGC that has finished downloading but has not yet been read via UGCRead()
int32	Steam_RemoteStorage::GetCachedUGCCount()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

UGCHandle_t Steam_RemoteStorage::GetCachedUGCHandle(int32 iCachedContent)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// The following functions are only necessary on the Playstation 3. On PC & Mac, the Steam client will handle these operations for you
// On Playstation 3, the game controls which files are stored in the cloud, via FilePersist, FileFetch, and FileForget.

#if defined(_PS3) || defined(_SERVER)
        // Connect to Steam and get a list of files in the Cloud - results in a RemoteStorageAppSyncStatusCheck_t callback
void Steam_RemoteStorage::GetFileListFromServer()
{

}
// Indicate this file should be downloaded in the next sync
bool Steam_RemoteStorage::FileFetch(const char* pchFile)
{
    return false;
}
// Indicate this file should be persisted in the next sync
bool Steam_RemoteStorage::FilePersist(const char* pchFile)
{
    return false;
}
// Pull any requested files down from the Cloud - results in a RemoteStorageAppSyncedClient_t callback
bool Steam_RemoteStorage::SynchronizeToClient()
{
    return false;
}
// Upload any requested files to the Cloud - results in a RemoteStorageAppSyncedServer_t callback
bool Steam_RemoteStorage::SynchronizeToServer()
{
    return false;
}
// Reset any fetch/persist/etc requests
bool Steam_RemoteStorage::ResetFileRequestState()
{
    return false;
}
#endif

// publishing UGC
SteamAPICall_t	Steam_RemoteStorage::PublishFile(const char* pchFile, const char* pchPreviewFile, AppId_t nConsumerAppId, const char* pchTitle, const char* pchDescription, ERemoteStoragePublishedFileVisibility eVisibility, SteamParamStringArray_t* pTags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return PublishWorkshopFile(pchFile, pchPreviewFile, nConsumerAppId, pchTitle, pchDescription, eVisibility, pTags, k_EWorkshopFileTypeCommunity);
}

SteamAPICall_t	Steam_RemoteStorage::PublishWorkshopFile(const char* pchFile, const char* pchPreviewFile, AppId_t nConsumerAppId, const char* pchTitle, const char* pchDescription, SteamParamStringArray_t* pTags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return PublishWorkshopFile(pchFile, pchPreviewFile, nConsumerAppId, pchTitle, pchDescription, k_ERemoteStoragePublishedFileVisibilityPublic, pTags, k_EWorkshopFileTypeCommunity);
}

STEAM_CALL_RESULT(RemoteStoragePublishFileProgress_t)
SteamAPICall_t	Steam_RemoteStorage::PublishWorkshopFile(const char* pchFile, const char* pchPreviewFile, AppId_t nConsumerAppId, const char* pchTitle, const char* pchDescription, ERemoteStoragePublishedFileVisibility eVisibility, SteamParamStringArray_t* pTags, EWorkshopFileType eWorkshopFileType)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

SteamAPICall_t	Steam_RemoteStorage::UpdatePublishedFile(RemoteStorageUpdatePublishedFileRequest_t updatePublishedFileRequest)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

PublishedFileUpdateHandle_t Steam_RemoteStorage::CreatePublishedFileUpdateRequest(PublishedFileId_t unPublishedFileId)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

bool Steam_RemoteStorage::UpdatePublishedFileFile(PublishedFileUpdateHandle_t updateHandle, const char* pchFile)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_RemoteStorage::UpdatePublishedFilePreviewFile(PublishedFileUpdateHandle_t updateHandle, const char* pchPreviewFile)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_RemoteStorage::UpdatePublishedFileTitle(PublishedFileUpdateHandle_t updateHandle, const char* pchTitle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_RemoteStorage::UpdatePublishedFileDescription(PublishedFileUpdateHandle_t updateHandle, const char* pchDescription)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_RemoteStorage::UpdatePublishedFileVisibility(PublishedFileUpdateHandle_t updateHandle, ERemoteStoragePublishedFileVisibility eVisibility)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_RemoteStorage::UpdatePublishedFileTags(PublishedFileUpdateHandle_t updateHandle, SteamParamStringArray_t* pTags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

STEAM_CALL_RESULT(RemoteStorageUpdatePublishedFileResult_t)
SteamAPICall_t	Steam_RemoteStorage::CommitPublishedFileUpdate(PublishedFileUpdateHandle_t updateHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

// Gets published file details for the given publishedfileid.  If unMaxSecondsOld is greater than 0,
// cached data may be returned, depending on how long ago it was cached.  A value of 0 will force a refresh.
// A value of k_WorkshopForceLoadPublishedFileDetailsFromCache will use cached data if it exists, no matter how old it is.
SteamAPICall_t	Steam_RemoteStorage::GetPublishedFileDetails(PublishedFileId_t unPublishedFileId)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return GetPublishedFileDetails(unPublishedFileId, 0);
}

STEAM_CALL_RESULT(RemoteStorageGetPublishedFileDetailsResult_t)
SteamAPICall_t	Steam_RemoteStorage::GetPublishedFileDetails(PublishedFileId_t unPublishedFileId, uint32 unMaxSecondsOld)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

STEAM_CALL_RESULT(RemoteStorageDeletePublishedFileResult_t)
SteamAPICall_t	Steam_RemoteStorage::DeletePublishedFile(PublishedFileId_t unPublishedFileId)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

// enumerate the files that the current user published with this app
STEAM_CALL_RESULT(RemoteStorageEnumerateUserPublishedFilesResult_t)
SteamAPICall_t	Steam_RemoteStorage::EnumerateUserPublishedFiles(uint32 unStartIndex)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    pFrameResult_t res(new FrameResult);
    RemoteStorageEnumerateUserPublishedFilesResult_t& rseupfr = res->CreateCallback<RemoteStorageEnumerateUserPublishedFilesResult_t>();
    rseupfr.m_nResultsReturned = 0;
    rseupfr.m_nTotalResultCount = 0;
    rseupfr.m_eResult = k_EResultOK;

    res->done = true;
    return res->GetAPICall();
}

STEAM_CALL_RESULT(RemoteStorageSubscribePublishedFileResult_t)
SteamAPICall_t	Steam_RemoteStorage::SubscribePublishedFile(PublishedFileId_t unPublishedFileId)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

STEAM_CALL_RESULT(RemoteStorageEnumerateUserSubscribedFilesResult_t)
SteamAPICall_t	Steam_RemoteStorage::EnumerateUserSubscribedFiles(uint32 unStartIndex)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    pFrameResult_t res(new FrameResult);
    RemoteStorageEnumerateUserSubscribedFilesResult_t& rseusfr = res->CreateCallback<RemoteStorageEnumerateUserSubscribedFilesResult_t>();
    rseusfr.m_nResultsReturned = 0;
    rseusfr.m_nTotalResultCount = 0;
    rseusfr.m_eResult = k_EResultOK;

    res->done = true;
    return res->GetAPICall();
}

STEAM_CALL_RESULT(RemoteStorageUnsubscribePublishedFileResult_t)
SteamAPICall_t	Steam_RemoteStorage::UnsubscribePublishedFile(PublishedFileId_t unPublishedFileId)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

bool Steam_RemoteStorage::UpdatePublishedFileSetChangeDescription(PublishedFileUpdateHandle_t updateHandle, const char* pchChangeDescription)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

STEAM_CALL_RESULT(RemoteStorageGetPublishedItemVoteDetailsResult_t)
SteamAPICall_t	Steam_RemoteStorage::GetPublishedItemVoteDetails(PublishedFileId_t unPublishedFileId)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

STEAM_CALL_RESULT(RemoteStorageUpdateUserPublishedItemVoteResult_t)
SteamAPICall_t	Steam_RemoteStorage::UpdateUserPublishedItemVote(PublishedFileId_t unPublishedFileId, bool bVoteUp)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

STEAM_CALL_RESULT(RemoteStorageGetPublishedItemVoteDetailsResult_t)
SteamAPICall_t	Steam_RemoteStorage::GetUserPublishedItemVoteDetails(PublishedFileId_t unPublishedFileId)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

STEAM_CALL_RESULT(RemoteStorageEnumerateUserPublishedFilesResult_t)
SteamAPICall_t	Steam_RemoteStorage::EnumerateUserSharedWorkshopFiles(CSteamID steamId, uint32 unStartIndex, SteamParamStringArray_t* pRequiredTags, SteamParamStringArray_t* pExcludedTags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    pFrameResult_t res(new FrameResult);
    RemoteStorageEnumerateUserPublishedFilesResult_t& rseupfr = res->CreateCallback<RemoteStorageEnumerateUserPublishedFilesResult_t>();
    rseupfr.m_nResultsReturned = 0;
    rseupfr.m_nTotalResultCount = 0;
    rseupfr.m_eResult = k_EResultOK;

    res->done = true;
    return res->GetAPICall();
}


SteamAPICall_t	Steam_RemoteStorage::PublishVideo(const char* pchVideoURL, const char* pchPreviewFile, AppId_t nConsumerAppId, const char* pchTitle, const char* pchDescription, ERemoteStoragePublishedFileVisibility eVisibility, SteamParamStringArray_t* pTags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

STEAM_CALL_RESULT(RemoteStoragePublishFileProgress_t)
SteamAPICall_t	Steam_RemoteStorage::PublishVideo(EWorkshopVideoProvider eVideoProvider, const char* pchVideoAccount, const char* pchVideoIdentifier, const char* pchPreviewFile, AppId_t nConsumerAppId, const char* pchTitle, const char* pchDescription, ERemoteStoragePublishedFileVisibility eVisibility, SteamParamStringArray_t* pTags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

STEAM_CALL_RESULT(RemoteStorageSetUserPublishedFileActionResult_t)
SteamAPICall_t	Steam_RemoteStorage::SetUserPublishedFileAction(PublishedFileId_t unPublishedFileId, EWorkshopFileAction eAction)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

STEAM_CALL_RESULT(RemoteStorageEnumeratePublishedFilesByUserActionResult_t)
SteamAPICall_t	Steam_RemoteStorage::EnumeratePublishedFilesByUserAction(EWorkshopFileAction eAction, uint32 unStartIndex)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    pFrameResult_t res(new FrameResult);
    RemoteStorageEnumeratePublishedFilesByUserActionResult_t& rsepfbuacr = res->CreateCallback<RemoteStorageEnumeratePublishedFilesByUserActionResult_t>();
    rsepfbuacr.m_nResultsReturned = 0;
    rsepfbuacr.m_nTotalResultCount = 0;
    rsepfbuacr.m_eResult = k_EResultOK;

    res->done = true;
    return res->GetAPICall();
}

// this method enumerates the public view of workshop files
STEAM_CALL_RESULT(RemoteStorageEnumerateWorkshopFilesResult_t)
SteamAPICall_t	Steam_RemoteStorage::EnumeratePublishedWorkshopFiles(EWorkshopEnumerationType eEnumerationType, uint32 unStartIndex, uint32 unCount, uint32 unDays, SteamParamStringArray_t* pTags, SteamParamStringArray_t* pUserTags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    pFrameResult_t res(new FrameResult);
    RemoteStorageEnumerateWorkshopFilesResult_t& rsewfr = res->CreateCallback<RemoteStorageEnumerateWorkshopFilesResult_t>();
    rsewfr.m_nResultsReturned = 0;
    rsewfr.m_nTotalResultCount = 0;
    rsewfr.m_eResult = k_EResultOK;

    res->done = true;
    return res->GetAPICall();
}

STEAM_CALL_RESULT(RemoteStorageDownloadUGCResult_t)
SteamAPICall_t Steam_RemoteStorage::UGCDownloadToLocation(UGCHandle_t hContent, const char* pchLocation, uint32 unPriority)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_uAPICallInvalid;
}

///////////////////////////////////////////////////////////////////////////////
//                                 IRunCallback                              //
///////////////////////////////////////////////////////////////////////////////
bool Steam_RemoteStorage::CBRunFrame()
{
    return true;
}

bool Steam_RemoteStorage::RunCallbacks(pFrameResult_t res)
{
    return res->done;
}