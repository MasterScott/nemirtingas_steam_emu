/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"

struct file_stream_t
{
    std::string file;
    std::string data;
};

class LOCAL_API Steam_RemoteStorage :
    public IRunCallback,
    public ISteamRemoteStorage001,
    public ISteamRemoteStorage002,
    public ISteamRemoteStorage003,
    public ISteamRemoteStorage004,
    public ISteamRemoteStorage005,
    public ISteamRemoteStorage006,
    public ISteamRemoteStorage007,
    public ISteamRemoteStorage008,
    public ISteamRemoteStorage009,
    public ISteamRemoteStorage010,
    public ISteamRemoteStorage011,
    public ISteamRemoteStorage012,
    public ISteamRemoteStorage013,
    public ISteamRemoteStorage014
{
    static const std::string remote_directory;

    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;

    bool _cloud;
    UGCFileWriteStreamHandle_t _write_handle;

    std::unordered_map<SteamAPICall_t, std::string> _async_file_name;
    std::unordered_map<UGCFileWriteStreamHandle_t, file_stream_t> _file_streams;
    std::vector<std::string> _files_cache;

    UGCFileWriteStreamHandle_t generate_write_handle();

public:
    static const std::string local_directory;
    std::recursive_mutex _local_mutex;

    Steam_RemoteStorage();
    virtual ~Steam_RemoteStorage();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    virtual bool CBRunFrame();
    virtual bool RunCallbacks(pFrameResult_t res);

    // Class funcs
    bool  file_write(std::string const& base_folder, std::string const& file, const void* pvData, int32 len);
    int32 file_read(std::string const& base_folder, std::string const& file, void* pvData, int32 len, int32 offset = 0);

    // NOTE
    //
    // Filenames are case-insensitive, and will be converted to lowercase automatically.
    // So "foo.bar" and "Foo.bar" are the same file, and if you write "Foo.bar" then
    // iterate the files, the filename returned will be "foo.bar".
    //

    // file operations
    virtual bool	FileWrite(const char* pchFile, const void* pvData, int32 cubData);
    virtual int32	FileRead(const char* pchFile, void* pvData, int32 cubDataToRead);

    STEAM_CALL_RESULT(RemoteStorageFileWriteAsyncComplete_t)
    virtual SteamAPICall_t FileWriteAsync(const char* pchFile, const void* pvData, uint32 cubData);

    STEAM_CALL_RESULT(RemoteStorageFileReadAsyncComplete_t)
    virtual SteamAPICall_t FileReadAsync(const char* pchFile, uint32 nOffset, uint32 cubToRead);
    virtual bool	FileReadAsyncComplete(SteamAPICall_t hReadCall, void* pvBuffer, uint32 cubToRead);

    virtual bool	FileForget(const char* pchFile);
    virtual bool	FileDelete(const char* pchFile);
    STEAM_CALL_RESULT(RemoteStorageFileShareResult_t)
    virtual SteamAPICall_t FileShare(const char* pchFile);
    virtual bool	SetSyncPlatforms(const char* pchFile, ERemoteStoragePlatform eRemoteStoragePlatform);

    // file operations that cause network IO
    virtual UGCFileWriteStreamHandle_t FileWriteStreamOpen(const char* pchFile);
    virtual bool FileWriteStreamWriteChunk(UGCFileWriteStreamHandle_t writeHandle, const void* pvData, int32 cubData);
    virtual bool FileWriteStreamClose(UGCFileWriteStreamHandle_t writeHandle);
    virtual bool FileWriteStreamCancel(UGCFileWriteStreamHandle_t writeHandle);

    // file information
    virtual bool	FileExists(const char* pchFile);
    virtual bool	FilePersisted(const char* pchFile);
    virtual int32	GetFileSize(const char* pchFile);
    virtual int64	GetFileTimestamp(const char* pchFile);
    virtual ERemoteStoragePlatform GetSyncPlatforms(const char* pchFile);

    // iteration
    virtual int32 GetFileCount();
    virtual const char* GetFileNameAndSize(int iFile, int32* pnFileSizeInBytes);

    // configuration management
    virtual bool GetQuota(int32* pnTotalBytes, int32* puAvailableBytes);
    virtual bool GetQuota(uint64* pnTotalBytes, uint64* puAvailableBytes);
    virtual bool IsCloudEnabledForAccount();
    virtual bool IsCloudEnabledForApp();
    virtual void SetCloudEnabledForApp(bool bEnabled);

    // user generated content

    // Downloads a UGC file.  A priority value of 0 will download the file immediately,
    // otherwise it will wait to download the file until all downloads with a lower priority
    // value are completed.  Downloads with equal priority will occur simultaneously.
    virtual SteamAPICall_t UGCDownload(UGCHandle_t hContent);
    STEAM_CALL_RESULT(RemoteStorageDownloadUGCResult_t)
    virtual SteamAPICall_t UGCDownload(UGCHandle_t hContent, uint32 unPriority);

    // Gets the amount of data downloaded so far for a piece of content. pnBytesExpected can be 0 if function returns false
    // or if the transfer hasn't started yet, so be careful to check for that before dividing to get a percentage
    virtual bool    GetUGCDownloadProgress(UGCHandle_t hContent, uint32* puDownloadedBytes, uint32* puTotalBytes);
    virtual bool    GetUGCDownloadProgress(UGCHandle_t hContent, int32* pnBytesDownloaded, int32* pnBytesExpected);

    // Gets metadata for a file after it has been downloaded. This is the same metadata given in the RemoteStorageDownloadUGCResult_t call result
    virtual bool	GetUGCDetails(UGCHandle_t hContent, AppId_t* pnAppID, STEAM_OUT_STRING() char** ppchName, int32* pnFileSizeInBytes, STEAM_OUT_STRUCT() CSteamID* pSteamIDOwner);

    // After download, gets the content of the file.  
    // Small files can be read all at once by calling this function with an offset of 0 and cubDataToRead equal to the size of the file.
    // Larger files can be read in chunks to reduce memory usage (since both sides of the IPC client and the game itself must allocate
    // enough memory for each chunk).  Once the last byte is read, the file is implicitly closed and further calls to UGCRead will fail
    // unless UGCDownload is called again.
    // For especially large files (anything over 100MB) it is a requirement that the file is read in chunks.
    virtual int32	UGCRead(UGCHandle_t hContent, void* pvData, int32 cubDataToRead);
    virtual int32	UGCRead(UGCHandle_t hContent, void* pvData, int32 cubDataToRead, uint32 cOffset);
    virtual int32	UGCRead(UGCHandle_t hContent, void* pvData, int32 cubDataToRead, uint32 cOffset, EUGCReadAction eAction);

    // Functions to iterate through UGC that has finished downloading but has not yet been read via UGCRead()
    virtual int32	GetCachedUGCCount();
    virtual	UGCHandle_t GetCachedUGCHandle(int32 iCachedContent);

    // The following functions are only necessary on the Playstation 3. On PC & Mac, the Steam client will handle these operations for you
    // On Playstation 3, the game controls which files are stored in the cloud, via FilePersist, FileFetch, and FileForget.

#if defined(_PS3) || defined(_SERVER)
        // Connect to Steam and get a list of files in the Cloud - results in a RemoteStorageAppSyncStatusCheck_t callback
    virtual void GetFileListFromServer();
    // Indicate this file should be downloaded in the next sync
    virtual bool FileFetch(const char* pchFile);
    // Indicate this file should be persisted in the next sync
    virtual bool FilePersist(const char* pchFile);
    // Pull any requested files down from the Cloud - results in a RemoteStorageAppSyncedClient_t callback
    virtual bool SynchronizeToClient();
    // Upload any requested files to the Cloud - results in a RemoteStorageAppSyncedServer_t callback
    virtual bool SynchronizeToServer();
    // Reset any fetch/persist/etc requests
    virtual bool ResetFileRequestState();
#endif

    // publishing UGC
    virtual SteamAPICall_t	PublishFile(const char* pchFile, const char* pchPreviewFile, AppId_t nConsumerAppId, const char* pchTitle, const char* pchDescription, ERemoteStoragePublishedFileVisibility eVisibility, SteamParamStringArray_t* pTags);
    SteamAPICall_t	PublishWorkshopFile(const char* pchFile, const char* pchPreviewFile, AppId_t nConsumerAppId, const char* pchTitle, const char* pchDescription, SteamParamStringArray_t* pTags);
    STEAM_CALL_RESULT(RemoteStoragePublishFileProgress_t)
    virtual SteamAPICall_t	PublishWorkshopFile(const char* pchFile, const char* pchPreviewFile, AppId_t nConsumerAppId, const char* pchTitle, const char* pchDescription, ERemoteStoragePublishedFileVisibility eVisibility, SteamParamStringArray_t* pTags, EWorkshopFileType eWorkshopFileType);

    virtual SteamAPICall_t	UpdatePublishedFile(RemoteStorageUpdatePublishedFileRequest_t updatePublishedFileRequest);

    virtual PublishedFileUpdateHandle_t CreatePublishedFileUpdateRequest(PublishedFileId_t unPublishedFileId);
    virtual bool UpdatePublishedFileFile(PublishedFileUpdateHandle_t updateHandle, const char* pchFile);
    virtual bool UpdatePublishedFilePreviewFile(PublishedFileUpdateHandle_t updateHandle, const char* pchPreviewFile);
    virtual bool UpdatePublishedFileTitle(PublishedFileUpdateHandle_t updateHandle, const char* pchTitle);
    virtual bool UpdatePublishedFileDescription(PublishedFileUpdateHandle_t updateHandle, const char* pchDescription);
    virtual bool UpdatePublishedFileVisibility(PublishedFileUpdateHandle_t updateHandle, ERemoteStoragePublishedFileVisibility eVisibility);
    virtual bool UpdatePublishedFileTags(PublishedFileUpdateHandle_t updateHandle, SteamParamStringArray_t* pTags);
    STEAM_CALL_RESULT(RemoteStorageUpdatePublishedFileResult_t)
        virtual SteamAPICall_t	CommitPublishedFileUpdate(PublishedFileUpdateHandle_t updateHandle);
    // Gets published file details for the given publishedfileid.  If unMaxSecondsOld is greater than 0,
    // cached data may be returned, depending on how long ago it was cached.  A value of 0 will force a refresh.
    // A value of k_WorkshopForceLoadPublishedFileDetailsFromCache will use cached data if it exists, no matter how old it is.
    virtual SteamAPICall_t	GetPublishedFileDetails(PublishedFileId_t unPublishedFileId);
    STEAM_CALL_RESULT(RemoteStorageGetPublishedFileDetailsResult_t)
    virtual SteamAPICall_t	GetPublishedFileDetails(PublishedFileId_t unPublishedFileId, uint32 unMaxSecondsOld);
    STEAM_CALL_RESULT(RemoteStorageDeletePublishedFileResult_t)
    virtual SteamAPICall_t	DeletePublishedFile(PublishedFileId_t unPublishedFileId);
    // enumerate the files that the current user published with this app
    STEAM_CALL_RESULT(RemoteStorageEnumerateUserPublishedFilesResult_t)
    virtual SteamAPICall_t	EnumerateUserPublishedFiles(uint32 unStartIndex);
    STEAM_CALL_RESULT(RemoteStorageSubscribePublishedFileResult_t)
    virtual SteamAPICall_t	SubscribePublishedFile(PublishedFileId_t unPublishedFileId);
    STEAM_CALL_RESULT(RemoteStorageEnumerateUserSubscribedFilesResult_t)
    virtual SteamAPICall_t	EnumerateUserSubscribedFiles(uint32 unStartIndex);
    STEAM_CALL_RESULT(RemoteStorageUnsubscribePublishedFileResult_t)
    virtual SteamAPICall_t	UnsubscribePublishedFile(PublishedFileId_t unPublishedFileId);
    virtual bool UpdatePublishedFileSetChangeDescription(PublishedFileUpdateHandle_t updateHandle, const char* pchChangeDescription);
    STEAM_CALL_RESULT(RemoteStorageGetPublishedItemVoteDetailsResult_t)
    virtual SteamAPICall_t	GetPublishedItemVoteDetails(PublishedFileId_t unPublishedFileId);
    STEAM_CALL_RESULT(RemoteStorageUpdateUserPublishedItemVoteResult_t)
    virtual SteamAPICall_t	UpdateUserPublishedItemVote(PublishedFileId_t unPublishedFileId, bool bVoteUp);
    STEAM_CALL_RESULT(RemoteStorageGetPublishedItemVoteDetailsResult_t)
    virtual SteamAPICall_t	GetUserPublishedItemVoteDetails(PublishedFileId_t unPublishedFileId);
    STEAM_CALL_RESULT(RemoteStorageEnumerateUserPublishedFilesResult_t)
    virtual SteamAPICall_t	EnumerateUserSharedWorkshopFiles(CSteamID steamId, uint32 unStartIndex, SteamParamStringArray_t* pRequiredTags, SteamParamStringArray_t* pExcludedTags);
    virtual SteamAPICall_t	PublishVideo(const char* pchVideoURL, const char* pchPreviewFile, AppId_t nConsumerAppId, const char* pchTitle, const char* pchDescription, ERemoteStoragePublishedFileVisibility eVisibility, SteamParamStringArray_t* pTags);
    STEAM_CALL_RESULT(RemoteStoragePublishFileProgress_t)
    virtual SteamAPICall_t	PublishVideo(EWorkshopVideoProvider eVideoProvider, const char* pchVideoAccount, const char* pchVideoIdentifier, const char* pchPreviewFile, AppId_t nConsumerAppId, const char* pchTitle, const char* pchDescription, ERemoteStoragePublishedFileVisibility eVisibility, SteamParamStringArray_t* pTags);
    STEAM_CALL_RESULT(RemoteStorageSetUserPublishedFileActionResult_t)
    virtual SteamAPICall_t	SetUserPublishedFileAction(PublishedFileId_t unPublishedFileId, EWorkshopFileAction eAction);
    STEAM_CALL_RESULT(RemoteStorageEnumeratePublishedFilesByUserActionResult_t)
    virtual SteamAPICall_t	EnumeratePublishedFilesByUserAction(EWorkshopFileAction eAction, uint32 unStartIndex);
    // this method enumerates the public view of workshop files
    STEAM_CALL_RESULT(RemoteStorageEnumerateWorkshopFilesResult_t)
    virtual SteamAPICall_t	EnumeratePublishedWorkshopFiles(EWorkshopEnumerationType eEnumerationType, uint32 unStartIndex, uint32 unCount, uint32 unDays, SteamParamStringArray_t* pTags, SteamParamStringArray_t* pUserTags);

    STEAM_CALL_RESULT(RemoteStorageDownloadUGCResult_t)
    virtual SteamAPICall_t UGCDownloadToLocation(UGCHandle_t hContent, const char* pchLocation, uint32 unPriority);
};
