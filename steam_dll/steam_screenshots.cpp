/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_screenshots.h"
#include "steam_client.h"
#include "settings.h"

decltype(Steam_Screenshots::screenshots_directory) Steam_Screenshots::screenshots_directory("screenshots");

Steam_Screenshots::Steam_Screenshots():
    _screenshot_hooked(false)
{
}

Steam_Screenshots::~Steam_Screenshots()
{
    emu_deinit();
}

void Steam_Screenshots::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _cb_manager = cb_manager;
    _network = network;
}

void Steam_Screenshots::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _network.reset();
    _cb_manager.reset();
}

ScreenshotHandle Steam_Screenshots::create_screenshot_handle()
{
    return _free_screenshot_handle++;
}

// Writes a screenshot to the user's screenshot library given the raw image data, which must be in RGB format.
// The return value is a handle that is valid for the duration of the game process and can be used to apply tags.
ScreenshotHandle Steam_Screenshots::WriteScreenshot(void* pubRGB, uint32 cubRGB, int nWidth, int nHeight)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    std::string buff(256, '\0');
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
    time_t now_time = std::chrono::system_clock::to_time_t(now);
    buff.resize(strftime(&buff[0], buff.length(), "%a_%b_%d_%H_%M_%S_%Y", localtime(&now_time)));
    std::string screenshot_name = FileManager::canonical_path(FileManager::join(screenshots_directory, buff + ".png"));

    FileManager::create_directory(screenshots_directory);
    if (!ImageManager::save_image_to_file(screenshot_name, pubRGB, nWidth, nHeight, 3))
    {
        APP_LOG(Log::LogLevel::WARN, "Failed to save screenshot: %s", screenshot_name.c_str());
        return INVALID_SCREENSHOT_HANDLE;
    }
    
    auto handle = create_screenshot_handle();
    auto& infos = _screenshots[handle];
    infos.screenshot_name = buff;
    infos.screenshot_time = now;

    APP_LOG(Log::LogLevel::INFO, "Saved screenshot: %s", screenshot_name.c_str());
    return handle;
}

// Adds a screenshot to the user's screenshot library from disk.  If a thumbnail is provided, it must be 200 pixels wide and the same aspect ratio
// as the screenshot, otherwise a thumbnail will be generated if the user uploads the screenshot.  The screenshots must be in either JPEG or TGA format.
// The return value is a handle that is valid for the duration of the game process and can be used to apply tags.
// JPEG, TGA, and PNG formats are supported.
ScreenshotHandle Steam_Screenshots::AddScreenshotToLibrary(const char* pchFilename, const char* pchThumbnailFilename, int nWidth, int nHeight)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (pchFilename == nullptr)
        return INVALID_SCREENSHOT_HANDLE;

    std::ifstream in_file(pchFilename, std::ios::in | std::ios::binary);
    if (!in_file.is_open())
        return INVALID_SCREENSHOT_HANDLE;

    std::string buff(256, '\0');
    auto now = std::chrono::system_clock::now();
    time_t now_time;
    now_time = now.time_since_epoch().count();
    buff.resize(strftime(&buff[0], buff.length(), "%a_%b_%d_%H_%M_%S_%Y", localtime(&now_time)));
    std::string screenshot_name = FileManager::canonical_path(FileManager::join(screenshots_directory, buff + ".png"));;

    FileManager::create_directory(screenshots_directory);
    std::ofstream out_file(screenshot_name, std::ios::out | std::ios::trunc | std::ios::binary);
    if (!out_file.is_open())
        return INVALID_SCREENSHOT_HANDLE;

    in_file.seekg(0, std::ios::end);
    size_t file_size = in_file.tellg();
    in_file.seekg(0, std::ios::beg);

    std::vector<char> buffer(file_size);
    in_file.read(buffer.data(), file_size);
    out_file.write(buffer.data(), file_size);

    ScreenshotHandle handle = create_screenshot_handle();
    auto& infos = _screenshots[handle];
    infos.screenshot_name = buff;
    infos.screenshot_time = now;

    return handle;
}

// Causes the Steam overlay to take a screenshot.  If screenshots are being hooked by the game then a ScreenshotRequested_t callback is sent back to the game instead. 
void Steam_Screenshots::TriggerScreenshot()
{
    APP_LOG(Log::LogLevel::TRACE, "");

    if (_screenshot_hooked)
    {
        pFrameResult_t res(new FrameResult);

        ScreenshotRequested_t& sr = res->CreateCallback<ScreenshotRequested_t>();

        res->done;
        _cb_manager->add_callback(nullptr, res);
    }
    else
    {
        APP_LOG(Log::LogLevel::INFO, "TODO: Make the overlay take a screenshot");
    }
}
// I:\Risk.of.Rain.2.v31.03.2020\Risk of Rain 2.exe
// Toggles whether the overlay handles screenshots when the user presses the screenshot hotkey, or the game handles them.  If the game is hooking screenshots,
// then the ScreenshotRequested_t callback will be sent if the user presses the hotkey, and the game is expected to call WriteScreenshot or AddScreenshotToLibrary
// in response.
void Steam_Screenshots::HookScreenshots(bool bHook)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    _screenshot_hooked = bHook;
}

// Sets metadata about a screenshot's location (for example, the name of the map)
bool Steam_Screenshots::SetLocation(ScreenshotHandle hScreenshot, const char* pchLocation)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    auto it = _screenshots.find(hScreenshot);
    if (it == _screenshots.end())
        return false;

    it->second.metadatas["locations"].push_back(pchLocation);
    FileManager::create_directory(screenshots_directory);
    FileManager::save_json(FileManager::join(screenshots_directory, it->second.screenshot_name + ".json"), it->second.metadatas);

    return true;
}

// Tags a user as being visible in the screenshot
bool Steam_Screenshots::TagUser(ScreenshotHandle hScreenshot, CSteamID steamID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    auto it = _screenshots.find(hScreenshot);
    if (it == _screenshots.end())
        return false;

    it->second.metadatas["users"].push_back(uint64_t(steamID.ConvertToUint64()));
    FileManager::create_directory(screenshots_directory);
    FileManager::save_json(FileManager::join(screenshots_directory, it->second.screenshot_name + ".json"), it->second.metadatas);

    return true;
}

// Tags a published file as being visible in the screenshot
bool Steam_Screenshots::TagPublishedFile(ScreenshotHandle hScreenshot, PublishedFileId_t unPublishedFileID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    auto it = _screenshots.find(hScreenshot);
    if (it == _screenshots.end())
        return false;

    it->second.metadatas["published_files"].push_back(uint64_t(unPublishedFileID));
    FileManager::create_directory(screenshots_directory);
    FileManager::save_json(FileManager::join(screenshots_directory, it->second.screenshot_name + ".json"), it->second.metadatas);

    return true;
}

// Returns true if the app has hooked the screenshot
bool Steam_Screenshots::IsScreenshotsHooked()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    
    return _screenshot_hooked;
}

// Adds a VR screenshot to the user's screenshot library from disk in the supported type.
// pchFilename should be the normal 2D image used in the library view
// pchVRFilename should contain the image that matches the correct type
// The return value is a handle that is valid for the duration of the game process and can be used to apply tags.
// JPEG, TGA, and PNG formats are supported.
ScreenshotHandle Steam_Screenshots::AddVRScreenshotToLibrary(EVRScreenshotType eType, const char* pchFilename, const char* pchVRFilename)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return 0;
}
