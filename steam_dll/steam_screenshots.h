/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"

struct screenshot_infos_t
{
    std::string screenshot_name;
    nlohmann::json metadatas;
    std::chrono::system_clock::time_point screenshot_time;
};

class LOCAL_API Steam_Screenshots :
    public ISteamScreenshots001,
    public ISteamScreenshots002,
    public ISteamScreenshots003
{
    static const std::string screenshots_directory;

    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;

    ScreenshotHandle _free_screenshot_handle;
    std::unordered_map<ScreenshotHandle, screenshot_infos_t> _screenshots;
    bool _screenshot_hooked;

public:
    std::recursive_mutex _local_mutex;

    Steam_Screenshots();
    virtual ~Steam_Screenshots();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    ScreenshotHandle create_screenshot_handle();

    // Writes a screenshot to the user's screenshot library given the raw image data, which must be in RGB format.
    // The return value is a handle that is valid for the duration of the game process and can be used to apply tags.
    virtual ScreenshotHandle WriteScreenshot(void* pubRGB, uint32 cubRGB, int nWidth, int nHeight);

    // Adds a screenshot to the user's screenshot library from disk.  If a thumbnail is provided, it must be 200 pixels wide and the same aspect ratio
    // as the screenshot, otherwise a thumbnail will be generated if the user uploads the screenshot.  The screenshots must be in either JPEG or TGA format.
    // The return value is a handle that is valid for the duration of the game process and can be used to apply tags.
    // JPEG, TGA, and PNG formats are supported.
    virtual ScreenshotHandle AddScreenshotToLibrary(const char* pchFilename, const char* pchThumbnailFilename, int nWidth, int nHeight);

    // Causes the Steam overlay to take a screenshot.  If screenshots are being hooked by the game then a ScreenshotRequested_t callback is sent back to the game instead. 
    virtual void TriggerScreenshot();

    // Toggles whether the overlay handles screenshots when the user presses the screenshot hotkey, or the game handles them.  If the game is hooking screenshots,
    // then the ScreenshotRequested_t callback will be sent if the user presses the hotkey, and the game is expected to call WriteScreenshot or AddScreenshotToLibrary
    // in response.
    virtual void HookScreenshots(bool bHook);

    // Sets metadata about a screenshot's location (for example, the name of the map)
    virtual bool SetLocation(ScreenshotHandle hScreenshot, const char* pchLocation);

    // Tags a user as being visible in the screenshot
    virtual bool TagUser(ScreenshotHandle hScreenshot, CSteamID steamID);

    // Tags a published file as being visible in the screenshot
    virtual bool TagPublishedFile(ScreenshotHandle hScreenshot, PublishedFileId_t unPublishedFileID);

    // Returns true if the app has hooked the screenshot
    virtual bool IsScreenshotsHooked();

    // Adds a VR screenshot to the user's screenshot library from disk in the supported type.
    // pchFilename should be the normal 2D image used in the library view
    // pchVRFilename should contain the image that matches the correct type
    // The return value is a handle that is valid for the duration of the game process and can be used to apply tags.
    // JPEG, TGA, and PNG formats are supported.
    virtual ScreenshotHandle AddVRScreenshotToLibrary(EVRScreenshotType eType, const char* pchFilename, const char* pchVRFilename);
};