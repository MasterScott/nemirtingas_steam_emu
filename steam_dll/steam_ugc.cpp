/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_ugc.h"
#include "steam_client.h"
#include "settings.h"

decltype(Steam_UGC::ugc_directory) Steam_UGC::ugc_directory("ugc");

Steam_UGC::Steam_UGC()
{
}

Steam_UGC::~Steam_UGC()
{
    emu_deinit();
}

void Steam_UGC::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock(_local_mutex, cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(cb_manager->_local_mutex, std::adopt_lock);

    _cb_manager = cb_manager;
    _network = network;

    _cb_manager->register_callbacks(this);
}

void Steam_UGC::emu_deinit()
{
    TRACE_FUNC();
    if (_cb_manager != nullptr)
    {
        std::lock(_local_mutex, _cb_manager->_local_mutex);
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
        std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

        _cb_manager->unregister_callbacks(this);
    }

    {
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
        _network.reset();
        _cb_manager.reset();
    }
}

// Query UGC associated with a user. Creator app id or consumer app id must be valid and be set to the current running app. unPage should start at 1.
UGCQueryHandle_t Steam_UGC::CreateQueryUserUGCRequest(AccountID_t unAccountID, EUserUGCList eListType, EUGCMatchingUGCType eMatchingUGCType, EUserUGCListSortOrder eSortOrder, AppId_t nCreatorAppID, AppId_t nConsumerAppID, uint32 unPage)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_UGCQueryHandleInvalid;
}

// Query for all matching UGC. Creator app id or consumer app id must be valid and be set to the current running app. unPage should start at 1.
UGCQueryHandle_t Steam_UGC::CreateQueryAllUGCRequest(EUGCQuery eQueryType, EUGCMatchingUGCType eMatchingeMatchingUGCTypeFileType, AppId_t nCreatorAppID, AppId_t nConsumerAppID, uint32 unPage)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_UGCQueryHandleInvalid;
}

// Query for all matching UGC using the new deep paging interface. Creator app id or consumer app id must be valid and be set to the current running app. pchCursor should be set to NULL or "*" to get the first result set.
UGCQueryHandle_t Steam_UGC::CreateQueryAllUGCRequest(EUGCQuery eQueryType, EUGCMatchingUGCType eMatchingeMatchingUGCTypeFileType, AppId_t nCreatorAppID, AppId_t nConsumerAppID, const char* pchCursor)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_UGCQueryHandleInvalid;
}

// Query for the details of the given published file ids (the RequestUGCDetails call is deprecated and replaced with this)
UGCQueryHandle_t Steam_UGC::CreateQueryUGCDetailsRequest(PublishedFileId_t* pvecPublishedFileID, uint32 unNumPublishedFileIDs)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_UGCQueryHandleInvalid;
}

// Send the query to Steam
STEAM_CALL_RESULT(SteamUGCQueryCompleted_t)
SteamAPICall_t Steam_UGC::SendQueryUGCRequest(UGCQueryHandle_t handle)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    pFrameResult_t res(new FrameResult);

    SteamUGCQueryCompleted_t& suqc = res->CreateCallback<SteamUGCQueryCompleted_t>();

    suqc.m_eResult = EResult::k_EResultOK;
    suqc.m_unNumResultsReturned = 0;
    suqc.m_unTotalMatchingResults = 0;
    suqc.m_handle = handle;

    res->done = true;
    _cb_manager->add_apicall(this, res);

    return res->GetAPICall();
}

// Retrieve an individual result after receiving the callback for querying UGC
bool Steam_UGC::GetQueryUGCResult(UGCQueryHandle_t handle, uint32 index, SteamUGCDetails_t* pDetails)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::GetQueryUGCPreviewURL(UGCQueryHandle_t handle, uint32 index, STEAM_OUT_STRING_COUNT(cchURLSize) char* pchURL, uint32 cchURLSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::GetQueryUGCMetadata(UGCQueryHandle_t handle, uint32 index, STEAM_OUT_STRING_COUNT(cchMetadatasize) char* pchMetadata, uint32 cchMetadatasize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::GetQueryUGCChildren(UGCQueryHandle_t handle, uint32 index, PublishedFileId_t* pvecPublishedFileID, uint32 cMaxEntries)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::GetQueryUGCStatistic(UGCQueryHandle_t handle, uint32 index, EItemStatistic eStatType, uint32* pStatValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    uint64 statVal;
    auto res = GetQueryUGCStatistic(handle, index, eStatType, &statVal);
    *pStatValue = static_cast<uint32>(statVal);
    return res;
}

bool Steam_UGC::GetQueryUGCStatistic(UGCQueryHandle_t handle, uint32 index, EItemStatistic eStatType, uint64* pStatValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

uint32 Steam_UGC::GetQueryUGCNumAdditionalPreviews(UGCQueryHandle_t handle, uint32 index)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

bool Steam_UGC::GetQueryUGCAdditionalPreview(UGCQueryHandle_t handle, uint32 index, uint32 previewIndex, char* pchURLOrVideoID, uint32 cchURLSize, bool* hz)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::GetQueryUGCAdditionalPreview(UGCQueryHandle_t handle, uint32 index, uint32 previewIndex, STEAM_OUT_STRING_COUNT(cchURLSize) char* pchURLOrVideoID, uint32 cchURLSize, STEAM_OUT_STRING_COUNT(cchURLSize) char* pchOriginalFileName, uint32 cchOriginalFileNameSize, EItemPreviewType* pPreviewType)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

uint32 Steam_UGC::GetQueryUGCNumKeyValueTags(UGCQueryHandle_t handle, uint32 index)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

bool Steam_UGC::GetQueryUGCKeyValueTag(UGCQueryHandle_t handle, uint32 index, uint32 keyValueTagIndex, STEAM_OUT_STRING_COUNT(cchKeySize) char* pchKey, uint32 cchKeySize, STEAM_OUT_STRING_COUNT(cchValueSize) char* pchValue, uint32 cchValueSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Return the first value matching the pchKey. Note that a key may map to multiple values.  Returns false if there was an error or no matching value was found.
bool Steam_UGC::GetQueryUGCKeyValueTag(UGCQueryHandle_t handle, uint32 index, const char* pchKey, STEAM_OUT_STRING_COUNT(cchValueSize) char* pchValue, uint32 cchValueSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Release the request to free up memory, after retrieving results
bool Steam_UGC::ReleaseQueryUGCRequest(UGCQueryHandle_t handle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Options to set for querying UGC
bool Steam_UGC::AddRequiredTag(UGCQueryHandle_t handle, const char* pTagName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::AddRequiredTagGroup(UGCQueryHandle_t handle, const SteamParamStringArray_t* pTagGroups)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::AddExcludedTag(UGCQueryHandle_t handle, const char* pTagName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::SetReturnOnlyIDs(UGCQueryHandle_t handle, bool bReturnOnlyIDs)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::SetReturnKeyValueTags(UGCQueryHandle_t handle, bool bReturnKeyValueTags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::SetReturnLongDescription(UGCQueryHandle_t handle, bool bReturnLongDescription)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::SetReturnMetadata(UGCQueryHandle_t handle, bool bReturnMetadata)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::SetReturnChildren(UGCQueryHandle_t handle, bool bReturnChildren)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::SetReturnAdditionalPreviews(UGCQueryHandle_t handle, bool bReturnAdditionalPreviews)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::SetReturnTotalOnly(UGCQueryHandle_t handle, bool bReturnTotalOnly)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::SetReturnPlaytimeStats(UGCQueryHandle_t handle, uint32 unDays)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::SetLanguage(UGCQueryHandle_t handle, const char* pchLanguage)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::SetAllowCachedResponse(UGCQueryHandle_t handle, uint32 unMaxAgeSeconds)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Options only for querying user UGC
bool Steam_UGC::SetCloudFileNameFilter(UGCQueryHandle_t handle, const char* pMatchCloudFileName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Options only for querying all UGC
bool Steam_UGC::SetMatchAnyTag(UGCQueryHandle_t handle, bool bMatchAnyTag)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::SetSearchText(UGCQueryHandle_t handle, const char* pSearchText)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::SetRankedByTrendDays(UGCQueryHandle_t handle, uint32 unDays)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::AddRequiredKeyValueTag(UGCQueryHandle_t handle, const char* pKey, const char* pValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// DEPRECATED - Use CreateQueryUGCDetailsRequest call above instead!
SteamAPICall_t Steam_UGC::RequestUGCDetails(PublishedFileId_t nPublishedFileID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return RequestUGCDetails(nPublishedFileID, 0);
}

SteamAPICall_t Steam_UGC::RequestUGCDetails(PublishedFileId_t nPublishedFileID, uint32 unMaxAgeSeconds)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// Steam Workshop Creator API
STEAM_CALL_RESULT(CreateItemResult_t)
SteamAPICall_t Steam_UGC::CreateItem(AppId_t nConsumerAppId, EWorkshopFileType eFileType)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    pFrameResult_t res(new FrameResult);

    CreateItemResult_t& cir = res->CreateCallback<CreateItemResult_t>();

    cir.m_eResult = EResult::k_EResultFail;
    cir.m_bUserNeedsToAcceptWorkshopLegalAgreement = false;
    cir.m_nPublishedFileId = 0;

    res->done = true;
    _cb_manager->add_apicall(this, res);

    return res->GetAPICall();
} // create new item for this app with no content attached yet

UGCUpdateHandle_t Steam_UGC::StartItemUpdate(AppId_t nConsumerAppId, PublishedFileId_t nPublishedFileID)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    return k_UGCUpdateHandleInvalid;
} // start an UGC item update. Set changed properties before commiting update with CommitItemUpdate()

bool Steam_UGC::SetItemTitle(UGCUpdateHandle_t handle, const char* pchTitle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} // change the title of an UGC item

bool Steam_UGC::SetItemDescription(UGCUpdateHandle_t handle, const char* pchDescription)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} // change the description of an UGC item

bool Steam_UGC::SetItemUpdateLanguage(UGCUpdateHandle_t handle, const char* pchLanguage)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} // specify the language of the title or description that will be set

bool Steam_UGC::SetItemMetadata(UGCUpdateHandle_t handle, const char* pchMetaData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} // change the metadata of an UGC item (max = k_cchDeveloperMetadataMax)

bool Steam_UGC::SetItemVisibility(UGCUpdateHandle_t handle, ERemoteStoragePublishedFileVisibility eVisibility)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} // change the visibility of an UGC item

bool Steam_UGC::SetItemTags(UGCUpdateHandle_t updateHandle, const SteamParamStringArray_t* pTags)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} // change the tags of an UGC item

bool Steam_UGC::SetItemContent(UGCUpdateHandle_t handle, const char* pszContentFolder)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} // update item content from this local folder

bool Steam_UGC::SetItemPreview(UGCUpdateHandle_t handle, const char* pszPreviewFile)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} //  change preview image file for this item. pszPreviewFile points to local image file, which must be under 1MB in size

bool Steam_UGC::SetAllowLegacyUpload(UGCUpdateHandle_t handle, bool bAllowLegacyUpload)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} //  use legacy upload for a single small file. The parameter to SetItemContent() should either be a directory with one file or the full path to the file.  The file must also be less than 10MB in size.

bool Steam_UGC::RemoveAllItemKeyValueTags(UGCUpdateHandle_t handle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} // remove all existing key-value tags (you can add new ones via the AddItemKeyValueTag function)

bool Steam_UGC::RemoveItemKeyValueTags(UGCUpdateHandle_t handle, const char* pchKey)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} // remove any existing key-value tags with the specified key

bool Steam_UGC::AddItemKeyValueTag(UGCUpdateHandle_t handle, const char* pchKey, const char* pchValue)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} // add new key-value tags for the item. Note that there can be multiple values for a tag.

bool Steam_UGC::AddItemPreviewFile(UGCUpdateHandle_t handle, const char* pszPreviewFile, EItemPreviewType type)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} //  add preview file for this item. pszPreviewFile points to local file, which must be under 1MB in size

bool Steam_UGC::AddItemPreviewVideo(UGCUpdateHandle_t handle, const char* pszVideoID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} //  add preview video for this item

bool Steam_UGC::UpdateItemPreviewFile(UGCUpdateHandle_t handle, uint32 index, const char* pszPreviewFile)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} //  updates an existing preview file for this item. pszPreviewFile points to local file, which must be under 1MB in size

bool Steam_UGC::UpdateItemPreviewVideo(UGCUpdateHandle_t handle, uint32 index, const char* pszVideoID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} //  updates an existing preview video for this item

bool Steam_UGC::RemoveItemPreview(UGCUpdateHandle_t handle, uint32 index)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
} // remove a preview by index starting at 0 (previews are sorted)

STEAM_CALL_RESULT(SubmitItemUpdateResult_t)
SteamAPICall_t Steam_UGC::SubmitItemUpdate(UGCUpdateHandle_t handle, const char* pchChangeNote)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
} // commit update process started with StartItemUpdate()

EItemUpdateStatus Steam_UGC::GetItemUpdateProgress(UGCUpdateHandle_t handle, uint64* punBytesProcessed, uint64* punBytesTotal)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EItemUpdateStatusInvalid;
}

// Steam Workshop Consumer API
STEAM_CALL_RESULT(SetUserItemVoteResult_t)
SteamAPICall_t Steam_UGC::SetUserItemVote(PublishedFileId_t nPublishedFileID, bool bVoteUp)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

STEAM_CALL_RESULT(GetUserItemVoteResult_t)
SteamAPICall_t Steam_UGC::GetUserItemVote(PublishedFileId_t nPublishedFileID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

STEAM_CALL_RESULT(UserFavoriteItemsListChanged_t)
SteamAPICall_t Steam_UGC::AddItemToFavorites(AppId_t nAppId, PublishedFileId_t nPublishedFileID)
{
    return 0;
}

STEAM_CALL_RESULT(UserFavoriteItemsListChanged_t)
SteamAPICall_t Steam_UGC::RemoveItemFromFavorites(AppId_t nAppId, PublishedFileId_t nPublishedFileID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

STEAM_CALL_RESULT(RemoteStorageSubscribePublishedFileResult_t)
SteamAPICall_t Steam_UGC::SubscribeItem(PublishedFileId_t nPublishedFileID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
} // subscribe to this item, will be installed ASAP

STEAM_CALL_RESULT(RemoteStorageUnsubscribePublishedFileResult_t)
SteamAPICall_t Steam_UGC::UnsubscribeItem(PublishedFileId_t nPublishedFileID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
} // unsubscribe from this item, will be uninstalled after game quits

uint32 Steam_UGC::GetNumSubscribedItems()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
} // number of subscribed items 

uint32 Steam_UGC::GetSubscribedItems(PublishedFileId_t* pvecPublishedFileID, uint32 cMaxEntries)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
} // all subscribed item PublishFileIDs

// get EItemState flags about item on this client
uint32 Steam_UGC::GetItemState(PublishedFileId_t nPublishedFileID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// get info about currently installed content on disc for items that have k_EItemStateInstalled set
// if k_EItemStateLegacyItem is set, pchFolder contains the path to the legacy file itself (not a folder)
bool Steam_UGC::GetItemInstallInfo(PublishedFileId_t nPublishedFileID, uint64* punSizeOnDisk, char* pchFolder, uint32 cchFolderSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    bool legacyItem;
    return GetItemInstallInfo(nPublishedFileID, punSizeOnDisk, pchFolder, cchFolderSize, &legacyItem);
}

bool Steam_UGC::GetItemInstallInfo(PublishedFileId_t nPublishedFileID, uint64* punSizeOnDisk, char* pchFolder, uint32 cchFolderSize, bool* pbLegacyItem)// returns true if item is installed
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::GetItemInstallInfo(PublishedFileId_t nPublishedFileID, uint64* punSizeOnDisk, STEAM_OUT_STRING_COUNT(cchFolderSize) char* pchFolder, uint32 cchFolderSize, uint32* punTimeStamp)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

bool Steam_UGC::GetItemUpdateInfo(PublishedFileId_t nPublishedFileID, bool* pbNeedsUpdate, bool* pbIsDownloading, uint64* punBytesDownloaded, uint64* punBytesTotal)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// get info about pending update for items that have k_EItemStateNeedsUpdate set. punBytesTotal will be valid after download started once
bool Steam_UGC::GetItemDownloadInfo(PublishedFileId_t nPublishedFileID, uint64* punBytesDownloaded, uint64* punBytesTotal)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// download new or update already installed item. If function returns true, wait for DownloadItemResult_t. If the item is already installed,
// then files on disk should not be used until callback received. If item is not subscribed to, it will be cached for some time.
// If bHighPriority is set, any other item download will be suspended and this item downloaded ASAP.
bool Steam_UGC::DownloadItem(PublishedFileId_t nPublishedFileID, bool bHighPriority)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// game servers can set a specific workshop folder before issuing any UGC commands.
// This is helpful if you want to support multiple game servers running out of the same install folder
bool Steam_UGC::BInitWorkshopForGameServer(DepotId_t unWorkshopDepotID, const char* pszFolder)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// SuspendDownloads( true ) will suspend all workshop downloads until SuspendDownloads( false ) is called or the game ends
void Steam_UGC::SuspendDownloads(bool bSuspend)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// usage tracking
STEAM_CALL_RESULT(StartPlaytimeTrackingResult_t)
SteamAPICall_t Steam_UGC::StartPlaytimeTracking(PublishedFileId_t* pvecPublishedFileID, uint32 unNumPublishedFileIDs)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

STEAM_CALL_RESULT(StopPlaytimeTrackingResult_t)
SteamAPICall_t Steam_UGC::StopPlaytimeTracking(PublishedFileId_t* pvecPublishedFileID, uint32 unNumPublishedFileIDs)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

STEAM_CALL_RESULT(StopPlaytimeTrackingResult_t)
SteamAPICall_t Steam_UGC::StopPlaytimeTrackingForAllItems()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// parent-child relationship or dependency management
STEAM_CALL_RESULT(AddUGCDependencyResult_t)
SteamAPICall_t Steam_UGC::AddDependency(PublishedFileId_t nParentPublishedFileID, PublishedFileId_t nChildPublishedFileID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

STEAM_CALL_RESULT(RemoveUGCDependencyResult_t)
SteamAPICall_t Steam_UGC::RemoveDependency(PublishedFileId_t nParentPublishedFileID, PublishedFileId_t nChildPublishedFileID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// add/remove app dependence/requirements (usually DLC)
STEAM_CALL_RESULT(AddAppDependencyResult_t)
SteamAPICall_t Steam_UGC::AddAppDependency(PublishedFileId_t nPublishedFileID, AppId_t nAppID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

STEAM_CALL_RESULT(RemoveAppDependencyResult_t)
SteamAPICall_t Steam_UGC::RemoveAppDependency(PublishedFileId_t nPublishedFileID, AppId_t nAppID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}
// request app dependencies. note that whatever callback you register for GetAppDependenciesResult_t may be called multiple times
// until all app dependencies have been returned
STEAM_CALL_RESULT(GetAppDependenciesResult_t)
SteamAPICall_t Steam_UGC::GetAppDependencies(PublishedFileId_t nPublishedFileID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// delete the item without prompting the user
STEAM_CALL_RESULT(DeleteItemResult_t)
SteamAPICall_t Steam_UGC::DeleteItem(PublishedFileId_t nPublishedFileID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

///////////////////////////////////////////////////////////////////////////////
//                           Network Send messages                           //
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//                          Network Receive messages                         //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//                                 IRunCallback                                 //
///////////////////////////////////////////////////////////////////////////////
bool Steam_UGC::CBRunFrame()
{
    return false;
}

bool Steam_UGC::RunNetwork(Network_Message_pb const& msg)
{
    return false;
}

bool Steam_UGC::RunCallbacks(pFrameResult_t res)
{
    return res->done;
}