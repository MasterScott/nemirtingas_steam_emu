/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"

class LOCAL_API Steam_UGC :
    public IRunCallback,
    public ISteamUGC001,
    public ISteamUGC002,
    public ISteamUGC003,
    public ISteamUGC004,
    public ISteamUGC005,
    public ISteamUGC006,
    public ISteamUGC007,
    public ISteamUGC008,
    public ISteamUGC009,
    public ISteamUGC010,
    public ISteamUGC012,
    public ISteamUGC013,
    public ISteamUGC014
{
    static const std::string ugc_directory;

    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;

public:
    std::recursive_mutex _local_mutex;

    Steam_UGC();
    virtual ~Steam_UGC();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    virtual bool CBRunFrame();
    virtual bool RunNetwork(Network_Message_pb const& msg);
    virtual bool RunCallbacks(pFrameResult_t res);

    // Query UGC associated with a user. Creator app id or consumer app id must be valid and be set to the current running app. unPage should start at 1.
    virtual UGCQueryHandle_t CreateQueryUserUGCRequest(AccountID_t unAccountID, EUserUGCList eListType, EUGCMatchingUGCType eMatchingUGCType, EUserUGCListSortOrder eSortOrder, AppId_t nCreatorAppID, AppId_t nConsumerAppID, uint32 unPage);

    // Query for all matching UGC. Creator app id or consumer app id must be valid and be set to the current running app. unPage should start at 1.
    virtual UGCQueryHandle_t CreateQueryAllUGCRequest(EUGCQuery eQueryType, EUGCMatchingUGCType eMatchingeMatchingUGCTypeFileType, AppId_t nCreatorAppID, AppId_t nConsumerAppID, uint32 unPage);

    // Query for all matching UGC using the new deep paging interface. Creator app id or consumer app id must be valid and be set to the current running app. pchCursor should be set to NULL or "*" to get the first result set.
    virtual UGCQueryHandle_t CreateQueryAllUGCRequest(EUGCQuery eQueryType, EUGCMatchingUGCType eMatchingeMatchingUGCTypeFileType, AppId_t nCreatorAppID, AppId_t nConsumerAppID, const char* pchCursor = NULL);

    // Query for the details of the given published file ids (the RequestUGCDetails call is deprecated and replaced with this)
    virtual UGCQueryHandle_t CreateQueryUGCDetailsRequest(PublishedFileId_t* pvecPublishedFileID, uint32 unNumPublishedFileIDs);

    // Send the query to Steam
    STEAM_CALL_RESULT(SteamUGCQueryCompleted_t)
    virtual SteamAPICall_t SendQueryUGCRequest(UGCQueryHandle_t handle);

    // Retrieve an individual result after receiving the callback for querying UGC
    virtual bool GetQueryUGCResult(UGCQueryHandle_t handle, uint32 index, SteamUGCDetails_t* pDetails);
    virtual bool GetQueryUGCPreviewURL(UGCQueryHandle_t handle, uint32 index, STEAM_OUT_STRING_COUNT(cchURLSize) char* pchURL, uint32 cchURLSize);
    virtual bool GetQueryUGCMetadata(UGCQueryHandle_t handle, uint32 index, STEAM_OUT_STRING_COUNT(cchMetadatasize) char* pchMetadata, uint32 cchMetadatasize);
    virtual bool GetQueryUGCChildren(UGCQueryHandle_t handle, uint32 index, PublishedFileId_t* pvecPublishedFileID, uint32 cMaxEntries);
    virtual bool GetQueryUGCStatistic(UGCQueryHandle_t handle, uint32 index, EItemStatistic eStatType, uint32* pStatValue);
    virtual bool GetQueryUGCStatistic(UGCQueryHandle_t handle, uint32 index, EItemStatistic eStatType, uint64* pStatValue);
    virtual uint32 GetQueryUGCNumAdditionalPreviews(UGCQueryHandle_t handle, uint32 index);
    virtual bool GetQueryUGCAdditionalPreview(UGCQueryHandle_t handle, uint32 index, uint32 previewIndex, char* pchURLOrVideoID, uint32 cchURLSize, bool* hz);
    virtual bool GetQueryUGCAdditionalPreview(UGCQueryHandle_t handle, uint32 index, uint32 previewIndex, STEAM_OUT_STRING_COUNT(cchURLSize) char* pchURLOrVideoID, uint32 cchURLSize, STEAM_OUT_STRING_COUNT(cchURLSize) char* pchOriginalFileName, uint32 cchOriginalFileNameSize, EItemPreviewType* pPreviewType);
    virtual uint32 GetQueryUGCNumKeyValueTags(UGCQueryHandle_t handle, uint32 index);
    virtual bool GetQueryUGCKeyValueTag(UGCQueryHandle_t handle, uint32 index, uint32 keyValueTagIndex, STEAM_OUT_STRING_COUNT(cchKeySize) char* pchKey, uint32 cchKeySize, STEAM_OUT_STRING_COUNT(cchValueSize) char* pchValue, uint32 cchValueSize);
    // Return the first value matching the pchKey. Note that a key may map to multiple values.  Returns false if there was an error or no matching value was found.
    virtual bool GetQueryUGCKeyValueTag(UGCQueryHandle_t handle, uint32 index, const char* pchKey, STEAM_OUT_STRING_COUNT(cchValueSize) char* pchValue, uint32 cchValueSize);

    // Release the request to free up memory, after retrieving results
    virtual bool ReleaseQueryUGCRequest(UGCQueryHandle_t handle);

    // Options to set for querying UGC
    virtual bool AddRequiredTag(UGCQueryHandle_t handle, const char* pTagName);
    virtual bool AddRequiredTagGroup(UGCQueryHandle_t handle, const SteamParamStringArray_t* pTagGroups);
    virtual bool AddExcludedTag(UGCQueryHandle_t handle, const char* pTagName);
    virtual bool SetReturnOnlyIDs(UGCQueryHandle_t handle, bool bReturnOnlyIDs);
    virtual bool SetReturnKeyValueTags(UGCQueryHandle_t handle, bool bReturnKeyValueTags);
    virtual bool SetReturnLongDescription(UGCQueryHandle_t handle, bool bReturnLongDescription);
    virtual bool SetReturnMetadata(UGCQueryHandle_t handle, bool bReturnMetadata);
    virtual bool SetReturnChildren(UGCQueryHandle_t handle, bool bReturnChildren);
    virtual bool SetReturnAdditionalPreviews(UGCQueryHandle_t handle, bool bReturnAdditionalPreviews);
    virtual bool SetReturnTotalOnly(UGCQueryHandle_t handle, bool bReturnTotalOnly);
    virtual bool SetReturnPlaytimeStats(UGCQueryHandle_t handle, uint32 unDays);
    virtual bool SetLanguage(UGCQueryHandle_t handle, const char* pchLanguage);
    virtual bool SetAllowCachedResponse(UGCQueryHandle_t handle, uint32 unMaxAgeSeconds);

    // Options only for querying user UGC
    virtual bool SetCloudFileNameFilter(UGCQueryHandle_t handle, const char* pMatchCloudFileName);

    // Options only for querying all UGC
    virtual bool SetMatchAnyTag(UGCQueryHandle_t handle, bool bMatchAnyTag);
    virtual bool SetSearchText(UGCQueryHandle_t handle, const char* pSearchText);
    virtual bool SetRankedByTrendDays(UGCQueryHandle_t handle, uint32 unDays);
    virtual bool AddRequiredKeyValueTag(UGCQueryHandle_t handle, const char* pKey, const char* pValue);

    // DEPRECATED - Use CreateQueryUGCDetailsRequest call above instead!
    virtual SteamAPICall_t RequestUGCDetails(PublishedFileId_t nPublishedFileID);
    virtual SteamAPICall_t RequestUGCDetails(PublishedFileId_t nPublishedFileID, uint32 unMaxAgeSeconds);

    // Steam Workshop Creator API
    STEAM_CALL_RESULT(CreateItemResult_t)
    virtual SteamAPICall_t CreateItem(AppId_t nConsumerAppId, EWorkshopFileType eFileType); // create new item for this app with no content attached yet

    virtual UGCUpdateHandle_t StartItemUpdate(AppId_t nConsumerAppId, PublishedFileId_t nPublishedFileID); // start an UGC item update. Set changed properties before commiting update with CommitItemUpdate()

    virtual bool SetItemTitle(UGCUpdateHandle_t handle, const char* pchTitle); // change the title of an UGC item
    virtual bool SetItemDescription(UGCUpdateHandle_t handle, const char* pchDescription); // change the description of an UGC item
    virtual bool SetItemUpdateLanguage(UGCUpdateHandle_t handle, const char* pchLanguage); // specify the language of the title or description that will be set
    virtual bool SetItemMetadata(UGCUpdateHandle_t handle, const char* pchMetaData); // change the metadata of an UGC item (max = k_cchDeveloperMetadataMax)
    virtual bool SetItemVisibility(UGCUpdateHandle_t handle, ERemoteStoragePublishedFileVisibility eVisibility); // change the visibility of an UGC item
    virtual bool SetItemTags(UGCUpdateHandle_t updateHandle, const SteamParamStringArray_t* pTags); // change the tags of an UGC item
    virtual bool SetItemContent(UGCUpdateHandle_t handle, const char* pszContentFolder); // update item content from this local folder
    virtual bool SetItemPreview(UGCUpdateHandle_t handle, const char* pszPreviewFile); //  change preview image file for this item. pszPreviewFile points to local image file, which must be under 1MB in size
    virtual bool SetAllowLegacyUpload(UGCUpdateHandle_t handle, bool bAllowLegacyUpload); //  use legacy upload for a single small file. The parameter to SetItemContent() should either be a directory with one file or the full path to the file.  The file must also be less than 10MB in size.
    virtual bool RemoveAllItemKeyValueTags(UGCUpdateHandle_t handle); // remove all existing key-value tags (you can add new ones via the AddItemKeyValueTag function)
    virtual bool RemoveItemKeyValueTags(UGCUpdateHandle_t handle, const char* pchKey); // remove any existing key-value tags with the specified key
    virtual bool AddItemKeyValueTag(UGCUpdateHandle_t handle, const char* pchKey, const char* pchValue); // add new key-value tags for the item. Note that there can be multiple values for a tag.
    virtual bool AddItemPreviewFile(UGCUpdateHandle_t handle, const char* pszPreviewFile, EItemPreviewType type); //  add preview file for this item. pszPreviewFile points to local file, which must be under 1MB in size
    virtual bool AddItemPreviewVideo(UGCUpdateHandle_t handle, const char* pszVideoID); //  add preview video for this item
    virtual bool UpdateItemPreviewFile(UGCUpdateHandle_t handle, uint32 index, const char* pszPreviewFile); //  updates an existing preview file for this item. pszPreviewFile points to local file, which must be under 1MB in size
    virtual bool UpdateItemPreviewVideo(UGCUpdateHandle_t handle, uint32 index, const char* pszVideoID); //  updates an existing preview video for this item
    virtual bool RemoveItemPreview(UGCUpdateHandle_t handle, uint32 index); // remove a preview by index starting at 0 (previews are sorted)

    STEAM_CALL_RESULT(SubmitItemUpdateResult_t)
    virtual SteamAPICall_t SubmitItemUpdate(UGCUpdateHandle_t handle, const char* pchChangeNote); // commit update process started with StartItemUpdate()

    virtual EItemUpdateStatus GetItemUpdateProgress(UGCUpdateHandle_t handle, uint64* punBytesProcessed, uint64* punBytesTotal);

    // Steam Workshop Consumer API
    STEAM_CALL_RESULT(SetUserItemVoteResult_t)
    virtual SteamAPICall_t SetUserItemVote(PublishedFileId_t nPublishedFileID, bool bVoteUp);

    STEAM_CALL_RESULT(GetUserItemVoteResult_t)
    virtual SteamAPICall_t GetUserItemVote(PublishedFileId_t nPublishedFileID);

    STEAM_CALL_RESULT(UserFavoriteItemsListChanged_t)
    virtual SteamAPICall_t AddItemToFavorites(AppId_t nAppId, PublishedFileId_t nPublishedFileID);

    STEAM_CALL_RESULT(UserFavoriteItemsListChanged_t)
    virtual SteamAPICall_t RemoveItemFromFavorites(AppId_t nAppId, PublishedFileId_t nPublishedFileID);

    STEAM_CALL_RESULT(RemoteStorageSubscribePublishedFileResult_t)
    virtual SteamAPICall_t SubscribeItem(PublishedFileId_t nPublishedFileID); // subscribe to this item, will be installed ASAP

    STEAM_CALL_RESULT(RemoteStorageUnsubscribePublishedFileResult_t)
    virtual SteamAPICall_t UnsubscribeItem(PublishedFileId_t nPublishedFileID); // unsubscribe from this item, will be uninstalled after game quits

    virtual uint32 GetNumSubscribedItems(); // number of subscribed items 
    virtual uint32 GetSubscribedItems(PublishedFileId_t* pvecPublishedFileID, uint32 cMaxEntries); // all subscribed item PublishFileIDs

    // get EItemState flags about item on this client
    virtual uint32 GetItemState(PublishedFileId_t nPublishedFileID);

    // get info about currently installed content on disc for items that have k_EItemStateInstalled set
    // if k_EItemStateLegacyItem is set, pchFolder contains the path to the legacy file itself (not a folder)
    virtual bool GetItemInstallInfo(PublishedFileId_t nPublishedFileID, uint64* punSizeOnDisk, char* pchFolder, uint32 cchFolderSize);
    virtual bool GetItemInstallInfo(PublishedFileId_t nPublishedFileID, uint64* punSizeOnDisk, char* pchFolder, uint32 cchFolderSize, bool* pbLegacyItem); // returns true if item is installed
    virtual bool GetItemInstallInfo(PublishedFileId_t nPublishedFileID, uint64* punSizeOnDisk, STEAM_OUT_STRING_COUNT(cchFolderSize) char* pchFolder, uint32 cchFolderSize, uint32* punTimeStamp);

    virtual bool GetItemUpdateInfo(PublishedFileId_t nPublishedFileID, bool* pbNeedsUpdate, bool* pbIsDownloading, uint64* punBytesDownloaded, uint64* punBytesTotal);

    // get info about pending update for items that have k_EItemStateNeedsUpdate set. punBytesTotal will be valid after download started once
    virtual bool GetItemDownloadInfo(PublishedFileId_t nPublishedFileID, uint64* punBytesDownloaded, uint64* punBytesTotal);

    // download new or update already installed item. If function returns true, wait for DownloadItemResult_t. If the item is already installed,
    // then files on disk should not be used until callback received. If item is not subscribed to, it will be cached for some time.
    // If bHighPriority is set, any other item download will be suspended and this item downloaded ASAP.
    virtual bool DownloadItem(PublishedFileId_t nPublishedFileID, bool bHighPriority);

    // game servers can set a specific workshop folder before issuing any UGC commands.
    // This is helpful if you want to support multiple game servers running out of the same install folder
    virtual bool BInitWorkshopForGameServer(DepotId_t unWorkshopDepotID, const char* pszFolder);

    // SuspendDownloads( true ) will suspend all workshop downloads until SuspendDownloads( false ) is called or the game ends
    virtual void SuspendDownloads(bool bSuspend);

    // usage tracking
    STEAM_CALL_RESULT(StartPlaytimeTrackingResult_t)
    virtual SteamAPICall_t StartPlaytimeTracking(PublishedFileId_t* pvecPublishedFileID, uint32 unNumPublishedFileIDs);
    STEAM_CALL_RESULT(StopPlaytimeTrackingResult_t)
    virtual SteamAPICall_t StopPlaytimeTracking(PublishedFileId_t* pvecPublishedFileID, uint32 unNumPublishedFileIDs);
    STEAM_CALL_RESULT(StopPlaytimeTrackingResult_t)
    virtual SteamAPICall_t StopPlaytimeTrackingForAllItems();

    // parent-child relationship or dependency management
    STEAM_CALL_RESULT(AddUGCDependencyResult_t)
    virtual SteamAPICall_t AddDependency(PublishedFileId_t nParentPublishedFileID, PublishedFileId_t nChildPublishedFileID);
    STEAM_CALL_RESULT(RemoveUGCDependencyResult_t)
    virtual SteamAPICall_t RemoveDependency(PublishedFileId_t nParentPublishedFileID, PublishedFileId_t nChildPublishedFileID);

    // add/remove app dependence/requirements (usually DLC)
    STEAM_CALL_RESULT(AddAppDependencyResult_t)
    virtual SteamAPICall_t AddAppDependency(PublishedFileId_t nPublishedFileID, AppId_t nAppID);
    STEAM_CALL_RESULT(RemoveAppDependencyResult_t)
    virtual SteamAPICall_t RemoveAppDependency(PublishedFileId_t nPublishedFileID, AppId_t nAppID);
    // request app dependencies. note that whatever callback you register for GetAppDependenciesResult_t may be called multiple times
    // until all app dependencies have been returned
    STEAM_CALL_RESULT(GetAppDependenciesResult_t)
    virtual SteamAPICall_t GetAppDependencies(PublishedFileId_t nPublishedFileID);

    // delete the item without prompting the user
    STEAM_CALL_RESULT(DeleteItemResult_t)
    virtual SteamAPICall_t DeleteItem(PublishedFileId_t nPublishedFileID);
};