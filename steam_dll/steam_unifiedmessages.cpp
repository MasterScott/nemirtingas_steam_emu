/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_unifiedmessages.h"
#include "steam_client.h"

Steam_UnifiedMessages::Steam_UnifiedMessages()
{
}

Steam_UnifiedMessages::~Steam_UnifiedMessages()
{
    emu_deinit();
}

void Steam_UnifiedMessages::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);
    _cb_manager = cb_manager;
    _network = network;
}

void Steam_UnifiedMessages::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);
    _network.reset();
    _cb_manager.reset();
}

// Sends a service method (in binary serialized form) using the Steam Client.
// Returns a unified message handle (k_InvalidUnifiedMessageHandle if could not send the message).
ClientUnifiedMessageHandle Steam_UnifiedMessages::SendMethod(const char* pchServiceMethod, const void* pRequestBuffer, uint32 unRequestBufferSize, uint64 unContext)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// Gets the size of the response and the EResult. Returns false if the response is not ready yet.
bool Steam_UnifiedMessages::GetMethodResponseInfo(ClientUnifiedMessageHandle hHandle, uint32* punResponseSize, EResult* peResult)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Gets a response in binary serialized form (and optionally release the corresponding allocated memory).
bool Steam_UnifiedMessages::GetMethodResponseData(ClientUnifiedMessageHandle hHandle, void* pResponseBuffer, uint32 unResponseBufferSize, bool bAutoRelease)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Releases the message and its corresponding allocated memory.
bool Steam_UnifiedMessages::ReleaseMethod(ClientUnifiedMessageHandle hHandle)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Sends a service notification (in binary serialized form) using the Steam Client.
// Returns true if the notification was sent successfully.
bool Steam_UnifiedMessages::SendNotification(const char* pchServiceNotification, const void* pNotificationBuffer, uint32 unNotificationBufferSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}