/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"

class LOCAL_API Steam_UnifiedMessages :
    public ISteamUnifiedMessages001
{
    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network>          _network;

public:
    std::recursive_mutex _local_mutex;

    Steam_UnifiedMessages();
    virtual ~Steam_UnifiedMessages();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    // Sends a service method (in binary serialized form) using the Steam Client.
    // Returns a unified message handle (k_InvalidUnifiedMessageHandle if could not send the message).
    virtual ClientUnifiedMessageHandle SendMethod(const char* pchServiceMethod, const void* pRequestBuffer, uint32 unRequestBufferSize, uint64 unContext);

    // Gets the size of the response and the EResult. Returns false if the response is not ready yet.
    virtual bool GetMethodResponseInfo(ClientUnifiedMessageHandle hHandle, uint32* punResponseSize, EResult* peResult);

    // Gets a response in binary serialized form (and optionally release the corresponding allocated memory).
    virtual bool GetMethodResponseData(ClientUnifiedMessageHandle hHandle, void* pResponseBuffer, uint32 unResponseBufferSize, bool bAutoRelease);

    // Releases the message and its corresponding allocated memory.
    virtual bool ReleaseMethod(ClientUnifiedMessageHandle hHandle);

    // Sends a service notification (in binary serialized form) using the Steam Client.
    // Returns true if the notification was sent successfully.
    virtual bool SendNotification(const char* pchServiceNotification, const void* pNotificationBuffer, uint32 unNotificationBufferSize);
};