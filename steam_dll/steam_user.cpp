/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_user.h"
#include "steam_client.h"
#include "settings.h"

Steam_User::Steam_User():
    _logged(true),
    _auth_manager(nullptr)
{
}

Steam_User::~Steam_User()
{
    emu_deinit();
}

void Steam_User::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock(_local_mutex, cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(cb_manager->_local_mutex, std::adopt_lock);

    _cb_manager = cb_manager;
    _network = network;

    _cb_manager->register_callbacks(this);

    _auth_manager = new Auth_Ticket_Manager();
    _auth_manager->emu_init(cb_manager, network);

    _network->set_peer_id(Network::peer_t(Settings::Inst().userid.ConvertToUint64()));
}

void Steam_User::emu_deinit()
{
    TRACE_FUNC();
    if (_cb_manager != nullptr)
    {
        std::lock(_local_mutex, _cb_manager->_local_mutex);
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
        std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

        _cb_manager->unregister_callbacks(this);
    }

    {
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
        delete _auth_manager; _auth_manager = nullptr;
        _network.reset();
        _cb_manager.reset();
    }
}

// returns the HSteamUser this interface represents
// this is only used internally by the API, and by a few select interfaces that support multi-user
HSteamUser Steam_User::GetHSteamUser()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return CLIENT_HSTEAMUSER;
}

// returns true if the Steam client current has a live connection to the Steam servers. 
// If false, it means there is no active connection due to either a networking issue on the local machine, or the Steam server is down/busy.
// The Steam client will automatically be trying to recreate the connection as often as possible.
bool Steam_User::BLoggedOn()
{
    //LOG(Log::LogLevel::TRACE, "");
    return _logged;
}

// returns the CSteamID of the account currently logged into the Steam client
// a CSteamID is a unique identifier for an account, and used to differentiate users in all parts of the Steamworks API
CSteamID Steam_User::GetSteamID()
{
    //LOG(Log::LogLevel::TRACE, "");
    return Settings::Inst().userid;
}

// Multiplayer Authentication functions

// InitiateGameConnection() starts the state machine for authenticating the game client with the game server
// It is the client portion of a three-way handshake between the client, the game server, and the steam servers
//
// Parameters:
// void *pAuthBlob - a pointer to empty memory that will be filled in with the authentication token.
// int cbMaxAuthBlob - the number of bytes of allocated memory in pBlob. Should be at least 2048 bytes.
// CSteamID steamIDGameServer - the steamID of the game server, received from the game server by the client
// CGameID gameID - the ID of the current game. For games without mods, this is just CGameID( <appID> )
// uint32 unIPServer, uint16 usPortServer - the IP address of the game server
// bool bSecure - whether or not the client thinks that the game server is reporting itself as secure (i.e. VAC is running)
//
// return value - returns the number of bytes written to pBlob. If the return is 0, then the buffer passed in was too small, and the call has failed
// The contents of pBlob should then be sent to the game server, for it to use to complete the authentication process.
int Steam_User::InitiateGameConnection(void* pAuthBlob, int cbMaxAuthBlob, CSteamID steamIDGameServer, CGameID gameID, uint32 unIPServer, uint16 usPortServer, bool bSecure)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return InitiateGameConnection(pAuthBlob, cbMaxAuthBlob, steamIDGameServer, unIPServer, usPortServer, bSecure);
}

int Steam_User::InitiateGameConnection(void* pAuthBlob, int cbMaxAuthBlob, CSteamID steamIDGameServer, uint32 unIPServer, uint16 usPortServer, bool bSecure)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (cbMaxAuthBlob < INITIATE_GAME_CONNECTION_TICKET_SIZE)
        return 0;

    uint32 out_size = INITIATE_GAME_CONNECTION_TICKET_SIZE;
    _auth_manager->get_ticket_data(pAuthBlob, INITIATE_GAME_CONNECTION_TICKET_SIZE, &out_size);
    return out_size;
}

// notify of disconnect
// needs to occur when the game client leaves the specified game server, needs to match with the InitiateGameConnection() call
void Steam_User::TerminateGameConnection(uint32 unIPServer, uint16 usPortServer)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// Legacy functions

// used by only a few games to track usage events
void Steam_User::TrackAppUsageEvent(CGameID gameID, int eAppUsageEvent, const char* pchExtraInfo)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

void Steam_User::RefreshSteam2Login()
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// get the local storage folder for current Steam account to write application data, e.g. save games, configs etc.
// this will usually be something like "C:\Progam Files\Steam\userdata\<SteamID>\<AppID>\local"
bool Steam_User::GetUserDataFolder(char* pchBuffer, int cubBuffer)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    std::string local_path = FileManager::canonical_path(Steam_RemoteStorage::local_directory);

    if (cubBuffer >= local_path.length())
    {
        strcpy(pchBuffer, local_path.c_str());
        return true;
    }

    return false;
}

// Starts voice recording. Once started, use GetVoice() to get the data
void Steam_User::StartVoiceRecording()
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// Stops voice recording. Because people often release push-to-talk keys early, the system will keep recording for
// a little bit after this function is called. GetVoice() should continue to be called until it returns
// k_eVoiceResultNotRecording
void Steam_User::StopVoiceRecording()
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// Determine the size of captured audio data that is available from GetVoice.
// Most applications will only use compressed data and should ignore the other
// parameters, which exist primarily for backwards compatibility. See comments
// below for further explanation of "uncompressed" data.
EVoiceResult Steam_User::GetAvailableVoice(uint32* pcbCompressed, uint32* pcbUncompressed_Deprecated, uint32 ncompressedVoiceDesiredSampleRate_Deprecated)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EVoiceResultNotRecording;
}

EVoiceResult Steam_User::GetAvailableVoice(uint32* pcbCompressed, uint32* pcbUncompressed)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return GetAvailableVoice(pcbCompressed, pcbUncompressed, 11025);
}

// ---------------------------------------------------------------------------
// NOTE: "uncompressed" audio is a deprecated feature and should not be used
// by most applications. It is raw single-channel 16-bit PCM wave data which
// may have been run through preprocessing filters and/or had silence removed,
// so the uncompressed audio could have a shorter duration than you expect.
// There may be no data at all during long periods of silence. Also, fetching
// uncompressed audio will cause GetVoice to discard any leftover compressed
// audio, so you must fetch both types at once. Finally, GetAvailableVoice is
// not precisely accurate when the uncompressed size is requested. So if you
// really need to use uncompressed audio, you should call GetVoice frequently
// with two very large (20kb+) output buffers instead of trying to allocate
// perfectly-sized buffers. But most applications should ignore all of these
// details and simply leave the "uncompressed" parameters as NULL/zero.
// ---------------------------------------------------------------------------

// Read captured audio data from the microphone buffer. This should be called
// at least once per frame, and preferably every few milliseconds, to keep the
// microphone input delay as low as possible. Most applications will only use
// compressed data and should pass NULL/zero for the "uncompressed" parameters.
// Compressed data can be transmitted by your application and decoded into raw
// using the DecompressVoice function below.
EVoiceResult Steam_User::GetCompressedVoice(void* pDestBuffer, uint32 cbDestBufferSize, uint32* nBytesWritten)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return GetVoice(true, pDestBuffer, cbDestBufferSize, nBytesWritten, false, nullptr, 0, nullptr);
}

EVoiceResult Steam_User::GetVoice(bool bWantCompressed, void* pDestBuffer, uint32 cbDestBufferSize, uint32* nBytesWritten, bool bWantUncompressed, void* pUncompressedDestBuffer, uint32 cbUncompressedDestBufferSize, uint32* nUncompressBytesWritten)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return GetVoice(bWantCompressed, pDestBuffer, cbDestBufferSize, nBytesWritten, bWantUncompressed, pUncompressedDestBuffer, cbUncompressedDestBufferSize, nUncompressBytesWritten, 11025);
}

EVoiceResult Steam_User::GetVoice(bool bWantCompressed, void* pDestBuffer, uint32 cbDestBufferSize, uint32* nBytesWritten, bool bWantUncompressed_Deprecated, void* pUncompressedDestBuffer_Deprecated, uint32 cbUncompressedDestBufferSize_Deprecated, uint32* nUncompressBytesWritten_Deprecated, uint32 nUncompressedVoiceDesiredSampleRate_Deprecated)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EVoiceResultNotRecording;
}

// Decodes the compressed voice data returned by GetVoice. The output data is
// raw single-channel 16-bit PCM audio. The decoder supports any sample rate
// from 11025 to 48000; see GetVoiceOptimalSampleRate() below for details.
// If the output buffer is not large enough, then *nBytesWritten will be set
// to the required buffer size, and k_EVoiceResultBufferTooSmall is returned.
// It is suggested to start with a 20kb buffer and reallocate as necessary.
EVoiceResult Steam_User::DecompressVoice(void* pCompressed, uint32 cbCompressed, void* pDestBuffer, uint32 cbDestBufferSize, uint32* nBytesWritten)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return DecompressVoice(pCompressed, cbCompressed, pDestBuffer, cbDestBufferSize, nBytesWritten, 11025);
}

EVoiceResult Steam_User::DecompressVoice(const void* pCompressed, uint32 cbCompressed, void* pDestBuffer, uint32 cbDestBufferSize, uint32* nBytesWritten)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return DecompressVoice(pCompressed, cbCompressed, pDestBuffer, cbDestBufferSize, nBytesWritten, 11025);
}

EVoiceResult Steam_User::DecompressVoice(const void* pCompressed, uint32 cbCompressed, void* pDestBuffer, uint32 cbDestBufferSize, uint32* nBytesWritten, uint32 nDesiredSampleRate)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EVoiceResultNotRecording;
}

// This returns the native sample rate of the Steam voice decompressor; using
// this sample rate for DecompressVoice will perform the least CPU processing.
// However, the final audio quality will depend on how well the audio device
// (and/or your application's audio output SDK) deals with lower sample rates.
// You may find that you get the best audio output quality when you ignore
// this function and use the native sample rate of your audio output device,
// which is usually 48000 or 44100.
uint32 Steam_User::GetVoiceOptimalSampleRate()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 48000;
}

// Retrieve ticket to be sent to the entity who wishes to authenticate you. 
// pcbTicket retrieves the length of the actual ticket.
HAuthTicket Steam_User::GetAuthSessionTicket(void* pTicket, int cbMaxTicket, uint32* pcbTicket)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    return _auth_manager->get_ticket(pTicket, cbMaxTicket, pcbTicket);
}

// Authenticate ticket from entity steamID to be sure it is valid and isnt reused
// Registers for callbacks if the entity goes offline or cancels the ticket ( see ValidateAuthTicketResponse_t callback and EAuthSessionResponse )
EBeginAuthSessionResult Steam_User::BeginAuthSession(const void* pAuthTicket, int cbAuthTicket, CSteamID steamID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    return _auth_manager->begin_auth(pAuthTicket, cbAuthTicket, steamID);
}

// Stop tracking started by BeginAuthSession - called when no longer playing game with this entity
void Steam_User::EndAuthSession(CSteamID steamID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _auth_manager->end_auth(steamID);
}

// Cancel auth ticket from GetAuthSessionTicket, called when no longer playing game with the entity you gave the ticket to
void Steam_User::CancelAuthTicket(HAuthTicket hAuthTicket)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _auth_manager->cancel_ticket(hAuthTicket);
}

// After receiving a user's authentication data, and passing it to BeginAuthSession, use this function
// to determine if the user owns downloadable content specified by the provided AppID.
EUserHasLicenseForAppResult Steam_User::UserHasLicenseForApp(CSteamID steamID, AppId_t appID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return k_EUserHasLicenseResultHasLicense;
}

// returns true if this users looks like they are behind a NAT device. Only valid once the user has connected to steam 
// (i.e a SteamServersConnected_t has been issued) and may not catch all forms of NAT.
bool Steam_User::BIsBehindNAT()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// set data to be replicated to friends so that they can join your game
// CSteamID steamIDGameServer - the steamID of the game server, received from the game server by the client
// uint32 unIPServer, uint16 usPortServer - the IP address of the game server
void Steam_User::AdvertiseGame(CSteamID steamIDGameServer, uint32 unIPServer, uint16 usPortServer)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// Requests a ticket encrypted with an app specific shared key
// pDataToInclude, cbDataToInclude will be encrypted into the ticket
// ( This is asynchronous, you must wait for the ticket to be completed by the server )
STEAM_CALL_RESULT(EncryptedAppTicketResponse_t)
SteamAPICall_t Steam_User::RequestEncryptedAppTicket(void* pDataToInclude, int cbDataToInclude)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    pFrameResult_t result(new FrameResult);
    EncryptedAppTicketResponse_t& data = result->CreateCallback<EncryptedAppTicketResponse_t>();
    _encrypted_app_ticket = std::string((char*)pDataToInclude, cbDataToInclude);

    _cb_manager->add_apicall(this, result);

    return result->GetAPICall();
}

// retrieve a finished ticket
bool Steam_User::GetEncryptedAppTicket(void* pTicket, int cbMaxTicket, uint32* pcbTicket)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (!pcbTicket) return false;

    unsigned int ticket_size = _encrypted_app_ticket.size() + 126;
    if (!cbMaxTicket) {
        *pcbTicket = ticket_size;
        return true;
    }

    if (!pTicket) return false;

    //TODO figure out exact sizes?
    cbMaxTicket = std::min<int>(cbMaxTicket, ticket_size);

    char ticket_base[] = { 0x08, 0x01 };
    memset(pTicket, 'g', cbMaxTicket);
    memcpy(pTicket, ticket_base, sizeof(ticket_base));
    *pcbTicket = cbMaxTicket;

    return true;
}

// Trading Card badges data access
// if you only have one set of cards, the series will be 1
// the user has can have two different badges for a series; the regular (max level 5) and the foil (max level 1)
int Steam_User::GetGameBadgeLevel(int nSeries, bool bFoil)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 100;
}

// gets the Steam Level of the user, as shown on their profile
int Steam_User::GetPlayerSteamLevel()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 100;
}

// Requests a URL which authenticates an in-game browser for store check-out,
// and then redirects to the specified URL. As long as the in-game browser
// accepts and handles session cookies, Steam microtransaction checkout pages
// will automatically recognize the user instead of presenting a login page.
// The result of this API call will be a StoreAuthURLResponse_t callback.
// NOTE: The URL has a very short lifetime to prevent history-snooping attacks,
// so you should only call this API when you are about to launch the browser,
// or else immediately navigate to the result URL using a hidden browser window.
// NOTE 2: The resulting authorization cookie has an expiration time of one day,
// so it would be a good idea to request and visit a new auth URL every 12 hours.
STEAM_CALL_RESULT(StoreAuthURLResponse_t)
SteamAPICall_t Steam_User::RequestStoreAuthURL(const char* pchRedirectURL)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// gets whether the users phone number is verified 
bool Steam_User::BIsPhoneVerified()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return true;
}

// gets whether the user has two factor enabled on their account
bool Steam_User::BIsTwoFactorEnabled()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return true;
}

// gets whether the users phone number is identifying
bool Steam_User::BIsPhoneIdentifying()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// gets whether the users phone number is awaiting (re)verification
bool Steam_User::BIsPhoneRequiringVerification()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

STEAM_CALL_RESULT(MarketEligibilityResponse_t)
SteamAPICall_t Steam_User::GetMarketEligibility()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// Retrieves anti indulgence / duration control for current user
STEAM_CALL_RESULT(DurationControl_t)
SteamAPICall_t Steam_User::GetDurationControl()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 0;
}

// Advise steam china duration control system about the online state of the game.
// This will prevent offline gameplay time from counting against a user's
// playtime limits.
bool Steam_User::BSetDurationControlOnlineState(EDurationControlOnlineState eNewState)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//                           Network Send messages                           //
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//                          Network Receive messages                         //
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//                                 IRunCallback                                 //
///////////////////////////////////////////////////////////////////////////////
bool Steam_User::CBRunFrame()
{
    return false;
}

bool Steam_User::RunNetwork(Network_Message_pb const& msg)
{
    return false;
}

bool Steam_User::RunCallbacks(pFrameResult_t res)
{
    switch (res->ICallback())
    {
        case EncryptedAppTicketResponse_t::k_iCallback:
        {
            EncryptedAppTicketResponse_t& eatr = res->GetCallback<EncryptedAppTicketResponse_t>();
            eatr.m_eResult = k_EResultOK;

            res->done = true;
        }
        break;
    }
    return res->done;
}