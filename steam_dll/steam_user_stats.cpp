/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_user_stats.h"
#include "steam_client.h"
#include "settings.h"

decltype(Steam_UserStats::achievements_path)    Steam_UserStats::achievements_path("achievements.json");
decltype(Steam_UserStats::db_achievements_path) Steam_UserStats::db_achievements_path("db_achievements.json");
decltype(Steam_UserStats::stats_path)           Steam_UserStats::stats_path("stats.json");
decltype(Steam_UserStats::leaderboards_path)    Steam_UserStats::leaderboards_path("leaderboards.json");

Steam_UserStats::Steam_UserStats():
    _achs_dirty(false),
    _stats_dirty(false)
{
}

Steam_UserStats::~Steam_UserStats()
{
    emu_deinit();
}

void Steam_UserStats::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock(_local_mutex, cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(cb_manager->_local_mutex, std::adopt_lock);

    _cb_manager = cb_manager;
    _network = network;

    _cb_manager->register_callbacks(this);

    CSteamID userid = Settings::Inst().userid;
    user_stats_t& stats = _user_stats[userid];

    FileManager::load_json(db_achievements_path, _achievements_db);
    FileManager::load_json(stats_path, stats.stats);
    FileManager::load_json(achievements_path, stats.achievements);
    FileManager::load_json(leaderboards_path, _leaderboards);
}

void Steam_UserStats::emu_deinit()
{
    TRACE_FUNC();
    // Save stats if leaving & dirty
    _user_stats[Settings::Inst().userid].requesting = false;
    save_stats();

    if (_cb_manager != nullptr)
    {
        std::lock(_local_mutex, _cb_manager->_local_mutex);
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
        std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);
        _cb_manager->unregister_callbacks(this);
    }

    {
        std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
        _network.reset();
        _cb_manager.reset();
    }
}

nlohmann::json* Steam_UserStats::get_achievment_db_info(std::string const& name)
{
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    for (int i = 0; i < _achievements_db.size(); ++i)
    {
        try
        {
            if (_achievements_db[i]["name"] == name)
            {
                return &_achievements_db[i];
            }
        }
        catch (...)
        {
        }
    }
    return nullptr;
}

nlohmann::json* Steam_UserStats::get_stats(CSteamID const& id, std::string const& name)
{
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    nlohmann::json &json = _user_stats[id].stats;
    
    try
    {
        auto it = json.find(name);
        if (it != json.end())
            return &(*it);
    }
    catch(...)
    { }

    return nullptr;
}

bool Steam_UserStats::save_stats()
{
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    CSteamID const& userid = Settings::Inst().userid;
    auto& stats = _user_stats[userid];

    if (stats.requesting)
        return false;

    if (_stats_dirty)
    {
        _stats_dirty = !FileManager::save_json(stats_path, stats.stats);
    }

    if (_achs_dirty)
    {
        _achs_dirty = !FileManager::save_json(achievements_path, stats.achievements);

        if (!_achs_dirty)
        {
            for (auto it = stats.achievements.begin(); it != stats.achievements.end(); ++it)
            {
                pFrameResult_t result(new FrameResult);
                UserAchievementStored_t& uas = result->CreateCallback<UserAchievementStored_t>();

                strncpy(uas.m_rgchAchievementName, it.key().c_str(), k_cchStatNameMax);
                uas.m_nGameID = Settings::Inst().gameid.ToUint64();
                uas.m_bGroupAchievement = false;
                bool earned;
                try
                {
                    earned = it.value()["earned"];
                }
                catch (...)
                {
                    earned = false;
                }
                try
                {
                    if (earned)
                        it.value()["cur_progress"] = static_cast<uint32_t>(0);

                    uas.m_nCurProgress = it.value()["cur_progress"];
                }
                catch (...)
                {
                    uas.m_nCurProgress = 0;
                    it.value()["cur_progress"] = static_cast<uint32_t>(0);
                }
                try
                {
                    if (earned)
                        it.value()["max_progress"] = static_cast<uint32_t>(0);

                    uas.m_nMaxProgress = it.value()["max_progress"];
                }
                catch (...)
                {
                    uas.m_nMaxProgress = 0;
                    it.value()["max_progress"] = static_cast<uint32_t>(0);
                }

                result->done = true;
                _cb_manager->add_callback(this, result);
            }
        }
    }

    return true;
}

// Ask the server to send down this user's data and achievements for this game
STEAM_CALL_BACK(UserStatsReceived_t)
bool Steam_UserStats::RequestCurrentStats()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    auto steamid = Settings::Inst().userid;
    _user_stats[steamid].requesting = true;
    
    pFrameResult_t result(new FrameResult);
    UserStatsReceived_t& stats = result->CreateCallback<UserStatsReceived_t>();
    stats.m_steamIDUser = steamid;

    _cb_manager->add_callback(this, result);

    return GetSteam_User().BLoggedOn();
}

// Data accessors
bool Steam_UserStats::GetStat(const char* pchName, int32* pData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (pchName == nullptr)
        return false;

    nlohmann::json* obj = get_stats(Settings::Inst().userid, pchName);
    if (obj == nullptr)
        return false;
    
    if (pData != nullptr)
    {
        try
        {
            *pData = (*obj)[pchName];
        }
        catch (...)
        {
            *pData = 0;
        }
    }

    return true;
}

bool Steam_UserStats::GetStat(const char* pchName, float* pData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (pchName == nullptr)
        return false;
    nlohmann::json* obj = get_stats(Settings::Inst().userid, pchName);
    if (obj == nullptr)
        return false;

    if (pData != nullptr)
    {
        try
        {
            *pData = (*obj)[pchName];
        }
        catch (...)
        {
            *pData = 0.0f;
        }
    }

    return true;
}

// Set / update data
bool Steam_UserStats::SetStat(const char* pchName, int32 nData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (pchName == nullptr)
        return false;

    _user_stats[Settings::Inst().userid].stats[pchName] = nData;
    _stats_dirty = true;

    return true;
}

bool Steam_UserStats::SetStat(const char* pchName, float fData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (pchName == nullptr)
        return false;

    _user_stats[Settings::Inst().userid].stats[pchName] = fData;
    _stats_dirty = true;

    return true;
}

bool Steam_UserStats::UpdateAvgRateStat(const char* pchName, float flCountThisSession, double dSessionLength)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    return false;
}

// Achievement flag accessors
bool Steam_UserStats::GetAchievement(const char* pchName, bool* pbAchieved)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (pchName == nullptr)
        return false;
    nlohmann::json* obj = get_achievment_db_info(pchName);

    if (obj == nullptr)
    {
        if(pbAchieved != nullptr)
            *pbAchieved = false;
        return false;
    }

    if (pbAchieved != nullptr)
    {
        try
        {
            *pbAchieved = _user_stats[Settings::Inst().userid].achievements[pchName]["earned"];
        }
        catch (...)
        {
            *pbAchieved = false;
        }
    }
    
    return true;
}

bool Steam_UserStats::SetAchievement(const char* pchName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (pchName == nullptr)
        return false;

    nlohmann::json* obj = get_achievment_db_info(pchName);
    if (obj == nullptr)
        return false;

    auto& ach = _user_stats[Settings::Inst().userid].achievements[pchName];
    if (!ach.contains("earned") || !ach["earned"])
    {
        GetSteam_Overlay().AddAchievementNotification(*obj);

        ach["earned"] = true;
        ach["earned_time"] = std::chrono::duration_cast<std::chrono::duration<uint32>>(std::chrono::system_clock::now().time_since_epoch()).count();
        _achs_dirty = true;
    }

    return true;
}

bool Steam_UserStats::ClearAchievement(const char* pchName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (pchName == nullptr)
        return false;

    nlohmann::json* obj = get_achievment_db_info(pchName);
    if (obj == nullptr)
        return false;

    auto& ach = _user_stats[Settings::Inst().userid].achievements[pchName];
    ach["earned"] = false;
    ach["cur_progress"] = 0;
    _achs_dirty = true;

    return true;
}

// Get the achievement status, and the time it was unlocked if unlocked.
// If the return value is true, but the unlock time is zero, that means it was unlocked before Steam 
// began tracking achievement unlock times (December 2009). Time is seconds since January 1, 1970.
bool Steam_UserStats::GetAchievementAndUnlockTime(const char* pchName, bool* pbAchieved, uint32* punUnlockTime)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (pchName == nullptr)
        return false;
    nlohmann::json* obj = get_achievment_db_info(pchName);

    if (obj == nullptr)
    {
        if (pbAchieved != nullptr)
            *pbAchieved = false;
        if (punUnlockTime != nullptr)
            *punUnlockTime = 0;

        return false;
    }

    auto& ach = _user_stats[Settings::Inst().userid].achievements[pchName];
    if (pbAchieved != nullptr)
    {
        try
        {
            *pbAchieved = ach["earned"];
        }
        catch (...)
        {
            *pbAchieved = false;
        }
    }
    if (pbAchieved != nullptr)
    {
        try
        {
            *punUnlockTime = ach["earned_time"];
        }
        catch (...)
        {
            *punUnlockTime = static_cast<uint32_t>(std::chrono::system_clock::now().time_since_epoch().count());
        }
    }

    return true;
}

// Store the current data on the server, will get a callback when set
// And one callback for every new achievement
//
// If the callback has a result of k_EResultInvalidParam, one or more stats 
// uploaded has been rejected, either because they broke constraints
// or were out of date. In this case the server sends back updated values.
// The stats should be re-iterated to keep in sync.
bool Steam_UserStats::StoreStats()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);
    
    {
        pFrameResult_t result(new FrameResult);
        UserStatsStored_t& uss = result->CreateCallback<UserStatsStored_t>();
        uss.m_nGameID = Settings::Inst().gameid.ToUint64();
        uss.m_eResult = k_EResultOK;

        _cb_manager->add_callback(this, result);
    }

    return true;
}

// Achievement / GroupAchievement metadata

// Gets the icon of the achievement, which is a handle to be used in ISteamUtils::GetImageRGBA(), or 0 if none set. 
// A return value of 0 may indicate we are still fetching data, and you can wait for the UserAchievementIconFetched_t callback
// which will notify you when the bits are ready. If the callback still returns zero, then there is no image set for the
// specified achievement.
int Steam_UserStats::GetAchievementIcon(const char* pchName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    nlohmann::json* obj = get_achievment_db_info(pchName);
    if (obj == nullptr)
        return 0;

    auto it = obj->find("icon_handle");
    if (it == obj->end())
    {
        auto name_it = obj->find("name");
        if (name_it == obj->end())
            return 0;
        
        (*obj)["icon_handle"] = GetSteam_Utils().generate_image_handle(FileManager::canonical_path("ach_images/" + name_it->get_ref<std::string&>() + ".jpg"), 64, 64);
    }

    return (*obj)["icon_handle"];
}

// Get general attributes for an achievement. Accepts the following keys:
// - "name" and "desc" for retrieving the localized achievement name and description (returned in UTF8)
// - "hidden" for retrieving if an achievement is hidden (returns "0" when not hidden, "1" when hidden)
const char* Steam_UserStats::GetAchievementDisplayAttribute(const char* pchName, const char* pchKey)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (pchName == nullptr || pchKey == nullptr)
        return "";

    nlohmann::json* obj = get_achievment_db_info(pchName);
    if (obj != nullptr)
    {
        try
        {
            switchstr(pchName)
            {
                casestr("desc")  : return (*obj)["description"].get_ref<std::string const&>().c_str();
                casestr("name")  : return (*obj)["displayName"].get_ref<std::string const&>().c_str();
                casestr("hidden"): return ((*obj)["hidden"].get<uint32_t>() == 0 ? "0" : "1");
            }
        }
        catch (...)
        {
        }
    }
    
    APP_LOG(Log::LogLevel::WARN, "Failed to get achievement attribute: %s - %s", pchName, pchKey);
    return "";
}

// Achievement progress - triggers an AchievementProgress callback, that is all.
// Calling this w/ N out of N progress will NOT set the achievement, the game must still do that.
bool Steam_UserStats::IndicateAchievementProgress(const char* pchName, uint32 nCurProgress, uint32 nMaxProgress)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (pchName == nullptr)
        return false;

    try
    {
        auto& ach = _user_stats[Settings::Inst().userid].achievements[pchName];
        ach["cur_progress"] = nCurProgress;
        ach["max_progress"] = nMaxProgress;

        _achs_dirty = true;
        return true;
    }
    catch(...)
    { }

    return false;
}

// Used for iterating achievements. In general games should not need these functions because they should have a
// list of existing achievements compiled into them
uint32 Steam_UserStats::GetNumAchievements()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    return _achievements_db.size();
}
// Get achievement name iAchievement in [0,GetNumAchievements)
const char* Steam_UserStats::GetAchievementName(uint32 iAchievement)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    try
    {
        return _achievements_db[iAchievement]["name"].get_ref<std::string&>().c_str();
    }
    catch (...)
    {}

    return "";
}

// Friends stats & achievements

// downloads stats for the user
// returns a UserStatsReceived_t received when completed
// if the other user has no stats, UserStatsReceived_t.m_eResult will be set to k_EResultFail
// these stats won't be auto-updated; you'll need to call RequestUserStats() again to refresh any data
STEAM_CALL_RESULT(UserStatsReceived_t)
SteamAPICall_t Steam_UserStats::RequestUserStats(CSteamID steamIDUser)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    _user_stats[steamIDUser].requesting = true;
    pFrameResult_t result(new FrameResult);
    UserStatsReceived_t& usr = result->CreateCallback<UserStatsReceived_t>();
    usr.m_steamIDUser = steamIDUser;

    _cb_manager->add_apicall(this, result);

    return result->GetAPICall();
}

// requests stat information for a user, usable after a successful call to RequestUserStats()
bool Steam_UserStats::GetUserStat(CSteamID steamIDUser, const char* pchName, int32* pData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (steamIDUser == Settings::Inst().userid)
        return GetStat(pchName, pData);

    return false;
}

bool Steam_UserStats::GetUserStat(CSteamID steamIDUser, const char* pchName, float* pData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (steamIDUser == Settings::Inst().userid)
        return GetStat(pchName, pData);

    return false;
}

bool Steam_UserStats::GetUserAchievement(CSteamID steamIDUser, const char* pchName, bool* pbAchieved)
{
    return GetUserAchievementAndUnlockTime(steamIDUser, pchName, pbAchieved, nullptr);
}
// See notes for GetAchievementAndUnlockTime above
bool Steam_UserStats::GetUserAchievementAndUnlockTime(CSteamID steamIDUser, const char* pchName, bool* pbAchieved, uint32* punUnlockTime)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    if (steamIDUser == Settings::Inst().userid)
        return GetAchievementAndUnlockTime(pchName, pbAchieved, punUnlockTime);

    return false;
}

// Reset stats 
bool Steam_UserStats::ResetAllStats(bool bAchievementsToo)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    user_stats_t& user_stats = _user_stats[Settings::Inst().userid];

    if (bAchievementsToo)
    {
        for (auto it = user_stats.achievements.begin(); it != user_stats.achievements.end(); ++it)
        {
            it.value()["earned"] = false;
            it.value()["cur_progress"] = static_cast<uint32_t>(0);
        }
        _achs_dirty = true;
    }

    for (auto it = user_stats.stats.begin(); it != user_stats.stats.end(); ++it)
    {
        switch (it.value().type())
        {
            case nlohmann::json::value_t::number_float:
                it.value() = 0.0f;
                break;

            case nlohmann::json::value_t::number_unsigned:
                it.value() = static_cast<uint32_t>(0);
                break;

            default:;
        }
    }
    _stats_dirty = true;

    return true;
}

// Leaderboard functions

// asks the Steam back-end for a leaderboard by name, and will create it if it's not yet
// This call is asynchronous, with the result returned in LeaderboardFindResult_t
STEAM_CALL_RESULT(LeaderboardFindResult_t)
SteamAPICall_t Steam_UserStats::FindOrCreateLeaderboard(const char* pchLeaderboardName, ELeaderboardSortMethod eLeaderboardSortMethod, ELeaderboardDisplayType eLeaderboardDisplayType)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "Leaderboard name: %s, Sort Method: %d, Display Type: %d", pchLeaderboardName, eLeaderboardSortMethod, eLeaderboardDisplayType);
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    pFrameResult_t res(new FrameResult);
    LeaderboardFindResult_t& lbfr = res->CreateCallback<LeaderboardFindResult_t>();

    auto it = _leaderboards.find(pchLeaderboardName);
    if (it == _leaderboards.end())
    {
        _leaderboards[pchLeaderboardName]["sort_method"]  = static_cast<uint32_t>(eLeaderboardSortMethod);
        _leaderboards[pchLeaderboardName]["display_type"] = static_cast<uint32_t>(eLeaderboardDisplayType);
        it = _leaderboards.find(pchLeaderboardName);
    }

    lbfr.m_bLeaderboardFound = true;
    lbfr.m_hSteamLeaderboard = std::distance(_leaderboards.begin(), it) + 1;

    res->done = true;
    _cb_manager->add_apicall(this, res);

    FileManager::save_json(leaderboards_path, _leaderboards);
    return res->GetAPICall();
}

// as above, but won't create the leaderboard if it's not found
// This call is asynchronous, with the result returned in LeaderboardFindResult_t
STEAM_CALL_RESULT(LeaderboardFindResult_t)
SteamAPICall_t Steam_UserStats::FindLeaderboard(const char* pchLeaderboardName)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::DEBUG, "Leaderboard name: %s", pchLeaderboardName);
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    return FindOrCreateLeaderboard(pchLeaderboardName, k_ELeaderboardSortMethodAscending, k_ELeaderboardDisplayTypeNumeric);

    //pFrameResult_t res(new FrameResult);
    //LeaderboardFindResult_t& lbfr = res->CreateCallback<LeaderboardFindResult_t>();
    //
    //auto it = _leaderboards.find(pchLeaderboardName);
    //if (it == _leaderboards.end())
    //{
    //    lbfr.m_bLeaderboardFound = false;
    //    lbfr.m_hSteamLeaderboard = 0;
    //}
    //else
    //{
    //    lbfr.m_bLeaderboardFound = true;
    //    lbfr.m_hSteamLeaderboard = std::distance(_leaderboards.begin(), it);
    //}
    //
    //res->done = true;
    //GetCBManager()->add_apicall(this, res);
    //return res->GetAPICall();
}

// returns the name of a leaderboard
const char* Steam_UserStats::GetLeaderboardName(SteamLeaderboard_t hSteamLeaderboard)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    --hSteamLeaderboard;

    if (hSteamLeaderboard >= _leaderboards.size())
        return "";

    auto it = _leaderboards.begin();
    std::advance(it, hSteamLeaderboard);

    return it.key().c_str();
}

// returns the total number of entries in a leaderboard, as of the last request
int Steam_UserStats::GetLeaderboardEntryCount(SteamLeaderboard_t hSteamLeaderboard)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    --hSteamLeaderboard;

    if (hSteamLeaderboard >= _leaderboards.size())
        return 0;

    auto it = _leaderboards.begin();
    std::advance(it, hSteamLeaderboard);

    return 0;
}

// returns the sort method of the leaderboard
ELeaderboardSortMethod Steam_UserStats::GetLeaderboardSortMethod(SteamLeaderboard_t hSteamLeaderboard)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    --hSteamLeaderboard;

    if (hSteamLeaderboard >= _leaderboards.size())
        return k_ELeaderboardSortMethodNone;

    auto it = _leaderboards.begin();
    std::advance(it, hSteamLeaderboard);

    return static_cast<ELeaderboardSortMethod>(it.value()["sort_method"].get<int>());
}

// returns the display type of the leaderboard
ELeaderboardDisplayType Steam_UserStats::GetLeaderboardDisplayType(SteamLeaderboard_t hSteamLeaderboard)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    --hSteamLeaderboard;

    if (hSteamLeaderboard >= _leaderboards.size())
        return k_ELeaderboardDisplayTypeNone;

    auto it = _leaderboards.begin();
    std::advance(it, hSteamLeaderboard);

    return static_cast<ELeaderboardDisplayType>(it.value()["display_type"].get<int>());
}

// Asks the Steam back-end for a set of rows in the leaderboard.
// This call is asynchronous, with the result returned in LeaderboardScoresDownloaded_t
// LeaderboardScoresDownloaded_t will contain a handle to pull the results from GetDownloadedLeaderboardEntries() (below)
// You can ask for more entries than exist, and it will return as many as do exist.
// k_ELeaderboardDataRequestGlobal requests rows in the leaderboard from the full table, with nRangeStart & nRangeEnd in the range [1, TotalEntries]
// k_ELeaderboardDataRequestGlobalAroundUser requests rows around the current user, nRangeStart being negate
//   e.g. DownloadLeaderboardEntries( hLeaderboard, k_ELeaderboardDataRequestGlobalAroundUser, -3, 3 ) will return 7 rows, 3 before the user, 3 after
// k_ELeaderboardDataRequestFriends requests all the rows for friends of the current user 
STEAM_CALL_RESULT(LeaderboardScoresDownloaded_t)
SteamAPICall_t Steam_UserStats::DownloadLeaderboardEntries(SteamLeaderboard_t hSteamLeaderboard, ELeaderboardDataRequest eLeaderboardDataRequest, int nRangeStart, int nRangeEnd)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    //auto it = _leaderboards.begin();
    //std::advance(it, hSteamLeaderboard);

    pFrameResult_t res(new FrameResult);
    LeaderboardScoresDownloaded_t& lbsd = res->CreateCallback<LeaderboardScoresDownloaded_t>();

    lbsd.m_hSteamLeaderboard = hSteamLeaderboard;
    --hSteamLeaderboard;

    if (hSteamLeaderboard >= _leaderboards.size())
    {
        lbsd.m_hSteamLeaderboardEntries = 0;
    }
    else
    {
        lbsd.m_hSteamLeaderboardEntries = 9999; // Dummy handle
    }
    lbsd.m_cEntryCount = 0;

    res->done = true;
    _cb_manager->add_apicall(this, res);
    return res->GetAPICall();
}
// as above, but downloads leaderboard entries for an arbitrary set of users - ELeaderboardDataRequest is k_ELeaderboardDataRequestUsers
// if a user doesn't have a leaderboard entry, they won't be included in the result
// a max of 100 users can be downloaded at a time, with only one outstanding call at a time
STEAM_METHOD_DESC(Downloads leaderboard entries for an arbitrary set of users - ELeaderboardDataRequest is k_ELeaderboardDataRequestUsers)
STEAM_CALL_RESULT(LeaderboardScoresDownloaded_t)
SteamAPICall_t Steam_UserStats::DownloadLeaderboardEntriesForUsers(SteamLeaderboard_t hSteamLeaderboard,
    STEAM_ARRAY_COUNT_D(cUsers, Array of users to retrieve) CSteamID * prgUsers, int cUsers)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    //auto it = _leaderboards.begin();
    //std::advance(it, hSteamLeaderboard);

    pFrameResult_t res(new FrameResult);
    LeaderboardScoresDownloaded_t& lbsd = res->CreateCallback<LeaderboardScoresDownloaded_t>();

    lbsd.m_hSteamLeaderboard = hSteamLeaderboard;
    --hSteamLeaderboard;

    if (hSteamLeaderboard >= _leaderboards.size())
    {
        lbsd.m_hSteamLeaderboardEntries = 0;
    }
    else
    {
        lbsd.m_hSteamLeaderboardEntries = 9999; // Dummy handle
    }
    lbsd.m_cEntryCount = 0;

    res->done = true;
    _cb_manager->add_apicall(this, res);
    return res->GetAPICall();
}

// Returns data about a single leaderboard entry
// use a for loop from 0 to LeaderboardScoresDownloaded_t::m_cEntryCount to get all the downloaded entries
// e.g.
//		void OnLeaderboardScoresDownloaded( LeaderboardScoresDownloaded_t *pLeaderboardScoresDownloaded )
//		{
//			for ( int index; index < pLeaderboardScoresDownloaded->m_cEntryCount; index++ )
//			{
//				LeaderboardEntry_t leaderboardEntry;
//				int32 details[3];		// we know this is how many we've stored previously
//				GetDownloadedLeaderboardEntry( pLeaderboardScoresDownloaded->m_hSteamLeaderboardEntries, index, &leaderboardEntry, details, 3 );
//				assert( leaderboardEntry.m_cDetails == 3 );
//				...
//			}
// once you've accessed all the entries, the data will be free'd, and the SteamLeaderboardEntries_t handle will become invalid
bool Steam_UserStats::GetDownloadedLeaderboardEntry(SteamLeaderboardEntries_t hSteamLeaderboardEntries, int index, LeaderboardEntry_t * pLeaderboardEntry, int32 * pDetails, int cDetailsMax)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    return false;
}

// Uploads a user score to the Steam back-end.
// This call is asynchronous, with the result returned in LeaderboardScoreUploaded_t
// Details are extra game-defined information regarding how the user got that score
// pScoreDetails points to an array of int32's, cScoreDetailsCount is the number of int32's in the list
SteamAPICall_t Steam_UserStats::UploadLeaderboardScore(SteamLeaderboard_t hSteamLeaderboard, int32 nScore, int32* pScoreDetails, int cScoreDetailsCount)
{
    return UploadLeaderboardScore(hSteamLeaderboard, k_ELeaderboardUploadScoreMethodKeepBest, nScore, pScoreDetails, cScoreDetailsCount);
}

STEAM_CALL_RESULT(LeaderboardScoreUploaded_t)
SteamAPICall_t Steam_UserStats::UploadLeaderboardScore(SteamLeaderboard_t hSteamLeaderboard, ELeaderboardUploadScoreMethod eLeaderboardUploadScoreMethod, int32 nScore, const int32 * pScoreDetails, int cScoreDetailsCount)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    pFrameResult_t res(new FrameResult);
    LeaderboardScoreUploaded_t& lbsu = res->CreateCallback<LeaderboardScoreUploaded_t>();

    lbsu.m_hSteamLeaderboard = hSteamLeaderboard;
    --hSteamLeaderboard;

    if (hSteamLeaderboard >= _leaderboards.size())
    {
        lbsu.m_bSuccess = 0;
    }
    else
    {
        lbsu.m_bSuccess = 1;
    }

    lbsu.m_nScore = nScore;
    lbsu.m_bScoreChanged = 0;
    lbsu.m_nGlobalRankNew = 0;
    lbsu.m_nGlobalRankPrevious = 0;

    res->done = true;
    _cb_manager->add_apicall(this, res);
    return res->GetAPICall();
}

// Attaches a piece of user generated content the user's entry on a leaderboard.
// hContent is a handle to a piece of user generated content that was shared using ISteamUserRemoteStorage::FileShare().
// This call is asynchronous, with the result returned in LeaderboardUGCSet_t.
STEAM_CALL_RESULT(LeaderboardUGCSet_t)
SteamAPICall_t Steam_UserStats::AttachLeaderboardUGC(SteamLeaderboard_t hSteamLeaderboard, UGCHandle_t hUGC)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    pFrameResult_t res(new FrameResult);
    LeaderboardUGCSet_t& lbus = res->CreateCallback<LeaderboardUGCSet_t>();

    lbus.m_hSteamLeaderboard = hSteamLeaderboard;
    --hSteamLeaderboard;

    if (hSteamLeaderboard >= _leaderboards.size())
    {
        lbus.m_eResult = k_EResultFail;
    }
    else
    {
        lbus.m_eResult = k_EResultOK;
    }

    res->done = true;
    _cb_manager->add_apicall(this, res);
    return res->GetAPICall();
}

// Retrieves the number of players currently playing your game (online + offline)
// This call is asynchronous, with the result returned in NumberOfCurrentPlayers_t
STEAM_CALL_RESULT(NumberOfCurrentPlayers_t)
SteamAPICall_t Steam_UserStats::GetNumberOfCurrentPlayers()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    std::lock(_local_mutex, _cb_manager->_local_mutex);
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    pFrameResult_t res(new FrameResult);
    NumberOfCurrentPlayers_t& nocp = res->CreateCallback<NumberOfCurrentPlayers_t>();

    nocp.m_bSuccess = true;
    nocp.m_cPlayers = 1;
    for (auto& frd : GetSteam_Friends()._friends)
    {
        if (frd.second.app_id == Settings::Inst().gameid.AppID())
        {
            ++nocp.m_cPlayers;
        }
    }

    res->done = true;

    _cb_manager->add_apicall(this, res);

    return res->GetAPICall();
}

// Requests that Steam fetch data on the percentage of players who have received each achievement
// for the game globally.
// This call is asynchronous, with the result returned in GlobalAchievementPercentagesReady_t.
STEAM_CALL_RESULT(GlobalAchievementPercentagesReady_t)
SteamAPICall_t Steam_UserStats::RequestGlobalAchievementPercentages()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    //std::lock(_local_mutex, _cb_manager->_local_mutex);
    //std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    //std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    return k_uAPICallInvalid;
}

// Get the info on the most achieved achievement for the game, returns an iterator index you can use to fetch
// the next most achieved afterwards.  Will return -1 if there is no data on achievement 
// percentages (ie, you haven't called RequestGlobalAchievementPercentages and waited on the callback).
int Steam_UserStats::GetMostAchievedAchievementInfo(char* pchName, uint32 unNameBufLen, float* pflPercent, bool* pbAchieved)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    //std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    return 0;
}

// Get the info on the next most achieved achievement for the game. Call this after GetMostAchievedAchievementInfo or another
// GetNextMostAchievedAchievementInfo call passing the iterator from the previous call. Returns -1 after the last
// achievement has been iterated.
int Steam_UserStats::GetNextMostAchievedAchievementInfo(int iIteratorPrevious, char* pchName, uint32 unNameBufLen, float* pflPercent, bool* pbAchieved)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    //std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    return 0;
}

// Returns the percentage of users who have achieved the specified achievement.
bool Steam_UserStats::GetAchievementAchievedPercent(const char* pchName, float* pflPercent)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    //std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    return false;
}

// Requests global stats data, which is available for stats marked as "aggregated".
// This call is asynchronous, with the results returned in GlobalStatsReceived_t.
// nHistoryDays specifies how many days of day-by-day history to retrieve in addition
// to the overall totals. The limit is 60.
STEAM_CALL_RESULT(GlobalStatsReceived_t)
SteamAPICall_t Steam_UserStats::RequestGlobalStats(int nHistoryDays)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    //std::lock(_local_mutex, _cb_manager->_local_mutex);
    //std::lock_guard<std::recursive_mutex> lk1(_local_mutex, std::adopt_lock);
    //std::lock_guard<std::recursive_mutex> lk2(_cb_manager->_local_mutex, std::adopt_lock);

    return k_uAPICallInvalid;
}

// Gets the lifetime totals for an aggregated stat
bool Steam_UserStats::GetGlobalStat(const char* pchStatName, int64 * pData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    //std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    return false;
}

bool Steam_UserStats::GetGlobalStat(const char* pchStatName, double* pData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    //std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    return false;
}

// Gets history for an aggregated stat. pData will be filled with daily values, starting with today.
// So when called, pData[0] will be today, pData[1] will be yesterday, and pData[2] will be two days ago, 
// etc. cubData is the size in bytes of the pubData buffer. Returns the number of 
// elements actually set.
int32 Steam_UserStats::GetGlobalStatHistory(const char* pchStatName, STEAM_ARRAY_COUNT(cubData) int64 * pData, uint32 cubData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    //std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    return 0;
}

int32 Steam_UserStats::GetGlobalStatHistory(const char* pchStatName, STEAM_ARRAY_COUNT(cubData) double* pData, uint32 cubData)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    //std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    return 0;
}

bool Steam_UserStats::GetAchievementProgressLimits(const char* pchName, int32* pnMinProgress, int32* pnMaxProgress)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    //std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    return false;
}

bool Steam_UserStats::GetAchievementProgressLimits(const char* pchName, float* pfMinProgress, float* pfMaxProgress)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    //std::lock_guard<std::recursive_mutex> lk(_local_mutex);

    return false;
}

bool Steam_UserStats::CBRunFrame()
{
    return false;
}

bool Steam_UserStats::RunNetwork(Network_Message_pb const& msg)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    //GLOBAL_LOCK();

    return false;
}

bool Steam_UserStats::RunCallbacks(pFrameResult_t res)
{
    switch (res->ICallback())
    {
        case UserStatsReceived_t::k_iCallback:
        {
            UserStatsReceived_t& result = res->GetCallback<UserStatsReceived_t>();
            CSteamID userid = Settings::Inst().userid;
            user_stats_t& stats = _user_stats[userid];

            if (userid == result.m_steamIDUser)
            { // Get local user stats (read achievments.json)
                stats.requesting = false;
                result.m_eResult = k_EResultOK;
            }
            else
            {
                assert(0 == 1); // Should never come here, friend stats will be handled in the network callback
            }
            stats.requesting = false;
            res->done = true;
        }
        break;

        case UserStatsStored_t::k_iCallback:
        {
            res->done = save_stats();
            if(res->done) // If we saved, check if we actually saved the stats
                res->GetCallback<UserStatsStored_t>().m_eResult = (_stats_dirty ? k_EResultFail : k_EResultOK);
        }
        break;
    }
    return res->done;
}
