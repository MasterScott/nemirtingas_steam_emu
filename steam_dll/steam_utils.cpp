/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_utils.h"
#include "steam_client.h"
#include "settings.h"

decltype(Steam_Utils::_image_handle) Steam_Utils::_image_handle(100);

Steam_Utils::Steam_Utils():
    _start_time(std::chrono::steady_clock::now()),
    _ipc_calls(0)
{
}

Steam_Utils::~Steam_Utils()
{
    emu_deinit();
}

void Steam_Utils::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
    _cb_manager = cb_manager;
    _network = network;
}

void Steam_Utils::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
    _network.reset();
    _cb_manager.reset();
}

int Steam_Utils::generate_image_handle()
{
    return _image_handle++;
}

int Steam_Utils::generate_image_handle(int32_t width, int32_t height)
{
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);
    int handle = generate_image_handle();
    std::shared_ptr<Image> image = ImageManager::create_image(nullptr, width, height, std::string("sutils") + std::to_string(handle));

    if(image == nullptr)
    {
        --_image_handle;
        return 0;
    }

    _images[handle] = image;
    return handle;
}

int Steam_Utils::generate_image_handle(std::string const& image_path, int32_t width, int32_t height)
{
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);
    int handle = generate_image_handle();
    std::shared_ptr<Image> image = ImageManager::load_image(image_path, std::string("sutils") + std::to_string(handle), width, height);

    if (image == nullptr)
    {
        --_image_handle;
        return 0;
    }

    _images[handle] = image;
    return handle;
}

int Steam_Utils::generate_image_handle(void const* data, size_t data_len, int32_t width, int32_t height)
{
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);
    int handle = generate_image_handle();
    std::shared_ptr<Image> image = ImageManager::load_image(data, data_len, std::string("sutils") + std::to_string(handle), width, height);

    if (image == nullptr)
    {
        --_image_handle;
        return 0;
    }

    _images[handle] = image;
    return handle;
}

int Steam_Utils::generate_image_handle(std::shared_ptr<Image> img)
{
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
    int handle = generate_image_handle();
    std::shared_ptr<Image> image = ImageManager::copy_image(img, std::string("sutils") + std::to_string(handle));

    if (image == nullptr)
    {
        --_image_handle;
        return 0;
    }

    _images[handle] = image;
    return handle;
}

int Steam_Utils::generate_image_handle(std::shared_ptr<Image> img, int32_t width, int32_t height)
{
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
    int handle = generate_image_handle();
    std::shared_ptr<Image> image = ImageManager::copy_image(img, std::string("sutils") + std::to_string(handle), width, height);

    if (image == nullptr)
    {
        --_image_handle;
        return 0;
    }

    _images[handle] = image;
    return handle;
}

std::shared_ptr<Image> Steam_Utils::get_image(int iImage)
{
    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);
    auto it = _images.find(iImage);
    if (it == _images.end())
        return std::shared_ptr<Image>();

    return it->second;
}

// return the number of seconds since the user 
uint32 Steam_Utils::GetSecondsSinceAppActive()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return static_cast<uint32>(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now()-_start_time).count());
}

uint32 Steam_Utils::GetSecondsSinceComputerActive()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return static_cast<uint32>(std::chrono::duration_cast<std::chrono::seconds>(get_uptime()).count());
}

// the universe this client is connecting to
EUniverse Steam_Utils::GetConnectedUniverse()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return Settings::Inst().userid.GetEUniverse();
}

// Steam server time.  Number of seconds since January 1, 1970, GMT (i.e unix time)
uint32 Steam_Utils::GetServerRealTime()
{
    //LOG(Log::LogLevel::TRACE, "");
    return static_cast<uint32>(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count());
}

// returns the 2 digit ISO 3166-1-alpha-2 format country code this client is running in (as looked up via an IP-to-location database)
// e.g "US" or "UK".
const char* Steam_Utils::GetIPCountry()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return "EU";
}

// returns true if the image exists, and valid sizes were filled out
bool Steam_Utils::GetImageSize(int iImage, uint32* pnWidth, uint32* pnHeight)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    if (pnWidth == nullptr)
        return false;
    if (pnHeight == nullptr)
        return false;

    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto it = _images.find(iImage);
    if( it == _images.end())
        return false;

    *pnWidth = it->second->width();
    *pnHeight = it->second->height();

    return true;
}

// returns true if the image exists, and the buffer was successfully filled out
// results are returned in RGBA format
// the destination buffer size should be 4 * height * width * sizeof(char)
bool Steam_Utils::GetImageRGBA(int iImage, uint8* pubDest, int nDestBufferSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    if (pubDest == nullptr)
        return false;

    std::lock_guard<std::recursive_mutex> lk1(_local_mutex);

    auto it = _images.find(iImage);
    if (it == _images.end())
        return false;

    // Image width * height * rgba
    if (nDestBufferSize != it->second->raw_size())
        return false;

    memcpy(pubDest, it->second->get_raw_pointer(), nDestBufferSize);

    return true;
}

// returns the IP of the reporting server for valve - currently only used in Source engine games
bool Steam_Utils::GetCSERIPPort(uint32* unIP, uint16* usPort)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

// return the amount of battery power left in the current system in % [0..100], 255 for being on AC power
uint8 Steam_Utils::GetCurrentBatteryPower()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return 255;
}

// returns the appID of the current process
uint32 Steam_Utils::GetAppID()
{
    //LOG(Log::LogLevel::TRACE, "");
    return Settings::Inst().gameid.AppID();
}

// Sets the position where the overlay instance for the currently calling game should show notifications.
// This position is per-game and if this function is called from outside of a game context it will do nothing.
void Steam_Utils::SetOverlayNotificationPosition(ENotificationPosition eNotificationPosition)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// API asynchronous call results
// can be used directly, but more commonly used via the callback dispatch API (see steam_api.h)
bool Steam_Utils::IsAPICallCompleted(SteamAPICall_t hSteamAPICall, bool* pbFailed)
{
    //LOG(Log::LogLevel::TRACE, "");
    return _cb_manager->IsAPICallCompleted(hSteamAPICall, pbFailed);
}

ESteamAPICallFailure Steam_Utils::GetAPICallFailureReason(SteamAPICall_t hSteamAPICall)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return _cb_manager->GetAPICallFailureReason(hSteamAPICall);
}

bool Steam_Utils::GetAPICallResult(SteamAPICall_t hSteamAPICall, void* pCallback, int cubCallback, int iCallbackExpected, bool* pbFailed)
{
    //LOG(Log::LogLevel::TRACE, "");
    return _cb_manager->GetAPICallResult(hSteamAPICall, pCallback, cubCallback, iCallbackExpected, pbFailed);
}

// Deprecated. Applications should use SteamAPI_RunCallbacks() instead. Game servers do not need to call this function.
void Steam_Utils::RunFrame_old()
{
    //LOG(Log::LogLevel::TRACE, "");
}

// returns the number of IPC calls made since the last time this function was called
// Used for perf debugging so you can understand how many IPC calls your game makes per frame
// Every IPC call is at minimum a thread context switch if not a process one so you want to rate
// control how often you do them.
uint32 Steam_Utils::GetIPCCallCount()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    _ipc_calls += 13;
    return _ipc_calls;
}

// API warning handling
// 'int' is the severity; 0 for msg, 1 for warning
// 'const char *' is the text of the message
// callbacks will occur directly after the API function is called that generated the warning or message
void Steam_Utils::SetWarningMessageHook(SteamAPIWarningMessageHook_t pFunction)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

// Returns true if the overlay is running & the user can access it. The overlay process could take a few seconds to
// start & hook the game process, so this function will initially return false while the overlay is loading.
bool Steam_Utils::IsOverlayEnabled()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    auto& overlay = GetSteam_Overlay();
    return overlay.Ready();
}

// Normally this call is unneeded if your game has a constantly running frame loop that calls the 
// D3D Present API, or OGL SwapBuffers API every frame.
//
// However, if you have a game that only refreshes the screen on an event driven basis then that can break 
// the overlay, as it uses your Present/SwapBuffers calls to drive it's internal frame loop and it may also
// need to Present() to the screen any time an even needing a notification happens or when the overlay is
// brought up over the game by a user.  You can use this API to ask the overlay if it currently need a present
// in that case, and then you can check for this periodically (roughly 33hz is desirable) and make sure you
// refresh the screen with Present or SwapBuffers to allow the overlay to do it's work.
bool Steam_Utils::BOverlayNeedsPresent()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    auto& overlay = GetSteam_Overlay();
    return overlay.NeedPresent();
}

// Asynchronous call to check if an executable file has been signed using the public key set on the signing tab
// of the partner site, for example to refuse to load modified executable files.  
// The result is returned in CheckFileSignature_t.
//   k_ECheckFileSignatureNoSignaturesFoundForThisApp - This app has not been configured on the signing tab of the partner site to enable this function.
//   k_ECheckFileSignatureNoSignaturesFoundForThisFile - This file is not listed on the signing tab for the partner site.
//   k_ECheckFileSignatureFileNotFound - The file does not exist on disk.
//   k_ECheckFileSignatureInvalidSignature - The file exists, and the signing tab has been set for this file, but the file is either not signed or the signature does not match.
//   k_ECheckFileSignatureValidSignature - The file is signed and the signature is valid.
STEAM_CALL_RESULT(CheckFileSignature_t)
SteamAPICall_t Steam_Utils::CheckFileSignature(const char* szFileName)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    pFrameResult_t result(new FrameResult);
    CheckFileSignature_t& cfs = result->CreateCallback<CheckFileSignature_t>();

    cfs.m_eCheckFileSignature = k_ECheckFileSignatureValidSignature;

    result->done = true;
    _cb_manager->add_apicall(nullptr, result);
    return result->GetAPICall();
}

// Activates the Big Picture text input dialog which only supports gamepad input
bool Steam_Utils::ShowGamepadTextInput(EGamepadTextInputMode eInputMode, EGamepadTextInputLineMode eLineInputMode, const char* pchDescription, uint32 unCharMax)
{
    return ShowGamepadTextInput(eInputMode, eLineInputMode, pchDescription, unCharMax, nullptr);
}

bool Steam_Utils::ShowGamepadTextInput(EGamepadTextInputMode eInputMode, EGamepadTextInputLineMode eLineInputMode, const char* pchDescription, uint32 unCharMax, const char* pchExistingText)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

// Returns previously entered text & length
uint32 Steam_Utils::GetEnteredGamepadTextLength()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return 0;
}

bool Steam_Utils::GetEnteredGamepadTextInput(char* pchText, uint32 cchText)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

// returns the language the steam client is running in, you probably want ISteamApps::GetCurrentGameLanguage instead, this is for very special usage cases
const char* Steam_Utils::GetSteamUILanguage()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return Settings::Inst().language.c_str();
}

// returns true if Steam itself is running in VR mode
bool Steam_Utils::IsSteamRunningInVR()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

// Sets the inset of the overlay notification from the corner specified by SetOverlayNotificationPosition.
void Steam_Utils::SetOverlayNotificationInset(int nHorizontalInset, int nVerticalInset)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    auto& overlay = GetSteam_Overlay();
    overlay.SetNotificationInset(nHorizontalInset, nVerticalInset);
}

// returns true if Steam & the Steam Overlay are running in Big Picture mode
// Games much be launched through the Steam client to enable the Big Picture overlay. During development,
// a game can be added as a non-steam game to the developers library to test this feature
bool Steam_Utils::IsSteamInBigPictureMode()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

// ask SteamUI to create and render its OpenVR dashboard
void Steam_Utils::StartVRDashboard()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

// Returns true if the HMD content will be streamed via Steam In-Home Streaming
bool Steam_Utils::IsVRHeadsetStreamingEnabled()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

// Set whether the HMD content will be streamed via Steam In-Home Streaming
// If this is set to true, then the scene in the HMD headset will be streamed, and remote input will not be allowed.
// If this is set to false, then the application window will be streamed instead, and remote input will be allowed.
// The default is true unless "VRHeadsetStreaming" "0" is in the extended appinfo for a game.
// (this is useful for games that have asymmetric multiplayer gameplay)
void Steam_Utils::SetVRHeadsetStreamingEnabled(bool bEnabled)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
}

// Returns whether this steam client is a Steam China specific client, vs the global client.
bool Steam_Utils::IsSteamChinaLauncher()
{
    APP_LOG(Log::LogLevel::TRACE, "");
    APP_LOG(Log::LogLevel::INFO, "TODO");
    return false;
}

// Initializes text filtering.
//   Returns false if filtering is unavailable for the language the user is currently running in.
bool Steam_Utils::InitFilterText()
{
    return InitFilterText(0);
}

bool Steam_Utils::InitFilterText(uint32 unFilterOptions)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return true;
}

// Filters the provided input message and places the filtered result into pchOutFilteredText.
//   pchOutFilteredText is where the output will be placed, even if no filtering or censoring is performed
//   nByteSizeOutFilteredText is the size (in bytes) of pchOutFilteredText
//   pchInputText is the input string that should be filtered, which can be ASCII or UTF-8
//   bLegalOnly should be false if you want profanity and legally required filtering (where required) and true if you want legally required filtering only
//   Returns the number of characters (not bytes) filtered.
int Steam_Utils::FilterText(char* pchOutFilteredText, uint32 nByteSizeOutFilteredText, const char* pchInputMessage, bool bLegalOnly)
{
    return FilterText(ETextFilteringContext::k_ETextFilteringContextUnknown, k_steamIDNil, pchInputMessage, pchOutFilteredText, nByteSizeOutFilteredText);
}

int Steam_Utils::FilterText(ETextFilteringContext eContext, CSteamID sourceSteamID, const char* pchInputMessage, char* pchOutFilteredText, uint32 nByteSizeOutFilteredText)
{
    APP_LOG(Log::LogLevel::TRACE, "");

    return 0;
}

ESteamIPv6ConnectivityState Steam_Utils::GetIPv6ConnectivityState(ESteamIPv6ConnectivityProtocol eProtocol)
{
    return ESteamIPv6ConnectivityState::k_ESteamIPv6ConnectivityState_Unknown;
}
