/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "steam_video.h"
#include "steam_client.h"

Steam_Video::Steam_Video()
{
}

Steam_Video::~Steam_Video()
{
    emu_deinit();
}

void Steam_Video::emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network)
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);
    _cb_manager = cb_manager;
    _network = network;
}

void Steam_Video::emu_deinit()
{
    TRACE_FUNC();
    std::lock_guard<std::recursive_mutex> lk(_local_mutex);
    _network.reset();
    _cb_manager.reset();
}

// Get a URL suitable for streaming the given Video app ID's video
void Steam_Video::GetVideoURL(AppId_t unVideoAppID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

// returns true if user is uploading a live broadcast
bool Steam_Video::IsBroadcasting(int* pnNumViewers)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}

// Get the OPF Details for 360 Video Playback
STEAM_CALL_BACK(GetOPFSettingsResult_t)
void Steam_Video::GetOPFSettings(AppId_t unVideoAppID)
{
    APP_LOG(Log::LogLevel::TRACE, "");
}

bool Steam_Video::GetOPFStringForApp(AppId_t unVideoAppID, char* pchBuffer, int32* pnBufferSize)
{
    APP_LOG(Log::LogLevel::TRACE, "");
    return false;
}