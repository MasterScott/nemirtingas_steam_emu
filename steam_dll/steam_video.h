/*
 * Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the Nemirtingas's Steam Emulator
 *
 * The Nemirtingas's Steam Emulator is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * The Nemirtingas's Steam Emulator is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the Nemirtingas's Steam Emulator; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "common_includes.h"
#include "callback_manager.h"
#include "network.h"

class LOCAL_API LOCAL_API Steam_Video :
public ISteamVideo001,
public ISteamVideo002
{
    std::shared_ptr<Callback_Manager> _cb_manager;
    std::shared_ptr<Network> _network;

public:
    std::recursive_mutex _local_mutex;

    Steam_Video();
    virtual ~Steam_Video();
    void emu_init(std::shared_ptr<Callback_Manager> cb_manager, std::shared_ptr<Network> network);
    void emu_deinit();

    // Get a URL suitable for streaming the given Video app ID's video
    virtual void GetVideoURL(AppId_t unVideoAppID);

    // returns true if user is uploading a live broadcast
    virtual bool IsBroadcasting(int* pnNumViewers);

    // Get the OPF Details for 360 Video Playback
    STEAM_CALL_BACK(GetOPFSettingsResult_t)
    virtual void GetOPFSettings(AppId_t unVideoAppID);
    virtual bool GetOPFStringForApp(AppId_t unVideoAppID, char* pchBuffer, int32* pnBufferSize);
};